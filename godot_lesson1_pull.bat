@ECHO off
set BROWSER=none

if [%1] == [] GOTO missing_csv

set INPUT_CSV=%1

:main
echo THIS SCRIPT WILL NOT RUN ANY TESTS
echo We will just git pull ALL repositories so that they can be easily
echo opened with Godot and manually tested
echo ...
echo Before proceeding, ensure that the following are true:
echo - The repositories/ folder exists and contains every Git repository
echo   as created by 05_initialize_repositories.sh
echo - Git username/email are correctly configured here
echo ...
pause
echo Changing to repositories folder...
cd repositories
(
FOR /F "tokens=2* delims=," %%A IN (..\%INPUT_CSV%) DO (
	echo %%A\programacion-multimedia-y-dispositivos-moviles
	cd %%A\programacion-multimedia-y-dispositivos-moviles
	git pull
	cd ..\..
 )
)

pause
cd ..
GOTO finish

:missing_csv
echo Missing 1st parameter. We are expecting the csv file path.
set /p USEDEFAULT="Do you want to use a default value (gitlab_projects.csv)? (y/n) "
if [%USEDEFAULT%] == [y] GOTO use_default_csv
echo OK. Aborting...
pause
exit

:use_default_csv
echo OK. Using default...
set INPUT_CSV=gitlab_projects.csv
GOTO main

:finish
echo All done. Finishing...
pause
