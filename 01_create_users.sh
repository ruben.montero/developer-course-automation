#!/bin/bash

GITLAB_URL="$(cat credentials/gitlab_url)"
CERT_NAME="credentials/my.crt"
TOKEN="$(cat credentials/gitlab_admin_token)"
GITLAB_UID="$(cat credentials/gitlab_admin_uid)"
CSV_PATH=$1
OUTPUT_PATH="gitlab_users.csv"

function print_help {
    echo Authenticates against a GitLab API and creates
    echo several users based on an input CSV file
    echo
    echo Usage: $0 USERS_CSV_FILE
    echo
    echo The USERS_CSV_FILE must contain data formed like
    echo
    echo username1,Name1 LastName1,Password1,name1@email.com
    echo username2,Name2 LastName2,Password2,name2@email.com
    echo ...
    echo
    echo A file named \'gitlab_users.csv\' will be generated with the
    echo very same content, but the GitLab uid is added as the first
    echo column like this:
    echo
    echo 32,username1,Name1 LastName1,name1@email.com
    echo 33,username2,Name2 LastName2,name2@email.com
    echo ...
}

if [[ "$1" = "--help" || "$1" = "-h" ]]; then
	print_help
	echo
    echo -e "\e[33mVersion: \e[0m1.2.0 (24/09/2024)"
	exit 0
fi

if [[ $GITLAB_URL == 'https://my.gitlab.instance' ]]; then
    echo -e "\e[1;31mERROR: \e[0mIt appears that default (dummy) credentials are being used. You should appropriately configure the contents of credentials/ folder to allow access to your GitLab instance."
    exit -1
fi

if (( $# != 1 )); then
    echo -e "\e[1;31mERROR: \e[0mIllegal number of parameters."
    echo
    print_help
    exit -1
fi

if [ -z "$(command -v jq)" ]; then
	echo jq is not installed, you can install it using sudo apt-get install jq
	echo In MinGW-w64: pacman -S mingw-w64-x86_64-jq
	exit -1
fi

if [ -f "$OUTPUT_PATH" ]; then
    echo -e "\e[1;31mERROR: \e[0mOutput file '$OUTPUT_PATH' already exists. Aborting execution to prevent overwritting it..."
    exit -1
fi

echo Removing responses folder for any previous run...
rm -rf responses
echo Creating new folder...
mkdir responses

echo Sending out version check request...
curl --cacert $CERT_NAME --header "PRIVATE-TOKEN: $TOKEN" "$GITLAB_URL/api/v4/version" | jq . > responses/version.json
gversion=$(jq .version responses/version.json)
if [[ -z ${gversion} || "$gversion" == "null" ]]; then
	echo -e "\e[1;31mERROR: \e[0mWrong response. Halting execution. Maybe you want to verify the GITLAB_URL, GITLAB_UID, TOKEN or CERT_NAME inside the script"
	exit -1
else
	echo -e "\e[42m[OK]\e[0m Successfully received response from host."
	echo GitLab version is $gversion
fi
echo Reading CSV file with user data...
while IFS=, read -r username name password email
do
	# This will fire HTTP requests against the GitLab API in a for-loop
	# That is dangerous! Take into consideration the number of users to
	# create and the rate limits configuration of your instance
	# https://docs.gitlab.com/ee/security/rate_limits.html
	echo ========================================
	echo "Creating user $name ($username) with email $email..."
	curl --cacert $CERT_NAME -X POST -F "can_create_group=false" \
		-F "private_profile=true" \
		-F "projects_limit=5" \
		-F "skip_confirmation=true" \
		-F "username=$username" \
		-F "password=$password" \
		-F "email=$email" \
		-F "name=$name" \
		--header "PRIVATE-TOKEN: $TOKEN" "$GITLAB_URL/api/v4/users" | jq . > responses/create_$username.json
	response_id=$(jq .id responses/create_$username.json)
	response_username=$(jq .username responses/create_$username.json)
	response_name=$(jq .name responses/create_$username.json)
	response_email=$(jq .email responses/create_$username.json)
	if [[ -z ${response_id} || "$response_id" = "null" ]]; then
		echo -e "\e[1;31mERROR: \e[0mWrong response. Halting execution. Maybe you want to check the server reply under the responses/ folder..."
		echo -e "\e[1;33mWARNING: \e[0mgitlab_users.csv output file is incomplete and could be overwritten. You might want to backup it"
		exit -1
	fi
	echo -e "\e[42m[OK]\e[0m Successfully created user $response_id: $response_username ($response_name) with email $response_email"
	echo Adding to output file...
	echo "$response_id,$response_username,$response_name,$response_email" >> $OUTPUT_PATH
done < $CSV_PATH
echo Removing responses folder after requests loop has exited successfully...
rm -rf responses
echo -e "\e[42m[OK]\e[0m All finished. Have a nice day!"
