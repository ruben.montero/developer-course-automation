# Contributing

Se queres contribuir ao proxecto, o mellor modo é tomar a tempo de instalar o teu servidor e probar o fluxo de traballo. O feedback dos demais sempre axuda a mellorar.

Tamén, podes crear as túas propias tarefas na carpeta templates/ do repositorio, e contribuir con novo material didáctico. ¡Se cres que o resto do mundo pode aproveitarse do teu traballo non dudes en enviar unha merge request!

## Crear novas tarefas

Por favor, divide cada tarefa en 5 seccións:

* Resumo
* Descripción
* Tarefa
* Por último

...como se mostra [nesta plantilla](templates/react/lesson1/issues/00_intro_pick_all/00_getting_started_pick_all/getting_started.md)
