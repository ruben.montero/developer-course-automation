# PATACA

#### (anteriormente Developer Course Automation)

`PATACA` significa:

* Platillas
* Aleatorizadas con
* Tests
* Automáticos para
* Cada
* Alumno

Este proxecto permite xerar, a partir de plantillas[^1] que soportan parámetros variables[^2], unha listaxe de tarefas deseñadas para que cada estudante traballe os contidos dunha unidade didáctica[^3]. Cada tarefa, ademáis dun plantexamento e obxectivos, ten un _test_ automático asociado. Estes _tests_ xéranse congruentemente cos parámetros variables antes descritos, de xeito que cada estudante ten un _test_ diferente.

O _software_ está pensado para que ti teñas a túa instalación de GitLab[^4] nun ordenador funcionando coma servidor no teu centro de ensinanza. A partir de ahí, varios _scripts_ permitiránche xestionar os usuarios, os seus repositorios, e procesar as plantillas e as correccións automáticas nun fluxo de traballo como se describe a continuación:

<img src="doc/img/workflow_explanation.png" />

Exemplo de uso dos _scripts_:

```bash
./01_create_users.sh csv/example/initial_input.csv # Evidentemente, empregarás un CSV cos datos dos teus estudantes
                                                   # Isto producirá un gitlab_users.csv
```

```bash
./02_create_projects.sh gitlab_users.csv 'Curso React' # Isto producirá un gitlab_projects.csv
```

```bash
./03_process_templates.sh gitlab_projects.csv templates/react/lesson1/issues reactLesson1 # Isto xerará a carpeta reactLesson1
```

```bash
./04_create_issues.sh reactLesson1 gitlab_projects.csv
# Pódese especificar tamén un milestone con datas de inicio e fin
./04_create_issues.sh reactLesson1 gitlab_projects.csv "Sprint 1" 2024-10-01 2024-10-31
```

```bash
./05_initialize_repositories.sh gitlab_projects.csv templates/react/lesson1/repository reactLesson1 react-introduction/cypress/e2e
# Se queres, podes omitir copiar os tests indicando --no-tests
./05_initialize_repositories.sh gitlab_projects.csv templates/react/lesson1/repository --no-tests
# Tamén podes omitir copiar o esqueleto do repositorio con --no-repo-template. Isto serviría para copiar só os tests a todos ao final
./05_initialize_repositories.sh gitlab_projects.csv --no-repo-template reactLesson1 react-introduction/cypress/e2e
```

### Ayuda

Cada _script_ ten o comando `--help` que explícache en detalle como debe usarse.

## Contribuir ao proxecto

Se queres contribuir ao proxecto, o mellor modo é tomar a tempo de instalar o teu servidor e probar o fluxo de traballo. O _feedback_ dos demais sempre axuda a mellorar.

Tamén, podes **crear** as túas propias tarefas na carpeta `templates/` do repositorio, e **contribuir** con novo material didáctico. ¡Se cres que o resto do mundo pode aproveitarse do teu traballo non dudes en [enviar unha _merge request_](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)!





[^1]: Cada plantilla consiste nun enunciado en formato _markdown_ e un test automático na linguaxe de programación da unidade didáctica en particular.

[^2]: Os parámetros que se soportan son: `RANDOMNUMBER` para xerar un número aleatorio, `RANDOMEXPRESSION` para elixir unha expresión dunha lista, `ISSUENUMBER`, `DEVELOPERNUMBER` e `TESTFILENAME`.

[^3]: Seguindo as convencións de desenvolvemento software, actualmente, as unidades didácticas denomínanse sprints e teñen unha duración de 2 semanas.

[^4]: Para instalar o teu GitLab sigue as instruccións [aquí](https://packages.gitlab.com/gitlab/gitlab-ce/install) ou [aquí](https://about.gitlab.com/install/)


