import context
import unittest
from advanced import loops


class DinoLoopTestCase(unittest.TestCase):

    def test_dino_is_found(self):
        self.assertTrue(loops.RANDOMEXPRESSIONfind_dino|search_for_dino|look_for_dinoEND("Triceratops"))
        self.assertTrue(loops.RANDOMEXPRESSIONfind_dino|search_for_dino|look_for_dinoEND("Tiranosaurio"))
        self.assertTrue(loops.RANDOMEXPRESSIONfind_dino|search_for_dino|look_for_dinoEND("Diplodocus"))
        self.assertTrue(loops.RANDOMEXPRESSIONfind_dino|search_for_dino|look_for_dinoEND("RANDOMEXPRESSIONCuellilargo|VelocirraptorEND"))
        self.assertTrue(loops.RANDOMEXPRESSIONfind_dino|search_for_dino|look_for_dinoEND("Pterodáctilo"))

    def test_dino_is_not_found(self):
        self.assertFalse(loops.RANDOMEXPRESSIONfind_dino|search_for_dino|look_for_dinoEND("Homo Habilis"))


if __name__ == '__main__':
    unittest.main()

