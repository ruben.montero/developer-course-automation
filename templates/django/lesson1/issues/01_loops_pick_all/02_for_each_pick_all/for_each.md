Dejemos los números atrás

## :books: Resumen

* Recorreremos una lista que tiene `strings` en vez de números
* Añadiremos una función a `loops.py`

## Descripción

Hemos trabajado con listas de números, que se asemejan a los _arrays_ de problemas que estamos acostumbrados a afrontar.

Deberíamos también poner en práctica algún ejemplo de bucle `for-each` sobre una lista _no numérica_.

## :package: La tarea

**Se pide** que añadas una nueva función `RANDOMEXPRESSIONfind_dino|search_for_dino|look_for_dinoEND` a `loops.py`:

* Recibirá por parámetro **un** `string`
* Recorrerá, en un bucle **for**, la siguiente _lista_:

```python
["Triceratops", "Tiranosaurio", "Diplodocus", "Pterodáctilo", "RANDOMEXPRESSIONCuellilargo|VelocirraptorEND"]
```

Para cada uno, lo comparará con el **parámetro** recibido:

* Si coinciden, instantánemente hará `return True`
* Si el bucle termina su ejecución y no ha habido una comparación exitosa, hará `return False`

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
