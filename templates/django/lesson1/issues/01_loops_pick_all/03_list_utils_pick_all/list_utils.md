Herramientas para usar en listas

## :books: Resumen

* Aprenderemos a usar algunas herramientas útiles para trabajar con listas
* Añadiremos una función a `loops.py`

## Descripción

Hasta ahora, nuestro manejo de las _listas_ se limita a un bucle `for`.

¡Podemos hacer muchas más cosas! 

### `in`

En la tarea anterior hemos recorrido una lista para _buscar_ un elemento. En concreto, el nombre de un **dinosaurio**.

¡No hace falta!

En ejercicios anteriores se mencionó de pasada que el operador `in` sirve para verificar si _un elemento_ está en _una lista_:

```python
'a' in ['a', 'b', 'c']
```

Probémoslo.

## :package: La tarea

**Añadamos** una **nueva función** `find_dino2` a `loops.py`:

```python
def find_dino2(dinosaur):
    if dinosaur in ["Triceratops", "Diplodocus", "Pterodáctilo"]:
        return True
    else:
        return False
```

### Un _casi_ error muy común

Código como este:

```python
if condicion == True:
    return True
else:
    return False
```

...es _boilerplate_ típico de principiante en cualquier lenguaje de programación, pues puede escribirse más brevemente:

```python
return condicion == True
```

**Modifica** `find_dino2` y **déjalo** así:

```python
def find_dino2(dinosaur):
    return dinosaur in ["Triceratops", "Diplodocus", "Pterodáctilo"]
```

¡Qué compacto!

### `len`

Para hallar la longitud (tamaño) de una _lista_ en Python se usa `len(...)`. Es una abreviatura de `length` (longitud).

**Añade** una nueva función a `loops.py`:

```python
def RANDOMEXPRESSIONlen_example|example_length|example_list_size|list_size_exampleEND():
    list1 = ["Alice", "Bob"]
    size1 = len(list1)
    print("El tamaño de la primera lista es: " + str(size1))

    list2 = [RANDOMEXPRESSIONNone, None, None|None, None, None, None|None, None, None, None, None|None, None, None, None, None, NoneEND]
    size2 = len(list2)
    print("El tamaño de la segunda lista es: " + str(size2))

    list3 = [[1, 2], [3, 4]]
    size3 = len(list3)
    print("El tamaño de la tercera lista es: " + str(size3))
```

### Acceder a un elemento

Para acceder a un elemento de una lista, usamos los corchetes (`[ ]`) y un _índice_, como en otros lenguajes de programación.

```
>>> numeros = [8, 14, 3]
>>> numeros[0]
8
>>> numeros[2]
3
```

¡Podemos liarla especificando un índice **inválido**!

```
>>> numeros[3]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: list index out of range
```

### Sublistas

Algo **muy interesante** de Python es que podemos acceder a un _fragmento_ de una lista con mucha facilidad, empleando dos puntos (`:`):

```
>>> numeros[0:0]
[]
>>> numeros[0:1]
[8]
>>> numeros[0:2]
[8, 14]
>>> numeros[0:3]
[8, 14, 3]
>>> numeros[0:4]
[8, 14, 3]
>>> numeros[1:3]
[14, 3]
```

Como ves, con esta sintaxis no saltan excepciones `IndexError`. Tan sólo se devuelven listas vacías.

¡Incluso se permite un índice **negativo**!

Significará _empezar por el final de la lista_:

```
>>> numeros[-1]
3
>>> numeros[-2]
14
```

Con respecto a esto, no profundizaremos mucho, aunque es interesante conocer el potencial.

### Continuando con la tarea...

**Añade** una nueva **función** a `loops.py` que **reciba un parámetro**. Será un _índice_.

La función se llamará `retrieve_value` y:

* Recuperará el valor del elemento de la lista `[4, 8, -35, "Pepe Depura", RANDOMNUMBER5-50END]` especificado por el _índice_ recibido por parámetro y lo devolverá usando `return` (e.g.: Si por parámetro recibe un `2`, debe devolver `-35`)
* En caso de ser un índice inválido, **no** fallará. Devolverá `None`

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

