Otro uso más de range

## :books: Resumen

* Añadiremos una nueva función a `loops.py` que hará un bucle `for` básico en Python, esta vez añadiendo el _número inicial_ **y** el _step_ a `range`

## Descripción

Hemos usado `range` especificando _desde qué número_ y _hasta qué número_ queremos nuestro rango.

También se puede el [**incremento** (o _step_)](https://realpython.com/python-range/#rangestart-stop-step). Así, en vez de ir **de 1 en 1**, podemos ir **de 2 en 2**, de **4 en 4**, etc.

```
range(start, end, step)
```

## :package: La tarea

**Se pide** que añadas una nueva función `RANDOMEXPRESSIONsimple_for_with_step|simple_for_step|simple_for_with_increment|simple_for_incrementEND` a `loops.py`:

* Funcionará como `simple_for()`, pero imprimirá los números:
  * Desde RANDOMNUMBER50-100END (incluido)
  * Hasta RANDOMNUMBER101-150END (excluido)
  * Los imprimira **de 3 en 3**

Prueba el _output_. ¿Tiene sentido? ¿Te parece lógico el último número que se ha imprimido?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
