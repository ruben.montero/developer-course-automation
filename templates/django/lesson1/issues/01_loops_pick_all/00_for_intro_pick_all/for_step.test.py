import context
import io
import unittest
from unittest.mock import patch
from advanced import loops


class StepForTestCase(unittest.TestCase):

    def test_simple_for_output_is_ok(self):
        expected_output = ""
        range_start = RANDOMNUMBER50-100END
        i = 0
        for i in range(range_start, RANDOMNUMBER101-150END, 3):
            expected_output += str(i) + "\n"

        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            loops.RANDOMEXPRESSIONsimple_for_with_step|simple_for_step|simple_for_with_increment|simple_for_incrementEND()
            self.assertTrue(fake_out.getvalue().__contains__(expected_output))
            self.assertFalse(fake_out.getvalue().__contains__(str(i+1)))
            self.assertFalse(fake_out.getvalue().__contains__(str(range_start-1)))


if __name__ == '__main__':
    unittest.main()
