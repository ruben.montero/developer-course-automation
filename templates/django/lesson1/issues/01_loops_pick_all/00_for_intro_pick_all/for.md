Bucles for

## :books: Resumen

* Estrenaremos el paquete `advanced/`
* Crearemos un fichero `loops.py` que hará un bucle `for` básico en Python dentro de una función

## Descripción

Vamos a pasar de trabajar en el paquete `intro/` a `advanced/`.

Hablaremos de...

### ...listas en Python `[ ]`

Aquí no hay `String[]`, `ArrayList<String>`,...

**Todas** las listas son iguales, y contienen elementos de _cualquier_ tipo.

Ejemplos:

```python
lista1 = [1, 5, 6]
otro_array = ["Pepe", "Alice"]
lista_vacia = []
lista_heterogenea = [1, "nombre", None]
lista_de_listas = [ [1, 5], [7, 8] ]
```

## [Bucles `for` en Python](https://www.geeksforgeeks.org/python-for-loops/)

En Python **no** existe el bucle `for` clásico, tipo C ó Java:

```java
// NO EXISTE
for (int i = 0; i < numero; i++)
```

**Todos los bucles** son del estilo `for-each`.

### ¿Entonces cómo _recorro_ una lista?

Así:

```python
lista1 = [1, 5, 6]

for elemento in lista1:
    print(elemento)
```

### ¿Y si quiero recorrer índices del `1` al `50`, por ejemplo?

Puedes hacerlo usando la utilidad `range`:

```python
# Imprime 0 1 2 3 4 5 6 7 8 9
for i in range(10):
    print(i)
```

## :package: La tarea

**Añade** un fichero `loops.py` a la carpeta (paquete) `advanced/`.

Escribe una nueva función llamada `simple_for`, que **no** reciba parámetros.

* Dentro de `def simple_for():`, escribe un bucle `for` como el de arriba, que imprima los números de `0` al `RANDOMNUMBER50-80END`

Puedes escribir un `if __name__ == '__main__':` para _invocar_ la función y probarla.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
