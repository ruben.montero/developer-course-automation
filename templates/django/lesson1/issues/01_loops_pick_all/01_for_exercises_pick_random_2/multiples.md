Múltiplos

## :books: Resumen

* Añadiremos una nueva función a `loops.py` que hará un bucle `for`
* Imprimirá los números que son múltiplos de _uno dado_ entre dos valores

## Descripción

Practiquemos un poco esto de los bucles.

No existe una solución única para esta tarea, así que adelante con tu mejor esfuerzo para cumplir los requisitos.

## :package: La tarea

**Se pide** que añadas una nueva función `RANDOMEXPRESSIONmultiples|print_multiples|show_multiplesEND` a `loops.py`:

* Imprimirá por pantalla usando `print` los números:
  * **Mayores** que **RANDOMNUMBER20-33END**
  * **Menores** que **RANDOMNUMBER40-100END**
  * Que sean **múltiplos** de **RANDOMNUMBER3-6END**
  
### Una idea...

El operador `módulo` se escribe en Python como `%`.

Recuerda que este operador representa _el resto de la división entera_, por ejemplo:

```
>>> 8 % 5
3
>>> 12 % 5
2
>>> (20 % 4) == 0
True
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
