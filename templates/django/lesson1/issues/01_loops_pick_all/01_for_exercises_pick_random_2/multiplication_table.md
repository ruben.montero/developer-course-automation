Tablas de multiplicar

## :books: Resumen

* Añadiremos una nueva función a `loops.py` que hará un bucle `for`
* Imprimirá la tabla de multiplicar de un número recibido por parámetro

## Descripción

No existe una solución única para esta tarea, así que adelante con tu mejor esfuerzo para cumplir los requisitos.

## :package: La tarea

**Se pide** que añadas una nueva función `RANDOMEXPRESSIONprint_table|multiplication_table|print_multiplication_table|show_multiplication_tableEND` a `loops.py`:

* Recibirá por parámetro un número cualquiera
* Imprimirá por pantalla usando `print` los números correspondientes a la tabla de multiplicar de ese número
  * Es decir, línea a línea, el resultado de multiplicar ese número por 1, 2, 3,... hasta 10 (incluido)
  
Por ejemplo, se espera lo siguiente:

```python
# Si invoco la función con 1
1
2
3
4
5
6
7
8
9
10

# Si invoco la función con 7
7
14
21
28
35
42
49
56
63
70
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
