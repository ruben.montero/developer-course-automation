import context
import io
import unittest
from unittest.mock import patch
from advanced import loops


class MultiplesForTestCase(unittest.TestCase):

    def test_for_multiples_contain_correct_values_within_closed_range(self):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            loops.RANDOMEXPRESSIONmultiples|print_multiples|show_multiplesEND()
            self.assertTrue(fake_out.getvalue().__contains__(self.__expected_out()))

    def test_for_multiples_doesnt_exceed_lower_bound(self):
        lower_bound = RANDOMNUMBER20-33END
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            loops.RANDOMEXPRESSIONmultiples|print_multiples|show_multiplesEND()
            for i in range(lower_bound, lower_bound - RANDOMNUMBER3-6END, -1):
                self.assertFalse(fake_out.getvalue().__contains__(str(i)))

    def test_for_multiples_doesnt_exceed_upper_bound(self):
        upper_bound = RANDOMNUMBER40-100END
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            loops.RANDOMEXPRESSIONmultiples|print_multiples|show_multiplesEND()
            for i in range(upper_bound, upper_bound + RANDOMNUMBER3-6END, 1):
                self.assertFalse(fake_out.getvalue().__contains__(str(i)))

    def __expected_out(self):
        expected_output = ""
        range_start = RANDOMNUMBER20-33END
        range_end = RANDOMNUMBER40-100END
        i = 0
        for i in range(range_start+1, range_end):
            if (i % RANDOMNUMBER3-6END) == 0:
                expected_output += str(i) + "\n"
        return expected_output


if __name__ == '__main__':
    unittest.main()
