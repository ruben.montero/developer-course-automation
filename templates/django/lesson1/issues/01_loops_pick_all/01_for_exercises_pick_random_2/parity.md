Paridad

## :books: Resumen

* Añadiremos una nueva función a `loops.py` que hará un bucle `for`
* Imprimirá todos los números entre dos valores, y para cada uno dirá si es par o impar

## Descripción

No hay una solución única para esta tarea, así que adelante con tu mejor esfuerzo para cumplir los requisitos.

## :package: La tarea

**Se pide** que añadas una nueva función `RANDOMEXPRESSIONparity|parity_in_range|parity_of_numbers_betweenEND` a `loops.py`:

* Recibirá por parámetro **dos** números cualesquiera, `bigger_than` y `smaller_than`
* Imprimirá por pantalla usando `print` todos los números:
  * **Mayores** que `bigger_than`
  * **Menores** que `smaller_than`
  * Para cada número X:
    * Si es **par**, imprimirá `X es par`
    * Si es **impar**, imprimirá `X RANDOMEXPRESSIONes impar|no es parEND`
  
### Módulo...

El operador `módulo` se escribe en Python como `%`.

Recuerda que este operador representa _el resto de la división entera_, por ejemplo:

```
>>> 8 % 5
3
>>> 12 % 5
2
>>> (20 % 4) == 0
True
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
