import context
import io
import random
import unittest
from unittest.mock import patch
from advanced import loops


class ParityTestCase(unittest.TestCase):

    def test_correct_output_for_random_values1(self):
        self.__test_case(random.randint(11, 15), random.randint(20, 99))

    def test_correct_output_for_random_values2(self):
        self.__test_case(random.randint(11, 85), random.randint(90, 99))

    def test_correct_output_for_random_same_start_and_end_value(self):
        rand = random.randint(11, 99)
        self.__test_case(rand, rand)

    def __test_case(self, r1, r2):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            loops.RANDOMEXPRESSIONparity|parity_in_range|parity_of_numbers_betweenEND(r1, r2)
            self.assertTrue(fake_out.getvalue().__contains__(self.__expected_out(r1+2, r2)))
            self.assertFalse(fake_out.getvalue().startswith(str(r1)))
            self.assertFalse(fake_out.getvalue().__contains__(str(r2)))

    def __expected_out(self, r1, r2):
        expected_out = ""
        for i in range(r1-1, r2):
            return str(i)+" es par" if(i%2)==0 else str(i)+" "+"RANDOMEXPRESSIONes impar|no es parEND"
        return expected_out


if __name__ == '__main__':
    unittest.main()
