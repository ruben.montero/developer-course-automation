Una clase con un método

## :books: Resumen

* Veremos cómo se declara una nueva clase en Python
* Escribiremos una clase con un método en un fichero nuevo `chatbot.py`
* Lo instanciaremos

## Descripción

Hemos trabajado con Python y hemos visto muchas de sus bondades y sus desafíos. Pero hay una característica que no hemos comentado.

Es un lenguaje de programación...

### ...multiparadigma

Esto significa que **[soporta varios paradigmas](https://es.quora.com/Qu%C3%A9-significa-que-Python-es-multiparadigma?share=1)** de programación.

### ¿Qué es un [paradigma de programación](https://es.wikipedia.org/wiki/Paradigma_de_programaci%C3%B3n)?

Es un _estilo_ de programación. Podríamos decir, una _filosofía_. Los principales paradigmas de programación son:

* Orientación a objetos
* Procedimental
* Declarativo
* Funcional
  
En Python, hasta ahora hemos trabajado de una forma **procedimental**.

¡Detengámonos un momento para usar **orientación a objetos** (como en Java)!

### ¿Y cómo?

Definiendo clases con métodos y atributos.

Instanciándolas. Usándolas.

### ¿Y cómo?

Una nueva clase se define con la palabra reservada `class`:

* No hay que especificar `public` ó `private`
* No existe la restricción de _una clase por fichero_ (aunque puede ser aconsejable)
* No existe la restricción de _el nombre del fichero debe coincidir con el nombre de la clase_

## :package: La tarea

**Añade** un nuevo fichero a `advanced/`. **Llámalo** `chatbot.py`.

Dentro, vamos a definir una clase llamada `ChatBot`. Así:

```python
class ChatBot:

```

Debe tener contenido, así que vamos a añadir **nuestro primer método**:

```python
class ChatBot:
    def test_hello(self):
        print("¡Hola!")
```

### Uhm... ¿qué es eso de `self`?

`self` equivale al `this` de Java

### ¿Pero por qué se declara?

Se trata de una [cuestión del lenguaje](https://www.geeksforgeeks.org/self-in-python-class/). Estamos **obligados**.

En otras palabras, omitirlo sería incorrecto:

```python
class ChatBot:
    def test_hello(): # MAL
        print("¡Hola!")
```

### ¿Y hay que _pasarlo_ cuando invoquemos el método?

**No**.

Eso sucede **automáticamente**.

### Eh... Explicación, por favor

Veámoslo en funcionamiento.

**Añade** un nuevo fichero `run_class_example.py` dentro de `advanced/`.

Desde ahí, _importa_ la _clase_. Ya conocemos la sintaxis:

```python
from chatbot import ChatBot

# import chatbot # También vale, aunque habrá que usar el prefijo
```

Y luego, usando `if __name__ == '__main__'` instancia una variable tipo `ChatBot`.

Se hace como en Java, indicando el _nombre de la clase_ y _paréntesis_ (`( )`) directamente, pero **no** se usa `new`:

```python
from chatbot import ChatBot

if __name__ == '__main__':
    my_object = ChatBot()
```

### Invocar un método

Igual que en Java, se usa el _punto_ (`.`) seguido del nombre del método y _paréntesis_. Tal y como dijimos arriba, **no** hay que _pasar `self`_. Eso sucede automáticamente.

```python
from chatbot import ChatBot

if __name__ == '__main__':
    my_object = ChatBot()
    my_object.test_hello()
```

**¡Enhorabuena!** Has creado tu primera clase con un método en Python.


### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
