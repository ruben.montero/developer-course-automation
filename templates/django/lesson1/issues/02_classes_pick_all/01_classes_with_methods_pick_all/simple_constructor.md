Método constructor

## :books: Resumen

* Veremos el método especial `__init__`
* Comprenderemos cuándo es invocado

## Descripción

En Java hemos manejado el concepto de [**método constructor**](https://www.solvetic.com/tutoriales/article/345-python-constructores/).

Recordemos que se trata de un _método_ cuyo código se ejecuta **al crear una nueva instancia**.

### ¿Cómo podemos _implementar_ un método constructor en Python?

Como _cualquier_ otro método, pero **usando el nombre especial `__init__`** (dos _barras bajas_ (`_`) a cada lado). También debe declarar el parámetro `self`:

```python
class UnaClase:

    def __init__(self):
        # Este código se ejecuta al instanciar la clase
        pass
```

## :package: La tarea

**Se pide** que añadas un **método** constructor a `ChatBot`:

* Únicamente, hará un _print_ indicando `"RANDOMEXPRESSIONInstancia de ChatBot creada|ChatBot creado|ChatBot inicializado|ChatBot encendido|ChatBot activado|ChatBot listo para el combateEND"`

Si re-lanzas `run_class_example.py` sin cambiar nada en ese fichero, ¿qué ves en la consola?


### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
