Métodos con parámetros

## :books: Resumen

* Veremos cómo se declaran métodos con parámetros formales a mayores que `self`
* Escribiremos dos nuevos métodos en `chatbot.py`
* Los invocaremos desde `run_class_example.py`

## Descripción

Nuestro primer método _declara_ `self`:

```python
class ChatBot:
    def test_hello(self):
        print("¡Hola!")
```

...pero este _parámetro_ **no** se pasa al invocar el método:

```python
    my_object.test_hello()
```

Esto no quita que **sí** podemos declarar y pasar _otro_ parámetros.

## :package: La tarea

**Escribe** un método nuevo en `chatbot.py` y llámalo `test_one_param`. Esperará un _número_ y hará un _print_:

```python
class ChatBot:
    def test_hello(self):
        print("¡Hola!")
        
    def test_one_param(self, number):
        print("Tu número es: " + str(number))
```

Ahora, desde `run_class_example.py` vamos a _invocarlo_ con un valor de ejemplo:

```python
if __name__ == '__main__':
    my_object = ChatBot()
    my_object.test_hello()
    # El siguiente método recibe un parámetro
    my_object.test_one_param(RANDOMNUMBER42-80END)
```

**Ejecuta** `run_class_example.py`. Funciona, ¿verdad?

### ¿Y más de uno?

Por supuesto.

### Completemos la tarea

* **Añade** otro método a `ChatBot`
  * Se llamará `RANDOMEXPRESSIONtest_two_params|substract_numbers|numbers_differenceEND`
  * _Declarará_ `self` **y** otros dos parámetros: `number1` y `number2`
  * Hará un _print_ de `number2 - number1`
  
* **Invoca** ese método desde tu _instancia_ de `ChatBot` **en `run_class_example.py`**, pasando dos valores cualesquiera.


### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

