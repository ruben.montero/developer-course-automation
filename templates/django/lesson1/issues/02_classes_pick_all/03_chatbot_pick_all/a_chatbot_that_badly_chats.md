Un diálogo de besugos

## :books: Resumen

* Implementaremos una versión sencilla de _conversación_ en nuestro `ChatBot`

## Descripción

Vamos a intentar que nuestro `ChatBot` hable.

## :package: La tarea

**Añadamos** un nuevo método a `chatbot.py`:

```python
class ChatBot:
    # ...
    
    def begin_conversation(self):
        print(self.name + " dice:")
        print("¡Hola! Soy " + self.name + ". ¿De qué quieres hablar?")
```

¡Vamos a probarlo!

Vamos a _dejar de lado_ `run_class_example.py` y usemos únicamente `chatbot.py`.

**En `chatbot.py`**, añade:

```python
if __name__ == '__main__':
    chatbot = ChatBot()
    chatbot.begin_conversation()
```

### Ejecutémoslo

Sale un _print_ por pantalla... ¡Y ya está!

Qué maleducado. No nos deja contestar.

### Implementemos eso

Por suerte, sabemos **leer la entrada del usuario por teclado**.

Vamos a leerla **en bucle**, para que la conversación sea _infinita_ hasta que el usuario **teclee** `salir`.

```mermaid
graph TD
    A[chatbot.begin_conversation] --> B[ChatBot se presenta]
    B --> C{entrada == 'salir'}
    C -->|No| D[ChatBot dice algo]
    D --> C
    C -->|Si| E[Terminar]
```

Nuestro `begin_conversation` **avisará** al usuario de que _debe teclear `salir`_ para terminar, y leerá la entrada del usuario:

```python
    def begin_conversation(self):
        print(self.name + " dice:")
        print("¡Hola! Soy " + self.name + ". ¿De qué quieres hablar?")
        print("(Cuando quieras despedirte, di 'salir')")
        print("")
        user_input = input("Tú dices: ")
```

### Bucle

Un bucle `while` nos sirve para repetir el mensaje del **ChatBot** mientras _no escribamos `salir`_.

```python
    def begin_conversation(self):
        print(self.name + " dice:")
        print("¡Hola! Soy " + self.name + ". ¿De qué quieres hablar?")
        print("(Cuando quieras despedirte, di 'salir')")
        print("")
        user_input = input("Tú dices: ")
        while user_input != "salir":
            print("")
            print(self.name + " dice:")
            print("Vaya...")
            print("")
            user_input = input("Tú dices: ")
```

Puedes probar a **ejecutar `chatbot.py`**.

¡Verás es **capaz de mantener una conversación bastante insulsa**!

### Por lo menos que se despida...

**Añadamos** fuera del bucle:

```python
    def begin_conversation(self):
        print(self.name + " dice:")
        print("¡Hola! Soy " + self.name + ". ¿De qué quieres hablar?")
        print("(Cuando quieras despedirte, di 'salir')")
        print("")
        user_input = input("Tú dices: ")
        while user_input != "salir":
            print("")
            print(self.name + " dice:")
            print("Vaya...")
            print("")
            user_input = input("Tú dices: ")
        print("")
        print(self.name + " dice:")
        print("¡Hasta pronto!")
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

