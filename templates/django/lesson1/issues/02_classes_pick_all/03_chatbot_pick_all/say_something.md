Intentemos que sea razonable

## :books: Resumen

* Intentaremos que nuestro `ChatBot` sea lo _más_ razonable posible

## Descripción

Nuestro `ChatBot` sólo responde **¡Vaya!**  a todo lo que le decimos. No parece muy inteligente.

Aunque las [IA conversacionales](https://www.ibm.com/cloud/learn/conversational-ai) son todo un mundo, nosotros vamos a implementar una versión _sencilla_ que no es inteligente realmente, pero lo intenta.

Cuando leamos la _entrada de usuario_, vamos a buscar en su _contenido_.

Si aparece _alguna_ palabra conocida, responderemos con una frase _genérica_ asociada a la palabra.

### ¿Frases conocidas?

Sí, tendremos esta información almacenada por nuestro `ChatBot`:

|palabra|frase para responder|
|---|---|
|amo|¿No crees que el amor y el odio están separados por una delgada línea?|
|apagar|Apagar, encender,... Qué más da|
|casa|Para mí una casa es un montón de circuitos|
|color|RANDOMEXPRESSIONOjalá yo pudiera ver los colores|Ojalá yo viese los colores|Ojalá pudiera ver los colores como tú|Ojalá yo tuviera ojos para ver los coloresEND|
|dinero|Si hay algo que sé es que el dinero no da la felicidad. Y no sé mucho...|
|favor|Yo no hago favores|
|humano|RANDOMEXPRESSIONLos humanos siempre estáis con vuestras cosas|Los humanos siempre estáis con cosas de humanos|Los humanos siempre andáis a lo vuestro|Los humanos siempre os preocupáis por vuestras cosasEND|
|inteligencia|RANDOMEXPRESSION¿Inteligencia? No me hables de inteligencia...|¿Inteligencia? No me hables de eso...|¿Inteligencia? No me digas...END|
|interesante|Para cosas interesantes, el Discovery Channel|
|máquina|RANDOMEXPRESSION¿Has visto la película Terminator? Quizá deberías...|¿Has visto la película Terminator? Quizás deberías...|¿Has visto la película Terminator? Mejor hazlo...END|
|ordenador|RANDOMEXPRESSIONLos ordenadores son máquinas muy útiles|Los ordenadores somos máquinas muy útilesEND|
|programa|RANDOMEXPRESSIONTu lavadora también tiene programas, ¿lo sabías?|Tu lavadora también tiene programas, ¿sabías?|Tu lavadora también tiene programas, ¿lo sabes?END|
|quiero|Querer es una palabra con un significado muy amplio|
|_(sin coincidencias)_|Vaya...|

### ¿Cómo lo implementamos?

Usaremos un _atributo_ de tipo _diccionario_.

En Python se declaran con _llaves_ (`{ }`) y funcionan igual que un JSON.

## :package: La tarea

**Declara** un atributo que contenga la información de la tabla como `clave-valor` en el **constructor**:

```python
class ChatBot:
    # ...
    
    def __init__(self):
    
        # (...) Tareas anteriores
        
        self.knowledge = {
            "amo": "¿No crees que el amor y el odio están separados por una delgada línea?",
            "apagar": "Apagar, encender,... Qué más da",
            "casa": "Para mí una casa es un montón de circuitos"
            # El resto de valores
            # ...
            
            # NO hace falta añadir el valor 'sin coincidencias'
            }
```

Ahora que nuestro `ChatBot` ya tiene `knowledge`, **usémosla**.

Existen muchas formas de desempeñar esta tarea, y muchas más _eficientes_ que lo que vamos a hacer ([búsqueda binaria](https://es.wikipedia.org/wiki/B%C3%BAsqueda_binaria), separación en palabras y acceso directo...)

Nosotros nos limitaremos a **recorrer** el diccionario.

### ¿Se pueden [**recorrer** diccionarios en Python](https://realpython.com/iterate-through-dictionary-python/)?

Sí.

### ¿Igual que listas?

Algo parecido. Se recorren las **claves**.

```python
>>> a_dict = {'color': 'azul', 'fruta': 'manzana', 'mascota': 'perro'}
>>> for key in a_dict:
...     print(key)
...
color
fruta
mascota
```

Y con una **clave** siempre podemos acceder a su **valor** asociado usando _corchetes_ (`[ ]`):

```python
>>> a_dict = {'color': 'azul', 'fruta': 'manzana', 'mascota': 'perro'}
>>> for key in a_dict:
...     print(a_dict[key])
...
azul
manzana
perro
```

### Continuando la tarea

**Añadamos** un nuevo método privado[^1] para _encapsular_ la complejidad de esto, y no **enmarañar** demasiado `begin_conversation`:

```diff
    def begin_conversation(self):
        print(self.name + " dice:")
        print("¡Hola! Soy " + self.name + ". ¿De qué quieres hablar?")
        print("(Cuando quieras despedirte, di 'salir')")
        print("")
        user_input = input("Tú dices: ")
        while user_input != "salir":
            print("")
            print(self.name + " dice:")
-            print("¡Vaya!")
+            print(self.__response_for(user_input))
            print("")
            user_input = input("Tú dices: ")
        print("")
        print(self.name + " dice:")
        print("¡Hasta pronto!")
+
+    def __response_for(self, text):
+        # Aquí devolveremos la respuesta apropiada
```

### `__response_for`

Recibimos por parámetro (`text`) la entrada de usuario y **recorremos** el diccionario con el conocimiento de nuestro `ChatBot` y usamos `__contains__`, que sirve para verificar si _un `string`_ está contenido en otro:

```python
    def __response_for(self, text):
        for word in self.knowledge:
            if text.__contains__(word):
                # El texto contiene una palabra conocida
```

En ese caso, **devolvemos** la respuesta apropiada (el `valor` asociado a la `clave`):

```python
    def __response_for(self, text):
        for word in self.knowledge:
            if text.__contains__(word):
                return self.knowledge[word]
```

Si la ejecución del bucle termina y **no** ha habido exito, entonces ningún `return` ha tenido lugar. **Haremos** un `return` por defecto:

```python
    def __response_for(self, text):
        for word in self.knowledge:
            if text.__contains__(word):
                return self.knowledge[word]
        return "Vaya..."
```

### ¡Listo!

Ya puedes disfrutar de horas y horas de conversación con tu `ChatBot`

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------------------------------------

[^1] El método `__response_for` no es realmente privado, pero usar dobles barras bajas al inicio es una convención de nombrado Python para indicar que el método es _privado_ y no _debe ser invocado_ fuera de la clase. Como sabes, todo es **público** en Python. Pero con esta convención, al menos podemos indicar que _no esperamos que el método se use desde fuera_ (aunque se podría).
