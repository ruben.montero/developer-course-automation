Atributos

## :books: Resumen

* Veremos cómo se declaran los _atributos_ en una clase Python
* Añadiremos en nuestro constructor el atributo `self.name`

## Descripción

En Java solíamos especificar atributos de la clase **fuera** de los métodos:

```java
public class JavaClass {
    private String name;
    private int age;

}
```

En Python, si hacemos _algo parecido a eso_, lo estamos haciendo **mal**.

Debemos conocer que la **siguiente sintaxis existe**, pero **no** la usaremos:

```python
class PythonClass:
    name = "Something" # NO es lo que esperas
    age = 42
```

Sirve para definir variables _que [son comunes a todas las instancias de la clase](https://www.pythontutorial.net/python-oop/python-class-attributes/)_. **No** es lo que esperamos. Son más bien como **constantes** o **atributos _estáticos_** de Java.

### Entonces, ¿cómo se definen **atributos de instancia** en Python?

**Dentro** del método constructor.

### ¿Cómo?

Se declaran como _variables_ **de `self`**.

Y ya está. Una vez hecho esto, estarán disponibles para _tu instancia_ durante toda su vida.

### ¿Por ejemplo?

Si tenemos un clase `Circle`, podemos darle un atributo `radius` así:

```python
class Circle:

    def __init__(self):
        self.radius = 5
```

#### ¿El método constructor puede recibir parámetros?

Sí. Basta con declararlos.

```python
class Circle:

    # En Python NO puede haber más de un método constructor
    def __init__(self, radius):
        self.radius = radius
```

#### ¿Para qué sirven los atributos?

Como ya sabes, la utilidad de los atributos es almacenar información de una instancia.

Puede ser usada más tarde o servir a multitud de propósitos.

```python
class Circle:

    def __init__(self, radius):
        self.radius = radius
        
    def calculate_circumference(self):
        return 2 * 3.1416 * self.radius
```

## :package: La tarea

**Se pide** que añadas _un atributo_ al método constructor que ya existe en `ChatBot`:

* La firma del constructor _seguirá siendo_ `__init__(self)`, sin parámetros
* Además del _print_ que ya se hace, se **inicializará** el atributo `self.name` a `"RANDOMEXPRESSIONGaia|Cronos|Atlas|Hyperion|Hades|Atenea|Ares|Hefestos|ApoloEND"`

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

