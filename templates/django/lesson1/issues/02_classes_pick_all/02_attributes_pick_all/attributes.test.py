import context
import unittest
from advanced import chatbot


class ChatBotSimpleConstructorTestCase(unittest.TestCase):

    def test_constructor_name_exists(self):
        t = chatbot.ChatBot()
        self.assertEqual(t.name, "RANDOMEXPRESSIONGaia|Cronos|Atlas|Hyperion|Hades|Atenea|Ares|Hefestos|ApoloEND")


if __name__ == '__main__':
    unittest.main()

