Un descanso

## :books: Resumen

* Tomaremos un descanso

## Descripción

Hemos visto bastantes conceptos nuevos y trabajado con un lenguaje que seguramente era desconocido.

¡Hagamos una breve pausa! 4 ó 5 minutos pueden ser nuestros aliados para **no** saturarnos.

### Las rachas son malas

No pasa nada por parar unos minutos si te ves **abrumado**. Las cosas van poco a poco. Intentar hacerlo todo de golpe **te jugará una mala pasada**.

Esto es una carrera a largo plazo.

### :smiley:

* Una vez consideres que cuentas con la energía y determinación para proseguir con las tareas, ¡adelante!

### Por último

No hagas nada más.

El _test_ pasará automáticamente.
