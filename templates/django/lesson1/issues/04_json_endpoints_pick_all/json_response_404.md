Códigos de respuesta HTTP

## :books: Resumen

* Repasaremos los posibles códigos de respuesta HTTP
* Crearemos un _endpoint_ que devuelve un 404
* Veremos cómo inspeccionarlo con el navegador

## Descripción

Los servidores HTTP entregan respuestas con las siguientes partes:

* Línea de respuesta
* Cabeceras
* Cuerpo

Hasta ahora sólo nos hemos preocupado por el cuerpo, mandando un JSON ó HTML.

En la línea de respuesta viaja algo esencial, el **código HTTP de respuesta**.

Se dividen en:

* 1xx: Informacionales
* 2xx: De éxito
* 3xx: De redirección
* 4xx: Error de cliente
* 5xx: Error de servidor

### ¿Y por qué son esenciales?

HTTP es clave en el mundo de Internet porque agiliza los intercambios de información.

Por ejemplo, si una respuesta es `200 OK`, puede que un _intermediario_ la _cachee_ un tiempo. Esa **caché** servirá para que la respuesta se entregue **más rápido** en el futuro.

En cambio, si es un `500 Internal Server Error`, **no** se _cacheará_, pues _quizá_ el servidor funcione bien en cualquier momento.

En otras palabras, el correcto uso de HTTP mejora las comunicaciones de red y la escalabilidad de tu aplicación.

### Vale, ¿y cómo juego con códigos de respuesta en Django?

Basta con especificar el parámetro `status`[^1] en la `JsonResponse`.

## :package: La tarea

**Añade** esta nueva función a `endpoints.py`:

```python
def my_not_found_endpoint(request):
    json_dictionary = {
        "message": "RANDOMEXPRESSIONEso que buscas no parece estar por aquí|Eso que buscas no existe|No encontrado|Parece que eso que buscas no existe|Parece que eso que buscas no existe|Lo siento, eso que buscas no anda por aquíEND"
    }
    return JsonResponse(json_dictionary, status=404)
```

Y **_mapéala_** en `urls.py`:

```python
urlpatterns = [
    # ...
    path('api/v1/not_found_example', endpoints.my_not_found_endpoint),
]
```

¿Qué ves al visitar http://localhost:8000/api/v1/not_found_example en tu navegador?

### Un JSON

...y nada más.

El **código HTTP** es algo _interno_. Pero podemos inspeccionarlo.

Haz _click_ derecho en el navegador > Inspeccionar > Red.

Refresca la página con F5.

¿Dónde encuentras la indicación de `404`?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------

[^1]: En Java, los parámetros son **posicionales**. Es decir, al invocar la función los escribimos _en el mismo orden_ que están declarados. En Python, también. Pero **a mayores**, podemos indicar parámetros usando la `keyword` con la que están declarados y así desordenarlos. Por ejemplo, `JsonResponse([], status=404, safe=False)` es igual de válido que `JsonResponse([], safe=False, status=404)`
