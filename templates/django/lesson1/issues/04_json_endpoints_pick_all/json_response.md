¿Páginas web? ¡Yo he venido aquí a hablar de mi libro!

## :books: Resumen

* Escribiremos un fichero nuevo llamado `endpoints.py`, análogo a `views.py`
* Crearemos nuestro primer _endpoint_ JSON, para un API REST
* Usaremos `JsonResponse` en vez de `HttpResponse`

## Descripción

¿Por qué estamos creando **páginas web**? ¿Esto es desarrollo web?

### No

¡Estábamos creando un API REST con Django

Queremos practicar cómo devolver datos en formato JSON, **no** HTML.

## :package: La tarea

**Crea** un nuevo fichero `endpoints.py` en la carpeta `webserviceDEVELOPERNUMBERapp`. Vamos a añadir un nuevo _import_ y un método:

```python
from django.http import JsonResponse

def my_first_endpoint(request):
    # Aquí generamos la respuesta
```

Ahora vamos a usar `JsonResponse` en lugar de `HttpResponse`. ¡Ojo! Cuando construimos un `JsonResponse`, pasamos un diccionario Python (que es casi como un JSON) directamente, en lugar de un `string`.

Hazlo **así**:

```python
def my_first_endpoint(request):
    json_dictionary = {
        "RANDOMEXPRESSIONsuccess|has_been_successful|everything_ok|okEND": True,
        "message": "RANDOMEXPRESSIONHa salido bien|Ha ido bien|Todo bien|Todo correctoEND"
    }
    return JsonResponse(json_dictionary)
```

Luego, vamos a elegir la URL `/api/v1/example` para nuestro primer _endpoint_ JSON.

En `urls.py` **añade** lo necesario:

```python
from django.contrib import admin
from django.urls import path
from webserviceDEVELOPERNUMBERapp import views, endpoints # Hemos añadido otro import en la misma línea

urlpatterns = [
    path('admin/', admin.site.urls),
    path('example', views.my_first_view),
    path('emperors', views.my_second_view),
    path('api/v1/example', endpoints.my_first_endpoint),
]
```

_(Hemos elegido `v1` como prefijo de la ruta. ¿Por qué crees que es buena idea **versionar** los endpoints?)_

Puedes lanzar `python manage.py runserver` y visitar http://localhost:8000/api/v1/example desde tu navegador.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
