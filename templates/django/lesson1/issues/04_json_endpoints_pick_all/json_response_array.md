¿safe=False?

## :books: Resumen

* Crearemos un _endpoint_ que serializa un JSON lista (`[ ]`)
* Explicaremos que, al no ser un diccionario, debemos usar `safe=False` en `JsonResponse`

## Descripción

Los documentos JSON generalmente contienen _objetos_ o _diccionarios_, que se indican con _llaves_ (`{ }`):

```json
{
  "ejemplo": "Ser buena persona",
  "valor": 10
}
```

Y un _tipo de dato_ que se admite son listas de elementos encerrados en _corchetes_ (`[ ]`):

```json
{
  "ejemplo": "Ser buena persona",
  "valor": 10,
  "recordatorios": ["¡Trabaja!", "¡Defiéndete!", "¡Ayuda al prójimo!"]
}
```

```json
{
  "ejemplo": "Ser buena persona",
  "valor": 10,
  "recordatorios": [
    {
      "mensaje": "¡Trabaja!",
      "importante": true
    },
    { 
      "mensaje": "¡Defiéndete!",
      "importante": true 
    },
    { 
      "mensaje": "¡Ayuda al prójimo!",
      "importante": true 
    }
  ]
}
```

### Una lista (`[ ]`) como elemento raíz...

...**también** es un JSON válido. COmo el siguiente ejemplo:

```json
[
  {
    "mensaje": "¡Trabaja!",
    "importante": true
  },
  { 
    "mensaje": "¡Defiéndete!",
    "importante": true 
  },
  { 
    "mensaje": "¡Ayuda al prójimo!",
    "importante": true 
  }
]
```

## :package: La tarea

**Crea** una nueva función en `endpoints.py`:

```python
def my_list_endpoint(request):
    animals = [
        "RANDOMEXPRESSIONFoca monje (Monachus monachus)|Visón europeo (Mustela lutreola)|Focha común (Fulica atra)END",
        "RANDOMEXPRESSIONMalvasía cabeciblanca (Oxyura leucocephala)|Lagartija carpetana (Iberolacerta cyreni)|Fartet (Aphanius iberus)END",
        "RANDOMEXPRESSIONSamaruc (Valencia hispanica)|Avispón europeo (Vespa crabro)|Lince ibérico (Lynx pardinus)END"
    ]
    return JsonResponse(animals, safe=False)
```

Como ves, devuelve una lista con tres [animales autóctonos de España](https://www.ecologiaverde.com/animales-autoctonos-de-espana-3453.html).

Nos vemos forzados a pasar un _parámetro_ muy curioso a `JsonResponse`

### `safe=False`

Es necesario cuando enviamos como respuesta cualquier cosa que **no** sea un diccionario Python.

Por ejemplo, una **lista**.

Si te preguntas _por qué_, la [documentación](https://docs.djangoproject.com/en/4.0/ref/request-response/#jsonresponse-objects) lo aclara:

>>>
Warning

Before the 5th edition of ECMAScript it was possible to poison the JavaScript Array constructor. For this reason, Django does not allow passing non-dict objects to the JsonResponse constructor by default. However, most modern browsers implement ECMAScript 5 which removes this attack vector. Therefore it is possible to disable this security precaution.
>>>

### `urls.py`

**Mapea** nuestro nuevo _endpoint_ a la URL: `/api/v1/animals`

**¡Tarea completada!** Puedes verificarla en el navegador tras lanzar tu servidor REST.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
