Si he dicho a, b ó c... Es a, b ó c

## :books: Resumen

* Usaremos un `while` para verificar la entrada del usuario en `contest.py`

## Descripción

En la tarea anterior escribimos un fichero `contest.py` que pregunta al usuario cuál es la universidad más antigua de Europa.

Le da tres opciones, **a)**, **b)** ó **c)**, y lee la entrada con:

```python
    choice = input("Escoge tu respuesta (a, b ó c): ")
```

Si tecleas **d** u otra opción inválida, como **3** ó **pato**, saltará el mensaje de _fallo_ del `else`.

### También podemos engañar al usuario sin querer

¿Qué pasa si el usuario teclea **B** (en mayúsculas) ó **b)** (con el paréntesis `)`)? El código **saltará** al `else`, y, ¡puede ser **inesperado** para el usuario!

### Bucle `while`

Existe en prácticamente todos los lenguajes de programación y Python no es una excepción. Su sintaxis es:

```python
while condition:
  # Hacer cosas DENTRO del bucle

# A este nivel de indentación, ya hemos salido del bucle
print("El bucle ha terminado")
```

Mientras `condition` sea `True`, el cuerpo del bucle se repetirá.

En el momento en que se evalúe `condition` y sea `False`, el bucle **no** se repetirá.

### ¿`True`? ¿No es `true`?

En Python, los valores booleanos básicos se expresan con letra **mayúscula**: `True`, `False`. A diferencia de Java: `true`, `false`.

O sea, si escribes `true`, estarás refiriéndote a una _variable_ de nombre `true`.

### A lo que íbamos

Es muy sencillo.

Podemos verificar que la entrada es `a`, `b` ó `c`:

```python
    choice = input("Escoge tu respuesta (a, b ó c): ")
    while choice != 'a' and choice != 'b' and choice != 'c':
        choice = input("RANDOMEXPRESSIONEscoge una respuesta válida, por favor (a, b ó c)|Por favor, escoge una respuesta válida (a, b ó c)|¿Serías tan amable de escoger una respuesta válida (a, b ó c)?|Disculpa, escoge una respuesta válida (a, b ó c)|¡Vaya! Escoge una respuesta válida (a, b ó c)|¡Vaya! Escoge una opción válida (a, b ó c)END: ")
```

#### Alternativas

Si recuerdas las [leyes de Morgan](https://es.wikipedia.org/wiki/Leyes_de_De_Morgan), sabrás que:

```python 
choice != 'a' and choice != 'b' and choice != 'c'
```

...es equivalente a:

```python
not (choice == 'a' or choice == 'b' or choice == 'c') 
```

También, para ir _introduciendo_ conceptos avanzados de _listas_, podemos exponer estas otras alternativas:

```python
    while not (choice in ['a', 'b', 'c']):
    while choice not in ['a', 'b', 'c']:
```

## :package: La tarea

Deja escrito en tu `contest.py` la validación de entrada mediante `while` como se expuso más arriba.

Usa tu opción preferida como condición de salida.

Lanza `contest.py` y pruébalo. ¿Valida la entrada como esperas?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

Haz `commit` y `push` para subir los cambios al repositorio.
