Cálculo numérico

## :books: Resumen

* Añadiremos otra pregunta a `contest.py`
* En lugar de ofrecer 3 opciones, pedirá un _número_
* Lo convertiremos a `int` y controlaremos `ValueError` para validarlo

## Descripción

Nuestro `contest.py` es entretenido.

¡Aprendamos a hacer más cosas! Esta vez, en lugar de ofrecer tres opciones, _estrujará_ un poco más las _neuronas_ del concursante.

**Añade** una nueva función `make_question2` e _invócala_. Comenzará siendo así:

## :package: La tarea

```python
# ...

def make_question2():
    question2 = """
RANDOMEXPRESSION¿Cuál es el resultado de SUMAR estos dos números?|¿Cuál es el resultado de SUMAR estos números?END
RANDOMNUMBER350-700END
RANDOMNUMBER351-700END
"""
    print(question2)
    answer = input("Dime tu respuesta: ")


if __name__ == '__main__':
    make_question()
    make_question2()
```

### Convirtiendo a `int`

La entrada por teclado es un `string` siempre.

Queremos interpretar la respuesta como un _número_, así que usaremos el ya conocido `int(...)`:

```python
    # ...
    print(question2)
    answer = int(input("Dime tu respuesta: "))
```

### Capturando el error

El equivalente al `try-catch` de Java es [`try-except` de Python](https://pythonbasics.org/try-except/).

Es ligeramente distinto:

```python
    # ...
    print(question2)
    try:
        answer = int(input("Dime tu respuesta: "))
    except ValueError:
        print("¡No has introducido un número válido!")
```

En **esta ocasión**, _capturamos_ un `ValueError`, pero **hay muchos más tipos** de **excepciones**.

### Si he dicho un número; es un número

Usemos un bucle `while` que fuerce a que la entrada del usuario sea numérica:

```python
    # ...
    print(question2)
    answer = None
    while answer is None: # Se prefiere 'is' a '==' para comparar con None
        try:
            answer = int(input("Dime tu respuesta: "))
        except ValueError:
            print("¡No has introducido un número válido!")
```

¡Bien! Ya sabemos **validar una entrada numérica**

Para terminar, si el usuario **acierta** con su cálculo, se imprimirá el mensaje `"RANDOMEXPRESSIONRespuesta correcta|¡Respuesta correcta!|Has acertado, ¡enhorabuena!|¡Una mente privilegiada para el cálculo!|¡Una mente privilegiada para la aritmética!|¡Una mente privilegiada para las matemáticas!END"`.

Si **falla**, se imprimirá: `"RANDOMEXPRESSIONLo sentimos, respuesta incorrecta|Parece que esa respuesta no es correcta, ¡no te rindas!|Vaya, la respuesta es erróneaEND"`

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
