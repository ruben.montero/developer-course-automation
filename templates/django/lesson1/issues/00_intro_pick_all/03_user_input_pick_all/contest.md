Sabemos programar... ¿Pero sabemos preguntar?

## :books: Resumen

* Escribiremos un nuevo fichero `contest.py`
* Imprimirá una pregunta con 3 opciones
* Leerá la entrada del usuario usando `input`

## Descripción

Vamos a aprender a leer la entrada de usuario _por consola_.

## :package: La tarea

**Añadamos** un nuevo fichero `contest.py` a `intro/`:

```python
if __name__ == '__main__':
    question = """
¿Cuál es la universidad más antigua de Europa, fundada en 1088, que sigue en funcionamiento actualmente?

a) Universidad de París
b) Universidad de Bolonia
c) Universidad de Oxford
    """
    print(question)
```

Como ves, usamos un elegante `string` multilínea delimitado por triples comillas.

**No** hay que _indentar_ el `string`. Eso provocaría que en su _contenido_ hubiera _espacios en blanco_ indeseados.

### `input()`

¡Es muy fácil de usar! Basta con _invocarlo_ y asignar su resultado a una variable:

```python
# Ejemplo
user_input = input()
```

Opcionalmente, podemos pasarle un `string`. Lo imprimirá por pantalla y servirá de información al usuario:

```python
# Ejemplo
user_intput = input("Dime el número de piezas de fruta que comes al día")
```

Ahora, **añádelo** a `contest.py`:

```python
if __name__ == '__main__':
    question = """
¿Cuál es la universidad más antigua de Europa, fundada en 1088, que sigue en funcionamiento actualmente?

a) Universidad de París
b) Universidad de Bolonia
c) Universidad de Oxford    
    """
    print(question)
    choice = input("Escoge tu respuesta (a, b ó c): ")
```

### ¿Y si acierto o fallo?

Mediante un `if`, mostraremos dos mensajes distintos al usuario:

```python
if __name__ == '__main__':
    question = """
¿Cuál es la universidad más antigua de Europa, fundada en 1088, que sigue en funcionamiento actualmente?

a) Universidad de París
b) Universidad de Bolonia
c) Universidad de Oxford    
"""
    print(question)
    choice = input("Escoge tu respuesta (a, b ó c): ")
    if choice == 'b':
        print("¡ENHORABUENA! ¡HAS ACERTADO! Ganaste RANDOMNUMBER100-200END puntos")
    else:
        print("¡Fallaste! Pero no pasa nada, ¡sigue intentándolo!")
```

Al **ejecutar** `contest.py` verás el _print_ y podrás responder.

¡Cuidado! Si usas PyCharm u otro IDE es posible que _el cursor_ no esté _posicionado_ en la consola. Es decir, al teclear `a`, `b` ó `c` editarás el fichero de código actual. Haz **_click_** en la **consola inferior** para asegurarte de que pasas _la pulsación de tu teclado_ al sitio correcto.

Si usas un terminal (cmd.exe) para ejecutar el código, no hay margen de error.

## Un pequeño _refactor_

Para mejorar este código, evitaremos usar _un `main` todopoderoso_. Vamos a encapsular nuestra pregunta en una función `make_question`.

Es decir, **cortar** (CTRL+X) y **pegar** (CTRL+V) nuestro código en una nueva función definida _arriba_.

Luego, lo _invocamos_.

De forma resumida, nuestro `contest.py` **quedará**:

```python
def make_question():
    question = """
¿Cuál es la universidad más antigua de Europa, fundada en 1088, que sigue en funcionamiento actualmente?

a) Universidad de París
b) Universidad de Bolonia
c) Universidad de Oxford    
"""
    print(question)
    choice = input("Escoge tu respuesta (a, b ó c): ")
    if choice == 'b':
        print("¡ENHORABUENA! ¡HAS ACERTADO! Ganaste RANDOMNUMBER100-200END puntos")
    else:
        print("¡Fallaste! Pero no pasa nada, ¡sigue intentándolo!")

if __name__ == '__main__':
    make_question()
```

**¡Enhorabuena!** Has escrito tu primer programa en Python que lee la entrada del usuario por consola. ¿Acertaste también la pregunta?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
