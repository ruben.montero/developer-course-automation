Tipos dinámicos

## :books: Resumen

* Añadiremos un nuevo fichero de código fuente `example.py` en `intro/`
* Invocaremos la función de `maths.py` con valores... Inesperados

## Descripción

Al igual que en Java, en Python el operador `+` sirve para:

* Sumar valores numéricos
* Concatenar cadenas de caracteres

En la tarea anterior, añadimos `maths.py` con una **función** que se encargaba de **sumar dos valores** y **devolver** el resultado.

Pongamos al límite el código :wink:

## :package: La tarea

**Se pide** que **añadas** un nuevo fichero `example.py`.

* Importa la **función** de `maths.py` creada en la tarea anterior
* Luego, dentro de `if __name__ == '__main__':`
  * Imprime por pantalla el **resultado** de invocar esta **función** pasando dos parámetros:
    * `"RANDOMEXPRESSIONTurbo|Mega|Nitro|Súper|Hiper|UltraEND"`
    * `"RANDOMEXPRESSIONTrueno|Onda|Rayo|LlamaradaEND"`
    
Si ejecutas `example.py`, ¿_qué_ ves por pantalla?

¿Comprendes algo mejor eso de _tipado dinámico_ y _qué implicaciones tiene_[^1]?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------------------------------------

[^1]: Por ejemplo, en Java, ¿tendrías permitido invocar la **función** pasando cadenas de caracteres en vez de números?
