Rompiendo cosas

## :books: Resumen

* Añadiremos un nuevo fichero de código fuente `example2.py` en `intro/`
* Intentaremos usar la función de `maths.py` para sumar un `str` y un `int`
* Comprenderemos que eso está mal

## Descripción

En la tarea anterior hemos dicho que:

>>>
 el operador `+` sirve para:

* Sumar valores numéricos
* Concatenar cadenas de caracteres
>>>

Pero, ¿qué pasa si **un operando es un _string_ y el otro es un _número_**?

## :package: La tarea

Pruébalo.

**Añade** un nuevo fichero `example2.py`.

* Importa la **función** de `maths.py` creada anteriormente
* Luego, dentro de `if __name__ == '__main__':`
  * Imprime por pantalla el **resultado** de invocar esta **función** pasando dos parámetros:
    * `"Greymon"`
    * `256`

**¿Qué pasa?**

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.


