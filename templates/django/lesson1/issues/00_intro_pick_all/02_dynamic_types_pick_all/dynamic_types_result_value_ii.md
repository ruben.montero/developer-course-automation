Conversión de cualquier cosa a número

## :books: Resumen

* Añadiremos una nueva función a `example3.py` que calcula el coste anual de un seguro dental
* Nos enfrentaremos a problemas _multiplicando_ valores `str` con `*`
* Usaremos la utilidad propia de Python `int(...)` para solventarlos

## Descripción

De la tarea anterior, tenemos una función que devuelve el precio mensual de un seguro en **tipo** `str` ó `int`, según cuadre:

```python
def dental_insurance_cost(company_name):
    if company_name == "Ratoncito Pérez S.L.":
        return "40"
    else:
        return 30
```

Completemos un poco más nuestro fichero `example3.py`.

## :package: La tarea

**Añade** _justo debajo_ de `dental_insurance_cost`, en el mismo nivel (primer nivel) de _indentación_, este código. Como ves, es una función que calcula el coste anual, invocando `dental_insurance_cost` y multiplicando por `12`.

```python
def show_annual_cost(a_company):
    cost = dental_insurance_cost(a_company)
    annual_cost = cost * 12
    print("Si contratas la compañía " + a_company + ", el coste anual será " + str(annual_cost) + " euros")
```

**Invócalo** desde el cuerpo principal:

```python
if __name__ == '__main__':
    # Código de tarea anterior
    # ...
    
    # Nueva tarea - ISSUENUMBER
    show_annual_cost("Dientes Baratos S.L.")
```

Si lo ejecutas, verás un excelente resultado:

```
...
Si contratas la compañía Dientes Baratos S.L., el coste anual será 360 euros
```

### Probemos de nuevo...

Usemos ahora el **nombre** de otra compañía, como `Ratoncito Pérez S.L.`:

```python
if __name__ == '__main__':
    # Código de tarea anterior
    # ...
    
    # Nueva tarea - ISSUENUMBER
    show_annual_cost("Dientes Baratos S.L.")
    show_annual_cost("Ratoncito Pérez S.L.")
```

**¡Ups!**

¿Qué ves?

No se trata de un error de ejecución... Pero tampoco es que esté _bien_.

### Parece demasiado caro, ¿no?

Sí.

¿Entiendes cómo el operador `*` afecta a `strings`?

### Pero yo quiero multiplicar un **número**

Entonces debemos asegurarnos de que el valor que se _multiplica por `12`_ es numérico.

Este es el **problema** de la tarea anterior, pero **a la inversa**. O sea, ahora queremos convertir un `string` a `int`.

### ¡Fácil!

En Python, podemos usar `int(cualquierCosa)` para convertir `cualquierCosa` a una variable que _es un número `int`_.

Si no es convertible, saltará un `ValueError`, aunque no nos preocuparemos por eso ahora.

Tan sólo **sustituiremos** la línea problemática en `show_annual_cost`:

```diff
-    annual_cost = cost * 12
+    annual_cost = int(cost) * 12
```

**¡Listo!**

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

