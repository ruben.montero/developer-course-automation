Te devuelvo una manzana o una pera, según me cuadre

## :books: Resumen

* Comprenderemos que el tipado dinámico hace el código más flexible también a la hora de devolver valores con `return`
* Añadiremos un nuevo fichero de código fuente `example3.py` en `intro/` con una función de ejemplo
* Nos enfrentaremos a problemas _concatenando_ valores `int` con `+`
* Usaremos la utilidad propia de Python `str(...)` para solventarlos

## Descripción

Hemos visto en la práctica que el **tipado dinámico** es responsable de que una función **admita 'cualquier' cosa** en sus parámetros de entrada.

* Por ejemplo, la función de `maths.py` que suma (`+`) dos valores, puede recibir `int` ó `str` indistintamente, produciendo así _resultados_ distintos (**sumar** vs **concatenar** vs **romper**).

Pues bien, el **tipado dinámico**... ¡También hace posible **devolver 'cualquier' cosa** con **`return`**!

## :package: La tarea

**Añadamos** un nuevo fichero `example3.py` que contenga una función llamada `dental_insurance_cost`.

* Recibirá: Un `string`[^1] con el nombre de una compañía de seguros dental
* Devolverá: El coste mensual que oferta dicha compañía

Es decir, dicha función _sabrá_ cuánto cuesta cada compañía.

**Añade** este código, suponiendo que la compañía ficticia [`Ratoncito Pérez S.L.`](https://blogs.20minutos.es/yaestaellistoquetodolosabe/el-origen-del-ratoncito-perez/) cuesta `40` euros al mes:

```python
def dental_insurance_cost(company_name):
    if company_name == "Ratoncito Pérez S.L.":
        return "40"
```

Si te fijas, devolvemos el valor `"40"` **como `string`**, fingiendo que nos hemos _equivocado_, por razones de aprendizaje:

Ahora vamos a _suponer que cualquier otra compañía_ tiene un precio de `30` euros al mes. **Añade** la rama `else`:

```python
def dental_insurance_cost(company_name):
    if company_name == "Ratoncito Pérez S.L.":
        return "40"
    else:
        return 30
```

¿Ves que podemos devolver dos **tipos de dato** distintos?

### Ahora, un `main`

Debajo de dicha función, **escribe** la sentencia `if` asociada al cuerpo principal del fichero.

Declaremos una variable nueva e _invoquemos_ la función recién creada, para recuperar el valor. Luego, hagamos un _print_ que enseña al usuario la información:

```python
if __name__ == '__main__':
    company = "Ratoncito Pérez S.L."
    cost = dental_insurance_cost(company)
    print("Tu seguro dental con la compañía " + company + " cuesta " + cost + " euros al mes")
```

Si **ejecutamos** `example3.py` deberíamos ver por consola algo como:

```
Tu seguro dental con la compañía Ratoncito Pérez S.L. cuesta 40 euros al mes

Process finished with exit code 0
```

**¡Bien!** Funciona como es esperado.

### Ahora, otra compañía

Vamos a **sustituir** el valor de `company` que acabamos de usar, por otro que _predeciblemente_ costará `30` euros al mes.

Así:

```python
if __name__ == '__main__':
    company = "RANDOMEXPRESSIONDentaExpert S.L.|DentaPlus S.L.|Dientes Sanos S.L.|Dientes Sanos y Robustos S.L.|No más endodoncias S.L.|No más caries S.L.END"
    cost = dental_insurance_cost(company)
    print("Tu seguro dental con la compañía " + company + " cuesta " + cost + " euros al mes")
```

### Si ejecutamos `example3.py`...

## ¿Qué pasa? :boom:

```
Traceback (most recent call last):
  File "C:\Users\Developer\...\python-introduction\intro\example3.py", line 10, in <module>
    print("Tu seguro dental con la compañía " + company + " cuesta " + cost + " euros al mes")
TypeError: can only concatenate str (not "int") to str
```

El **mismo código** que antes funcionaba, ahora **ya no**.

### Conclusión

## **MUST**

* Se **debe** verificar y conocer **en profundidad** el código que escribimos
* Cada pocas líneas, debemos **ejecutarlo** y **probarlo**
* Debido al tipado dinámico, los **errores inesperados** surgen con **frecuencia**. Escribir mucho **código sin probarlo** es sinónimo de **catástrofe**

### Ahora, arreglemos esto

_Podríamos_ simplemente modificar el valor de retorno de `dental_inssurance_cost`:

```diff
-    return 30
+    return "30"
```

Pero vamos a elegir **otra opción**

### `str()`

En Python, podemos usar `str(cualquierCosa)` para convertir `cualquierCosa` a una variable que _es un `string`_.

En el cuerpo **principal** de `example3.py`, podemos sustituir:

```diff
-   print("Tu seguro dental con la compañía " + company + " cuesta " + cost + " euros al mes")
+   print("Tu seguro dental con la compañía " + company + " cuesta " + str(cost) + " euros al mes")
```

**¡Listo!**

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------------------------------------

[^1]: Bueno, en realidad, _esperaremos_ que sea un `string`


