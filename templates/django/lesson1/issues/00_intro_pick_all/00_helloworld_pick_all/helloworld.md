Introducción a Python

## :books: Resumen

* Hablaremos del lenguaje de programación Python
* Instalaremos Python
* Escribiremos un sencillo fichero de código
* _(Y como siempre, subiremos los cambios al repositorio)_

## Descripción

El lenguaje de programación [Python](https://www.python.org/) es un lenguaje interpretado y dinámicamente tipado.

Un lenguaje interpretado, en contraposición a uno compilado, es aquel cuyas instrucciones se van ejecutando en la máquina paso a paso. Es decir, existe un intérprete que va leyendo las instrucciones del programa, traduciéndolas a _código máquina_ bajo demanda y ejecutándolas al momento.

Por otro lado, un lenguaje compilado atraviesa un proceso de _compilación_ antes de ser ejecutado. Es decir, el programa entero se traduce a _código máquina_ antes de ejecutarse.

### Tipado dinámico

Una variable es un hueco en la memoria RAM de nuestros ordenadores que puede albergar cualquier valor. Dependiendo del tipo de dato que queremos albergar, hará falta más o menos espacio.

<img src="python-introduction/docs/dynamically_typed.png" width="400">

Si este programa fue escrito en un lenguaje de programación _fuertemente tipado_, como [Java](https://www.java.com/es/), entonces el programador habrá tenido que teclear a mano el tipo de las variables en el momento de declararlas en el código:

```java
short edad;
float temperatura_corporal;
```

En contraposición, si este programa fue escrito en un lenguaje _dinámicamente tipado_ como Python, el programador no se preocupó necesariamente de _qué_ tipo serían las variables. Trabajó con ellas directamente y el intérprete les asignó el espacio necesario en memoria RAM.

Las variables, por lo tanto, no son _declaradas_. Se asignan directamente:

```python
edad = 42
temperatura_corporal = 37.555
```

Además, una variable puede _cambiar de tipo_ durante la ejecución:

```python
edad = 42
edad = "42 años"
```

### Instalando Python

Existen dos versiones de Python: `Python 2` y `Python 3`. Aunque lo parece, una **no** _sustituye_ a la otra. Debes considerarlas como dos lenguajes separados, con muchas cosas en común que a veces facilitan que el código sea intercambiable, pero **no necesariamente**.

Nosotros instalaremos Python 3 desde la [página de descargas](https://www.python.org/downloads/). Se aconseja descargar e instalar [Python 3.12.5](https://www.python.org/ftp/python/3.12.5/python-3.12.5-amd64.exe), el más reciente en el momento de definir estas tareas.

Basta con:

* **Ejecutar** el `.exe` descargado
* **Marcar** la opción _Add Python 3.12 to PATH_.
  * Esto facilita que al lanzar un terminal (cmd.exe), el comando `python` funcione directamente. Si te olvidas, puedes [añadirlo al PATH _a posteriori_](https://www.kyocode.com/2019/10/agregar-python-path-windows/)
* **_Install Now_**
* Cuando termine, **_Close_**

**¡Listo!**

Debes poder abrir un terminal (Símbolo del sistema) con **Tecla de Windows > cmd** y escribir lo siguiente:

```
python --version
```

Producirá una salida como esta:

```
Python 3.12.5
```

## :package: La tarea

En primer lugar, usa un terminal (cmd.exe) en tu ordenador y cambia el directorio activo hasta la ubicación de tu repositorio usando `cd`. Luego, haz:

```
git pull
```

...para traer los cambios de remoto a local. Contienen correcciones del proyecto anterior y **el esqueleto** del **nuevo proyecto**.

Se encontrará en **una carpeta llamada `python-introduction`**.

### Usa tu [IDE](https://www.redhat.com/es/topics/middleware/what-is-ide) favorito

Para trabajar con Python existen muchas alternativas. Puedes usar [IntelliJ IDEA](https://www.jetbrains.com/idea/) con un _plugin para Python_, [VisualStudio Code](https://code.visualstudio.com/), [Geany](https://www.geany.org/), [Sublime Text](https://www.sublimetext.com/), o, si no tienes miedo a nada, opciones más rudimentarias como [Vi](https://www.javatpoint.com/vi-editor) o [Bloc de notas](https://www.solvetic.com/tutoriales/article/6254-como-abrir-bloc-de-notas-windows-10-8-7/).

Con tal de que te resulte cómodo y ágil a la hora de escribir código y lanzar los _tests_, es suficiente.

### PyCharm

Nosotros veremos cómo trabajar con [PyCharm Community Edition (CE)](https://www.jetbrains.com/pycharm/download/#section=windows). Puedes descargarlo desde [aquí](https://www.jetbrains.com/pycharm/download/download-thanks.html?platform=windows&code=PCC), abrir el **.exe** e instalarlo con las opciones por defecto.

Una vez instalado, puedes abrirlo buscándolo en el Menú Inicio. _Abriremos_ nuestro proyecto desde el _popup_ inicial, o bien, desde **File > Open**. Buscamos y seleccionamos la carpeta `python-introduction` de nuestro repositorio local.

* Si te dice que el proyecto es de un autor desconocido, puedes darle a **Trust Project** sin problema
* Si indica que el certificado de `raspi` es inválido, puedes confiar sin problemas tras corroborar que la firma SHA-256 es la correcta
* Si indica que existe un `requirements.txt`, puedes crear un entorno virtual con las dependencias.
  * Nosotros iremos viendo cómo configurar e instalar tu entorno en caso de que omitas este paso.

### Hola, mundo

Abrimos el fichero `helloworld.py` cuyo contenido es:

```py
# TO-DO: Write a hello world!
```

...y escribimos:

```py
message = 'Hello, world!'
print(message)
```

### ¿Cómo ejecuto mi `helloworld.py`?

Vamos a ver dos formas:

**1)** Desde un terminal (cmd.exe), nos posicionaremos en la carpeta `python-introduction/intro/`. Desde ahí, podemos usar `python` igual que antes; **esta vez**, pasando **el nombre del fichero** como **argumento** (separado con espacio). Así:

```
python helloworld.py
```

Ejecutará el programa y producirá la salida esperada.

**2)** Configurando nuestro IDE para que permita ejecutar los ficheros con uno o dos _clicks_. Por ejemplo, en PyCharm CE podemos hacer _click_ abajo a la derecha en _**Add Interpreter**_
y eso nos permitirá decidir _qué_ **intérprete** debe usar el IDE. Cualquier opción nos vale, prefiriendo usar System (intérprete del sistema) o un entorno virtual (venv). 

También podemos llegar a este menú desde **File > Settings**, y en el panel lateral de la ventana, **Project > Python Interpreter**.

Si lo conseguimos configurar, podremos hacer _click_ derecho > **Run 'helloworld.py'** y veremos la salida por pantalla.

### :play_pause: Lanza el _test_

Prueba el _test_ asociado a la tarea. Lo ejecutaremos siguiendo los pasos de arriba, y esta vez lanzando `TESTFILENAME.py`.

Si ves algo como...

```
Ran 1 test in 0.003s

OK
```

¡Tarea terminada!

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

No está de más visitar la página de GitLab y verificar que el _commit_ se ha subido

