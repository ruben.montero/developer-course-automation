Un print muuuuuuuuuuuuuy largo

## :books: Resumen

* Veremos cómo definir cadenas de texto multilínea usando las triples comillas (`"""`)
* Implementaremos una función que hace un _print_ muy largo en `functions.py`

## Descripción

Los `string` pueden declararse encerrados en dobles comillas (`""`):

```python
una_cadena = "Esto es un texto"
```

...o comillas simples (`''`):

```python
otra_cadena = 'Esto también es un texto'
```

Esto es útil si queremos que nuestros `string` contengan comillas simples o dobles, porque los encerramos en las comillas _del otro tipo_.

Por ejemplo:

```python
frase_mafioso1 = "Cuando te dije que te 'encargaras' de él, no me refería a que lo mataras"
```

Y:

```python
frase_mafioso2 = 'Entonces, ¿a qué te referías con "encargarme" de él?'
```

### ¿No hace falta _escapar_ caracteres con un _slash_ invertido (`\`)?

Puedes hacerlo; `\n` ó `\"` son válidos en un `string` Python.

Pero es raro que se usen.

Incluso los saltos de línea pueden generarse de forma más clara y legible empleando un...

### `string` multilínea

Va encerrado en triples comillas, simples (`'''`) o dobles (`"""`).

Por ejemplo:

```python 
    print("""
    Con diez cañones por banda
    Viento en popa a toda vela
    No corta el mar sino vuela
    Un velero bergantín...
    
    ¿Qué poeta escribió estos versos?
    """)
```

## :package: La tarea

**Se pide** que añadas una nueva función en `functions.py`:

* Se llamará `RANDOMEXPRESSIONlong_print|big_printEND`
* No recibirá parámetros
* Únicamente, hará un `print` de un `string` multilínea con varias líneas. Se pide que en dicho `print` escribas un **chiste** que contenga la palabra **RANDOMEXPRESSIONcoche|calle|ordenador|programador|desarrollador|estudiar|examenEND**
  * El contenido del chiste es _libre_. Basta con que _tenga_ esa palabra.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

