Importar tiene consecuencias

## :books: Resumen

* Comprenderemos que un `import` _ejecuta_ el fichero importado
* Veremos la sentencia `if __name__ == "__main__"` para encapsular código que no queremos que se ejecute cuando se importa un fichero
* Trabajaremos en `greeter.py` y `other_program.py`

## Descripción

Podemos escribir código Python mucho más libremente que en Java. Cualquier fichero puede ser el _punto de entrada_ de una aplicación.

Por ejemplo, en nuestro `helloworld.py`:

```python
message = 'Hello, world!'
print(message)
```

_Podríamos_ definir una función y usarla, dentro del **mismo** fichero:

```python
def how_are_you()
    return "How are you?"

message = 'Hello, world!'
print(message)

other_message = how_are_you()
print(other_message)
```

Pero, ¡ojo! Escribir código esparcido y desordenado en varios ficheros, provocará _problemas_. Sobre todo, porque...

### ...la sentencia `import` ejecuta un fichero

En Java, la _información_ de clases y métodos se encuentra **compilada**. Al hacer `import`, _traemos_ dicha _información_, sin _ejecutarla_.

Pero, recordemos que Python es **interpretado**. Por eso, para _importar_ un fichero, Python lo **interpreta**. Es decir, lo **lee línea a línea**. Y para el intérprete, **leer** es **ejecutar**.

**¡Ojo!** Se ejecuta lo principal (lo que hay en el primer nivel de indentación). Pero tampoco creas que las funciones definidas se invocan _automáticamente_.

¡Veámoslo en la práctica!

## :package: La tarea

**Añade** un nuevo fichero `greeter.py` a `intro/`:

```python
def good_morning():
    return "Good morning! I hope you enjoy your day!"
    
message = good_morning()
print(message)
```

Puedes comprobar que al ejecutar este fichero desde el terminal (cmd.exe) con `python greeter.py`, o con **Run** en PyCharm, veremos la salida esperada:

```
"Good morning! I hope you enjoy your day!"
```

### Otro fichero

**Añadamos** un `other_program.py`:

```python
print("I wish I could say good morning...")
```

Y ahora, en `other_program.py` también, **añade** código para importa la función anterior:

```python
from greeter import good_morning

print("I wish I could say good morning...")
print("Wait, I can!")

my_variable = good_morning()
print(my_variable)
```

Probamos a **ejecutar** `other_program.py` y...

### Ups... ¿**Qué** pasa?

Verás que tu `other_program.py` no funciona como es debido.

La salida producida es:

```python
Good morning! I hope you enjoy your day!
I wish I could say good morning...
Wait, I can!
Good morning! I hope you enjoy your day!
```

¿**Por qué** sale el mensaje `Good morning! I hope you enjoy your day!` **dos** veces?

### Uhm... :hushed:

¡Como hemos dicho, cuando se _importa_ un fichero, se **lee** línea a línea!

### ¿Siempre pasa?

Sí.

### ¿Y no puedo _importar_ un fichero sin _ejecutarlo_?

Puedes **evitar** escribir código en el _primer nivel_ de _indentación_. Así, nada se ejecutará directamente si lo importas.

Y, adicionalmente, **si** tu fichero **debe** lanzar código cuando es _ejecutado_ (es decir, es el punto de entrada de tu aplicación), dicho código deberá ir **englobado** en un `if` como este:

```python
if __name__ == "__main__":
    # Aquí el código
```

### ¿Eso qué es?

Un `if` que contiene variables especiales. En **resumen**, es útil porque:

* El código dentro de `if __name__ == "__main__":`:
  * Se **ejecuta** si lanzamos el fichero Python directamente (desde el terminal o PyCharm)
  * **No** se **ejecuta** si el fichero es **importado**

### Arreglando nuestro `greeter.py`

**Corrige** `greeter.py` debería ser así:

```python
def good_morning():
    return "Good morning! I hope you enjoy your day!"

if __name__ == "__main__":
    print(good_morning())
```

Puedes verificar que:

* Si ejecutas `greeter.py`, se imprime:

```
Good morning! I hope you enjoy your day!
```

* Si ejecutas `other_program.py`, se imprime:

```
I wish I could say good morning...
Wait, I can!
Good morning! I hope you enjoy your day!
```

_(Ya no hay un `print` duplicado)_

### Conclusión

A partir de ahora, **no** escribiremos código **directamente** en el primer nivel de los archivos `.py`.

Si nuestro fichero Python, aparte de definiciones de funciones o clases, **contiene código que queremos lanzar**, debemos escribirlo **dentro de `if __name__ == "__main__":`**

_(Nota: No cambies `helloworld.py`)_

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

