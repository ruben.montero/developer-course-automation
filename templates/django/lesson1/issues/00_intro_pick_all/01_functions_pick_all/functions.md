Funciones que funcionan

## :books: Resumen

* Veremos las diferencias entre Java y Python a la hora de definir funciones
* Hablaremos de la _indentación_ en Python
* Añadiremos un nuevo fichero de código fuente `.py` en `intro/`
* Veremos qué es `pass`
* Definiremos tres funciones

## Descripción

Ya somos familiares con los conceptos de [procedimiento y función](https://diferenciario.com/procedimiento-y-funcion/). Recordemos que en Java se _declaran_, por ejemplo, así:

```java
public void doSomething() { /* ... */ }

public String getMyMessage(int someValue, float otherValue) { /* ... */ }
```

¿Qué diferencias hay con Python a la hora de declarar procedimientos ó funciones? _(Por brevedad, diremos sólo 'funciones' a partir de ahora)_

1. No hace falta que estén en una clase. Puedes declarar funciones en cualquier sitio. Incluso dentro de funciones
2. No hay que especificar visibilidad. Ya nos podemos olvidar de `public` y `private`. En Python, todo es público
3. Se usa la palabra `def`, y _no_ hay que especificar el _tipo_ de dato devuelto
4. Igual que en Java, se escribe el _nombre_ de la función y paréntesis

Por ejemplo:

```python
def my_function()

def other_function()
```

_(Nótese que usamos `snake_case` en vez de `lowerCamlCase`)_

### ¿Y los **parámetros**?


En Java, los declarábamos separados por comas (`,`), indicando su **tipo** como prefijo:

```java
public void myFunction(String param1, int param2)
```

En Python, los declaramos separados por comas (`,`) si hay más de uno, pero **no** indicamos su tipo:

```python
def my_function(param1, param2)
```

Es un lenguaje dinámicamente tipado, así que si algo está mal... ¡Ya romperá en tiempo de ejecución!

### Se usan dos puntos (`:`), **no** llaves (`{ }`)

Basta con indicar con dos puntos para finalizar la declaración de una función:

```python
def my_function(param1, param2):
```

Como no hay una llave (`}`) que _cierre_ la función, se usa la [indentación](https://lenguajejs.com/javascript/introduccion/indentacion/) ó sangría, que consiste en _tabular_ (añadir espacio) a ciertas líneas para aumentar la legibilidad.

En Java, estamos acostumbrados a _indentar_ el contenido de una función, clase, bucle...

```java
public void doWhatever() {
  System.out.println("whatever");
}
```

En Python, estamos **obligados**:

```python
def my_function(param1, param2):
    print(param1)
    print(param2)
    print("Esta es la última línea de la función")
print("¡Ojo! Este print ya no pertenece a la función")
```

Así es como definimos dónde **empiezan** y **terminan**.

### La práctica hace al maestro

La _indentación_ en Python comienza siendo algo extraña y anti-intuitiva. Hace falta practicar para perderle el miedo.

## :package: La tarea

**Añadamos** un nuevo fichero de código `functions.py` a la carpeta `intro/`. Puedes hacerlo con _click_ derecho > New > Python File.

Escribiremos una función sencilla que **no haga nada**:

```python
def do_nothing():
```

¡Un momento! Eso **falla**.

PyCharm lo subraya en rojo, y si pasamos el ratón por encima indica `Indent expected`.

### `pass`

Precisamente, como Python se basa en _indentaciones_ para comprender _qué_ es parte de una función... ¡Hay problemas si la función está vacía!

Para este caso particular, existe la palabra reservada `pass`:

```python
def do_nothing():
    pass
```

¡Primera función completada!

### Una segunda función

Probemos ahora a hacer algo. Como sólo sabemos hacer un `print`... **Añade** esta función también:

```python
def do_something():
    print("RANDOMEXPRESSIONI'm doing something|I am doing something|Something is getting doneEND")
```

### ¿Y devolver un valor?

Antes hemos comentado que **no** se _declara_ el tipo de de dato devuelto.

Pero **sí** podemos **devolver** un valor. Se usa `return` acompañado de la variable que queramos. **Añade** esta función:

```python
def return_something():
    a_number = RANDOMNUMBER20-100END
    return a_number
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

