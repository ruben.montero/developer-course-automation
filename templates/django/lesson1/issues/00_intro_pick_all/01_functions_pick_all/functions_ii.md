Un par de ejemplos más

## :books: Resumen

* Implementaremos dos nuevas funciones sencillas en `functions.py`

## :package: La tarea

**Se pide** que añadas una nueva función en `functions.py`:

* Se llamará `RANDOMEXPRESSIONbrief_print|short_print|small_printEND`
* No recibirá parámetros
* Únicamente, hará un `print` con el mensaje **RANDOMEXPRESSIONEn 1453 se inventó la imprenta|En 1789 se produjo la revolución francesa|James Watt patentó en 1769 la máquina de vapor|El imperio romano creció tanto que se fragmento en dos partes principales en torno al mar Mediterráneo y en el año 476, cayó el imperio romano de occidente|La primera guerra mundial tuvo lugar de 1914 a 1918END**

También, **se pide** **otra** función en `functions.py`:

* Se llamará `lucky_number`
* No recibirá parámetros
* Devolverá el número **RANDOMNUMBER1-100END**

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

