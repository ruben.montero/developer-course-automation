Practicando eso de las consecuencias

## :books: Resumen

* Añadiremos dos ficheros `.py` que emulen la casuística de la tarea anterior

## :package: La tarea

**Se pide** que crees el fichero `maths.py`.

* **Tendrá** una función llamada `RANDOMEXPRESSIONsum_two_numbers|sum_two|sum_two_values|sum_two_variablesEND`.
  * Declarará que recibe **dos** parámetros
  * Devolverá la **suma** (`+`) de dichos parámetros
  
* **Si** el **fichero `maths.py`** se **ejecuta**, _invocará_ esa función pasando los valores:
  * RANDOMNUMBER1-50END
  * RANDOMNUMBER2-50END

...e **imprimirá** (`print`) el resultado. Por el contrario, si el fichero **es importado**, **no** se producirá este comportamiento.

Se pide **también** que crees el fichero `evaluate_student.py`:

* **Importará** la función de `maths.py` anteriormente descrita
* **Si se ejecuta** directamente:
  1. Asignará la variable `nota_practica=RANDOMNUMBER4-5END`
  2. Asignará la variable `nota_teoria=RANDOMNUMBER3-5END`
  3. Imprimirá: `"La nota de prácticas es:"`
  4. Imprimirá el valor de `nota_practica`
  5. Imprimirá: `"La nota de teoría es:"`
  6. Imprimirá el valor de `nota_teoria`
  7. **Invocará** el método de `maths.py` para sumar ambas variables y _almacenará_ el resultado en una nueva variable
  8. Imprimirá `"RANDOMEXPRESSIONNota final:|Evaluación final:|Resultado final:|Suma de ambas notas:END"`
  9. Imprimirá el resultado de la suma

* Si **no** se ejecuta **directamente**, ¡no hará nada!

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

