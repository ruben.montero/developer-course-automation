Varios ficheros

## :books: Resumen

* Veremos cómo funciona la sentencia `import` en Python
* Crearemos un fichero nuevo en `intro/`
* Importaremos `functions` e _invocaremos_ funciones

## Descripción

Nuestras funciones en `functions.py` son muy bonitas, pero **no** las estamos _invocando_.

Es decir, si ejecutamos directamente:

```
python functions.py
```

¡No se imprime nada por pantalla!

Aunque los _tests_ las usan, pero... ¡Nosotros **no**!

### `import`

Mientras que en Java la sentencia `import` se usaba para _importar_ una librería y poder usarla... En Python sirve para **dos** cosas:

* Importar una librería (también llamada _paquete_)
* Importar un fichero (también llamado _módulo_)

Puedes [leer algo más](https://betterprogramming.pub/how-imports-work-in-python-59c2943d87dc) sobre la nomenclatura y cuestiones relevantes a los `import` en Python. Nosotros, de momento, sólo consideramos lo siguiente:

* Si importamos una librería esencial, el `import` funciona **siempre** (e.g.: `import sys`)
* Si importamos una librería instalable, el `import` funciona si la librería está _instalada_ en nuestro sistema
  * Para instalar nuevos paquetes se suele emplear `pip`, por ejemplo, `pip install bcrypt`. Ya lo veremos
  * Los entornos virtuales tienen por objetivo ahorrarnos problemas de dependencias
* Si importamos un fichero, debe ubicarse en la carpeta actual.
  * Pueden importarse de otras ubicaciones; aunque no profundizaremos
  
### Dos tipos de `import`

Si te imaginas un nuevo fichero `simple_program.py` en `intro/`, podría invocar funciones de otro fichero (e.g.: `functions.py`) así:

```
import functions

functions.do_something()
```

Has notado que no _invocamos_ `do_something` directamente, como haríamos en Java. Aquí usamos el _nombre del módulo_ seguido de un punto (`.`).

Esto es típico de Python, ¡pero es tedioso!

Por eso, podemos usar las sentencias `from {...} import {...}`. Por ejemplo:

```python
from functions import do_nothing

do_nothing() # ¡Ya no lleva prefijo!
```

## :package: La tarea

**Se pide** que, en `functions.py` añadas una nueva función:

* Se llamará `RANDOMEXPRESSIONone_color|a_color|get_colorEND`
* No recibirá parámetros
* Únicamente, hará un `return` del _string_ **"RANDOMEXPRESSIONRojo|Verde|Azul|Amarillo|MoradoEND"**

Además, **se pide** que, en `simple_program.py` importes la función anterior (usando el método que prefieras) y:

1. La _invoques_ para asignar su resultado a una variable
2. Hagas `print("RANDOMEXPRESSIONEl color es:|Este es el color:|Ahora, vemos el color:END")`
3. Añadas otro `print` con el valor de la variable. O sea, el color

**Puedes** borrar el resto de código de `simple_program.py`

**Puedes** probar a ejecutar `simple_program.py` y ver que se imprime por consola el resultado esperable

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

