import context
import io
import unittest
from contextlib import redirect_stdout
from intro import functions


class ImportsTestCase(unittest.TestCase):

    def test_function_exists_and_simple_program_is_correct(self):
        value = functions.RANDOMEXPRESSIONone_color|a_color|get_colorEND()
        self.assertEqual(value, "RANDOMEXPRESSIONRojo|Verde|Azul|Amarillo|MoradoEND")

        with redirect_stdout(io.StringIO()) as f:
            from intro import simple_program
        console_output = f.getvalue()

        self.assertTrue(console_output.__contains__("RANDOMEXPRESSIONEl color es:|Este es el color:|Ahora, vemos el color:END"))
        self.assertTrue(console_output.__contains__("RANDOMEXPRESSIONRojo|Verde|Azul|Amarillo|MoradoEND"))


if __name__ == '__main__':
    unittest.main()
