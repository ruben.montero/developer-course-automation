import context
import io
import unittest
from contextlib import redirect_stdout
from intro import functions


class MoreFunctionsTestCase(unittest.TestCase):

    def test_more_functions(self):
        with redirect_stdout(io.StringIO()) as f:
            functions.RANDOMEXPRESSIONbrief_print|short_print|small_printEND()
        console_output = f.getvalue()

        self.assertTrue(console_output.__contains__("RANDOMEXPRESSIONEn 1453 se inventó la imprenta|En 1789 se produjo la revolución francesa|James Watt patentó en 1769 la máquina de vapor|El imperio romano creció tanto que se fragmento en dos partes principales en torno al mar Mediterráneo y en el año 476, cayó el imperio romano de occidente|La primera guerra mundial tuvo lugar de 1914 a 1918END"))
        self.assertEqual(functions.lucky_number(), RANDOMNUMBER1-100END)


if __name__ == '__main__':
    unittest.main()
