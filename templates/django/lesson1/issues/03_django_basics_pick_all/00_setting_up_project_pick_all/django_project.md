Django

## :books: Resumen

* Veremos qué es Django
* Lanzaremos un comando para inicializar un proyecto nuevo
* Visitaremos la página de ejemplo desde el navegador

## Descripción

Vamos a comenzar a trabajar con Python como se suele hacer habitualmente: _Aprovechando la inmensa variedad de utilidades y librerías que están a nuestra disposición_.

Uno de los puntos fuertes de Python es que, para _algo_ que queramos hacer, seguramente _alguien_ ya lo haya hecho antes.

## [Django](http://www.djangoproject.com/)

Este [Django](http://www.djangoproject.com/) está igual de _desencadenado_ que el de la [película](https://es.wikipedia.org/wiki/Django_Unchained). Se trata de un _framework_ de desarrollo web con Python [usado en](https://www.monocubed.com/blog/django-website-examples/): Youtube, Instagram, Pinterest, Dropbox, Spotify, National Geographic, Bitbucket, Mozilla Firefox (sitio web), NASA (sitio web),...

## :package: La tarea

**Abramos** nuestro _terminal_ de Windows. Podemos pulsar **Tecla de windows > cmd** y darle a ENTER.

_Para instalar Django_, ejecutamos[^1]:

```
pip install django
```

_(Y ya que estamos, instalemos una dependencia que facilitará la ejecución de los tests)_

```
pip install requests
```

Luego, usamos `cd` para cambiar el directorio activo a la carpeta de nuestro proyecto `python-introduction/`, cambiamos a la carpeta `api/` en su interior.

Desde `python-introduction/api/` **ejecutamos** este comando:

```
django-admin startproject RestAPI
```

Si tienes problemas porque el comando `django-admin` parece no estar disponible, quizá hubo algún error al registrarlo en el PATH durante la instalación de Django.

Alternativamente, puedes probar este comando que es **equivalente**:

```
python -m django startproject RestAPI
```

Si **no** ha salido ningún mensaje de error, habrás creado una nueva carpeta en `api/` con el siguiente contenido:

![](python-introduction/docs/django_structure1.png)

Ahora, vamos a [lanzar el proyecto](https://vegibit.com/how-to-run-django-development-server/) que contiene una página de ejemplo. **Cambia** al directorio recién creado:

```
cd RestAPI
```

...y **ejecuta** el fichero `manage.py`, pasando el _argumento_ `runserver`:

```
python manage.py runserver
```

Esta será la salida esperable:

```
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).

You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
July 08, 2022 - 09:42:43
Django version 4.0.5, using settings 'RestAPI.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CTRL-BREAK.
```

### ¡El servidor está funcionando!

Si te fijas, una de las líneas reza:

```
Starting development server at http://127.0.0.1:8000/
```

`127.0.0.1` es sinónimo de `localhost` en IPv4.

Es decir, podemos visitar http://localhost:8000/ y, ¡ver nuestro servidor! Para detenerlo basta con pulsar `CTRL+C` o bien cerrar el terminal (cmd.exe)

### ¿Se puede ejecutar Django desde PyCharm?

¡Sí! Es tan fácil como añadir una nueva **configuración**.

Para ello, seleccionamos el _desplegable_ en la barra superior, al lado del botón verde de **'Play'** y elegimos **Edit Configurations**

En la ventana que sale, _arriba a la izquierda_ hay un botón _más_ (**`+`**). Le damos **Add new configuration > Python**.

Ahora, en el panel principal de la ventana editamos 3 cosas:

1. `Name:` (arriba). En vez de _Unnamed_, podemos dar algo más significativo como _Django_ ó _RestAPI_
2. `Script path:` Hacemos click en el icono de _carpeta_ (a la derecha) y buscamos _python-introduction > api > RestAPI > `manage.py`_.
3. `Parameters:` (justo debajo). Escribimos _runserver_

Dándole a **OK** se guarda la configuración, y **el botón verde 'Play'** la ejecuta. ¡Ya podemos probar el servidor desde PyCharm!

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.


------------------------------------------------------------------------

[^1]: Las librerías en Python se instalan, generalmente, usando [`pip`](https://pypi.org/project/pip/). Es un comando que ya obtuvimos al instalar Python.
