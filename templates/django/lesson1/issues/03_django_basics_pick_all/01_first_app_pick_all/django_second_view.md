Una segunda página con Django

## :books: Resumen

* Repetiremos pasos de la tarea anterior

## :package: La tarea

**Se pide** que modifiques `views.py` y `urls.py` para entregar el siguiente contenido HTML:

```html
<html>
  <body>
    <h1>RANDOMEXPRESSIONProbando, probando...|1, 2, probando...|Encendiendo...|Calentando...END</h1>
    <p>RANDOMEXPRESSIONEn esta página de prueba se muestran tres emperadores romanos|En esta página se muestran tres emperadores romanos|A continuación se muestran tres emperadores romanos|Ahí van tres emperadores romanosEND</p>
    <ul>
      <li>RANDOMEXPRESSIONAugusto|TiberioEND</li>
      <li>RANDOMEXPRESSIONCalígula|ClaudioEND</li>
      <li>RANDOMEXPRESSIONNerón|Trajano|Marco AurelioEND</li>
    <ul>
  </body>
</html>
```

...desde la URL http://localhost:8000/emperors

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
