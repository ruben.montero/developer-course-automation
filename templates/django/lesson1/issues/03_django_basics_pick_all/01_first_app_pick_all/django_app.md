Django (II)

## :books: Resumen

* Crearemos nuestra primera _app_ y la incluiremos en `settings.py`
* Modificaremos `views.py` para añadir una función que sirve una página
* Modificaremos `urls.py` para _mapear_ una URL a dicha vista, y servir así nuestra primera página

## Descripción

Una de las claves de Django es su _modularidad_. Es decir, podemos crear distintas _apps_.

¡Comencemos trabajando en nuestra primera _app_ Django!

## :package: La tarea

**Abramos** nuestro _terminal_ (cmd.exe) de Windows y cambiemos el directorio activo a `python-introduction/api/RestAPI/`.

Desde aquí, volveremos a ejecutar `manage.py`, pero esta vez pasando el argumento `startapp`. Elegiremos el nombre **`webserviceDEVELOPERNUMBERapp`** para nuestra _app_.

```
python manage.py startapp webserviceDEVELOPERNUMBERapp
```

Cuando lo hagamos... ¡Python habrá creado un _esqueleto_ de _app_ para nosotros! 

![](python-introduction/docs/django_structure2.png)

### Hay que añadirla en `settings.py`

El fichero de [configuración de Django](https://docs.djangoproject.com/en/4.0/topics/settings/) es ese `settings.py` resaltado en rojo. Contiene _información_ de las _apps_ que usa nuestro proyecto. [Mucha gente desarrolla _apps_ que podríamos decidir usar](https://djangopackages.org/) y serían de utilidad. Nosotros vamos únicamente a incluir `webserviceDEVELOPERNUMBERapp`, que acabamos de crear.

**Abrimos** `settings.py` y en el apartado `INSTALLED_APPS`, **añadimos** la nuestra empleando la sintaxis `nombreapp.apps.NombreAppConfig`. O sea:

```diff
INSTALLED_APPS = [
+   'webserviceDEVELOPERNUMBERapp.apps.WebserviceDEVELOPERNUMBERAppConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```

_(Es aconsejable ejecutar `python manage.py runserver` ahora para comprobar que no hemos roto nada)_

### `views.py`

El fichero `views.py` contiene las **funciones** que renderizarán el contenido de la web, sea este un HTML o un JSON (REST API).

Vamos a crear nuestra primera **vista web** definiendo un método `my_first_view`, que devolverá un mensaje. _(Más adelante veremos que devolver un simple `string` falla, y lo arreglaremos :wink:)_. **Así**:

```python
from django.shortcuts import render

def my_first_view(request):
    return "Hola, mundo"
```

De momento, `my_first_view` **no** está conectada a nada. ¡Hay que _conectarla_ a una URL!

### `urls.py`

**Abrimos** el archivo que `RestAPI/urls.py`:

```python
"""RestAPI URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
]
```

Primero vamos a **importar** el módulo (fichero) `views.py`:

```diff
from django.contrib import admin
from django.urls import path
+from webserviceDEVELOPERNUMBERapp import views

urlpatterns = [
    path('admin/', admin.site.urls),
]
```

Si PyCharm lo marca en rojo, **no** es un problema. Como nuestro proyecto Django está en una subcarpeta, a PyCharm [le cuesta entender qué _módulos_ están disponibles](https://stackoverflow.com/questions/38342618/pycharm-not-recognizing-django-project-imports-from-my-app-models-import-thing).

Si te molesta, puedes hacer _click_ derecho en la carpeta `RestAPI/` padre (la que cuelga de `api/`) y:

* _Mark directory as > Sources root_

Arreglado.

Para terminar, sólo falta **añadir** a `urls.py` la línea que _mapea_ la URL `/example` (por decisión arbitraria) con la función `my_first_view`. Así:

```python
from django.contrib import admin
from django.urls import path
from webserviceDEVELOPERNUMBERapp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('example', views.my_first_view),
]
```

**Listo**.

Si ejecutamos el servidor (con el botón **'Play'** o con `python manage.py runserver`) _parece_ que todo va bien.

Podemos abrir nuestro navegador favorito y dirigirnos a http://localhost:8000/example y... ¿Qué vemos?

### Una página de error

Vaya. Django nos indica que _algo ha ido mal_.

En concreto, quiere decir que **no** podemos devolver un simple `string` en nuestra función de vista.

### ¿Y qué hacemos?

Debemos usar un objeto `HttpResponse`, o, más adelante, `JsonResponse`.

**Corrige** el problema en `views.py` así:

```diff
from django.shortcuts import render
+from django.http import HttpResponse

def my_first_view(request):
-    return "Hola, mundo"
+    return HttpResponse("RANDOMEXPRESSIONHola, mundo, ahora bien hecho|Hola, mundo, usando un HttpResponse|Hola a todo el mundo, ahora bien hecho|Hola a todo el mundo, usando un HttpResponse|Hola a todo el mundo, esta vez usando un HttpResponse|Hola, mundo, esta vez usando un HttpResponseEND")
```

¿Qué muestra tu navegador al ir a http://localhost:8000/example?

**¡Parece que has creado tu primera página con Django! ¡Enhorabuena!**

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
