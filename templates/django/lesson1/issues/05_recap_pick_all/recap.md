¡Trabajo terminado!

## :trophy: ¡Felicidades!

Has realizado las tareas del _sprint_ y has validado tu trabajo con los _tests_. ¡Enhorabuena!

<img src="python-introduction/docs/sunset.jpg" width=500 />

Has...

* Instalado Python
* Escrito funciones que hacen _prints_ de `string` normales y multilínea
* Importado ficheros Python desde otros ficheros de código fuente Python, comprendiendo que al importar, el código principal se _ejecuta_
* Usado `if __name__ == "__main__"` para encapsular el código principal de un fichero Python
* Escrito funciones que reciben parámetros y operan con `+`
* Comprendido que en función de lo que se reciba, como el tipado es dinámico, el `+` (y todo, en general) funciona de una manera u otra
* Escrito funciones que devuelven tipos de datos distintos
* Usado `str()` para convertir a `string`, e `int()` para convertir a `int`. En este último, has usado `try: | except ValueError:` para controlar errores de conversion
* Leído la entrada del usuario con `input()`, y usado un bucle `while` para validarla
* Escrito bucles `for`, empleando `range`, y las distintas maneras de usar `range`
* Escrito bucles `for-each` que recorren listas de elementos, y varias utilidades y maneras de usarlos
* Trabajado con clases Python, escribiendo una clase `ChatBot` que tiene método constructor donde se inicializan atributos, y otros métodos
* Comprendido que, por obligación, siempre se declara el parámetro `self` (en realidad lo puedes llamar como quieras) en los métodos de las clases
* Creado un ChatBot muy chulo
* Inicializado un proyecto Django y configurado una _app_ dentro de él
* Creado un par de URLs que son controladas desde una función en `views.py` y devuelven HTML
* Creado varios _endpoints_ REST controlados desde funciones en `endpoints.py`, y devuelven contenido JSON
* En dichos _endpoints_ has probado a devolver un código HTTP `404` y a devolver un _array_ JSON (`[ ]`) (en lugar de un diccionario)
