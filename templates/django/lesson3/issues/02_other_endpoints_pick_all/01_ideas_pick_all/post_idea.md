Subir una idea

## :books: Resumen

* Explicaremos cómo funciona la petición `POST` con la que los usuarios suben ideas
* Veremos por qué no se puede falsificar. Sólo cada usuario sube sus ideas
* La implementaremos

## Descripción

El **corazón** (:heart:) de nuestro `IdeAPI` es la petición `POST` que permite a los usuarios _subir_ (crear) una nueva idea en la base de datos.

A diferencia de los proyectos anteriores, ahora, en nuestra petición, soportaremos...

### ...autenticación

Esperaremos en las [**cabeceras HTTP**](https://developer.mozilla.org/es/docs/Web/HTTP/Headers) un _token de sesión_ `Api-Session-Token` (esta clave podría ser cualquier otra).

```
POST /v1/categories/2/ideas
Api-Session-Token: 51a3bc2ada4909f5dffd50e468aa887ca2a0a62b

{
  "RANDOMEXPRESSIONnew_idea|idea_title|idea_name|new_idea_title|new_idea_name|new_idea_summaryEND": "Bajar el precio de la máquina de café",
  "RANDOMEXPRESSIONdescription|content|long_description|explanationEND": "¡Está muy caro!"
}
```

Nótese que hay una serie de [cabeceras estándar](https://es.wikipedia.org/wiki/Anexo:Cabeceras_HTTP). Pero está permitido usar _cabeceras propias_, como nuestra `Api-Session-Token`.

### ¿Y cómo se envían desde un cliente HTTP las **cabeceras**?

Eso es preocupación de quien esté implementando el cliente HTTP (e.g.: _App_ Android, JavaScript de una web,...)

Nostros, para nuestras pruebas, podemos usar `cURL` con `--header`:

```
curl -X POST http://localhost:8000/v1/categories/2/ideas --header "Api-Session-Token: 51a3bc2ada4909f5dffd50e468aa887ca2a0a62b" -d "{\"RANDOMEXPRESSIONnew_idea|idea_title|idea_name|new_idea_title|new_idea_name|new_idea_summaryEND\": \"Bajar el precio de la máquina de café\", \"RANDOMEXPRESSIONdescription|content|long_description|explanationEND\": \"¡Está muy caro!\"}"
```

### ¿Y cómo se _interpretan_ las cabeceras HTTP desde el **servidor**?

Con Django, basta con usar `.headers`:

```python
def my_endpoint(request):
    print(request.headers['Api-Session-Token'])
    
    # Como headers es un diccionario, podemos
    # acceder a sus clave-valor también usando
    # .get y un valor por defecto
    session_header = request.headers.get('Api-Session-Token', None):
    if session_header is not None:
        print("Tenemos un token del cliente HTTP. Es: " + session_header)
    else:
        print("¡Este cliente no está autenticado!")
```

Ya estamos preparados para implementar la nueva operación...

### POST /v1/categories/{categoryId}/ideas

**Parámetros:**

| Nombre | En | Descripción | ¿Obligatorio? | Tipo |
| ---- | ---------- | ----------- | -------- | ---- |
| Api-Session-Token | headers | Token de sesión que identifica y autentica a un usuario | Sí | string |
| {categoryId} | path | ID de la categoría a la que pertenecerá la idea | Sí | int |
| RANDOMEXPRESSIONnew_idea|idea_title|idea_name|new_idea_title|new_idea_name|new_idea_summaryEND | body (json) | Breve descripción de la nueva idea | Sí | string |
| RANDOMEXPRESSIONdescription|content|long_description|explanationEND | body (json) | Descripción extendida de la idea | Sí | string |

**Respuestas:**

| Código | Descripción |
| ---- | ----------- |
| 201 | La idea se ha creado con éxito |
| 400 | La petición HTTP no se envió con todos los parámetros en el JSON |
| 401 | El _token_ de sesión no se ha enviado o no es válido |
| 404 | La categoría identificada por `{categoryId}` no existe |


------------------------------------------------------------------------

## :package: La tarea

### `endpoints.py`

**Añadamos** una nueva función.

Organizaremos el control del método HTTP de la siguiente manera, ya que en el futuro implementaremos un `GET`:

```python
@csrf_exempt
def ideas(request, category_id):
    if request.method == 'POST':
        # El usuario va a añadir una idea a la base de datos
    elif request.method == 'GET':
        # El usuario quiere consultar las ideas de la categoría con id == category_id
        pass
    else:
        return JsonResponse({'error': 'RANDOMEXPRESSIONUnsupported HTTP method|HTTP method not supported|HTTP method unsupported|Not supported HTTP methodEND'}, status=405)
```

### Si las ideas fueran anónimas...

...bastaría con:

* Recuperar la categoría de la base de datos, procesar la información del cuerpo JSON de la petición y crear una nueva `Idea`, que se guardará como nueva _fila_

```python
        # El usuario va a añadir una idea a la base de datos
        try:
            category = Category.objects.get(id=category_id)
            body_json = json.loads(request.body)
            json_title = body_json['RANDOMEXPRESSIONnew_idea|idea_title|idea_name|new_idea_title|new_idea_name|new_idea_summaryEND']
            json_description = body_json['RANDOMEXPRESSIONdescription|content|long_description|explanationEND']
            idea = Idea()
            # ¿De quién es la idea?
            idea.title = json_title
            idea.description = json_description
            idea.category = category
            idea.save()
            return JsonResponse({"RANDOMEXPRESSIONsuccess|created|ok|uploaded|is_success|is_createdEND": True}, status=201)
        except KeyError:
            return JsonResponse({"error": "RANDOMEXPRESSIONMissing parameter|Missing parameter in body|Missing parameter in json request body|Missing parameter in request|You are missing a parameterEND"}, status=400)
        except Category.DoesNotExist:
            return JsonResponse({"error": "RANDOMEXPRESSIONCategory not found|Category was not found|Category does not existEND"}, status=404)

```

### Pero hay un problema...

¿De quién es la idea?

```python
        idea = Idea()
        idea.user = # ???
```

La respuesta es la misma que...

### ¿...de quién es el _token_ de la HTTP _header_?

Vamos a crear una función que devuelve el **usuario** asociado al _token_ de la petición, o `None` si hay algún problema:

## `__get_request_user(request)`

Esta función recibe la `request`, y _extrae_ de las cabeceras el _token de sesión_, como vimos antes. Si el _token_ no está presente en las cabeceras bajo la clave `'Api-Session-Token'` según la especificación, devolvemos `None` directamente:

```python
def __get_request_user(request):
    header_token = request.headers.get('Api-Session-Token', None)
    if header_token is None:
        return None
```

En caso contrario, recuperamos la `UserSession` asociada a dicho _token_. Gracias al _mapeo objeto-relacional_, ¡basta con acceder al atributo `RANDOMEXPRESSIONuser|author|person|creatorEND` para obtener la _fila_ del `User`!

```python
def __get_request_user(request):
    header_token = request.headers.get('Api-Session-Token', None)
    if header_token is None:
        return None
    try:
        db_session = UserSession.objects.get(token=header_token)
        return db_session.RANDOMEXPRESSIONuser|author|person|creatorEND
    except UserSession.DoesNotExist:
        return None
```

### ¡Listos para devolver `401` o seguir con éxito!

Ahora, en nuestra función original:

```diff
        idea = Idea()
        # ¿De quién es la idea?
+       authenticated_user = __get_request_user(request)
+       if authenticated_user is None:
+           return JsonResponse({"error": "RANDOMEXPRESSIONInvalid authentication|Authentication not valid|Not valid token or missing header|Not valid or missing token|You did not provide a valid session token|You did not provide a valid tokenEND"}, status=401)
+       idea.user = authenticated_user
        # ...
```

### `urls.py`

Ya sólo falta **modificar** `urls.py` para añadir el _mapeo_ relevante. **¡Hazlo!**

¡Y habremos terminado con éxito nuestra primera petición que requiere **autenticación de usuario**

Puedes verificar cómo funciona lanzando un `cURL` como el de arriba[^1].

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------------------------------------

[^1]: Si te encuentras con un error `UnicodeDecodeError: 'utf-8' codec can't decode byte`, seguramente se están mandando caracteres especiales (e.g.: `á`, `é`, `¡`,...) codificados según su código Unicode. Prueba a usar `json.loads(request.body.decode('raw_unicode_escape'))` en lugar de `json.loads(request.body)`


