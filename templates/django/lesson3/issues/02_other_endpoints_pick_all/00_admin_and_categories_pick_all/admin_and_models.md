Modelos y sitio de administración

## :books: Resumen

* Crearemos clases en `models.py` para representar _Categorías_, _Ideas_ y _Comentarios_
* Hablaremos del sitio de administración Django
* Permitiremos editar las _Categorías_ de la base de datos con un superusuario Django

## Descripción

Anteriormente presentamos cuáles serían nuestros modelos en lo relevante a las aportaciones de los usuarios:

```mermaid
graph LR;
D[Category]-->|Contiene varios|E[Idea];
E-->|Contiene varios|F[Comment];
```

* Las `Category` son prefijadas, gestionadas por el administrador
* Los usuarios crean `Idea`, que pertenecen a una `Category`
* Los usuarios crean `Comment`, que pertenecen a una `Idea`

Por lo tanto, en realidad:

```mermaid
graph LR;
D[Category]-->|Contiene varios|E[Idea];
E-->|Contiene varios|F[Comment];
A[CustomUser]-->|Crea|E;
A[CustomUser]-->|Crea|F;
```

### Panel de administración

Ahora, hemos dicho varias veces que:

> Las `Category` son prefijadas, gestionadas por el administrador

Nosotros **sabemos** manipular la base de datos mediante la línea de comandos `sqlite3.exe`.

¡Django nos ofrece algo mucho más estimulante!

### [Panel de administración](https://docs.djangoproject.com/en/4.0/ref/contrib/admin/)

¿Recuerdas esta enigmática línea que se crea en `urls.py` por defecto?

```python
    path('admin/', admin.site.urls),
```

Pues tiene **su significado**.

Si visitas http://localhost:8000/admin, verás el _panel de administración_ de Django:

<img src="python-sessions/docs/django_panel.png" width=500 />

### ¿Y cómo hago _login_?

Necesitamos **crear un usuario administrador**.

## :package: La tarea

Desde la carpeta `IdeAPI/` donde se aloja `manage.py`, **lanzamos** el comando:

```
python manage.py createsuperuser
```

Nos pedirá:

* Nombre de usuario
* Email
* Contraseña

¡...y añadirá al usuario a la base de datos!

Ya podemos introducir dichas credenciales en http://localhost:8000/admin

### Me he _logueado_ en el panel

Excelente.

### `models.py`

**Añade** los modelos `Category`, `Idea` y `Comment` a `models.py`:

```py
class Category(models.Model):
    title = models.CharField(max_length=RANDOMEXPRESSION35|40|45|50|55|60|65|70END, unique=True)

class Idea(models.Model):
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=RANDOMEXPRESSION2000|2200|2400|2600|2800|3000END)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

class Comment(models.Model):
    content = models.CharField(max_length=RANDOMEXPRESSION1000|1100|1200|1300|1400|1500END)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    idea = models.ForeignKey(Idea, on_delete=models.CASCADE)
```

...y **migra** la base de datos:

```
python manage.py makemigrations
python manage.py migrate
```

### ¡Manejando nuestros modelos desde el panel!

Si regresas a http://localhost:8000/admin, verás una sección que te permite gestionar _Groups_ y _Users_. Estos son modelos predeterminados de Django. Pero aún no podemos gestionar nuestros **propios modelos**.

### `admin.py`

**Abre** `admin.py` en `idearestDEVELOPERNUMBERapp/`. Habrá algo como:

```py
from django.contrib import admin

# Register your models here.
```

**¡Añade!** lo siguiente:

```python
from django.contrib import admin

from .models import Category

admin.site.register(Category)
```

**Refresca** la página http://localhost:8000/admin en tu navegador

Ahora, podrás **añadir** estas cuatro `Category`:

* **Title**: Sociedad
* **Title**: Educación
* **Title**: RANDOMEXPRESSIONProducciones artísticas|Arte|Cine y libros|ProduccionesEND
* **Title**: RANDOMEXPRESSIONInvenciones|InventosEND

Estás **añadiendo filas** a la base de datos, sin necesidad de `sqlite3.exe`. ¿No es genial?

Si ves:

```
Category object (4)
Category object (3)
Category object (2)
Category object (1)
```

¡No te preocupes! Simplemente, Django **no** sabe cómo _representar_ nuestras `Category`.

Podemos enseñarle, _sobreescribiendo_ el método [`__str__`](https://www.geeksforgeeks.org/python-str-function/) en la clase, que es análogo al [`toString()`](https://javautodidacta.es/metodo-tostring-java/) de Java.

En `models.py` **añade** `__str__`:

```diff
class Category(models.Model):
    title = models.CharField(max_length=RANDOMEXPRESSION35|40|45|50|55|60|65|70END, unique=True)
+
+   def __str__(self):
+       return self.title
```

¿Qué tal se ve ahora http://localhost:8000/admin/idearestDEVELOPERNUMBERapp/category/?

¡Tarea terminada!

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

