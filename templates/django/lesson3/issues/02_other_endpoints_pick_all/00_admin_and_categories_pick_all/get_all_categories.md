GET /v1/categories

## :books: Resumen

* Definiremos el _endpoint_ `GET /v1/categories`
* Lo implementaremos

## Descripción

En nuestra `IdeAPI` los _endpoints_ para acceder a la información son públicamente accesibles.

El primero es aquel que devuelve todas las categorías.

### GET /v1/categories

**Parámetros:**

_(Ninguno)_

**Respuestas:**

| Código | Descripción |
| ---- | ----------- |
| 200 | Se devuelve la lista de categorías |

**Ejemplo de cuerpo de respuesta (`200`):**

```json
[
  {
    "RANDOMEXPRESSIONid|category_id|cat_idEND": 1,
    "RANDOMEXPRESSIONtitle|name|category_title|category_nameEND": "Sociedad"
  },
  {
    "RANDOMEXPRESSIONid|category_id|cat_idEND": 2,
    "RANDOMEXPRESSIONtitle|name|category_title|category_nameEND": "Educación"
  },
  {
    "RANDOMEXPRESSIONid|category_id|cat_idEND": 3,
    "RANDOMEXPRESSIONtitle|name|category_title|category_nameEND": "RANDOMEXPRESSIONProducciones artísticas|Arte|Cine y libros|ProduccionesEND"
  },
  {
    "RANDOMEXPRESSIONid|category_id|cat_idEND": 4,
    "RANDOMEXPRESSIONtitle|name|category_title|category_nameEND": "RANDOMEXPRESSIONInvenciones|InventosEND"
  }
]
```

------------------------------------------------------------------------

## :package: La tarea

**Se pide** que:

* Implementes el _endpoint_, añadiendo el código necesario en `urls.py` y `endpoints.py`

### Pruébalo

Con un `cURL`, por ejemplo:

```
curl -X GET http://localhost:8000/v1/categories
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.


