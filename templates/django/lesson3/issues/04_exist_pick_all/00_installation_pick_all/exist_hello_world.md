NoSQL

## :books: Resumen

* Hablaremos de bases de datos NoSQL
* Instalaremos eXistDB y visitaremos el _dashboard_ de administración
* Introduciremos XQuery
* Crearemos un sencillo programa en XQuery que genera un XML de _Hola mundo_
* Lo guardaremos en nuestro repositorio

## Descripción

Hemos trabajado detenidamente [bases de datos relacionales](https://www.ionos.es/digitalguide/hosting/cuestiones-tecnicas/bases-de-datos-relacionales/) usando el lenguaje de consultas SQL y el _mapeo objeto-relacional_. Exponemos los datos mediante una [fachada REST](https://blog.hubspot.com/website/what-is-rest-api) y, en resumen, nos hemos ganado una buena porción del pastel del desarrollo _full-stack_.

Pero, ¿eso es todo?

### ¡Nunca es todo!

Así es.

Dediquemos un rato a otro tipo de tecnología que también pertenece al mundo del acceso a datos, pero no guarda relación con las bases de datos SQL. De hecho, se llama...

## [NoSQL](https://www.rackspace.com/es/library/what-is-a-nosql-database)

Las [**bases de datos NoSQL**](https://www.rackspace.com/es/library/what-is-a-nosql-database) son sistemas de bases de datos **no** estructurados.

* Son aptas para manejar tipos de datos complejos y que cambian constantemente
* Se pueden agregar datos sin definir previamente su estructura
* Posibilitan procesar rápidamente grandes volúmenes de datos y realizar desarrollos ágiles
* Principalmente, los datos son almacenados en forma de _documentos_ XML ó JSON

[MongoDB](https://www.mongodb.com/es) y [ElasticSearch](https://www.elastic.co/es/elasticsearch/) son dos de las bases de datos NoSQL más populares.

Nosotros trabajaremos con...

### [eXist DB](http://exist-db.org/)

También es una de las plataformas NoSQL más conocida:

* Se centra en el manejo de **documentos XML**
* Ofrece mucho más potencial que aquello a lo que estamos acostumbrados en SQL. Veremos cómo mediante unos _scripts_ [XQuery](https://es.wikipedia.org/wiki/XQuery) se puede, por ejemplo, crear páginas web generadas dinámicamente
* Es de licencia abierta

## :package: La tarea

¡Instalemos [eXist DB](http://exist-db.org/)!

### 1. Asegurémonos de tener Java disponible

Abre un terminal (cmd.exe) de Windows. Teclea:

```
java -version
```

¿Ves la salida correcta del comando?

```
openjdk version "18.0.2.1" 2022-08-18
OpenJDK Runtime Environment (build 18.0.2.1+1-1)
```

### ¡Sí!

Excelente. Puedes avanzar hasta el apartado de **Descargar e instalar eXist**

### Un momento, ¡no lo veo!

Si no puedes usar Java desde la línea de comandos, seguramente instalaste el [JDK](https://www.infoworld.com/article/3296360/what-is-the-jdk-introduction-to-the-java-development-kit.html) cuando descargaste [IntelliJ IDEA](https://www.jetbrains.com/idea/) pero no está disponible en las variables de entorno.

Abre el explorador de archivos de Windows.

Ve a _Este equipo_ > _C:_ > _Usuarios_ > _Developer_. Si ves una carpeta llamada `.jdks/`, **¡bien!** Ábrela. Dentro habrá una carpeta `openjdk-x.y.z/`, siendo `x.y.z` la versión de Java que instalaste. Dentro, otra carpeta llamada `bin/`.

**En la carpeta `bin/` se encuentra Java**.

### Entonces, ¿por qué no funciona `java -version`?

Porque el directorio `C:/Users/Developer/.jdks/openjdk-x.y.z/bin` **no** está en el PATH.

PATH es una _variable de entorno_ que está en todos los sistemas operativos.

Cuando escribes un _comando_ (e.g.: `java`) en un terminal (cmd.exe), Windows _busca_ ese archivo ejecutable en los directorios que pertenecen al PATH. Si **no** lo encuentra, salta el error que estarás viendo:

```
"java" no se reconoce como un comando interno o externo,
programa o archivo por lotes ejecutable.
```

Para arreglarlo:

1. Pulsa la tecla de Windows y teclea _variables de entorno_. Deberías ver una opción _Panel de Control > **Editar variables de entorno del sistema**_. Haz _click_
2. En la ventana que sale, haz _click_ en el botón _Variables de entorno..._ de la parte inferior.
3. Verás un diálogo con dos recuadros blancos. En el inferior (Variables del sistema), busca "Path".
4. Dale a _Editar..._
5. Vamos a pulsar _Nuevo_ y a escribir la ruta que descubrimos anteriormente: `C:/Users/Developer/.jdks/openjdk-x.y.z/bin`. Asegúrate que `x.y.z` se corresponde con tu versión de Java y que la ruta es correcta y existe en tu sistema
6. Aceptamos, aceptamos y aceptamos

Cuando se cierre el último diálogo, abre un **nuevo** terminal (cmd.exe). Ahora, `java -version` debería generar la salida esperada.

## Descargar e instalar eXist

Desde la [página de eXist](http://exist-db.org/) habrá un enlace de Descarga que lleva a GitHub. Allí, veremos las [página de descarga de las distintas _releases_](https://github.com/eXist-db/exist/releases/).

**Descargamos** el `.jar` de la más reciente. Será `exist-installer-x.y.z.jar` (ojo, `x.y.z` serán números :wink:)

### Instalación

Seguiremos los [pasos de la guía oficial de instalación](http://exist-db.org/exist/apps/doc/basic-installation).

En primer lugar, abrimos un terminal Windows (cmd.exe) y usando `cd` cambiamos hasta el directorio de Descargas.

**Lanzamos** el instalador con:

```
java -jar exist-installer-x.y.z.jar
```

<img src="python-sessions/docs/exist_installer.png" width=400 />

Cuando lleguemos al paso que solicita **nombre de usuario** y **contraseña** de administrador, introducimos unas credenciales cualesquiera de las que nos acordemos después. No haremos nada serio, así que pueden ser `admin`, `admin`.

El resto de la instalación la completamos manteniendo todas las opciones por defecto.

### Tras la instalación

Ejecutamos **eXist DB** pulsando en la tecla de windows y buscando el programa:

<img src="python-sessions/docs/exist_launching.png" width=550 />

Deberemos **confirmar** la configuración.

Luego, el programa procederá a **instalar eXist como servicio Windows**. Aceptamos todo y esperamos a que el proceso termine.

### Cuando termine...

Verás abajo a la derecha el _popup_ de Windows indicando que el servicio está corriendo.

¡Enhorabuena! Has instalado tu base de datos NoSQL.

Puedes visitar el _dashboard_ de administración desde tu navegador favorito en:

* http://localhost:8080/exist

### ¿Y ahora?

Probaremos **eXide**, la herramienta que tienes a disposición para escribir programas en [XQuery](http://www.w3.org/TR/xquery-30/) y administrar los datos de tu base de datos.

### ¿Qué es [XQuery](https://www.ticarte.com/contenido/que-es-el-lenguaje-xquery)?

Un **lenguaje de consulta** que permite trabajar con bases de datos o documentos XML.

Es como SQL, pero para bases de datos XML. Se trata de un lenguaje amplio y complejo, recomendado por el [W3C](https://www.w3.org/).

**Abre eXide** para comprobar su funcionamiento.

### Un primer programa sencillo

Hay una [guía con muy buenos ejemplos](https://en.wikibooks.org/wiki/XQuery#XQuery_Examples_Collection) para comenzar con XQuery.

Nosotros vamos a escribir un sencillo programa que _genera un XML de "Hola mundo"_:

```xquery
xquery version "3.1";

let $message := 'RANDOMEXPRESSIONHello World!|Hello World|Hello there|Hola, mundo|Hola a todos|HolaEND'
return
<RANDOMEXPRESSIONresults|resultEND>
   <message>{$message}</message>
</RANDOMEXPRESSIONresults|resultEND>
```

Para guardarlo, **haz** click en _File_ > _Manage_. Verás un diálogo.

Entra en `apps/`, y, desde allí, usa el tercer botón de la barra superior dentro del diálogo. Tiene un icono de una pequeña carpeta. Indica _Create collection_.

Dale el nombre `hello` a tu nueva colección. Luego, _Close_.

### Guardando...

Ya estamos listos para darle a _Save_ y entrar en `apps/hello/`. Introduce tus credenciales si aún no estás _logueado_.

Guarda tu _script_ como `hello.xql`. Ya podremos darle a **Run**.

### Salida de ejecución

Nuestro _Hola mundo_ **generará un XML** que se abrirá en una pestaña nueva.

Vamos a **guardar** el archivo generado (_click_ derecho > Guardar como) y salvar `hello.xql.xml` en nuestro repositorio, **dentro** de la carpeta `python-sessions/exist/results/`.

Hemos comprobado que funciona el entorno y hecho una breve introducción.

¡Tarea terminada!

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

