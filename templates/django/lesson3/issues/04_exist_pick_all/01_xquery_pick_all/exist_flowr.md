XQuery

## :books: Resumen

* Añadiremos un documento XML a nuestra base de datos _eXist_
* Escribiremos un programa en XQuery que consulta datos de dicho XML y produce una salida en XML

## Descripción

El _Hello World_ de nuestra tarea anterior no hace mucho:

```xquery
let $message := 'RANDOMEXPRESSIONHello World!|Hello World|Hello there|Hola, mundo|Hola a todos|HolaEND'
return
<RANDOMEXPRESSIONresults|resultEND>
   <message>{$message}</message>
</RANDOMEXPRESSIONresults|resultEND>
```

1. Asigna una variable. Como ves, se usa `let` y `:=`
2. La emplea en una salida XML
3. Genera dicho XML. Se usa `return`

### Entonces, XQuery sirve para **generar** documentos XML

En realidad, se centra en **consultar** y **procesar** documentos XML

## [FLWOR](https://www.progress.com/tutorials/xquery/restructuring-data-with-xquery-flwor-expressions-tutorial)

Es como el `SELECT, FROM, WHERE, ORDER BY` de SQL, pero se refiere a:

* `for`: _Qué_ items se quieren seleccionar _(opcional)_
* `let`: Usado para crear variables temporales _(opcional)_
* `where`: Limitar los ítems seleccionados _(opcional)_
* `order`: Cambiar el orden de los resultados _(opcional)_
* `return`: Especifica la estructura de la salida **_(obligatorio)_**

En nuestro _Hello world!_ hemos usado `let` y `return`.

### Un ejemplo

Si trabajásemos con una _colección_ `films` de un archivo XML como este:

```xml
<films>
  <film>
    <name>Pokemon: The Movie</name>
    <year>1998</year>
  </film>
  <film>
    <name>Lucario and the Mystery of Mew</name>
    <year>2005</year>
  </film>
</films>
```

Entonces, la siguiente **consulta XQuery**:

```xquery
for $film in doc("films.xml")/films/film
  let $name := $film/name
  let $year := $film/year
  where $year = 1998
  return $name
```

...producirá esta **salida**:

```xml
<name>Pokemon: The Movie</name>
```

##### `text()`

En esta asignación:

```xquery
  let $name := $film/name
```

...se le está dando a `$name` el valor del **nodo**. Es decir, `<name>...</name>`.

Si usamos `text()`, le daremos el valor del **contenido** (lo que haya _dentro_ de `<name>...</name>`)

Dicho de otra forma, la anterior consulta es **equivalente a**:

```xquery
for $film in doc("films.xml")/films/film
  let $name := $film/name/text()
  let $year := $film/year
  where $year = 1998
  return <name>{$name}</name>
```

Nosotros, en esta tarea, trabajaremos contra la colección [sessions.xml](python-sessions/exist/xml/sessions.xml)

## :package: La tarea

**Añade** una nueva colección desde **eXide**. _File_ > _Manage_ > _Create collection_. Será `/db/apps/sessions/`.

Luego, pulsa el cuarto icono de la nube con una flecha que indica _Upload files_:

<img src="python-sessions/docs/exist_upload.png" width=400 />

En nuestra nueva [colección](http://exist-db.org/exist/apps/doc/using-collections) `sessions/` vamos a subir [este archivo XML](python-sessions/exist/xml/sessions.xml). Descárgalo a tu ordenador (en realidad ya está en tu repositorio local) y **arrástralo** hasta el área _Drop files here_. Sucederá instantáneamente y no aparecerá una confirmación. Dale a _Close_.

### Nuevo XQuery

Selecciona _New XQuery_ para crear un programa nuevo.

Vamos a usar para ejemplificar la sintaxis de la sentencia **FLOWR** de Xquery

En un _New XQuery_, **pega** la siguiente consulta:

```xquery
for $session in doc("sessions.xml")/sessions/session
  let $username := $session/username/text()
  let $hour := $session/login/time/hour/text()
  let $minute := $session/login/time/minute/text()
  where $username = "Paul"
  return <time>{$hour}:{$minute}</time>
```

Tras esperar unos segundos para que **eXist** lo procese e indexe correctamente, podrás verificar que produce la siguiente salida:

```xml
<time>19:58</time>
<time>22:39</time>
<time>10:36</time>
<time>8:57</time>
<time>14:52</time>
<time>22:51</time>
<time>9:52</time>
<time>8:56</time>
```

#### `order by`

Para ordenar de forma ascendiente por `hour`, modificaremos la consulta así:

```xquery
for $session in doc("sessions.xml")/sessions/session
  let $username := $session/username/text()
  let $hour := $session/login/time/hour/number()
  let $minute := $session/login/time/minute/number()
  where $username = "Paul"
  order by $hour, $minute
  return <time>{$hour}:{$minute}</time>
```

Como ves:

* Se ha cambiado `text()` por `number()` en `$hour` y `$minute`. Así conseguimos que el contenido se interprete como número. Esto es necesario para que la ordenación sea correcta
* Se ha añadido `order by $hour, $minute`. Como ves, se puede especificar más de un criterio (campo) de ordenación. Así, si dos sesiones tiene la misma _hora_, se seguirá ordenando por _minuto_.

### Elemento XML raíz

Si queremos que el `for` produzca resultados y se vean _dentro_ de una etiqueta XML raíz, debemos _embeberlo_ así:

```xquery
<times>
{for $session in doc("sessions.xml")/sessions/session
  let $username := $session/username/text()
  let $hour := $session/login/time/hour/number()
  let $minute := $session/login/time/minute/number()
  where $username = "Paul"
  order by $hour, $minute
  return <time>{$hour}:{$minute}</time>
}
</times>
```

### Completando la tarea...

**Se pide** que escribas una consulta `sessions.xql` que obtenga la información de _cuándo_ fue accedido el libro `RANDOMEXPRESSIONHarry Potter y el prisionero de Azkaban|El inocente|El ingenioso hidalgo Don Quijote de la ManchaEND`:

* Devolverá un XML conformado por:
  * Elemento raíz `<RANDOMEXPRESSIONbookRegister|register|bookAccessEND>`
  * Elemento `<RANDOMEXPRESSIONname|titleEND>RANDOMEXPRESSIONHarry Potter y el prisionero de Azkaban|El inocente|El ingenioso hidalgo Don Quijote de la ManchaEND</RANDOMEXPRESSIONname|titleEND>`
  * Elemento `<accessTimes>` que contenga:
     * Varios elementos `<access>`. Cada uno con:
        * `<RANDOMEXPRESSIONviewedBy|accessedBy|user|usernameEND>`, contiene el _nombre de usuario_
        * `<date>`, contiene la fecha de acceso siguiendo el formato `dd/MMM/yyyy`

* Sólo recuperará la información de los accesos que se produjeron al libro: `"RANDOMEXPRESSIONHarry Potter y el prisionero de Azkaban|El inocente|El ingenioso hidalgo Don Quijote de la ManchaEND"`

* Cada `<access>` estará ordenado de forma ascendiente por el _nombre de usuario_.

------------------------------------------------------------------------

_Ejemplo para `El principito`_:

```xml
<RANDOMEXPRESSIONbookRegister|register|bookAccessEND>
    <RANDOMEXPRESSIONname|titleEND>El principito</RANDOMEXPRESSIONname|titleEND>
    <accessTimes>
        <access>
            <RANDOMEXPRESSIONviewedBy|accessedBy|user|usernameEND>Helena</RANDOMEXPRESSIONviewedBy|accessedBy|user|usernameEND>
            <date>9/December/2010</date>
        </access>
        <access>
            <RANDOMEXPRESSIONviewedBy|accessedBy|user|usernameEND>Helena</RANDOMEXPRESSIONviewedBy|accessedBy|user|usernameEND>
            <date>20/August/2011</date>
        </access>

    ...

    <accessTimes>
</RANDOMEXPRESSIONbookRegister|register|bookAccessEND>
```

Una vez tengas tu consulta, **ejecútala** y genera el documento **en una nueva pestaña**.

**Guarda `sessions.xql.xml`** en `python-sessions/exist/results/` de tu repositorio.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
