Un endpoint para crear sesiones

## :books: Resumen

* Definiremos cómo es la petición REST mediante la que soportaremos el _login_ de usuarios
* Crearemos una función en `endpoints.py` que procese dicha petición
* Leerá del cuerpo del POST el _email_ y _contraseña_
* Los validará con `checkpw`
* Si la contraseña es correcta, generará un _token_ aleatorio
* Lo persistirá en la base de datos y también lo devolverá en la respuesta JSON

## Descripción

Nuestro API REST soportará la siguiente operación REST:

### POST /v1/sessions

**Parámetros:**

| Nombre | En | Descripción | ¿Obligatorio? | Tipo |
| ---- | ---------- | ----------- | -------- | ---- |
| RANDOMEXPRESSIONemail|useremail|user_email|login_emailEND | body (json) | Correo electrónico | Sí | string |
| RANDOMEXPRESSIONpassword|user_password|login_passwordEND | body (json) | Contraseña del usuario | Sí | string |

**Ejemplo de cuerpo de petición:**

```json
{
  "RANDOMEXPRESSIONemail|useremail|user_email|login_emailEND": "testemail@test.email.co.uk",
  "RANDOMEXPRESSIONpassword|user_password|login_passwordEND": "asdf1234"
}
```

**Respuestas:**

| Código | Descripción |
| ---- | ----------- |
| 201 | La sesión se ha creado con éxito |
| 400 | La petición HTTP no se envió con todos los parámetros en el JSON |
| 401 | La contraseña es incorrecta |
| 404 | El usuario especificado no existe |

**Ejemplo de cuerpo de respuesta (`201`):**

```json
{
  "RANDOMEXPRESSIONtoken|session_token|user_session_tokenEND": "51a3bc2ada4909f5dffd50e468aa887ca2a0a62b"
}
```

------------------------------------------------------------------------

### Pero... ¿cómo funciona exactamente eso de _loguearse_?

Las [APIs REST (REpresentational State Transfer)](https://desarrolloweb.com/articulos/que-es-rest-caracteristicas-sistemas.html) no almacenan estado del cliente. 

<img src="python-sessions/docs/stateless_login_problems.png" width=700 />

La _sesión_ del cliente se persiste en la base de datos.

## Concepto de sesión: Autenticarse con un token

<img src="python-sessions/docs/stateless_login_explained.png" width=700 />

## :package: La tarea

### `urls.py`

**Añade** la línea para `v1/sessions`:

```python
urlpatterns = [
    path('admin/', admin.site.urls),
    path('v1/health', endpoints.health_check),
    path('v1/users', endpoints.users),
    path('v1/sessions', endpoints.sessions) # Nueva línea
]
```

### `endpoints.py`

**Añadamos** la función `sessions`, donde, predeciblemente no vamos a soportar otra cosa que no sea un `POST`.

Además, **procesaremos** los datos `RANDOMEXPRESSIONemail|useremail|user_email|login_emailEND` y `RANDOMEXPRESSIONpassword|user_password|login_passwordEND` enviados por el cliente HTTP:

```python
@csrf_exempt
def sessions(request):
    if request.method != 'POST':
        return JsonResponse({'error': 'RANDOMEXPRESSIONUnsupported HTTP method|HTTP method not supported|HTTP method unsupported|Not supported HTTP methodEND'}, status=405)
    body_json = json.loads(request.body)
    json_email = body_json['RANDOMEXPRESSIONemail|useremail|user_email|login_emailEND']
    json_password = body_json['RANDOMEXPRESSIONpassword|user_password|login_passwordEND']
    # De momento sólo controlamos el happy path
    # ...
```

### Siguientes pasos...

1. Recuperar al usuario con _email_ `json_email` de la base de datos
2. Comprobar que la _contraseña_ `json_password` es correcta
3. Si lo es, generar una nueva _sesión_ (`UserSession`)
4. Guardarla en la base de datos y devolverla en una respuesta JSON como está especificado

Primero, **añade** el código para (1) recuperar al usuario con _email_ `json_email` de la base de datos

```python
    # De momento sólo controlamos el happy path
    try:
        db_user = CustomUser.objects.get(RANDOMEXPRESSIONemail|useremail|e_mail|user_emailEND=json_email)
    except CustomUser.DoesNotExist:
        pass # No existe el usuario. En la siguiente tarea lo gestionamos
```

Luego comprobaremos que la _contraseña_ `json_password` es correcta. Como lo que tenemos guardado en la base de datos es un _hash_ de contraseña y _salt_, **no** podemos simplemente hacer una comparación de igualdad (`==`).

Necesitamos coger el _salt_ de la base de datos, concatenarlo a la contraseña, _hashearla_, y, entonces, verificar si el _hash_ calculado y el almacenado son iguales. Todo esto se hace con una sola línea[^1], **así**:

```python
    # ...
    if bcrypt.checkpw(json_password.encode('utf8'), db_user.encrypted_password.encode('utf8')):
        # json_password y db_user.encrypted_password coinciden
    else:
        pass # Contraseña incorrecta. En la siguiente tarea lo gestionamos
```

En caso de que la contraseña sea la correcta, **generaremos** una nueva _sesión_ (`UserSession`)[^2], la **guardamos** en base de datos y la **devolvemos** al cliente HTTP:

Ahora bien, :

```python
import secrets
from .models import UserSession

# ...

        # json_password y db_user.encrypted_password coinciden
        random_token = secrets.token_hex(10)
        session = UserSession(RANDOMEXPRESSIONuser|author|person|creatorEND=db_user, token=random_token)
        session.save()
        return JsonResponse({"RANDOMEXPRESSIONtoken|session_token|user_session_tokenEND": random_token}, status=201)
```


**¡Autenticación lista!**

Con este _token_ que los clientes deben recordar, nos aseguraremos de que _ellos son_ quienes en el futuro envíen las peticiones que requieran autenticación.

Prueba a lanzar un `cURL`:

```
curl -X POST http://localhost:8000/v1/sessions -d "{\"RANDOMEXPRESSIONemail|useremail|user_email|login_emailEND\": \"testemail@test.email.co.uk\", \"RANDOMEXPRESSIONpassword|user_password|login_passwordEND\": \"asdf1234\"}"
```

¿Tienes éxito?

¿Ves el _token_ generado?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.


------------------------------------------------------------------------

[^1]: `.encode('utf8')` es necesario para transformar texto en _bytes_ (aquello con lo que trabajan las funciones criptográficas)

[^2]: Cada _sesión_ se identifica por un _token_ aleatorio. Aunque podríamos usar la librería `random`, se considera criptográficamente más seguro emplear `secrets`
