Not so happy endpoint (II)

## :books: Resumen

* Controlaremos las situaciones de error en el _login_ de usuario

## Descripción

En la petición REST que hemos implementado en la tarea anterior, había definidas tres respuestas HTTP de error:

| Código | Descripción |
| ---- | ----------- |
| 400 | La petición HTTP no se envió con todos los parámetros en el JSON |
| 401 | La contraseña es incorrecta |
| 404 | El usuario especificado no existe |

## :package: La tarea

**Se pide** que controles estas situaciones:

* Devuelve una `JsonResponse` con _cuerpo de respuesta_ `{"error": "RANDOMEXPRESSIONMissing parameter|Missing parameter in JSON|Missing parameter in body|Missing parameter in JSON body|Missing parameter in body request|You are missing a parameterEND"}` y código `400` si _falta_ algún parámetro en el cuerpo JSON de la petición
* Devuelve una `JsonResponse` con _cuerpo de respuesta_ `{"error": "RANDOMEXPRESSIONInvalid password|Password not valid|Not valid passwordEND"}` y código `401` si la comprobación `checkpw` devuelve `False`
* Devuelve una `JsonResponse` con _cuerpo de respuesta_ `{"error": "RANDOMEXPRESSIONUser does not exist|User not found|User not in database|User not in our systemEND"}` y código `404` si _**no** existe_ un usuario registrado con el _email_ recibido

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
