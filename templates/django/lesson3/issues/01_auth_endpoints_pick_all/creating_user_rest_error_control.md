Not so happy endpoint (I)

## :books: Resumen

* Controlaremos las situaciones de error en la que debemos responder un `400 Bad Request` o `409 Conflict` en el registro de usuario

## Descripción

En la petición REST que hemos implementado en la tarea anterior, había definidas dos respuestas HTTP de error:

| Código | Descripción |
| ---- | ----------- |
| 400 | La petición HTTP no se envió con todos los parámetros en el JSON, o alguno es de tipo incorrecto |
| 409 | Ya existe un usuario registrado con el `RANDOMEXPRESSIONnew_email|email|useremail|user_emailEND` especificado |

## :package: La tarea

**Se pide** que controles estas situaciones:

* Devuelve una `JsonResponse` con _cuerpo de respuesta_ `{"error": "RANDOMEXPRESSIONMissing parameter|Missing parameter in JSON|Missing parameter in body|Missing parameter in JSON body|Missing parameter in body requestEND"}` y código `400` si _falta_ algún parámetro en el cuerpo JSON de la petición
* Devuelve una `JsonResponse` con _cuerpo de respuesta_ `{"error": "RANDOMEXPRESSIONInvalid email|Email not valid|Not valid emailEND"}` y código `400` si:
  * El `RANDOMEXPRESSIONnew_email|email|useremail|user_emailEND` de la petición **_no_** contiene el carácter `@`
  * **Ó** tiene una longitud inferior a RANDOMNUMBER5-10END caracteres
* Devuelve una `JsonResponse` con _cuerpo de respuesta_ `{"error": "RANDOMEXPRESSIONAlready registered|User already registered|Email already registered|An user with that email already existsEND"}` y código `409` si _ya existe_ un usuario registrado con el `RANDOMEXPRESSIONnew_email|email|useremail|user_emailEND` dado.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
