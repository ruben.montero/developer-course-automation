Una tabla de sesiones

## :books: Resumen

* Crearemos `UserSession` en `model.py` que represente las sesiones de los usuarios
* Efectuaremos las migraciones a la base de datos

## Descripción

Crearemos un nuevo _modelo_ que represente las _sesiones_ de los usuarios de nuestro `IdeAPI`.

### ¿Qué es una sesión?

El intervalo de tiempo durante el cual un usuario _participa_ en una aplicación, sea web o nativa.

Nosotros representamos cada _sesión_ como una fila de una tabla. Cada usuario puede tener varias sesiones. Por lo tanto, podrán hacer _login_ desde distintos ordenadores y abrir y cerrar sesiones independientemente. Cada una de ellas es una fila separada. La clase asociada es...

### `UserSession`

```mermaid
graph LR;

A[CustomUser]-->|Contiene varias|B[UserSession];
```

## :package: La tarea

**Editemos** `IdeAPI/idearestDEVELOPERNUMBERapp/models.py` y añadamos:

```python
# ...

class UserSession(models.Model):
    RANDOMEXPRESSIONuser|author|person|creatorEND = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    token = models.CharField(unique=True, max_length=RANDOMEXPRESSION20|25|30|35|40|45|50END)
```
**Migra** tu _modelo_ a una base de datos real **ejecutando** estos dos comandos desde la carpeta `IdeAPI/` que contiene `manage.py`:

```
python manage.py makemigrations
python manage.py migrate
```

¡Tarea terminada!

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
