Una tabla de usuarios

## :books: Resumen

* Crearemos una clase en `model.py` que represente los usuarios
* Efectuaremos las migraciones a la base de datos

## Descripción

Como bien sabemos, un _modelo_ es una clase que, según el [mapeo objeto-relacional](https://programarfacil.com/blog/que-es-un-orm/) representa una tabla SQL de una base de datos.

Crearemos un nuevo _modelo_ que represente a los _usuarios_ de nuestro `IdeAPI`.

### `CustomUser`

```mermaid
graph LR;

A[CustomUser]
```

#### ¿Y por qué no `User`?

Entraría en conflicto con el `User` que ya crea Django por defecto desde la _app_ `django.contrib.admin`. Nosotros crearemos un modelo nuevo **desde cero**. ¡Es la mejor forma de aprender!

## :package: La tarea

Si abres `IdeAPI/idearestDEVELOPERNUMBERapp/models.py` verás:

```python
from django.db import models

# Create your models here.
```

Nuestra clase `CustomUser` **debe** indicar `models.Model` entre los _paréntesis_ (`( )`), que es la sintaxis de Python para indicar [herencia](https://ellibrodepython.com/herencia-en-python).

```python
from django.db import models

class CustomUser(models.Model):
    # ...
```

### Tres `VARCHAR`

Especificamos _tres_ atributos de tipo `models.CharField`, que es la _clase_ Django correspondiente a un `VARCHAR`:

```python
from django.db import models

class CustomUser(models.Model):
    RANDOMEXPRESSIONemail|useremail|e_mail|user_emailEND = models.CharField(max_length=RANDOMEXPRESSION200|210|220|230|240|250|260|270|280|290|300END, unique=True)
    username = models.CharField(max_length=RANDOMEXPRESSION200|220|240|260|280|300END)
    encrypted_password = models.CharField(max_length=120)
```

Observa que `RANDOMEXPRESSIONemail|useremail|e_mail|user_emailEND` es `unique=True, pero ``username` no lo es.

Es decir, los usuarios se identificarán por su e-mail, pero dos usuarios distintos podrán tener el mismo `username`.

### ¿Por qué?

Pues... Porque podemos. Es nuestra decisión de diseño.

Ahora, **migra** tu _modelo_ a una base de datos real **ejecutando** estos dos comandos desde la carpeta `IdeAPI/` que contiene `manage.py`:

```
python manage.py makemigrations
python manage.py migrate
```

¡Tarea terminada!

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
