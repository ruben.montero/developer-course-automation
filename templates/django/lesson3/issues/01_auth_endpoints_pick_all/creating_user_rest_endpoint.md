Un endpoint para crear usuarios

## :books: Resumen

* Definiremos cómo es la petición REST mediante la que soportaremos el registro de nuevos usuarios
* Crearemos una función en `endpoints.py` que procese dicha petición
* Leerá del cuerpo del POST los atributos del nuevo usuario
* Codificará (_hasheará_) la contraseña antes de almacenarla
* Guardará el nuevo usuario en la base de datos y enviará una respuesta HTTP de éxito si todo va bien

## Descripción

Nuestro API REST soportará la siguiente operación REST:

### POST /v1/users

**Parámetros:**

| Nombre | En | Descripción | ¿Obligatorio? | Tipo |
| ---- | ---------- | ----------- | -------- | ---- |
| RANDOMEXPRESSIONnew_email|email|useremail|user_emailEND | body (json) | Correo electrónico | Sí | string |
| RANDOMEXPRESSIONnew_username|username|nameEND | body (json) | Nombre de usuario | Sí | string |
| RANDOMEXPRESSIONnew_password|password|user_passwordEND | body (json) | Contraseña del usuario | Sí | string |

**Ejemplo de cuerpo de petición:**

```json
{
  "RANDOMEXPRESSIONnew_email|email|useremail|user_emailEND": "testemail@test.email.co.uk",
  "RANDOMEXPRESSIONnew_username|username|nameEND": "Bill Doe",
  "RANDOMEXPRESSIONnew_password|password|user_passwordEND": "asdf1234"
}
```

**Respuestas:**

| Código | Descripción |
| ---- | ----------- |
| 201 | El usuario se ha creado con éxito |
| 400 | La petición HTTP no se envió con todos los parámetros en el JSON, o alguno es de tipo incorrecto |
| 409 | Ya existe un usuario registrado con el `RANDOMEXPRESSIONnew_email|email|useremail|user_emailEND` especificado |

------------------------------------------------------------------------


Este `curl` servirá para registrar a un usuario:

```
curl -X POST http://localhost:8000/v1/users -d "{\"RANDOMEXPRESSIONnew_email|email|useremail|user_emailEND\": \"testemail@test.email.co.uk\", \"RANDOMEXPRESSIONnew_username|username|nameEND\": \"Bill Doe\", \"RANDOMEXPRESSIONnew_password|password|user_passwordEND\": \"asdf1234\"}"
```

¡Trabajemos en nuestro _endpoint_!

## :package: La tarea

### `urls.py`

**Añadamos** una nueva línea de _mapeo_:

```python
urlpatterns = [
    path('admin/', admin.site.urls),
    path('v1/health', endpoints.health_check),
    path('v1/users', endpoints.users), # Asumimos que existe la función users
]
```

### `endpoints.py`

En `endpoints.py`, **añadamos** la función `users`, que sólo soportará `POST`:

```python
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def users(request):
    if request.method != 'POST':
        return JsonResponse({'error': 'RANDOMEXPRESSIONUnsupported HTTP method|HTTP method not supported|HTTP method unsupportedEND'}, status=405)
    # ...
```

Ahora, _procesaremos_ los valores que esperamos del cuerpo JSON de la petición, y _guardaremos_ el nuevo usuario en base de datos:

```python
import json

@csrf_exempt
def users(request):
    if request.method != 'POST':
        return JsonResponse({'error': 'RANDOMEXPRESSIONUnsupported HTTP method|HTTP method not supported|HTTP method unsupportedEND'}, status=405)
    body_json = json.loads(request.body)
    json_username = body_json['RANDOMEXPRESSIONnew_username|username|nameEND']
    json_email = body_json['RANDOMEXPRESSIONnew_email|email|useremail|user_emailEND']
    json_password = body_json['RANDOMEXPRESSIONnew_password|password|user_passwordEND']
    # De momento no controlaremos peticiones de cliente erróneas. Sólo programamos el happy path
    user_object = CustomUser(RANDOMEXPRESSIONemail|useremail|e_mail|user_emailEND=json_email, username=json_username, encrypted_password=json_password)
    user_object.save()
    return JsonResponse({"RANDOMEXPRESSIONregistered|created|success|is_registered|is_created|is_successEND": True}, status=201) # Devolvemos al cliente un 201 Created
```

### `encrypted_password`

¿Qué hay de...

```
encrypted_password=json_password
```

¡No está bien! ¡Deberíamos _encriptar_ la contraseña antes de guardarla!

## **Contraseñas** y **bases de datos**

**Nunca** debemos almacenar las contraseñas de los usuarios _en texto claro_ dentro de la base de datos. Nuestra base de datos puede ser atacada y la información robada.

### Una solución: _Hashear_ contraseñas

Una [función _hash_](https://es.wikipedia.org/wiki/Funci%C3%B3n_hash) transforma un _dato de entrada_ en un _valor de salida_. Existen varias funciones _hash_ populares como [MD5](https://es.wikipedia.org/wiki/MD5) ó [SHA256](https://academy.bit2me.com/sha256-algoritmo-bitcoin/).

Las funciones _hash_ tienen las siguientes [propiedades](https://www.analyticslane.com/2018/06/22/propiedades-de-las-funciones-de-hash-criptograficas/):

* Determinista: Para una entrada `N`, siempre produce la misma salida `f(N)`
* Computacionalmente eficiente: ¡Que el ordenador lo haga rapidito!
* No reversible: No debe poder obtenerse `N` a partir de `f(N)`. Si eso es posible, no nos sirve de nada
* Propiedad de avalancha: _Pequeños_ cambios en la entrada `N` deben traducirse en _enormes_ cambios en la salida `f(N)`
* Resistente a colisiones: Hablamos de _robustez_ de la función _hash_. Tiene que ver con la [paradoja del cumpleaños](https://tech.gobetech.com/49369/cual-es-una-explicacion-intuitiva-del-ataque-de-cumpleanos-en-criptografia.html)

| N | SHA-1(N) |
|---|------|
| hola | 99800b85d3383e3a2fb45eb7d0066a4879a9dad0    |
| ola | 793f970c52ded1276b9264c742f19d1888cbaf73    |
| my_password | 5eb942810a75ebc850972a89285d570d484c89c4    |

### Pero _hashear_ contraseñas es insuficiente... :cry:

_Hashear_ una contraseña antes de almacenarla **no** es suficiente. Existe un ataque denominado [**rainbow table attack**](https://www.geeksforgeeks.org/understanding-rainbow-table-attack/) que _crackea_ con sencillez contraseñas guardadas así, mediante el uso de tablas de _hashes_ precalculados.

### Una mejor solución: _Saltear_ y _hashear_ contraseñas :smile:

[`salt`](https://cryptography.fandom.com/wiki/Salt_(cryptography) es como llamamos a un _churro aleatorio de bytes_ generado en el momento de almacenar la contraseña:

1. El `salt` se concatena a la contraseña en _claro_
2. Se _hashea_ la concatenación de `salt+contraseña`
3. Se almacena toda la información en un único campo

Esto imposibilita ataques [rainbow table attack](https://www.geeksforgeeks.org/understanding-rainbow-table-attack/), puesto que el `salt` se genera aleatoriamente.

### ¿Y qué función _hash_ usaremos nosotros?

[`BCrypt`](https://www.topcoder.com/thrive/articles/bcrypt-algorithm), un algoritmo de _hash_ basado en [Blowfish](https://es.wikipedia.org/wiki/Blowfish).

## Terminando la tarea... (:package:)

**Modifica** tu función `users` en `endpoints.py` **así**:

```diff
+import bcrypt

@csrf_exempt
def users(request):
    if request.method != 'POST':
        return JsonResponse({'error': 'RANDOMEXPRESSIONUnsupported HTTP method|HTTP method not supported|HTTP method unsupportedEND'}, status=405)
    body_json = json.loads(request.body)
    json_username = body_json['RANDOMEXPRESSIONnew_username|username|nameEND']
    json_email = body_json['RANDOMEXPRESSIONnew_email|email|useremail|user_emailEND']
    json_password = body_json['RANDOMEXPRESSIONnew_password|password|user_passwordEND']
    # De momento no controlaremos peticiones de cliente erróneas. Sólo programamos el happy path
+   salted_and_hashed_pass = bcrypt.hashpw(json_password.encode('utf8'), bcrypt.gensalt()).decode('utf8')
-   user_object = CustomUser(RANDOMEXPRESSIONemail|useremail|e_mail|user_emailEND=json_email, username=json_username, encrypted_password=json_password)
+   user_object = CustomUser(RANDOMEXPRESSIONemail|useremail|e_mail|user_emailEND=json_email, username=json_username, encrypted_password=salted_and_hashed_pass)
    user_object.save()
    return JsonResponse({"RANDOMEXPRESSIONregistered|created|success|is_registered|is_created|is_successEND": True}, status=201) # Devolvemos al cliente un 201 Created
```

Prueba a **registrar** un usuario efectuando el `curl` indicado al inicio.

¿Tienes éxito?

¿Cómo se ve el campo `encrypted_password` si inspeccionas la base de datos con `sqlite3.exe`?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
