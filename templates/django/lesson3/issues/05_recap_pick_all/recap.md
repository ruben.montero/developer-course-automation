¡Trabajo terminado!

## :trophy: ¡Felicidades!

Has realizado las tareas del _sprint_ y has validado tu trabajo con los _tests_. ¡Enhorabuena!

<img src="python-sessions/docs/sunset.jpg" width=500 />

Has...

* Creado un proyecto IdeAPI, con una _app_
* Creado un modelo `CustomUser` que almacenaba `RANDOMEXPRESSIONemail|useremail|e_mail|user_emailEND`, `username` y `encrypted_password`
* Comprendido qué es una [función _hash_](https://es.wikipedia.org/wiki/Funci%C3%B3n_hash) (como SHA256, por ejemplo) y sus propiedades: Determinista, computacionalmente eficiente, no reversible, propiedad de avalancha y resistencia a colisiones
* Implementando un _endpoint_ REST que permite un `POST` para registrar usuarios
* Creado un modelo `UserSession` que almacena sesiones de usuarios en un campo `token`
* Implementado un _endpoint_ REST que permite un `POST` para iniciar sesión. Este _endpoint_ genera un token criptográficamente seguro con `bcrypt` y (1) lo guarda como una `UserSession` y (2) se lo devuelve al cliente en la respuesta HTTP
* Usado el comando `python manage.py createsuperuser` para crear un superusuario Django
* Creado los modelos `Category`, `Idea` y `Comment`
* Escrito `admin.site.register(...)` con cada modelo en `admin.py` para permitir manipularlos desde el panel de administración Django
* Escrito _endpoints_ `GET` y `POST` para recuperar y publicar Categorías, Ideas y Comentarios
* Controlado, en los _endpoints_ `POST`, la cabecera cliente `Api-Session-Token`. Si viaja con un `token` válido (asociado a una `UserSession` existente) considerábamos la petición _autenticada_
* Creado una API REST sencilla... ¡Con tu lista de regalos para Navidad!
* Instalado la base de datos NoSQL eXist
* Escrito una XQuery sencilla que hacía un Hola Mundo
* Comprendido lo básico sobre consultas For, Let, Where, Order, Return (FLOWR)
* Escrito una XQuery más compleja que recuperaba datos de un archivo XML de sesiones de una biblioteca
