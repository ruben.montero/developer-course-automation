Navidad

## :books: Resumen

* Crearemos un nuevo proyecto `XmasAPI` con un sencillo _endpoint_ `GET`

## Descripción

¿Has escrito tu carta a Papá Noel?

Y en caso de que lo hayas hecho, ¿a qué dirección la quieres mandar?

Es posible que Papá Noel no la reciba.

### ¿Qué hacemos?

¡Vamos a implementar un _endpoint_ REST!

Así, Papá Noel en cuestión de milisegundos podrá enviar un `GET` HTTP y saber qué queremos.

## :package: La tarea

**Se pide** que:

* **Crees** un nuevo proyecto Django con `django-admin` dentro de la carpeta `apis/` del repositorio. Se llamará `XmasAPI`
* **Añadas** una `app` con el nombre que quieras, y modifiques los archivos que creas oportuno para que al ejecutar `python manage.py runserver` se sirva el siguiente _endpoint_:

### GET /v1/RANDOMEXPRESSIONwishlist|giftsEND

**Parámetros:**

_(Ninguno)_

**Respuestas:**

| Código | Descripción |
| ---- | ----------- |
| 200 | Se devuelve la respuesta con claves JSON como en el ejemplo a continuación |

**Ejemplo de cuerpo de respuesta (`200`):**

```json
{
    "RANDOMEXPRESSIONmensaje|comentario|saludoEND": "Hola, Papá Noel, he sido una persona excelente, trabajora, estudiosa, amable y buena. Te envío mi lista de regalos. Más vale que no falte ni uno o te vas a enterar"
    "RANDOMEXPRESSIONregalos|deseos|solicitudesEND": [
        "PlayStation 8",
        "Ordenador i9 con gráfica potente",
        "Yoyó"
    ]
}
```

### Notas

* El **contenido** de la respuesta es libre. Basta con que tenga:
    * Un mensaje, bajo la clave `"RANDOMEXPRESSIONmensaje|comentario|saludoEND"`
    * Una lista de _string_, bajo la clave `"RANDOMEXPRESSIONregalos|deseos|solicitudesEND"` 
* No hace falta que generes ningún modelo ni ninguna base de datos

### ¡Veríficalo!

Lanza el servidor y visita http://localhost:8000/v1/RANDOMEXPRESSIONwishlist|giftsEND

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.


