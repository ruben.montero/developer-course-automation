Una idea para un API REST... De ideas

## :books: Resumen

* Presentaremos la idea de `IdeApp`
* Expondremos cómo es el _backend_ necesario, en comparación con `DashboardAPI`
* Crearemos un nuevo proyecto Django `IdeAPI` dentro de la carpeta `apis`
* Probaremos que la página de ejemplo funciona
* _(Y como siempre, subiremos los cambios al repositorio)_

## Descripción

Ha llegado el momento de retomar nuestro _framework_ favorito de desarrollo web escrito en Python: [Django](http://www.djangoproject.com/).

### ¡Hurra!

Vamos a centrarnos en la creación de un API REST que dará soporte a `IdeApp`.

>>>
¿Alguna vez has tenido una idea y has necesitado _feedback_ para mejorarla?

¿Encuentras difícil perfeccionar tus ideas y trabajar en ellas porque no hay una voz crítica que las comente?

¿Quieres avalar la calidad de tus ideas con la opinión de un montón de gente desconocida?
>>>

¡`IdeApp` es lo que deseas!

Consistirá en una aplicación donde:

* Los usuarios pueden registrarse y _loguearse_
* Dentro de una serie de **categorías** prefijadas, podrán _crear_ **ideas** (explicadas como texto plano)
* Podrán visitar y **comentar** sus propias ideas y las de los demás

Vamos a trabajar en el API REST que confiere operaciones de acceso a la base de datos desde _cualquier plataforma_.

### Se parece a `DashboardAPI`... :thinking:

Efectivamente.

Había tres _modelos_ principales en `DashboardAPI`:

```mermaid
graph LR;

A[Dashboard]-->|Contiene varios|B[Question]
B-->|Contiene varios|C[Answer]
```

En nuestra `IdeAPI` también tendremos tres _modelos_ de la misma naturaleza:

```mermaid
graph LR;

A[Category]-->|Contiene varios|B[Idea]
B-->|Contiene varios|C[Comment]
```

Las _**Category**_ son prefijadas y sólo los administradores las manipulamos en la base de datos. Los usuarios tienen permitido añadir _**Idea**_ y _**Comment**_.

Adicionalmente, vamos a trabajar con...

### Autenticación de usuarios

Y necesitaremos estos dos _modelos_:

```mermaid
graph LR;

A[CustomUser]-->|Contiene varios|B[UserSession];
```

## :package: La tarea

En primer lugar, usaremos un terminal (cmd.exe) para cambiar el directorio activo hasta la ubicación de nuestro repositorio local usando `cd`.

Luego:

```
git pull
```

...traerá los cambios de remoto a local. Contienen correcciones del proyecto anterior y **el esqueleto** del **nuevo proyecto**.

Se encontrará en **una carpeta llamada `python-sessions`**.

Usa tu [IDE](https://www.redhat.com/es/topics/middleware/what-is-ide) favorito para abrir...

### `python-sessions/`

Luego, **crea** un proyecto Django llamado `IdeAPI` **dentro de la carpeta `apis/`**.

```
django-admin startproject IdeAPI
```

Como resultado, obtendremos una nueva carpeta.

```
- apis/
   |
   |- .gitkeep
   |
   |- IdeAPI/

- docs/

- tests/
```

### Dependencias

¡Ojo! Si tu IDE no te advirtió sobre instalar las dependencias de `requirements.txt` al abrir `python-sessions/`, **instálalas** manualmente desde un cmd.exe:

```
pip install django
pip install requests
pip install bcrypt
```

## Lanzar `IdeAPI`

Nuestro nuevo proyecto está vacío, pero podemos probarlo.

```
cd IdeAPI
python manage.py runserver
```

Como ya sabes, también puedes **añadir una configuración** en PyCharm para **ejecutarlo** cómodamente desde el IDE.

Si la salida de ejecución es la esperable...

```
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).

You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
July 08, 2022 - 09:42:43
Django version 4.0.5, using settings 'SimpleAPI.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CTRL-BREAK.
```

...y http://localhost:8000 muestra la página de ejemplo...

**¡...enhorabuena!** Has puesto en marcha tu proyecto inicial.

Recuerda que puedes detenerlo con CTRL+C.

### :play_pause: Lanza el _test_

Prueba el _test_ asociado a la tarea. Lo ejecutaremos siguiendo los pasos de arriba, y esta vez lanzando `TESTFILENAME.py`.

Si ves algo como...

```
Ran 1 test in 0.003s

OK
```

¡Tarea terminada!

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

No está de más visitar la página de GitLab y verificar que el _commit_ se ha subido

