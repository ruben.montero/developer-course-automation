Interludio: Repasemos ORM

## :books: Resumen

* Repasaremos _qué_ son los _endpoint_ de WallAPI que hemos implementado
* Propondremos un ejercicio de repaso de teoría

## Descripción

Hemos creado un proyecto Django que está, mediante el _mapeo_ objeto-relacional (ORM) trabajando contra una base de datos SQL como esta:

* `Entry`:

id|title|content|publication_date
--|-----|-------|----------------
1|Primera pintada en el muro|¡Ahí va esta obra de arte!|2022-07-13 10:25:00
2|Pole|Espero no haber fallado en hacer la primera publicación|2022-07-13 10:25:01
...|...|...|...

* `Comment`:

id|content|entry
--|-------|--------
1|¡Pues sí que fallaste!|2
2|Ohhhh... Sí que fallaste, lo siento|2
3|Bonita primera pintada :)|1

Las dos clases Python, `Entry` y `Comment` son nuestra manera de trabajar sobre las respectivas tablas. Estas clases se conocen como _modelos_[^1].

En nuestro API REST existe los siguientes _endpoints_, que puedes verificar tras lanzar tu servidor con `python manage.py runserver`:

* **1)** Obtener todas las entradas (`GET` http://localhost:8000/entries)
* **2)** Crear una nueva entrada (`POST` http://localhost:8000/entries) _(Recuerda que para enviar un POST puedes lanzar el siguiente comando desde un cmd.exe)_

```
curl http://localhost:8000/entries -d "{\"new_title\": \"Una  entrada de prueba\", \"new_content\": \"¡Solamente es una prueba!\"}"
```

* **3)** Obtener los comentarios de una entrada (e.g.: `GET` http://localhost:8000/entries/1/comments para los comentarios de la entrada `1`)
* **4)** Crear un nuevo comentario asociado a una entrada (e.g.: `POST` http://localhost:8000/entries/1/comments) _(Para enviar un POST podrías lanzar lo siguiente)_

```
curl http://localhost:8000/entries/1/comments -d "{\"new_content\": \"Un comentario de prueba\"}"
```

### ¿Para qué el API REST?

Recordemos: ¡Los API REST son las fachadas más habituales mediante las cuales, un servidor, se expone a un mundo lleno de aplicaciones distribuidas (_app_ Android ó iOS, aplicación de PC, página web React,...)!
 
## :package: La tarea

**Repasemos** las sentencias ORM típicas de Django que hemos empleado.

Para las siguientes sentencias...

| Sentencia Django                                 |
|--------------------------------------------------|
| (1) `Entry.objects.all()`                        |
| (2) `Entry(title="Ejemplo", content="Test").save()`|
| (3) `Comment.objects.filter(entry=3)`            |
| (4) `Entry.objects.get(id=4).comment_set.all()`  |
| (5) `Comment(content="Hola", entry_id=1).save()` |

¿...cuál sería el SQL en el que Django las transforma?

| SQL equivalente... ¿Cuál corresponde a cada una?                   |
|--------------------------------------------------------------------|
| (A) `INSERT INTO Comment(content, entry_id) VALUES ("Hola", 1)`    |
| (B) `SELECT * FROM Comment WHERE entry_id=3`                       |
| (C) `SELECT * FROM Entry`                                          |
| (D) `INSERT INTO Entry(title, content) VALUES ("Ejemplo", "Test")`|
| (E) `SELECT * FROM Comment WHERE entry_id=4`                       |

**Escribe** como **comentario** en esta tarea la respuesta que consideres correcta.

### :play_pause: No hay _test_

Esta tarea no es evaluable. El _test_ pasará automáticamente.

### :trophy: Por último

¡No hagas nada más! A por la siguiente tarea.

------------------------------------------------------------------------

[^1]: La palabra _modelo_ puede descubrirse en muchos más contextos.
