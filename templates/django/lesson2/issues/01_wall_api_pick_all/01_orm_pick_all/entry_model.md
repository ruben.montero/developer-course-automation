Ladrillos para el muro

## :books: Resumen

* Hablaremos de _mapeo objeto-relacional_ (ORM)
* Crearemos nuestro primer _modelo_ en `models.py`

## Descripción

Como ya hemos visto con anterioridad, estamos trabajando en la creación de APIs REST que _reciben_ peticiones de los usuarios y las _traducen_ a modificaciones en una base de datos:

<img src="python-django/docs/distributed-app-architecture.png" width=600/>

### ¿Otra vez SQL?

¡No! Algo nuevo.

### [_Mapeo objeto-relacional_ (ORM)](https://es.wikipedia.org/wiki/Asignaci%C3%B3n_objeto-relacional)

Es una técnica de acceso a datos implementada por _frameworks_ como [Django](http://www.djangoproject.com/), [Hibernate](https://hibernate.org/), [Laravel](https://laravel.com/) y [muchos otros](https://en.wikipedia.org/wiki/List_of_object%E2%80%93relational_mapping_software)...

Consiste en usar **clases** de _programación orientada a objetos (POO)_, como **sinónimos** de las **tablas** de una base de datos SQL.

En nuestro **WallAPI** queremos tener _dos_ tablas en la base de datos _relacional_.

_Podrían_ verse, por ejemplo, así:

* `Entry`: Contiene _Entradas_ publicadas en el muro

id|title|content|publication_date
--|-----|-------|----------------
1|Primera pintada en el muro|¡Ahí va esta obra de arte!|2022-07-13 10:25:00
2|Pole|Espero no haber fallado en hacer la primera publicación|2022-07-13 10:25:01
...|...|...|...

* `Comment`: Contiene _Comentarios_ de las entradas.

id|content|entry
--|-------|--------
1|¡Pues sí que fallaste!|2
2|Ohhhh... Sí que fallaste, lo siento|2
3|Bonita primera pintada :)|1

Pero... ¡No vamos a crear _tablas_! En su lugar, crearemos _clases_. Cada una _representa_ una tabla:

```python
class Entry:
    # ...

class Comment:
    # ...
```

### ¿Qué ventaja tiene el _mapeo objeto-relacional_?

Modificaremos la base de datos con código orientado a objetos únicamente, sin usar SQL.

Por ejemplo, **crear** una nueva _instancia_ de `Comment` será como hacer un SQL `INSERT`.

¡Trabajaremos contra una base de datos SQL sin escribir una línea de SQL!

## [`models.py`](https://docs.djangoproject.com/en/4.0/topics/db/models/)

Cada una de esas _clases_ de las que hemos hablado arriba se denominan **modelo**.

En un proyecto Django, estas clases se encuentran **todas** en el archivo `models.py` de cada _app_.

Si abres `WallAPI/wallrestDEVELOPERNUMBERapp/models.py` verás:

```python
from django.db import models

# Create your models here.
```

La documentación cuenta con un [ejemplo muy descriptivo](https://docs.djangoproject.com/en/4.0/topics/db/models/#quick-example) de cómo empezar.

## :package: La tarea

Comenzaremos creando la clase `Entry` dentro de `WallAPI/wallrestDEVELOPERNUMBERapp/models.py`. **Debe** indicar `models.Model` entre los _paréntesis_ (`( )`), que es la sintaxis de Python para indicar [herencia](https://ellibrodepython.com/herencia-en-python).

```python
from django.db import models

class Entry(models.Model):
    # ...
```

### Los dos primeros `VARCHAR`

Comenzaremos especificando _dos_ primeros atributos[^1] de tipo `models.CharField`, que es la _clase_ Django correspondiente a un `VARCHAR`:

```python
from django.db import models

class Entry(models.Model):
    title = models.CharField(max_length=RANDOMEXPRESSION100|120|140|160|180|200END)
    content = models.CharField(max_length=RANDOMEXPRESSION2000|2500|3000|3500|4000|4500|5000|5500|6000END)
```

¡Y completemos nuestro `Entry` con un último _atributo_ para la **fecha de publicación**!

```python
from django.db import models

class Entry(models.Model):
    title = models.CharField(max_length=RANDOMEXPRESSION100|120|140|160|180|200END)
    content = models.CharField(max_length=RANDOMEXPRESSION2000|2500|3000|3500|4000|4500|5000|5500|6000END)
    publication_date = models.DateTimeField(auto_now=True)
```

Con el parámetro `auto_now=True` indicamos que, cuando se **guarde** una nueva instancia de `Entry`, queremos que la fecha/hora del servidor se asigne automáticamente.

Si quisiéramos almacenar únicamente una fecha (sin hora), podríamos usar `models.DateField`

### Un momento, ¿y el `id`?

En Django **no necesitamos** indicar el `id` de las tablas manualmente.

Django se encargará de que haya un `id` tipo numérico autoincremental, **automáticamente**.

### Modelo `Entry` terminado

**¡Enhorabuena!** Has creado tu primer model ORM con Django.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------------------------------------

[^1]: A diferencia de los _atributos de instancia_ que se declaran en `__init__`, en Django emplearemos _atributos de clase_ que se declaran fuera. Igualmente, en Django, cada _instancia_ de modelo tendrá sus valores correctos en sus atributos.
