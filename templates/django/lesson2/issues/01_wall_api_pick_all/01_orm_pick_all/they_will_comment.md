Comentarios para los ladrillos

## :books: Resumen

* Crearemos nuestro segundo _modelo_ en `models.py`
* Recordaremos qué es una _clave foránea_ en SQL

## :package: La tarea

**Añadamos** `Comment` a nuestro `models.py`.

Constará inicialmente de un campo tipo VARCHAR que reflejará el _contenido_ (`content`) del comentario:

```python
from django.db import models

class Entry(models.Model):
    title = models.CharField(max_length=RANDOMEXPRESSION100|120|140|160|180|200END)
    content = models.CharField(max_length=RANDOMEXPRESSION2000|2500|3000|3500|4000|4500|5000|5500|6000END)
    publication_date = models.DateTimeField(auto_now=True)

class Comment(models.Model): # Nuevo modelo
    content = models.CharField(max_length=RANDOMEXPRESSION1000|1100|1200|1300|1400|1500|1600|1700|1800|1900END)
```

Ahora falta que cada _comentario_ (`Comment`) pertenezca a una _entrada_ (`Entry`)

### ¿Qué necesitamos?

Una _clave foránea_. Nuestras tablas tienen una relación **1:N**.

Cada `Comment` pertenece a **un** `Entry` y cada `Entry` puede tener **varios** `Comment`.

¿Puedes decir a qué `Entry` pertenece cada `Comment` en las tablas de ejemplo del ejercicio anterior?

### [`models.ForeignKey`](https://docs.djangoproject.com/en/4.0/ref/models/fields/#django.db.models.ForeignKey)

Es el _tipo de dato_ que ofrece Django para representar un atributo _clave foránea_.

Debemos especificar _a qué **clase**_ hace referencia, y además _cómo se comportará si hay un borrado_.

**Añádelo** así:

```python
class Comment(models.Model):
    content = models.CharField(max_length=RANDOMEXPRESSION1000|1100|1200|1300|1400|1500|1600|1700|1800|1900END)
    entry = models.ForeignKey(Entry, on_delete=models.CASCADE) # Clave foránea a Entry
```

**¡Segundo modelo listo!**

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
