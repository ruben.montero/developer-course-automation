Migraciones

## :books: Resumen

* Aprenderemos cómo generar una base de datos con `makemigrations` y `migrate`
* Cacharrearemos con la base de datos SQLite usando sqlite3.exe

## Descripción

Tenemos dos modelos muy bonitos y relucientes en `models.py`.

Pero, ¿dónde está la base de datos?

### Digno de recordar: Fichero de configuración

Si abres `settings.py` verás una parte que indica:

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
```

**No** vamos a modificarlo, pero destacaremos que **ahí** está la configuración de la base de datos. Podríamos configurar una base de datos MySQL, PostreSQL...

La nuestra es SQLite. Concretamente, se ubica en el archivo `db.sqlite3`.

### ¿Migraciones?

Las [migraciones](https://docs.djangoproject.com/en/4.0/topics/migrations/) son la forma que tiene Django de _propagar_ los cambios de nuestro `models.py` a la base de datos.

Django lee `models.py` y realiza cambios SQL en las tablas de la base de datos (o las crea desde cero).

## :package: La tarea

**Abre** un terminal y navega hasta `WallAPI/`. Lanzaremos el comando `makemigrations` que es responsable de crear nuevas _migraciones_:

```
python manage.py makemigrations
```

Esto ha generado **un nuevo** fichero en la carpeta `migrations/`. Ese fichero contiene la información de los _cambios en `models.py`_ procesada por Django.

Ahora, **ejecuta**:

```
python manage.py migrate
```

Esto _leerá_ el contenido de `migrations/` y **ejecutará** las sentencias SQL necesarias para crear las tablas en la base de datos.

### ¡Migraciones realizadas!

¡Excelente!

### ¿El proceso inverso sería posible?

Sí. Con `python manage.py inspectdb`:

<img src="python-django/docs/migrations.png" />

No lo pondremos en práctica.

### Vale. Entonces... ¿Con `makemigrations` y `migrate` hemos creado tablas SQL?

Sí. Verifiquémoslo.

**Abre** `sqlite3.exe`, que está en la carpeta `python-django/` de tu repositorio.

_(Debes abrirlo desde el explorador de archivos de Windows, **no** desde PyCharm)_

Si haces **doble _click_** sobre él, se lanzará un terminal como este:

```
SQLite version 3.39.0 2022-06-25 14:57:57
Enter ".help" for usage hints.
Connected to a transient in-memory database.
Use ".open FILENAME" to reopen on a persistent database.
sqlite>
```

Vamos a ejecutar `.open` para abrir la base de datos del proyecto Django:

```
sqlite> .open apis/WallAPI/db.sqlite3
```

Este par de comandos ayudarán a mejorar la legibilidad de los resultados de las consultas:

```
sqlite> .mode column
sqlite> .headers on
```

Si ejecutamos `.tables` veremos las tablas de la base de datos. En concreto las interesante son las correspondientes a los modelos que hemos creado nosotros.

```
sqlite> .tables
```

¿Distingues **cuáles** son?

### Un sencillo `SELECT`

Podemos comprobar que dichas tablas están vacías haciendo `SELECT`:

```
sqlite> SELECT * FROM wallrestDEVELOPERNUMBERapp_entry;
sqlite> SELECT * FROM wallrestDEVELOPERNUMBERapp_comment;
sqlite>
```

Como ves no hay resultados, pero las tablas están ahí. Puedes comprobar su estructura con `.schema wallrestDEVELOPERNUMBERapp_entry` y `.schema wallrestDEVELOPERNUMBERapp_comment`

### Un sencillo `INSERT`

¡Probemos a agregar una fila a `wallrestDEVELOPERNUMBERapp_entry`!

**Ejecuta**:

```
sqlite> INSERT INTO wallrestDEVELOPERNUMBERapp_entry (title, content, publication_date)
   ...> VALUES ("RANDOMEXPRESSIONHola a todos|Hola mundo|HolaEND", "RANDOMEXPRESSIONEste es el primer ladrillo|Este es el primer ladrillo en el muro|Un primer ladrillo|Un primer ladrillo en el muroEND", datetime('now'));
```

Después de esto, repite el `SELECT`. ¿Ves los resultados?

Puedes teclear `.exit` cuando quieras para salir de `sqlite3.exe`.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
