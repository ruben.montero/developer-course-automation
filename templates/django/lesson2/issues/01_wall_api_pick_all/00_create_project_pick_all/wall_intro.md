El muro

## :books: Resumen

* Presentaremos la idea de un nuevo proyecto
* Crearemos un nuevo proyecto con una _app_ dentro de la carpeta `apis/`

## Descripción

Nuestro viaje de la mano de **SimpleAPI** ha llegado a su fin.

Vamos a comenzar un proyecto donde pondremos en práctica los conceptos aprendidos y, adicionalmente, contará con una base de datos donde se almacenarán datos de los usuarios.

### ¿Qué datos?

_The Wall_, además de ser [un éxito de Pink Floyd de 1979](https://www.youtube.com/watch?v=fvPpAPIIZyo), será la idea central de nuestra nueva API REST.

Los clientes HTTP podrán:

* Publicar una nueva _Entrada_ (`Entry`) anónima.
* Leer las _Entradas_ del muro.
* Publicar un nuevo _Comentario_ (`Comment`) anónimo. Cada _Comentario_ está asociado a una _Entrada_.
* Leer los _Comentarios_ de cada _Entrada_.

## :package: La tarea

Usando un terminal (cmd.exe), posiciónate en la carpeta `apis/` de tu repositorio y crea un proyecto llamado `WallAPI`:

```
django-admin startproject WallAPI
```

Después, cambiaremos a la carpeta recién creada y arrancaremos una nueva _app_ llamada `wallrestDEVELOPERNUMBERapp`:

```
cd WallAPI
python manage.py startapp wallrestDEVELOPERNUMBERapp
```

### `settings.py`

Como recodarás, es necesario que la variable `INSTALLED_APPS` del fichero `settings.py` quede como:

```python
INSTALLED_APPS = [
    'wallrestDEVELOPERNUMBERapp.apps.WallrestDEVELOPERNUMBERAppConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```

### Configuraciones en PyCharm

Recuerda que también puedes:

* _Click_ derecho en la carpeta **WallAPI** padre (la que cuelga de `apis/`) > _Mark Directory as_ > _Sources Root_, para prevenir errores de PyCharm interpretando los módulos disponibles
* _Edit configurations_ > **+** y añadir una configuración que ejecute `manage.py runserver` de **WallAPI**

### ¡Proyecto arrancado!

Ya puedes lanzar el servidor REST y verificar si se ve la página de ejemplo en `http://localhost:8000`.

Recuerda que deberás detener SimpleAPI si está siendo ejecutado.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
