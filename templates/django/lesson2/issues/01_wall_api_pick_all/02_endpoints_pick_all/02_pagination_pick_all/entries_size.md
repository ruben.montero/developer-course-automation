Otra pequeña mejora: Tamaño del resultado

## :books: Resumen

* Añadiremos soporte para el _query param_ `?size=N` en el _endpoint_ `/entries`
* Si viene, devolveremos sólo las primeras `N` _Entradas_

## Descripción

Si **todo el mundo** va a enviar _Entradas_ a nuestro **muro**... ¡Va a acabar bastante **lleno**!

Es previsible que la tabla tenga un tamaño enorme, y en ese caso `Entry.objects.order_by("-publication_date")` devolverá muchos resultados y las peticiones HTTP se volverán voluminosas. Todo funcionará más lento hasta que llegue un punto en que nadie quiera usar nuestro API REST.

Vamos a empezar permitiendo el _query param_ `?size=N` en el _endpoint_ `/entries`. 

* Si viene, sólo devolvemos las `N` primeras _Entradas_ en la respuesta JSON

### ¿Cómo?

Lo único que debemos recordar es cómo acceder a _parte_ de un _array_ en Python.

Si tenemos:

```python
guitarist = ["Paco", "de", "Lucía"]
```

...entonces:

```python
# Imprime ["Paco", "de"]
print(guitarist[:2])
```

### ¿Y se puede usar el operador `[:N]` en un `QuerySet[]` de Django?

¡Sí!

## :package: La tarea

Vayamos a `all_entries` en `endpoints.py` y centrémonos en la parte que procesa el `"GET"`:

```python
    # ...
    if request.method == "GET":
        all_rows = Entry.objects.order_by("-publication_date")
        json_response = []
        for row in all_rows:
            json_response.append(row.to_json())
        return JsonResponse(json_response, safe=False)
```

### Primero, ¿el cliente HTTP mandó _query param_ `?size`?

**Añade** estas líneas para recuperar el _query param_ `size` y convertirlo a `int`, devolviendo una respuesta de error si falla la conversión:

```python
    if request.method == "GET":
        size = request.GET.get("size", None)
        if size is not None:
            try:
                size = int(size)
            except ValueError:
                return JsonResponse({"error": "Wrong size parameter"}, status=400)
        # ...
        all_rows = Entry.objects.order_by("-publication_date")
        json_response = []
        for row in all_rows:
            json_response.append(row.to_json())
        return JsonResponse(json_response, safe=False)
```

Ahora, **completa** tu código con un `if` así:

```diff
-        # ...
-        all_rows = Entry.objects.order_by("-publication_date")
+        if size is None:
+            all_rows = Entry.objects.order_by("-publication_date")
+        else:
+            all_rows = Entry.objects.order_by("-publication_date")[:size]
```

**¡Listo!**

Ya puedes verificar algún ejemplo:

* http://localhost:8000/entries?size=1

Como supondrás, si no mandas el _param_ `size`, sigue funcionando como hasta ahora:

* http://localhost:8000/entries

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
