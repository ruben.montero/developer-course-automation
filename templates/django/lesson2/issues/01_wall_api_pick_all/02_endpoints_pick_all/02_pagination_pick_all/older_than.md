1 + 1 + 1 = 3 pequeñas mejoras

## :books: Resumen

* Añadiremos soporte para el _query param_ `?older_than=<fecha>` en el _endpoint_ `/entries`
* Si viene, devolveremos sólo las _Entradas_ con **fecha de publicación** anterior a `<fecha>`

## Descripción

La [paginación](https://www.moesif.com/blog/technical/api-design/REST-API-Design-Filtering-Sorting-and-Pagination/#keyset-pagination) consiste en presentar los datos al usuario poco a poco.

Usemos un ejemplo ficticio, donde un _endpoint_:

* http://algunservidor:8000/mensajes

...devuelve los siguientes datos:

|Todos los datos|
|-|
|Mensaje 1: ¡Bonito domingo para terminar la semana! _(Escrito el domingo a las 22.00)_|
|Mensaje 2: ¡Un sábado de deporte! _(Escrito el sábado a las 21.00)_|
|Mensaje 3: Hoy empieza el fin de semana :innocent: _(Escrito el viernes a las 20.30)_|
|Mensaje 4: El día de Júpiter _(Escrito el jueves a las 18.30)_|
|Mensaje 5: Media semana, ¡qué bien! _(Escrito el miércoles a las 11.05)_|
|Mensaje 6: Ni te cases ni te embarques _(Escrito el martes a las 11.00)_|
|Mensaje 7: ¡Así empezó todo! _(Escrito el lunes a las 9.00)_|

Si soportase parámetros como `size` y `older_than`, el cliente HTTP podría efectuar las siguientes peticiones para consumir los datos **poco a poco** (en páginas de 3 elementos):

* http://algunservidor:8080/mensajes?size=3

|Primera página|
|-|
|Mensaje 1: ¡Bonito domingo para terminar la semana! _(Escrito el domingo a las 22.00)_|
|Mensaje 2: ¡Un sábado de deporte! _(Escrito el sábado a las 21.00)_|
|Mensaje 3: Hoy empieza el fin de semana :innocent: _(Escrito el viernes a las 20.30)_|

* http://algunservidor:8080/mensajes?older_than=viernes20.30&size=3

|Segunda página|
|-|
|Mensaje 4: El día de Júpiter _(Escrito el jueves a las 18.30)_|
|Mensaje 5: Media semana, ¡qué bien! _(Escrito el miércoles a las 11.05)_|
|Mensaje 6: Ni te cases ni te embarques _(Escrito el martes a las 11.00)_|

* http://algunservidor:8080/mensajes?older_than=martes11.00&size=3

|Tercera página|
|-|
|Mensaje 7: ¡Así empezó todo! _(Escrito el lunes a las 9.00)_|

¿Te fijas en los valores de `size` y `older_than` en cada petición?

### Operadores especiales de `filter`

Con `filter`, como ya sabemos, podemos _recuperar sólo las filas que cumplen una condición_, por ejemplo:

```python
values = Partida.objects.filter(player="PepeDepura")
```

Y... También podemos usar _sufijos especiales_ para comparar valores _numéricos_.

* `__lt`: _less than_
* `__lte`: _less than or equal_
* `__gt`: _greater than_
* `__gte`: _greater than or equal_

Por ejemplo:

```python
values = Partida.objects.filter(puntuacion__gte=200)
```

### ¿Cómo implementar algo parecido al `older_than` de arriba?

Así:

```python
all_rows = Entry.objects.filter(publication_date__lt=<fecha>) # Entradas más antiguas que <fecha>
```

## :package: La tarea

Vayamos a la parte que procesa el `"GET"` de `all_entries` y asignemos el _query param_ `?older_than` a una **variable**:

```diff
    if request.method == "GET":
+       older_than = request.GET.get("older_than", None)
        size = request.GET.get("size", None)
        if size is not None:
            try:
                size = int(size)
            except ValueError:
                return JsonResponse({"error": "Wrong size parameter"}, status=400)
        # Y ahora, haremos la consulta
        # ...
```

**No** hace falta que efectuemos una validación. Aunque sería una buena práctica, vamos a evitar complicar demasiado el código.

Ya tenemos **4** casos que contemplar ante la pregunta:

* ¿Viene el _query param_ en la petición?

`size`|`older_than`
------|------
No|No
No|Sí
Sí|No
Sí|Sí

A continuación se da una versión de código que contempla claramente estos **4** casos y asigna el `all_rows` adecuado en cada uno:

_(Nótese cómo podemos concatenar `.filter()` y `.order_by()`)_

**Añade** lo siguiente:

```python
        # Y ahora, haremos la consulta
        if size is None:
            if older_than is None:
                all_rows = Entry.objects.order_by("-publication_date")
            else:
                all_rows = Entry.objects.filter(publication_date__lt=older_than).order_by("-publication_date")

        else:
            if older_than is None:
                all_rows = Entry.objects.order_by("-publication_date")[:size]
            else:
                all_rows = Entry.objects.filter(publication_date__lt=older_than).order_by("-publication_date")[:size]
        json_response = []
        for row in all_rows:
            json_response.append(row.to_json())
        return JsonResponse(json_response, safe=False)
```

Sin embargo, existen **muchas formas** y **más compactas** de implementar esta funcionalidad.

¿Te atreves a mejorar este código?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
