Una pequeña mejora: Ordenando el resultado

## :books: Resumen

* Sustituiremos `Entry.objects.all()` por otra alternativa que ordena las _Entradas_

## Descripción

Ahora mismo, cada vez que alguien añade una _Entrada_ a la base de datos, va a parar **al final**.

|Título|Fecha
|------|-----
|Primera publicación|Lunes
|Otra publicación|Martes
|¡Llenando el muro!|Jueves

Debemos saber que `Entry.objects.all()` devuelve **todos** los datos, pero no sabemos en _qué_ orden.

Predeciblemente, lo que le resulte más comodo a la base de datos. Es decir, **empezando por el más antiguo**.

Siendo así, el usuario tendrá que mirar **todas** las _Entradas_ hasta poder llegar a la más reciente.

¿No es común ordenar de _más reciente_ a _más antiguo_? (e.g.: _Timeline_ de Twitter, bandeja del correo electrónico,...)

### `.order_by`

En Django, podemos sustituir:

```python
all_rows = Entry.objects.all()
```

...por:

```python
all_rows = Entry.objects.order_by("-publication_date")
```

* El `-` significa orden _descendente_. Si lo quisiéramos al revés, podríamos usar `+`

## :package: La tarea

**Modifica** la parte que procesa el `"GET"` de la función `all_entries` en `endpoints.py`, así:

```diff
@csrf_exempt
def all_entries(request):
    if request.method == "GET":
-        all_rows = Entry.objects.all()
+        all_rows = Entry.objects.order_by("-publication_date")
        json_response = []
        for row in all_rows:
            json_response.append(row.to_json())
        return JsonResponse(json_response, safe=False)
    # ...
```

_(Modifica **sólo** este endpoint. **No** alteres el de los Comentarios o el test romperá)_

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
