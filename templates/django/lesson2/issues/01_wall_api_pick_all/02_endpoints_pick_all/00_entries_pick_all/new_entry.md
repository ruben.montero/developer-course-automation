POST para subir una entrada

## :books: Resumen

* Modificaremos el _endpoint_ `/entries` para que permita **añadir** datos a la base de datos si recibe un **POST**

## Descripción

Hemos visto cómo a través del _mapeo objeto-relacional_ podemos recuperar datos de una base de datos y exponerlos en una fachada REST.

Ahora permitiremos que los clientes HTTP puedan **crear** _Entradas_.

### Un momento... ¿con **qué** datos creamos la _Entrada_?

Buena pregunta.

El cliente HTTP enviará en **el cuerpo de la petición** los datos necesarios (**título** y **contenido**). Los esperaremos en formato JSON con claves como en el siguiente ejemplo:

```json
{
  "new_title": "Una entrada nueva para el muro",
  "new_content": "¡Aunque no muy relevante!"
}
```

Es decir, **al terminar nuestro _endpoint_**, el siguiente POST enviado desde **cURL** debería generar una nueva `Entry` en la base de datos:

```
curl http://localhost:8000/entries -d "{\"new_title\": \"Una  entrada nueva para el muro\", \"new_content\": \"¡Aunque no muy relevante!\"}"
```

Nuestra función `all_entries` quedó así tras la tarea anterior:

```python
def all_entries(request):
    if request.method != "GET":
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
    all_rows = Entry.objects.all()
    json_response = []
    for row in all_rows:
        json_response.append(row.to_json())
    return JsonResponse(json_response, safe=False)
```
## :package: La tarea

Retomaremos `all_entries` de `endpoints.py`:

* Añadiendo `@csrf_exempt`
* Englobando en un `if` el código anterior, asociado a `"GET"`
* Añadiendo `if request.method == "POST"`
* Devolviendo un `405` en el `else:` final

**Así**:

```python
from django.views.decorators.csrf import csrf_exempt
from .models import Entry

@csrf_exempt
def all_entries(request):
    if request.method == "GET":
        all_rows = Entry.objects.all()
        json_response = []
        for row in all_rows:
            json_response.append(row.to_json())
        return JsonResponse(json_response, safe=False)
    elif request.method == "POST":
        # Aquí procesamos un POST
        # ...
    else:
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
```

### Obtener los datos enviados por el cliente HTTP

Vamos a asignar a dos variables el contenido esperable del JSON del cliente HTTP:

```python
    elif request.method == "POST":
        # Aquí procesamos un POST
        client_json = json.loads(request.body)
        entry_title = client_json.get("new_title", None)
        entry_content = client_json.get("new_content", None)
        # ...
```

_(Recuerda `import json`)_

Podemos aprovechar a devolver un `400 Bad Request` si el cuerpo es incorrecto:

```python
    elif request.method == "POST":
        # Aquí procesamos un POST
        client_json = json.loads(request.body)
        entry_title = client_json.get("new_title", None)
        entry_content = client_json.get("new_content", None)
        if entry_title is None or entry_content is None:
            return JsonResponse({"error": "Missing new_title or new_content"}, status=400)
        # ...
```

### Crear una nueva **instancia** del **modelo** y guardarla con `.save()`

Como ya tenemos los datos (_título_ y _contenido_) con los que el cliente quiere crear (INSERT) una nueva `Entry`... ¡Hagámoslo!

**Crea** una nueva instancia (`new_entry`), y, sobre ella, invoca `new_entry.save()`[^1].

```python
    elif request.method == "POST":
        # Aquí procesamos un POST
        client_json = json.loads(request.body)
        entry_title = client_json.get("new_title", None)
        entry_content = client_json.get("new_content", None)
        if entry_title is None or entry_content is None:
            return JsonResponse({"error": "Missing new_title or new_content"}, status=400)
        # Creamos una nueva instancia de Entry
        new_entry = Entry(title=entry_title, content=entry_content)
        new_entry.save()
```

### Código HTTP 201

`201 Created` es el código de respuesta apropiado cuando hemos insertado un nuevo dato en el servidor. Añádelo **así**:

```python
    elif request.method == "POST":
        # Aquí procesamos un POST
        client_json = json.loads(request.body)
        entry_title = client_json.get("new_title", None)
        entry_content = client_json.get("new_content", None)
        if entry_title is None or entry_content is None:
            return JsonResponse({"error": "Missing new_title or new_content"}, status=400)
        # Creamos una nueva instancia de Entry
        new_entry = Entry(title=entry_title, content=entry_content)
        new_entry.save()
        return JsonResponse({"RANDOMEXPRESSIONsuccess|it_was_ok|it_was_successful|entry_added|new_entry_added|entry_created|new_entry_created|entry_saved|new_entry_savedEND": True}, status=201)
```

**¡Terminado!**

Tu primer _endpoint_ POST que sirve para guardar una nueva fila en base de datos ya está listo. Si lanzas el servidor y el `curl` de arriba, crearás una nueva fila en la tabla.

Además, los datos que añadas usando POST puedes consultarlos con un GET.

### _Endpoints_ reales

Estos _endpoints_ son perfectamente válidos para poner a andar en un servidor de producción para una aplicación.

Permitirían a **usuarios de todo el mundo** **acceder** y **actualizar** concurrentemente la misma base de datos. ¡Así es como funcionan las aplicaciones distribuidas!

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------------------------------------

[^1]: `.save()` se puede invocar sobre _cualquier_ instancia de un _modelo_ (subclase de `models.Model`), y sirve para **guardarla** en la base de datos. O sea, hace efectivo el INSERT.
