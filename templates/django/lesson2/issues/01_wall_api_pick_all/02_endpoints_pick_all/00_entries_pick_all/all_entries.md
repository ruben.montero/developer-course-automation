GET para obtener todas las entradas

## :books: Resumen

* Usaremos el método `Entry.objects.all()` para hacer un `SELECT` de todas las filas de la tabla `Entry`
* Crearemos un primer _endpoint_ que devuelve **todas** las `Entry`

## :package: La tarea

**Crea** un archivo `endpoints.py` dentro de `wallrestDEVELOPERNUMBERapp` y añade una primera función `all_entries`:

```python
from django.http import JsonResponse

def all_entries(request):
    if request.method != "GET":
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
```

Ahora, desde aquí, **accederemos** a todas las filas de la tabla `Entry` de la base de datos.

Basta con **importar** la clase `Entry` e **invocar** `Entry.objects.all()`:

```python
from django.http import JsonResponse
from .models import Entry # Es necesario el punto (.)

def all_entries(request):
    if request.method != "GET":
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
    all_rows = Entry.objects.all()
```

Como ves, devuelve un resultado que estamos almacenando en `all_rows`.

Este resultado es de un tipo especial `QuerySet []` que podemos **iterar** con un bucle `for` de Python.

**Hazlo** así:

```python
def all_entries(request):
    if request.method != "GET":
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
    json_response = []
    all_rows = Entry.objects.all()
    for row in all_rows:
        # Iteramos sobre cada fila SQL de la tabla Entry
        # ...
```

### Accediendo a los campos de cada fila

Aquí viene una de las piezas **clave** entendiendo el _mapeo objeto-relacional_ (:boom:)

* Acceder a un **atributo** de una _instancia_ de `Entry` es como acceder a un **campo** de la fila SQL asociada

### En otras palabras...

Cada `row` (dentro del bucle `for`) es una de las filas de la tabla SQL y **también** una _instancia_ de `Entry`.

Podemos acceder a `row.id`, `row.title`, `row.content` y `row.publication_date`.

### ¡Accedamos al `title`!

Vamos a guardar el título de cada `Entry` en una lista `json_response` y devolverla finalmente, **así**:

```python
def all_entries(request):
    if request.method != "GET":
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
    all_rows = Entry.objects.all()
    json_response = [] # Esta variable sirve como acumulador
    for row in all_rows:
        # Iteramos sobre cada fila SQL de la tabla Entry
        json_response.append(row.title)
    return JsonResponse(json_response, safe=False)
```

**¡Función lista!** Ya sólo falta conectarla en...

### `urls.py`

```python
from wallrestDEVELOPERNUMBERapp import endpoints

urlpatterns = [
    path('admin/', admin.site.urls),
    path('entries', endpoints.all_entries),
]
```

**¡_Endpoint_ listo!**

Lanza el servidor con `python manage.py runserver` y visita:

* http://localhost:8000/entries

¿Tiene sentido el resultado?

### Una pequeña mejora

De momento, sólo se devuelve el `title` de cada fila.

Podemos mejorarlo devolviendo **todos** los datos.

Una buena práctica es hacer esto **creando un nuevo método** en nuestra clase `Entry` para que _serialice_ sus datos.

### ¿Cómo?

**Añade** este método `to_json` a `Entry` en `models.py`:

```python
class Entry(models.Model):
    title = models.CharField(max_length=RANDOMEXPRESSION100|120|140|160|180|200END)
    content = models.CharField(max_length=RANDOMEXPRESSION2000|2500|3000|3500|4000|4500|5000|5500|6000END)
    publication_date = models.DateTimeField(auto_now=True)
    
    def to_json(self):
        return {
            "title": self.title,
            "content": self.content,
            "created": self.publication_date,
        }
```

Ahora, **reemplaza** la siguiente línea para que nuestro API REST genere una respuesta más completa, con toda la información de cada _entrada_:

```diff
def all_entries(request):
    if request.method != "GET":
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
    all_rows = Entry.objects.all()
    json_response = []
    for row in all_rows:
-        json_response.append(row.title)
+        json_response.append(row.to_json())
    return JsonResponse(json_response, safe=False)
```

Probemos de nuevo qué responde nuestro API:

* http://localhost:8000/entries

**¡Enhorabuena!** Has creado tu primer API REST que accede a datos mediante _mapeo objeto-relacional_.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
