GET de comentarios de una entrada

## :books: Resumen

* Crearemos un _endpoint_ para recuperar los comentarios de una entrada

## Descripción

Ahora mismo tenemos funcionando un _endpoint_ para recuperar **todas** las _Entradas_:

* http://localhost:8000/entries

_Podríamos_ hacer un _endpoint_ para **todos** los comentarios, pero eso **no tiene mucho sentido**.

Lo normal es que los clientes HTTP muestren al usuario una lista de _Entradas_, y cuando el usuario _abre_ una de ellas, ve los **comentarios** asociados.

Por ello, vamos a crear un _endpoint_ **GET** que devuelva todos los _Comentarios_ de una _Entrada_

Será:

```
GET /entries/<int:path_param_id>/comments
```

Como puedes ver, tiene un _path param_ que será el **ID** de la _Entrada_ que **tiene _Comentarios_**.

## :package: La tarea

Añadamos una nueva función a `endpoints.py` para que sirva nuestro propósito:

```python
def entry_comments(request, path_param_id):
    if request.method != "GET":
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
    # ...
```

Ya ves que hemos declarado el `path_param_id`.

Ahora queremos recuperar _todas las filas_ de la tabla `Comment` cuyo `entry` sea el mismo que el recibido (`path_param_id`). En otras palabras, todos los `Comment` asociados a la _Entrada_con `id=path_param_id`.

La forma más intuitiva sería lanzar una sentencia SQL como:

```sql
SELECT *
FROM wallrestDEVELOPERNUMBERapp_comment
WHERE entry=<path_param_id>
```

Para esto podemos usar **`.filter()`**, que nos permite especificar **condiciones** (`WHERE`).

Hazlo **así**:

```python
from .models import Comment # Hay que añadir también este import

def entry_comments(request, path_param_id):
    if request.method != "GET":
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
    comments = Comment.objects.filter(entry=path_param_id)
```

Y ahora  completaremos nuestro método igual que hicimos anteriormente: Vamos a _iterar_ sobre todas las filas de la tabla, y a _serializarlas_ en una respuesta HTTP.

Para ello, **añade**:

```python
def entry_comments(request, path_param_id):
    if request.method != "GET":
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
    comments = Comment.objects.filter(entry=path_param_id)
    json_response = []
    for row in comments:
        json_response.append(row.to_json())
    return JsonResponse(json_response, safe=False)
```

Y **añade** `to_json` a la clase `Comment` en `models.py`:

```python
class Comment(models.Model):
    content = models.CharField(max_length=RANDOMEXPRESSION1000|1100|1200|1300|1400|1500|1600|1700|1800|1900END)
    entry = models.ForeignKey(Entry, on_delete=models.CASCADE) # Clave foránea a Entry

    def to_json(self):
        return {
            "RANDOMEXPRESSIONcontent|comment|message|data|info|user_content|user_comment|user_dataEND": self.content,
        }
```

...y **completa** la tarea añadiendo la línea necesaria a `urls.py` para _mapear_ tu nuevo _endpoint_:

```python
urlpatterns = [
    # ...
    path('entries/<int:path_param_id>/comments', endpoints.entry_comments),
]
```

### ¡Listo!

Ya podemos obtener los comentarios de una _Entrada_.

De momento no hay ninguno en la base de datos, ¡pero si quieres puedes probar a añadir alguno usando `sqlite3.exe`!

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
