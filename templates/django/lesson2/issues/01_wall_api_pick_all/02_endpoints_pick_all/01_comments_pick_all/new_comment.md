POST para un comentario

## :books: Resumen

* Modificaremos el _endpoint_ `/entries/<int:path_param_id>/comments` para que permita **añadir** datos a la base de datos si recibe un **POST**

## Descripción

En nuestro tercer _endpoint_ (`GET /entries/<int:path_param_id>/comments`) permitimos **recuperar** todos los _Comentarios_ de una _Entrada_.

Ahora toca... ¡Creación de un comentario!

```
POST /entries/<int:path_param_id>/comments`
```

Vamos a esperar que el cliente HTTP envíe un **cuerpo de petición** como este:

```json
{
  "new_content": "Excelente, me gusta mucho"
}
```

Y con respecto _a qué Entrada asignar el nuevo Comentario_... ¡eso ya viene en la URL `path_param_id`!

### `cURL`

**Al terminar nuestro _endpoint_**, el siguiente POST enviado desde **cURL** debería generar un nuevo `Comment`:

```
curl http://localhost:8000/entries/1/comments -d "{\"new_content\": \"Excelente, me gusta mucho\"}"
```

Ahora mismo, tu `entry_comments` debe ser...

```python
def entry_comments(request, path_param_id):
    if request.method != "GET":
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
    comments = # Tu opción preferida para recuperar los comentarios de una entrada
    json_response = []
    for row in comments:
        json_response.append(row.to_json())
    return JsonResponse(json_response, safe=False)
```

## :package: La tarea

Retomaremos `entry_comments` de `endpoints.py`:

* Añadiendo `@csrf_exempt`
* Englobando en un `if` el código anterior, asociado a `"GET"`
* Añadiendo `if request.method == "POST"`
* Devolviendo un `405` en el `else:` final

**Así**:

```python
@csrf_exempt
def entry_comments(request, path_param_id):
    if request.method == "GET":
        comments = # Tu opción preferida para recuperar los comentarios de una entrada
        json_response = []
        for row in comments:
            json_response.append(row.to_json())
        return JsonResponse(json_response, safe=False)
    elif request.method == "POST":
        # Añadiremos un nuevo comentario
        # ...
    else:
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
```


### Obtener los datos enviados por el cliente HTTP

```python
    elif request.method == "POST":
        # Añadiremos un nuevo comentario
        client_json = json.loads(request.body)
        client_content = client_json.get("new_content", None)
        if client_content is None:
            return JsonResponse({"error": "Missing new_content"}, status=400)
        # ...
```

### Crear una nueva **instancia** del **modelo** y guardarla con `.save()`

```python
    elif request.method == "POST":
        # Añadiremos un nuevo comentario
        client_json = json.loads(request.body)
        client_content = client_json.get("new_content", None)
        if client_content is None:
            return JsonResponse({"error": "Missing new_content"}, status=400)
        # Creamos una nueva instancia de Comment
        # Nótese pasamos entry_id=... en vez de entry=...
        # para indicar que nos referimos a la clave primaria (id)
        # del atributo. Esto se hace así para los models.ForeignKey
        new_comment = Comment(content=client_content, entry_id=path_param_id)
        new_comment.save()
        return JsonResponse({"RANDOMEXPRESSIONsuccess|it_was_ok|it_was_successful|comment_added|new_comment_added|comment_created|new_comment_created|comment_saved|new_comment_savedEND": True}, status=201)
```

También devolvemos un `201 Created` si todo va bien.

**¡Terminado!**

Los usuarios ya podrán añadir _Comentarios_ a las publicaciones. 

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
