Más sintaxis ORM de Django

## :books: Resumen

* Veremos la sintaxis `_set` para acceder a filas relacionadas con otra tabla mediante una clave foránea

## Descripción

En la tarea anterior hemos recuperado _todas las filas_ de la tabla `Comment` cuyo `entry` sea el mismo que el dado por el cliente HTTP (`path_param_id`). 

Habíamos usado esta sentencia:

```python
comments = Comment.objects.filter(entry=path_param_id)
```

La siguiente alternativa puede facilitarnos recuperar datos de una relación **1:N**.

### 1) Recuperar la _Entrada_

Con `.get()` podemos lanzar un `SELECT` especificando condiciones igual que con `.filter()`. La diferencia es que **`.get()` entrega siempre una (1) fila**.

```python
entry = Entry.objects.get(id=path_param_id)
```

**¡Ojo!**. Si una `Entry` con ese `id` no existe, `.get()` lanzará una excepción `DoesNotExist` que debemos controlar así:

```python
try:
    entry = Entry.objects.get(id=path_param_id)
except Entry.DoesNotExist:
    return JsonResponse({"error": "Entry not found"}, status=404)
```

### 2) Extraer los _Comentarios_ asociados a la _Entrada_

Una vez tenemos la **instancia** de `Entry`, podemos acceder a sus comentarios. Hay que invocar un atributo especial autogenerado:

* Comienza con el nombre del modelo asociado en minúsculas (`comment`)
* Termina con `_set`
* Debemos invocar `.all()` para que entregue un `QuerySet []` iterable

Es decir:

```python
try:
    entry = Entry.objects.get(id=path_param_id)
except Entry.DoesNotExist:
    return JsonResponse({"error": "Entry not found"}, status=404)
# La Entrada sí existe. Vamos a acceder a los Comentarios relacionados
comments = entry.comment_set.all() # Así
```

## :package: La tarea

**Lee** con calma la sintaxis explicada arriba. Es útil.

Si lo deseas, **reemplaza** tu línea:

```python
comments = Comment.objects.filter(entry=path_param_id)
```

...de la tarea anterior por lo expuesto más arriba. ¿Entiendes que son dos formas diferentes de conseguir _casi_[^1] lo mismo?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------------------------------------

[^1]: _Casi_ lo mismo. ¡Pero con una diferencia! En la versión de este ejercicio, si la _Entrada_ con el ID enviado por cliente no existe, salta el `DoesNotExist` y se devuelve un `404` . En la versión del ejercicio anterior... ¡Simplemente se devuelve un _array_ JSON vacío!
