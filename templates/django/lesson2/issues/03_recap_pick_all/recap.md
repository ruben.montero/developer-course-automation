¡Trabajo terminado!

## :trophy: ¡Felicidades!

Has realizado las tareas del _sprint_ y has validado tu trabajo con los _tests_. ¡Enhorabuena!

<img src="python-django/docs/sunset.jpg" width=500 />

Has...

* Creado un proyecto SimpleAPI, con una _app_
* Implementado un _endpoint_ que devolvía, como array JSON, la tabla de multiplicar del 6
* Implementado un _endpoint_ que devolvía una tabla de multiplicar genérica, de 1 a 10, recibiendo un _path param_
* Prefijado un _path param_ con `int:` (`<int:number>`) para que sólo se ejecute con valores numéricos
* Manejado un _query param_, y creado algunos ejercicios con _path params_ y _query params_
* Controlado si `request.method` era `POST`, e interpretado datos JSON del cuerpo de la petición cliente usando `json.loads(body)`
* Implementado ejemplos de _endpoints_ que manejaban `PUT` y `DELETE`, y usado comandos `curl` para probar los `POST`, `PUT`, `DELETE`
* Creado un proyecto WallAPI
* Creado _modelos_ Django `Entry` y `Comment`, y comprendido que, en _mapeo objeto-relacional_ se usan para trabajar contra tablas de la base de datos SQL
* Creado las tablas asociadas con `python manage.py makemigrations` y `python manage.py migrate`
* Implementado un _endpoint_ que recuperaba todas las entradas (`.all`)
* Implementado un _endpoint_ que recuperaba los comentarios de una entrada (`.filter`)
* Implementado el POST en dichos _endpoints_ para guardar entradas y comentarios a base de datos (`.save`)
* Comprendido estrategias de paginación con parámetros `size` y `older_than`
* Implementado paginación en el GET de entradas gestionando _query params_ `size` y `older_than`, y contemplando varios escenarios
* Visualizado un fichero de especificación de un API REST en formato OpenAPI con una extensión de VSCode
* Creado un proyecto DashboardsAPI, que era como una versión extendida de WallAPI pero con _Dashboards_, _Preguntas_ y _Respuestas_
* ¡Implementado las 5 operaciones (_endpoints_) indicadas en la especificación, una a una!
