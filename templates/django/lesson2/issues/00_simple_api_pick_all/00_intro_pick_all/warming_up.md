Hola de nuevo, Django

## :books: Resumen

* Retomaremos nuestro camino con Django
* Crearemos un nuevo proyecto Django `SimpleAPI` dentro de la carpeta `apis`
* Probaremos que la página de ejemplo funciona
* _(Y como siempre, subiremos los cambios al repositorio)_

## Descripción

Como ya sabemos, [Django](http://www.djangoproject.com/) es un _framework_ de desarrollo web escrito en Python. Asumiendo que ya lo tenemos instalado en nuestro sistema operativo, vamos a comenzar un nuevo proyecto con él.

## :package: La tarea

En primer lugar, usaremos un terminal (cmd.exe) para cambiar el directorio activo hasta la ubicación de nuestro repositorio local usando `cd`. Luego:

```
git pull
```

...traerá los cambios de remoto a local. Contienen correcciones del proyecto anterior y **el esqueleto** del **nuevo proyecto**.

Se encontrará en **una carpeta llamada `python-django`**.

Usa tu [IDE](https://www.redhat.com/es/topics/middleware/what-is-ide) favorito para abrir...

### `python-django/`

Asumiendo que usas [PyCharm](https://www.jetbrains.com/pycharm/), si haces _click_ en el pequeño botón `Projects` de la barra vertical de la izquierda, verás las carpetas del proyecto.

```
- apis/

- docs/

- tests/
```

Nuestra primera misión será crear un proyecto Django llamado `SimpleAPI` **dentro de la carpeta `apis/`**.

### Creando un nuevo proyecto

Desde el terminal (cmd.exe) cambiaremos el directorio activo a `apis/`:

```
cd apis
```

Y desde allí, usaremos `django-admin` para generar un nuevo proyecto llamado `SimpleAPI`:

```
django-admin startproject SimpleAPI
```

Como resultado deberíamos ver una nueva **carpeta**:

```
- apis/
   |
   |- .gitkeep
   |
   |- SimpleAPI/

- docs/

- tests/
```

### Lanzar `SimpleAPI`

Este nuevo proyecto está vacío, pero podemos probarlo.

```
cd SimpleAPI
python manage.py runserver
```

Como ya sabes, también puedes **añadir una configuración** en PyCharm para **ejecutarlo** cómodamente desde el IDE.

Si la salida de ejecución es la esperable...

```
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).

You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
July 08, 2022 - 09:42:43
Django version 4.0.5, using settings 'SimpleAPI.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CTRL-BREAK.
```

...y http://localhost:8000 muestra la página de ejemplo...

**¡...enhorabuena!** Has puesto en marcha tu proyecto inicial.

Recuerda que puedes detenerlo con CTRL+C.

### :play_pause: Lanza el _test_

Prueba el _test_ asociado a la tarea. Lo ejecutaremos siguiendo los pasos de arriba, y esta vez lanzando `TESTFILENAME.py`.

Si ves algo como...

```
Ran 1 test in 0.003s

OK
```

¡Tarea terminada!

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

No está de más visitar la página de GitLab y verificar que el _commit_ se ha subido


