Números primos

## :books: Resumen

* Recordaremos qué son los números primos
* Crearemos un nuevo _endpoint_ que entregue en un JSON simple indicando si un número cualquiera dado por _query param_ es primo o no

## Descripción

Los [números primos](https://numeros.xyz/numeros-primos/) son:

> Aquellos números enteros mayores que cero que **solo son divisibles entre ellos mismos y el 1**, es decir, que tienen **exactamente dos divisores**.

### Complejidad

Los números primos representan un ámbito de estudio interesante, [especialmente en criptografía](https://www.logaritmoneperiano.com/numeros-primos-y-criptografia/). Una de las razones primordiales es que, dado un número, no podemos afirmar que **es** primo, sin antes intentar dividirlo por **todos los números menores que él**.

Por lo tanto, es un problema _computacionalmente costoso_ verificar si un número es primo.

## :package: La tarea 

**Se pide**:

* Añadir un nuevo _endpoint_ que devuelva si un número dado a través de un _query param_ `RANDOMEXPRESSIONn|i|j|k|d|m|qEND` es primo o no
  * La ruta será `/prime`
  
  * Si el número **es primo**, la respuesta JSON será:
  
```json
{"RANDOMEXPRESSIONprime|is_prime|is_prime_number|number_is_primeEND": true}
```

  * Por el contrario, si el número **no es primo**, la respuesta JSON será:
  
```json
{"RANDOMEXPRESSIONprime|is_prime|is_prime_number|number_is_primeEND": false}
```

  * Si el parámetro no es un **número** ó **no es un número > 0**, se devolverá el error 400:

```json
{"error": "RANDOMEXPRESSIONParameter must be a number greater than 0|Parameter must be a number greater than zero|Parameter must be a number bigger than 0|Parameter must be a number bigger than zeroEND"}
```

  * Si el parámetro no está presente en la petición, se devolverá el error 400:

```json
{"error": "Missing required 'RANDOMEXPRESSIONn|i|j|k|d|m|qEND' parameter"}
```

### Ejemplos

* http://localhost:8000/prime?RANDOMEXPRESSIONn|i|j|k|d|m|qEND=7

```json
{"RANDOMEXPRESSIONprime|is_prime|is_prime_number|number_is_primeEND": true}
```

* http://localhost:8000/prime?RANDOMEXPRESSIONn|i|j|k|d|m|qEND=9

```json
{"RANDOMEXPRESSIONprime|is_prime|is_prime_number|number_is_primeEND": false}
```

* http://localhost:8000/prime?RANDOMEXPRESSIONn|i|j|k|d|m|qEND=-5

```json
{"error": "RANDOMEXPRESSIONParameter must be a number greater than 0|Parameter must be a number greater than zero|Parameter must be a number bigger than 0|Parameter must be a number bigger than zeroEND"}
```

* http://localhost:8000/prime

```json
{"error": "Missing required 'RANDOMEXPRESSIONn|i|j|k|d|m|qEND' parameter"}
```

### Una curiosidad

¿Cuánto tarda tu servidor en responder ante esta URL?

* http://localhost:8000/prime?RANDOMEXPRESSIONn|i|j|k|d|m|qEND=1777777777

¿Y esta?

* http://localhost:8000/prime?RANDOMEXPRESSIONn|i|j|k|d|m|qEND=1777777778

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
