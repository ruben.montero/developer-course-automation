Las tablas del -∞ al +∞

## :books: Resumen

* Recordaremos qué son los _query params_
* Veremos cómo se interpretan en Django los _query params_
* Añadiremos una nueva función a `endpoints.py` que se _mapeará_ a una URL no-parametrizada. Será la nueva función quien compruebe ella sola si ha recibido _query params_
  * Devolverá en JSON una tabla de multiplicar

## Descripción

Hemos visto cómo _mapear_ y usar _path params_ en Django. Pero recordemos que en las URL hay otro tipo de parámetros de mucha relevancia: Los **_query params_**.

Son parámetros de la URL que viajan en formato `clave=valor` detrás del carácter _interrogante_ (`?`). Si hay varios, se anexan con _ampersand_ (`&`). Por ejemplo:

* http://localhost:8000/dashboards?size=10&search=new

_(Viajan [URL-encoded](https://www.urlencoder.io/) en las peticiones HTTP)_

### ¿Y por qué hay **dos** tipos distintos de parámetros?

* Los **_path params_** son **parte esencial** de la URL
* Los **_query params_** son **parámetros adicionales** que permiten especificar valores añadidos a la petición, por motivos de paginación, búsqueda, _tracking_,...
  
Por ejemplo:

  * https://twitter.com/elonmusk
  * https://twitter.com/neiltyson
  
vs.


  * https://twitter.com/neiltyson?last_tweets=5
  * https://twitter.com/neiltyson?last_tweets=10

### Venga, un pequeño ejemplo

De las siguientes URLs, ¿_qué_ partes crees que son _path params_?

¿_Qué_ partes crees que son _query params_? Y de estos, ¿cuál es la _clave_ y el _valor_ de cada uno?

* https://www.listaspam.com/busca.php?Telefono=5555555
* https://www.elmundo.es/television/momentvs/2022/07/12/62ccffc3fdddff92098b45c9.html?utm_source=marca
* https://es.wikipedia.org/wiki/Protestas_en_Sri_Lanka_de_2022
* https://duckduckgo.com/?t=ffab&q=programación&ia=web

### Y en Django... ¿cómo se _leen_ los _query params_ que hemos recibido?

Dado que los _query params_ se consideran muy variables (digamos, volátiles), **no** se indican en `urls.py`.

Es decir, tanto la URL:

* http://localhost:8000/ejemplo

...como:

* http://localhost:8000/ejemplo?una_cosa=cualquiera

...serían **procesadas** por el mismo _**endpoint**_:

```python
urlpatterns = [
    # ...
    path('ejemplo', endpoints.mi_funcion),
]
```

### Vale, entonces...

## En Django, ¿cómo se _leen_ los _query params_?

**Dentro** de la función, accedemos a ellos a través del diccionario de `GET` en el objeto `request`.

Es decir:

```python
def mi_funcion(request):
    # Siguiendo el ejemplo de arriba,
    # Esto imprimirá 'cualquiera'
    print(request.GET["una_cosa"])
```

### Acceder al valor del _query param_ controlando su posible ausencia

Si el _query param_ **no** ha sido enviado por el cliente, entonces el código de arriba **lanzará una excepción `KeyError`**[^1].

Podemos usar el método `.get()` para **acceder** al contenido del diccionario indicando un _valor por defecto_: `.get("clave", "Valor por defecto")`

En caso de que la clave _no exista_ en el diccionario, entonces toma el _valor por defecto_. Por ejemplo:

```python
def mi_funcion(request):
    # Para: http://localhost:8000/ejemplo?una_cosa=cualquiera
    # ...imprimirá: 'cualquiera'
    print(request.GET.get("una_cosa", "ninguna"))
    # Pero para: http://localhost:8000/ejemplo
    # ...imprimirá 'ninguna'
```

## :package: La tarea

Vamos a **añadir** en nuestro `urls.py` un nuevo _endpoint_:

```python
urlpatterns = [
    # ...
    path('v3/RANDOMEXPRESSIONmultiplications|multiplication|tableEND', endpoints.multiplication_table_query_param),
]
```

En `endpoints.py` **añadiremos** la función asociada.

Asignará a una **variable** el valor del _query param_ `RANDOMEXPRESSIONn|i|j|k|dEND`, con valor por defecto `None`:

```python
def multiplication_table_query_param(request):
    number = request.GET.get("RANDOMEXPRESSIONn|i|j|k|dEND", None)
```

O sea que podemos esperar responder a URLs como:

* http://localhost:8000/v3/RANDOMEXPRESSIONmultiplications|multiplication|tableEND?RANDOMEXPRESSIONn|i|j|k|dEND=5
* http://localhost:8000/v3/RANDOMEXPRESSIONmultiplications|multiplication|tableEND?RANDOMEXPRESSIONn|i|j|k|dEND=14

Si el valor **no** es `None`, fabricaremos y devolveremos una `JsonResponse` adecuada:

```python
def multiplication_table_query_param(request):
    number = request.GET.get("RANDOMEXPRESSIONn|i|j|k|dEND", None)
    if number is not None:
        result = []
        for i in range(1, 11):
            result.append(number * i)
        return JsonResponse(result, safe=False)
```

### ¿Y si el _query param_ `RANDOMEXPRESSIONn|i|j|k|dEND` no ha sido enviado por el cliente?

Si no lo recibimos, entonces la variable `number` tendrá el valor por defecto `None`.

Devolvemos un JSON indicando al cliente el error. El código HTTP adecuado es `400 Bad Request`:

```python
def multiplication_table_query_param(request):
    number = request.GET.get("RANDOMEXPRESSIONn|i|j|k|dEND", None)
    if number is not None:
        result = []
        for i in range(1, 11):
            result.append(number * i)
        return JsonResponse(result, safe=False)
    else:
        return JsonResponse({"error": "Missing 'RANDOMEXPRESSIONn|i|j|k|dEND' parameter"}, status=400)
```

Lanza el servidor y navega a:

* http://localhost:8000/v3/RANDOMEXPRESSIONmultiplications|multiplication|tableEND?RANDOMEXPRESSIONn|i|j|k|dEND=2

Es posible que veas algo como esto:

```json
[
  "2",
  "22",
  "222",
  "2222",
  "22222",
  "222222",
  "2222222",
  "22222222",
  "222222222",
  "2222222222"
]
```

**¡Ups!**

Parece que el operador _multiplicación_ (`*`) **no** está funcionando como esperamos.

¿Te resulta familiar este error?

Es un problema conocido. Deriva de que `number` es un `string` para Python, en lugar de un `int`.

### Vaya...

Piensa que podríamos recibir cualquier cosa _no numérica_. Por ejemplo:

* http://localhost:8000/v3/RANDOMEXPRESSIONmultiplications|multiplication|tableEND?RANDOMEXPRESSIONn|i|j|k|dEND=texto

### ¿Y qué hay que hacer?

**Se pide** que:

* **Arregles** el problema mencionado (deberás convertir `number` a tipo `int`)
* Además, **devuelvas** el siguiente JSON con un código 400:

```json
{"error": "Parameter 'RANDOMEXPRESSIONn|i|j|k|dEND' must be a number"}
```

...si el cliente HTTP envía algo que _no es convertible a número entero_ como _query param_ `RANDOMEXPRESSIONn|i|j|k|dEND`.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------------------------------------

[^1]: Puede ser controlada mediante un `try-except`
