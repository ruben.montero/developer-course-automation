Las tablas del 0 al +∞

## :books: Resumen

* Veremos que en Django se pueden _mapear path params_ de tipo `int` específicamente
* Añadiremos una nueva función a `endpoints.py` que recibirá como parámetro un `int` directamente y calculará la tabla de multiplicar del número recibido, sea cual sea

## Descripción

En nuestra tarea anterior hemos implementado _a mano_ las respuestas para las tablas de multiplicar del 1 al 10. Esto es un poco tedioso, ¿no?

La [unidad aritmético-lógica (ALU)](https://sistemas.com/alu.php) de nuestro ordenador puede procesar un montón de operaciones en cuestión de milisegundos

Calcular una sencilla tabla de multiplicar no le costará mucho.

En Python, bastaría el código:

```python
def multiply_number_improved(number):
    result = []
    for i in range(1, 11):
        result.append(number * i)
    return result
```

¡Vamos a crear una versión _mejorada_ del _endpoint_ de la tarea anterior!

Funcionará con URLs como:

* http://localhost:8000/v2/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/5
* http://localhost:8000/v2/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/8
* http://localhost:8000/v2/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/142

## :package: La tarea

Vamos a añadir un nuevo _mapeo_ a `urls.py`, pero esta vez **tendrá el prefijo `int:` en el nombre**. Así:

```python
urlpatterns = [
    # ...
    path('v2/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/<int:number>', ... ),
]
```

Si **añadimos** la función de arriba a `endpoints.py`, podemos **conectarla** al _endpoint_ directamente:

```python
urlpatterns = [
    # ...
    path('v2/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/<int:number>', endpoints.multiply_number_improved),
]
```

Fíjate en los siguientes detalles:

* En la URL, `number` lleva el prefijo `int:`
* **Sólo** valores numéricos **positivos** en la URL son válidos. Por ejemplo, si el usuario visita http://localhost:8000/v2/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/pepe_depura, **no** se ejecutará la función.
  * Por ello, dentro de `multiply_number_improved` tenemos **garantizado** que `number` es de tipo entero (`int`) y **positivo**
  
### Terminando...

**1) Añade** el método `multiply_number_improved` expuesto arriba a `endpoints.py`, y modifica su firma para que declare el **necesario** parámetro `request`:

```diff
-def multiply_number_improved(number):
+def multiply_number_improved(request, number):
```

**2) Modifica** el valor devuelto (`return`) para que sea una `JsonResponse`
  * Recuerda que necesitarás `safe=False`

Cuando completes estos pasos, lanza el servidor con `python manage.py runserver` y visita tu _endpoint_ para comprobar que funciona

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

