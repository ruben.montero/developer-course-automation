La tabla de multiplicar del 6

## :books: Resumen

* Crearemos una nueva función en `endpoints.py` dentro de nuestra `simplerestDEVELOPERNUMBERapp`
  * Devolverá una lista JSON con la tabla de multiplicar del **6**
* Lo _mapearemos_ a la URL `/RANDOMEXPRESSIONmultiply_by_six|six_table|multiplication_table_sixEND`

## Descripción

Vamos a crear un _endpoint_ que **calcule una tabla de multiplicar** y la devuelva como un sencillo JSON.

## :package: La tarea

Añadamos una nueva función _todavía incompleta_ `def table_of_six()` a `endpoints.py`:

```python
def table_of_six(request):
    http_response = []
    # En http_response vamos a insertar estos números:
    # 6
    # 12
    # 18
    # 24
    # 30
    # 36
    # 42
    # 48
    # 54
    # 60
    return JsonResponse(http_response, safe=False)
```

Podemos **completar** la función superior usando un [`for` con un `range`](https://www.pythontutorial.net/python-basics/python-for-range/):

¿Cómo lo **harás tú**?

### `urls.py`

Como siguiente paso, _mapeamos_ nuestro _endpoint_ a la URL `/RANDOMEXPRESSIONmultiply_by_six|six_table|multiplication_table_sixEND`:

```python
urlpatterns = [
    path('admin/', admin.site.urls),
    path('health', endpoints.health_check),
    path('RANDOMEXPRESSIONmultiply_by_six|six_table|multiplication_table_sixEND', endpoints.table_of_six),
]
```

**¡Listo!**

Tras lanzar (`manage.py runserver`) el servidor, ya podemos verificar:

* http://localhost:8000/RANDOMEXPRESSIONmultiply_by_six|six_table|multiplication_table_sixEND

...en nuestro navegador.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
