Las tablas del 1 al 10

## :books: Resumen

* Repasaremos qué son los _path params_ y los veremos en Django
* Crearemos una nueva función en `endpoints.py` que producirá como resultado una lista JSON con los valores de una tabla de multiplicar
  * Recibirá un parámetro, y en función de si vale `one`, `two`, `three`,... el resultado será la tabla del **1**, **2**, **3**,... hasta **10**
  * Devolverá un `404` si el valor del _path param_ es desconocido

## Descripción

Tener un _endpoint_ para la tabla de multiplicar del **6** no está mal, pero sería mejor poder ver **más** tablas de multiplicar.

Para ello, comenzaremos a trabajar con **_path params_** en Django.

Ya sabemos que un [**_path param_**](https://pythonguides.com/get-url-parameters-in-django/) es una parte variable de la URL que se encuentra en la parte principal de la dirección. Por ejemplo:

* https://es.wikipedia.org/wiki/Isaac_Newton
* https://es.wikipedia.org/wiki/Michael_Schumacher
* https://twitter.com/elonmusk/media
* https://twitter.com/neiltyson/media

...son ejemplos de URLs con _path params_.

Podemos _señalar_ dichos parámetros entre _mayor que_ y _menor que_ (`< >`), por ejemplo:

* `https://es.wikipedia.org/wiki/<article>`
* `https://twitter.com/<user>/media`

### ¿Y esto qué tiene que ver con nuestro `SimpleAPI`?

Vamos a intentar que las siguientes URLs:

* http://localhost:8000/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/one
* http://localhost:8000/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/two
* http://localhost:8000/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/three
* http://localhost:8000/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/four
* http://localhost:8000/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/five
* http://localhost:8000/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/six
* http://localhost:8000/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/seven
* http://localhost:8000/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/eight
* http://localhost:8000/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/nine
* http://localhost:8000/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/ten

Sean manejadas por nuestra API mediante un único _endpoint_:

* `http://localhost:8000/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/<number>`

## :package: La tarea

En **`urls.py`** añadiremos una línea así:

```python
urlpatterns = [
    # ...
    path('RANDOMEXPRESSIONmultiplications|multiplication|tableEND/<number>', endpoints.multiplication_table),
]
```

### ¿Se puede usar `< >` dentro de la URL?

Es **precisamente** la forma de indicar en Django que esa parte es variable. O sea, un _path param_.

### Definir la función `multiplication_table` en `endpoints.py`

En `endpoints.py` **añadimos** la firma del método `multiplication_table`, con **una condición muy especial**:

```python
def multiplication_table(request, number):
    # ...
```

**¡Tiene un parámetro 'extra'!**

* El _path param_ debe añadirse como **parámetro** de la función
* El **nombre** del _path param_ (en `urls.py`) debe coincidir con el **nombre** del parámetro (en `endpoints.py`; en este caso, `number`)
* Si hubiera más de uno, se añade también

Puedes imaginarte que si tu método _fuera así_:

```python
def multiplication_table(request, number):
    print("El usuario ha visitado la tabla del " + number)
    # ...
```

Entonces, al navegar a:

* http://localhost:8000/RANDOMEXPRESSIONmultiplications|multiplication|tableEND/one

...tu servidor haría el `print`:

```
El usuario ha visitado la tabla del one
```

### Terminando...

**Se pide** que en tu función `multiplication_table` tengas varios `if` anidados para manejar los siguientes **posibles** valores del _path param_:

* `one`
* `two`
* `three`
* `four`
* `five`
* `six`
* `eight`
* `nine`
* `ten`

...y para cada uno, devuelvas una lista JSON con los valores de la tabla de multiplicar. Igual que hacíamos con la tabla del **6** en la tarea anterior.

Por ejemplo[^1]:

```python
def multiplication_table(request, number):
    if number == "one":
        return JsonResponse([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], safe=False)
    elif number == "two":
        return JsonResponse([2, 4, 6, 8, 10, 12, 14, 16, 18, 20], safe=False)
    elif number == "three":
        # ...
```

_(Como ves, puedes aprovechar la sentencia `elif` que equivale a un `else` y un `if`)_

Si al llegar al final, el parámetro `number` no es ninguno de los considerados más arriba:

```python
def multiplication_table(request, number):
    
    # ...
    
    elif number == "ten":
        return JsonResponse([10, 20, 30, 40, 50, 60, 70, 80, 90, 100], safe=False)
    else:
        # ¡No coincide con ninguno!
```

...**entonces se devolverá** una respuesta JSON con este contenido:

```json
{
  "error": "RANDOMEXPRESSIONNumber not valid. Only one to ten are supported|Number not valid. Only one-ten are supported|Number invalid. Only one to ten are supported|Number invalid. Only one-ten are supportedEND"
}
```

...y el **código HTTP 404**.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------------

[^1]: Se muestra una sugerencia. Puedes escribir tu código tan compacto y legible como desees, usando técnicas diferentes si lo deseas (e.g. [map](https://stackoverflow.com/questions/37769435/understanding-python-map-function-range),...)
