Un primer endpoint

## :books: Resumen

* Crearemos nuestra primera _app_ dentro de `SimpleAPI` y la incluiremos en `settings.py`
* Añadiremos el fichero `endpoints.py`, que contendrá una función que sirve un _endpoint_ sencillo
* Modificaremos `urls.py` para _mapear_ una URL a dicha función, y servir así el _endpoint_ a los navegadores

## Descripción

Las convenciones de uso de Django indican que debemos [encapsular en _apps_](https://docs.djangoproject.com/en/4.0/intro/tutorial01/#creating-the-polls-app) nuestras funcionalidades. Vamos a crear en `SimpleAPI` una nueva _app_ llamada `simplerestDEVELOPERNUMBERapp`.

## :package: La tarea 

Usando un terminal (cmd.exe) debemos _posicionar_ el directorio activo en la carpeta `SimpleAPI` que contiene `manage.py`. Desde ahí:

```
python manage.py startapp simplerestDEVELOPERNUMBERapp
```

### **`simplerestDEVELOPERNUMBERapp`**

Contendrá inicialmente una serie de ficheros. Ya conocemos algunos de ellos.

Comencemos **eliminando `views.py`**, desde el explorador de archivos o desde el IDE, mismamente.

**Añadiremos** un _nuevo fichero_ **`endpoints.py`** que contendrá las _funciones_ de _backend_.

Llegaremos a algo así:

![](python-django/docs/django_simplerest.png)

### `settings.py`

Como ya sabemos, para que nuestro proyecto [Django](http://www.djangoproject.com/) (SimpleAPI) pueda hacer uso efectivo de nuestra _app_ (simplerestDEVELOPERNUMBERapp), debemos añadir la siguiente línea a `settings.py`:

```diff
INSTALLED_APPS = [
+    'simplerestDEVELOPERNUMBERapp.apps.SimplerestDEVELOPERNUMBERAppConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```

## `endpoints.py`

Un fichero clave que contendrá el **código importante** de nuestro servidor REST.

De momento, añadamos una primera _función_ que devuelva una sencilla respuesta en JSON:

```python
from django.http import JsonResponse

def health_check(request):
    http_response = {"RANDOMEXPRESSIONalive|is_alive|lives|is_living|running|is_running|is_upEND": True}
    return JsonResponse(http_response)
```

_(¿Recuerdas **qué** es el parámetro `request`? ¡Lo usaremos próximamente!)_

## `urls.py`

Mientras dicha función no esté _conectada_ a una URL ó ruta, **no sirve de nada**.

La segunda pieza clave que sirve a este propósito es el fichero `urls.py`:

Conectaremos nuestra función al _endpoint_ `/health`, de tal manera que si visitas:

* http://localhost:8000/health

...verás la respuesta JSON deseada.

### ¿Cómo lo hacemos?

Así:

```python
from django.contrib import admin
from django.urls import path
from simplerestDEVELOPERNUMBERapp import endpoints # ¡Acordémonos del import!

urlpatterns = [
    path('admin/', admin.site.urls),
    path('health', endpoints.health_check), # Esta línea mapea la URL a la función Python
]
```

### Configuraciones en PyCharm

Recuerda que también puedes:

* _Click_ derecho en la carpeta **SimpleAPI** padre (la que cuelga de `apis/`) > _Mark Directory as_ > _Sources Root_, para prevenir errores de PyCharm interpretando los módulos disponibles

**¡Listo!**

Comprueba que ves el _endpoint_ desde el navegador.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

