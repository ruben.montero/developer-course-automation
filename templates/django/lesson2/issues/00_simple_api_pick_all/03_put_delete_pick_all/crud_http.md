GET-POST-PUT-DELETE

## :books: Resumen

* Repasaremos qué es CRUD
* Veremos que los principales verbos HTTP tienen una correspondencia 1:1 con las operaciones CRUD
* Crearemos un nuevo _endpoint_ que responde de forma distinta en función del verbo HTTP recibido

## Descripción

Un concepto esencial que aparece en distintas disciplinas de la informática es el de [**CRUD**](https://www.sumologic.com/glossary/crud/). Son siglas en inglés para:

* **C**reate
* **R**ead
* **U**pdate
* **D**elete

...y definen las operaciones básicas de acceso a datos.

Los métodos HTTP principales _encajan_ con cada una de las operaciones CRUD así:

* `POST`: Create
* `GET`: Read
* `PUT`: Update
* `DELETE`: Delete

## Una visión global

<img src="python-django/docs/distributed-app-crud.png" width=600/>

### ¿Base de datos?

Lo veremos pronto.

## :package: La tarea

Vamos a crear un nuevo _endpoint_ que, simplemente, entregue al [cliente HTTP](https://developer.mozilla.org/es/docs/Web/HTTP/Overview) una respuesta distinta en función del verbo HTTP usado en la petición.

De momento **no** hay conexión a ninguna base de datos.

Bastará con añadir este código a `endpoints.py`:

```python
@csrf_exempt
def example(request):
    if request.method == "GET":
        message = "Reading some data, huh?"
    elif request.method == "POST":
        message = "RANDOMEXPRESSIONThis should create a new thing!|This should create a new thingEND"
    elif request.method == "PUT":
        message = "RANDOMEXPRESSIONYou can update any element now|You can update any element with thisEND"
    elif request.method == "DELETE":
        message = "RANDOMEXPRESSIONThis will remove one or many elements, for sure!|This will remove one or many elementsEND"
    else:
        # Esta línea nunca se ejecutará si el verbo HTTP no es GET, POST, PUT o DELETE
        return JsonResponse({"error": "HTTP method not allowed"}, status=405)
    # Flujo normal
    return JsonResponse({"message": message})
```

...y en `urls.py` **_mapearemos_** la función a `/example`, así:

```python
urlpatterns = [
    # ...
    path('example', endpoints.example),
]
```

¿Cuál es la respuesta HTTP si ejecutas los siguientes comandos en un terminal (cmd.exe)?

```
curl -X POST localhost:8000/example
```

```
curl -X GET localhost:8000/example
```

```
curl -X PUT localhost:8000/example
```

```
curl -X DELETE localhost:8000/example
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
