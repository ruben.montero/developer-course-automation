POST

## :books: Resumen

* Veremos qué es un verbo ó método HTTP
* Entenderemos que tienen diferente _semántica_
* Crearemos un _endpoint_ que procesa una petición **POST**
  * Especificaremos el decorador `@csrf_exempt`
* Como ya no servirá el navegador web, usaremos `curl` para probarlo

## Descripción

Cuando vimos las [peticiones HTTP](https://www.ionos.es/digitalguide/hosting/cuestiones-tecnicas/http-request/) dijimos que constaban de **tres** partes, de las cuales sólo es _obligatoria_ la primera:

* Línea de petición
* Cabeceras
* Cuerpo

Pues bien, la línea de petición está compuesta _principalmente_ de **2** cosas:

* Método (o verbo) HTTP
* Recurso solicitado

### ¿[Método HTTP](https://developer.mozilla.org/es/docs/Web/HTTP/Methods)?

Es un valor de los definidos en el [RFC 7231](https://datatracker.ietf.org/doc/html/rfc7231#section-4). `GET` es uno de ellos.

Así, una petición HTTP típica de un navegador web podría tener esta línea de petición:

```
GET /index.php
```

¡Pero existen más que `GET`! Por ejemplo: `HEAD`, `POST`, `PUT`, `DELETE`, `CONNECT`, `OPTIONS`, `TRACE`, `PATCH`...

Cada uno tiene una _semántica_ diferente. Esto es parte de la arquitectura HTTP y sirve para que se pueda _distinguir_ el objetivo de una petición. Por ejemplo:

* Una petición `GET` se considera _segura_. Sólo **lee** datos del servidor
* Una petición `DELETE` se considera _insegura_. Puede eliminar datos del servidor

### ¿Por qué hay distintos métodos HTTP? Ejemplo 1

Imaginemos un [_webcrawler_](https://www.ionos.es/digitalguide/online-marketing/marketing-para-motores-de-busqueda/que-es-un-web-crawler/) de Google ejecutándose.

Un _webcrawler_ es un proceso que manda peticiones automáticas y _navega_ por la web, para _descubrirla_. ¡Así es como Google sabe qué resultados de búsqueda ofrecerte! Sus _webcrawlers_ navegan por la web con frecuencia.

Si no hubiera diferentes métodos HTTP, un _webcrawler_ podría mandar peticiones que, sin querer, eliminasen datos de un servidor. ¡Que error!


### ¿Por qué hay distintos métodos HTTP? Ejemplo 2

`POST`, además de ser inseguro, tiene **permitido** producir efectos _colaterales_. Esto tiene que ver con el concepto de [idempotencia](https://www.adictosaltrabajo.com/2015/06/29/rest-y-el-principio-de-idempotencia/).

El método HTTP `POST` **no** es idempotente.

En resumen, cuando en el navegador nos salta una alerta _¿Desea confirmar el reenvío del formulario?_ esto sucede al refrescar una página que lanzaba un `POST` porque podría producir efectos colaterales. Gracias a que `POST` está definido como _no idempotente_, el navegador sabe que debe _avisarnos_ de que vamos a repetir una petición peligrosa. Por ejemplo, podríamos pagar **2** veces un pedido en una tienda on-line.

### Resumen

Tabla de los **4** métodos HTTP que más nos importan:

-|GET|PUT|DELETE|POST
-|---|----|---|------
**¿Seguro?**|Sí|No|No|No
**¿Idempotente?**|Sí|Sí|Sí|No

## :package: La tarea

Vamos a crear un nuevo _endpoint_ en nuestro proyecto Django que **procese una petición POST**.

**Añadiremos** en `urls.py` una nueva línea:

```python
urlpatterns = [
    # ...
    path('resource/<int:number>', endpoints.resource_example),
]
```

En `endpoints.py`, **añadiremos** una función con el nombre apropiado y que declara recibir `number` como parámetro. Como vamos a procesar un POST, debemos añadir el _decorador_ `@csrf_exempt`[^1]. Así:


```python
from django.views.decorators.csrf import csrf_exempt

# ...

@csrf_exempt
def resource_example(request, number):
    # ...
```

Para _comprobar_ **qué** método HTTP fue usado en la petición, sólo hay que investigar `request.method`. Si es otro método distinto de **POST**, vamos a devolver una respuesta distinta:

```python
@csrf_exempt
def resource_example(request, number):
    if request.method == 'POST':
        return JsonResponse({"message": "You have sent a POST to the resource " + str(number)})
    else:
        return JsonResponse({"error": "HTTP method not supported"})
```

Si lo prefieres, de forma equivalente ahorrándote una línea...

```python
@csrf_exempt
def resource_example(request, number):
    if request.method == 'POST':
        return JsonResponse({"message": "You have sent a POST to the resource " + str(number)})
    return JsonResponse({"error": "HTTP method not supported"})
```

### Probemos nuestro _endpoint_

Si lanzas el servidor con `python manage.py runserver` y visitas la siguiente URL:

* http://localhost:8000/resource/15

...¿qué respuesta ves?

### Mi navegador **no** ha mandado un POST

Correcto.

### ¿Qué ha mandado?

Un GET. Siempre manda GET.

### ¿Y cómo envío un HTTP `POST`?

Debes usar **algo distinto**. 

Puedes usar alternativas como [Postman](https://www.postman.com/), pero nosotros veremos cómo usar una utilidad que viene con todos los sistemas operativos.

### [cURL](https://github.com/curl/curl)

**Abre** un terminal (cmd.exe) de Windows y escribe `curl`.

Al darle a ENTER verás:

```
curl: try 'curl --help' for more information
```

Si tecleas:

```
> curl -X POST localhost:8000/resource/10
```

...y ves:

```
{"message": "You have sent a POST to the resource 10"}
```

¡Enhorabuena! Has implementado y verificado tu primer **_endpoint_ POST con Django**.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------

[^1]: En este paso deshabilitamos un mecanismo de Django para proteger los formularios web de ataques [Cross Site Request Forgery](https://portswigger.net/web-security/csrf). Como no estamos desarrollando páginas web, sino APIs REST, nos daría problemas tenerlo habilitado.
