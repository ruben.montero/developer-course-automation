Animal favorito

## :books: Resumen

* Añadiremos un nuevo _endpoint_ que procesará una petición POST
  * Leerá el cuerpo de la petición y devolverá un mensaje condicionalmente
  * Si no hay cuerpo de petición o es incorrecto, devolverá un 400

## Descripción

Es momento de practicar un poco esto de los POST y los cuerpos de peticiones HTTP.

## :package: La tarea

**Se pide** que:

* Añadas un nuevo _endpoint_ `/RANDOMEXPRESSIONfavorite_animal|preferred_animalEND`
  * En dicho _endpoint_ sólo se procesarán peticiones POST. Si el método HTTP de la petición es otro, se devolverá un código HTTP `405` _(el cuerpo de la respuesta es libre)_
  * Se esperará un _cuerpo de petición_ HTTP como este:
  
```
{"name": <valor del nombre>}
```

Por ejemplo:

```json
{"name": "Dog"}
```

### Respuesta del servidor

* **Si** el valor de `"name"` recibido es **`"Cat"`**, el servidor responderá:
  
```json
{"message": "RANDOMEXPRESSIONThat is nice! Seven lives will be enough|That is nice! Seven lives will make it|That is good! Seven lives will be enough|That is great! Seven lives will be enough|Nice! Seven lives will be enough|Great! Seven lives will be enoughEND"}
```

* En cualquier **otro caso**, responderá:
  
```json
{"message": "RANDOMEXPRESSIONOK! Have a great day|Alright! Have a great day|OK! Have a nice day|Alright! Have a nice day|OK! Have a wonderful day|Alright! Have a wonderful dayEND"}
```

Además:

* **Si**:
  * La petición **no** tiene cuerpo HTTP (`len(request.body)==0`) ó
  * El JSON **no es válido** (no contiene la clave `"name"`)
  
...se devolverá un error `400` _(el cuerpo de la respuesta es libre)_.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
