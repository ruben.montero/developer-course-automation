405: Método no permitido

## :books: Resumen

* Mejoraremos ligeramente el código de la tarea anterior, añadiendo un código de respuesta HTTP 405

## Descripción

Nuestro método que procesaba peticiones a...

* `http://localhost:8000/resource/<int:number>`

...en la tarea anterior; tenía un `return` al final del todo:

```python
@csrf_exempt
def resource_example(request, number):
    if request.method == 'POST':
        return JsonResponse({"message": "You have sent a POST to the resource " + str(number)})
    return JsonResponse({"error": "HTTP method not supported"})
```

Esta respuesta:

```json
{"error": "HTTP method not supported"}
```

...viaja con un código HTTP de respuesta `200 OK`.

Pues bien, hay un [código de respuesta HTTP](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status) que será perfecto para expresar esto:

### `405: Method not allowed`

Cuando un servidor recibe una petición HTTP a un _endpoint_ con un verbo HTTP inesperado, debe responder usando un `405`.

¡Hagámoslo!

## :package: La tarea

```diff
@csrf_exempt
def resource_example(request, number):
    if request.method == 'POST':
        return JsonResponse({"message": "You have sent a POST to the resource " + str(number)})
-    return JsonResponse({"error": "HTTP method not supported"})
+    return JsonResponse({"error": "HTTP method not supported"}, status=405)
```

Este código sería **más legible** si le damos la vuelta.

Es decir, primero devolvamos el `405` si hay problemas. Después, _asumiendo_ que el método es correcto, ejecutamos el código normal.

Así:

```python
@csrf_exempt
def resource_example(request, number):
    if request.method != 'POST':
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
    # Precondición comprobada: El método es POST
    return JsonResponse({"message": "You have sent a POST to the resource " + str(number)})
```

**¡Listo!**.

Hemos mejorado un poquito el código.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
