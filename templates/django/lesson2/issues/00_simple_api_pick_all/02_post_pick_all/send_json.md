Cuerpo de petición

## :books: Resumen

* Comprenderemos _qué_ es el cuerpo de una petición HTTP
* Veremos cómo _enviar_ JSON al servidor
* En nuestro _endpoint_ de la tarea anterior, interpretaremos un valor recibido en el _cuerpo de la petición_

## Descripción

Recordemos que las peticiones HTTP tienen **tres** partes:

* Línea de petición
* Cabeceras
* Cuerpo

Pues bien, el _cuerpo_ puede tener un contenido _totalmente libre_.

### Una restricción:

Las peticiones `GET` no **pueden** llevan cuerpo.

Pero las `POST`... ¡sí!

### ¿Y por qué mandar un cuerpo HTTP desde el cliente, en la petición?

Quizá pienses que como cliente HTTP, ya puedes enviar datos al servidor como _query params_ ó _path params_... ¿Por qué enviar datos en el **cuerpo HTTP**?

Cada parte de la petición HTTP tiene su cometido.

El cuerpo está destinado a albergar datos que son de mayor tamaño o cumplen un propósito que no _encaja_ en la URL.

Por ejemplo: Si estás subiendo una imagen JPG a tu perfil, ¿cómo va a viajar en la URL?

### ¿Cómo enviar datos en el cuerpo HTTP de la petición del cliente?

Desde [cURL](https://es.wikipedia.org/wiki/CURL), podemos usar `-d`:

```
curl -X POST http://localhost:8000/resource/10 -d "{"mood": "Happy"}"
```

¡Ah! Si usamos `-d`, **cURL** entenderá que estamos enviando un **POST**, así que podemos _omitir_ `-X POST`:

```
curl http://localhost:8000/resource/10 -d "{"mood": "Happy"}"
```

**¡Ojo!** Este ejemplo aún no está completo. Hay que [_escapar_ las dobles comillas](http://stackoverflow.com/questions/7172784/ddg#7173011) (`" "`) dentro del JSON, o el terminal de Windows tendrá problemas:

```
curl http://localhost:8000/resource/10 -d "{\"mood\": \"Happy\"}"
```

Esos datos (ese JSON) lo leeremos desde el servidor Django accediendo a `request.body`. ¡Manos a la obra!

## :package: La tarea

Nuestro método era así:

```python
@csrf_exempt
def resource_example(request, number):
    if request.method != 'POST':
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
    # Precondición comprobada: El método es POST
    return JsonResponse({"message": "You have sent a POST to the resource " + str(number)})
```

### 1) Comprobamos que `request.body` tiene contenido

Lo podemos hacer verificando el _tamaño_ de `request.body`:

```python
@csrf_exempt
def resource_example(request, number):
    if request.method != 'POST':
        return JsonResponse({"error": "HTTP method not supported"}, status=405)

    # Precondición comprobada: El método es POST
    if len(request.body) == 0:
        # No hay cuerpo de petición
        # Nos comportamos como antes
        return JsonResponse({"message": "You have sent a POST to the resource " + str(number)})

    # Si hemos llegado aquí, el cuerpo HTTP de la petición tiene algo
    # ...
```


### 2) _Traducimos_ el JSON del cuerpo con `json.loads`

Para _interpretar_ el cuerpo de la petición HTTP, usamos la librería `json`[^1] sobre `request.body`.

```python
import json

# ...

@csrf_exempt
def resource_example(request, number):

    # ...
    
    # Si hemos llegado aquí, el cuerpo HTTP de la petición tiene algo
    http_body = json.loads(request.body)
```

### 3) Lo procesamos como un diccionario Python normal

**Completaremos** nuestro `resource_example` así:

```python
    # Si hemos llegado aquí, el cuerpo HTTP de la petición tiene algo
    http_body = json.loads(request.body)
    client_mood = http_body.get("mood", "No mood") # "No mood" será un valor por defecto
    return JsonResponse({"message": "You have sent a POST to the resource " + str(number) + " RANDOMEXPRESSIONand your mood is|and you are|and you're|and you are in the following mood:|and you're in the following mood:END " + client_mood})
```

**¡Tarea completada!**

Ya puedes usar el comando `curl` de arriba y verificar _qué_ responde nuestro servidor REST.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------------------------------------

[^1]: Si alguna te encuentras con un error como `UnicodeDecodeError: 'utf-8' codec can't decode byte`, seguramente estás mandando caracteres especiales (e.g.: `á`, `é`, `¡`,...) que viajan codificados según su código Unicode. Prueba a usar `json.loads(request.body.decode('raw_unicode_escape'))` en lugar de `json.loads(request.body)`
