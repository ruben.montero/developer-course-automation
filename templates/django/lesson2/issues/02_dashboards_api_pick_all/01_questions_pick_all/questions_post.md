POST /api/v1/dashboards/{dashboardId}/questions

## :books: Resumen

* Implementaremos el _endpoint_ para `POST` de una _Pregunta_

## Descripción

Previsualiza el fichero de especificación OpenAPI `python-django/docs/swagger/openapi.yaml` desde VisualStudio, como hicimos en una tarea anterior.

Implementaremos el tercer _endpoint_ `POST /api/v1/dashboards/{dashboardId}/questions` tal y como está especificado.

## :package: La tarea

Dentro de `endpoints.py` dentro de `dashboardDEVELOPERNUMBERapp` **modifica** `questions_from_dashboard` para soportar la operación HTTP `POST`, **así**:

```diff
+@csrf_exempt
def questions_from_dashboard(request, path_param_id):
    if request.method == "GET":
       before = request.GET.get("before", None)
       size = request.GET.get("size", None)
       if size is None:
           if before is None:
               questions = Question.objects.filter(dashboard=path_param_id).order_by('-publication_date')
           else:
               questions = Question.objects.filter(dashboard=path_param_id).filter(publication_date__lt=before).order_by("-publication_date")

       else:
           # ¿Quizá aquí controlas que 'size' sea un integer válido y devuelves un JsonResponse({}, status=400) en caso de KeyError? ;)
           if before is None:
               questions = Question.objects.filter(dashboard=path_param_id).order_by('-publication_date')[:size]
           else:
               questions = Question.objects.filter(dashboard=path_param_id).filter(publication_date__lt=before).order_by("-publication_date")[:size]               
        json_response = []
        for row in questions:
            json_response.append(row.to_json())
        return JsonResponse(json_response, safe=False)
+   elif request.method == "POST":
+       client_json = json.loads(request.body)
+       client_title = client_json.get("title", None)
+       client_summary = client_json.get("summary", None)
+       if client_title is None or client_summary is None:
+           return JsonResponse({"error": "Missing summary or title in request body"}, status=400)
+       new_question = Question(title=client_title, RANDOMEXPRESSIONsummary|content|descriptionEND=client_summary, dashboard_id=path_param_id)
+       new_question.save()
+       return JsonResponse({"success": True}, status=201)
    else:
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
```

**Pruébalo**, lanzando el siguiente `curl` con el servidor levantado:

```
curl http://localhost:8000/api/v1/dashboards/1/questions -d "{\"title\": \"Una preguntilla\", \"summary\": \"Qué tal va todo?\"}"
```

Si en tu base de datos existe el _dashboard_ con `id` 1... ¡Deberías haber añadido una pregunta asociada!

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
