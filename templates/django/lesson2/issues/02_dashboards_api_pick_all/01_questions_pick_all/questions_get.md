GET /api/v1/dashboards/{dashboardId}/questions

## :books: Resumen

* Implementaremos el _endpoint_ para `GET` de _Preguntas de un Dashboard_

## Descripción

Previsualiza el fichero de especificación OpenAPI `python-django/docs/swagger/openapi.yaml` desde VisualStudio, como hicimos en una tarea anterior.

Implementaremos el segundo _endpoint_ `GET /api/v1/dashboards/{dashboardId}/questions` tal y como está especificado.

## :package: La tarea

Dentro de `endpoints.py` dentro de `dashboardDEVELOPERNUMBERapp` **añade** la siguiente función, que recupera las _preguntas_ asociadas a un _dashboard_ ordenadas de reciente a antigua:

```python
from .models import Question

def questions_from_dashboard(request, path_param_id):
    if request.method == "GET":
        questions = Question.objects.filter(dashboard=path_param_id).order_by('-publication_date')
        json_response = []
        for row in questions:
            json_response.append(row.to_json())
        return JsonResponse(json_response, safe=False)
    else:
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
```

Necesitaremos, también, **añadir** el siguiente método `to_json` dentro de la clase **`Question`** en `models.py`:

```python
    def to_json(self):
        return {
            "id": self.id,
            "title": self.title,
            "summary": self.RANDOMEXPRESSIONsummary|content|descriptionEND,
            "publication_date": self.publication_date
        }
```

Y, ahora, en `urls.py`, **conecta** la función recién creada a la URL correcta, **así**:

```diff
from dashboardDEVELOPERNUMBERapp import endpoints

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/dashboards', endpoints.all_dashboards),
+   path('api/v1/dashboards/<int:path_param_id>/questions', endpoints.questions_from_dashboard),
]
```

Casi terminado.

Puedes añadir _un dashboard_ a la base de datos con SQLite y probar el resultado de http://localhost:8000/api/v1/dashboards/1/questions tras levantar el servidor con `python manage.py runserver`.

### _Query params_ `before` y `size`

¡En la especificación se indica que debemos _soportar_ los parámetros `before` y `size`! Servirán para que los clientes HTTP paginen las respuestas.

**Modifica** la función `questions_from_dashboard` **así**:

```diff
def questions_from_dashboard(request, path_param_id):
    if request.method == "GET":
-       questions = Question.objects.filter(dashboard=path_param_id).order_by('-publication_date')
+       before = request.GET.get("before", None)
+       size = request.GET.get("size", None)
+       if size is None:
+           if before is None:
+               questions = Question.objects.filter(dashboard=path_param_id).order_by('-publication_date')
+           else:
+               questions = Question.objects.filter(dashboard=path_param_id).filter(publication_date__lt=before).order_by("-publication_date")
+
+       else:
+           if before is None:
+               questions = Question.objects.filter(dashboard=path_param_id).order_by('-publication_date')[:size]
+           else:
+               questions = Question.objects.filter(dashboard=path_param_id).filter(publication_date__lt=before).order_by("-publication_date")[:size]               
        json_response = []
        for row in questions:
            json_response.append(row.to_json())
        return JsonResponse(json_response, safe=False)
    else:
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
```

_Sólo falta_ que si el parámetro `size` **no es un número válido** (o sea, `int(size)` lanza `ValueError`) devolvamos un código HTTP `400`.

**Añade** un `try-except` donde consideres oportuno para contemplar este caso :smiley:

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
