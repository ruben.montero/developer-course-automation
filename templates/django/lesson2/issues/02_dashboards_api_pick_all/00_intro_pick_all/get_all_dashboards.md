GET /api/v1/dashboards

## :books: Resumen

* Implementaremos el _endpoint_ GET /api/v1/dashboards

## :package: La tarea

Previsualiza el fichero de especificación OpenAPI `python-django/docs/swagger/openapi.yaml` desde VisualStudio, como hicimos en una tarea anterior.

Implementaremos el primer _endpoint_ `GET /api/v1/dashboards` tal y como está especificado.

**Añade** un fichero `endpoints.py` dentro de `dashboardDEVELOPERNUMBERapp`.

**Añade** la siguiente función, que recupera _todos_ los _dashboards_ de la base de datos:

```python
from django.http import JsonResponse
from .models import Dashboard

def all_dashboards(request):
    if request.method != "GET":
        return JsonResponse({"error": "HTTP method not supported"}, status=405)
    all_rows = Dashboard.objects.all()
    json_response = []
    for row in all_rows:
        json_response.append(row.to_json())
    return JsonResponse(json_response, safe=False)
```

Necesitaremos, también, **añadir** el siguiente método `to_json` dentro de la clase **`Dashboard`** en `models.py`:

```python
    def to_json(self):
        return {
            "id": self.id,
            "title": self.title,
            "summary": self.summary
        }
```

Y, por último, en `urls.py`, **conecta** la función recién creada a la URL correcta, **así**:

```python
from dashboardDEVELOPERNUMBERapp import endpoints

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/dashboards', endpoints.all_dashboards),
]
```

**¡Listo!**

Si lanzas tu servidor con `python manage.py runserver`, puedes verificar que obtienes un _array_ vacío en http://localhost:8000/api/v1/dashboards

### :play_pause: Lanza el _test_

El _test_ **inyecta** una base de datos temporal, ficticia, para probar tu código automáticamente.

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
