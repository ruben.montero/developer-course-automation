Especificación de Dashboards API

## :books: Resumen

* Comprenderemos la importancia de una especificación clara y detallada de los APIs REST
* Instalaremos una extensión para VisualStudio Code que nos ayudará a visualizar un fichero de especificación OpenAPI

## Descripción

Hasta ahora hemos dicho _alegremente_ cómo se debían llamar los _endpoints_ o cómo eran los JSON que se transferían. Esos detalles son la **especificación de un API REST**.

Dicha **especificación**... ¡Es muy importante!

* Los **clientes HTTP** deberán mandar las peticiones adecuadas que nosotros esperamos
* Por eso, **cliente** y **servidor** deben estar de acuerdo. Ese acuerdo... ¡es casi como un contrato!

### [OpenAPI](https://swagger.io/specification/)

OpenAPI (anteriormente Swagger) es un estándar de _cómo_ se debe escribir la especificación de un API REST.

No es algo que debamos estudiar. Tan sólo una especificación que va evolucionando. Cuando necesitemos saber _cómo_ se hace algo, consultaremos la [documentación oficial](https://swagger.io/specification/)

La mejor forma de entenderlo es con un ejemplo.

### DashboardAPI

Nuestro siguiente proyecto está **especificado** en un fichero OpenAPI (`.yaml`).

El fichero está en tu propio repositorio, en `python-django/docs/swagger/openapi.yaml`.

_(Si quieres visitarlo desde GitLab directamente, puedes hacerlo [aquí](python-django/docs/swagger/openapi.yaml))_

## :package: La tarea

Si no tienes instalado [VisualStudio Code](https://code.visualstudio.com/), [**instálalo**](https://code.visualstudio.com/docs/?dv=win64user).

**Abre** Visual Studio Code, y en la pestaña `Complementos`, **instala** `Swagger Viewer`:

<img src="python-django/docs/swagger_viewer_vscode.png" width="250">

Luego, también desde Visual Studio Code, **abre** el fichero `python-django/docs/swagger/openapi.yaml`. ¡Es la especificación de un nuevo API REST que implementaremos!

**Pulsa** F1 y ejecuta _Preview Swagger_ (esta opción sólo está disponible si has instalado el complemento `Swagger Viewer`):

<img src="python-django/docs/preview_swagger.png" width="400">

Deberías ver una _previsualización_ del API REST _dashboards_:

<img src="python-django/docs/dashboards_api_swagger.png" width="500">

Ahí está la información de _cómo es_ el API REST que crearemos en las siguientes tareas. Curiosea un poco.

### :trophy: Por último

No hagas nada más. ¡Esta tarea no es de código! No hace falta subir ningún _commit_.
