DashboardsAPI modelos: Dashboards, preguntas, respuestas

## :books: Resumen

* Crearemos un nuevo proyecto Django: DashboardsAPI
* Haremos la configuración inicial y crearemos tres _modelos_: `Dashboard`, `Question`, `Answer`

## Descripción

¡Repasaremos cómo funciona Django creando un nuevo API REST! Crearemos algo así como una versión ampliada de `WallAPI`.

Esta vez, tendremos _Dashboards_ que contienen _Preguntas_, y a su vez, las _Preguntas_ contienen _Respuestas_.

¡`DashboardAPI`!

## :package: La tarea

**Se pide** que uses `django-admin` para **crear** un nuevo proyecto dentro de `apis/`.

* Se llamará `DashboardAPI`
* Dentro, **crea** una _app_ llamada `dashboardDEVELOPERNUMBERapp`

```
- apis/
   |
   |-- DashboardAPI/
   |    |
   |    |-- DashboardAPI/
   |    |    |
   |    |    |-- __init__.py
   |    |    |-- asgi.py
   |    |    |-- settings.py
   |    |    |-- urls.py
   |    |    |-- wsgi.py
   |    |
   |    |-- dashboardDEVELOPERNUMBERapp/
   |    |    |
   |    |    |-- migrations/
   |    |    |-- __init__.py
   |    |    |-- admin.py
   |    |    |-- apps.py
   |    |    |-- views.py
   |    |    |-- models.py
   |    |    |-- tests.py
   |    |
   |    |-- db.sqlite3
   |    |-- manage.py
   |
   |-- SimpleAPI/
   |
   |-- WallAPI/
```

### En **`models.py`**

**Crea 3** modelos con **estos atributos**:

* `Dashboard`
  * `title`: CharField
  * `summary`: CharField
* `Question`
  * `dashboard`: ForeignKey
  * `title`: CharField
  * `RANDOMEXPRESSIONsummary|content|descriptionEND`: CharField
  * `publication_date`: DateTimeField (usa `auto_now=True`)
* `Answer`
  * `question`: ForeignKey
  * `RANDOMEXPRESSIONsummary|description|contentEND`: CharField
  * `publication_date`: DateTimeField (usa `auto_now=True`)

Estos **nombres** son necesarios para que las bases de datos de _test_ carguen adecuadamente y los **_tests_** funcionen.

Recuerda, también, usar:

```
python manage.py makemigrations
python manage.py migrate
```

...para _propagar los cambios_ a la base de datos.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
