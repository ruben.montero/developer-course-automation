GET /api/v1/questions/{questionId}/answers

## :books: Resumen

* Implementaremos, con menos ayuda, el _endpoint_ para `GET` de _Respuestas (Answers) de una pregunta (Question)_

## Descripción

Previsualiza el fichero de especificación OpenAPI `python-django/docs/swagger/openapi.yaml` desde VisualStudio, como hicimos en una tarea anterior.

Implementaremos el cuarto _endpoint_ `GET /api/v1/questions/{questionId}/answers` tal y como está especificado.

## :package: La tarea

**Añade** una función a `endpoints.py` que servirá para recuperar las _respuestas_ asociadas a una _pregunta_ ordenadas de reciente a antigua.

Será parecida a la versión inicial del `GET` de preguntas, dos tareas atrás. No necesitarás soportar `size` ni `before`.

**Añade** el `to_json` necesario a la clase `Answer` en `models.py`. Esta vez, sólo necesitas _serializar_ la clave `summary` (el atributo se llama `RANDOMEXPRESSIONsummary|description|contentEND`) y `publication_date`.

Por último, **conecta** tu nueva función a la URL correcta en `urls.py`.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.py` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
