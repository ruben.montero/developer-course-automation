Usando atributos, de verdad

## :books: Resumen

* Hablaremos de métodos `getters` y `setters`
* Añadiremos un método `getter` a nuestra clase `Person`
* Añadiremos un método `greeting()` que devuelva un `String` compuesto 

## Descripción

Hemos usado atributos, pero de momento sólo los hemos **escrito**.

La finalidad de almacenar un **estado** en las instancias de nuestras clases es que _sirva_ para algo.

Si es `private`, ¿de qué sirve? ¿Cómo podemos _leerlo_ fuera de la clase?

### Un método [`getter`](https://www.geeksforgeeks.org/getter-and-setter-in-java/)

La respuesta es: Escribiendo un método `getter`. Suele tener el prefijo `get` en su nombre, seguido del nombre del atributo.

Por ejemplo, en `Person.java`:

```java
public class Person {
    private String name;
    
    public Person(String name) {
        System.out.println(name + " acaba de ser instanciado");
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
}
```

Este ejemplo demuestra que podemos tener una clase `Person` con un atributo `name`. Este atributo _sólo se puede modificar una vez_, indicando el valor inicial al **construir** la clase.

Después, únicamente se puede _leer_ invocando `getName()`.

### Todo esto, ¿para qué?

La fuerza de la programación orientada a objetos (POO) es la **encapsulación**.

Es decir, escribimos una clase que es **responsable** de algo, y **sólo** esa clase es la que tiene dicha responsabilidad.

### Un ejemplo

Escribamos un nuevo método en `Person.java`:

* `public`
* Devuelve `String`
* Se llama `greeting`

...que funcione así:

```java
    public String greeting() {
        return "Hola " + this.name;
    }
```

Este método ilustra el **encapsulamiento**.

Aquí, la clase `Person` es responsable de _cómo se saluda_ a una persona.

Imagina que `Person.java` es parte de una aplicación que muestra una página web.

En la página web, hay diferentes sitios donde se saluda al usuario después de _loguearse_ y se le dice:

> Hola {usuario}

Los beneficios:

* Si el día de mañana se quiere cambiar el mensaje por **Bienvenido, {usuario}**, sólo hay que hacerlo en un sitio
* Si se encuentra un problema con el mensaje (e.g. Falta una coma), se sabe **dónde** arreglarlo

## :package: La tarea

**Se pide** que añadas los métodos `getName` y `greeting` como en los ejemplos anteriores, con la siguiente modificación:

* El método `greeting` devolverá `"RANDOMEXPRESSIONHola,|Muy buenas,|Bienvenido/a,|Hola de nuevo,|Me alegro de verte,END " + this.name;`.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
