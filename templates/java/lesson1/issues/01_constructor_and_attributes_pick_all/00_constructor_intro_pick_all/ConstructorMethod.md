Método constructor

## :books: Resumen

* Hablaremos de la _instanciación_ de una clase
* Comprenderemos qué es el método **constructor**
* Crearemos una nueva clase `Person` con un método **constructor** escrito por nosotros, que admita un parámetro

## Descripción

Hasta ahora, cuando hemos **creado** (_instanciado_) una clase, lo hemos hecho con la palabra reservada `new` así:

```java
Greeter miVariableEspecial = new Greeter();
```

Cuando usamos `new` estamos **construyendo** una clase.

### Construyendo... A nuestra manera

Pues bien, podemos especificar un método **especial** denominado [**constructor**](https://javautodidacta.es/constructor-en-java/) en nuestras clases.

Tiene una sintaxis **especial**:

* El tipo de dato devuelto es **igual** al nombre de la clase
* El nombre del método igual al tipo de dato devuelto

## Un ejemplo

En una nueva clase `Person`, se indicaría así:

```java
public class Person {
    public Person() {
        // Este es un método constructor
    }
}
```

Lo interesante del asunto es que dicho método **se ejecuta** al construir la clase. Es decir, al hacer `Person unaPersona = new Person();`

### ¿Podemos pasar parámetros?

**Sí**.

Los ejemplos anteriores, si añademos un parámetro `String`, serían:

```java
public class Person {
    public Person(String name) {
        // Este es un método constructor
    }
}
```

```java
Person unaPersona = new Person("Pepe");
```

### ¿Podemos sobrecargar el método constructor?

Sí.

## :package: La tarea

Para terminar, crea en tu proyecto la clase `Person` mostrada arriba con un método constructor que admita un `String`, como en el ejemplo arriba.

Además:

* En el cuerpo del método constructor, haz un `System.out.println` que indique:

```
name + " acaba de ser instanciado"
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
