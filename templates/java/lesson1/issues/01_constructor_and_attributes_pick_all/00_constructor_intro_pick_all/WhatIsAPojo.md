POJOs

## :books: Resumen

* Introduciremos el concepto de POJO
* Crearemos un POJO llamado `Animal`, con la agilidad que nos otorga IntelliJ IDEA

## Descripción

Las clases que hemos empezado a crear (con atributos privados, constructor, _getters_,...) se usan con mucha frecuencia. Tanto que **tienen nombre propio** y nos podemos referir a ellas como [Plain Old Java Object (POJO)](https://www.javatpoint.com/pojo-in-java)

Son, simplemente, clases básicas con atributos que sirven para _representar_ (modelar) una entidad del mundo.

### Crear un POJO velozmente

Los IDE proporcionan muchas herramientas para agilizar nuestro desarollo.

## :package: La tarea

Prueba lo siguiente:

1. Añade una nueva clase `Animal`
2. En ella, escribe un nuevo atributo `private String name;`
3. Escribe otro atributo más `private String species;`
4. Escribe un último atributo `private int RANDOMEXPRESSIONnumberOfLegs|numberOfEyes|numberOfTeethEND`

Ahora, en la barra superior elige **Code > Generate > Constructor**.

Se desplegará una ventana donde aparecen los tres atributos. Usando la tecla Shift de tu teclado, selecciona los **3** y dale a **OK**.

¡Ya se ha generado el _típico_ constructor automáticamente!

Ahora, ve a **Code > Generate > Getter and Setter**. Selecciona todos los atributos.

¡Tenemos _getters_ y _setters_ listos en un suspiro!

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
