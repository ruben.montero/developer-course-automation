Usando atributos

## :books: Resumen

* Introduciremos los _atributos_
* Añadiremos un _atributo_ `name` a nuestra clase `Person`
* Lo inicializaremos en el método **constructor**

## Descripción

En la primera tarea, comenzamos diciendo que una clase es un **tipo especial** de dato.

Hasta ahora, hemos usado nuestras _clases_ para implementar métodos que consigan distintos **comportamientos** (lógica).

Pero la segunda parte **esencial** de las clases son los **atributos**.

Así, se dice que una clase almacena un **estado** (en sus atributos) y tiene un **comportamiento** (gracias a sus métodos).

### ¿Por ejemplo?

La idea es que una clase _represente_ algo necesario en nuestra aplicación.

Imagina que nuestro programa trabaja con **nombres de personas**:

```java
String name1 = "Lisa Simpson";
String name2 = "Homer J. Simpson";
String name3 = "María del Carmen Jiménez";
```

¿Qué pasa si necesitamos _distinguir_ los _nombres_ de los _apellidos_?

Si, **por ejemplo**, definiéramos una clase **`Name`** que almacenase **2** atributos distintos, el problema estaría solventado.

```java
public class Name {
    String firstName;
    String surname;
    
    // Aquí la clase podría tener métodos
}
```

Como puedes observar, los **atributos** de una clase se declaran como "variables" corrientes y molientes, dentro de ella.

Lo especial es que **pertenecen** a esa clase. O mejor dicho, a la _instancia_.

#### ¿Cómo se usan los atributos?

Se accede a ellos (para _recuperarlos_ o _asignarlos_) mediante un punto (`.`), igual que los métodos.

Por ejemplo:

```java
Name name1 = new Name();
name1.firstName = "Lisa";
name1.surname = "Simpson";

Name name2 = new Name();
name2.firstName = "Homer";
name2.surname = "J. Simpson";

Name name3 = new Name();
name3.firstName = "María del Carmen";
name3.surname = "Jiménez";
```

#### En la práctica los atributos suelen ser privados

El ejemplo anterior no se suele aplicar en la realidad.

Lo normal es que los **atributos** se _inicialicen_ a través del **método constructor**.

Se suele desear que los **atributos** sean privados (`private`) para que no puedan alterarse desde fuera:

```java
public class Name {
    private String firstName;
    private String surname;
    
    // Método constructor
    public Name(String firstName, String surname) {
        this.firstName = firstName;
        this.surname = surname;
    }
}
```

¿Comprendes este ejemplo?

¿Ves para qué se usa `this`?

#### Hhhmmmmmm... Sí...

Quizá se vea más claro si reescribimos el código anterior de forma coherente.

Quedaría así:

```java
Name name1 = new Name("Lisa", "Simpson");
Name name2 = new Name("Homer", "J. Simpson");
Name name3 = new Name("María del Carmen", "Jiménez");
```

Estos ejemplos son **esenciales** y es importante comprender el uso del método constructor y de atributos privados en una clase Java.

### Otro ejemplo más...

Supongamos que existe de una clase `Periodico`:

```java
public class Periodico {
    private String nombre;
    private int dia;
    private int mes;
    private int año;
    
    public Periodico(String nombre, int dia, int mes, int año) {
        this.nombre = nombre;
        this.dia = dia;
        this.mes = mes;
        this.año = año;
    }
}
```

Cada _instancia_ sería como un periódico con distintos valores:

![](java-introduction/docs/newspaper_example.png)

## :package: La tarea

**Se pide** que, en la clase `Person`:

* Añadas un atributo `private` llamado `name`
* En el método constructor, lo inicialices (ademas de hacer un _print_), usando `this.name = name;`

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
