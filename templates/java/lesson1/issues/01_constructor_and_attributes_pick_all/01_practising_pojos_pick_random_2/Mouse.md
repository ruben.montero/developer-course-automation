Hacer click en el ratón

## :books: Resumen

* Crearemos una clase `Mouse` que simbolice el ratón del ordenador
* A través de un atributo, podrá estar configurada para zurdos
* Tendrá dos métodos que simbolizan los _clicks_ principal y secundario
  * En función de la configuración harán dos _print_ distintos

## :package: La tarea

**Se pide** que:

* Crees una nueva clase `Mouse.java` que:
  * Almacene en un atributo privado tipo `boolean` si debe ser para zurdos. Dale al atributo el nombre que quieras. Se sugiere `isLeftHanded`
  * Tenga un método constructor que permita inicializar dicho atributo
  * Tenga un método `public void RANDOMEXPRESSIONclickLeft|leftButton|clickLeftButtonEND()`:
    * Si el ratón está configurado para zurdos, hara un `System.out.println` con el mensaje `Has hecho click secundario`
    * De lo contrario, hará un `System.out.println` con el mensaje `Has hecho click principal`
  * Tenga un método `public void RANDOMEXPRESSIONclickRight|rightButton|clickRightButtonEND()`:
    * Si el ratón está configurado para zurdos, hara un `System.out.println` con el mensaje `Has hecho click principal`
    * De lo contrario, hará un `System.out.println` con el mensaje `Has hecho click secundario`

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
