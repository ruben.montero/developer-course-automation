Moneda y recompensa

## :books: Resumen

* Crearemos una clase `Coin` que represente una moneda
* Tendrá un atributo que represente la **recompensa** por _ganar una tirada_
* Tendrá un método que hará un _print_ y represente una tirada aleatoria

## :package: La tarea

**Se pide** que:

* Crees una nueva clase `Coin.java` (moneda) que:
  * Almacene en un atributo privado tipo `int` la _recompensa_ por ganar una tirada. Dale al atributo el nombre que quieras.
  * Tenga un método constructor que permita inicializar la _recompensa_.
    * Si la _recompensa_ es **0 ó negativa**, lanzarás una excepción así: `throw new RuntimeException("Recompensa no válida");`
  * Tenga un método `RANDOMEXPRESSIONflip|flipCoin|flipTheCoinEND()`:
    * Devolverá un valor tipo `String`
    * Será `public`
    * El valor devuelto consistirá en uno de los dos siguientes mensajes. Se elegirá uno u otro aleatoriamente:
    
```
    "Has tirado al aire la moneda y ha salido cruz. RANDOMEXPRESSIONHas perdido|Perdiste|Lo siento, has perdidoEND"
```

```
    "Has tirado al aire la moneda y ha salido cara. RANDOMEXPRESSIONHas ganado|GanasteEND {recompensa} euros"
```

Donde `{recompensa}` será el valor de _recompensa_ con el que se inicializó la moneda.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
