Tirar el dado

## :books: Resumen

* Crearemos una clase `Dice` que represente un dado
* Tendrá un método que devolverá una tirada aleatoria, en tipo `int`

## :package: La tarea

**Se pide** que:

* Crees una nueva clase `Dice.java` (dado) que:
  * Almacene en un atributo privado tipo `int` el _número de caras_ del dado. Dale al atributo el nombre que quieras.
  * Tenga un método constructor que permita inicializar el _número de caras_.
    * Si el _número de caras_ recibido es **menor que 3**, lanzarás una excepción así: `throw new RuntimeException("Número de caras no válido");`
  * Tenga un método `RANDOMEXPRESSIONroll|rollDice|rollTheDiceEND()`:
    * Devolverá un valor tipo `int`
    * Será `public`
    * El valor devuelto será un número **mayor o igual** que `1`, y **menor o igual** que el _número de caras del dado_.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
