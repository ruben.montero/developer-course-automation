import org.junit.Test;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import static org.junit.Assert.*;

public class TESTFILENAME {

    @Test
    public void testClassConstructorAndException() {
        int num = (int) Math.floor(Math.random()*100) + 3;
        try {
            Class cls = Class.forName("Dice");
            Object instance = cls.getDeclaredConstructor(int.class).newInstance(num);
            assertTrue(true);
        } catch (ClassNotFoundException e) {
            fail("La clase especificada no existe");
        } catch (NoSuchMethodException e) {
            fail("La clase especificada no contiene un metodo con el nombre indicado o con un constructor apropiado");
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            fail("La clase especificada no puede ser instanciada");
        }
        try {
            Class cls = Class.forName("Dice");
            Object instance = cls.getDeclaredConstructor(int.class).newInstance(2);
            fail("Dice is supposed to throw RuntimeException if initialized with 2, 1, 0 or negative");
        } catch (ClassNotFoundException e) {
            fail("La clase especificada no existe");
        } catch (NoSuchMethodException e) {
            fail("La clase especificada no contiene un metodo con el nombre indicado o con un constructor apropiado");
        } catch (IllegalAccessException | InstantiationException e) {
            fail("Error no esperado");
        } catch (InvocationTargetException e) {
            assertTrue(true);
        }
        try {
            Class cls = Class.forName("Dice");
            Object instance = cls.getDeclaredConstructor(int.class).newInstance(1);
            fail("Dice is supposed to throw RuntimeException if initialized with 2, 1, 0 or negative");
        } catch (ClassNotFoundException e) {
            fail("La clase especificada no existe");
        } catch (NoSuchMethodException e) {
            fail("La clase especificada no contiene un metodo con el nombre indicado o con un constructor apropiado");
        } catch (IllegalAccessException | InstantiationException e) {
            fail("Error no esperado");
        } catch (InvocationTargetException e) {
            assertTrue(true);
        }
        try {
            Class cls = Class.forName("Dice");
            Object instance = cls.getDeclaredConstructor(int.class).newInstance(0);
            fail("Dice is supposed to throw RuntimeException if initialized with 2, 1, 0 or negative");
        } catch (ClassNotFoundException e) {
            fail("La clase especificada no existe");
        } catch (NoSuchMethodException e) {
            fail("La clase especificada no contiene un metodo con el nombre indicado o con un constructor apropiado");
        } catch (IllegalAccessException | InstantiationException e) {
            fail("Error no esperado");
        } catch (InvocationTargetException e) {
            assertTrue(true);
        }
        int negativeNum = (int) Math.floor(Math.random()*-100) - 1;
        try {
            Class cls = Class.forName("Dice");
            Object instance = cls.getDeclaredConstructor(int.class).newInstance(negativeNum);
            fail("Dice is supposed to throw RuntimeException if initialized with 2, 1, 0 or negative");
        } catch (ClassNotFoundException e) {
            fail("La clase especificada no existe");
        } catch (NoSuchMethodException e) {
            fail("La clase especificada no contiene un metodo con el nombre indicado o con un constructor apropiado");
        } catch (IllegalAccessException | InstantiationException e) {
            fail("Error no esperado");
        } catch (InvocationTargetException e) {
            assertTrue(true);
        }
    }

    @Test
    public void testValidOutput() {
        int num = (int) Math.floor(Math.random()*100) + 4;
        try {
            Class cls = Class.forName("Dice");
            Object instance = cls.getDeclaredConstructor(int.class).newInstance(num);
            Method method = cls.getDeclaredMethod("RANDOMEXPRESSIONroll|rollDice|rollTheDiceEND");
            boolean atLeastTwoDifferentOutputsHappened = false;
            int output = (int) method.invoke(instance);
            for (int i = 0; i < 100; i++) {
                int otherOutput = (int) method.invoke(instance);
                if (otherOutput != output) {
                    atLeastTwoDifferentOutputsHappened = true;
                }
                assertTrue("Roll result is supposed to be >= 1 and <= numFaces", otherOutput >= 1 && otherOutput <= num);
            }
            assertTrue("The output wasn't really random! I rolled the dice 100 times and it was always the same", atLeastTwoDifferentOutputsHappened);
        } catch (ClassNotFoundException e) {
            fail("La clase especificada no existe");
        } catch (NoSuchMethodException e) {
            fail("La clase especificada no contiene un metodo con el nombre indicado o con un constructor apropiado");
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            fail("La clase especificada no puede ser instanciada");
        }
    }
}
