Una array sencillo, un poco mejor

## :books: Resumen

* Añadiremos un método para imprimir el _enésimo_ elemento de `TravelStops`

## Descripción

Nuestra clase `TravelStops` tiene **3** métodos:

```java
    public void printFirstStop() {
        System.out.println(this.stops[0]);
    }

    public void printSecondStop() {
        System.out.println(this.stops[1]);
    }

    public void printThirdStop() {
        System.out.println(this.stops[2]);
    }
```

¿No sería mejor que tuviera un método para imprimir _cualquier posición del array_?

A ese método:

* Le pasaremos como parámetro un índice
* Se encargará de imprimir la _enésima_ posición correspondiente

¡Hazlo realidad!

## :package: La tarea

Añade:

```java
    public void printStop(int position) {
        System.out.println(this.stops[position]);
    }
```

_(No se pide que valides el índice recibido)_

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
