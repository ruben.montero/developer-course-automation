Una array sencillo, aún mejor

## :books: Resumen

* Añadiremos un método para imprimir _todas_ las paradas de `TravelStops`

## Descripción

Recordemos la sintaxis de un bucle sencillo en Java:

```java
for (int i = 0; i < 100; i++) {
  // Cuerpo del bucle
}
```

## :package: La tarea

**Se pide** que:

* Añadas un nuevo **método** a `TravelStops` llamado `RANDOMEXPRESSIONprintAllStops|printEveryStop|showAllStops|showEveryStopEND`
* Será `public void` y **no** recibirá parámetros
* Imprimirá todas las paradas de `stops`, cada una con `System.out.println`

### Un apunte...

Como sabemos que el tamaño del _array_ es **3**, podemos estar tentados de escribir un bucle como:

```java
for (int i = 0; i < 3; i++) {
```

¡No caigamos en la tentación!

Ese **3** _hardcodeado_ no es correcto. Si, en el futuro[^1], el tamaño del _array_ **cambia**, entonces romperá.

Es mejor usar `.length`, que devuelve el _tamaño_ del _array_:

```java
for (int i = 0; i < this.stops.length; i++) {
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

-------------------------------------------

[^1]: El futuro llega antes de lo que te esperas
