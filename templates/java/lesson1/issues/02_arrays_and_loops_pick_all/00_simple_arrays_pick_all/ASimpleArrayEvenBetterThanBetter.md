Una array sencillo, todavía mejor

## :books: Resumen

* Añadiremos un método para _actualizar_ (modificar) una parada de `TravelStops`

## Descripción

Un _array_ puede ser:

* Inicializado
* Leído (consultado)

...y también podemos **modificar** (actualizar) sus valores.

La sintaxis es sencilla:

```java
miArray[posicion] = nuevoValor;
```

Por ejemplo:

```java
String[] diasDeSemana = new String[] {
  "Lunes",
  "Martes",
  "Miércoles",
  "Jueves",
  "Viernes",
  "Sábado",
  "Domingo"
};

// Actualizamos un valor
// 
diasDeSemana[0] = "Sábado"; // ¡Empezamos con un Sábado en vez de un Lunes!
```

## :package: La tarea

**Se pide** que:

* Añadas un nuevo **método** a `TravelStops` llamado `RANDOMEXPRESSIONupdateStop|modifyStop|changeStopEND`
* Será `public void` y recibirá **2** parámetros:
  * El _índice_ (`int`) de la posición a modificar
  * El **nuevo** valor (`String`)
* Actualizará el valor de la posición del _array_ con el **nuevo** valor

_(No se pide que valides el índice recibido)_

### Una reflexión

¿Por qué no pruebas lo siguiente?

1. Crea una nueva instancia de `TravelStops` desde `Main`
2. Invoca `printFirstStop`
3. Invoca el método que acabas de crear y modifica la primera posición (`0`)
4. Invoca de nuevo `printFirstStop`

¿El resultado es coherente?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
