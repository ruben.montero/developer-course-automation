Una array sencillo

## :books: Resumen

* Repasaremos el concepto de _array_
* Repasaremos la sintaxis para declarar e inicializar un _array_ en Java
* Crearemos una clase `TravelStops` 
  * Almacenará en un atributo privado un _array_
  * Tendrá tres métodos para imprimir sus valores

## Descripción

Un [_array_](https://lenguajesdeprogramacion.net/diccionario/que-es-un-array-en-programacion/) es _una colección ordenada_ de elementos. Podemos interpretarlo como una 'lista':

```java
// Esto es un número
2
```

```java
// Esto son 2 números. ¡Podríamos decir que es un array!
2 8

// ¡Esto también!
-1 9 0 35 6 8

// Y cosas que no son números... ¡También!
"John" "Jane" "Tom" "Builder"
```

### ¿Y en Java?

En Java un _array_ se declara como una variable normal del tipo que sea, con corchetes (`[ ]`) a continuación:

```java
int[] unArrayDeNumeros;
String[] unArrayDeCadenas;
```

### ¿Puede haber _arrays_ con elementos de tipos distintos?

No.

Y sí.

Los _arrays_ siempre son de elementos del **mismo tipo** (`int`, `String`, `MiClase`,...) aunque gracias a la [**herencia**](https://informaticapc.com/poo/herencia.php) en orientación a objetos, pueden ser de un _mismo tipo_ (superclase) que _camufle_ diferentes tipos (subclases). Como no hemos trabajado con herencia, no hablaremos de ello.

### ¿Y cómo se inicializa un _array_?

Podemos **inicializar** un _array_ de **2** formas distintas:

**1)** Especificando su tamaño (se usa `new`):

```java
int[] unArrayDeNumeros = new int[3]; // Habrá disponibles 3 posiciones
String[] unArrayDeCadenas = new String[7];
```

En este caso los valores dentro del _array_ no los conocemos. Sólo la _longitud_ (tamaño) del mismo.

**2)** Especificando sus valores (se usa `new` y llaves `{ }`):

```java
int[] unArrayDeNumeros = new int[] {5, 14, -99};
String[] unArrayDeCadenas = new String[] {
  "Lunes",
  "Martes",
  "Miércoles",
  "Jueves",
  "Viernes",
  "Sábado",
  "Domingo"
};
```

### ¡Me encantan los _arrays_! ¿Qué puedo hacer con ellos?

Muchas cosas[^1].

Para empezar, habrá qué saber cómo _acceder_ a sus elementos.

Esto se hace con la siguiente sintaxis:

```java
// nombre del array e índice entre corchetes
String primerDiaDeSemana = unArrayDeCadenas[0];

// Recuerda que los índices empiezan en 0, no en 1
String tercerDiaDeSemana = unArrayDeCadenas[2];
```

## :package: La tarea

**Se pide** que añadas una clase que representa una lista de destinos para un viaje:

```java
public class TravelStops {

}
```

Los destinos se almacenarán en un atributo privado:

```java
private String[] stops;
```

En el **constructor** de la clase **inicializarás** dicha variable, con estos 3 valores:

_(Este constructor **no** recibirá parámetros. El valor del atributo se inicializará en el cuerpo del método constructor siempre igual)_

```java
"Tokyo"
"Frankfurt"
"RANDOMEXPRESSIONKuala Lumpur|Buenos Aires|Ciudad del CaboEND"
```

### ¿Para qué sirve esta clase?

De momento, será como una _lista_ donde hemos almacenado nuestros destinos y podemos _consultarlos_.

Los _consultaremos_ mediante nuevos **métodos**, llamados `printFirstStop`, `printSecondStop` y `printThirdStop`. Harán lo que [su nombre indica](https://clibre.io/blog/por-secciones/codigo/item/366-el-arte-de-la-programacion):

```java
    public void printFirstStop() {
        System.out.println(this.stops[0]);
    }

    public void printSecondStop() {
        System.out.println(this.stops[1]);
    }

    public void printThirdStop() {
        System.out.println(this.stops[2]);
    }
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

-------------------------------------------

[^1]: Pueden existir incluso _arrays_ [bidimensionales](https://lenguajesdeprogramacion.net/diccionario/que-es-un-array-en-programacion/)
