Un nuevo tipo de bucle

## :books: Resumen

* Introduciremos el bucle _for-each_
* Implementaremos un método en `NewTravelStops` que imprima todas las paradas

## Descripción

En un `ArrayList` podemos recorrer todos los elementos empleando un bucle tradicional y `.get`:

```java
ArrayList<String> finDeSemana = new ArrayList<>();
finDeSemana.add("Sábado");
finDeSemana.add("Domingo");

// Este bucle imprime "Sábado" "Domingo"
for (int i = 0; i < finDeSemana.length; i++) {
    System.out.println(finDeSemana.get(i));
}
```

Pero, aunque ya tenemos las herramientas necesarias, podemos hacerlo **mejor**.

¿Por qué andar _manchándonos_ las manos con índices?

### El bucle [`for-each`](https://www.geeksforgeeks.org/for-each-loop-in-java/)

* Es un bucle `for` especial. No se usan índices
* Recorre los elementos de una colección como `ArrayList` ó `LinkedList`[^1]
* Su sintaxis es la siguiente:
  * Se escribe la palabra `for`
  * Entre paréntesis `( )` se escribe:
    1. El **tipo** de dato que esperamos recorrer
    2. Un **nombre** cualquiera, para referirnos a cada elemento de la colección _dentro_ del bucle
    3. Dos puntos (`:`)
    4. La **variable colección**

### En la práctica

Podríamos reescribir el bucle del ejemplo anterior así:

```java
// Este bucle imprime "Sábado" "Domingo"
for (String dia : finDeSemana) {
    System.out.println(dia);
}
```

Una vez comencemos a usarlos, verás que el código se vuelve más comprensible.

## :package: La tarea

**Se pide** que añadas un método `RANDOMEXPRESSIONprintAllStops|printEveryStop|showAllStopsEND` a `NewTravelStops`:

  * `public`
  * Devolverá `void`
  * No recibe argumentos
  * En el cuerpo, mediante un bucle _for-each_ hará `System.out.println` de cada uno de los elementos de la lista de paradas

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

-------------------------------------------

[^1]: [LinkedList](https://docs.oracle.com/javase/7/docs/api/java/util/LinkedList.html) es una alternativa a `ArrayList`. Difiere en su _implementación_, que consiste en una [lista enlazada](https://es.wikipedia.org/wiki/Lista_enlazada)
