Alternativa fácil a arrays

## :books: Resumen

* Hablaremos de `ArrayList`
* Veremos algunas de sus ventajas
* Implementaremos un `NewTravelStops`, esta vez usando un `ArrayList`

## Descripción

Un [_array_](https://lenguajesdeprogramacion.net/diccionario/que-es-un-array-en-programacion/) es tipo de dato que existe en prácticamente todos los lenguajes de programación. El lenguaje [C](https://en.wikipedia.org/wiki/C_(programming_language)) también permite el uso de _arrays_ y **no** es un lenguaje orientado a objetos (POO).

Sin embargo, existe una manera más _orientada a objetos_ de trabajar con _arrays_: `ArrayList`.

### ¿Qué es [`ArrayList`](https://docs.oracle.com/javase/7/docs/api/java/util/ArrayList.html)?

Se trata de una clase **que ya viene con las librerías esenciales de Java**. Podemos **usarlo** libremente.

Por ejemplo, crearíamos una nueva instancia así:

```java
ArrayList<String> miArrayList = new ArrayList<>();
```

### Un segundo... ¿qué es eso de `<String>`?

Bien visto.

Es una sintaxis **especial** que se usa en ciertas ocasiones (no sólo con `ArrayList`) para indicar el tipo específico de algún dato genérico.

### ¿Y eso qué significa?

En el caso de `ArrayList`, se usa para indicar de **qué tipo** son los elementos _dentro_ del `ArrayList`.

### ¿Con `ArrayList` puedo _añadir_ elementos?

Sí. (En contraposición a un _array_ corriente y moliente).

Hasta ahora hemos **inicializado**, **consultado** y **actualizado** un _array_.

¿Te has preguntado cómo sería **añadir** un elemento a un _array_?

Fácil. ¡No se puede!

Un _array_ tiene un tamaño _fijo_. No se puede _modificar_. La solución más parecida sería crear un **nuevo** _array_ con **1** posición más, copiar **todo** el contenido y luego insertar al final el nuevo elemento.

Algo así:

```java
String[] diasDeSemana = new String[] {"Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"};

// Vamos a añadir un nuevo día a la semana
String nuevosDiasDeSemana = new String[8];
for (int i = 0; i < diasDeSemana.length; i++) {
    nuevosDiasDeSemana[i] = diasDeSemana[i];
}
nuevosDiasDeSemana[7] = "Redomingo";
```

### Qué tedioso

En efecto.

¡Pero usando `ArrayList`, lo conseguimos con una línea!

```java
ArrayList<String> diasDeSemana = new ArrayList<>(List.of("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"));

// Vamos a añadir un nuevo día a la semana
diasDeSemana.add("Redomingo");
```

### Entonces, ¿cómo uso un `ArrayList`?

En ejemplos anteriores hemos visto cómo inicializarlo, vacío o con valores[^1]:

```java
ArrayList<String> diasVacio = new ArrayList<>();
ArrayList<String> diasDeSemana = new ArrayList<>(List.of("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"));
```

...y cómo añadir un valor **al final**:

```java
ArrayList<String> finDeSemana = new ArrayList<>();
finDeSemana.add("Sábado");
finDeSemana.add("Domingo");
```

También, al igual que con un _array_ normal, podemos:

* **Consultar** (recuperar) un valor con `.get()`, pasando el _índice_/_posición_:

```java
// Equivale a String primerDia = dias[0];
String primerDia = dias.get(0);
```

* **Actualizar** un valor con `.set()`, pasando el _índice_/_posición_ y el nuevo valor:

```java
// Equivale a dias[0] = "Lunes";
dias.set(0, "Lunes");
```

## :package: La tarea

**Se pide** que añadas una nueva clase `NewTravelStops`.

En dicha clase, **de momento sólo se pide** que:

* Tenga un atributo privado tipo `ArrayList<String>`
* En el método constructor (que **no** recibirá parámetros), se inicialice con los valores:

```
"Kyoto"
"Singapur"
"RANDOMEXPRESSIONSydney|Manila|HanoiEND"
```

* Tenga un método `public void printStop(int index)` que imprima con `System.out.prinln` el valor de la _parada_ en la posición correspondiente
* Tenga un método `public void RANDOMEXPRESSIONupdateStop|modifyStop|changeStopEND(int index, String newValue)` que _actualice_ la _parada_ en la posición correspondiente, con el nuevo valor indicado
* Tenga un método `public void addStop(String newValue)` que **añada** al final del `ArrayList`, la nueva parada

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

-------------------------------------------

[^1]: https://www.geeksforgeeks.org/initialize-an-arraylist-in-java/
