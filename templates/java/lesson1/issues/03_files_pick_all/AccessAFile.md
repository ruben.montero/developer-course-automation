Ficheros

## :books: Resumen

* Hablaremos del árbol de directorios del sistema operativo
* Veremos cómo leer un fichero línea a línea en Java
* Hablaremos del carácter de escapado (`\`)
* Crearemos una clase `HomeCinemaPreferences` que tendrá un método para leer el fichero `assets\cinemaPrefs.txt` e imprimir su contenido por pantalla

## Descripción

Hasta el momento, todo nuestro trabajo se limita a clases Java y código que manipula variables en la [memoria RAM](https://www.xataka.com/basics/memoria-ram-que-sirve-como-mirar-cuanta-tiene-tu-ordenador-movil) del ordenador.

Escribamos programas que interaccionen con el mundo real. Y por mundo real, nos referimos al disco duro.

### ¿El disco duro (HDD)?

También llamado [**almacenamiento secundario**](https://www.misaelaleman.com/almacenamiento-secundario-en-una-computadora) o **no _volátil_**. En él se almacenan archivos mediante un árbol de directorios.

### ¿Árbol de directorios?

Los ficheros (MP3, vídeos, código Java,...) de nuestro ordenador no se hallan en el disco duro como juguetes en un baúl desordenado. Están muy bien **organizados** mediante carpetas, también llamadas directorios.

En sistemas Unix (Linux, MacOS), la **raíz** del árbol de directorios se señala con un _slash_ (`/`).

En Windows, la **raíz** del árbol de directorios se señala con la letra asignada al disco duro (por ejemplo, `C:\` ó `D:\`). A partir de ahí, hay **carpetas** y **ficheros**. Cada **carpeta** puede contener otras **carpetas o ficheros**, de tal forma que queda organizado **jerárquicamente** como un **árbol**.

Por ejemplo:

```mermaid
graph LR
  C:\-->id1[ProgramFiles];
  C:\-->id2[Drivers];
  C:\-->id3[Windows];
  C:\-->id4[Users];
  id1-->7-zip
  7-zip-->nada[...];
  id1-->Adobe
  Adobe-->otronada[...];
  id1-->Git
  Git-->otronada2[...];
  id2-->id5[...]
  id3-->id6[...]
  id4-->Developer
  Developer-->Documents
  Documents-->MiNuevoLibro.doc
  Developer-->Images
  Developer-->Music
  Music-->Canta_Y_No_Llores.mp3
  Music-->Pocoyo_intro_theme.mp3
```

### Ruta

El concepto de **ruta** de un fichero ó directorio simboliza el _camino_ a seguir para llegar a él, viajando por el árbol de directorios

Una **ruta absoluta** comienza en el directorio **raíz**. Por ejemplo:

```
C:\Users\Developer\Music\Canta_Y_No_Llores.mp3
```

Una **ruta relativa** comienza en un directorio arbitrario. Por ejemplo:

```
Desde el directorio HOME del usuario,
que equivale a C:\Users\Developer

Music\Canta_Y_No_Llores.mp3
```

### Volvamos a Java

En nuestro proyecto existe una carpeta `assets`. Como puedes ver, contiene el archivo `cinemaPrefs.txt`:

```
username=John Doe
prefersDarkMode=true
```

Vamos a suponer que este archivo contiene las **preferencias** para una nueva aplicación que se está desarrollando: `Home Cinema`. Será una aplicación que permita ver el contenido de Netflix, HBO, Amazon Prime y Disney+ de forma gratuita. (No te asustes por problemas legales; no llegaremos a desarrollarla nunca).

## :package: La tarea

Vamos a comenzar escribiendo una nueva clase `HomeCinemaPreferences.java`. Estará destinada a leer este fichero de preferencias e interpretarlas.

Comencemos **añadiendo** la clase y un método constructor sin parámetros:

```java
public class HomeCinemaPreferences {
    public HomeCinemaPreferences() {
        
    }
}
```

Ahora vamos a **leer el fichero** e **imprimir su contenido** por pantalla. Hay [muchas formas](https://www.baeldung.com/reading-file-in-java). Nosotros usaremos [`BufferedReader`](https://docs.oracle.com/javase/7/docs/api/java/io/BufferedReader.html) porque facilita leer _línea a línea_.

El primer paso será definir en un `String` la **ruta** del fichero. Es una ruta **relativa**, comenzando en el directorio del proyecto.

```java
public class HomeCinemaPreferences {
    public HomeCinemaPreferences() {
        String file ="assets\cinemaPrefs.txt";
        
    }
}
```

**¡Vaya!** IntelliJ IDEA lo subraya en rojo e indica un error. Hay ciertos caracteres no imprimibles que se escriben de forma especial dentro de un String:

* `\n` equivale a un _salto de línea_
* `\t` equivale a una _tabulación_
* ...

Como puedes ver, son letras normales (`n`, `t`,...) precedidas del carácter especial `\`. Este **carácter de escapado** se usa para eso: _Dar un significado especial_ a otros caracteres, y así imprimir caracteres **no imprimibles**.

Pero, ¿cómo emplear un _slash_ invertido (`\`) dentro del String?

### _Escapando_ (`\`) el _carácter de escapado_ (`\\`)

Soluciona el problema así:

```diff
-    String file ="assets\cinemaPrefs.txt";
+    String file ="assets\\cinemaPrefs.txt";
```

El siguiente paso es instanciar la clase `FileReader`. Así:

```java
public class HomeCinemaPreferences {
    public HomeCinemaPreferences() {
        String file ="assets\\cinemaPrefs.txt";
        FileReader reader = new FileReader(file);
        
    }
}
```

**¡Vaya!** IntelliJ IDEA lo subraya en rojo de nuevo e indica _Unhandled exception_.

Aún no hemos hablado de **excepciones**, pero, brevemente, son una _forma_ usada en el código de los programas (en Java y otros lenguajes) para señalar errores fuera de lo habitual.

Por ejemplo, ¿qué pasa si _el fichero `cinemaPrefs.txt` no existe_?

De momento, nos conformaremos con _controlar_ dicha excepción empleando el `try/catch` que se genera automáticamente si pasamos el ratón encima del error y seleccionamos **More actions > Surround with try/catch**:

```java
public class HomeCinemaPreferences {
    public HomeCinemaPreferences() {
        String file ="assets\\cinemaPrefs.txt";
        try {
            FileReader reader = new FileReader(file);
            
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
```

### `BufferedReader`

Se crea a partir de un `FileReader`, que es la única razón por la cual _instanciamos_ la variable del paso anterior.

**Construye** un `BufferedReader` así:

```java
public class HomeCinemaPreferences {
    public HomeCinemaPreferences() {
        String file ="assets\\cinemaPrefs.txt";
        try {
            FileReader reader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(reader);
            
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
```

### `.readLine()`

...es el método que debemos _invocar_ sobre `bufferedReader` para leer _una nueva línea_ del fichero.

Cada vez[^1] que lo _invocamos_, la siguiente línea **se devuelve** en formato String.

Como queremos leer dos líneas, podemos hacer así:

```java
public class HomeCinemaPreferences {
    public HomeCinemaPreferences() {
        String file ="assets\\cinemaPrefs.txt";
        try {
            FileReader reader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String firstLine = bufferedReader.readLine();
            String secondLine = bufferedReader.readLine();
            
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
```

**¡Vaya!** IntelliJ IDEA lo subraya en rojo de nuevo e indica _Unhandled exception_. Ahora, controlar la excepción `FileNotFoundException` no es suficiente, porque pueden darse _otros errores_. Por ejemplo, si no disponemos de _privilegios_ para leer el fichero.

Basta con sustituir `FileNotFoundException` for el error más general `IOException`:

```diff
public class HomeCinemaPreferences {
    public HomeCinemaPreferences() {
        String file ="assets\\cinemaPrefs.txt";
        try {
            FileReader reader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String firstLine = bufferedReader.readLine();
            String secondLine = bufferedReader.readLine();
            
-        } catch (FileNotFoundException e) {
+        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
```

### Lectura secuencial vs. aleatoria

Como puedes comprender, con `BufferedReader` leemos el fichero de forma **secuencial**. Aunque no trabajaremos con ella, existe otra clase `RandomAccessFile` que permite acceder a un fichero y [leer cualquuier parte de su contenido de forma directa](https://doctortecnologico.com/web/cuales-son-las-formas-de-acceder-a-un-fichero-java/). Esto es, acceso **aleatorio**. El acceso **secuencial** es más rápido pero menos flexible (estamos obligados a ir línea a línea).

### `.readLine()` _loop_

Nuestra forma para leer el fichero es válida, porque esperamos **2** líneas. Pero es un poco mala.

¿Y si se añaden más líneas? Dejará de funcionar.

Es habitual emplear un bucle `do-while` para leer ficheros.

1. Se declara fuera del bucle la variable `String newLine;`
2. Dentro de `do { ... }` se asigna la nueva línea. **Si no hay más líneas**, `.readLine()` devolverá `null`
3. En la condición de salida del bucle `while( ... )` especificamos que _el bucle debe repetirse **mientras** la línea (`newLine`) **no** sea `null`_

```diff
public class HomeCinemaPreferences {
    public HomeCinemaPreferences() {
        String file ="assets\\cinemaPrefs.txt";
        try {
            FileReader reader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(reader);
-            String firstLine = bufferedReader.readLine();
-            String secondLine = bufferedReader.readLine()
+            String newLine = null;
+            do {
+                newLine = bufferedReader.readLine();
+            } while (newLine != null);
            
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
```

¿Se entiende?

### La prueba de fuego

Añadamos una sentencia dentro del bucle para imprimir por consola cada línea que leemos:

```diff
public class HomeCinemaPreferences {
    public HomeCinemaPreferences() {
        String file ="assets\\cinemaPrefs.txt";
        try {
            FileReader reader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String newLine = null;
            do {
                newLine = bufferedReader.readLine();
+                System.out.println(newLine);
            } while (newLine != null);
+            // Un detalle: Cerrar el fichero tras su uso
+            fileReader.close();
            
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
```

Si al instanciar esta clase desde `Main.java`:

```java
        HomeCinemaPreferences prefs = new HomeCinemaPreferences();
```

...vemos el siguiente contenido en la consola:

```
username=John Doe
prefersDarkMode=true
null
```

¡Funciona correctamente!

**¡Enhorabuena!** Has leído con éxito tu primer fichero de preferencias en Java.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

-------------------------------------------

[^1]: No hace falta que llevemos la cuenta de _qué_ línea estamos leyendo. `BufferedReader` lo hace automáticamente. ¿Sabes qué significa eso? ¡Efectivamente! Almacena **internamente** el estado (en un atributo) de **qué** línea está leyendo
