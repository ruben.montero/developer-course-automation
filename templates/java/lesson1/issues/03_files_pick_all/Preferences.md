Almacenando preferencias en atributos

## :books: Resumen

* Añadiremos dos atributos privados y dos `getters` a `HomeCinemaPreferences`
* Escribiremos un nuevo método privado que _parsee_ una línea del fichero
* Usaremos el método para _parsear_ todas las líneas del fichero

## :package: La tarea

Continuemos trabajando en nuestro `HomeCinemaPreferences`.

Ahora, vamos a añadir dos atributos privados a la clase:

```java
    private String username;
    private boolean darkModePreferred;
```

**Añadamos también** dos `getters` llamados `getUsername` e `isDarkModePreferred`. Recuerda que puedes hacerlo automáticamente.

### Un método que interprete una línea

Vamos a añadir un nuevo método **privado** a la clase:

```java
    private void parseLine(String line) {

    }
```

Lo invocaremos desde el métood **constructor** y _delegaremos_ en él la responsabilidad de interpretar la línea.

```diff
    public HomeCinemaPreferences() {
        String file ="assets\\cinemaPrefs.txt";
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String newLine = null;
            do {
                newLine = bufferedReader.readLine();
                System.out.println(newLine);
+                // Invocamos el método que trabaja línea a línea
+                parseLine(newLine)
            } while (newLine != null);
            // Un detalle: Cerrar el fichero tras su uso
            fileReader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
```

Este método recibe por parámetro un String:

1. Si es la preferencia _username_, la guarda en el atributo `username`
2. Si es la preferencia _prefersDarkMode_, la guarda en el atributo `darkModePreferred`

### Condición de guarda

El primer paso será verificar que el parámetro tipo `String` que recibimos **no** es **nulo**.

En caso de que lo sea, _retornaremos_ de forma _temprana_.

Es lo que se conoce como una condición de guarda:

```java
    private void parseLine(String line) {
        if (line == null) {
            return;
        }
        // ...
    }
```

### Usando `.split()`, de `String`

Ahora vamos a separar la línea en **2** partes. Esto lo conseguimos con [`.split()`](https://www.geeksforgeeks.org/split-string-java-examples/), que _recibe un carácter_ y devuelve el String _troceado_ en piezas separadas por ese _carácter_.

```java
    private void parseLine(String line) {
        if (line == null) {
            return;
        }
        String[] separatedString = line.split("=");
        String firstHalf = separatedString[0];
        String secondHalf = separatedString[1];
        // ...
    }
```

En este momento debemos ser capaces de responder a:

* ¿Qué valor tiene `firstHalf` para la **primera** línea del fichero? ¿Y `secondHalf`?
* ¿Qué valor tiene `firstHalf` para la **segunda** línea del fichero? ¿Y `secondHalf`?

Si no es así, escribe sentencias `System.out.println` que arrojen algo de luz.

### Si la línea empieza por `username=`...

Entonces, al atributo `this.username` queremos asignarle el valor que aparece en el fichero.

Esto se escribe en código así:

```java
    if (firstHalf == "username") { // OJO: MAL
        this.username = secondHalf;
    }
```

## [`.equals()`](https://www.javatpoint.com/java-string-equals)

¡Un momento! **No** podemos comparar `Strings` (ni cualquier `Objeto` complejo) usando `==`.

Está **mal**.

En Java, `==` sólo sirve para **tipos primitivos**. Los objetos se comparan con [`.equals()`](https://www.javatpoint.com/java-string-equals). Así:

```diff
-    if (firstHalf == "username") { // OJO: MAL
+    if (firstHalf.equals("username")) { // Bien
        this.username = secondHalf;
    }
```

### Y si la línea empieza por `prefersDarkMode=`...

Haremos algo parecido:

```java
    if (firstHalf.equals("username")) {
        this.username = secondHalf;
    }
    if (firstHalf.equals("prefersDarkMode")) {
        this.darkModePreferred = secondHalf;
    }
```

Parece que IntelliJ IDEA marca un **error**, ¿no?

Es esperable.

* `secondHalf` es de tipo `String`.
* `this.darkModePreferred` es de tipo `boolean`

¡No podemos asignarlos!

### Pensemos en una solución

Seremos capaces de llegar a ella.

Una vez corrijamos este problema, podemos verificar los valores de los atributos de `HomeCinemaPreferences` con `.getUsername()` y `.isDarkModePreferred()`.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
