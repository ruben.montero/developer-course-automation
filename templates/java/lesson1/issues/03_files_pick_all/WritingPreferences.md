Almacenando preferencias en atributos

## :books: Resumen

* Añadiremos dos métodos `setters` a `HomeCinemaPreferences`
* Escribiremos un nuevo método escribe las preferencias en el fichero `cinemaPrefs.txt`

## Descripción

De forma análoga a `FileReader` y `BufferedReader`, podemos usar:

* `FileWriter`
* `BufferedWriter`

El siguiente método almacenaría en disco los valores de las preferencias:

```java
    public void RANDOMEXPRESSIONwriteToFile|saveToFile|save|writeEND() {
        String file ="assets\\cinemaPrefs.txt";
        try {
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            // Escribimos la primera línea
            bufferedWriter.write("username=");
            bufferedWriter.write(this.username);
            bufferedWriter.newLine();
            // Escribimos la segunda línea
            if (this.darkModePreferred) {
                bufferedWriter.write("prefersDarkMode=true");
            } else {
                bufferedWriter.write("prefersDarkMode=false");
            }
            bufferedWriter.newLine();
            bufferedWriter.flush(); // Guardamos el fichero a disco
            fileWriter.close(); // Y lo cerramos
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
```

## :package: La tarea

**Se pide** que añadas el método anterior y también métodos `setters` para manipular los atributos.

¡Ahora ya puedes usar la clase para leer, modificar, y guardar las preferencias!

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
