Sintonizando la radio

## :books: Resumen

* Se pedirá una clase que permita guardar un JSON con información de emisoras FM en un rango de frecuencias

## Descripción

La [radio](https://en.wikipedia.org/wiki/Invention_of_radio) es una invención de principios del siglo XX en cuyo nacimiento jugó un papel importantísimo [Guglielmo Marconi](https://en.wikipedia.org/wiki/Guglielmo_Marconi), entre otros.

Las ondas de radio de _frecuencia modulada_ (FM) hoy en día son emitidas a nivel mundial por entretenimiento. Basta con conocer la frecuencia de una emisora para poder sintonizar correctamente un receptor.

Algunas emisoras de Coruña son:

|Nombre       |Frecuencia (MHz)|
|-------------|-----------|
|Kiss FM      |88.0       |
|Cadena 100   |88.7       |
|Onda Cero    |89.2       |
|Los 40       |91.0       |
|COPE         |96.9       |
|Los 40 Classic | 97.6    |
|Hit          |100.7      |
|esRadio      |102.7      |
|Rock FM      |103.2      |
|Cuac FM      |103.4      |
|Radio Galega |104.8      |

## :package: La tarea

Crearemos una clase `RadioInfo`. Esta clase servirá únicamente para **generar** un JSON que contenga una lista con información sobre las emisoras:

Tendrá un método `RANDOMEXPRESSIONgenerateJSON|createJSON|saveJSON|produceJSON|storeJSONEND`:

* `public void`, recibe **2** parámetros tipo `double`
  * `minFrequency`. Debe ser `> 0` y `< maxFrequency`. De lo contrario, se lanzará una excepción con `throw new RuntimeException();`
  * `maxFrequency`. Debe ser `> 0` y `> minFrequency`. De lo contrario, se lanzará una excepción con `throw new RuntimeException();`
* Este método _seleccionará_ la información de las emisoras cuya frecuencia se encuentra **entre** `minFrequency` y `maxFrequency`
* Generará un objeto JSON y lo **guardará** en `assets\radios.json`
* Tendrá una forma _como_ la siguiente:

_(Ejemplo para el caso de `minFrequency=50` y `maxFrequency=100`)_

```json
[
  {
    "radioName": "Kiss FM",
    "radioFrequency": 88
  },
  {
    "radioName": "Cadena 100",
    "radioFrequency": 88.7
  },
  {
    "radioName": "Onda Cero",
    "radioFrequency": 89.2
  },
  {
    "radioName": "Los 40",
    "radioFrequency": 91
  },
  {
    "radioName": "COPE",
    "radioFrequency": 96.9
  },
  {
    "radioName": "Los 40 Classic",
    "radioFrequency": 97.6
  },
]
```

_(Ejemplo para el caso de `minFrequency=104` y `maxFrequency=500`)_

```json
[
  {
    "radioName": "Radio Galega",
    "radioFrequency": 104.8
  }
]
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
