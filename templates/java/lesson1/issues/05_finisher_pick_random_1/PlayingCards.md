Repartiendo unas cartas

## :books: Resumen

* Se pedirá una clase que tenga un método constructor y un método público que almacene un JSON con información de la repartición inicial de cartas de una baraja de naipes para `N` jugadores

## Descripción

[Brisca](https://es.wikipedia.org/wiki/Brisca). [Tute](https://en.wikipedia.org/wiki/Tute). [Escoba](https://www.nhfournier.es/como-jugar/escoba/). [Chinchón](https://www.nhfournier.es/como-jugar/chinchon/). Existen muchísimos juegos de cartas que se juegan con una baraja. La mayoría tiene algo en común: **Inicialmente hay que repartir cartas a los jugadores**.

### ¿Qué cartas?

En nuestro caso, no serán cartas reales. Únicamente trabajaremos con _identificadores_ (códigos) de cartas.

Así:

|            |Oros|Copas|Espadas|Bastos
|------------|----|-----|-------|------
|**1**       |O1  |C1   |E1     |B1
|**2**       |O2  |C2   |E2     |B2
|**3**       |O3  |C3   |E3     |B3
|**4**       |O4  |C4   |E4     |B4
|**5**       |O5  |C5   |E5     |B5
|**6**       |O6  |C6   |E6     |B6
|**7**       |O7  |C7   |E7     |B7
|**Sota**    |OS  |CS   |ES     |BS
|**Caballo** |OC  |CC   |EC     |BC
|**Rey**     |OR  |CR   |ER     |BR

## :package: La tarea

Crearemos una clase `CardDistributer`.

Tendrá un método constructor que reciba **2** parámetros tipo `short`. Son:

* El número de jugadores.
  * Debe ser `> 0` y `<= RANDOMEXPRESSION3|4END`. De lo contrario, se lanzará una excepción con `throw new RuntimeException();`
* El número de cartas por jugador.
  * Debe ser `> 0` y `<= RANDOMEXPRESSION8|9|10END`. De lo contrario, se lanzará una excepción con `throw new RuntimeException();`
  
Esta clase servirá únicamente para **simular** la repartición inicial de cartas en una partida, donde _no_ está prefijado el número de jugadores ni el tamaño de la mano de cartas, sino que nos adaptamos a lo pasado por **parámetro** en el método constructor.

Dicha _repartición_ se hará  mediante un método `RANDOMEXPRESSIONgenerateJSON|createJSON|saveJSON|produceJSON|storeJSONEND`:

* `public void`, **no** recibe parámetros
* Generará un objeto JSON y lo **guardará** en `assets\cards.json`
* Tendrá una forma _como_ la siguiente:

_(Ejemplo para el caso de `número de jugadores=3` y `número de cartas=3`)_

```json
{
  "player1": ["O5", "C1", "E1"],
  "player2": ["O7", "C3", "C2"],
  "player3": ["BS", "CR", "E5"]
}
```

_(Ejemplo para el caso de `número de jugadores=2` y `número de cartas=4`)_

```json
{
  "player1": ["O2", "CR", "ER", "BS"],
  "player2": ["C2", "E6", "E3", "C1"]
}
```

Nótese que:

* Las cartas asignadas a cada jugador son elegidas aleatoriamente
* **No** pueden repetirse (¡no se puede dar la misma carta 2 veces!)

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
