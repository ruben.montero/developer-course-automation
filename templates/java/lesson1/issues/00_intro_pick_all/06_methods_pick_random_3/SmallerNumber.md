El menor número

## :books: Resumen

* Escribiremos un método nuevo en `SimpleMathDemo` que reciba **4** valores enteros y _devuelva_ el menor de ellos

## :package: La tarea

**Se pide** que:

* Añadas un nuevo método `RANDOMEXPRESSIONsmallerNumber|getSmallerNumber|getTheSmallerValueEND` a `SimpleMathDemo`:
  * `public`
  * Devolverá un tipo `int`
  * Recibirá (parámetros formales) **4** datos tipo `int`
  * En el _cuerpo_ del método, implementarás la _lógica_ necesaria para devolver finalmente (`return`) el **menor** de los **4** valores
  
### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
