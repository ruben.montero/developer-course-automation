Un número aleatorio entre dos valores que definen un rango

## :books: Resumen

* Escribiremos un método nuevo en `SimpleMathDemo` que reciba **2** valores enteros, `start` y `length`
* Devolverá un número entero **aleatorio** en el rango definido por esos valores

## Descripción

Un rango es un espacio de valores entre un mínimo y un máximo.

Por ejemplo, podemos hablar del rango de valores **entre 0 y 100**, ó **entre 56 y 205**.

```
[0, 100]
[56, 205]
```

Una forma habitual de _definir_ un rango en informática es usar:

* Valor mínimo del rango (extremo izquierdo)
* Longitud del rango (tamaño)

Llamémoslos `start` y `length`.

### Un ejemplo

Para `start` = `0` y `length` = `10`, el rango será:

```
[0, 10]
``` 

Para `start` = `10` y `length` = `10`, el rango será:

```
[10, 20]
``` 

Para `start` = `25` y `length` = `5`, el rango será:

```
[25, 30]
``` 

## :package: La tarea

**Se pide** que:

* Añadas un nuevo método `RANDOMEXPRESSIONrandomNumberInRange|getRandomNumberInRangeEND` a `SimpleMathDemo`:
  * `public`
  * Devolverá un tipo `int`
  * Recibirá (parámetros formales) **2** datos tipos `int`
    * El primero se llamará `start`
    * El segundo se llamará `length`
  * En el _cuerpo_ del método, implementarás la _lógica_ necesaria para devolver finalmente (`return`) un número entero **aleatorio** comprendido en el rango definido por `start` y `length`:
    * **Mayor o igual** que `start`
    * **Menor estrictamente** que `start+length`
  * Si detectas _algún problema_ con los valores de entrada, devolverás un `RANDOMEXPRESSION0|-1END`[^1]
  
### Por ejemplo

Si desde `Main.java` invocas el método así:

```java
SimpleMathDemo demo = new SimpleMathDemo();
int result = demo.RANDOMEXPRESSIONrandomNumberInRange|getRandomNumberInRangeEND(3, 5);
// Imprimimos el valor devuelto para comprobarlo
System.out.println(result);
```

...se espera que el valor devuelto sea `3`, `4`, `5`, `6` ó `7`, _aleatoriamente_.

### ¿Números aleatorios?

Puedes generar un número aleatorio _decimal_ entre `0` y `1` con `Math.random()`, así:

```java
double randomNumber = Math.random();
```

Puedes convertir un número _decimal_ a un _entero_ (truncándolo), así:

```java
int integerValue = (int) Math.floor(randomNumber);
```

_(También hay otras formas)_

_(Puedes probar a [buscar en Internet](https://stackoverflow.com/questions/5271598/java-generate-random-number-between-two-given-values))_
  
### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------

[^1]: ¿Crees que esta forma de gestionar los casos de error es adecuada? ¿Se te ocurre alguna forma en la que pueda dar problemas? ¿Cómo lo solucionarías?
