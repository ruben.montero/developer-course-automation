Un número aleatorio entre dos valores

## :books: Resumen

* Escribiremos un método nuevo en `SimpleMathDemo` que reciba **2** valores enteros
* Devolverá un número entero **aleatorio** entre ambos

## :package: La tarea

**Se pide** que:

* Añadas un nuevo método `RANDOMEXPRESSIONrandomNumberBetween|getRandomNumberBetweenEND` a `SimpleMathDemo`:
  * `public`
  * Devolverá un tipo `int`
  * Recibirá (parámetros formales) **2** datos tipos `int`
    * El primero se llamará `min`
    * El segundo se llamará `max`
  * En el _cuerpo_ del método, implementarás la _lógica_ necesaria para devolver finalmente (`return`) un número entero **aleatorio** comprendido entre `min` y `max`:
    * **Mayor o igual** que `min`
    * **Menor estrictamente** que `max`
  * Si detectas _algún problema_ con los valores de entrada, devolverás un `RANDOMEXPRESSION0|-1END`[^1]
  
### Por ejemplo

Si desde `Main.java` invocas el método así:

```java
SimpleMathDemo demo = new SimpleMathDemo();
int result = demo.RANDOMEXPRESSIONrandomNumberBetween|getRandomNumberBetweenEND(1, 5);
// Imprimimos el valor devuelto para comprobarlo
System.out.println(result);
```

...se espera que el valor devuelto sea `1`, `2`, `3` ó `4`, _aleatoriamente_.

### ¿Números aleatorios?

Puedes generar un número aleatorio _decimal_ entre `0` y `1` con `Math.random()`, así:

```java
double randomNumber = Math.random();
```

Puedes convertir un número _decimal_ a un _entero_ (truncándolo), así:

```java
int integerValue = (int) Math.floor(randomNumber);
```

_(También hay otras formas)_

_(Puedes probar a [buscar en Internet](https://stackoverflow.com/questions/5271598/java-generate-random-number-between-two-given-values))_

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------

[^1]: ¿Crees que esta forma de gestionar los casos de error es adecuada? ¿Se te ocurre alguna forma en la que pueda dar problemas? ¿Cómo lo solucionarías?
