El mayor número

## :books: Resumen

* Escribiremos un método nuevo en `SimpleMathDemo` que reciba **3** valores enteros y _devuelva_ el mayor de ellos

## :package: La tarea

**Se pide** que:

* Añadas un nuevo método `RANDOMEXPRESSIONbiggerNumber|getBiggerNumber|getTheBiggerNumberEND` a `SimpleMathDemo`:
  * `public`
  * Devolverá un tipo `int`
  * Recibirá (parámetros formales) **3** datos tipo `int`
  * En el _cuerpo_ del método, implementarás la _lógica_ necesaria para devolver finalmente (`return`) el **mayor** de los **3** valores
  
### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
