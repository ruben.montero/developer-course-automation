Comprendiendo el espacio de trabajo

## :books: Resumen

* Veremos una introducción breve a Java
* Comprenderemos cómo está montado el espacio de trabajo
* Crearemos una clase `Greeter` con un método `sayHello`
* Probaremos el _test_ automático de la tarea
* Distinguiremos _ejecutar `Main.java`_ y _ejecutar tests_
* Haremos `git add`, `git commit` y `git push` para subir los cambios al repositorio

## Descripción

[Java](https://es.wikipedia.org/wiki/Java_%28lenguaje_de_programaci%C3%B3n%29) es un lenguaje de programación [orientado a objetos](https://profile.es/blog/que-es-la-programacion-orientada-a-objetos/). Nos otorga el poder de escribir el código fuente de programas que se [compilarán](https://www.alegsa.com.ar/Dic/compilacion.php) y funcionarán en cualquier ordenador, independientemente de su arquitectura, con tal de que la [máquina virtual de Java (JVM)](https://creatividadcodificada.com/java/que-es-la-maquina-virtual-de-java-jvm/) esté disponible.

Para comenzar a trabajar con Java, lo primero será _bajar_ este repositorio a tu disco duro.

Si ya lo inicializaste, puedes saltarte el siguiente apartado.

### Configuración de Git e inicialización del repositorio

**Instala** [Git](https://git-scm.com/download/win).

Después, **genera** una clave SSH[^1] en tu máquina local. Para ello, abre un terminal (tecla de Windows > cmd.exe) y escribe:

```
ssh-keygen -t rsa
```

**Pulsa** ENTER varias veces para completar el proceso. Luego, **abre** la carpeta `.ssh` que se habrá creado en tu directorio de usuario[^2]. Allí verás dos archivos `id_rsa` e `id_rsa.pub`. **Abre** (con el Bloc de Notas) el archivo `id_rsa.pub` y **copia** (CTRL+C) su contenido.

Después, **navega** a https://raspi y haz _login_ con tu cuenta de usuario. Arriba a la derecha, abre el _popup_ de ajustes y **navega** a `Settings`.

A la izquierda verás un panel lateral. Busca `SSH and GPG keys` y **haz click** ahí.

En esa pantalla, verás un recuadro de texto grande. **Pega** (CTRL+V) tu clave pública y **pulsa** en el botón verde `Add key` más abajo.

¡Clave configurada!

Ahora, desde tu terminal (cmd.exe) **clona** (descarga) el repositorio. Para ello, **escribe**:

```
git clone <url_de_tu_repositorio>
```

Donde `<url_de_tu_repositorio>` será la URL que puedes encontrar en https://raspi, yendo a tu proyecto y desde el botón azul de _Clone_:

<img src="java-introduction/docs/ssh_url_gitlab.png" width=400 />

¡Acabas de obtener la carpeta `ad/` (tu repositorio)!

Para terminar la configuración, desde el terminal, **ejecuta**:

```
git config --global user.name "Nombre Apellido"
git config --global user.email "nombre.apellido@fpcoruna.afundacion.org"
```

¡**Sustituye** correctamente `"Nombre Apellido"` y `"nombre.apellido@fpcoruna.afundacion.org"` por tu nombre real y tu correo institucional! (Debes mantener las comillas `" "`).

### Estructura del proyecto

La carpeta `java-introduction/` debe haber aparecido en la carpeta de tu repositorio local.

Abre [**IntelliJ IDEA Community**](https://www.jetbrains.com/idea/) y selecciona `File > Open`. Busca la carpeta `ad/java-introduction/` y elígela. Puedes darle a `Trust Project` sin problema. A la izquierda aparecerá la jerarquía de carpetas:

```
- .idea/

- assets/

- docs/

- lib/

- src/
   |
   |- Main.java
   
- tests/
```

* `.idea/` contiene información que IntelliJ IDEA guarda automáticamente (configuración del editor, etc.)
* `assets/` contiene ficheros que usaremos más adelante
* `docs/` contiene una imagen con un ejemplo. En general, es buena idea tener una carpeta separada para _documentación_
* `lib/` contiene librerías `.jar` con _bytecode Java_ ya compilado y listo para la JVM. De momento, sólo hay 3 librerías. `junit` y `hamcrest` se necesitan para los _tests_. `org.json:json` sirve para manipular ficheros [JSON](https://file.org/extension/json). Las usaremos más adelante.
* `src/` contiene **nuestro** código fuente, (ó _source code_). ¡Es la más importante! Única e irremplazable.
* `tests/` contienen _tests_ automáticos para cada tarea. Cuando terminemos una tarea, podremos comprobar que está bien hecha lanzando el _test_ automático asociado. 

Como puedes observar, la clase `src/Main.java` **ya** tiene código escrito:

```java
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }
}
```

...es el típico [Hello world](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program) en Java. Podemos ejecutarlo, pero antes hay que añadir una _Configuración de ejecución_:

## :package: La tarea

Tras tener tu repositorio clonado, y la carpeta `java-introduction/` abierta desde IntelliJ IDEA como se indica más arriba:

1. Dale a _Add Configuration..._ en la barra superior, en el centro-derecha.
2. Selecciona _Add new..._, y en el menú desplegable, _Application_ (el segundo elemento)
3. Puedes ponerle otro nombre (mejor **Main** en vez de **Unnamed**)
4. Hay que seleccionar _qué Main class_ se ejecutará. Basta con teclear **Main** en el cuadro de texto resaltado, o bien seleccionar en el pequeño icono y darle a **OK** en la ventana que aparece.
5. Dale a **OK**

Ya aparecerá un **botón verde 'Play'**. Pulsémoslo. Alternativamente, podemos pulsar Mayus+F10. Ésto lanzará la _Configuración_ `Main` que acabamos de crear, y, así, se ejecutará el método _estático_ `main`[^3].

¡Pruébalo!

Verás algo como esto:

```
C:\Users\Developer\.jdks\openjdk-18.0.1.1\bin\java.exe "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.1.2\lib\idea_rt.jar=16763:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.1.2\bin" -Dfile.encoding=UTF-8 Main
Hello world!

Process finished with exit code 0
```

Ahora, haz **click derecho** en la carpeta `src/` y selecciona `New > Java Class`. En el diálogo que aparece, escribe `Greeter` y dale a ENTER. 

Un nuevo fichero `Greeter.java` se ha creado con el siguiente contenido[^4]:

```java
public class Greeter {
}
```

### Usando `Greeter`

En la clase `Main.java`, dentro del método `main`, **declara** e **inicializa** una nueva variable `Greeter` así:

```java
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Greeter greeter = new Greeter();
    }
}
```

### Ya tengo mi variable... ¿Y ahora?

Dispones de todo el potencial que te otorgue la clase `Greeter`.

Por el momento, ese potencial es **absolutamente nada**.

Vamos a escribir **un método** en la clase `Greeter` para que sirva para algo.

### ¿Qué es un [método](https://www.javatpoint.com/method-in-java)?

Es un fragmento de código que se puede _invocar_ (o _llamar_). Prácticamente lo mismo que una función o un procedimiento en programación estructurada.

Se escriben **dentro** de una clase (**¡Ojo!** **No** puedes escribir un método _dentro de otro método_, o en lugares _raros_).

Constan de las siguientes partes:

1. Visibilidad: `public`, `package`, `protected` ó `private`. Si no es especifica, [por defecto](https://devtut.github.io/java/visibility-controlling-access-to-members-of-a-class.html#package-visibility) es `package` 
2. Tipo de dato devuelto. Puede ser un dato primitivo (e.g.: `int`), una clase (e.g.: `Greeter`) o `void` si el método _no devuelve nada_
3. Nombre del método, escrito en lowerCamlCase.
4. Paréntesis indicando los argumentos del método. Si no recibe argumentos, se escriben paréntesis vacíos (`()`) igualmente.
5. Cuerpo del método entre llaves (`{ }`)

**Escribe** el siguiente método **dentro** de `Greeter.java`:

```java
public class Greeter {
    public void sayHello() {
        System.out.println("Hello world!");
    }
}
```

_(¿Identificas cada una de las partes mencionadas anteriormente?)_

### Invocar el método

Ahora, el método está **escrito** y está **dentro** de la clase, pero no va a pasar _nada_ mientras no lo _invoquemos_

Recuerda que el punto de entrada de la aplicación es `Main.java`, así que desde ahí podemos invocarlo:

```java
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Greeter greeter = new Greeter();
        greeter.sayHello();
    }
}
```

**Ejecútalo**.

En la salida de ejecución verás que aparece `Hello world!` **2** veces.

Borraremos la siguiente línea para que sólo aparezca **1**:

```diff
public class Main {
    public static void main(String[] args) {
-       System.out.println("Hello world!");
        Greeter greeter = new Greeter();
        greeter.sayHello();
    }
}
```

## :play_pause: Lanzar _tests_

Nuestro método de trabajo consistirá en ir modificando `Main.java` e ir creando clases para conseguir distintos objetivos.

Cuando terminemos, lanzaremos un _test_ automático que validará nuestro trabajo.

Para esta primera tarea, **despliega** la carpeta `tests/` a la izquierda y elige TESTFILENAME. Haz:

* Click derecho > **'Play' verde** Run 'TESTFILENAME'

Se ejecutará el _test_ automáticamente y en la ventana que se despliega abajo, a la izquierda verás _ticks_ verdes e indicaciones de que el _test_ ha sido exitoso.

### Lanzar `Main` vs. lanzar _tests_

¡Ojo! Verás que arriba, donde le solíamos dar al **'Play' verde** para ejecutar `Main` **ya no pone `Main`**. Ahora indica TESTFILENAME.

Si queremos volver a ejecutar `Main.java`, deberemos hacer _click_ en el desplegable y seleccionar **Run configurations > Main**.

### :trophy: Por último

La tarea ha sido completada. Ahora, desde el terminal de Windows (cmd) escribiremos:

```
git add *
```

...para añadir los cambios a un nuevo _commit_. Después:

```
git commit -m "Completada tarea 1"
```

...y por último subimos los cambios al repositorio remoto:

```
git push
```

No está de más visitar la página de GitLab y verificar que el _commit_ se ha subido.

------------------------------------------------------------------------

[^1]: En realidad son un par de claves: La pública y la privada.

[^2]: Tu directorio de usuario está dentro de la ruta `C:\Users`. Por ejemplo, si tu usuario se llama `PepeDepura`, será `C:\Users\PepeDepura`.

[^3]: ¿Te has preguntado por qué el método tiene `String[] args`?

[^4]: `public` es un modificador de visibilidad. Se escribe antes de nombres de clases, métodos o atributos, y sirve para indicar que _aquello a lo que modifica_ debe ser _visible_ (accesible) desde _cualquier otra parte del código del programa_.
