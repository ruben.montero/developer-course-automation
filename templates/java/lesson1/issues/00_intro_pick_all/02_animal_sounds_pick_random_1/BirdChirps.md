Otra clase sencilla

## :books: Resumen

* Escribiremos una nueva clase con un nuevo método sencillo
* Lo invocaremos como hicimos con `Greeter` y `.sayHello()`
* _(Y como siempre debemos hacer, subiremos los cambios al repositorio)_

## Descripción

Hasta ahora hemos visto cómo crear una clase nueva con un método sencillo. Repasemos qué significa esto.

La noción central de la programación orientada a objetos (POO) es la **clase**.

Una **clase** es un _tipo_ de variable.

### Tipos [primitivos](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html)

En Java existen los siguientes _tipos_ de variable [primitivos](https://www.campusmvp.es/recursos/post/variables-y-tipos-de-datos-en-java-tipos-simples-clases-y-tipos-envoltorio-o-wrapper.aspx):

* `byte`: Almacena 1 byte de información (8 _ceros_ ó _unos_). Numéricamente, permite almacenar un valor en el rango [-128, 127]
* `short`: Almacena un número entero en 2 bytes; [-32768, 32767]
* `int`: Almacena un número entero en 4 bytes; [-2<sup>31</sup>, 2<sup>31</sup>-1]
* `long`: Almacena un número entero en 8 bytes; [-2<sup>63</sup>, 2<sup>63</sup>-1]
* `float`: Almacena un número decimal[^2] en 4 bytes. Es lo que se conoce como _precisión simple_
* `double`: Almacena un número decimal en 8 bytes. Precisión _doble_
* `boolean`: Almacena un valor que sólo puede ser `true` o `false`
* `char`: Almacena un carácter (una letra) en 2 bytes. Se emplea la codificación de caracteres UTF16.

Como ya sabes, se usan así:

```java
int miNumero = 6;
int otroNumero = 14;
// Espero que este resultado sea 20!!
int resultado = miNumero + otroNumero;
```

### Tipos no tan primitivos

Creando una **nueva clase**, podemos definir un **_tipo_** de variable nuevo.

La nueva clase:

1. Debe ser escrita en un fichero nuevo _(De momento no trabajaremos con clases internas)_
2. Debe declararse con la palabra `class`, seguida del nombre de la clase y llaves (`{ }`)
3. El nombre del fichero y el nombre de la clase deben ser iguales

### ¿Cómo se usa?

Igual que con los tipos primitivos, se **declara** una variable escribiendo el _tipo_ y luego el _nombre de la variable_. Así:

```java
int unNumero;
float unNumeroDecimal;
// El siguiente dato no es primitivo
Greeter miVariableEspecial;
```

Cuando **declaramos** una variable, el sistema reserva la memoria RAM necesaria para almacenarla, pero **aún no tiene ningún valor**. _(¿Cuánto vale `unNumero` en el ejemplo anterior?)_

Para darle un valor, debemos **inicializarla**. Eso se consigue con la palabra reservada `new`, así:

```java
int unNumero = 3;
float unNumeroDecimal = 3.333;
// El siguiente dato no es primitivo
Greeter miVariableEspecial = new Greeter();
```

Recuerda que los nombres de las clases deben escribirse en UpperCamlCase (la primera letra mayúscula), y las variables en lowerCamlCase (la primera letra minúscula).

Pongámoslo en práctica:

## :package: La tarea

**Se pide** que:

* Añadas una nueva clase `Bird` con un método `chirp`.
* El método `chirp` imprimirá por _salida estándar_ (`System.out`) el mensaje: `RANDOMEXPRESSIONpio|pio pio|pio pio pio|Pío pío|pio!!|pio pio pi-pi-pio! pioEND`

Pruébalo desde `Main.java` igual que hiciste con `Greeter`.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio. No está de más verificar que tu _commit_ se ha subido con éxito.


------------------------------------------------------------------------

[^2]: Los números decimales se almacenan en posiciones de memoria que son _ceros_ o _unos_. Esto se consigue mediante [IEEE754](https://en.wikipedia.org/wiki/IEEE_754), y es importante recordar que en los ordenadores existen _error de precisión_ con respecto a los números decimales o _en coma flotante_
