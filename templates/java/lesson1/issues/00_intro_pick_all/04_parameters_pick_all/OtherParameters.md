Más parámetros

## :books: Resumen

* Veremos brevemente cómo un método puede recibir más de 1 parámetro
* Escribiremos un método en `SimpleMathDemo` que _recibe_ 2 números enteros y los multiplica

## Descripción

Hemos visto que se puede declarar _mi método recibe un parámetro `int`_ así:

```java
    public void printNumber(int nombreArbitrario) {
        System.out.println(nombreArbitrario);
    }
```

Pues bien, si queremos declarar la recepción de _más de 1 parámetro_, los indicaremos de la misma forma pero separados por **comas** (`,`). Los nombres también deben ser distintos. Por ejemplo:

```java
    public void printTwoNumbers(int number1, int number2) {
        System.out.println(number1);
        System.out.println(number2);
    }
```

## :package: La tarea

**Se pide** que añadas un método `printProduct`:

* `public`
* Devolverá `void` (nada)
* Declarará que recibe **2** `int`
* Dentro del _cuerpo_ del método, _multiplicará esos dos valores_.
* Imprimirá (con `System.out.println`) el resultado de la multiplicación.

Invócalo desde `Main.java` con un valores cualesquiera para comprobar que funciona.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
