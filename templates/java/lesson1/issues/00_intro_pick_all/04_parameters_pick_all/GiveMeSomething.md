Parámetros

## :books: Resumen

* Entenderemos que los métodos admiten _parámetros_
* Veremos cómo se declaran los _parámetros formales_
* Veremos cómo se envían (al invocar el método) los _parámetros actuales_
* Escribiremos un ejemplo en `SimpleMathDemo` y lo invocaremos desde `Main.java`

## Descripción

Los métodos pueden devolver valores, pero también _recibirlos_.

Recordemos que en la primera tarea dijimos que en la **declaración** de un método, se indica:

> * Paréntesis indicando los argumentos del método. Si no recibe argumentos, se escriben paréntesis vacíos (`()`) igualmente.

Todos nuestros métodos hasta ahora _no_ reciben parámetros:

```java
    public void sayHello() { ... }
    public String byeWorld() { ... }
    public int sumTwoNumbers() { ... }
    public void printMultiplicationTable() { ... }
    public void loop50Times() { ... }
    
    ...
```

Sin embargo, además de _devolver_ datos (salida) también está permitido el **flujo de datos de entrada**, _hacia_ el método.

### Parámetros formales

En primer lugar, debemos _declarar_ **qué** parámetros espera el método.

Por ejemplo, si queremos recibir un número **entero** en un método `printNumber`, lo _declararíamos_ así:

```java
    public void printNumber(int nombreArbitrario) {
        
    }
```

Aquí, `nombreArbitrario` es un _nombre_ cualquiera. Lo usaremos para referirnos al **valor del parámetro** desde **dentro** del método. Por ejemplo:

```java
    public void printNumber(int nombreArbitrario) {
        System.out.println(nombreArbitrario);
    }
```

Nótese que **no** hace falta que `nombreArbitrario` coincida con ninguna variable en otras clases o ficheros de código.

### Parámetros actuales

Pero, ¿**qué** valor tiene `nombreArbitrario`?

Pues eso depende de con **qué** valor _invoquemos_ el método. Por ejemplo, imagínate en `Main.java`:

```java
public class Main {
    public static void main(String[] args) {
        // ...
        SimpleMathDemo demo = new SimpleMathDemo();
        demo.printNumber(10);
    }
}
```

Aquí, `10` es lo que se conoce como _parámetro actual_. Es decir, el **valor real** en tiempo de ejecución.

## ¡Lo entiendo!

¡Muy bien!

Para terminar, deja escrito el método `printNumber` en tu clase `SimpleMathDemo.java`.

Invócalo desde `Main.java` con un valor arbitrario.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
