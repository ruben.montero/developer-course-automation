Todavía más parámetros

## :books: Resumen

* Implementaremos otro método `printProduct` que reciba _distintos_ parámetros, de _distintos_ tipos
* Comprenderemos el concepto de _sobrecarga_ (`overload`)

## Descripción

Después de la tarea anterior, tendremos en `SimpleMathDemo` un método parecido a este:

```java
    public void printProduct(int num1, int num2) {
        System.out.println(num1 * num2);
    }
```

## :package: La tarea

Probemos a escribir otro método que reciba un parámetro formal tipo `String`, adicionalmente. Así comprobaremos que se pueden recibir parámetros de tipos _heterogéneos_:

```diff
    public void printProduct(int num1, int num2) {
        System.out.println(num1 * num2);
    }
    
+    public void printProduct(String var, int num1, int num2) {
+        // ...
+    }
```

Podemos usar ese parámetro en el _cuerpo_ del método.

**Escribamos** lo siguiente:

```java
    public void printProduct(String var, int num1, int num2) {
        System.out.println("RANDOMEXPRESSIONHola,|Saludos,|Bienvenido,|Un saludo,|Muy buenas,END" + " " + var);
        System.out.println("Este es el producto de tus dos valores:");
        System.out.println(num1 * num2);
    }
```

**Probémoslo** desde `Main.java`, enviando _valores cualesquiera_ como parámetros actuales.

¿Ves cómo funciona?

Ahora, para terminar, sólo **se pide** que tengas añadido el método `printProduct` que recibe **1** `String` y **2** `int`, como arriba.

### ¡Un momento!

¡Hay dos métodos `printProduct` diferentes!

Efectivamente, en nuestro camino hemos escrito **2** métodos con **el mismo nombre** en **la misma clase**:

```java
    public void printProduct(int num1, int num2) { /* ... */ }

    public void printProduct(String var, int num1, int num2) { /* ... */ }
```

Esto **está permitido**, y se conoce como **sobrecarga**.

Decimos que el método `printProduct` está **sobrecargado** con _distintos parámetros_.

### Ojo

El principio de la _sobrecarga_ consiste en indicar _distintos parámetros_.

**No** se puede sobrecargar un método indican _distintos tipos de dato devuelto_.

```java
    public int returnProduct() { /* ... */ }

    // MAL
    // Este método producirá un error de compilación
    public float returnProduct() { /* ... */ }
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
