La sentencia return es como un hacha

## :books: Resumen

* Veremos ejemplos de _Unreachable statement_
* Repasaremos brevemente la sintaxis del bucle `for` simple
* Escribiremos un método en `SimpleMathDemo` que tiene un `return` dentro de un bucle
* Veremos cómo se comporta invocándolo desde `Main`

## Descripción

En los métodos que hemos escrito hasta ahora, la sentencia `return` devuelve un valor.

Todavía no hemos comentado que, la sentencia `return` devuelve un valor **y** termina la ejecución del método.

Por ejemplo:

```java
    private String greeting() {
        String string1 = "hola, ";
        String string2 = "amigo";
        return string1 + string2;
        // La siguiente linea produce un error Unreachable Statement
        System.out.println("Este print va despues");
    }
```

Se conoce como _Dead code_, _Unreachable code_, código _muerto_ o _inalcanzable_ a aquel que **nunca puede ejecutarse**.

## ¿Y si ponemos `return` en un bucle?

### Pequeño repaso: Bucles [`for`](https://javadesdecero.es/basico/bucles-for-while-do-while-ejemplos/#2_Bucle_for)

Recordemos que un bucle `for` **sencillo** (también hay bucles _for-each_) tiene la siguiente sintaxis:

```java
for (int i = 0; i < 10; i++) {

}
```

* `i` es la variable _índice_ que se va actualizando dentro del bucle
* `i < 10` es la _condición de terminación_. Si es verdadera, el bucle se ejecuta. En el momento en que pasa a ser **falsa**, el bucle no se repite
* `i++` es el modo de incremento del índice. Podría ser `i+=2`, por ejemplo

## :package: La tarea

**Se pide** que:

* Añadas un nuevo método a `SimpleMathDemo`
* Tendrá este código:

```java
    public void RANDOMEXPRESSIONprintMultiplicationTable|printMultiplication|printMultiplicationInfoEND() {
        System.out.println("Los múltiplos de 7 menores que 70 son:");
        for (int i = 0; i < 70; i+=7) {
            System.out.println(i);
        }
    }
```

## A lo nuestro

**Continuemos**.

* ¿Y si ponemos `return` en un bucle?

Vamos a añadir este **otro** método a `SimpleMathDemo`:

```java
    public void loop50Times() {
        for (int i = 0; i < 50; i++) {
            System.out.println("Imprimiendo... " + i);
            if (i == RANDOMNUMBER20-40END) {
                return; // Se puede hacer return si el método devuelve
                        // void. Tiene el mismo efecto... 
                        // ¡Pero no devuelve nada!
            }
        }
    }
```

**_Invócalo_** también desde `Main.java`.

¿Qué se imprime por consola?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
