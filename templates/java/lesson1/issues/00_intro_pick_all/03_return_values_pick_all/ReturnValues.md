Devolver un valor

## :books: Resumen

* Distinguiremos entre función y procedimiento
* Implementaremos un método `byeWorld` en nuestra clase `Greeter`, que devolverá un String
* Invocaremos dicho método desde `Main`

## Descripción

Un **procedimiento** (o subrutina) es una _pieza_ de código fuente que se ejecuta de forma separada. Al igual que los métodos de las clases, puede _invocarse_.

Una **función** es, igualmente, una _pieza_ de código fuente que se ejecuta de forma separada. La diferencia es que **devuelve un valor**.

### Hemos usado procedimientos

Nuestras clases declaraban `void` como _tipo de dato devuelto_. Por ejemplo, en `Greeter` tenemos:

```java
    public void sayHello() {
        System.out.println("Hello world!");
    }
```

¡Usemos funciones!

## :package: La tarea

Vamos a estrenar un **nuevo método** en `Greeter` que **devuelva un valor**. Lo escribiremos _debajo_ de `sayHello()`.

Llamémoslo `byeWorld`. Devolverá un dato tipo `String`:

```java
    public void sayHello() {
        System.out.println("Hello world!");
    }

    public String byeWorld() {
    
    }
```

Si te fijas, **aparece un error de compilación** que indica:

> Missing return statement

Claro.

Declaramos que `byeWorld` _devuelve_ un `String`, pero... ¿Qué `String`? ¿Qué **valor** exactamente _devuelve_?

Debemos indicarlo con la palabra especial `return`.

Nosotros vamos a devolver el valor: `Bye!`. Así:

```java
    public String byeWorld() {
        return "Bye!";
    }
```

### ¿Para qué sirve **devolver** algo?

Es el principio básico de funcionamiento de la programación estructurada.

Un programa se divide en partes que procesan datos y producen un resultado. Ese _resultado_, se entrega al resto del programa _devolviéndolo_.

### Usémoslo en `Main`

Por ejemplo, en `Main.java` añadamos la siguiente sentencia:

```java
public class Main {
    public static void main(String[] args) {
        // ...
        String miString = greeter.byeWorld();
    }
}
```

Estamos haciendo algo importante:

1. Declarar una nueva variable tipo `String`, llamada `miString`
2. Invocar `greeter.byeWorld()`
3. **Asignar** el valor _devuelto_ a `miString`

A continuación, desde _esta parte del programa_ podremos trabajar con dicho resultado.

### Terminando...

Añade un `System.out.println()`[^1] así:

```diff
public class Main {
    public static void main(String[] args) {
        // ...
        String miString = greeter.byeWorld();
+        System.out.println(miString);
    }
}
```

**Ejecuta** la aplicación principal. Deberías ver varios _print_ que se producen desde _distintas partes del programa_.

¿Comprendes el _flujo de ejecución_?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.


---------------------------------------------------

[^1]: Desde IntelliJ IDEA, si tecleas `sout`, verás que te permite autocompletar automáticamente a `System.out.println()`. También se pueden configurar más _shortcuts_
