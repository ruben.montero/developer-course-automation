import org.junit.Test;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import static org.junit.Assert.*;

public class TESTFILENAME {

    @Test
    public void testMethodReturnValue() {
        int result = 0;
        Class cls = null;
        try {
            cls = Class.forName("SimpleMathDemo");
            Method method = cls.getDeclaredMethod("firstNumber");
            Object instance = cls.getDeclaredConstructor().newInstance();
            Object shouldFail = method.invoke(instance);
            fail("No se debería poder invocar el método");
        } catch (ClassNotFoundException e) {
            fail("La clase especificada no existe");
        } catch (NoSuchMethodException e) {
            fail("La clase especificada no contiene un metodo con el nombre indicado o con un constructor apropiado");
        } catch (InvocationTargetException | InstantiationException e) {
            fail("La clase especificada no puede ser instanciada");
        } catch (IllegalAccessException e) {
            // Good. Method is expected to be private
            assertTrue(true);
        }
        try {
            cls = Class.forName("SimpleMathDemo");
            Method method = cls.getDeclaredMethod("secondNumber");
            Object instance = cls.getDeclaredConstructor().newInstance();
            Object shouldFail = method.invoke(instance);
            fail("No se debería poder invocar el método");
        } catch (ClassNotFoundException e) {
            fail("La clase especificada no existe");
        } catch (NoSuchMethodException e) {
            fail("La clase especificada no contiene un metodo con el nombre indicado o con un constructor apropiado");
        } catch (InvocationTargetException | InstantiationException e) {
            fail("La clase especificada no puede ser instanciada");
        } catch (IllegalAccessException e) {
            // Good. Method is expected to be private
            assertTrue(true);
        }
        try {
            cls = Class.forName("SimpleMathDemo");
            Method method = cls.getDeclaredMethod("thirdNumber");
            Object instance = cls.getDeclaredConstructor().newInstance();
            Object shouldFail = method.invoke(instance);
            fail("No se debería poder invocar el método");
        } catch (ClassNotFoundException e) {
            fail("La clase especificada no existe");
        } catch (NoSuchMethodException e) {
            fail("La clase especificada no contiene un metodo con el nombre indicado o con un constructor apropiado");
        } catch (InvocationTargetException | InstantiationException e) {
            fail("La clase especificada no puede ser instanciada");
        } catch (IllegalAccessException e) {
            // Good. Method is expected to be private
            assertTrue(true);
        }
        try {
            cls = Class.forName("SimpleMathDemo");
            Method method = cls.getDeclaredMethod("RANDOMEXPRESSIONcalculateResult|doCalculations|getResultEND");
            Object instance = cls.getDeclaredConstructor().newInstance();
            result = (Integer) method.invoke(instance);
        } catch (ClassNotFoundException e) {
            fail("La clase especificada no existe");
        } catch (NoSuchMethodException e) {
            fail("La clase especificada no contiene un metodo con el nombre indicado o con un constructor apropiado");
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            fail("La clase especificada no puede ser instanciada");
        }
        int value1 = RANDOMNUMBER1000-5000END;
        int value2 = RANDOMNUMBER1001-5000END;
        int value3 = RANDOMNUMBER1002-5000END;
        assertEquals(value1+value2+value3, result);
    }
}

