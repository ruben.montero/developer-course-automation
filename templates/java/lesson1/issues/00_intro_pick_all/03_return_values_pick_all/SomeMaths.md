Calcular y devolver un valor

## :books: Resumen

* Escribiremos **3** métodos distintos en `Greeter`.
  * Dos de ellos devolverán un número entero
  * El tercero _invocará_ a los otros dos y sumará esos valores
* Veremos que un método puede invocarse desde _otro método_
* Invocaremos dicho método desde `Main`

## Descripción

Hemos visto que:

1. Podemos escribir clases
2. Podemos _instanciar_ clases (inicializarlas como variables)
3. Podemos escribir métodos
4. Podemos _invocar_ dichos métodos desde `Main`

Pero... ¿podemos invocar los métodos desde _otros sitios_? Por ejemplo, ¿desde _dentro_ de la misma clase?

¡Sí!

## :package: La tarea

En `Greeter.java`, añade un método llamado `firstNumber()`. Devolverá un tipo `int` y **tendrá visibilidad `private`**.

En el cuerpo, sólo _devolverá un valor_. El mismo que se indica a continuación:

```java
    private int firstNumber() {
        return RANDOMNUMBER8-50END;
    }
```

Intenta _invocar_ dicho método desde `Main.java`. ¿Hay algún problema?

### Un segundo método

Elimina cualquier código que genere errores y añade un segundo método a `Greeter.java`, así:

```java
    private int secondNumber() {
        return RANDOMNUMBER9-50END;
    }
```

### Dos métodos privados

Como estos métodos son `private`, **no** se pueden _invocar_ desde _fuera_ de la clase.

¡Pero sí desde _dentro_!

Escribamos un **nuevo** método en `Greeter.java`. Se llamará `sumTwoNumbers`. Esta vez, `public`. También devolverá un tipo `int`:

```java
    public int sumTwoNumbers() {
        
    }
```

### Los métodos privados tienen sentido

Un método privado sirve para _encapsular_ un trozo de código **dentro** de una clase. Piensa que en el futuro escribiremos clases con muchas líneas de código y **no** debe ir todo **junto** como un **churro**.

La idea de la programación estructurada y las _subrutinas_ también tienen sentido **dentro de una misma clase**.

### ¿Cómo se hace?

Basta con escribir esto:

```java
    public int sumTwoNumbers() {
        return firstNumber() + secondNumber();
    }
```

Sería equivalente a:

```java
    public int sumTwoNumbers() {
        int result = firstNumber() + secondNumber();
        return result;
    }
```

```java
    public int sumTwoNumbers() {
        int first = firstNumber();
        int second = secondNumber();
        return first + second;
    }
```

Déjalo como te sea más claro.

### Un momento...

## La ~~s 8~~ 1 diferencia ~~s~~

Desde `Main.java`, en ejercicios anteriores, hemos invocado un método de `Greeter` así:

```java
        Greeter greeter = new Greeter();
        greeter.sayHello();
```

**pero ahora**, desde `Greeter.java`, invocamos sus propios métodos privados así:

```java
        int first = firstNumber();
        int second = secondNumber();
```

Hay **1** diferencia.

En `Main.java` invocamos los métodos _sobre una variable_ (antes del punto (`.`)).

En `Greeter.java`, _escribimos los nombres directamente_. La pregunta es: ¿Sobre **qué** variable?

### Repasemos... ¿Cuál es el problema?

Se supone que un método es parte de una _variable_ (o _instancia_) de una clase en particular.

En `Main.java` queda claro **qué** _variable_ ejecuta sus métodos. La hemos creado en la línea:

```java
    Greeter greeter = new Greeter();
```

Pero... ¿en `Greeter.java`?

## `this`

Lo entenderemos mejor si vemos que estas dos líneas son **equivalentes**:

```java
    public int sumTwoNumbers() {
        int first = firstNumber();
        int second = secondNumber();
        return first + second;
    }
```

```java
    public int sumTwoNumbers() {
        int first = this.firstNumber();   // Usamos this
        int second = this.secondNumber(); // Usamos this
        return first + second;
    }
```

`this` se refiere a **la instancia actual**.

**Explicación**:

* Si el método `sumTwoNumbers` está siendo _invocado_, **alguien** ha creado una _instancia_
* No sabemos **qué nombre** han elegido para esa _instancia_ (pudo ser `greeter`, `myGreeter`,...), **ni nos interesa**.
* `this` es un _sinónimo_ de esa variable, **sea cual sea**. Sólo se puede usar **dentro** de la clase.

### Para terminar

Si se ha entendido a la perfección, bien.

Si no, lo dejaremos macerando e iremos comprendiendo más con los siguientes ejercicios.

**Ahora**, _invoca_ el método `sumTwoNumbers` desde `Main.java` e **imprime** el resultado, así:

```java
public class Main {
    public static void main(String[] args) {
        // ...
        System.out.println(greeter.sumTwoNumbers());
    }
}
```

¿Encaja?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.


---------------------------------------------------

[^1]: Desde IntelliJ IDEA, si tecleas `sout`, verás que te permite autocompletar automáticamente a `System.out.println()`. También se pueden configurar más _shortcuts_
