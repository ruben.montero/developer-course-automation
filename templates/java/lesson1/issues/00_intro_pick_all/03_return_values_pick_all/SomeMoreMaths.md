Calcular y devolver un valor

## :books: Resumen

* Escribiremos una nueva clase `SimpleMathDemo`
* Escribiremos **4** métodos, similares a la tarea anterior
  * Tres de ellos devolverán un número entero
  * El cuarto _invocará_ a los otros tres y sumará esos valores
* Invocaremos dicho método desde `Main`

## :package: La tarea

**Se pide** que:

* **Crees** una nueva clase `SimpleMathDemo.java`
* **Escribas** (_implementes_) en dicha clase un método `firstNumber`:
  * Será `private`
  * Devolverá el valor `RANDOMNUMBER1000-5000END`, tipo `int`
* **Escribas** en dicha clase un método `secondNumber`:
  * Será `private`
  * Devolverá el valor `RANDOMNUMBER1001-5000END`, tipo `int`
* **Escribas** en dicha clase un método `thirdNumber`:
  * Será `private`
  * Devolverá el valor `RANDOMNUMBER1002-5000END`, tipo `int`
* **Escribas** en dicha clase un método `RANDOMEXPRESSIONcalculateResult|doCalculations|getResultEND`:
  * Será `public`
  * Invocará los 3 métodos anteriores y devolverá la suma de sus valores

Para terminar, en `Main.java`:

* Declara e inicializa una _nueva instancia_ de tipo `SimpleMathDemo`
* Sobre ella, **invoca** el método `public` que creaste al final
* **Imprime** dicho resultado con `System.out.println`

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
