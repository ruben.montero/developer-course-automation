Un pequeño comentario

## :books: Resumen

* Un pequeño comentario sobre `Main`

## Descripción

Hasta ahora, hemos ido escribiendo diferentes clases con funcionalidad que _invocábamos_ desde `Main.java`.

Bien.

Los **tests** automáticos _sólo verifican las clases_.

Es decir, **no** hace falta que `Main.java` contenga unas líneas específicas para que los _tests_ funcionen.

### ¿Entonces para qué sirve?

Es el punto de entrada de la aplicación principal.

Nosotros podemos usar la aplicación principal como _playground_ ó **patio de recreo** para hacer pruebas sobre las clases que vamos creando y cómo funcionan.

Pero, en este proyecto, son las **clases** las que usamos para aprender y las que nos importan finalmente.

## :package: La tarea

Si lo has entendido:

* Borra el código dentro de `Main.java`. Será tu espacio para probar futuras clases que escribas a partir de ahora, libremente, a tu manera
* Escribe una clase `RANDOMEXPRESSIONIHaveUnderstood|IUnderstand|ThisIsUnderstoodEND.java` que esté **vacía**. Verifica que el _test_ de esta tarea pasa correctamente

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
