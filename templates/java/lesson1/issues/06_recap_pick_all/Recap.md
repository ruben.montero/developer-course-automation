¡Trabajo terminado!

## :trophy: ¡Felicidades!

Has realizado las tareas del _sprint_ y has validado tu trabajo con los _tests_. ¡Enhorabuena!

<img src="java-introduction/docs/sunset.jpg" width=500 />

Has...

* Comprendido que una clase, en Programación Orientada a Objetos (POO) es un tipo especial de variable que tú mismo defines
* Escrito métodos que hacían cálculos y devolvían valores
* Repasado los bucles `for` convencionales
* Escrito métodos que reciben parámetros
* Implementado métodos con la misma firma pero distintos parámetros formales (sobrecargados)
* Entendido que el método constructor de una clase se ejecuta cuando es instanciada (con `new`), y tiene una sintaxis especial
* Implementado atributos dentro de clases, entendiendo diferentes tipos de _visibilidad_
* Usado `getters` y `setters`
* Entendido qué es un _POJO_ (Plain Old Java Object)
* Usado _arrays_ convencionales, entendiendo cómo se pueden insertar y leer datos del mismo
* Usado `List`, y entendido que presenta ventajas con respecto a un _array_ convencional
* Accedido a un fichero de forma secuencial con `BufferedReader` para leer las 'preferencias de un usuario'
* Escrito a un fichero con `BufferedWriter`
* Visto una introducción al formato JSON
* Leído y escrito ficheros JSON mediante `JSONObject` y otras clases
* Visto una introducción a XML, y escrito un documento de ejemplo
* Escrito una clase con una funcionalidad compleja que era capaz de generar un fichero JSON en función de unos parámetros
