Listas en JSON

## :books: Resumen

* Veremos cómo se representa una lista en JSON
* Crearemos una clase sencilla con un método que sólo lee e imprime una lista de _strings_ JSON

## Descripción

En [JavaScript Object Notation](https://json.org/json-es.html) (JSON) se pueden representar _listas_ de elementos.

Se hace usando **corchetes** (`[ ]`) y separando los valores con **comas** (`,`).

Por ejemplo, esto es un documento JSON válido:

```json
[5, 4, -6, 4]
```

Las listas pueden ser de cualquier tipo:

```json
["Benzema", "Messi"]
```

Incluso objetos:

```json
[
  { "jugador": "Benzema", "equipo": "Real Madrid" },
  { "jugador": "Messi", "equipo": "FC Barcelona" }
]
```

### Anidamiento

No es que una lista _tenga que ser_ el elemento raíz de un JSON.

Como ya sabes, los objetos JSON pueden anidarse, y las listas también.

Por ejemplo, esta sintaxis es válida:

```json
{
  "jugador": "Benzema",
  "equipos": [
    "Olympique Lyonnais",
    "Real Madrid"
  ]
}
```

### ¿Y cómo se lee una lista JSON de un fichero?

De la misma forma que ya sabemos, pero empleando un `JSONArray` en lugar de un `JSONObject`:

```java
        FileReader reader = new FileReader("assets\\wrestlers.json");
        JSONTokener tokener = new JSONTokener(reader);
        JSONArray jsonArray = new JSONArray(tokener);
```

## ¿Y cómo se usa un `JSONArray`?

### Acceder a sus elementos

Es parecido a un `ArrayList` de los que hemos usado con anterioridad.

Tiene métodos:

* `.getString`
* `.getInt`
* ...

...para acceder a su contenido. Reciben un **índice**:

```json
["Undertaker", "John Cena", "David Batista"]
```

```java
// Imprime "Undertaker"
System.out.println(jsonArray.getString(0));

// Imprime "David Batista
System.out.println(jsonArray.getString(2))
```

Así que es común acceder a ellos con un **bucle `for`**:

```java
for (int i = 0; i < jsonArray.length(); i++) {
    String wrestlerName = jsonArray.getString(i);
    System.out.println(wrestlerName);
}
```

### Añadir elementos

La operación inversa, es decir, **introducir** elementos en un `JSONArray`, es trivial. Basta con usar `.put` y pasar como parámetro la variable que deseemos añadir:

```java
JSONArray array = new JSONArray();
array.put(/* Aquí iría un elemento */);
array.put(/* Podemos añadir tantos como queramos */);
```

...algo muy similar al `.add` de `ArrayList` que ya conocemos.

## :package: La tarea

**Se pide** que:

* **Crees** una nueva clase `RANDOMEXPRESSIONWrestlersReader|WWEReader|WrestlersJSON|WrestlersJSONReader|WWEJSONReaderEND`
* **No** definas un constructor
* Únicamente, **añade un** método `RANDOMEXPRESSIONprintWrestlers|readWrestlers|printList|readListEND` que:
  * Sea `public void`
  * **No** reciba parámetros
  * Lea el fichero `wrestlers.json` e imprima, línea a línea, los nombres de los luchadores que se hallan en formato JSON

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
