Guardando preferencias en XML

## :books: Resumen

* Echaremos un vistazo al formato XML 
* Añadiremos un método `saveExampleXML` a nuestra clase `HomeCinemaPreferences.java` que creará y guardará un XML de ejemplo (`assets\example.xml`), con una etiqueta raíz y dos nodos

## Descripción

[eXtensible Markup Language](https://cdrh.unl.edu/articles/basicguide/XML) ([XML](http://www.w3.org/TR/2006/REC-xml-20060816/Overview.html)) es un **formato estándar** que describe una serie de reglas sobre cómo almacenar información en un fichero.

Se basa en el concepto de **etiqueta**, expresado como una palabra _englobada_ en los caracteres _menor que_ y _mayor que_ (`<` y `>`), y **anidamiento** (jerarquía) de tal forma que las etiquetas pueden tener contenido, sea éste texto u otras etiquetas.

¿Recuerdas el _árbol_ de directorios de tu disco duro?

La idea es _muy similar_. Por ejemplo, esta información:

```mermaid
graph TD
  Philosopher-->Name
  Name-->id2[Friedrich Nietzsche]
  Philosopher-->id1[BirthDate];
  id1-->Day
  Day-->15
  id1-->Month
  Month-->October
  id1-->Year
  Year-->1844
```

...se guardaría en XML como:

```xml
<Philosopher>
  <Name>Friedrich Nietzsche</Name>
  <BirthDate>
    <Day>15</Day>
    <Month>October</Month>
    <Year>1844</Year>
  <BirthDate>
</Philosopher>
```

Fíjate que las etiquetas se **abren** (`<Philosopher>`) y luego se **cierran** escribiendo un _slash_ (`/`) que precede al nombre (`</Philosopher>`).

Esta estructura de información, sea la que sea, se conoce como un _modelo_. Concretamente, el [**DOM**](https://www.javatpoint.com/xml-dom) (_Document Object Model_) viene a significar dicha _estructura lógica_.

Veámoslo así:

* El archivo en disco duro está escrito con etiquetas (como en el segundo ejemplo)
* El DOM es una representación de ese archivo (como en el primer ejemplo), que se carga en la memoria RAM y contra la que trabajaremos empleando variables de clases específicas

### ¿Y esto qué tiene que ver con Java?

XML es un formato para almacenar información, y existen clases específicas de la librería Java destinadas a usar XML.

## :package: La tarea

Escribamos un nuevo método `saveExampleXML` en nuestro `HomeCinemaPreferences.java`.

El **objetivo final** de este **ejemplo** será crear _desde 0_ un DOM como el siguiente:

```mermaid
graph TD
  Student-->Name
  Name-->id2[Pepe Depura]
  Student-->id1[IsLearning];
  id1-->true
```

...y escribirlo a disco:

```xml
<Student>
    <Name>Pepe Depura</Name>
    <IsLearning>true</IsLearning>
</Student>
```

### Crear un `Document`

La clase que _representa_ un documento se llama `Document`.

Por desgracia, no se puede _instanciar_ (con `new`) directamente. A través de un [patrón factoría](https://www.ionos.es/digitalguide/paginas-web/desarrollo-web/patron-factory/), la _instanciaremos_ con estas **3** líneas:

```java
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/* ... */

    public void saveExampleXML() throws ParserConfigurationException {
        DocumentBuilderFactory factory1 = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory1.newDocumentBuilder();
        Document xmlDocument = builder.newDocument();
        
    }
```

### Crear nodo raíz

Cada _nodo_ debe ser **creado** y **anexado** al documento o a un nodo _padre_.

Nosotros lo haremos así con el _nodo raíz_:

```mermaid
graph TD
  Student
```

```java
        Element rootNode = xmlDocument.createElement("Student");
        xmlDocument.appendChild(rootNode);
```

### Nombre

A continuación hay que:

**1) Crear** un nodo `Name`:

```java
        Element node1 = xmlDocument.createElement("Name");
```

```mermaid
graph TD
  Name
```

**2) Crear** un nodo _hoja_ (será tipo texto, `Text`) que albergue el valor del nombre:

```java
        Text node1Content = xmlDocument.createTextNode("Pepe Depura");
```

```mermaid
graph TD
  id1[Pepe Depura]
```

**3) Anexarlo**

```java
        node1.appendChild(node1Content);
```

```mermaid
graph TD
  Name-->id1[Pepe Depura]
```

Y **4) anexar** dicho nodo a la _raíz_

```java
        rootNode.appendChild(node1);
```

```mermaid
graph TD
  Student-->Name
  Name-->id1[Pepe Depura]
```

### ¿Está aprendiendo el estudiante?

Escribiremos otras 4 líneas similares para el otro atributo de nuestro ejemplo:

```java
        Element node2 = xmlDocument.createElement("IsLearning");
        Text node2Content = xmlDocument.createTextNode("true");
        node2.appendChild(node2Content);
        rootNode.appendChild(node2);
```

### ¡Ya está el XML!

...en la **memoria** de nuestro ordenador, en la forma de una **instancia** de `Document`

Ahora sólo falta **guardarlo** al disco duro.

Ojalá fuera tan sencillo como:

```java
// Esto no existe
xmlDocument.saveToFile("assets\\example.xml")
```

Pero los pasos a seguir consisten en:

1. Instanciar, a través de una factoría, un `Transformer`
2. Convertir el _documento_ a `DOMSource`
3. Instanciar un `StreamResult` asociado a un fichero
4. Invocar `.transform` para convertir el `DOMSource` a `StreamResult`

No hace falta memorizar estos pasos. Son relativamente mecánimos y acoplados a la librería `javax.xml`.

Basta con poner en práctica el código relevante:

```java
        TransformerFactory factory2 = TransformerFactory.newInstance();
        Transformer transformer = factory2.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource dom = new DOMSource(xmlDocument);
        StreamResult outputStream = new StreamResult(new File("assets\\example.xml"));

        transformer.transform(dom, outputStream);
```

_(Tendrás que añadir también `TransformerException` a las excepciones lanzadas, señaladas en la firma del método `saveExampleXML`)_

### Probando, probando...

**Invoquemos** `saveExampleXML` desde `Main.java`.

Habrá que controlar las excepciones. Cualquiera de las soluciones propuestas por IntelliJ IDEA nos vale de momento.

Una vez verifiques el archivo guardado... **¡Enhorabuena!** Has generado tu primer XML programáticamente desde Java.

¿Te imaginas cómo sería guardar las _preferencias_ (atributos) de `HomeCinemaPreferences` a XML igual que hiciste con el formato JSON?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
