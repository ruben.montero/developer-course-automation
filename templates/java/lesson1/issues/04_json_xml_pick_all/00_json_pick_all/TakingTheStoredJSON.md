Leyendo preferencias en JSON

## :books: Resumen

* Veremos cómo se puede recuperar la información de un archivo JSON
* Veremos qué es un `Enum` en Java
* Crearemos un nuevo constructor `HomeCinemaPreferences` que reciba un parámetro `Enum`:
  * Permitirá inicializar las preferencias desde `cinemaPrefs.txt`, `cinemaPrefs.xml` ó `cinemaPrefs.json` según su valor
* _Deprecaremos_ los constructores anteriores

## Descripción

El proceso de leer un fichero JSON es bastante sencillo. Basta con emplear un `FileReader` como el que ya sabemos _instanciar_:

```java
FileReader reader = new FileReader("assets\\cinemaPrefs.json");
```

...y con ese objeto, crear un `JSONTokener`, que es la clase diseñada para _parsear_ un fichero JSON en la librería `org.json:json20220320`:

```java
FileReader reader = new FileReader("assets\\cinemaPrefs.json");
JSONTokener tokener = new JSONTokener(reader);
```

En Java, se permite escribir todo **en una sola línea** así:

```java
JSONTokener tokener = new JSONTokener(new FileReader("assets\\cinemaPrefs.json"));
```

## :package: La tarea

**Crearemos** un nuevo método _privado_ en `HomeCinemaPreferences.java` que sirva para _inicializar_ las preferencias desde un JSON.

Comenzará así:

```java
    private void initializeFromJSON() {
        try {
            JSONTokener tokener = new JSONTokener(new FileReader("assets\\cinemaPrefs.json"));
            
            // ...
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
```

...y luego es necesario _instanciar_ un `JSONObject`.

En la tarea anterior _instanciábamos_ un `JSONObject` **vacío**, sin parámetros en el constructor.

Ahora, le pasamos el `tokener` para que **lea** el fichero asociado:

```diff
    private void initializeFromJSON() {
        try {
            JSONTokener tokener = new JSONTokener(new FileReader("assets\\cinemaPrefs.json"));
+            JSONObject jsonObject = new JSONObject(tokener);
            
            // ...
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
```

Los siguientes métodos se emplean para recuperar información de un objeto JSON _(Si tecleas `jsonObject.` y esperas a que IntelliJ IDEA autocomplete, los verás)_:

* `.getString`
* `.getBoolean`
* `.getFloat`
* `.getInt`
* `.getJSONObject`
* `.getJSONArray`

Por lo tanto, debemos saber de **qué** tipo es el valor que estamos recuperando. En nuestro caso, `username` es un `String` y `prefersDarkMode` es un `boolean`, así que haríamos así:

```diff
    private void initializeFromJSON() {
        try {
            FileReader reader = new FileReader("assets\\cinemaPrefs.json");
            JSONTokener tokener = new JSONTokener(reader);
            JSONObject jsonObject = new JSONObject(tokener);
+            String fileUsername = jsonObject.getString("username");
+            boolean fileDarkMode = jsonObject.getBoolean("prefersDarkMode");

            // ...
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
```

### ¿Para qué queríamos estos valores?

¡Ah, sí! Estábamos **inicializando** las preferencias.

```diff
    private void initializeFromJSON() {
        try {
            FileReader reader = new FileReader("assets\\cinemaPrefs.json");
            JSONTokener tokener = new JSONTokener(reader);
            JSONObject jsonObject = new JSONObject(tokener);
            String fileUsername = jsonObject.getString("username");
            boolean fileDarkMode = jsonObject.getBoolean("prefersDarkMode");
+           this.username = fileUsername;
+           this.darkModePreferred = fileDarkMode;
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
```

### Un nuevo constructor

Nuestro método `initializeFromJSON` funciona estupendamente. Estaría bien invocarlo desde el método constructor... ¡Pero ya existe un método constructor!

### Camino hacia el futuro... _Deprecar_ métodos viejos manteniendo compatibilidad

Vamos a **escribir** un **nuevo método privado** `initializeFromTXT`:

```java
    private void initializeFromTXT() {
    
    }
```

Y vamos a **cortar** (CTRL+X) **todo el contenido** actual del método constructor, que se encarga de _leer un archivo `.txt`_:

```diff
    public HomeCinemaPreferences() {
-       String file ="assets\\cinemaPrefs.txt";
-       try {
-           FileReader fileReader = new FileReader(file);
-           BufferedReader bufferedReader = new BufferedReader(fileReader);
-           String newLine = null;
-           do {
-               newLine = bufferedReader.readLine();
-               System.out.println(newLine);
-               // Invocamos el método que trabaja línea a línea
-               parseLine(newLine)
-           } while (newLine != null);
-           // Un detalle: Cerrar el fichero tras su uso
-           fileReader.close();
-       } catch (IOException e) {
-           throw new RuntimeException(e);
-       }
    }
```

...y **pegarlo** (CTRL+V) en `initializeFromTXT`:

```diff
    private void initializeFromTXT() {
+       String file ="assets\\cinemaPrefs.txt";
+       try {
+           FileReader fileReader = new FileReader(file);
+           BufferedReader bufferedReader = new BufferedReader(fileReader);
+           String newLine = null;
+           do {
+               newLine = bufferedReader.readLine();
+               System.out.println(newLine);
+               // Invocamos el método que trabaja línea a línea
+               parseLine(newLine)
+           } while (newLine != null);
+           // Un detalle: Cerrar el fichero tras su uso
+           fileReader.close();
+       } catch (IOException e) {
+           throw new RuntimeException(e);
+       }
    }
```

Para que funcione igual, en el método **constructor** que hemos dejado vacío, debemos **invocar** el método:

```java
    public HomeCinemaPreferences() {
        initializeFromTXT();
    }
```

### Y... ¡Un segundo método constructor!

Ahora podemos **añadir** un segundo **método constructor** que reciba un parámetro `boolean`:

```java
    public HomeCinemaPreferences(boolean readJSON) {
        if (readJSON) {
            initializeFromJSON();
        } else {
            initializeFromTXT();
        }
    }
```

### Pero, ¿por qué queremos tener dos métodos constructores distintos?

Imagina que un compañero de trabajo, en su código, usa `new HomeCinemaPreferences();`. Si borramos el constructor original, ¡**rompemos** su código! 

* El constructor nuevo es más moderno y soporta leer desde un fichero `.txt` o desde uno `.json`.

* El constructor viejo se mantiene por razones de compatibilidad.

Aún así tenemos **código duplicado** en nuestra clase `HomeCinemaPreferences.java`. Eso **no** es deseable.

El código duplicado es el viejo constructor:

```java
    public HomeCinemaPreferences() {
        initializeFromTXT();
    }
```

...ya que _invocar_ el **nuevo constructor** y pasar `false`, es equivalente:

```java
// Ahora mismo, esto...
HomeCinemaPreferences prefs = new HomeCinemaPreferences();
// ...y esto:
HomeCinemaPreferences prefs = new HomeCinemaPreferences(false);
// Es LO MISMO
```

### Marcar como obsoleto

En situaciones como esta, es bueno marcar el viejo código como obsoleto o _deprecado_. Esto se consigue con la **anotación** `@Deprecated`.

Así:

```java
    @Deprecated
    public HomeCinemaPreferences() {
        initializeFromTXT();
    }
```

Sirve para que _futuros_ desarrolladores tengan en cuenta que **no** se debería usar ese código, ya que está ahí por razones _históricas_ o _legacy_.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------

[^1]: ¿Te has preguntado qué es ese `break`?
