Guardando preferencias en JSON

## :books: Resumen

* Echaremos un vistazo al formato JSON
* Añadiremos un método `saveExampleJSON` a nuestra clase `HomeCinemaPreferences.java` que creará y guardará un JSON de ejemplo (`assets\example.json`), con dos atributos
* Añadiremos `saveAsJSON`, que guardará un JSON (`cinemaPrefs.json`) con los valores de las preferencias

## Descripción

[JavaScript Object Notation](https://json.org/json-es.html) (JSON) es un formato ligero de intercambio de datos. A pesar de su nombre, es independiente del lenguaje JavaScript.

Es un formato sencillo que se basa en almacenar _tuplas_ **clave-valor** y encerrar _objetos_ entre **llaves** (`{ }`). Por ejemplo, esta información:

```mermaid
graph TD
  Philosopher-->Name
  Name-->id2[Friedrich Nietzsche]
  Philosopher-->id1[BirthDate];
  id1-->Day
  Day-->15
  id1-->Month
  Month-->October
  id1-->Year
  Year-->1844
```

...se almacenaría así:

```json
{
  "philosopher": {
    "name": "Friedrich Nietzsche",
    "birthDate": {
      "day": 15,
      "month": "October",
      "year": 1844
    }
  }
}
```

Fíjate que los miembros que se encuentran al **mismo nivel** se **separan** con **comas** (`,`), y que los valores, a excepción de los numéricos, van entre **comillas** (`""`).

## Ejemplo de JSON

Escribiremos un nuevo método `saveExampleJSON` en nuestro `HomeCinemaPreferences.java`.

El **objetivo final** de este **ejemplo** será crear _desde 0_ un objeto JSON con dos atributos:

```mermaid
graph TD
  name-->id2[Pepe Depura]
  isLearning-->id1[true]
```

...y escribirlo a disco:

```json
{
  "name": "Pepe Depura",
  "isLearning": true
}
```

### Crear un `JSONObject`

La clase que _representa_ un _objeto_ JSON se llama `JSONObject`, en la librería `org.json:json-20220320` usada en el proyecto.

Crearlo es **fácil**:

```java
JSONObject jsonObject = new JSONObject();
```

...y **almacenar** un par de valores, también:

```java
jsonObject.put("name", "Pepe Depura");
jsonObject.put("isLearning", true);
```

### Escribir a un fichero

Podemos guardarlo a un fichero... **fácilmente**:

```java
FileWriter writer = new FileWriter("assets\\example.json");
jsonObject.write(writer, 2, 0); // Estos números indican la identación del resultado,
                                // espacios en blanco que mejoran la legibilidad
writer.flush();
writer.close();
```

_(Hay que considerar el `IOException`)_

¡Método **terminado**!

## :package: La tarea

**Se pide**:

* Que `HomeCinemaPreferences.java` cuente con el código `saveExampleJSON` visto arriba
* Que añadas un nuevo método `RANDOMEXPRESSIONsaveAsJSON|saveJSON|writeAsJSON|writeJSONEND`:
  * Guardará el valor de los atributos `username` y `darkModePreferred` a disco duro en un archivo `assets\cinemaPrefs.json`
  * Tendrá un contenido JSON _con el siguiente formato_:
  
```json
{
  "username": "XXXXXXXXXX",
  "prefersDarkMode": true|false
}
```

_(Es decir, si en tu instancia `this.username` vale `John Doe` y `this.darkModePreferred` es `false`, guardarás lo siguiente:)_

```json
{
  "username": "John Doe",
  "prefersDarkMode": false
}
```

### Un comentario extra

**Nótese** que podríamos _anidar_ un `JSONObject` dentro de otro si quisiéramos conseguir resultados como el primer ejemplo de la tarea. Por ejemplificarlo, sería algo así:

```java
// Esto es un ejemplo aparte
JSONObject jsonBirthDate = new JSONObject();
jsonBirthDate.put("day", 15);
jsonBirthDate.put("month", "October");
jsonBirthDate.put("year", 1844);

JSONObject jsonPhilosopher = new JSONObject();
jsonPhilosopher.put("birthDate", jsonBirthDate);
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
