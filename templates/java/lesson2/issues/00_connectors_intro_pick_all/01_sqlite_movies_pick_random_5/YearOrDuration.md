Condición OR

## :books: Resumen

* Añadiremos un nuevo método a `MoviesDataProvider` que lance una consulta condicional y devuelva los resultados en un `ArrayList`

## Descripción

Como ves, la lógica _booleana_ (AND, OR,...) está presente en muchas realidades de la informática.

Será bueno practicarlo un poco.

## :package: La tarea

**Se pide** que, en la clase `MoviesDataProvider`:

* Añadas un método `getResultsIssueISSUENUMBER` que abra una conexión a `movies.db`
* Lanzará una consulta para recuperar el **id** y **título** de las películas:
  * Posteriores al año RANDOMNUMBER1980-2010END **ó**
  * De duración inferior a RANDOMNUMBER90-160END minutos 
* Para cada fila del resultado, concatenará el **id** y **título**, usando un carácter `RANDOMEXPRESSION,|-|;END` (sin espacios en blanco), y lo acumulará en un `ArrayList<String>`
* _Devolverá_ el `ArrayList<String>` resultante

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
