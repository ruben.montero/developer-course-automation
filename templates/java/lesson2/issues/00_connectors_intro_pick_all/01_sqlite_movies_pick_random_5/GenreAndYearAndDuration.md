Más condiciones

## :books: Resumen

* Añadiremos un nuevo método a `MoviesDataProvider` que lance una consulta condicional y devuelva los resultados en un `ArrayList`

## Descripción

Esto de lanzar consultas, obtener resultados y devolverlos tiene su utilidad.

Será bueno practicarlo un poco.

## :package: La tarea

**Se pide** que, en la clase `MoviesDataProvider`:

* Añadas un método `getResultsIssueISSUENUMBER` que abra una conexión a `movies.db`
* Lanzará una consulta para recuperar el **título** y **año** de las películas con:
  * Género `comedy`, **y**
  * Anteriores al año RANDOMEXPRESSION2015|2010END, **y**
  * Con duración superior a RANDOMEXPRESSION80|87|100END minutos 
* Para cada fila del resultado, concatenará el **título** y **año**, usando un carácter `RANDOMEXPRESSION,|-|;END` (sin espacios en blanco), y lo acumulará en un `ArrayList<String>`
* _Devolverá_ el `ArrayList<String>` resultante

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
