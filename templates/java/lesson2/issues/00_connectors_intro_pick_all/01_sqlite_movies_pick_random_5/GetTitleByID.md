Título de una película por ID

## :books: Resumen

* Añadiremos un nuevo método a `MoviesDataProvider` que lance una consulta condicional y devuelva el resultado como `String`

## Descripción

Suele suceder que se recupera una **única** fila por ID en una base de datos.

Practiquémoslo. 

## :package: La tarea

**Se pide** que, en la clase `MoviesDataProvider`:

* Añadas un método `getResultIssueISSUENUMBER` que abra una conexión a `movies.db`
* Lanzará una consulta para recuperar el **título** de:
  * La película con `id` igual a RANDOMNUMBER1-8END
* _Devolverá_ el `String` resultante

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
