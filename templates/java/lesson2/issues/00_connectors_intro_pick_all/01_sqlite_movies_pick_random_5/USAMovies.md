¡U.S.A! ¡U.S.A!

## :books: Resumen

* Añadiremos un nuevo método a `MoviesDataProvider` que lance una consulta condicional y devuelva los resultados en un `ArrayList`

## Descripción

La industria norteamericana cuenta con la mayor producción de películas a nivel mundial.

## :package: La tarea

**Se pide** que, en la clase `MoviesDataProvider`:

* Añadas un método `getResultsIssueISSUENUMBER` que abra una conexión a `movies.db`
* Lanzará una consulta para recuperar el **título**, **año** y **duración** de:
  * Las películas de EE.UU. (código de país, `US`)
* Para cada fila del resultado, concatenará el **título**, **año** y **duración**, usando un carácter `RANDOMEXPRESSION,|-|;END` (sin espacios en blanco), y lo acumulará en un `ArrayList<String>`
* _Devolverá_ el `ArrayList<String>` resultante

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
