Abrir puerta... Cerrar puerta

## :books: Resumen

* Cerraremos la conexión que hemos abierto en la tarea anterior
* Hablaremos de la importancia del código destinado a _cleanup_ (cerrar conexiones, ficheros,...)

## :package: La tarea

Hemos **abierto** con éxito nuestra primera conexión a una base de datos SQLite.

Ahora, dejaremos escrita la sentencia destinada a **cerrar** la conexión.

Hazlo en el `try-catch` de `MoviesDataProvider`, debajo del _print_ anterior:

```java
        // ...
        try {
            Connection conn = DriverManager.getConnection(connectionStr);
            System.out.println(/* Mensaje anterior */);
            
            conn.close(); // Cerramos la conexión
            System.out.println("RANDOMEXPRESSIONConnection closed|Connection shutdown|Finished connection|The connection has been finished|The connection has been stopped|The connection has been shutdown|Farewell, connection!END");
            
        } catch (SQLException e) {
            // ...
```

Como ves, bastan con _invocar_ `.close();`.

Además, **se pide** que dejes añadido el _print_ posterior a cerrar la conexión, tal y como se indica arriba.

### ¿Por qué cerrar la conexión?

Normalmente, al **conectarse** a una base de datos, establecemos una conexión TCP (similar a la conexión que se abre cuando visitas una página web) y para ello **se consumen recursos** (memoria RAM, CPU) del _servidor_ de base de datos.

Al usar una base de datos SQLite en local, realmente no estamos _fastidiando_ a nadie. Pero si usásemos MySQL o PostgreSQL para conectarnos a un _servidor_, **sí** estaríamos _molestando_ a una máquina ajena.

### ¿Y qué pasa si no cierro la conexión?

La conexión seguirá abierta un tiempo, hasta que por _timeout_ u otra política del servidor, se cierre sola. Sin embargo, en el proceso, puede que antes se _cierren otras conexiones_ distintas por nuestra culpa, o bien, que el _servidor_ sufra una sobrecarga por la cual se reinicie, se apague, o, simplemente, deje de estar en servicio temporalmente.

### ¡Qué interesante es este tema!

Si la ciberseguridad te resulta llamativa, aquí tienes algunos enlaces:

* [¿Qué es la ciberseguridad?](https://itecan.es/que-es-la-ciberseguridad/)
* [¿Qué es el _hacking_ ético?](https://www.avansis.es/ciberseguridad/hacking-etico-que-es/)
* [Tipos de hackers y sombreros](https://esgeeks.com/tipos-hackers-y-sombreros/)
* [Cómo almacenar contraseñas en bases de datos](https://openwebinars.net/blog/almacenar-contrasenas-bases-de-datos/)
* [¿Qué son los ataques CORS y cómo evitarlos?](https://ciberseguridad.com/amenzas/vulnerabilidades/ataques-cors/)

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
