Condiciones

## :books: Resumen

* Veremos ejemplos de uso de la cláusula `WHERE` en SQL.
* Añadiremos un nuevo método a `MoviesDataProvider`, similar al anterior, que esta vez lance una consulta que use `WHERE`

## Descripción

La vida y lo que hay en ella está lleno de condiciones. Las cláusulas de una hipoteca. El pago del seguro de un vehículo. La nota mínima para aprobar una asignatura...

Podemos decir que estas condiciones reducen y (valga la redundancia) _condicionan_ nuestras libertades. Pero también podemos ver positivamente que, en muchas ocasiones, nos fuerzan a ir por un camino determinado que nos hace aprender a ser parte del mundo.

Ahora, vamos a imponer nosotros mismos nuestras condiciones.

### ¿Cómo usar WHERE en SQL?

Hay que redactar una condición lógica. Podemos hacer usando comparadores _mayor que_ (`>`), _menor que_ (`<`) o de _igualdad_ (`=`) _**¡Ojo!** 1 igual, y no 2 (`==`) como en Java_

Algunos ejemplos:

```sql
SELECT year
FROM TMovies
WHERE title="Alien"
```

```sql
SELECT title
FROM TMovies
WHERE year<2000
```

```sql
SELECT title
FROM TMovies
WHERE countryIso3166="FR"
```

### AND y OR

También se pueden usar operadores lógicos para agrupar varias condiciones.

Por ejemplo:

```sql
SELECT title, synopsis
FROM TMovies
WHERE countryIso3166="US" AND duration<100
```

```sql
SELECT *
FROM TMovies
WHERE genre="comedy" OR genre="science-fiction"
```

## :package: La tarea

**Se pide** que:

* Añadas un método _similar_ al de la tarea anterior
* Esta vez se llamará `getColumnUsingWhere`
* Recuperará el **título** de todas las películas posteriores al año RANDOMEXPRESSION1990|2001|2005|2010END
* Devolverá un `ArrayList<String>` con los valores del resultado

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

