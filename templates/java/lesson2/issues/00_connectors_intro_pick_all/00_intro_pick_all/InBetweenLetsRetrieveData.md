Hagamos algo con esa conexión

## :books: Resumen

* Expondremos la estructura de `movies.db`
* Emplearemos `.createStatement` y `.executeQuery` para lanzar un `SELECT` que consulte los títulos de todas las películas
* Asignaremos el resultado de la consulta a un objeto `ResultSet`
* Usaremos un bucle `while` para imprimir, uno a uno, los valores obtenidos

## Descripción

Hemos abierto una conexión a una base de datos local SQLite, y luego la hemos cerrado.

Estaría bien **hacer algo** en el medio. Por ejemplo, **consultar** datos.

### ¿Qué datos?

Nuestra base de datos `movies.db` contiene información sobre películas. Es muy **sencilla**. Contiene **sólo una tabla** llamada `TMovies` con los siguientes **campos** (o _columnas_)

* **id** (INTEGER)
* **title** (VARCHAR(200))
* **year** (SMALLINT)
* **duration** (SMALLINT)
* **countryIso3166** (CHAR(2))
* **genre** (VARCHAR(30))
* **synopsis** (VARCHAR(1000))

#### ¿Columnas?

Efectivamente. Recordemos que la idea del [modelo relacional](https://economipedia.com/definiciones/modelo-relacional.html) es que los datos se almacenan en forma de tablas:

|id |title                    |year|duration|countryIso3166|genre |synopsis
|---|-------------------------|----|--------|--------------|------|--------
|1  |The Shawshank Redemption |1994|142     |US            |drama |Acusado del asesinato de su mujer, Andrew Dufresne (Tim Robbins), tras ser condenado a cadena perpetua, es enviado a la cárcel de Shawshank. Con el paso de los años...
|2  |Secondhand Lions         |2003|109     |US            |comedy|Cuando al introvertido Walter (Haley Joel Osment) lo obliga su irresponsable madre (Kyra Sedgwick) a pasar el verano en un rancho de Texas con sus viejos y excéntricos tíos (Michael Caine y Robert Duvall), a los que apenas conoce, la idea no le hace ninguna gracia. Pero tampoco a ellos les agrada mucho la idea de cuidar del chaval. Sin embargo...
|...|...                      |... |...     |...           |...   |...

A pesar de que la **gracia de SQL** está en que diferentes tablas se **relacionan** entre sí, en nuestro primer caso de uso, sólo lanzaremos `SELECT` sencillos a **una** tabla `TMovies`

### Un `SELECT` sencillo

Vamos a consultar sólo el campo `title` de las filas de la solitaria tabla `TMovies`. Así:

```sql
SELECT title
FROM TMovies;
```

## :package: La tarea

Primero, en el constructor de `MoviesDataProvider`, después de abrir la conexión y antes de cerrarla, **crea** un `Statement` invocando `.createStatement()` sobre la _instancia_ de nuestra conexión.


```java
Statement statement = conn.createStatement();
```

Sobre dicho objeto podemos invocar `.executeQuery`, y pasar como parámetro **el código SQL** en formato `String`. _Devolverá_ el **resultado** de la consulta en un objeto de tipo `ResultSet`:

```java
ResultSet result = statement.executeQuery("SELECT title FROM TMovies");
```

### Leyendo el resultado

Nuestro objeto `ResultSet` contiene los datos resultantes, **y** un cursor para acceder a ellos uno por uno

### ¿Cursor?

Es como un puntero que almacena internamente _qué_ fila estamos leyendo.

Por ejemplo, si una hipotética consulta `"SELECT name FROM developers;"` devuelve un resultado con 3 filas:

|name    |
|--------|
|Alice   |
|Bob     |
|Charlie |

...invocar `.next()` _avanzará_ el cursor al siguiente resultado (el primero):

```java
result.next();
```

|name    |
|--------|
|**Alice**   |
|Bob     |
|Charlie |

Una vez ahí, podemos recuperar los datos _de esa fila_.

Por ejemplo, para acceder a una **cadena de texto** (e.g.: VARCHAR) de la **primera** columna (¡sólo hay **una**!), invocamos:

```java
// Devuelve "Alice"
String primerNombre = result.getString(1);
```

**¡Ojo!** Se empieza en **1**, **no** en **0**.

### Terminando

Después de la línea que genera un `ResultSet`, **escribe**[^1]:

```java
while (result.next()) {
    // Para cada fila, imprimimos la primera (y única) columna tipo VARCHAR que hay
    System.out.println(result.getString(1));
}
```

Como ves, esto imprime el _título_ de cada película.

**Ejecuta** `Main`. ¿Funciona bien?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------------------------------------

[^1]: Como ves, `result.next()` devuelve un `boolean`. Si hay más filas disponibles, es `true`. En caso contrario, es `false`. ¡Por eso lo invocamos dentro de la condición del `while`!

