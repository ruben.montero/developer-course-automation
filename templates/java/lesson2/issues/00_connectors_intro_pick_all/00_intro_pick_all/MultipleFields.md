Más de una columna

## :books: Resumen

* Añadiremos un nuevo método a `MoviesDataProvider` que abra una conexión
* Lance una consulta que recupere más de una columna
* Acumule los resultados en un `ArrayList`
* Cierre la conexión
* Devuelva (`return`) los resultados
  
## Descripción

Nuestra primera consulta era:

```sql
SELECT title
FROM TMovies;
```

...que recupera los **títulos** de cada fila:

|id |title                    |year|duration|countryIso3166|genre |synopsis
|---|-------------------------|----|--------|--------------|------|--------
|1  |The Shawshank Redemption |1994|142     |US            |drama |Acusado del asesinato de su mujer, Andrew Dufresne (Tim Robbins), tras ser condenado a cadena perpetua, es enviado a la cárcel de Shawshank. Con el paso de los años...
|2  |Secondhand Lions         |2003|109     |US            |comedy|Cuando al introvertido Walter (Haley Joel Osment) lo obliga su irresponsable madre (Kyra Sedgwick) a pasar el verano en un rancho de Texas con sus viejos y excéntricos tíos (Michael Caine y Robert Duvall), a los que apenas conoce, la idea no le hace ninguna gracia. Pero tampoco a ellos les agrada mucho la idea de cuidar del chaval. Sin embargo...
|...|...                      |... |...     |...           |...   |...

Esta consulta sólo selecciona una columna. La columna `title`.

### ¡Se puede seleccionar **más** de un campo en una consulta!

Por ejemplo:

```sql
SELECT title, year, duration
FROM TMovies;
```

```sql
SELECT id, countryIso3166, synopsis
FROM TMovies;
```

O, si queremos seleccionar _todos_ los campos, podemos hacerlo con:

```sql
SELECT *
FROM TMovies;
```

```java
Statement statement = conn.createStatement();
ResultSet result = statement.executeQuery("SELECT title, year FROM TMovies");
while(result.next()) {
    System.out.println("La película es del año:");
    System.out.println(result.getInt(2)); // Funciona
    System.out.println(result.getInt(3)); // Falla. Hay que especificar el índice de la columna dentro del resultado
}
```

## :package: La tarea

**Añade** un nuevo método en nuestra clase `MoviesDataProvider`:

* Público
* Devolverá una lista de _Strings_ (`ArrayList<String>`)
* Se llamará `getTwoColumns`
* **No** recibirá parámetros:

Así:

```java
    public ArrayList<String> getTwoColumns() {

    }
```

### Algo muy típico: Acumular elementos a devolver

En primer lugar, **declara** e **inicializa** una variable `ArrayList` vacía.

Al final, la devolverá:

```java
    public ArrayList<String> getTwoColumns() {
        ArrayList<String> finalResult = new ArrayList<>();
        // ...
        return finalResult;
    }
```

### La consulta SQL

Primero, **abre** y **cierra** la conexión como ya hemos visto:

```java
    public ArrayList<String> getTwoColumns() {
        ArrayList<String> finalResult = new ArrayList<>();
        String connectionStr = "jdbc:sqlite:db/sqlite3/movies.db";
        try {
            Connection conn = DriverManager.getConnection(connectionStr);
            
            // ...
            
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return finalResult;
    }
```

Y luego, **lanza** una consulta que recupera **dos** columnas de todas las filas de la tabla, como se muestra a continuación.

Para **cada** fila, _fabricamos_ un nuevo `String` que concatena los **dos** valores y lo añadimos al resultado final:

```java
    public ArrayList<String> getTwoColumns() {
        ArrayList<String> finalResult = new ArrayList<>();
        String connectionStr = "jdbc:sqlite:db/sqlite3/movies.db";
        try {
            Connection conn = DriverManager.getConnection(connectionStr);
            // Crear y ejecutar la consulta
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT RANDOMEXPRESSIONtitle, genre|title, countryIso3166|genre, countryIso3166|genre, titleEND FROM TMovies");
            while(result.next()) {
                String firstColumnValue = result.getString(1);
                String secondColumValue = result.getString(2);
                String concatenated = firstColumnValue + ", " + secondColumValue;
                finalResult.add(concatenated);
            }
            conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return finalResult;
    }
```

Si quieres probar el método recién creado desde `Main.java`, algo como esto será suficiente:

```java
        MoviesDataProvider provider = new MoviesDataProvider();
        for (String lineOfMyArray : provider.getTwoColumns()) {
            System.out.println(lineOfMyArray);
        }
```

¿Tienen sentido los resultados que se ven por pantalla?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

