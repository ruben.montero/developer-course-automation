Mi primera conexión a una base de datos

## :books: Resumen

* Hablaremos de la evolución de las bases de datos
* Comprenderemos que hay distintos tipos de _sistemas gestores de bases de datos_ (SGBD)
* Escribiremos una clase nueva en Java que se conectará a una base de datos mediante un _conector_
* _(Y como siempre, subiremos los cambios al repositorio)_

## Descripción

Las [bases de datos](https://www.ionos.es/digitalguide/hosting/cuestiones-tecnicas/bases-de-datos-relacionales/) son una parte esencial de todo sistema informático. ¿Haces una compra _online_ y esperas que el pedido esté en el historial? ¿Inicias sesión en Twitter y ves tus _tweets_ marcados como favoritos? ¿Realizas un trámite en la [sede de la Xunta](https://sede.xunta.gal/) y finalmente llega el resultado del procedimiento? ¿Juegas a tu título favorito de [PlayStation](https://www.playstation.com/es-es/) (¿o [Xbox](https://www.xbox.com/es-ES/)? ¿[Switch](https://www.nintendo.com/es_LA/switch/)?) y la partida sigue donde la dejaste? **Claro**. **Nosotros, desarrolladores**, escribimos **programas** que procesan datos de forma **automatizada**.

Pero, ¿_dónde_ se **almacenan** esos **datos**?

Para no tenerle miedo a las _bases de datos_, lo mejor es entender que surgen como evolución natural de nuestras necesidades como desarrolladores.

1. Los lenguajes de programación y entornos de desarrollo eran muy distintos décadas atrás
2. Para las necesidades de una aplicación, probablemente se guardase información en ficheros _(¡como nosotros en el proyecto anterior!)_ a gusto del programador
3. Así que cada programa vivía en su pequeño reino e intercambiar o reutilizar datos se volvía complicado
4. Además surgían problemas de inconsistencias, duplicidades...

### La evolución natural: SQL

[_Structured Query Language_ (SQL)](https://es.wikipedia.org/wiki/SQL) es un lenguaje que viene de la mano del [_modelo relacional_](https://es.wikipedia.org/wiki/Modelo_relacional) de [Frank Codd](https://es.wikipedia.org/wiki/Edgar_Frank_Codd). 

La idea es muy sencilla:

* Guardar los datos en forma de **tablas** _(modelo relacional)_
* Tener un **lenguaje estandarizado** para procesar los datos _(SQL)_

#### ¿Pero **cómo** exactamente se guardan los datos?

¡Ah! Depende.

Eso, el bueno de [Frank](https://es.wikipedia.org/wiki/Edgar_Frank_Codd) no nos lo dijo. Tan sólo podemos afirmar que SQL pasó a ser el estándar [ANSI](https://es.wikipedia.org/wiki/Instituto_Nacional_Estadounidense_de_Est%C3%A1ndares) en 1986, e [ISO](https://es.wikipedia.org/wiki/Organizaci%C3%B3n_Internacional_de_Normalizaci%C3%B3n) en 1987.

¿**Cómo** implementar el estándar? ¿**Cómo** guardar los datos?

Depende.

Es decir, los **programas** que soportan **consultas SQL** y almacenan y entregan los datos, cada uno a su manera, son muchos y muy variados. Tienen un nombre:

### Sistemas gestores de bases de datos (SGBD)

[MySQL](https://www.mysql.com/), [MariaDB](https://mariadb.org/), [SQLite](https://www.sqlite.org/index.html), [PostgreSQL](https://www.postgresql.org/) o [SQLServer](https://www.microsoft.com/es-es/sql-server/sql-server-downloads) son algunos ejemplos de _sistemas gestores de bases de datos_.

Nosotros trabajaremos principalmente con **SQLite** por su simplicidad.

## :package: La tarea

Abre un terminal (cmd.exe) en tu ordenador y cambia el directorio activo hasta la ubicación de tu repositorio usando `cd`. Haz:

```
git pull
```

...para traer los cambios de remoto a local, que contienen correcciones del proyecto anterior y **el esqueleto** del **nuevo proyecto**. Se encontrará en una carpeta llamada `java-rest/`.

Abre `java-rest/` desde [IntelliJ IDEA](https://www.jetbrains.com/idea/) y verás un entorno con el que ya eres familiar.

Dentro de `src/`, **crea** una nueva clase llamada `MoviesDataProvider`.

**Será** así:

```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MoviesDataProvider {

    // Método constructor
    public MoviesDataProvider() {
        String connectionStr = "jdbc:sqlite:db/sqlite3/movies.db";
        try {
            Connection conn = DriverManager.getConnection(connectionStr);
            // Se ha establecido la conexión
            // ...
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
```

Este código _se conecta programáticamente_ (desde Java) a una base de datos. Para ello, usa un _conector_.

### ¿Qué es un **conector**?

Es una clase Java que se encarga de realizar la conexión.

Estas clases **no** las escribimos nosotros, sino los desarrolladores del SGBD correspondiente.

En nuestro proyecto `java-rest/`, dentro de `lib/`, **ya** están añadidos los conectores necesarios para que se puedan establecer conexiones a:

* Bases de datos SQLite ([`sqlite-jdbc-3.36.0.3.jar`](https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc))

Estos conectores son fácilmente accesibles buscando los `.jar` en Internet, o añadiendo las dependencias al proyecto si usamos un sistema automatizado como [Maven](https://www.genbeta.com/desarrollo/que-es-maven) o [Gradle](https://gradle.org/).

Para realizar la conexión, como ya has hecho, hay que _invocar_ el método [estático](https://www.geeksforgeeks.org/static-method-in-java-with-examples/) `.getConnection` de la clase `DriverManager` del paquete `java.sql`. Viene a ser un [patrón factoría](https://www.ionos.es/digitalguide/paginas-web/desarrollo-web/patron-factory/).

Nótese que tanto `DriverManager` como `Connection` son clases estándar de Java. Podemos usarlas _aunque no hayamos añadido_ los `.jar` de los conectores arriba mencionados. En `.getConnection` **pediremos** una conexión a un SGBD en particular, y si **no** está presente el conector necesario, saltará una **excepción**. También, si falla la conexión.

### `String` para describir una conexión

A `.getConnection` le hemos pasado un `String` de la siguiente forma:

```
"jdbc:<tipo de SGBD>:<parámetros>"
```

### [Conectarse a SQLite](https://www.sqlitetutorial.net/sqlite-java/sqlite-jdbc-driver/)

Cada SGBD necesita diferentes parámetros.

[SQLite](https://sqlite.org/about.html) es un SGBD **ultra-ligero**. Básicamente, **toda** la base de datos se almacena en un **fichero** en el disco duro. Por lo tanto, para conectarnos a una base de datos **SQLite** sólo necesitamos conocer la **ruta** al fichero de base de datos. Usando una ruta relativa:

```
"jdbc:sqlite:unaCarpeta/otraCarpeta/miBaseDeDatos.db"
```

...o absoluta, con esta sintaxis:

```
"jdbc:sqlite:C:/carpetaEnRaiz/otraCarpeta/miBaseDeDatos.db"
```

También se permiten bases de datos en memoria RAM usando SQLite (`"jdbc:sqlite::memory:"`), aunque no lo usaremos.

### Para terminar

**Añade** un `System.out.println` en el constructor de `MoviesDataProvider` **después** de que se establezca la conexión. Imprimirá `"RANDOMEXPRESSIONConnection established|Successfully connected|The connection has been established successfully|I have connected to the database|I have connected to the SQLite databaseEND"`.

Si creas una nueva _instancia_ desde `Main.java`:

```java
public class Main {
    public static void main(String[] args) {
        MoviesDataProvider provider = new MoviesDataProvider();
    }
}
```

...y añades una _configuración_ (barra superior > Add Configuration) para `Main`; al darle al **botón verde 'Play'** debería ejecutarse la aplicación y mostrar el _print_ en pantalla.

**¡Enhorabuena!** Has abierto tu primera conexión automática a un _sistema gestor de base de datos_ mediante un conector Java.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
