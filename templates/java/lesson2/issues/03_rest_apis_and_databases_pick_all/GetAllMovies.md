GET /movies

## :books: Resumen

* Crearemos una nueva clase `MoviesREST`
* Uniremos los dos grandes conceptos trabajados hasta ahora:
  * **APIs REST**
  * **Conectores a bases de datos**
* Serviremos en un nuevo _endpoint_ `/movies` los datos de las películas en `movies.db`

## Descripción

Hemos trabajado con dos conceptos esenciales durante este proyecto: APIs REST y conectores JDBC para bases de datos.

¿Es posible unirlos? ¿Tiene sentido? ¿Cómo?

### [Uniendo dos conceptos](https://www.youtube.com/watch?v=NfuiB52K7X8)

Las respuestas son **sí**, **sí**, y **así**:

## :package: La tarea

**Crea** una nueva clase `MoviesREST.java`. Será similar a `SimpleREST.java` pero estará destinada a ofrecer un API REST que por detrás trabaja contra la base de datos SQLite `movies.db`:

```java
import org.restlet.Component;
import org.restlet.data.Protocol;
import org.restlet.routing.VirtualHost;

public class MoviesREST {
    private Component component;

    public void runServer() {
        try {
            this.component = new Component();
            this.component.getServers().add(Protocol.HTTP, 81DEVELOPERNUMBER);
            VirtualHost host = this.component.getDefaultHost();
            // Aquí mapearemos los endpoints
            // ...
            this.component.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void stopServer() throws Exception {
        if (this.component != null) {
            this.component.stop();
        }
    }
}
```

Como puedes ver, también escucha en el puerto TCP 81DEVELOPERNUMBER, por lo que no es posible que una instancia de `MoviesREST` y una de `SimpleREST` estén activas simultáneamente.

### Un primer _endpoint_: `/movies`

## Empecemos por el POJO

**Añadamos** un nuevo POJO `Movie.java`, con los **atributos** que queramos enseñar desde nuestra API REST. Usaremos los mismos que hay disponibles en la base de datos:

```java
public class Movie {
    private int id;
    private String title;
    private int year;
    private int duration;
    private String countryIso3166;
    private String genre;
    private String synopsis;
    
}
```

Ahora, podemos darle a **Code > Generate > Constructor** para crear automáticamente el método constructor _con todos_ los atributos.

Para terminar el POJO, crearemos un método `toJSONObject` que _serialice_ a `JSONObject` **todos** los atributos, usando como _clave_ siempre el mismo _nombre_ del atributo:

```java
    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("id", this.id);
        object.put("title", this.title);
        // Resto de atributos
        // ...
        
        return object;
    }
```

## Clase `ServerResource`

Ahora, **añadamos** una nueva subclase de `ServerResource`.

La llamaremos `GetAllMovies.java`:

```java
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class GetAllMovies extends ServerResource {

    @Get
    public StringRepresentation getEndpointResponse() {
        // ...

    }
}
```

Ahora la idea es que en `getEndpointResponse`, nos **conectemos** a la base de datos, lancemos un `SELECT` y usemos el resultado.

### ¿Podemos escribir todo el código aquí, en `getEndpointResponse`?

Sí.

### ¿Lo haremos?

No.

Vamos a usar una solución más elegante, aprovechando el `MoviesConnector.java` de tareas anteriores.

### Nuevo método en `MoviesConnector`

**Añadamos** a dicha clase un nuevo método que devuelva una _lista de `Movies`_:

```java
    public ArrayList<Movie> getAll() {

    }
```

¿Recuerdas cómo usábamos `this.connection` en dicha clase para crear un `statement` y lanzar una consulta? ¿Recuerdas la diferencia entre `executeUpdate` y `executeQuery`?

Nosotros lanzaremos un `SELECT * FROM TMovies` y usaremos un bucle `while` donde se _instancia_ una nueva `Movie` por cada película, y se se _acumula_ en un `ArrayList` que se devuelve:

```java
    public ArrayList<Movie> getAll() {
        ArrayList<Movie> allMovies = new ArrayList<>();
        try {
            Statement statement = this.connection.createStatement();
            String sql = "SELECT * FROM TMovies";
            ResultSet result = statement.executeQuery(sql);
            while(result.next()) {
                int id = result.getInt(1);
                String title = result.getString(2);
                int year = result.getInt(3);
                int duration = result.getInt(4);
                String countryIso3166 = result.getString(5);
                String genre = result.getString(6);
                String synopsis = result.getString(7);
                // Instanciamos una nueva Movie con los valores de la fila
                Movie aMovie = new Movie(id, title, year, duration, countryIso3166, genre, synopsis);
                allMovies.add(aMovie);
            }
            statement.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return allMovies;
    }
```

**¡Listo!**

## Completando `ServerResource`

Ya estamos en condiciones de terminar la clase **`GetAllMovies.java`** que comenzamos anteriormente.

Sólo debemos...

#### 1) Instanciar un `MoviesConnector`

```java
    @Get
    public StringRepresentation getEndpointResponse() {
        MoviesConnector connector = new MoviesConnector();
        // ...
    }
```

#### 2) Instanciar un `JSONArray`

Se trata del _array_ JSON (`[ ]`) que enviaremos como respuesta HTTP.

De momento, está vacío.

```java
    @Get
    public StringRepresentation getEndpointResponse() {
        MoviesConnector connector = new MoviesConnector();
        JSONArray resultArray = new JSONArray();
        // ...
    }
```

#### 3) Invocar el `connector.getAll()` que creamos anteriormente

Y almacenamos el resultado en una variable tipo `ArrayList<Movie>`

```java
    @Get
    public StringRepresentation getEndpointResponse() {
        MoviesConnector connector = new MoviesConnector();
        JSONArray resultArray = new JSONArray();
        ArrayList<Movie> databaseFilms = connector.getAll();
        // ...
    }
```

#### 4) Para cada resultado, lo añadimos al _array_ JSON

Usamos un bucle `for-each` por su sencillez, aunque un bucle `for` convencional también sería posible.

```java
    @Get
    public StringRepresentation getEndpointResponse() {
        MoviesConnector connector = new MoviesConnector();
        JSONArray resultArray = new JSONArray();
        ArrayList<Movie> databaseFilms = connector.getAll();
        for (Movie mov : databaseFilms) {
            resultArray.put(mov.toJSONObject());
        }
        // ...
    }
```

#### 5) Finalmente, devolvemos todo como un `StringRepresentation`

```java
    @Get
    public StringRepresentation getEndpointResponse() {
        MoviesConnector connector = new MoviesConnector();
        JSONArray resultArray = new JSONArray();
        ArrayList<Movie> databaseFilms = connector.getAll();
        for (Movie mov : databaseFilms) {
            resultArray.put(mov.toJSONObject());
        }
        connector.closeConnection(); // No olvidemos cerrar la conexión
        String jsonString = resultArray.toString();
        StringRepresentation representation = new StringRepresentation(jsonString);
        representation.setMediaType(MediaType.APPLICATION_JSON);
        return representation;
    }
```

**¡Listo!**

Basta con _mapear_ el _endpoint_ adecuadamente en `MoviesREST.java`:

```diff
-            // Aquí mapearemos los endpoints
-            // ...
+            host.attach("/movies", GetAllMovies.class);
```

### Probando y saliendo victoriosos

Si instancias y ejecutas tu nuevo servidor en `Main.java`:

```diff
-        SimpleREST myServer = new SimpleREST();
-        myServer.runServer();
+
+        MoviesREST server = new MoviesREST();
+        server.runServer();
```

y le das a **Play**, ya podrás visitar http://localhost:81DEVELOPERNUMBER/movies desde tu navegador.

Estás observando los **datos** de la **base de datos _SQLite_**.

#### ¿Qué significa esto?

Que has desarrollado tu primera API REST que **realmente** consume datos de una base de datos.

**¡Enhorabuena!** Es un paso muy importante.

Ahora, la base de datos podría modificarse y tu servidor, sin detenerse y relanzarse, mostraría los datos **actualizados** a las **nuevas peticiones**.

Así es como funcionan las cosas en el mundo de las aplicaciones distribuidas.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

