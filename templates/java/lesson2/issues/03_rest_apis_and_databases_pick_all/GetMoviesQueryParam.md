GET /movies?queryParam=value

## :books: Resumen

* Hablaremos de _query params_
* Soportaremos un _query param_ `orderBy...` en la petición `/movies` que permita al cliente HTTP especificar si quiere los resultados ordenador por cierto criterio

## Descripción

Somos capaces de soportar _path params_ como:

* http://localhost:81DEVELOPERNUMBER/movies/2
* http://localhost:81DEVELOPERNUMBER/movies/6

Aún nos falta hablar de _query params_.

Estos _query params_ viajan normalmente en la **línea de petición** detrás de un símbolo de interrogación (`?`), y tienen la forma `clave=valor`.

Por ejemplo:

```
http://localhost:81DEVELOPERNUMBER/movies?favorites=true
```

Si hay más de uno, se pueden anexar con _ampersand_ (`&`):

```
http://localhost:81DEVELOPERNUMBER/movies?favorites=true&genre=comedy
```

### ¿Para qué sirven?

Permiten a los clientes HTTP especificar condiciones adicionales sobre los resultados de una consulta REST, y así filtrar, ordenar, paginar...

Nosotros, a través de un ejemplo, veremos **cómo tratar con _query params_ usando Restlet**.

### `getQueryValue`

Se usa en la clase `ServerResource`, _**a modo de ejemplo**_, así:

```java
    @Get
    public StringRepresentation getEndpointResponse() {
        String queryParamValue = getQueryValue("favorites");
        // ...
    }
```

...si se tratase de un valor `boolean`, **podría** _parsearse_ con estas líneas:

```java
    @Get
    public StringRepresentation getEndpointResponse() {
        String queryParamValue = getQueryValue("favorites");
        boolean shouldShowFavorites = false;
        if (queryParamValue != null){
            shouldShowFavorites = Boolean.parseBoolean(queryParamValue);
        }
        // ...
    }
```

## :package: La tarea

**Se pide** que:

* Soportes leer el _query param_ `RANDOMEXPRESSIONorderByDurationAsc|orderByDurationDesc|orderByYearAsc|orderByYearDescEND` en `GetAllMovies.java`
  * Si el _query param_ **no** viene, o viene como `false`, el _endpoint_ se comportará como hasta ahora
  * Si viene como `true`, devolverás la lista de películas ordenadas según el criterio
    * Puedes conseguirlo de varias maneras. Algunas ideas:
      * _Ordenar_ el _array_ resultante de `.getAll`
      * Modificar la consulta SQL para que use [`ORDER BY`](https://www.sqltutorial.org/sql-order-by/)

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

