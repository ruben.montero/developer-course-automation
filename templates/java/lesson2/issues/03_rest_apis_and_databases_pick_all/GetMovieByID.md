GET /movies/{movieID}

## :books: Resumen

* Crearemos un nuevo método en `MoviesConnector.java`
* Crearemos una nueva clase `GetMovieByID.java`
* Entenderemos qué es un _path param_
* Serviremos en un nuevo _endpoint_ `/movies/{movieID}` los datos de las película con `id = {movieID}` de `movies.db`

## Descripción

Todos nuestros _endpoints_, hasta ahora, eran **fijos**.

Vamos a empezar a trabajar con _endpoints_ que tiene **partes variables** ó _**path params**_.

En esta tarea conseguiremos que el usuario pueda visitar:

* `http://localhost:81DEVELOPERNUMBER/movies/2`
* `http://localhost:81DEVELOPERNUMBER/movies/6`
* etc.

...y obtenga en JSON la información de la película con `id=2`, `id=6`, etc.

## :package: La tarea

### Primer paso: Otro método en `MoviesConnector`

**Crea** un método similar al de la tarea anterior, pero en esa ocasión recibe un **parámetro** tipo `int`. Se prevee que sea el `id` de la película deseada:

```java
    public Movie RANDOMEXPRESSIONgetByID|retrieveByID|fetchByID|getMovieUsingID|retrieveMovieUsingID|fetchMovieUsingIDEND(int movieID) {
        Movie aMovie = null;
        try {
            Statement statement = this.connection.createStatement();
            String sql = "SELECT * FROM TMovies WHERE id = " + movieID;
            ResultSet result = statement.executeQuery(sql);
            while(result.next()) {
                int id = result.getInt(1);
                String title = result.getString(2);
                int year = result.getInt(3);
                int duration = result.getInt(4);
                String countryIso3166 = result.getString(5);
                String genre = result.getString(6);
                String synopsis = result.getString(7);
                aMovie = new Movie(id, title, year, duration, countryIso3166, genre, synopsis);
            }
            statement.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return aMovie;
    }
```

La `Movie` devuelta será `null` si **no** existe una película con el `int movieID` recibido.

### Segundo paso: Un `ServerResource`

**Añadamos** un nuevo `GetMovieByID.java`.

Para poder interpretar correctamente el _path param_ enviado por el cliente HTTP que mencionamos anteriormente:

* `http://localhost:81DEVELOPERNUMBER/movies/{pathParam}`

...la clave será usar `getAttribute("nombrePathParam")`.

Podemos invocarlo _alegremente_ sobre la instancia actual porque **heredamos** de `ServerResource`. Así:

```java
public class GetMovieByID extends ServerResource {
    @Get
    public StringRepresentation getEndpointResponse() {
        String movieID = getAttribute("movieID");
        System.out.println("El ID de película visitado es: " + movieID);
        // ...
    }
}
```

Este código ayudará a que cuando visitemos `http://localhost:81DEVELOPERNUMBER/movies/6`, veamos que IntelliJ IDEA imprime por consola:

```
El ID de película visitado es 6
```

**¡Ojo!** Si visitásemos `http://localhost:81DEVELOPERNUMBER/movies/pepeDepura`, también veríamos:

```
El ID de película visitado es pepeDepura
```

Como queremos **ceñirnos a valores numéricos** en **esta ocasión**, usaremos `Integer.parseInt(...)`:

```java
public class GetMovieByID extends ServerResource {
    @Get
    public StringRepresentation getEndpointResponse() {
        String movieID = getAttribute("movieID");
        Integer movieInt = Integer.parseInt(movieID);
        System.out.println("El ID de película visitado es: " + movieInt);
        // ...
    }
}
```

Si nos olvidamos del _print_ y escribimos el código completo, sería:

```java
    @Get
    public StringRepresentation getEndpointResponse() {
        String movieID = getAttribute("movieID");
        Integer movieInt = Integer.parseInt(movieID);
        MoviesConnector connector = new MoviesConnector();
        Movie movie = connector.RANDOMEXPRESSIONgetByID|retrieveByID|fetchByID|getMovieUsingID|retrieveMovieUsingID|fetchMovieUsingIDEND(movieInt);
        // Aquí podríamos controlar que el valor no sea nulo
        // De momento, lo dejamos así
        connector.closeConnection();
        String jsonString = movie.toJSONObject().toString();
        StringRepresentation representation = new StringRepresentation(jsonString);
        representation.setMediaType(MediaType.APPLICATION_JSON);
        return representation;
    }
```

### Tercer paso: _Mapear_ el _endpoint_

Lo haremos en `MoviesREST.java` igual que en la tarea anterior.

Pero, **muy importante**, debemos **especificar entre llaves (`{ }`)** la parte correspondiente al **`path param`** esperado.

Es decir:

```java
            host.attach("/movies/{movieID}", GetMovieByID.class);
```

Nótese que **`{movieID}`** debe ser equivalente al **`getAttribute("movieID");`** para que funcione correctamente.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.


