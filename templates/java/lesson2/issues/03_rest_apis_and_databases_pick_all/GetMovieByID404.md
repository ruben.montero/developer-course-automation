GET /movies/{movieID} (404)

## :books: Resumen

* Si la película especificada no existe, devolveremos un código de error HTTP `404 Not Found`
* Para ello, añadiremos un `if` y lanzaremos una excepción `ResourceException` en `GetMovieByID.java`

## Descripción

Ahora mismo, si visitamos:

* http://localhost:81DEVELOPERNUMBER/movies/4984 _(el ID no existe)_

...la página de respuesta indicará **Internal Server Error**, y en IntelliJ IDEA podemos ver:

```
Exception or error caught in server resource

org.restlet.resource.ResourceException: Internal Server Error (500) - The server encountered an unexpected condition which prevented it from fulfilling the request
	at org.restlet.resource.ServerResource.doHandle(ServerResource.java:527)
	at org.restlet.resource.ServerResource.get(ServerResource.java:723)
	...
```

### Un código de error adecuado

Las respuestas HTTP van siempre acompañadas de un código:

* 1xx Información
* 2xx Éxito
* 3xx Redirección
* 4xx Error cliente
* 5xx Error servidor

Hasta ahora no nos hemos preocupado por ello, pues hemos programado el _happy path_ por el cual las cosas **siempre** salen bien y una respuesta 2xx es generada automáticamente. Predeciblemente, `200 OK`.

Pero ahora, ante un cliente que visita un `/movies/{movieID}` con un `{movieID}` inválido... ¡está saltando un `NullPointerException` que Restlet traduce en _Internal Server Error_!

Nuestra `Movie movie` es `null`.

## :package: La tarea

En `GetMovieByID.java`, podemos controlar dicho escenario:

```java
    @Get
    public StringRepresentation getEndpointResponse() {
        String movieID = getAttribute("movieID");
        Integer movieInt = Integer.parseInt(movieID);
        MoviesConnector connector = new MoviesConnector();
        Movie movie = connector.getByID(movieInt);
        if (movie == null) {
            // ...
        }
        // ...
```

#### `ResourceException`

Para encargar a Restlet que _devuelva un código HTTP_ en concreto, lo hacemos _lanzando_ una excepción.

Igual que el típico `throw new RuntimeException();` que solíamos escribir. Pero en esta ocasión, será:

```java
        if (movie == null) {
            throw new ResourceException(404, "RANDOMEXPRESSIONMovie not found|The movie was not found|This movie does not exist|Film not found|The film was not found|This film does not existEND");
        }
```

**¡Listo!**

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.


