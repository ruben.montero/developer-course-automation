Un método más genérico

## :books: Resumen

* Implementaremos un método en `MoviesConnector` que recibe por parámetro ciertos datos de una nueva película para insertar en la base de datos

## Descripción

Nuestro método `insertExampleFilm` está bien para probar a efectuar una inserción. Algo así como una [_prueba de concepto_ (PoC)](https://www.projectmanager.com/blog/proof-of-concept-definition). Pero, ¿es útil?

No mucho.

## :package: La tarea

**Escribe** nuevo método llamado `RANDOMEXPRESSIONinsertDrama|newDrama|createDrama|addDramaEND`, que sea:

* `public`
* Devolverá un tipo `boolean`
* Recibirá los parámetros:
  * `String title`
  * `int year`
  * `int duration`
  * `String countryCode`
  * `String synopsis`

_(Nótese que **no** recibe el parámetro `genre`.)_

**Se pide** que el método anterior:

* **Cree** un `Statement` con una sentencia `INSERT` para añadir una fila a `TMovies`
  * Los valores a insertar serán aquellos recibidos por parámetro
  * El valor de `genre` **siempre** será `drama`
* **Ejecute** la inserción
  * Si es **exitosa**, _devolverá_ `true` (`return true;`)
  * Si es **fallida**, _devolverá_ `false` (`return false;`)

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
