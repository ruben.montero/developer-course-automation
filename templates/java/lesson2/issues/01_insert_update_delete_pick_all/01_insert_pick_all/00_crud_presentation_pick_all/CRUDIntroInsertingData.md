CRUD

## :books: Resumen

* Entenderemos el concepto de CRUD
* Escribiremos un método en nuestro `MoviesConnector` que **inserta** (crea) una nueva fila en la tabla de la base de datos

## Descripción

Un concepto esencial que aparece en distintas disciplinas de la informática es el de [**CRUD**](https://www.sumologic.com/glossary/crud/). Son siglas en inglés para:

* **C**reate
* **R**ead
* **U**pdate
* **D**elete

...y definen las operaciones básicas de acceso a datos.

En una base de datos SQL, dichas operaciones se corresponden con sentencias:

* `INSERT`
* `SELECT`
* `UPDATE`
* `DELETE`

### SQL `INSERT`

Es la sentencia destinada a grabar un nuevo registro (fila) en una tabla.

Su sintaxis es:

```sql
INSERT INTO Tabla VALUES (valor1, valor2, valor3)
```

Si queremos realizar una inserción indicando **sólo algunos** campos, podemos hacerlo indicándolos con unos paréntesis:

```sql
INSERT INTO Tabla (columna2, columna3) VALUES (valor2, valor3)
```

Esto es útil, por ejemplo, si no queremos especificar un ID porque el _sistema gestor de base de datos_ lo genera de manera automática al ser _autoincremental_.

### Filas afectadas

Cuando invocamos `.executeQuery`, se nos _devuelve_ un objeto tipo `ResultSet` que contiene los resultados de la consulta.

¿Y si queremos hacer un `INSERT`?

En ese caso, el resultado **no** consiste en un conjunto de filas. 

El resultado es un simple y sencillo `int`. Viene a ser **el número de filas afectadas**.

Cuando _insertamos_ datos, siempre será `1`. (**Una** fila afectada, es decir, **insertada**).

Si estamos _eliminando_ (`DELETE`) o _actualizando_ (`UPDATE`) filas, puede haber _más de **una**_ afectada. Lo veremos más adelante.

## `.executeUpdate` en vez de `.executeQuery`

Al lanzar un `SELECT`, invocamos `.executeQuery` y se devuelve un `ResultSet`.

**Para ejecutar un `INSERT`, `UPDATE` o `DELETE`**, debemos invocar **`.executeUpdate`**, que devuelve un tipo `int`:

## :package: La tarea

**Crea** un nuevo método `insertExampleFilm` en `MoviesConnector`.

Su primera misión será crear un objeto `Statement` aprovechando `this.connection`

```java
    public void insertExampleFilm() {
        try {
            Statement statement = this.connection.createStatement();
            // ...
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
```

### Insertar la película _Inside Out_

La película _Inside Out_ fue un éxito de Disney del año 2015.

Vamos a insertarla en la base de datos ejecutando la siguiente sentencia:

```sql
INSERT INTO TMovies (title, year, duration, countryIso3166, genre, synopsis) VALUES ('Inside Out', 2015, 94, 'us', 'animation', 'Riley es una chica que disfruta o padece toda clase de sentimientos. Aunque su vida ha estado marcada por la Alegría, también se ve afectada por otro tipo de emociones. Lo que Riley no entiende muy bien es por qué motivo tiene que existir la Tristeza en su vida. Una serie de acontecimientos hacen que Alegría y Tristeza se mezclen en RANDOMEXPRESSIONuna peligrosa aventura|una trepidante aventura|un peligroso viaje|un trepidante viaje|un sorprendente viaje|una sorprendente aventura|un inesperado viaje|una inesperada aventuraEND que dará un vuelco al mundo de Riley.')
```

¡Adelante! **Añade** el `executeUpdate` necesario. Escribe la consulta **correctamente**:

```java
    public void insertExampleFilm() {
        try {
            Statement statement = this.connection.createStatement();
            int affectedRows = statement.executeUpdate(/* Debe ser INSERT, DELETE o UPDATE. De lo contrario, saltará una excepción */); 
            // ...
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
```

La inserción puede fallar si insertamos una fila que viola una restricción como NOT NULL o UNIQUE. También si la sentencia INSERT está mal formada o contiene tipos de datos incorrectos.

**Controla** esa posibilidad con el siguiente código. También, como se muestra a continuación, **invoca** `.close()` sobre la instancia de `Statement`. Esto liberará recursos[^1].


```java
    public void insertExampleFilm() {
        try {
            Statement statement = this.connection.createStatement();
            int affectedRows = statement.executeUpdate(/* Debe ser INSERT, DELETE o UPDATE. De lo contrario, saltará una excepción */); 
            if (affectedRows == 1) {
                System.out.println("RANDOMEXPRESSIONSe ha insertado satisfactoriamente|Se ha insertado correctamente|Se ha insertado una nueva fila|Se ha insertado una nueva fila correctamente|Se ha insertado una nueva fila satisfactoriamenteEND");
            } else {
                System.out.println("Parece que ha habido un problema");
            }
            statement.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
```


**¡Enhorabuena!** Has lanzado con éxito tu primera inserción a una base de datos empleando un conector Java.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------------------------------------

[^1]: En casos anteriores no lo hemos hecho, puesto que al cerrar la conexión de la base de datos, todos los recursos se liberan. Como ahora hacemos las cosas ligeramente distintas, debemos liberar el `Statement` lo antes posible
