Conectarse y desconectarse una sóla vez

## :books: Resumen

* Crearemos una nueva clase en la que trabajaremos en las siguientes tareas, `MoviesConnector.java`
* Tendrá un atributo privado de tipo `Connection` para almacenar la conexión a la base de datos
* Establecerá dicha conexión a la base de datos en su método constructor
* Tendrá un método `closeConnection` para cerrar la conexión

## Descripción

Nuestro `MoviesDataProvider` tiene diversos métodos que establecen conexiones a la base de datos `movies.db`, lanzan consultas, y cierran la conexión.

Abrir una conexión es una operación relativamente costosa. No queremos abrir una nueva conexión para cada consulta. Lo ideal sería aprovechar la misma conexión para lanzar todas las consultas necesarias.

Recordemos que la conexión a la base de datos se representa mediante un objeto de tipo `Connection`. Podemos _almacenar_ dicho objeto como un atributo de la clase.

## :package: La tarea

**Crea** una **nueva clase** `MoviesConnector.java`, con un atributo privado como el que acabamos de comentar.

```java
import java.sql.Connection;

public class MoviesConnector {
    private Connection connection;
    
    // ...
}
```

Ahora, en el método **constructor**, como ya sabemos hacer, podemos **establecer una conexión**.

```java
import java.sql.*;

public class MoviesConnector {
    private Connection connection;

    public MoviesConnector() {
        String connectionStr = "jdbc:sqlite:db/sqlite3/movies.db";
        try {
            // Esta vez la conexión se guarda
            // como atributo.
            // Cada instancia de MoviesConnector
            // tendrá su conexión. ¡Hurra!
            this.connection = DriverManager.getConnection(connectionStr);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
```

### ¿Y esto para qué?

Recordemos que queremos abrir la conexión **una sóla vez**.

Luego, la aprovecharemos para lanzar múltiples consultas. La idea es que haya métodos que se sirvan de `this.connection` para ello, aunque _no implementaremos ninguno de momento_.

### ¿Y cuándo se cierra la conexión?

¡Vaya!

Es verdad. De momento, no hemos tenido en cuenta que la conexión **debe cerrarse**.

**Añadamos** un nuevo método `closeConnection` que se encargue de ello:

```java
public class MoviesConnector {
    private Connection connection;

    /* ... */
    
    public void closeConnection() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
```

**¡Listo!**

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

