Actualizaciones (II)

## :books: Resumen

* Implementaremos un método en `MoviesConnector` que recibe por parámetro un `id` de película y un nuevo **año**, para actualizarlo en la base de datos

## :package: La tarea

**Se pide** que añadas un nuevo método `public boolean RANDOMEXPRESSIONupdateYearByFilmId|setNewYearByFilmId|changeYearByFilmId|modifyYearByFilmIdEND` que reciba **dos** parámetros:

* `int newYear`
* `int filmId`

Este método:

* Creará un `Statement` y lo ejecutará con `.executeUpdate`
* El SQL servirá para _actualizar_ (`UPDATE`) el `year` de aquella película **cuyo `id` coincida con el parámetro `filmId`**
  * El nuevo valor de _año_ en base de datos será el del parámetro `newYear`
* _Devolverá_ `true` si la ejecución de la consulta altera (`affectedRows`) **una** fila
* _Devolverá_ `false` en cualquier otro caso
  
### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
