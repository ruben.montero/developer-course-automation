Actualizaciones (I)

## :books: Resumen

* Veremos la sintaxis del SQL `UPDATE`
* Implementaremos un método en `MoviesConnector` que recibe por parámetro un `id` de película y una nueva **duración**, para actualizarla en la base de datos

## Descripción

Poniendo en práctica las operaciones **CRUD** hemos llevado a cabo _inserciones_ (**Create**), y, anteriormente, múltiples _consultas_ (**Read**).

Veamos ahora `UPDATE`.

### SQL `UPDATE`

Se corresponde con la sentencia SQL de nombre homónimo, `UPDATE`. Su sintaxis es sencilla:

```sql
UPDATE <tabla> SET campo=<nuevoValor> WHERE <condicion>
```

Esto **modificará** la columna `campo` en **todas** las filas de la `<tabla>` que **cumplan `<condición>`**.

Por ejemplo, para actualizar el _género_ de todas las películas de ciencia-ficción:

```diff
- "science-fiction"
+ "sci-fi"
```

...lo _haríamos_ así:

```sql
UPDATE TMovies SET genre='sci-fi' WHERE genre='science-fiction'
```

Los campos que se actualizan con `SET` son totalmente independientes de la cláusula `WHERE`. Aquí tienes algunos ejemplos más:

```sql
UPDATE TMovies SET genre='fantasy' WHERE title = 'Bedazzled'
UPDATE TMovies SET year = 2014, duration = 90 WHERE id = 8
UPDATE TMovies SET duration = 143 WHERE title = 'The Shawshank Redemption' AND genre = 'drama'
```

Debería entenderse sin mucha complicación el objetivo de estas sentencias. ¿Dirías que es así?

## :package: La tarea

**Se pide** que añadas un nuevo método `public boolean RANDOMEXPRESSIONupdateDurationByFilmId|setNewDurationByFilmId|changeDurationByFilmId|modifyDurationByFilmIdEND` que reciba **dos** parámetros:

* `int newDuration`
* `int filmId`

Este método:

* Creará un `Statement` y lo ejecutará con `.executeUpdate`
* El SQL servirá para _actualizar_ (`UPDATE`) la `duration` de aquella película **cuyo `id` coincida con el parámetro `filmId`**
  * El nuevo valor de _duración_ en base de datos será el del parámetro `newDuration`
* _Devolverá_ `true` si la ejecución de la consulta altera (`affectedRows`) **una** fila
* _Devolverá_ `false` en cualquier otro caso
  
### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
