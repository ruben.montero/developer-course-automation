Adiós con el corazón

## :books: Resumen

* Veremos la sintaxis del SQL `DELETE`
* Implementaremos un método en `MoviesConnector` que recibe por parámetro un `id` de película y ~~LA DESTRUYE~~ la **elimina**

## Descripción

La última parada del recorrido que hemos efectuado sobre las operaciones **CRUD** es el **borrado**.

### SQL `DELETE`

Su sintaxis es muy sencilla. Se asemeja a un `UPDATE` con la diferencia de que _no_ se especifica _qué_ campos se manipulan, porque la idea es que **se eliminen todas las filas coincidentes**:

```sql
DELETE FROM <tabla> WHERE <condicion>
```

Por ejemplo, para eliminar la película con _id_ `14`, lo _haríamos_ así:

```sql
DELETE FROM TMovies WHERE id = 14
```

...o si queremos eliminar _todas_ las películas estadounidenses de la base de datos, lo _haríamos_ así:

```sql
DELETE FROM TMovies WHERE countryIso3166 = 'US'
```

### Una sentencia peligrosa

**¡Ojo!** Esta sentencia es peligrosa.

Si omitimos el `WHERE`, funcionará igualmente...

```sql
DELETE FROM TMovies
```

...y borrará **todas** las filas de la base de datos.

### No confundir con `DROP`

Quizá recuerdes la sentencia SQL [`DROP`](http://sql.11sql.com/sql-drop.htm). Esta sentencia no la pondremos en práctica. Sirve para eliminar una tabla o una base de datos.

Con **DELETE** eliminamos **filas**, pero la tabla _sigue_ existiendo.

## :package: La tarea

**Se pide** que añadas un nuevo método `public boolean RANDOMEXPRESSIONdeleteFilm|removeFilm|wipeFilm|eraseFilmEND` que reciba **un** parámetro:
* `int filmId`

Este método:

* Creará un `Statement` y lo ejecutará con `.executeUpdate`
* El SQL servirá para _eliminar_ (`DELETE`) aquella película de `TMovies` **cuyo `id` coincida con el parámetro `filmId`**
* _Devolverá_ `true` si la ejecución de la consulta altera (`affectedRows`) **una** fila
* _Devolverá_ `false` en cualquier otro caso
  
### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
