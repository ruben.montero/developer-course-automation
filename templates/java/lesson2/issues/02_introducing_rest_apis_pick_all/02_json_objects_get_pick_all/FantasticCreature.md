Animales fantásticos y un endpoint para encontrarlos

## :books: Resumen

* Crearemos un nuevo _endpoint_ `/creature` y las clases necesarias para mostrar la información JSON que se indica

## Descripción

[_Animales fantásticos y dónde encontrarlos_](https://www.filmaffinity.com/es/film215415.html) es una película de 2016 basada en el universo de Harry Potter creado por J.K.Rowling. En ella, una catálogo de criaturas mágicas forman parte esencial de la aventura del mago protagonista, Newt Scamander.

Nosotros vamos a publicar en JSON información sobre _una nueva criatura_ recién inventada.

## :package: La tarea

**Se pide**:

* Que **añadas** una nueva clase que extienda de `ServerResource`
  * En su método anotado con `@Get`, devolverá un `StringRepresentation` que represente el siguiente JSON:
  
```json
{
  "name": "RANDOMEXPRESSIONFuzbick|Rismein|Draco negro de ojos rojos|Draco blanco de ojos azules|Acorin de patas cortas|Acorin de patas largas|Duende rojo de fuegoEND",
  "lifespanYears": RANDOMNUMBER200-500END,
  "RANDOMEXPRESSIONdangerous|isDangerous|canBeDangerousEND": true,
  "magic": {
    "RANDOMEXPRESSIONpower|magicPower|powerAmountEND": 42,
    "ability": "RANDOMEXPRESSIONPuede pulverizar a sus enemigos con una llamarada de fuego|Escupe fuego por la boca que derrite cualquier metal|Se dice que su aliento de fuego alcanza la temperatura del Sol|Dispara bolas de fuego por los ojos, por las manos, por los pies y por la bocaEND"
  }
}
```

_(Añadiremos los POJOs que consideremos oportunos)_

* En `SimpleREST.java`, **añade** la línea `.attach` necesaria para servir este nuevo JSON en el _endpoint_ `/creature`

### No pasa nada si lo ves desordenado

Si al programar tu nuevo _endpoint_ y visitarlo desde el navegador, la información no aparece en el orden que esperas, **no pasa nada**.

Según [el estándar](https://www.ecma-international.org/publications-and-standards/standards/ecma-404/), _no es relevante_ el orden con el que aparecen los `clave-valor` dentro de un JSON.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
