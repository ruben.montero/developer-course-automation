REST APIs

## :books: Resumen

* Introduciremos el concepto de API REST
* Veremos cómo estan formateados las peticiones y respuestas HTTP
* Hablaremos del concepto _endpoint_
* Usaremos la librería _Restlet_ para servir un _endpoint_ REST sencillo, escribiendo las clases `SimpleGetExample` y `SimpleREST`

## Descripción

Acceder directamente a los datos de una base de datos conlleva establecer conexiones como las que hemos empleado en tareas anteriores. 

En el mundo de las aplicaciones distribuidas, es importante conocer **qué** es la **arquitectura cliente-servidor**. Aplicaciones **cliente** (e.g.: navegador web, _app_ móvil,...) se conectan a una máquina **servidor** que atiende **peticiones**.

Por lo general, **no** se quiere exponer una base de datos **directamente al público**. La arquitectura predominante en la actualidad es la de [**API REST**](https://www.ibm.com/es-es/cloud/learn/rest-apis).

### ¿Qué es un API REST?

En esencia, se trata de una _fachada_ empleada para exponer los datos de una base de datos.

<img src="java-rest/docs/distributed-app-architecture.png" width="500">

## Conceptos esenciales

### HTTP

En una API REST se sirven los datos que piden los clientes.

Al igual que cuando se navega a una página web, el protocolo mediante el cual se atienden las peticiones es [HTTP](https://es.wikipedia.org/wiki/Protocolo_de_transferencia_de_hipertexto).

**Podemos entender** que HTTP es una **autovía**. Una **red de carreteras**, digamos.

Se concibió para que **el mundo de la web** funcionase. Es decir, por esas **carreteras** circulan camiones **cargados** de **páginas web** y solicitudes de navegadores.

**Nosotros**, desarrollando un **API REST**, _aprovechamos_ esa infraestructura de carreteras para poner a circular unos _ligeros y rápidos_ **taxis** que se dedicarán a llevar _datos_. Más concretamente, trabajarán con JSON (en lugar de con pesados ficheros HTML y CSS).

### Peticiones HTTP

Sea un camión o un taxi lo que circule, todo se pone en marcha cuando un _cliente_ efectúa una _petición HTTP_.

Las peticiones tienen **3** partes principales:

* **Línea de petición**. En la primera línea se especifica el [verbo](https://www.ramoncarrasco.es/es/content/es/kb/157/resumen-de-verbos-http) (ó método) HTTP y el _recurso_ solicitado. Algo así:

```
GET /publicaciones
```

Los verbos más usados son GET, POST, PUT, DELETE

* **Cabeceras**. Son pares clave-valor que especifican información adicional.

```
User-agent: Mozilla Firefox
Accept: application/json
```

* **Cuerpo**. Viene después de una línea en blanco e indica información adicional que se manda al servidor. Por ejemplo, si estamos usando una petición POST para crear una nueva 'publicación', puede que la información del usuario vaya en formato JSON:

```
{
  'texto_publicacion': '¡Estoy trasteando con APIs REST!'
}
```

### Respuestas

También tienen **3** partes principales:

* **Línea de respuesta**: Contiene un [código de respuesta](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status). ¿Te suena el típico _404 Not Found_? Es un código de respuesta HTTP. Se dividen en 5 categorías: 1xx Información, 2xx Éxito, 3xx Redirección, 4xx Error cliente, 5xx Error servidor.

* **Cabeceras**: Pares clave-valor enviados en la respuesta. Cosas como las famosas _cookies_ viajan en las cabeceras de petición/respuesta

* **Cuerpo**: Donde el API REST responde información relevante. Nos centraremos en el formato JSON

## :package: La tarea

Nosotros queremos **implementar un API REST**. Como la máquina central en la imagen de arriba.

Usaremos la librería [Restlet](https://restlet.talend.com/), un _framework_ de código abierto que emplea clases y anotaciones. Es similar al popular [Spring Boot](https://spring.io/projects/spring-boot), pero mucho más sencillo (`org.restlet.jar` de 699kb vs. ~70Mb de librerías Spring).

Dispone de [documentación](https://restlet.talend.com/documentation/tutorials/2.4) con varios ejemplos, aunque lo que necesitemos para nuestros sencillos casos de uso lo veremos enteramente aquí.

### Un sencillo `GET`

Vamos a implementar un _endpoint_ `/example` en el puerto `81DEVELOPERNUMBER`. Es decir, al final de la tarea, si visitas http://localhost:81DEVELOPERNUMBER/example desde tu navegador web preferido (e.g.: Mozilla, Chrome), verás el resultado.

### Primer paso: Una clase `ServerResource`

Cada _endpoint_ se gestiona con una clase Java.

**Crea** una clase llamada `SimpleGetExample`. El nombre _da igual_. Lo que importa es que **extienda** (_herede_) de `ServerResource`, que es una clase de la librería Restlet.

Esto de la _herencia_ lo hemos mencionado con anterioridad. Basta con aclarar que debemos usar la palabra `extends`, así:

```java
import org.restlet.resource.ServerResource;

public class SimpleGetExample extends ServerResource {

}
```

Ahora, necesitamos **un método**. Debe estar anotado con `@Get` y llamarse `toString()`. _Devolverá_ un `String`.

```java
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class SimpleGetExample extends ServerResource {

    @Get
    public String toString() {
        
    }
}
```

Sirve para que indiquemos _qué_ respuesta dará el servidor a la petición. Devolvamos el siguiente mensaje:

```java
public class SimpleGetExample extends ServerResource {

    @Get
    public String toString() {
        return "RANDOMEXPRESSIONHola, caracola|Hola, mundo|Hola a todos|Mi primer servidor|Estoy usando Restlet|Soy un endpoint|No soy una teteraEND";
    }
}
```

### Segundo paso: Un `Component`

A continuación, **añade** una nueva clase `SimpleREST.java`. _No_ es estrictamente obligatorio (podríamos usar `Main.java` directamente), pero **lo haremos** para encapsular el servidor y escribir buen código.

```java
public class SimpleREST {

}
```

Escribamos un método `runServer`. El objetivo es que **cuando se invoque**, el **servidor REST se ponga en marcha**.

Como primer paso, **instancia** un objeto `Component`[^1] de la librería Restlet. Así:

```java
import org.restlet.Component;

public class SimpleREST {
    private Component component;
    
    public void runServer() {
        this.component = new Component();
        // ...
    }
}
```

Nosotros _serviremos_ nuestro API REST mediante HTTP sin cifrar, y en el puerto arbitrariamente escogido 81DEVELOPERNUMBER. **Así**.

```java
import org.restlet.Component;
import org.restlet.data.Protocol;

public class SimpleREST {
    private Component component;
    
    public void runServer() {
        this.component = new Component();
        this.component.getServers().add(Protocol.HTTP, 81DEVELOPERNUMBER);
        // ...
    }
}
```

A continuación, especificaremos _Quiero que la clase SimpleGetExample se encargue de atender peticiones al 'endpoint' `/example`_

Para ello, recurrimos a una clase propia de la librería, llamada `VirtualHost` y sobre ese _host_, invocamos **`attach`**. Se encarga de hacer justo lo que queremos, _**mapear**_ un _endpoint_ a una **clase**.

**Así**

```java
import org.restlet.Component;
import org.restlet.data.Protocol;
import org.restlet.routing.VirtualHost;

public class SimpleREST {
    private Component component;
    
    public void runServer() {
        this.component = new Component();
        this.component.getServers().add(Protocol.HTTP, 81DEVELOPERNUMBER);
        VirtualHost host = this.component.getDefaultHost();
        host.attach("/example", SimpleGetExample.class);
        // ...
    }
}
```

Luego, **invoca** `.start()`. Controlaremos la posible excepción relanzándola **así**:

```java
public class SimpleREST {
    private Component component;
    
    public void runServer() {
        try {
            this.component = new Component();
            this.component.getServers().add(Protocol.HTTP, 81DEVELOPERNUMBER);
            VirtualHost host = this.component.getDefaultHost();
            host.attach("/example", SimpleGetExample.class);
            this.component.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
```

### Tercer paso: Que se pueda detener

Vamos a permitir que se pueda _detener_ el servidor.

Esto es **estrictamente necesario** para que pasen los _tests_. Simplemente, añadiremos un **nuevo método** a `SimpleREST.java`:

```java
public class SimpleREST {
    private Component component;

    public void runServer() { /* ... */ }
    
    public void stopServer() throws Exception {
        if (this.component != null) {
            this.component.stop();
        }
    }
}
```

### Cuarto paso: Poner en marcha el tinglado

Para probar lo que acabamos de hacer, basta con _instanciar_ nuestra clase desde `Main.java` e _invocar_ el método `runServer`.

```java
SimpleREST myServer = new SimpleREST();
myServer.runServer();
```

Observa que, cuando lo hagas, verás el mensaje:

```
Starting the internal [HTTP/1.1] server on port 81DEVELOPERNUMBER
```

...y la ejecución **se mantiene activa** hasta que pulsas el botón de **Re-run**, de **Stop**, o bien sales de IntelliJ IDEA.

Con tu API REST activa, abre tu navegador web y visita:

* http://localhost:81DEVELOPERNUMBER/example

¿Ves el resultado?

**¡Felicidades!** Has implementado tu primer _endpoint_ REST usando Java.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

------------------------------------------------------------------------

[^1]: En este objeto `Component` podemos indicar el **protocolo** y **puerto** donde se atenderán las peticiones. Por defecto, sueles navegar en Internet en el puerto 443 con protocolo HTTPS. En caso de una conexión _no_ cifrada, será HTTP y puerto 80.
