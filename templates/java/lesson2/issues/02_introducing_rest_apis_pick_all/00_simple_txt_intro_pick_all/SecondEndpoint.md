Un segundo endpoint de ejemplo

## :books: Resumen

* Escribiremos una clase `AnotherGetExample` que devuelva un mensaje distinto
* Lo _mapearemos_ al _endpoint_ `/example2`

## :package: La tarea

**Se pide** que añadas una nueva **clase** `AnotherGetExample`:

* Será análoga a `SimpleGetExample`, pero esta _devolverá_ el mensaje `RANDOMEXPRESSIONEsta es otra prueba|Otro mensaje distinto|Mi segundo endpoint|Este es mi segundo endpoint|Un segundo endpoint para quien quiera verlo|To whom it may concern|El secreto de la vida es...END`

También, en `SimpleREST`, **añadirás** otra línea `host.attach` para que los dos _endpoints_ se sirvan **simultáneamente**.

```diff
    host.attach("/example", SimpleGetExample.class);
+   host.attach("/example2", AnotherGetExample.class);
```

¿Ves el nuevo _endpoint_ desde tu navegador?

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
