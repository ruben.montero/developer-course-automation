Otra compañía lucrativa

## :books: Resumen

* Reutilizaremos `Company` y `GrossProfit` en una nueva clase destinada a un _endpoint_ `/otherCompany`

## :package: La tarea

**Se pide** que **añadas** una nueva clase `RANDOMEXPRESSIONJSONGetOtherCompany|JSONGetCompany2|JSONGetNewCompanyEND` que extienda de `ServerResource`:

* En su método anotado con `@Get`, devolverá un `StringRepresentation` que represente el siguiente JSON:
  
```json
{
  "name": "RANDOMEXPRESSIONRoca Cola|Netflux|HBA|Armazon PrimeEND",
  "owner": "RANDOMEXPRESSIONPepe Depura|John Doe|Alice Doe|Jane DoeEND",
  "grossProfit": {
    "year": RANDOMNUMBER2015-2022END,
    "amount": RANDOMNUMBER5000000-10000000END,
    "currencyCodeIso4217": "RANDOMEXPRESSIONUSD|EUR|AUD|PEN|PLNEND"
  }
}
```

* También se pide que en `SimpleREST.java`, **añadas** la línea `.attach` necesaria para servir este nuevo JSON en el _endpoint_ `/otherCompany`

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
