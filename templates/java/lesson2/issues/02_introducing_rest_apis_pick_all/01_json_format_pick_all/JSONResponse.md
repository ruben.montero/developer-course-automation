Un tercer endpoint... JSON

## :books: Resumen

* Escribiremos una clase `JSONGetExample` que devuelva un mensaje distinto e intentaremos que sea en formato JSON
* Hablaremos de la cabecera `Content-Type: application/json`
* Usaremos `StringRepresentation` para que nuestro servidor envía dicha cabecera
* Serviremos el resultado en `/example3`

## Descripción

Nuestros dos _endpoint_ anteriores entregan contenido en _texto plano_.

Esto está bien, pero podemos hacerlo mejor.

Un buen API REST transferirá los datos usando un formato estándar, como [XML](https://developer.mozilla.org/es/docs/Web/XML/XML_introduction) o [JSON](https://json.org/json-es.html).

## :package: La tarea

Sigue los pasos que ya conoces para **añadir** un tercer _endpoint_.

* **Crea** una nueva clase `JSONGetExample.java` que extienda de `ServerResource` 
* En `SimpleREST.java`, la **_mapearemos_** al _endpoint_ `/example3` (usando `.attach`)

### Retomando `JSONObject`

Dentro de `JSONGetExample.java`, en el método `toString` vamos a **fabricar** un `JSONObject` sencillo como ya sabemos. El objetivo es que sea algo así:

```json
{
  "message": "Esto es un JSON de prueba"
}
```

Así que haremos...

```java
    JSONObject json = new JSONObject();
    json.put("message", "Esto es un JSON de prueba");
```

Y luego **invocamos**`.toString()` para que dicho `JSONObject` sea convertido en un _texto_ transferible. **Así**:

```java
    @Get
    public String toString() {
        JSONObject json = new JSONObject();
        json.put("message", "Esto es un JSON de prueba");
        String response = json.toString();
        return response;
    }
```

Si ahora navegamos a http://localhost:81DEVELOPERNUMBER/example3, veremos la respuesta JSON esperada. Pero esto no funciona suficientemente bien.

El _tipo de contenido_ de una petición/respuesta HTTP se especifica con una cabecera `Content-Type`. Ahora mismo, Restlet está enviando al navegador la cabecera:

```
Content-Type: text/plain
```

Esto es insuficiente porque muchos clientes HTTP fallarán a la hora de interpretar la respuesta correctamente. Debemos encargar a Restlet que responda usando esta cabecera HTTP:

```
Content-Type: application/json
```

### Content-Type: application/json

En `JsonGetExample` **añade** un método que devuelva `StringRepresentation` (es una clase propia de Restlet).

El nombre del método, ahora da igual siempre y cuando esté anotado con `@Get`.

En dicho método vamos a añadir el código que _genera_ un JSON, como antes:

```java
import org.json.JSONObject;
import org.restlet.data.MediaType;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class JSONGetExample extends ServerResource {

    // Método toString viejo
    @Get
    public String toString() { /* ... */ }
    
    // Nuevo método
    @Get
    public StringRepresentation getEndpointResponse() {
        JSONObject json = new JSONObject();
        json.put("message", "Esto es un JSON de prueba");
        String jsonString = json.toString();
        // ...
        
    }
```

Pero, esta vez, vamos a **instanciar** un objeto `StringRepresentation` pasando nuestro `String` como parámetro al construirlo.

Así:

```java
    // Nuevo método
    @Get
    public StringRepresentation getEndpointResponse() {
        JSONObject json = new JSONObject();
        json.put("message", "Esto es un JSON de prueba");
        String jsonString = json.toString();
        StringRepresentation representation = new StringRepresentation(jsonString);
        // ...
        
    }
```

De esta forma, sobre el `StringRepresentation` podemos invocar `.setMediaType` pasando la _constante_ `MediaType.APPLICATION_JSON`, **así**:

```java
    // Nuevo método
    @Get
    public StringRepresentation getEndpointResponse() {
        JSONObject json = new JSONObject();
        json.put("message", "Esto es un JSON de prueba");
        String jsonString = json.toString();
        StringRepresentation representation = new StringRepresentation(jsonString);
        representation.setMediaType(MediaType.APPLICATION_JSON);
        // ...
        
    }
```

Esta es la forma en la que, usando la librería Restlet, indicamos que la respuesta debe tener la cabecera HTTP `Content-Type: application/json`.

### ¡Listo!

Ya sólo falta _devolver_ la instancia que hemos construido. Restlet entenderá perfectamente que se trata de un JSON.

Podemos borrar el `toString` anterior. Se trata de un método sencillo para devolver respuestas en _texto plano_, pero que **no** usaremos de ahora en adelante.

```diff
public class JSONGetExample extends ServerResource {
-
-    @Get
-    public String toString() {
-        JSONObject json = new JSONObject();
-        json.put("message", "Esto es un JSON de prueba");
-        String jsonString = json.toString();
-        return jsonString;
-    }

    @Get
    public StringRepresentation getEndpointResponse() {
        JSONObject json = new JSONObject();
        json.put("message", "Esto es un JSON de prueba");
        String jsonString = json.toString();
        StringRepresentation representation = new StringRepresentation(jsonString);
        representation.setMediaType(MediaType.APPLICATION_JSON);
+        return representation;
    }
}
```

Si ahora visitamos http://localhost:81DEVELOPERNUMBER/example3 desde el navegador, veremos como _se muestra de forma diferente_.

El _contenido_ de la respuesta es el mismo, pero el navegador (cada uno lo hace a su manera) decide _presentarlo_ de una forma más entendible.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
