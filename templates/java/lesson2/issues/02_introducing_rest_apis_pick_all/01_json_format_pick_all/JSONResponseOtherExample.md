Simpsons endpoint

## :books: Resumen

* Comprenderemos que nuestro servidor vale para cualquier cliente
* Añadiremos un nuevo _endpoint_ `/example4` muy similar al anterior

## :package: La tarea

**Se pide**:

* Añade una nueva clase `JSONGetAnotherExample` que extienda de `ServerResource`
* Añade el código que creas conveniente para que en el _endpoint_ `/example4` se sirva adecuadamente el siguiente JSON:

```json
{
  "characterPopularity": RANDOMNUMBER1-50END,
  "characterDescription": "RANDOMEXPRESSIONHomer J. Simpson es un hombre de muchas agallas, pero con un punto flaco: Las rosquillas|Marge Simpson es la eminente e incansable madre de la familia Simpson|Bart Simpson es el rebelde incansable e incapaz de respetar a la autoridad|Lisa Simpson es la saxofonista que adora el Jazz y a los animales|Maggie Simpson es el angelito de la familia Simpson. Por primera vez, su voz fue escuchada en el primer corto emitido 'Good Night'END"
}
```

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.

