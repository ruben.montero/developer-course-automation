Una compañía lucrativa

## :books: Resumen

* Crearemos dos POJOs que tendrán un método destinado a _producir una representación_ en JSON
* Los usaremos en un nuevo _endpoint_

## Descripción

Para producir una respuesta JSON desde nuestro servidor, hemos escrito código parecido a este en nuestras últimas tareas:

```java
    @Get
    public StringRepresentation getEndpointResponse() {
        JSONObject json = new JSONObject();
        // Aquí moldeo el objeto json a mi gusto
        // Añado claves-valor, etc.
        json.put("clave1", "valor 1 tipo String");
        json.put("clave2", 55) // numérico;
        String jsonString = json.toString();
        // Convertimos nuestro String a StringRepresentation
        StringRepresentation representation = new StringRepresentation(jsonString);
        representation.setMediaType(MediaType.APPLICATION_JSON);
        return representation;
    }
```

Vamos a llevar a cabo una **mejora**.

Usaremos un _Plain Old Java Object_ (**POJO**) para _encapsular_ en una clase separada la lógica destinada a _producir un `String` resultado_.

En la práctica, la idea es llegar a algo como esto:

```java
    @Get
    public StringRepresentation getEndpointResponse() {
        POJO pojo = new POJO("valor 1 tipo String", 55);
        String jsonString = pojo.toJSONObject().toString();
        // Convertimos nuestro String a StringRepresentation
        StringRepresentation representation = new StringRepresentation(jsonString);
        representation.setMediaType(MediaType.APPLICATION_JSON);
        return representation;
    }
```

### ¿Por qué es una mejora?

Generar una respuesta de manera estática con `JSONObject` es poco práctico en la realidad.

Usar un **POJO** es el primer paso para conseguir que nuestra respuesta sea generada de manera _dinámica_. Profundizaremos en esto más adelante.

## El objetivo de la tarea

Vamos a conseguir que en el _endpoint_ `http://localhost:81DEVELOPERNUMBER/company` se sirva un JSON como el siguiente:

```json
{
  "name": "Amazon",
  "owner": "Jeff Bezos",
  "grossProfit": {
    "year": 2021,
    "amount": 54577000000,
    "currencyCodeIso4217": "USD"
  }
}
```

De momento los valores `Amazon`, `Jeff Bezos` serán _hardcodeados_.

Pero en un futuro próximo entenderemos la **utilidad** de emplear POJOs para encapsular los datos que se sirven.

### ¿Cuántos POJOs?

Usaremos **dos**. 

Uno estará **dentro del otro**. El primero acumulará los valores del JSON raíz, y el segundo está destinado a englobar los valores del _objeto_ dentro de `"grossProfit"`.

## :package: La tarea

**Añade** una nueva clase `GrossProfit.java` que tenga **3** atributos privados y un constructor así:

```java
public class GrossProfit {
    private int year;
    private long amount;
    private String currencyCode;

    public GrossProfit(int year, long amount, String currencyCode) {
        this.year = year;
        this.amount = amount;
        this.currencyCode = currencyCode;
    }
}
```

...y tendrá también un método que **produzca un `JSONObject`**:

```java
    public JSONObject toJSONObject() {
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("year", this.year);
        jsonResult.put("amount", this.amount);
        jsonResult.put("currencyCodeIso4217", this.currencyCode);
        return jsonResult;
    }
```

Encajaremos esta pieza con el resto más adelante.

### Segundo **POJO**

Representará el JSON final.

**Añade** una nueva clase `Company` con los atributos mencionados y un constructor:

```java
public class Company {
    private String name;
    private String owner;
    private GrossProfit grossProfit;

    public Company(String name, String owner, GrossProfit grossProfit) {
        this.name = name;
        this.owner = owner;
        this.grossProfit = grossProfit;
    }
}
```

### `toJSONObject` aquí también

Nuestro `Company` debe poder convertirse fácilmente a un `JSONObject`.

**Añadamos** un **nuevo método**:

```java
    public JSONObject toJSONObject() {
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("name", this.name);
        jsonResult.put("owner", this.owner);
        jsonResult.put("grossProfit", this.grossProfit.toJSONObject()); // Observa cómo invocamos toJSONObject del atributo tipo GrossProfit
        return jsonResult;
    }
```

## Un nuevo _endpoint_

**Añadamos** una nueva **clase** `JSONGetCompany`, análoga a la de la tarea anterior.

Vamos a usar las clases que acabamos de crear. Aunque ahora estos valores están _hardcodeados_, podrían provenir de otra fuente de datos :wink: 

```java
public class JSONGetCompany extends ServerResource {

    @Get
    public StringRepresentation getEndpointResponse() {
        GrossProfit profit = new GrossProfit(
                2021,
                54577000000L, // En Java, los long terminan en "L"
                "USD"
        );
        Company company = new Company(
                "Amazon",
                "Jeff Bezos",
                profit // ¡Aquí pasamos una variable tipo GrossProfit!
        );
        String jsonString = company.toJSONObject().toString();
        StringRepresentation representation = new StringRepresentation(jsonString);
        representation.setMediaType(MediaType.APPLICATION_JSON);
        return representation;
    }
}
```

### El _endpoint_... ¡y listo!

Basta con añadir esta línea a `SimpleREST.java`:

```java
        host.attach("/company", JSONGetCompany.class);
```

Visitamos http://localhost:81DEVELOPERNUMBER/company y vemos nuestro resultado.

### :play_pause: Lanza el _test_

**Ejecuta** el _test_ `TESTFILENAME.java` y comprueba que es exitoso. Si hay fallos, repasa tu código y la tarea para arreglarlos.

### :trophy: Por último

Haz `commit` y `push` para subir los cambios al repositorio.
