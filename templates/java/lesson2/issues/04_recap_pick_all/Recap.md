¡Trabajo terminado!

## :trophy: ¡Felicidades!

Has realizado las tareas del _sprint_ y has validado tu trabajo con los _tests_. ¡Enhorabuena!

<img src="java-rest/docs/sunset.jpg" width=500 />

Has...

* Aprendido a abrir una conexión a una base de datos desde Java con `Connection` y `DriverManager`
* Comprendido que se pasa un `String` para especificar cómo es la conexión. Por ejemplo, para conectarse a un fichero SQLite, 
`"jdbc:sqlite:unaCarpeta/otraCarpeta/miBaseDeDatos.db"`
* Entendido que en función de a _qué_ tipo de base de datos nos conectamos, necesitamos tener la librería adecuada incluida en el proyecto
* Entendido la necesidad de cerrar las conexiones a base de datos con `.close()`
* Usado `Statement` y `.executeQuery` para lanzar un `SELECT` SQL
* Procesado los resultados de un `ResultSet` empleando un bucle `while` con `resultSet.next()` en la condición
* Extraído los resultados de un `ResultSet`
* Usado `.executeUpdate` (en contraposición a `.executeQuery`), que devuelve el número de filas afectadas por un `INSERT`, `UPDATE` o `DELETE`, principalmente
* Creado un servidor REST sencillo empleando Restlet
* Creado un _endpoint_ sencillo, que lo controla una clase Java que hereda de `ServerResource`
* _Mapeado_ clases a _rutas_ (_endpoints_) mediante el `VirtualHost`
* Empleado `StringRepresentation` de Restlet para devolver contenido con la cabecera HTTP `Content-Type: application/json`
* Escrito POJOs para encapsular los datos de una respuesta JSON
* Usado un conector a una base de datos para sacar los datos que viajan en la respuesta del servidor REST de películas
* Usado un _path param_ para devolver los datos de una película por ID en específico
* Controlado el caso de que una película no exista con un ID recibido, y devuelto un `404`
* Usado un _query param_ para devolver datos ordenados por un criterio específico
