Opcional: Tutorial Dodge the Creeps

## :books: Resumen

* Seguiremos el tutorial _Dodge the Creeps!_ para crear un sencillo juego 2D

## Descripción

En esta tarea _opcional_ se plantea un objetivo que te ayudará a familiarizarte con Godot y a tener más confianza para trabajar en _tu propio juego_.

¡Ojo! Esta tarea no es evaluable. Sólo _tu juego_ contará para la nota del _sprint_.

## :package: La tarea

![](godot/images/dodge_preview.gif)

**Sigue** los pasos del [**tutorial de _Dodge the Creeps_!**](https://docs.godotengine.org/es/stable/getting_started/first_2d_game/index.html)

Se aconseja _crear_ tu proyecto dentro de la carpeta `dodge-the-creeps/` del repositorio que verás tras hacer `git pull`. Así, puedes guardar (_commit_) tu trabajo en tu repositorio.
