Opcional: Enviar petición POST en Godot

## :books: Resumen

* Ejecutaremos el _backend_ disponible en `django-backend/` en nuestro ordenador local
* Modificaremos _Dodge the Creeps!_ para que mande un POST cuando se completa cada partida

## Descripción

En esta tarea _opcional_ se plantea un objetivo que te ayudará a familiarizarte con Godot y a tener más confianza para trabajar en _tu propio juego_.

¡Ojo! Esta tarea no es evaluable. Sólo _tu juego_ contará para la nota del _sprint_.

### Completando la tarea anterior

Al principio de la tarea anterior indicamos dos objetivos:

1. Leer un archivo JSON de disco con el _username_
2. Mandar petición POST para publicar la puntuación de la partida

¡Vamos a por el segundo!

## :package: La tarea

En primer lugar, desde un _terminal_ (cmd.exe) **cambia** el directorio activo (`cd`) hasta la carpeta `django-backend/scoreboard/` de nuestro repositorio local. 

Luego, **lanza** el API REST con:

```
python manage.py runserver
```

### ¿Qué es este API REST?

Un _sencillísimo_ API REST con un único _endpoint_ `/score`.

* Si mandas un `GET`, verás la lista de puntuaciones publicadas. Puedes visitarlo desde el navegador: http://localhost:8000/score
* Si mandas un `POST` debidamente formado, publicarás una nueva puntuación

### ¿Cómo es el POST?

Como este ejemplo:

```
POST /score
Content-Type: application/json
Client-Secret: abc

{
  "username": "TestUserDEVELOPERNUMBER",
  "score": 39
}
```

### ¿Y cómo lo lanzamos?

¡Muy fácil! **Abre** Godot con tu proyecto _Dodge the Creeps!_. En el _script_ principal `Main.gd` tenemos una función llamada `game_over()`. **Invoca** un nuevo método ahí:

```diff
func game_over():
    $ScoreTimer.stop()
    $MobTimer.stop()
    $HUD.show_game_over()
    $Music.stop()
    $DeathSound.play()
+   send_post_new_score()
```

...y luego, **añade** dicho método. El primer paso será ejecutar una _condición_ de guarda, por si hubo algún problema cargando el `username`. **Así**:

```python
func send_post_new_score():
	if username == null:
		printerr("Will NOT send POST data with score due to invalid username")
		printerr("There might have been an error loading user_data file")
		return
```

### [HTTPRequest](https://docs.godotengine.org/en/stable/classes/class_httprequest.html)

Es el tipo de _nodo_ que se usa para mandar peticiones HTTP. Puede añadirse a la escena desde el editor. Nosotros lo haremos _programáticamente_, dentro del _script_. Para ello, **añade** este código:

```python
func send_post_new_score():
	if username == null:
		printerr("Will NOT send POST data with score due to invalid username")
		printerr("There might have been an error loading user_data file")
		return
	
	# Username OK. Let's send the request
	var http_request = HTTPRequest.new()
	add_child(http_request)
```

Y, a continuación, **envía** una petición con cuerpo JSON y cabeceras adecuadas, **así**:

```python
func send_post_new_score():
	if username == null:
		printerr("Will NOT send POST data with score due to invalid username")
		printerr("There might have been an error loading user_data file")
		return
	
	# Username OK. Let's send the request
	var http_request = HTTPRequest.new()
	add_child(http_request)
	var body = JSON.stringify({"username": username, "score": score})
	var headers = ["Content-Type: application/json", "Client-Secret: abc"] # CLIENT_SECRET should never be public! If leaked, ALL clients should be force-updated to use a new one
	http_request.request("http://127.0.0.1:8000/score", headers, HTTPClient.METHOD_POST, body)
``` 

### Parámetros de `.request`

1. El _host_ y su _endpoint_. En nuestro caso, `http://127.0.0.1:8000/score`
2. Las cabeceras, en formato _lista_ de _Strings_
3. Método HTTP
4. Cuerpo de la petición

### ¿Y ya está?

¡La petición ya debe enviarse! Ahora, para _escuchar_ la respuesta del servidor, **añade** el siguiente código que conecta la señal `"request_completed"`:

```diff
func send_post_new_score():
	if username == null:
		printerr("Will NOT send POST data with score due to invalid username")
		printerr("There might have been an error loading user_data file")
		return
	
	# Username OK. Let's send the request
	var http_request = HTTPRequest.new()
	add_child(http_request)
+	http_request.connect("request_completed", _on_server_has_responded)
	var body = to_json({"username": username, "score": score})
	var headers = ["Content-Type: application/json", "Client-Secret: abc"] # CLIENT_SECRET should never be public! If leaked, ALL clients should be force-updated to use a new one
	http_request.request("http://127.0.0.1:8000/score", headers, false, HTTPClient.METHOD_POST, body)
+
+func _on_server_has_responded(result, response_code, headers, body):
+	var response = JSON.parse_string(body.get_string_from_utf8())
+	print("Server response:")
+	print(response)
```

Esto es especialmente útil si queremos lanzar un `GET` para obtener información de un _endpoint_.

Nosotros, en este breve ejemplo, sólo lanzaremos un `POST` que publica la puntuación.

¡Adelante! Con el servidor REST en ejecución, **juega** una partida. Al terminar, deberías ver lo siguiente en la consola de Godot:

```
Godot Engine v3.5.stable.official.991bb6ac7 - https://godotengine.org
OpenGL ES 3.0 Renderer: Intel(R) UHD Graphics
Async. shader compilation: OFF
 
username loaded correctly
Server response:
{created:True}
```

Si es así, **¡enhorabuena!** Has creado tu primer juego en Godot que se comunica con un API REST.
