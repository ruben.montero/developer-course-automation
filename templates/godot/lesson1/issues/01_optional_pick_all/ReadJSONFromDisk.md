Opcional: Leer archivo de disco en Godot

## :books: Resumen

* Modificaremos _Dodge the Creeps!_ para que lea un sencillo archivo de disco
* Asignaremos a una variable `username` el dato leído

## Descripción

En esta tarea _opcional_ se plantea un objetivo que te ayudará a familiarizarte con Godot y a tener más confianza para trabajar en _tu propio juego_.

¡Ojo! Esta tarea no es evaluable. Sólo _tu juego_ contará para la nota del _sprint_.

### ¿Y qué haremos?

Tras [completar _Dodge the Creeps!_](https://docs.godotengine.org/es/stable/getting_started/first_2d_game/index.html) tenemos un sencillo juego 2D que nos ha guiado en nuestro aprendizaje sobre muchos fundamentos de Godot:

![](godot/images/dodge_preview.gif)

Completaremos este juego para que publique la puntuación del jugador en un API REST, mediante una [petición `POST`](https://docs.godotengine.org/en/stable/classes/class_httprequest.html).

### ¿Qué jugador?

¡Buena pregunta!

Ahora mismo, no sabemos cómo se llama el jugador.

Por lo tanto, vamos a dividir nuestra tarea en dos:

1. Leer un archivo JSON de disco con el _username_
2. Mandar petición POST para publicar la puntuación de la partida

En esta tarea, sólo abordaremos la primera parte.

## :package: La tarea

Para no complicarnos _pidiendo al usuario su nombre_, _almacenándolo_, etc. Vamos a asumir que un archivo de configuración en la ruta `save/user_data.json` guarda este dato.

Adelante, **guarda** un **nuevo archivo** con un sencillo JSON con la clave `username` en `save/user_data.json`, dentro de la raíz de tu proyecto _Dodge the Creeps!_.

Puedes usar tu editor de texto favorito:

```json
{
  "username": "TestUserDEVELOPERNUMBER"
}
```

_(Pon el nombre de usuario que quieras. La idea es que lo podamos modificar desde aquí libremente)_

```diff
  programacion-multimedia-y-dispositivos-moviles/
   |
   |- godot/
       |
       |-- django-backend/
       |
       |-- dodge-the-creeps/
       |    |
       |    |- .import/
       |    |- art/
       |    |- fonts/
       |    |- save/
       |    |   |
       |    |   |- .gitkeep
+      |    |   |- user_data.json
       |    |
       |    |- screenshots/
       |    |- HUD.gd
       |    |- HUD.tscn
       |    |- Main.gd
       |    |- Main.tscn
       |    |- Mob.gd
       |    |- Mob.tscn
       |    |- Player.gd
       |    |- Player.tscn
       |    |- project.godot
       |
       |-- images/
```

Luego, **abre** Godot y desde el _script_ del nodo principal (`Main.gd`) **añade** una variable global `username`:

```diff
 extends Node

 export(PackedScene) var mob_scene
 var score
+var username
```

A continuación, en la función `_ready()` (_¿te recuerda a `onCreate` :smile:?_) **invoca** una nueva función:

```diff
func _ready():
    randomize()
+   load_username_from_disk()
```

...y **crea** dicha función más abajo. **Así**:

```python
func load_username_from_disk():
	var FILE_NAME = "save/user_data.json"
	if FileAccess.file_exists(FILE_NAME):
		var dataFile = FileAccess.open(FILE_NAME, FileAccess.READ)
		var data = JSON.parse_string(dataFile.get_as_text())
		dataFile.close()
		if typeof(data) == TYPE_DICTIONARY:
			print("username loaded correctly")
			username = data.username
		else:
			printerr("Corrupted data!")
	else:
		printerr("No saved data!")
```

_(Puedes buscar tutoriales para tu versión de Godot que muestren cómo se podría guardar, en vez de leer, dicho archivo)._

Tras estos pasos, nuestra variable `username` ya contendrá el valor dentro de `user_data.json` en tiempo de ejecución. Deberías ver el _print_ por consola al ejecutar el juego:

```
Godot Engine v3.5.stable.official.991bb6ac7 - https://godotengine.org
OpenGL ES 3.0 Renderer: Intel(R) UHD Graphics
Async. shader compilation: OFF
 
username loaded correctly
```
