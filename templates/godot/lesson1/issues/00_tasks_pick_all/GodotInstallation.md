Instalar Godot

## :books: Resumen

* Hablaremos del motor de videojuegos Godot
* Lo instalaremos

## Descripción

[Godot](https://godotengine.org/) es un motor de videojuegos multiplataforma _open source_ 2D y 3D.

### ¿Motor de videojuegos?

Un motor de videojuegos ó _game engine_ es un programa que pone en nuestra mesa de trabajo las herramientas necesarias para crear videojuegos.

Por ejemplo: Si creas un _juego_ en código Python que lea la entrada por consola y otorgue al usuario una puntuación dependiendo de si responde correctamente a preguntas (_a), b), c)_), seguramente no necesites un _game engine_.

Pero, ¿y si quieres crear algo más complejo? _Podríamos_ programar todo _desde cero_... Pero, ¿para qué reinventar la rueda?

Técnicas de _rendering_, reproducción de sonido, uso de _assets_ gráficos,... Un motor de videojuegos pone a nuestra disposición todas esas herramientas y más. Así, pueden ser creados juegos _más complejos_ de forma _más sencilla_.

### _Game engines_ populares

Hoy en día, [Unity](https://unity.com/es) y [Unreal Engine](https://www.unrealengine.com/) son dos de los motores de videojuegos más populares, pero no son realmente libres.

* Con Unity, existe una licencia personal limitada y [el resto de licencias](https://store.unity.com/compare-plans) son de pago, llegando hasta a 4000$/mes para compañías grandes.
* Con Unreal, aunque puedes descargar y comenzar a usar gratuitamente el motor, deberás [pagar el 5% de tus ingresos](https://www.unrealengine.com/en-US/faq) si tu proyecto supera una cierta cantidad de beneficios. También hay otras opciones de licencia.

## [Godot](https://godotengine.org/)

### ¿Por qué?

Es un motor _open source_ (de código abierto), completamente gratuito. Puedes generar beneficios sin deber nada a nadie, e incluso puedes _modificar_ o _contribuir_ al propio proyecto Godot.

Ello no lo hace menos potente. Está pensado para creación de juegos 2D y 3D y los juegos pueden desarrollarse empleando [GDScript](https://gdscript.com/) (un lenguaje propio similar a Python) ó [C#](https://docs.microsoft.com/en-us/dotnet/csharp/).

Es _multiplataforma_, por lo que podrás crear tus videojuegos desde Windows, Linux o macOS. Además, los videojuegos creados [**podrán funcionar** en casi cualquier **plataforma**](https://godotengine.org/features#deploy): Windows, macOS, Linux, UWP, BSD, iOS, Android, Nintendo Switch, PlayStation 4, Xbox One, web (HTML5).

Además, dispone de una [documentación muy completa](https://docs.godotengine.org/en/stable/) y una [comunidad activa](https://godotengine.org/community). 

Existen muchos ejemplos de videojuegos hechos con Godot, como [Tanks of Freedom](https://github.com/w84death/Tanks-of-Freedom/)

<img src="godot/images/tanks_of_freedom1.png" width=400>

<img src="godot/images/tanks_of_freedom2.jpg" width=400>

...o muchos más que puedes consultar en el [_showcase_ de Godot](https://godotengine.org/showcase)

## :package: La tarea

**Descarga** Godot y pruébalo.

Si has conseguido abrir Godot, ¡listo!

No es necesario hacer un _commit_ para finalizar esta tarea. Puedes pasar a la siguiente.
