/// <reference types="cypress" />

describe('a_simple_button_that_does_something ', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/', {
        onBeforeLoad (win) {
          cy.stub(win.console, 'log').as("consoleLog")
        },
      })
    })
  
    it('issueISSUENUMBER button clicked produces expected console log output', () => {
      cy.get('button[data-cy=simplebutton]').click()
      cy.get('@consoleLog').should('be.calledWith', "RANDOMEXPRESSIONClick|Hiciste click|You clicked|Has clicado|Se ha clicadoEND");
    })
})
