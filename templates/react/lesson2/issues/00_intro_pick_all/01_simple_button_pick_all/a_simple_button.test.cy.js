/// <reference types="cypress" />

describe('a_simple_button', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER title exists', () => {
      cy.get('h2[data-cy=issueISSUENUMBERtitle]')
      .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER button exists and has correct text', () => {
        cy.get('button[data-cy=simplebutton]')
        .should('have.text', 'RANDOMEXPRESSIONHaz click|Click|Hacer click|Activar|ClicarEND');
    })
})
