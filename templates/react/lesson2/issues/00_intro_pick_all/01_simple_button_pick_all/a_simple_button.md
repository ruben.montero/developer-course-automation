Click, click, click

## :books: Resumen

* Crearemos un componente sencillo que consista en un botón
* Cuando se clique, imprimirá un _log_ por consola
* Modificaremos `MainPage.js` para que incluya ese componente

## Descripción

Hasta ahora no hemos trabajado con la interacción de usuario desde React.

¡Ha llegado la hora de desbloquear el infinito potencial de nuestros usuarios!

<img src="react-states/docs/unlimited_power.jpg" width=150 />

## :package: La tarea

**Crea** una carpeta `buttons` dentro de `src/components/`.

Dentro de `src/components/buttons/`, **crea** un nuevo fichero `SimpleButton.js`:

```jsx
const SimpleButton = (props) => {

    return <button data-cy="simplebutton">RANDOMEXPRESSIONHaz click|Click|Hacer click|Activar|ClicarEND</button>
}

export default SimpleButton;
```

### Incluyéndolo en la página

En `MainPage.js`, **añade** un `<h2>` y el componente `<SimpleButton />` como se indica a continuación:

```diff
import WelcomeAlert from "../components/WelcomeAlert";
+import SimpleButton from "../components/buttons/SimpleButton";

const MainPage = (props) => {
    return <div>
        <h2>Ejercicio 1</h2>
        <WelcomeAlert />
+       <h2 data-cy="issueISSUENUMBERtitle">Ejercicio ISSUENUMBER</h2>
+       <SimpleButton />
    </div>
}

export default MainPage;
```

Aparece en http://localhost:3000, ¿correcto?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
