Refactorizando el botón

## :books: Resumen

* Modificaremos, otra vez, `SimpleButton.js`

## Descripción

¿Sabías que no hace falta declarar la función asociada a `onClick` por separado?

Tenemos permitido... ¡hacerlo todo de golpe!

## :package: La tarea

**Reescribe** la función `buttonWasClicked` de `SimpleButton.js` para que sea una _arrow function_ anónima directamente indicada en `onClick`, de la siguiente manera:

```jsx
const SimpleButton = (props) => {

    return <button onClick={ (event) => { 
          console.log("RANDOMEXPRESSIONClick|Hiciste click|You clicked|Has clicado|Se ha clicadoEND");
          console.log("RANDOMEXPRESSIONEvento:|Información del click:|Info del click:|Evento click:|Objeto evento:END");
          console.log(event);
          console.log("RANDOMEXPRESSIONFunción refactorizada|Refactorizado|Refactor del botón|SimpleButton refactor|He refactorizado estoEND");
        } 
      } data-cy="simplebutton">RANDOMEXPRESSIONHaz click|Click|Hacer click|Activar|ClicarEND</button>
}
```

También, **añade** el `console.log("RANDOMEXPRESSIONFunción refactorizada|Refactorizado|Refactor del botón|SimpleButton refactor|He refactorizado estoEND");` como se indica arriba.

¿Qué te parece?

Esta manera es especialmente útil cuando la función es muy compacta y se puede poner en una sola línea.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
