/// <reference types="cypress" />

describe('event_in_on_click ', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/', {
        onBeforeLoad (win) {
          cy.stub(win.console, 'log').as("consoleLog")
        },
      })
    })
  
    it('issueISSUENUMBER button clicked produces expected console log output', () => {
      cy.get('button[data-cy=simplebutton]').click()
      cy.get('@consoleLog').should('be.calledWith', "RANDOMEXPRESSIONEvento:|Información del click:|Info del click:|Evento click:|Objeto evento:END");
    })
})
