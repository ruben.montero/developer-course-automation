Click, click, click... ¿y entonces?

## :books: Resumen

* Modificaremos `SimpleButton.js` para que al clicar haga algo

## Descripción

Trabajemos con nuestro botón.

## :package: La tarea

**Modifica** `SimpleButton.js`, añadiendo una función que se ejecutará cuando cliquemos en el botón. Deberás _engancharla_ al botón mediante el atributo `onClick`:

```jsx
const SimpleButton = (props) => {

    const buttonWasClicked = () => {
        console.log("RANDOMEXPRESSIONClick|Hiciste click|You clicked|Has clicado|Se ha clicadoEND");
    }

    return <button onClick={buttonWasClicked} data-cy="simplebutton">RANDOMEXPRESSIONHaz click|Click|Hacer click|Activar|ClicarEND</button>
}

export default SimpleButton;
```

Ahora, ¡tu botón hace algo!

El código en `buttonWasClicked` se ejecuta cuando lo clicas.

Puedes comprobarlo haciendo click derecho > Inspeccionar > Consola, y ver que ahí aparece el mensaje `"RANDOMEXPRESSIONClick|Hiciste click|You clicked|Has clicado|Se ha clicadoEND"` cada vez que haces click.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
