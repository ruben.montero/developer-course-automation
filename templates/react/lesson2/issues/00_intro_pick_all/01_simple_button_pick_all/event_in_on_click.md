Evento de click

## :books: Resumen

* Modificaremos la función `buttonWasClicked` en `SimpleButton.js`

## Descripción

Veremos que, al hacer click, el navegador inyecta un objeto de tipo [evento](https://developer.mozilla.org/en-US/docs/Web/API/PointerEvent) en la función que es invocada.

## :package: La tarea

**Declara** un nuevo parámetro `event` en la función `buttonWasClicked` de `SimpleButton.js`:

```diff
const SimpleButton = (props) => {
-
-   const buttonWasClicked = () => {
+   const buttonWasClicked = (event) => {
        console.log("RANDOMEXPRESSIONClick|Hiciste click|You clicked|Has clicado|Se ha clicadoEND");
    }

    return <button onClick={buttonWasClicked} data-cy="simplebutton">RANDOMEXPRESSIONHaz click|Click|Hacer click|Activar|ClicarEND</button>
}

export default SimpleButton;
```

Acabas de declarar un parámetro `event`[^1]. Ahora, el navegador puede _inyectar_ información sobre el evento (el click) cada vez que invoca `buttonWasClicked`.

**Añade** estos dos `console.log` **dentro** de la función:

```diff
    const buttonWasClicked = (event) => {
        console.log("RANDOMEXPRESSIONClick|Hiciste click|You clicked|Has clicado|Se ha clicadoEND");
+       console.log("RANDOMEXPRESSIONEvento:|Información del click:|Info del click:|Evento click:|Objeto evento:END");
+       console.log(event);
    }
```

¿Qué ves en la consola cuando clicas el botón?

¡Ese complejo objeto tiene mucha información!

Aunque, por el momento, no la usaremos para nada :smile:

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
