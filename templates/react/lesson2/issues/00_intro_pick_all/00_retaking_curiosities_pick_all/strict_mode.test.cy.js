/// <reference types="cypress" />

describe('strict_mode', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER welcome alert has been correctly modified', () => {
        cy.get('h1[data-cy=issue1Title]')
        .should('have.text', 'RANDOMEXPRESSIONHola, developer|¡Hola, developer!|Hola :D|Arrancando motores|Arrancando motores, developer...|Encendiendo, developer|Calentando motores, developer...END');
    })
})
