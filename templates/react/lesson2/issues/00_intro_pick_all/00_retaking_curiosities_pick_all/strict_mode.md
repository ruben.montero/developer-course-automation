Retomando la curiosidad

## :books: Resumen

* Actualizaremos nuestro repositorio con `git pull` y comenzaremos a trabajar en la nueva carpeta `react-states`
* Modificaremos el componente `WelcomeAlert`
* Entenderemos por qué la alerta se muestra dos veces

## Descripción

En primer lugar, efectúa un `git pull` desde tu repositorio para bajar los archivos iniciales de este Sprint.

Después de esto, verás que ha aparecido la carpeta `react-states`. **Ábrela** desde tu [IDE](https://www.redhat.com/es/topics/middleware/what-is-ide) favorito.

Como ya sabes, para arrancar la maquinaria, tenemos que ejecutar los siguientes dos comandos desde un terminal de Windows (cmd.exe):

* `npm install` *(Sólo una vez, para instalar las librerías. Generará la carpeta `node_modules`)*
* `npm run start`

Debería abrirse un navegador que visita la página `http://localhost:3000`.

## :package: La tarea

Tras seguir los pasos de arriba, ahora, **modifica** el archivo `components/WelcomeAlert.js` para que el mensaje sea distinto:

```diff
const WelcomeAlert = (props) => {

     // Aquí mostraremos una alerta...
-
-    return <h1 data-cy="issue1Title">¡Hola!</h1>
+    return <h1 data-cy="issue1Title">RANDOMEXPRESSIONHola, developer|¡Hola, developer!|Hola :D|Arrancando motores|Arrancando motores, developer...|Encendiendo, developer|Calentando motores, developer...END</h1>
}
```

Ves el mensaje actualizado en http://localhost:3000, ¿verdad?

A continuación, **añade** un `alert` a dicho componente:

```diff
const WelcomeAlert = (props) => {

    // Aquí mostraremos una alerta...
    alert("RANDOMEXPRESSIONBienvenido a la página|Bienvenido|Bienvenido a la nueva página|Bienvenido a esta página|Bienvenido seasEND");

    return <h1 data-cy="issue1Title">RANDOMEXPRESSIONHola, developer|¡Hola, developer!|Hola :D|Arrancando motores|Arrancando motores, developer...|Encendiendo, developer|Calentando motores, developer...END</h1>
}
```

¿Qué pasa en la página?

¿Aparece dicha alerta? Quizás... ¿Dos veces?

### ¿Por qué aparece la alerta dos veces?

Tu proyecto tiene la siguiente jerarquía de componentes:

<img src="react-states/docs/components_welcome_alert.png" />

Si abres `index.js`, verás:

```js
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
```

Resulta que, como es habitual, _toda_ la aplicación React está envuelta en [`<React.StrictMode>`](https://react.dev/reference/react/StrictMode).

Esto produce, entre otras cosas, que todos los componentes se _rendericen_ **dos** veces. Así es más fácil detectar ciertos _bugs_ en nuestro código.

Tiene que ver con la naturaleza _funcional_ del código React. Iremos sumergiéndonos en ello poco a poco.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

No está de más verificar que tu _commit_ se ha subido correctamente desde GitLab (https://raspi).
