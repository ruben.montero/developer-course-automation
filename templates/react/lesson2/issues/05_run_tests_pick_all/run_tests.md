Great Grades Online: Ejecutar tests

## :books: Resumen

* Solicitaremos al profesor que suba los _tests_, y los ejecutaremos
* Si hay errores en nuestro código de las tareas anteriores, lo corregiremos

## Descripción

La nota de cada _sprint_ será calculada individualmente, lanzando _tests_ automáticos sobre tu código.

Los _tests_ no están inicialmente en tu repositorio, y para lanzarlos **debes solicitar al profesor** que los **suba**. Debes hacerlo cuando hayas completado todas las tareas anteriores.

Luego, podrás ejecutar:

```
git pull
```

...desde una línea de comandos (cmd.exe), y verás que los _tests_ habrán aparecido en la carpeta `react-states/cypress/e2e`

### Ya tengo _tests_ automáticos... ¿y ahora?

¡Lánzalos!

Desde **un nuevo terminal** (cmd.exe), cambiamos (`cd`) a la carpeta `react-states` y ejecutamos:

```
npx cypress open
```

...para **lanzar Cypress**:

## Cypress

Se abrirá una ventana. Elegimos _end-to-end testing_ (e2e):

Luego, elegimos iniciar el _software_ de pruebas en nuestro navegador preferido.

Se abrirá el navegador y mostrará la lista de _tests_.

### ¡Ejecuta todos los _tests_!

La misión es **verificar que todos los _tests_ pasan** (es decir, _ticks verdes_).

**Si encuentras que algún _test_ no pasa (_tick rojo_), debes buscar el error y corregir tu código para que el _test_ pase**. Esa es nuestra misión. Podemos hacer los _commits_ adicionales que queramos para arreglar errores en el código. ¡Nuestra nota será directamente proporcional al número de _tests_ que pasen satisfactoriamente!

### :trophy: Por último

Una vez verifiques que **todos** los _tests_ pasan correctamente... ¡Trabajo del _sprint_ finalizado!
