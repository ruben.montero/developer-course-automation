¡Trabajo terminado!

## :trophy: ¡Felicidades!

Has realizado las tareas del _sprint_ y has validado tu trabajo con los _tests_. ¡Enhorabuena!

<img src="react-states/docs/sunset.jpg" width=500 />

Has...

* Aprendido que StrictMode fuerza el renderizado de los componentes dos veces
* Escrito un componente sencillo con un botón
* Asociado el `onClick` del botón a una función convencional y a una _arrow_ `=>`
* Creado estados con `useState`
* Comprendido las diferencias entre estados y variables normales
* Aprendido a invocar el `setter` de un estado pasando una función actualizadora
* Instalado la librería `react-router-dom`
* Implementado varias rutas en tu página web
* Implementado distintos contadores que ejemplificaban el uso de estados en React
* Creado una chatroom offline que obtenía la entrada del usuario
* Aprendido sobre alternativas válidas para modificar estados _array_ en React
