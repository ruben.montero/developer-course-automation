Contador sencillo

## :books: Resumen

* Crearemos un componente sencillo que sirva como contador
* Añadiremos una nueva ruta `/contadores` a nuestra web y lo mostraremos ahí

## Descripción

Retomemos nuestro trabajo con componentes que albergan _estados_. Al fin y al cabo, son el corazón (:heart:) de React.

## :package: La tarea

**Añade** una nueva carpeta `src/components/counters/`, y ahí, **crea** `Counter.js`.

En el componente `Counter`, **incluye** un _estado_ como este:

```jsx
  const [value, setValue] = useState(0);
```

También, **añade** un método `increaseValue` a `Counter`:

```jsx
  const increaseValue = () => {
    setValue(oldValue => { return oldValue + 1 });
  }
```

¿Recuerdas que cuando _setteamos_ un estado cuyo nuevo valor depende del anterior, pasamos una _función actualizadora_? De lo contrario, el valor [_podría_ no ser fiable](https://reactjs.org/docs/state-and-lifecycle.html#state-updates-may-be-asynchronous) .

Para terminar con el componente, **implementa** `return` para que muestre un `<div>` raíz que contenga:

* Un `<p>` que muestre el valor del estado, **así**:

```jsx
<p>RANDOMEXPRESSIONNúmero|Valor|Cuenta|Estado|NÚMERO|VALOREND: {value}</p>
```

* Un botón que invoque `increaseValue` al pulsarlo. **Así**:

```jsx
<button data-cy='issueISSUENUMBERbutton' onClick={ increaseValue }>RANDOMEXPRESSIONContar|Sumar|IncrementarEND</button>
```

### Una nueva página

**Crea** también un nuevo fichero `src/pages/CountersPage.js`:

```jsx
import Counter from "../components/counters/Counter";

const CountersPage = (props) => {
    return <> 
      <div data-cy='issueISSUENUMBER'>
        <h1>Ejercicio ISSUENUMBER</h1>
        <Counter />
      </div>
    </>
}

export default CountersPage;
```

### Y una nueva ruta...

Por último, en `App.js` **crea** una nueva _ruta_ que _mapee_ la URL `/contadores` a `<CountersPage />`

¿Ves cómo funciona el contador en http://localhost:3000/contadores?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
