El mejor de los contadores

## :books: Resumen

* Crearemos `BestCounter.js`, similar a `EvenBetterCounter.js`, pero si se ha alcanzado `props.maxValue` o `props.minValue`, los botones RANDOMEXPRESSIONContar|Sumar|IncrementarEND/RANDOMEXPRESSIONDescontar|Restar|Contar hacia abajo|DecrementarEND aparecerán como deshabilitados, respectivamente
* Lo mostraremos desde http://localhost:3000/contadores

## Descripción

Siempre hay margen para mejorar, ¿no crees?

## :package: La tarea

**Crea** `BestCounter.js`.

**Haz** una _copia_[^1] de `EvenBetterCounter.js` y **pégala** en `BestCounter.js`.

Después, **codifica lo necesario** en `BestCounter.js` para que:

* Si `value` es igual a `props.maxValue`, el botón RANDOMEXPRESSIONContar|Sumar|IncrementarEND **tendrá** el atributo `disabled={true}`. En caso contrario, `disabled={false}`
* Si `value` es igual a `props.minValue`, el botón RANDOMEXPRESSIONDescontar|Restar|Contar hacia abajo|DecrementarEND **tendrá** el atributo `disabled={true}`. En caso contrario, `disabled={false}`.

Por último, en `CountersPage.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Tendrá el atributo `data-cy='issueISSUENUMBER'` y en su contenido, **incluye**:

* Una etiqueta `<h1>` que indique 'Ejercicio ISSUENUMBER'.
* El componente `<BestCounter startValue={0} minValue={0} maxValue={RANDOMNUMBER2-10END}/>`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Evidentemente, hace falta modificar el nombre del componente por `BestCounter` y la sentencia `export` al final.
