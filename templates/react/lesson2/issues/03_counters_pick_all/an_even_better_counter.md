Contador aún mejor

## :books: Resumen

* Modificaremos `BetterCounter.js` para que admita un valor inicial mediante una _prop_
* Modificaremos `CountersPage.js` para usarlo

## Descripción

En esta tarea, trabajaremos sobre _el mismo_ `BetterCounter.js` que creamos en la tarea anterior.

## :package: La tarea

**Modifica** `BetterCounter.js` para que, en función de una _prop_ `startValue` se pueda determinar el valor inicial del contador.

Si **no** se ha especificado la _prop_ `startValue`, el valor por defecto **será `0`**. Es decir, no se debería _romper_ el ejercicio anterior.

Para ello, emplea el [operador coalesce](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator).

### Y añadimos otra sección...

Por último, en `CountersPage.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Tendrá el atributo `data-cy='issueISSUENUMBER'` y en su contenido, **incluye**:

* Una etiqueta `<h1>` que indique 'Ejercicio ISSUENUMBER'.
* El componente `<BetterCounter startValue={RANDOMNUMBER5-300END} />`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
