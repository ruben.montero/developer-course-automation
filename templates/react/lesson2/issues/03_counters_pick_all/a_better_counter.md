Contador mejorado

## :books: Resumen

* Crearemos `BetterCounter.js` similar al contador que ya hemos creado, pero también permitirá decrementar la cuenta
* Lo mostraremos en http://localhost:3000/contadores

## Descripción

¡Tenemos un contador bueno!

¡Hagamos uno mejor!

## :package: La tarea

**Crea** `src/components/counters/BetterCounter.js`.

**Haz** una _copia_[^1] de `Counter.js` y **pégala** en `BetterCounter.js`.

En este nuevo componente, **modifica** el `data-cy` del botón para que sea `data-cy='increaseButton'`.

Después, **añade** un **segundo botón** con las siguientes características:

* Tendrá `data-cy='decreaseButton'`
* Mostrará 'RANDOMEXPRESSIONDescontar|Restar|Contar hacia abajo|DecrementarEND'
* Decrementará en `1` el valor del estado `value`.

Por último, en `CountersPage.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Tendrá el atributo `data-cy='issueISSUENUMBER'` y en su contenido, **incluye**:

* Una etiqueta `<h1>` que indique 'Ejercicio ISSUENUMBER'.
* El componente `<BetterCounter />`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

--------------------------------------------------------------------------

[^1]: Evidentemente, hace falta modificar el nombre del componente por `BetterCounter` y la sentencia `export` al final.
