/// <reference types="cypress" />

describe('the_best_counter', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/contadores')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains an appropriate starting paragraph', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'RANDOMEXPRESSIONNúmero|Valor|Cuenta|Estado|NÚMERO|VALOREND: 0');
    })

    it('issueISSUENUMBER div contains an appropriate increase button', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=increaseButton]')
        .should('have.text', 'RANDOMEXPRESSIONContar|Sumar|IncrementarEND');
    })

    it('issueISSUENUMBER div contains an appropriate decrease button', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=decreaseButton]')
        .should('have.text', 'RANDOMEXPRESSIONDescontar|Restar|Contar hacia abajo|DecrementarEND');
    })

    it('issueISSUENUMBER div paragraph has decrease button initially disabled', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=decreaseButton]')
        .should('be.disabled')
    })

    it('issueISSUENUMBER div paragraph has increase button initially enabled', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=increaseButton]')
        .should('not.be.disabled')
    })

    it('issueISSUENUMBER div paragraph has correct value incremented by 1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=increaseButton]')
        .click()
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'RANDOMEXPRESSIONNúmero|Valor|Cuenta|Estado|NÚMERO|VALOREND: 1');
    })

    it('issueISSUENUMBER div paragraph has correct value incremented by 1, and both buttons are enabled', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=increaseButton]')
        .click()
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'RANDOMEXPRESSIONNúmero|Valor|Cuenta|Estado|NÚMERO|VALOREND: 1');

        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=increaseButton]')
        .should('not.be.disabled')

        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=decreaseButton]')
        .should('not.be.disabled')
    })

    it('issueISSUENUMBER div paragraph has correct value incremented up to limit, and increase button is disabled', () => {
        for (let i = 0; i < RANDOMNUMBER2-10END; i++) {
           cy.get('div[data-cy=issueISSUENUMBER]')
           .find('button[data-cy=increaseButton]')
           .click()
        }

        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'RANDOMEXPRESSIONNúmero|Valor|Cuenta|Estado|NÚMERO|VALOREND: RANDOMNUMBER2-10END');
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=increaseButton]')
        .should('be.disabled')
    })
})
