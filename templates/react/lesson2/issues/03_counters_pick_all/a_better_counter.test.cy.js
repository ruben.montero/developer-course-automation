/// <reference types="cypress" />

describe('a_better_counter', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/contadores')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains an appropriate starting paragraph', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'RANDOMEXPRESSIONNúmero|Valor|Cuenta|Estado|NÚMERO|VALOREND: 0');
    })

    it('issueISSUENUMBER div contains an appropriate increase button', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=increaseButton]')
        .should('have.text', 'RANDOMEXPRESSIONContar|Sumar|IncrementarEND');
    })

    it('issueISSUENUMBER div contains an appropriate decrease button', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=decreaseButton]')
        .should('have.text', 'RANDOMEXPRESSIONDescontar|Restar|Contar hacia abajo|DecrementarEND');
    })

    it('issueISSUENUMBER div paragraph has correct value incremented by 1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=increaseButton]')
        .click()
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'RANDOMEXPRESSIONNúmero|Valor|Cuenta|Estado|NÚMERO|VALOREND: 1');
    })

    it('issueISSUENUMBER div paragraph has correct value decremented by 1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=decreaseButton]')
        .click()
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'RANDOMEXPRESSIONNúmero|Valor|Cuenta|Estado|NÚMERO|VALOREND: -1');
    })

    it('issueISSUENUMBER div paragraph has correct value decremented by 4 and incremented by 2', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=decreaseButton]')
        .click().click().click().click()

        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=increaseButton]')
        .click().click()

        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'RANDOMEXPRESSIONNúmero|Valor|Cuenta|Estado|NÚMERO|VALOREND: -2');
    })
})
