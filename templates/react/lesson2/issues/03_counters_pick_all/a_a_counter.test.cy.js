/// <reference types="cypress" />

describe('a_a_counter', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/contadores')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains an appropriate starting paragraph', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'RANDOMEXPRESSIONNúmero|Valor|Cuenta|Estado|NÚMERO|VALOREND: 0');
    })

    it('issueISSUENUMBER div contains an appropriate button', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=issueISSUENUMBERbutton]')
        .should('have.text', 'RANDOMEXPRESSIONContar|Sumar|IncrementarEND');
    })

    it('issueISSUENUMBER div paragraph has correct value incremented by 1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=issueISSUENUMBERbutton]')
        .click()
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'RANDOMEXPRESSIONNúmero|Valor|Cuenta|Estado|NÚMERO|VALOREND: 1');
    })

    it('issueISSUENUMBER div paragraph has correct value incremented by 3', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('button[data-cy=issueISSUENUMBERbutton]')
        .click().click().click()
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'RANDOMEXPRESSIONNúmero|Valor|Cuenta|Estado|NÚMERO|VALOREND: 3');
    })
})
