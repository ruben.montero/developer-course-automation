Contador aún mejor, todavía mejor

## :books: Resumen

* Crearemos `EvenBetterCounter.js` similar a `BetterCounter.js`, pero también permitirá especificar un número máximo y un número mínimo. 
* Lo mostraremos desde http://localhost:3000/contadores

## Descripción

Se pide que añadas un nuevo componente `<EvenBetterCounter>` que sea _prácticamente idéntico_ al `<BetterCounter>` que ya hemos programado.

Pero... ¡aún mejor!

## :package: La tarea

**Crea** `src/components/counters/EvenBetterCounter.js`.

**Haz** una _copia_[^1] de `BetterCounter.js` y **pégala** en `EvenBetterCounter.js`.

Después, **codifica lo necesario** en `EvenBetterCounter.js` para tener el cuenta el valor de `props.maxValue` y `props.minValue` en las funciones asociadas a los `onClick`:

* Si `value` va a rebasar positivamente `maxValue`, **no** lo actualizaremos
* Si `value` va a rebasar negativamente `minValue`, **no** lo actualizaremos

Por último, en `CountersPage.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Tendrá el atributo `data-cy='issueISSUENUMBER'` y en su contenido, **incluye**:

* Una etiqueta `<h1>` que indique 'Ejercicio ISSUENUMBER'.
* El componente `<EvenBetterCounter startValue={0} minValue={0} maxValue={RANDOMNUMBER2-10END}/>`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Evidentemente, hace falta modificar el nombre del componente por `EvenBetterCounter` y la sentencia `export` al final.
