/// <reference types="cypress" />

describe('function_update_state_not_bugged', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/');
    })

    const nextValue = (prevValue) => {
      return prevValue === 'No' ? 'Sí' : 'No';
    }


    it('issueISSUENUMBER button clicked produces expected output', () => {
      cy.get('p[data-cy=alternatortext]').should('have.text', 'RANDOMEXPRESSIONNo|SíEND');
      cy.get('button[data-cy=alternatorbutton').click();
      cy.get('p[data-cy=alternatortext]').should('have.text', nextValue('RANDOMEXPRESSIONNo|SíEND'));
    })

    it('issueISSUENUMBER button clicked twice produces expected output', () => {
      cy.get('p[data-cy=alternatortext]').should('have.text', 'RANDOMEXPRESSIONNo|SíEND');
      cy.get('button[data-cy=alternatorbutton').click();
      cy.get('p[data-cy=alternatortext]').should('have.text', nextValue('RANDOMEXPRESSIONNo|SíEND'));
      cy.get('button[data-cy=alternatorbutton').click();
      cy.get('p[data-cy=alternatortext]').should('have.text', nextValue(nextValue('RANDOMEXPRESSIONNo|SíEND')));
    })
    
    it('issueISSUENUMBER button clicked 3 times produces expected output', () => {
      cy.get('p[data-cy=alternatortext]').should('have.text', 'RANDOMEXPRESSIONNo|SíEND');
      cy.get('button[data-cy=alternatorbutton').click();
      cy.get('p[data-cy=alternatortext]').should('have.text', nextValue('RANDOMEXPRESSIONNo|SíEND'));
      cy.get('button[data-cy=alternatorbutton').click();
      cy.get('p[data-cy=alternatortext]').should('have.text', nextValue(nextValue('RANDOMEXPRESSIONNo|SíEND')));
      cy.get('button[data-cy=alternatorbutton').click();
      cy.get('p[data-cy=alternatortext]').should('have.text', nextValue(nextValue(nextValue('RANDOMEXPRESSIONNo|SíEND'))));
    })

    it('issueISSUENUMBER happymsg is correct', () => {
      cy.get('p[data-cy=issueISSUENUMBERhappymsg]').should('have.text', 'RANDOMEXPRESSION¡Ahora funciona bien!|¡Ahora va bien!|¡Ahora sin bugs!|¡Ahora libre de bugs!|Ahora funciona bien|Ahora va bien|Ahora sin bugs|Ahora libre de bugsEND');
    })
})
