Cambia de Sí a No con un click. Mal

## :books: Resumen

* Añadiremos un componente `SimpleAlternator.js`, que cambia entre "Sí" o "No" al pulsar un botón
* Lo incluiremos en `MainPage.js`

## Descripción

Vamos a crear un componente que alterna el texto mostrado entre dos opciones: "Sí" o "No".

Y lo vamos a hacer _mal_.

## :package: La tarea

**Crea** un nuevo componente `SimpleAlternator.js` dentro de `src/components/buttons/`. **Así**:

```jsx
import { useState } from "react";

const SimpleAlternator = (props) => {

    const [text, setText] = useState("RANDOMEXPRESSIONNo|SíEND");

    const alternateClicked = (event) => {

    }

    return <div>
        <p data-cy="alternatortext">{text}</p>
        <button data-cy="alternatorbutton" onClick={alternateClicked}>RANDOMEXPRESSIONCambiar|Modificar|AlternarEND</button>
    </div>
}

export default SimpleAlternator;
```

Ahora, **añade** una función `switchText` (podría ser cualquier nombre) e **invócala** desde `alternateClicked`:

```diff
const SimpleAlternator = (props) => {

    const [text, setText] = useState("RANDOMEXPRESSIONNo|SíEND");
+
+   const switchText = () => {
+
+   }

    const alternateClicked = (event) => {
+       switchText();
    }

    return <div>
        <p data-cy="alternatortext">{text}</p>
        <button data-cy="alternatorbutton" onClick={alternateClicked}>RANDOMEXPRESSIONCambiar|Modificar|AlternarEND</button>
    </div>
}
```

Y, en `switchText`, **cambia** el estado `text` a "Sí" en caso de que valga "No", y viceversa:

```jsx
    const switchText = () => {
        if (text === "No") {
            setText("Sí");
        } else {
            setText("No");
        }
    }
```

¡Componente completado! (de momento)

Para terminar, en `MainPage.js`, **añádelo** _a continuación_ (debajo) del código de ejercicios anteriores, **así**:

```jsx
const MainPage = (props) => {
    return <div>
        { /* ... */ }
        <h2 data-cy="issueISSUENUMBERtitle">Ejercicio ISSUENUMBER</h2>
        <h3 data-cy="issueISSUENUMBERsubtext">RANDOMEXPRESSIONEl siguiente texto alterna entre sí o no cuando pulsas el botón|El siguiente texto alterna entre sí o no al pulsar el botón|Este texto alterna entre sí o no cuando pulsas el botón|Este texto alterna entre sí o no al pulsar el botónEND</h3>
        <SimpleAlternator />
      </div>
}
```

¿Qué tal funciona?

¿Funciona correctamente en http://localhost:3000?

A pesar de lo que veas... ¡está _mal_ implementado!

¡Y en la siguiente tarea explicaremos _por qué_!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
