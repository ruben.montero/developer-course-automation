/// <reference types="cypress" />

describe('function_update_state_bugged', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/');
    })

    const nextValue = (prevValue) => {
      return prevValue === 'No' ? 'Sí' : 'No';
    }

    it('issueISSUENUMBER button text is correct', () => {
      cy.get('button[data-cy=alternatorbutton]').should('have.text', 'RANDOMEXPRESSIONCambiar|Modificar|AlternarEND');
    })

    it('issueISSUENUMBER initial output is correct', () => {
      cy.get('p[data-cy=alternatortext]').should('have.text', 'RANDOMEXPRESSIONNo|SíEND');
    })

    it('issueISSUENUMBER button clicked produces expected output', () => {
      cy.get('p[data-cy=alternatortext]').should('have.text', 'RANDOMEXPRESSIONNo|SíEND');
      cy.get('button[data-cy=alternatorbutton').click();
      cy.get('p[data-cy=alternatortext]').should('have.text', nextValue('RANDOMEXPRESSIONNo|SíEND'));
    })

    it('issueISSUENUMBER button clicked twice produces expected output', () => {
      cy.get('p[data-cy=alternatortext]').should('have.text', 'RANDOMEXPRESSIONNo|SíEND');
      cy.get('button[data-cy=alternatorbutton').click();
      cy.get('p[data-cy=alternatortext]').should('have.text', nextValue('RANDOMEXPRESSIONNo|SíEND'));
      cy.get('button[data-cy=alternatorbutton').click();
      cy.get('p[data-cy=alternatortext]').should('have.text', nextValue(nextValue('RANDOMEXPRESSIONNo|SíEND')));
    })

    it('issueISSUENUMBER initial subtext is correct', () => {
      cy.get('h3[data-cy=issueISSUENUMBERsubtext]').should('have.text', 'RANDOMEXPRESSIONEl siguiente texto alterna entre sí o no cuando pulsas el botón|El siguiente texto alterna entre sí o no al pulsar el botón|Este texto alterna entre sí o no cuando pulsas el botón|Este texto alterna entre sí o no al pulsar el botónEND');
    })
})
