Cambia de Sí a No con un click... ¡bien!

## :books: Resumen

* Demostraremos que el componente `SimpleAlternator.js` no está bien
* Lo mejoraremos

## Descripción

¡El código del ejercicio anterior está _mal_!

Para _experimentar_, **añade** la siguiente línea:

```diff
const SimpleAlternator = (props) => {

    const [text, setText] = useState("RANDOMEXPRESSIONNo|SíEND");

    const switchText = () => {
        if (text === "No") {
            setText("Sí");
        } else {
            setText("No");
        }
    }

    const alternateClicked = (event) => {
        switchText();
+       switchText();
    }

    return <div>
        <p data-cy="alternatortext">{text}</p>
        <button data-cy="alternatorbutton" onClick={alternateClicked}>RANDOMEXPRESSIONCambiar|Modificar|AlternarEND</button>
    </div>
}
```

Ahora, cuando clicas, se alterna _dos_ veces.

Pruébalo en http://localhost:3000

### ¿Funciona como debería?

Debería alternar dos veces seguidas y quedarse finalmente en "RANDOMEXPRESSIONNo|SíEND", ¿verdad?

¡Pero no lo hace!

¡Recuerda que _cuando se modifica un estado, el nuevo valor no se refleja instantáneamente_!

## :package: La tarea

En `SimpleAlternator.js`, en `switchText` **borra** todo el contenido:

```diff
    const switchText = () => {
-        if (text === "No") {
-            setText("Sí");
-        } else {
-            setText("No");
-        }
    }
```

Ahora, **añade** `setText`:

```jsx
    const switchText = () => {
       setText();
    }
```

Y, esta vez, en lugar de pasar _un valor_, **pasa** una _función_:

```jsx
    const switchText = () => {
       setText(() => {
       
       });
    }
```

¡Una función que declara _un parámetro_!

```jsx
    const switchText = () => {
       setText((textoActual) => {
       
       });
    }
```

### ¿Por qué?

Como observaste al inicio de esta tarea, al actualizar un _estado_ partimos del valor _anterior_, ¡no tenemos garantizado que dicho valor esté correctamente reflejado!

Por eso, en React, podemos pasar una _función_ al _setter_ (en lugar de un valor directamente), y React, haciendo su magia, se encargará de _lanzar ordenadamente_ las actualizaciones de los estados, respetando las modificaciones que se vayan haciendo una a una.

Por ello, ahora, **dentro** de la función, **implementa** el `if` (esta vez contemplando `textoActual` en vez de `text` directamente):

```jsx
    const switchText = () => {
       setText((textoActual) => {
           if (textoActual === 'No') {
               return 'Sí';
           } else {
               return 'No';
           }
       });
    }
```

_(Observa que se debe hacer `return` del nuevo valor para el estado)_

### En resumen

Si para actualizar un estado, partimos de su valor anterior, NO debemos usar el _setter_ pasando un valor directamente.

En su lugar, [debemos pasar una _función_ que _transforma_ el estado viejo (parámetro) en el nuevo (`return`)](https://es.react.dev/reference/react/useState#updating-state-based-on-the-previous-state).

### Probando, probando...

Pruébalo en http://localhost:3000. ¿Se comporta ahora como esperas?

¿Tiene sentido?

### Para cerrar...

**Borra** la línea experimental que añadimos al principio:

```diff
const SimpleAlternator = (props) => {

    const [text, setText] = useState("RANDOMEXPRESSIONNo|SíEND");

    const switchText = () => {
       setText((textoActual) => {
           if (textoActual === 'No') {
               return 'Sí';
           } else {
               return 'No';
           }
       });
    }

    const alternateClicked = (event) => {
        switchText();
-       switchText();
    }

    return <div>
        <p data-cy="alternatortext">{text}</p>
        <button data-cy="alternatorbutton" onClick={alternateClicked}>RANDOMEXPRESSIONCambiar|Modificar|AlternarEND</button>
    </div>
}
```

Y **añade** un párrafo en `MainPage.js`:

```jsx
const MainPage = (props) => {
    return <div>
        { /* ... */ }
        <SimpleAlternator />
        <p data-cy="issueISSUENUMBERhappymsg">RANDOMEXPRESSION¡Ahora funciona bien!|¡Ahora va bien!|¡Ahora sin bugs!|¡Ahora libre de bugs!|Ahora funciona bien|Ahora va bien|Ahora sin bugs|Ahora libre de bugsEND</p>
      </div>
}
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
