Un estado NO es una variable normal

## :books: Resumen

* Añadiremos un componente `SimpleQuestionInteractor.js`
* Lo mostraremos en `MainPage.js`

## Descripción

¿Te parece que un _estado_ en React es algo similar a una variable?

Vuelcas un valor en él, lo modificas, accedes posteriormente...

¡Ojo! ¡Un estado no es una variable normal! Hay dos reglas que debemos considerar:

### 1. Debe modificarse con el _setter_ apropiado

Nunca debes modificar el valor de un estado directamente:

```jsx
const [state, setState] = useState("")

// ESTO ESTÁ MAL
state = "Nuevo valor";
```

Si no empleamos la función retornada por `useState` (en el ejemplo de arriba la hemos llamado `setState`), estamos _cortocircuitando_ los mecanismos de React, e imposibilitando que los componentes _reaccionen_ a los cambios en los estados. Es, precisamente, en `setState` donde la librería React implementa su magia.

### 2. Después de modificarse, el nuevo valor no se refleja instantáneamente

El siguiente código tiene sentido, ¿verdad?

```
let nota = 5;
console.log(nota); // Imprime 5

nota = 10;
console.log(nota); // Imprime 10
```

Pues, con estados de React, ¡no funciona así!

```jsx
const [nota, setNota] = useState(5);

console.log(nota); // Imprime 5

setNota(10);
console.log(nota); // ¡Vuelve a imprimir 5!
```

Cuando hacemos `setNota`, estamos dándole un nuevo valor a `nota` (eso es verdad), pero no _instantáneamente_.

React va a agrupar las actualizaciones de los estados por lotes, y a asignarlos de manera _efectiva_ todos de golpe, para repintar el componente sólo una vez y ser más eficiente.

Veremos más sobre esto más adelante.

## :package: La tarea

**Crea** un nuevo componente `SimpleQuestionInteractor.js` dentro de `src/components/buttons/`. **Así**:

```jsximport { useState } from "react";

const SimpleQuestionInteractor = (props) => {

    const [paragraph, setParagraph] = useState("RANDOMEXPRESSION¿Son los estados de React variables normales?|¿Son los estados variables normales?|¿Son los estados en React variables normales?END");

    return <div>
        <p data-cy="questiontext">{paragraph}</p>
        <button data-cy="questionbutton" onClick={ () => { setParagraph("RANDOMEXPRESSIONNo|No!|¡No!|NO|¡NO!|NO!|¡NOOOO!|¡¡No!!|¡¡NO!!END"); } }>RANDOMEXPRESSIONResponder|Ver respuesta|Dar respuesta|Obtener respuestaEND</button>
    </div>
}

export default SimpleQuestionInteractor;
```

Y, en `MainPage.js`, **añádelo** _a continuación_ (debajo) del código de ejercicios anteriores, **así**:

```jsx
const MainPage = (props) => {
    return <div>
        { /* ... */ }
        <h2 data-cy="issueISSUENUMBERtitle">Ejercicio ISSUENUMBER</h2>
        <SimpleQuestionInteractor />
      </div>
}
```

¿Va igual de bien que los ejercicios anteriores en http://localhost:3000?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
