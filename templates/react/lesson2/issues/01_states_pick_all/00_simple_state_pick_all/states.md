El corazón de React: Los estados

## :books: Resumen

* Añadiremos un componente `SimpleHelloInteractor.js`
* ¡Tendrá un estado!
* Lo mostraremos en `MainPage.js`

## Descripción

¿Sabrías cómo conseguir que, al hacer click en un botón, la página web cambiase visualmente?

¿Sabes por qué "React" se llama _"React"_?

Vamos a dar respuesta a esas preguntas introduciendo un concepto clave en React: los [estados](https://react.dev/learn/state-a-components-memory).

## :package: La tarea

**Crea** un nuevo componente `SimpleHelloInteractor.js` dentro de `src/components/buttons/`. **Así**:

```jsx
const SimpleHelloInteractor = (props) => {

    const helloClicked = (event) => {

    }

    return <div>
        <p data-cy="hellotext"></p>
        <button data-cy="hellobutton" onClick={helloClicked}>RANDOMEXPRESSIONSay hello|Di hola|Decir hola|Saludar|Say hi|Say something|Di algo|Decir algoEND</button>
    </div>
}

export default SimpleHelloInteractor;
```

Y, en `MainPage.js`, **añádelo** _a continuación_ (debajo) del código de ejercicios anteriores, **así**:

```jsx
const MainPage = (props) => {
    return <div>
        { /* ... */ }
        <h2 data-cy="issueISSUENUMBERtitle">Ejercicio ISSUENUMBER</h2>
        <SimpleHelloInteractor />
      </div>
}
```

Se ve el nuevo botón en http://localhost:3000, ¿verdad?

### Declarar, mostrar y modificar un estado

A continuación, en `SimpleHelloInteractor.js`, **crea** un estado `text` mediante [`useState`](https://es.react.dev/reference/react/useState):

```diff
+import { useState } from "react";

const SimpleHelloInteractor = (props) => {
+
+   const [text, setText] = useState("RANDOMEXPRESSIONValor inicial|Valor vacío|Valor al inicio|Valor por defecto|Texto por defecto|Texto inicial|Texto vacíoEND");

    const helloClicked = (event) => {

    }

    return <div>
        <p data-cy="hellotext"></p>
        <button data-cy="hellobutton" onClick={helloClicked}>RANDOMEXPRESSIONSay hello|Di hola|Decir hola|Saludar|Say hi|Say something|Di algo|Decir algoEND</button>
    </div>
}
```

**Muestra** el contenido de `texto` dentro del `<p>`:

```diff
import { useState } from "react";

const SimpleHelloInteractor = (props) => {

    const [text, setText] = useState("RANDOMEXPRESSIONValor inicial|Valor vacío|Valor al inicio|Valor por defecto|Texto por defecto|Texto inicial|Texto vacíoEND");

    const helloClicked = (event) => {

    }

    return <div>
-       <p data-cy="hellotext"></p>
+       <p data-cy="hellotext">{text}</p>
        <button data-cy="hellobutton" onClick={helloClicked}>RANDOMEXPRESSIONSay hello|Di hola|Decir hola|Saludar|Say hi|Say something|Di algo|Decir algoEND</button>
    </div>
}
```

Y **modifica** el estado `texto` (con `setText`) cuando se haga click en el botón:

```diff
import { useState } from "react";

const SimpleHelloInteractor = (props) => {

    const [text, setText] = useState("RANDOMEXPRESSIONValor inicial|Valor vacío|Valor al inicio|Valor por defecto|Texto por defecto|Texto inicial|Texto vacíoEND");

    const helloClicked = (event) => {
+       setText("RANDOMEXPRESSION¡Hola!|Hi!|Hello!|¡Un saludo!|¡Saludos!|¡Buenos días!|Buenos días, developer!|Good morning!END");
    }

    return <div>
       <p>{text}</p>
        <button data-cy="hellobutton" onClick={helloClicked}>RANDOMEXPRESSIONSay hello|Di hola|Decir hola|Saludar|Say hi|Say something|Di algo|Decir algoEND</button>
    </div>
}
```

Si todo va bien, en http://localhost:3000 verás el mensaje `"RANDOMEXPRESSIONValor inicial|Valor vacío|Valor al inicio|Valor por defecto|Texto por defecto|Texto inicial|Texto vacíoEND"` inicialmente. Y, cuando cliques el botón, observarás que cambia a `"RANDOMEXPRESSION¡Hola!|Hi!|Hello!|¡Un saludo!|¡Saludos!|¡Buenos días!|Buenos días, developer!|Good morning!END"`.

¡Enhorabuena!

Has usado tu primer estado en React.

### Pero, ¿qué es esto exactamente? ¿Por qué?

Si quisiéramos programar un comportamiento como este en puro JavaScript, en `helloClicked` deberíamos:

1. _Obtener_ la etiqueta `p` mediante `findElementById`
2. _Modificar_ su contenido alterando `innerHTML`

Sin embargo, con React, basta con _modificar_ un _estado_.

Esta es la _filosofía_ React.

### Dos responsabilidades separadas

En primer lugar, programamos (con JSX) _cómo_ se muestra la página, y _cómo_ los datos (_props_, estados, etc.) aparecen visualmente.

<img src="react-states/docs/mario_pipe1.png" width=650 />

En segundo lugar, programamos _cómo_ esos datos se alteran (modificando estados)

#### React se encarga de trasladar los cambios en los datos (estados) a la parte visual, automáticamente

¡Efectivamente!

¿Te has fijado que, al hacer `setText("RANDOMEXPRESSION¡Hola!|Hi!|Hello!|¡Un saludo!|¡Saludos!|¡Buenos días!|Buenos días, developer!|Good morning!END")`, la página cambia _automáticamente_? ¡Ha _reaccionado_ al cambio!

<img src="react-states/docs/mario_pipe_explained.gif" width=650 />

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
