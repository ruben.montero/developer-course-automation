/// <reference types="cypress" />

describe('states_again', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/');
    })

    it('issueISSUENUMBER button text is correct', () => {
      cy.get('button[data-cy=examplebutton]').should('have.text', 'RANDOMEXPRESSIONDescubrir al asesino|Adivinar asesino|Pronosticar asesino|Descubrir al culpable|Adivinar culpable|Pronosticar culpable|Descubrir culpable|Adivinar culpableEND');
    })

    it('issueISSUENUMBER initial output is correct', () => {
      cy.get('RANDOMEXPRESSIONp|h3|h4END[data-cy=exampletext]').should('have.text', '¿Quién RANDOMEXPRESSIONes|será|puede serEND el RANDOMEXPRESSIONasesino|culpableEND?');
    })

    it('issueISSUENUMBER button clicked produces expected output', () => {
      cy.get('RANDOMEXPRESSIONp|h3|h4END[data-cy=exampletext]').should('have.text', '¿Quién RANDOMEXPRESSIONes|será|puede serEND el RANDOMEXPRESSIONasesino|culpableEND?');
      cy.get('button[data-cy=examplebutton').click();
      cy.get('RANDOMEXPRESSIONp|h3|h4END[data-cy=exampletext]').should('have.text', 'RANDOMEXPRESSIONMr. Brown!|Miss Scarlet!|Colonel Mustard!|Mr. White|Mr. Green!|Mrs. Peacock!END');
    })
})
