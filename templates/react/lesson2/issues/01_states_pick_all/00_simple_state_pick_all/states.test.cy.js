/// <reference types="cypress" />

describe('states', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/');
    })

    it('issueISSUENUMBER button text is correct', () => {
      cy.get('button[data-cy=hellobutton]').should('have.text', 'RANDOMEXPRESSIONSay hello|Di hola|Decir hola|Saludar|Say hi|Say something|Di algo|Decir algoEND');
    })

    it('issueISSUENUMBER initial output is correct', () => {
      cy.get('p[data-cy=hellotext]').should('have.text', 'RANDOMEXPRESSIONValor inicial|Valor vacío|Valor al inicio|Valor por defecto|Texto por defecto|Texto inicial|Texto vacíoEND');
    })

    it('issueISSUENUMBER button clicked produces expected output', () => {
      cy.get('p[data-cy=hellotext]').should('have.text', 'RANDOMEXPRESSIONValor inicial|Valor vacío|Valor al inicio|Valor por defecto|Texto por defecto|Texto inicial|Texto vacíoEND');
      cy.get('button[data-cy=hellobutton').click();
      cy.get('p[data-cy=hellotext]').should('have.text', 'RANDOMEXPRESSION¡Hola!|Hi!|Hello!|¡Un saludo!|¡Saludos!|¡Buenos días!|Buenos días, developer!|Good morning!END');
    })
})
