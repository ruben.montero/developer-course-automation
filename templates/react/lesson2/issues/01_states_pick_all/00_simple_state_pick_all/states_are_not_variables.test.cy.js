/// <reference types="cypress" />

describe('states_are_not_variables', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/');
    })

    it('issueISSUENUMBER button text is correct', () => {
      cy.get('button[data-cy=questionbutton]').should('have.text', 'RANDOMEXPRESSIONResponder|Ver respuesta|Dar respuesta|Obtener respuestaEND');
    })

    it('issueISSUENUMBER initial output is correct', () => {
      cy.get('p[data-cy=questiontext]').should('have.text', 'RANDOMEXPRESSION¿Son los estados de React variables normales?|¿Son los estados variables normales?|¿Son los estados en React variables normales?END');
    })

    it('issueISSUENUMBER button clicked produces expected output', () => {
      cy.get('p[data-cy=questiontext]').should('have.text', 'RANDOMEXPRESSION¿Son los estados de React variables normales?|¿Son los estados variables normales?|¿Son los estados en React variables normales?END');
      cy.get('button[data-cy=questionbutton').click();
      cy.get('p[data-cy=questiontext]').should('have.text', 'RANDOMEXPRESSIONNo|No!|¡No!|NO|¡NO!|NO!|¡NOOOO!|¡¡No!!|¡¡NO!!END');
    })
})
