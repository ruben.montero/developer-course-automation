Practicando... ¡otro estado!

## :books: Resumen

* Añadiremos un componente `SimpleExampleInteractor.js`
* Lo mostraremos en `MainPage.js`

## Descripción

Repasemos lo visto en la tarea anterior.

## :package: La tarea

**Crea** un nuevo componente `SimpleExampleInteractor.js` dentro de `src/components/buttons/`. **Así**:

```jsximport { useState } from "react";

const SimpleExampleInteractor = (props) => {

    const [state, setState] = useState("¿Quién RANDOMEXPRESSIONes|será|puede serEND el RANDOMEXPRESSIONasesino|culpableEND?");

    return <div>
        <RANDOMEXPRESSIONp|h3|h4END data-cy="exampletext">{state}</RANDOMEXPRESSIONp|h3|h4END>
        <button data-cy="examplebutton" onClick={ () => { setState("RANDOMEXPRESSIONMr. Brown!|Miss Scarlet!|Colonel Mustard!|Mr. White|Mr. Green!|Mrs. Peacock!END"); } }>RANDOMEXPRESSIONDescubrir al asesino|Adivinar asesino|Pronosticar asesino|Descubrir al culpable|Adivinar culpable|Pronosticar culpable|Descubrir culpable|Adivinar culpableEND</button>
    </div>
}

export default SimpleExampleInteractor;
```

Y, en `MainPage.js`, **añádelo** _a continuación_ (debajo) del código de ejercicios anteriores, **así**:

```jsx
const MainPage = (props) => {
    return <div>
        { /* ... */ }
        <h2 data-cy="issueISSUENUMBERtitle">Ejercicio ISSUENUMBER</h2>
        <SimpleExampleInteractor />
      </div>
}
```

¿Funciona como prevés en http://localhost:3000?

Por cierto, ¿has jugado al [Cluedo](https://es.wikipedia.org/wiki/Cluedo)?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
