Colores

## :books: Resumen

* Crearemos `SimpleColorExample.js`, que cambiará el color de un fondo al clicar un botón
* Lo mostraremos en `MainPage.js`

## Descripción

Los estados no son algo que podemos reflejar únicamente en el contenido de etiquetas JSX.

¡Pueden estar en _cualquier parte_ del JSX!

Por ejemplo... ¡modificando el valor [`className`](https://stackoverflow.com/questions/55445194/what-does-the-classname-attribute-mean-in-jsx) de un elemento (y por consiguiente, su CSS)!

## :package: La tarea

**Crea** una nueva carpeta `colors/` dentro de `src/components/buttons/`.

En `src/components/buttons/colors/`, **crea** un nuevo fichero `colors.css`:

```css
/* Nuestras clases CSS en React deberían tener nombres
 * únicos que no entren en conflicto.
 * Aunque cada componente importe ficheros CSS por separado,
 * al final, el navegador, ¡mezclará todas las clases CSS! */
.simpleColor {
    width: RANDOMNUMBER150-200ENDpx;
    height: RANDOMNUMBER70-110ENDpx;
}

.simpleColorBackgroundRed {
    background-color: rgb(RANDOMNUMBER190-245END, 0, RANDOMNUMBER1-8END);
}

.simpleColorBackgroundBlack {
    background-color: rgb(RANDOMNUMBER1-15END, 0, RANDOMNUMBER1-8END);
}
```

Luego, **crea** un componente `SimpleColorExample.js` dentro de la misma carpeta `colors/`:

```jsx
import "./colors.css"
import { useState } from "react";

const SimpleColorExample = (props) => {

    const [additionalCssClassName, setAdditionalCssClassName] = useState("simpleColorBackgroundBlack");

    const buttonClicked = (event) => {
        setAdditionalCssClassName("simpleColorBackgroundRed");
    }

    /* ... */
}

export default SimpleColorExample;
```

Como ves, guarda una estado `additionalCssClassName`.

A continuación, **completa** el `return` para que sea así:

```jsx
const SimpleColorExample = (props) => {

    /* ... */

    return <>
        <div data-cy="simplecolordiv" className={"simpleColor " + additionalCssClassName}></div>
        <button data-cy="simplecolorbutton" onClick={buttonClicked}>RANDOMEXPRESSIONPoner rojo|Pintar rojo|Dibujar rojo|Rellenar rojoEND</button>
    </>
}

export default SimpleColorExample;
```

¿Ves cómo se concatena el _string_ `"simpleColor"` al valor del estado?

Así, el `<div>` tendrá siempre dos clases CSS: `simpleColor simpleColorBackgroundBlack` inicialmente, y `simpleColor simpleColorBackgroundRed` después de clicar el botón.

**Añade** esto a `MainPage.js` para poder verlo y probarlo:


```jsx
const MainPage = (props) => {
    return <div>
        { /* ... */ }
        <h2 data-cy="issueISSUENUMBERtitle">Ejercicio ISSUENUMBER</h2>
        <SimpleColorExample />
      </div>
}
```

¿Te convence lo que ves en http://localhost:3000?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
