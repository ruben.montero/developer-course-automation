/// <reference types="cypress" />

describe('color_states_looper', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/');
    })

    it('issueISSUENUMBER button text is correct', () => {
      cy.get('button[data-cy=simplecolorbutton]').should('have.text', 'RANDOMEXPRESSIONPoner rojo|Pintar rojo|Dibujar rojo|Rellenar rojoEND');
    })

    it('issueISSUENUMBER div width is correct', () => {
      cy.get('div[data-cy=simplecolordiv]').should('have.css', 'width', 'RANDOMNUMBER150-200ENDpx');
    })

    it('issueISSUENUMBER div height is correct', () => {
      cy.get('div[data-cy=simplecolordiv]').should('have.css', 'height', 'RANDOMNUMBER70-110ENDpx');
    })

    it('issueISSUENUMBER initial background is correct', () => {
      cy.get('div[data-cy=simplecolordiv]').should('have.css', 'background-color', 'rgb(RANDOMNUMBER1-15END, 0, RANDOMNUMBER1-8END)');
    })

    it('issueISSUENUMBER background is correct after change', () => {
      cy.get('div[data-cy=simplecolordiv]').should('have.css', 'background-color', 'rgb(RANDOMNUMBER1-15END, 0, RANDOMNUMBER1-8END)');
      cy.get('button[data-cy=simplecolorbutton]').click();
      cy.get('div[data-cy=simplecolordiv]').should('have.css', 'background-color', 'rgb(RANDOMNUMBER190-245END, 0, RANDOMNUMBER1-8END)');
    })
})
