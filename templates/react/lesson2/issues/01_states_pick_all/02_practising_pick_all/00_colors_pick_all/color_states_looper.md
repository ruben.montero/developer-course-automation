Colores a vueltas

## :books: Resumen

* Crearemos `SimpleColorAlternator.js`, parecido a `SimpleColorExample.js`, pero alternará entre rojo y negro
* Lo mostraremos en `MainPage.js`

## Descripción

Anteriormente hemos trabajado en una peculiaridad sobre los estados.

¿Recuerdas que cuando [el nuevo valor de un estado depende del anterior, debemos actualizarlo pasando una _función_](https://es.react.dev/reference/react/useState#updating-state-based-on-the-previous-state)?

Vamos a practicarlo nuevamente.

## :package: La tarea

**Crea** un nuevo componente `SimpleColorAlternator.js` dentro de `src/components/buttons/colors/`:

```jsx
import "./colors.css"
import { useState } from "react";

const SimpleColorAlternator = (props) => {

    const [additionalCssClassName, setAdditionalCssClassName] = useState("simpleColorBackgroundBlack");

    const buttonClicked = (event) => {
        // TO-DO: Invocar setAdditionalCssClassName pasando una función
        // Esa función retornará "simpleColorBackgroundBlack" o
        // "simpleColorBackgroundRed", en función de si el estado viejo
        // es uno u otro.
    }

    return <>
        <div data-cy="coloralternatordiv" className={"simpleColor " + additionalCssClassName}></div>
        <button data-cy="coloralternatorbutton" onClick={buttonClicked}>RANDOMEXPRESSIONAlternar color|Cambiar color|Regresar al otro color|Poner de otro color|Pintar de otro color|Pintar el otro colorEND</button>
    </>
}

export default SimpleColorAlternator;
```

Como ves, estamos reutilizando el fichero CSS de la tarea anterior.

Luego, **añade** este componente a `MainPage.js`:

```jsx
const MainPage = (props) => {
    return <div>
        { /* ... */ }
        <h2 data-cy="issueISSUENUMBERtitle">Ejercicio ISSUENUMBER</h2>
        <SimpleColorAlternator />
      </div>
}
```

Y, para terminar, **implementa** correctamente la función `buttonClicked` para que alterne `additionalCssClassName` entre `"simpleColorBackgroundBlack"` y `"simpleColorBackgroundRed"`:

```diff
    const buttonClicked = (event) => {
-       // TO-DO: Invocar setAdditionalCssClassName pasando una función
-       // Esa función retornará "simpleColorBackgroundBlack" o
-       // "simpleColorBackgroundRed", en función de si el estado viejo
-       // es uno u otro.
+       setAdditionalCssClassName(cssAnterior => {
+           if (cssAnterior === "simpleColorBackgroundBlack") {
+               return "simpleColorBackgroundRed";
+           } else {
+               return "simpleColorBackgroundBlack";
+           }
+       });
    }
```

¿Funciona bien en http://localhost:3000?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
