/// <reference types="cypress" />

describe('switching_image', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/');
    })

    it('issueISSUENUMBER image has correct width after click', () => {
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyalternatorimg|disneyalternatorimage|disney_alternator_img|disney_alternator_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalWidth).to.be.eq(237)
        })
      cy.get('button[data-cy=disneyalternatorbtn]').should('have.text', 'RANDOMEXPRESSIONMostrar otro personaje|Mostrar al otro|Ver otro personaje|Ver otro|Ver al otro|AlternarEND');
      cy.get('button[data-cy=disneyalternatorbtn]').click()
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyalternatorimg|disneyalternatorimage|disney_alternator_img|disney_alternator_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalWidth).to.be.eq(236)
        })
    })

    it('issueISSUENUMBER image has correct height after click', () => {
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyalternatorimg|disneyalternatorimage|disney_alternator_img|disney_alternator_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalHeight).to.be.eq(303)
        })
      cy.get('button[data-cy=disneyalternatorbtn]').should('have.text', 'RANDOMEXPRESSIONMostrar otro personaje|Mostrar al otro|Ver otro personaje|Ver otro|Ver al otro|AlternarEND');
      cy.get('button[data-cy=disneyalternatorbtn]').click()
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyalternatorimg|disneyalternatorimage|disney_alternator_img|disney_alternator_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalHeight).to.be.eq(259)
        })
    })

    
    it('issueISSUENUMBER image has correct width after 2 clicks', () => {
      cy.get('button[data-cy=disneyalternatorbtn]').should('have.text', 'RANDOMEXPRESSIONMostrar otro personaje|Mostrar al otro|Ver otro personaje|Ver otro|Ver al otro|AlternarEND');
      cy.get('button[data-cy=disneyalternatorbtn]').click()
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyalternatorimg|disneyalternatorimage|disney_alternator_img|disney_alternator_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalWidth).to.be.eq(236)
        })
      cy.get('button[data-cy=disneyalternatorbtn]').should('have.text', 'RANDOMEXPRESSIONMostrar otro personaje|Mostrar al otro|Ver otro personaje|Ver otro|Ver al otro|AlternarEND');
      cy.get('button[data-cy=disneyalternatorbtn]').click()
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyalternatorimg|disneyalternatorimage|disney_alternator_img|disney_alternator_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalWidth).to.be.eq(237)
        })
    })

    it('issueISSUENUMBER image has correct height after 2 clicks', () => {
      cy.get('button[data-cy=disneyalternatorbtn]').should('have.text', 'RANDOMEXPRESSIONMostrar otro personaje|Mostrar al otro|Ver otro personaje|Ver otro|Ver al otro|AlternarEND');
      cy.get('button[data-cy=disneyalternatorbtn]').click()
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyalternatorimg|disneyalternatorimage|disney_alternator_img|disney_alternator_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalHeight).to.be.eq(259)
        })
      cy.get('button[data-cy=disneyalternatorbtn]').should('have.text', 'RANDOMEXPRESSIONMostrar otro personaje|Mostrar al otro|Ver otro personaje|Ver otro|Ver al otro|AlternarEND');
      cy.get('button[data-cy=disneyalternatorbtn]').click()
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyalternatorimg|disneyalternatorimage|disney_alternator_img|disney_alternator_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalHeight).to.be.eq(303)
        })
    })
})
