Una imagen

## :books: Resumen

* Veremos cómo mostrar una imagen estática desde React
* Lo probaremos en un componente que utilizaremos desde `MainPage.js`

## Descripción

Cuando mostramos una `<img>`, podemos especificar un `src` que apunte a un recurso externo y el navegador lo cargará. Habitualmente, las imágenes y el CSS son servidos por [redes de servidores CDN](https://learn.microsoft.com/en-us/azure/cdn/cdn-overview).

Pero, para casos sencillos, ¡podemos mostrar nuestras imágenes directamente desde el servidor React!

## :package: La tarea

**Crea** una nueva carpeta `disney/` dentro de `src/components/`.

**Descarga** la siguiente imagen (click derecho > Guardar como):

<img src="react-states/docs/donald_duck.png" />

**Guárdala** dentro de `src/components/disney/`, asegurándote de que tiene el nombre `donald_duck.png`.

A continuación, en `src/components/disney`, **crea** un nuevo fichero `Donald.js`:

```jsx
const Donald = (props) => {
    return <img data-cy="RANDOMEXPRESSIONdonaldimg|donaldimage|donald_img|donald_imageEND" />
}

export default Donald;
```

Para mostrar una imagen, ¡basta con _importarla_!

**Completa** tu componente **así**:

```jsx
import donald from "./donald_duck.png"

const Donald = (props) => {
    return <img data-cy="RANDOMEXPRESSIONdonaldimg|donaldimage|donald_img|donald_imageEND" src={donald} />
}

export default Donald;
```

Y **muéstralo** desde `MainPage.js`:

```jsx
const MainPage = (props) => {
    return <div>
        { /* ... */ }
        <h2 data-cy="issueISSUENUMBERtitle">Ejercicio ISSUENUMBER</h2>
        <Donald />
      </div>
}
```

¡Felicidades por haber mostrado tu primera imagen directamente empaquetada en el proyecto React!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
