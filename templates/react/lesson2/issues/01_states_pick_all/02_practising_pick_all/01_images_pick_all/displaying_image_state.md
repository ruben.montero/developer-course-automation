Una imagen en un estado

## :books: Resumen

* Crearemos `DisneyImageWithButton.js`, que mostrará inicialmente a Donald, pero permitirá cambiar a Mickey
* Lo utilizaremos desde `MainPage.js`

## Descripción

[El pato Donald](https://es.wikipedia.org/wiki/Pato_Donald) es un personaje interesante, pero, ¿el protagonista no es [Mickey Mouse](https://es.wikipedia.org/wiki/Mickey_Mouse)?

## :package: La tarea

**Descarga** la siguiente imagen (click derecho > Guardar como):

<img src="react-states/docs/mickey_mouse.png" />

**Guárdala** dentro de `src/components/disney/`, asegurándote de que tiene el nombre `mickey_mouse.png`.

A continuación, en `src/components/disney`, **crea** un nuevo fichero `DisneyImageWithButton.js`:

```jsx
import donald from "./donald_duck.png";
import mickey from "./mickey_mouse.png";
import { useState } from "react";

const DisneyImageWithButton = (props) => {
    const [image, setImage] = useState(donald);

    const buttonClicked = (event) => {
        setImage(mickey);
    }

    return <>
        <img data-cy="RANDOMEXPRESSIONdisneyimg|disneyimage|disney_img|disney_imageEND" src={image} />
        <button data-cy="disneybtn" onClick={buttonClicked}>RANDOMEXPRESSIONCambiar a Mickey|Ver a Mickey|Mostrar Mickey|Cambiar al ratón|Ver al ratón|Mostrar al ratónEND</button>
      </>
}

export default DisneyImageWithButton;
```

¡Y **muestra** este componente desde `MainPage.js`!

```jsx
const MainPage = (props) => {
    return <div>
        { /* ... */ }
        <h2 data-cy="issueISSUENUMBERtitle">Ejercicio ISSUENUMBER</h2>
        <DisneyImageWithButton />
      </div>
}
```

¿Qué tal? ¿Te gusta el resultado en http://localhost:3000?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
