/// <reference types="cypress" />

describe('displaying_image', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/');
    })

    it('issueISSUENUMBER image has correct width', () => {
      cy.get('img[data-cy=RANDOMEXPRESSIONdonaldimg|donaldimage|donald_img|donald_imageEND]').should('be.visible')
        .and(($img) => {
        // "naturalWidth" and "naturalHeight" are set when the image loads
          expect($img[0].naturalWidth).to.be.eq(237)
        })
    })

    it('issueISSUENUMBER image has correct height', () => {
      cy.get('img[data-cy=RANDOMEXPRESSIONdonaldimg|donaldimage|donald_img|donald_imageEND]').should('be.visible')
        .and(($img) => {
        // "naturalWidth" and "naturalHeight" are set when the image loads
          expect($img[0].naturalHeight).to.be.eq(303)
        })
    })
})
