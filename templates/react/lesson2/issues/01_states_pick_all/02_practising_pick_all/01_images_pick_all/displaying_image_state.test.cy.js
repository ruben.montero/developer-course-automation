/// <reference types="cypress" />

describe('displaying_image_state', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/');
    })

    it('issueISSUENUMBER image has correct width', () => {
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyimg|disneyimage|disney_img|disney_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalWidth).to.be.eq(237)
        })
    })

    it('issueISSUENUMBER image has correct height', () => {
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyimg|disneyimage|disney_img|disney_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalHeight).to.be.eq(303)
        })
    })

    it('issueISSUENUMBER image has correct width after click', () => {
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyimg|disneyimage|disney_img|disney_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalWidth).to.be.eq(237)
        })
      cy.get('button[data-cy=disneybtn]').should('have.text', 'RANDOMEXPRESSIONCambiar a Mickey|Ver a Mickey|Mostrar Mickey|Cambiar al ratón|Ver al ratón|Mostrar al ratónEND');
      cy.get('button[data-cy=disneybtn]').click()
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyimg|disneyimage|disney_img|disney_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalWidth).to.be.eq(236)
        })
    })

    it('issueISSUENUMBER image has correct height after click', () => {
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyimg|disneyimage|disney_img|disney_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalHeight).to.be.eq(303)
        })
      cy.get('button[data-cy=disneybtn]').should('have.text', 'RANDOMEXPRESSIONCambiar a Mickey|Ver a Mickey|Mostrar Mickey|Cambiar al ratón|Ver al ratón|Mostrar al ratónEND');
      cy.get('button[data-cy=disneybtn]').click()
      cy.get('img[data-cy=RANDOMEXPRESSIONdisneyimg|disneyimage|disney_img|disney_imageEND]').should('be.visible')
        .and(($img) => {
          expect($img[0].naturalHeight).to.be.eq(259)
        })
    })
})
