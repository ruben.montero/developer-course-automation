Una imagen en un estado... ¡a vueltas!

## :books: Resumen

* Crearemos `DisneyAlternator.js`, que permitirá alternar entre la imagen de Donald y la de Mickey
* Lo mostraremos en `MainPage.js`

## Descripción

¿Y si alguien se arrepiente de haber mostrado a Mickey en vez de Donald?

¡Rápido! ¡Creemos un componente que permita _alternarlos_!

## :package: La tarea

**Crea** un nuevo fichero `DisneyAlternator.js` en `src/components/disney/`:

```jsx
import donald from "./donald_duck.png";
import mickey from "./mickey_mouse.png";
import { useState } from "react";

const DisneyAlternator = (props) => {
    const [image, setImage] = useState(donald);

    const buttonClicked = (event) => {
        // TO-DO: Invocar setImage pasando una función
        // Esa función retornará donald o mickey en función
        // de si el estado viejo es uno u otro.
    }

    return <>
        <button data-cy="disneyalternatorbtn" onClick={buttonClicked}>RANDOMEXPRESSIONMostrar otro personaje|Mostrar al otro|Ver otro personaje|Ver otro|Ver al otro|AlternarEND</button>
        <br />
        <img data-cy="RANDOMEXPRESSIONdisneyalternatorimg|disneyalternatorimage|disney_alternator_img|disney_alternator_imageEND" src={image} />
        
      </>
}

export default DisneyAlternator;
```

Se parece bastante al `DisneyImageWithButton.js` del ejercicio anterior, ¿verdad?

Ahora, ¡**muestra** este nuevo componente desde `MainPage.js`!

```jsx
const MainPage = (props) => {
    return <div>
        { /* ... */ }
        <h2 data-cy="issueISSUENUMBERtitle">Ejercicio ISSUENUMBER</h2>
        <DisneyAlternator />
      </div>
}
```

Y, para terminar, **implementa** correctamente la función `buttonClicked` para que alterne `image` entre `donald` y `mickey`.

¿Qué tal? ¿Te encaja el resultado en http://localhost:3000?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
