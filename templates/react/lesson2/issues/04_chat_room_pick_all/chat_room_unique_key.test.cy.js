/// <reference types="cypress" />

describe('chat_room_unique_key', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND', {
          onBeforeLoad (win) {
            cy.stub(win.console, 'error').as("consoleError")
          },
        })
      })
  
    it('issueISSUENUMBER no errors in page', () => {
        cy.wait(300).then(() => {
            cy.get('@consoleError').should('have.callCount', 0);
        });
    })

})
