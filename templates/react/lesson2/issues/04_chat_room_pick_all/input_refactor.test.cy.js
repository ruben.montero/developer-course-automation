/// <reference types="cypress" />

describe('input_refactor', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND', {
        onBeforeLoad (win) {
          cy.stub(win.console, 'log').as("consoleLog")
        },
      })
    })

    const randomTestBody = () => {
        const text = "texto" + Math.floor(Math.random() * 1000);
        cy.get('input[data-cy=input_RANDOMEXPRESSIONchatroom|chat_roomEND]').type(text)
        cy.get('input[data-cy=RANDOMEXPRESSIONchatroom|chat_roomEND_submit]').click();
        cy.get('@consoleLog').should('be.calledWith', "RANDOMEXPRESSIONEnhorabuena, has enviado|¡Enhorabuena! Has enviado|Enhorabuena, enviaste|¡Enhorabuena! EnviasteEND el mensaje:");
        cy.get('@consoleLog').should('be.calledWith', text);
        cy.get('@consoleLog').should('be.calledWith', "RANDOMEXPRESSIONGestionando|Manejando|Gestionando el|Manejando el|Trabajando con elEND mensaje " + text + " desde ChatRoom");

    }

    it('issueISSUENUMBER form input submit produces correct log (rnd test 1)', () => {
        randomTestBody();
    })
    
    it('issueISSUENUMBER form input submit produces correct log (rnd test 2)', () => {
        randomTestBody();
    })
    
    it('issueISSUENUMBER form input submit produces correct log (rnd test 3)', () => {
        randomTestBody();
    })
    
    it('issueISSUENUMBER form input submit produces correct log (rnd test 4)', () => {
        randomTestBody();
    })
    
    it('issueISSUENUMBER form input submit produces correct log (rnd test 5)', () => {
        randomTestBody();
    })
})
