ChatRoom Offline: Lista de mensajes

## :books: Resumen

* Crearemos `ChatRoom.js` que de momento mostrará una lista de mensajes _hardcodeados_
* ¡Se verán en http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND!
* En el proceso, aprenderemos a usar `map`

## Descripción

Un breve repaso:

En JSX sólo podemos escribir instrucciones declarativas. ¡No imperativas! Es decir, _no_ podemos emplear bucles (`for`, `while`...)

```jsx
const EjemploComponente = (props) => {
  
  // ¡ESTO NO SE PUEDE HACER!
  return <ol>Un bucle de tres números: { for (let i = 0; i<3; i++) { <li>{i}</li> } }</ol>
}
```

Por ello, anteriormente, para trabajar con _listas_ en nuestros componentes, hemos _sacado el bucle_ "fuera" del JSX:

```jsx
const EjemploComponente = (props) => {
  
  const lista = []
  for (let i = 0; i<3; i++) {
      lista.push(<li>{i}</li>);
  }
  // ESTO SÍ ES VÁLIDO
  return <ol>{lista}</ol>
}
```

Ha llegado la hora de la verdad: Esta solución no es muy común... Ni muy buena :anguished:

### [`map`](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Array/map)

Usar [`map`](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Array/map) sí es algo típico y más cercano a la filosofía de React.

Este método _se invoca sobre una lista_ y devuelve _la lista transformada_.

Siguiendo con el ejemplo anterior:

```jsx
const EjemploComponente = (props) => {
  
  const lista = [1, 2, 3]
  
  // ¡BUENA SOLUCIÓN, USANDO MAP!
  return <ol>{lista.map(elemento => { return <li>elemento</li> })}</ol>
}
```

Como ves, a `map` sólo hay que _pasarle_ (como parámetro) una función: Dicha función será invocada _para cada elemento_ de la lista, produciendo así, una lista transformada (_mapeada_).

En el ejemplo, queremos convertir:

* `1` a `<li>1</li>`
* `2` a `<li>2</li>`
* `3` a `<li>3</li>`

Por ello, la _función_ que pasamos a `map` es:

```jsx
elemento => { return <li>elemento</li> }
```

¡Adelante! Continuemos con nuestro trabajo para crear una _chatroom offline_.

## :package: La tarea

**Añade** un nuevo componente `src/components/chat_room/ChatRoom.js`.

Dentro de `ChatRoom`, **declara** un estado `messagesList` **así**:

```jsx
    const [messagesList, setMessagesList] = useState([
        'RANDOMEXPRESSIONHola|Primer mensaje|Un saludo|¡Buenos días!|Aquí estamosEND',
        'RANDOMEXPRESSIONVamos a ser los mejores|Vamos a triunfar|Seremos la bombaEND'
    ]);
```

Luego, **escribe** el siguiente `return` para que dicha lista de _strings_ aparezca visualmente como varios `<li>` dentro de un `<RANDOMEXPRESSIONul|olEND>`:

```jsx
    return <>
        <ul data-cy='RANDOMEXPRESSIONchatroom|chat_roomEND_RANDOMEXPRESSIONmsg|message|messagesEND_list'>
            { messagesList.map(string => <li>{string}</li>) }
        </ul>
    </>
```

Para terminar, **añade** el componente `<ChatRoom />` a la `ChatRoomPage.js`.

Verifica que funciona.

¿Ves los dos mensajes en http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

-------------------------------------------------------------

[^1]: Seguramente aparecerá también haciendo referencia a las tablas de ejercicios anteriores. De momento, puedes centrarte sólo en el relativo a la tarea actual; _Check the render method of `SimpleMessagesList...`_
