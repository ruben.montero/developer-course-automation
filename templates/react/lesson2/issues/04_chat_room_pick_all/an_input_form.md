ChatRoom Offline: Entrada de texto

## :books: Resumen

* Escribiremos un componente `Input.js` que contiene un `<form>` y permite al usuario teclear un texto, almacenado en un estado
* Lo mostrará por consola al darle a RANDOMEXPRESSIONEnviar|Aceptar|OkEND
* Añadiremos http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND y mostraremos dicho componente

## Descripción

Nuestras interacciones con el usuario de momento sólo han tenido lugar a través de botones. Pero las entradas de texto son algo muy habitual que, simplemente... ¡No podemos pasar por alto!

[Reaccionar a la entrada de texto del usuario](https://react.dev/learn/reacting-to-input-with-state) es un problema muy interesante en React. ¿Recuerdas el trabajo que nos daba _gestionar un formulario_ desde puro JavaScript? Al hacerlo de manera imperativa, hay que explicar _cada cambio_ que se produce. Manejar _a mano_ el API del DOM del navegador supone una dificultad que crece exponencialmente con la complejidad de la interfaz de usuario (UI):

<img src="react-states/docs/imperative.png" width=350 />

Con React, [plasmamos la interfaz de forma declarativa](https://react.dev/learn/reacting-to-input-with-state#thinking-about-ui-declaratively):

<img src="react-states/docs/declarative.png" width=350 />

## :package: La tarea

**Añade** un nuevo componente `src/components/chat_room/Input.js`:

```jsx
const Input = (props) => {

    return <>
      <form>
        <input type="text" data-cy="input_RANDOMEXPRESSIONchatroom|chat_roomEND" placeholder="RANDOMEXPRESSIONEscribe algo...|Escribe algo|Escribe tu mensaje...|Escribe tu mensaje|Escribe un mensaje...|Escribe un mensajeEND"/>
        <input type="submit" data-cy="RANDOMEXPRESSIONchatroom|chat_roomEND_submit" value="RANDOMEXPRESSIONEnviar|Aceptar|OkEND" />
      </form>
    </>
}

export default Input;
```

Ahora, **crea** un nuevo componente en `src/pages/ChatRoomPage.js` para una nueva página.  De momento, sólo mostrará el `<Input />`:

```jsx
import Input from "../components/chat_room/Input";

const ChatRoomPage = (props) => {
    return <div>
        <Input />
      </div>
}

export default ChatRoomPage;
```

Y, para poder verlo, **añade** en `App.js` la línea `<Route />` necesaria para _mapear_ la ruta `"/RANDOMEXPRESSIONchat_room|chatroomEND"` al componente `<ChatRoomPage />`.

¿Lo has hecho?

¿Puedes ver el campo de texto y el botón desde http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND?

¡Genial! Continuemos.

### Un estado... ¡cómo no!

El _texto_ que el usuario introduce es algo único y relevante para nuestra interfaz. ¡Almacenémoslo en un estado!

**Crea** el siguiente estado en `Input.js`:

```jsx
    const [message, setMessage] = useState("");
```

A continuación, **crea** una función para _cada vez_ que el usuario pulsa una tecla:

```jsx
    const customOnChange = (event) => {
        setMessage(event.target.value);
    }
```

Y **modifica** el elemento `<input>` del `<form>` de la siguiente manera:

```diff
-        <input type="text" data-cy="input_RANDOMEXPRESSIONchatroom|chat_roomEND" placeholder="RANDOMEXPRESSIONEscribe algo...|Escribe algo|Escribe tu mensaje...|Escribe tu mensaje|Escribe un mensaje...|Escribe un mensajeEND"/>
+        <input type="text" data-cy="input_RANDOMEXPRESSIONchatroom|chat_roomEND" placeholder="RANDOMEXPRESSIONEscribe algo...|Escribe algo|Escribe tu mensaje...|Escribe tu mensaje|Escribe un mensaje...|Escribe un mensajeEND" onChange={customOnChange} value={message} />
```

Como ves, hemos añadido `onChange` y `value`:

* `onChange` indica _qué_ método se ejecutará cada vez que _cambia_ el campo (con cada tecla tecleada). En `customOnChange`, `event.target` representa el propio elemento `<input>`. Por ello, `event.target.value` es el texto tecleado.
* `value` se emplea para _enlazar_ el _valor_ del formulario al estado `message`. Así, está _atado_ "bidireccionalmente". Si en el futuro cambiamos el estado (`setMessage()`), entonces se reflejará en la interfaz. Pista: Lo haremos :smiley:

### Y `onSubmit`

Ahora, cuando pulsas RANDOMEXPRESSIONEnviar|Aceptar|OkEND en http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND la página se refresca, ¿verdad?

¡Es el comportamiento por defecto de los formularios!

**Añade**:

```jsx
    const customOnSubmit = (event) => {
        event.preventDefault(); // Cancelar comportamiento por defecto
        console.log("RANDOMEXPRESSIONEnhorabuena, has enviado|¡Enhorabuena! Has enviado|Enhorabuena, enviaste|¡Enhorabuena! EnviasteEND el mensaje:");
        console.log(message);
        setMessage(""); // Vaciar el input
    }
```

Y **asócialo** a `onSubmit` del formulario:

```diff
-      <form>
+      <form onSubmit={customOnSubmit}>
```

¡Enhorabuena! Ya has implementado un comportamiento _customizado_. De momento, no es gran cosa. ¡Sólo un `console.log`!

Pero, ¿puedes verificar que lo ves en http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

-------------------------------------------------------------

[^1]: Seguramente aparecerá también haciendo referencia a las tablas de ejercicios anteriores. De momento, puedes centrarte sólo en el relativo a la tarea actual; _Check the render method of `SimpleMessagesList...`_
