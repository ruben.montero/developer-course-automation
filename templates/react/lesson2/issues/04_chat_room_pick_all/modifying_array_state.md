ChatRoom Offline: Añadir a la lista

## :books: Resumen

* Modificaremos `ChatRoom.js` para que, cuando se ejecute `onNewMessage`, sea concatenado a la lista

## Descripción

¿Recuerdas que has modificado (añadido) en varias ocasiones a listas JavaScript usando [`.push`](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Array/push)?

Pues, ¡no lo debemos usar para modificar un _estado_ React! (Para variables normales sigue siendo igual de útil)

Recuerda que los _estados_ en React no se deben modificar _directamente_. Si usamos un método como `.push`, que _muta_ un _array_... ¡Estamos rompiendo esa regla!

Aquí ves una lista con casos típicos y cómo se deben implementar:

|           | evita (muta el array) | preferido (devuelve un nuevo array) |
| ------------- |:-------------:| - |
| añadir     | `push`, `unshift` | `concat`, `[...arr]` _spread operator_ ([ejemplo](https://es.react.dev/learn/updating-arrays-in-state#adding-to-an-array))
| eliminar   | `pop`, `shift`, `splice` | `filter`, `slice` ([ejemplo](https://es.react.dev/learn/updating-arrays-in-state#removing-from-an-array)) |
| reemplazar | `splice`, `arr[i] = ...` asigna | `map` ([ejemplo](https://es.react.dev/learn/updating-arrays-in-state#replacing-items-in-an-array)) |
| ordenar    | `reverse`, `sort`     | copia el _array_ primero ([ejemplo](https://es.react.dev/learn/updating-arrays-in-state#making-other-changes-to-an-array))

Tienes [más información y ejemplos interactivos en la documentación oficial](https://es.react.dev/learn/updating-arrays-in-state)

## :package: La tarea

En `ChatRoom.js`, **declara** una nueva variable `newObject` en el método que gestiona los nuevos mensajes:

```jsx
    const onNewMessageFunction = (newMessage) => {
        console.log("RANDOMEXPRESSIONGestionando|Manejando|Gestionando el|Manejando el|Trabajando con elEND mensaje " + newMessage + " desde ChatRoom");
        const newObject = {
            message: newMessage,
            uniqueId: Date.now() // Nos vale como identificador
        }
    }
```

Como ves, es un nuevo _objeto_ que queremos concatenar a `messagesList`.

Para hacerlo, **implementa** a continuación cualquiera de las dos formas que está indicadas arriba (ambas valen):

```jsx
        // Opción A
        setMessagesList(messagesList.concat(newObject));
```

```jsx
        // Opción B
        setMessagesList([...messagesList, newObject]);
```

¡Listo! Así de fácil, tu _chatroom offline_ debería estar funcionando en http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND.

En el futuro, trabajaremos con React conectándonos a un servidor y viendo cómo se manejan peticiones que podrían, por ejemplo, hacer esta _chatroom offline_ una _chatroom real_.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
