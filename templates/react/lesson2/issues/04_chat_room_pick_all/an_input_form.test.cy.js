/// <reference types="cypress" />

describe('an_input_form', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND', {
        onBeforeLoad (win) {
          cy.stub(win.console, 'log').as("consoleLog")
        },
      })
    })
  
    it('issueISSUENUMBER form exists', () => {
        cy.get('form').should('be.visible');
    })

    it('issueISSUENUMBER form input text exists', () => {
        cy.get('input[data-cy=input_RANDOMEXPRESSIONchatroom|chat_roomEND]').should('be.visible');
    })

    it('issueISSUENUMBER form input submit exists', () => {
        cy.get('input[data-cy=RANDOMEXPRESSIONchatroom|chat_roomEND_submit]').should('be.visible');
    })

    it('issueISSUENUMBER form input has correct placeholder', () => {
        cy.get('input[data-cy=input_RANDOMEXPRESSIONchatroom|chat_roomEND]')
          .invoke('attr', 'placeholder')
          .should('eq', 'RANDOMEXPRESSIONEscribe algo...|Escribe algo|Escribe tu mensaje...|Escribe tu mensaje|Escribe un mensaje...|Escribe un mensajeEND')
    })
  
    it('issueISSUENUMBER form input submit has correct text', () => {
        cy.get('input[data-cy=RANDOMEXPRESSIONchatroom|chat_roomEND_submit]')
        .invoke('attr', 'value')
        .should('eq', 'RANDOMEXPRESSIONEnviar|Aceptar|OkEND')
    })

    it('issueISSUENUMBER form input submit produces correct log once', () => {
        const text = "texto" + Math.floor(Math.random() * 10);
        cy.get('input[data-cy=input_RANDOMEXPRESSIONchatroom|chat_roomEND]').type(text)
        cy.get('input[data-cy=RANDOMEXPRESSIONchatroom|chat_roomEND_submit]').click();
        cy.get('@consoleLog').should('be.calledWith', "RANDOMEXPRESSIONEnhorabuena, has enviado|¡Enhorabuena! Has enviado|Enhorabuena, enviaste|¡Enhorabuena! EnviasteEND el mensaje:");
        cy.get('@consoleLog').should('be.calledWith', text);
    })
})
