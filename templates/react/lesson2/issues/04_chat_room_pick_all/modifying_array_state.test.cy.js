/// <reference types="cypress" />

describe('modifying_array_state', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND', {
        onBeforeLoad (win) {
          cy.stub(win.console, 'log').as("consoleLog")
        },
      })
    })

    const randomTestBody = (timesItWasExecuted) => {
        const text = "texto" + Math.floor(Math.random() * 1000);
        cy.get('input[data-cy=input_RANDOMEXPRESSIONchatroom|chat_roomEND]').type(text)
        cy.get('input[data-cy=RANDOMEXPRESSIONchatroom|chat_roomEND_submit]').click();
        it('issueISSUENUMBER new li is correct', () => {
            cy.get('ul[data-cy=RANDOMEXPRESSIONchatroom|chat_roomEND_RANDOMEXPRESSIONmsg|message|messagesEND_list]')
            .find('li')
            .should('have.length', 2 + timesItWasExecuted); // Because there are 2 default messages
        })
        it('issueISSUENUMBER new li is correct', () => {
            cy.get('ul[data-cy=RANDOMEXPRESSIONchatroom|chat_roomEND_RANDOMEXPRESSIONmsg|message|messagesEND_list]')
            .find('li')
            .last() 
            .should('have.text', text);
        })
    }

    it('issueISSUENUMBER form input submit correctly appends li (rnd test 1)', () => {
        randomTestBody(1);
    })
    
    it('issueISSUENUMBER form input submit correctly appends li (rnd test 2)', () => {
        for (let i = 1; i <= 2; i++) {
            randomTestBody(i);
        }
    })
    
    it('issueISSUENUMBER form input submit correctly appends li (rnd test 3)', () => {
        for (let i = 1; i <= 3; i++) {
            randomTestBody(i);
        }
    })
    
    it('issueISSUENUMBER form input submit correctly appends li (rnd test 4)', () => {
        for (let i = 1; i <= 4; i++) {
            randomTestBody(i);
        }
    })
    
    it('issueISSUENUMBER form input submit correctly appends li (rnd test 7)', () => {
        for (let i = 1; i <= 7; i++) {
            randomTestBody(i);
        }
    })

    it('issueISSUENUMBER form input submit correctly appends li (rnd test 10)', () => {
        for (let i = 1; i <= 10; i++) {
            randomTestBody(i);
        }
    })

    it('issueISSUENUMBER form input submit correctly appends li (rnd test 11)', () => {
        for (let i = 1; i <= 11; i++) {
            randomTestBody(i);
        }
    })
})
