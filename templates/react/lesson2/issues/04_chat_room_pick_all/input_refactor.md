ChatRoom Offline: prop onClick

## :books: Resumen

* Modificaremos `ChatRoom.js` para que `Input.js` esté _dentro_
* De momento no funcionará distinto, pero será el primer paso para que los nuevos mensajes aparezcan en la lista

## Descripción

Ahora mismo, tenemos esto:

```mermaid
graph TD
    A[ChatRoomPage]-->B[ChatRoom]
    A[ChatRoomPage]-->C[Input]
```

¿Cómo podemos hacer que los mensajes tecleados en `<Input />` se vean en la `<ChatRoom />`?

Optaremos por esta solución:

```mermaid
graph TD
    A[ChatRoomPage]-->B[ChatRoom]
    B[ChatRoom]-->C[Input]
```

## :package: La tarea

En `ChatRoomPage.js`, **elimina** `<Input />`.

```diff
-import Input from "../components/chat_room/Input";
import ChatRoom from "../components/chat_room/ChatRoom";

const ChatRoomPage = (props) => {
    return <div>
        <ChatRoom />
-       <Input />
      </div>
}
```

Y **añádelo** a `ChatRoom.js`:

```diff
    return <>
        <ul data-cy='RANDOMEXPRESSIONchatroom|chat_roomEND_RANDOMEXPRESSIONmsg|message|messagesEND_list'>
            { messagesList.map(obj => <li key={obj.uniqueId}>{obj.message}</li>) }
        </ul>
+       <Input />
    </>
```

### El problema

El nuevo mensaje que escribe el usuario _todavía_ reside en `message` dentro de `Input.js`.

Si queremos añadirlo visualmente a la lista... ¡Tendremos que conseguir ese mensaje en `ChatRoom.js`!

### La solución

**Crea** una nueva función en `ChatRoom.js`:

```jsx
    const onNewMessageFunction = () => {
    
    }
```

**Pásala** a `<Input />` a través de una _prop_:

```diff
-        <Input />
+        <Input onNewMessage={onNewMessageFunction}/>
```

Y, ahora, en `Input.js`, ¡lo has recibido! Es una función invocable, así que... **invócala**:

```diff
    const customOnSubmit = (event) => {
        event.preventDefault(); // Cancelar comportamiento por defecto
+       props.onNewMessage();
        console.log("Enhorabuena, has enviado el mensaje:");
        console.log(newMessage);
        setMessage(""); // Vaciar el input
    }
```

¿Qué te parece?

```mermaid
graph TD
    A[ChatRoomPage]-->B[ChatRoom]
    B[ChatRoom]-->|prop: onNewMessage|C[Input]
    C-->|"onNewMessage()"|B
```

### La solución... Completa

En `onNewMessageFunction` de `ChatRoom.js` _todavía_ no disponemos del nuevo mensaje.

**Decláralo** como un parámetro y **añade** un `console.log`:

```diff
-    const onNewMessageFunction = () => {
+    const onNewMessageFunction = (newMessage) => {
+        console.log("RANDOMEXPRESSIONGestionando|Manejando|Gestionando el|Manejando el|Trabajando con elEND mensaje " + newMessage + " desde ChatRoom");
     }
```

Y, coherentemente, **pasa** el parámetro en `Input.js`:

```diff
    const customOnSubmit = (event) => {
        event.preventDefault(); // Cancelar comportamiento por defecto
-       props.onNewMessage();
+       props.onNewMessage(message);
        console.log("Enhorabuena, has enviado el mensaje:");
        console.log(message);
        setMessage(""); // Vaciar el input
    }
```

¡Ahora está completo!

```mermaid
graph TD
    A[ChatRoomPage]-->B[ChatRoom]
    B[ChatRoom]-->|prop: onNewMessage|C[Input]
    C-->|"onNewMessage(message)"|B
```

Puedes verificar en http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND que el nuevo _log_ aparece.

¡Nuestro camino está allanado para terminar en la siguiente tarea!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
