ChatRoom Offline: Warning - unique key attribute

## :books: Resumen

* Modificaremos `ChatRoom.js` para evitar un _warning_ que salta, y entenderemos por qué

## Descripción

No todo lo que hacemos es perfecto a la primera. ¡Sería raro!

Debemos _verificar_ que nuestro trabajo va bien cada pocos pasos.

Por ejemplo, en http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND haz _click derecho_ y selecciona Inspeccionar > Consola. ¿Ves una advertencia como esta?

```
> Warning: Each child in a list should have a unique "key" prop.

Check the render method of `ChatRoom`. See https://reactjs.org/link/warning-keys for more information.
li
ChatRoom@http://localhost:3000/static/js/bundle.js:1002:90
div
ChatRoomPage
RenderedRoute@http://localhost:3000/static/js/bundle.js:40365:7
Routes@http://localhost:3000/static/js/bundle.js:41055:7
Router@http://localhost:3000/static/js/bundle.js:40994:7
BrowserRouter@http://localhost:3000/static/js/bundle.js:38944:7
App
```

### ¿Qué está pasando?

Como ya sabes, en React, cuando los estados cambian la interfaz _reacciona_ automáticamente.

Para transicionar adecuadamente del estado anterior al nuevo estado, React necesita que cada elemento de una lista (`<li>`, `<tr>`,...) esté [identificado con una `key`](https://reactjs.org/tutorial/tutorial.html#picking-a-key).

¡Arreglemos nuestro _bug_!

## :package: La tarea

En `ChatRoom.js`, **convierte** el _array_ de mensajes _(strings)_ a un _array_ de objetos.

Cada objeto tendrá dos valores: `message` _(string)_ y `uniqueId` _(integer)_. **Así**:

```diff
    const [messagesList, setMessagesList] = useState([
-        'RANDOMEXPRESSIONHola|Primer mensaje|Un saludo|¡Buenos días!|Aquí estamosEND',
-        'RANDOMEXPRESSIONVamos a ser los mejores|Vamos a triunfar|Seremos la bombaEND'
+        {
+            message: 'RANDOMEXPRESSIONHola|Primer mensaje|Un saludo|¡Buenos días!|Aquí estamosEND',
+            uniqueId: 1
+        },
+        {
+            message: 'RANDOMEXPRESSIONVamos a ser los mejores|Vamos a triunfar|Seremos la bombaEND',
+            uniqueId: 2
+        }
    ]);
```

Ahora, como `messagesList` ya no es una lista de _strings_ sino una lista de objetos, **modifica** el `map` coherentemente:

```diff
-            { messagesList.map(string => <li>{string}</li>) }
+            { messagesList.map(obj => <li>{obj.message}</li>) }
```

_(En realidad no es obligatorio sustituir `string` por `obj`, ya que podemos bautizar ese parámetro a nuestro gusto. ¡Pero `obj` es más adecuado ahora!)_

Y, a cada `<li>`, dale una `key` que corresponda con `uniqueId`. **Así**:

```diff
-            { messagesList.map(obj => <li>{obj.message}</li>) }
+            { messagesList.map(obj => <li key={obj.uniqueId}>{obj.message}</li>) }
```

¡Ahora ya no debe aparecer ningún _warning_ en la consola en http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
