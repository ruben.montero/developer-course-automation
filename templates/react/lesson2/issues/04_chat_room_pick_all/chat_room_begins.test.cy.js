/// <reference types="cypress" />

describe('chat_room_begins', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/RANDOMEXPRESSIONchat_room|chatroomEND')
    })
  
    it('issueISSUENUMBER ul exists', () => {
        cy.get('ul[data-cy=RANDOMEXPRESSIONchatroom|chat_roomEND_RANDOMEXPRESSIONmsg|message|messagesEND_list]').should('be.visible');
    })

    it('issueISSUENUMBER ul contains 2 li', () => {
        cy.get('ul[data-cy=RANDOMEXPRESSIONchatroom|chat_roomEND_RANDOMEXPRESSIONmsg|message|messagesEND_list]')
        .find('li')
        .should('have.length', 2);
    })

    it('issueISSUENUMBER first li is correct', () => {
        cy.get('ul[data-cy=RANDOMEXPRESSIONchatroom|chat_roomEND_RANDOMEXPRESSIONmsg|message|messagesEND_list]')
        .find('li')
        .eq(0)
        .should('have.text', 'RANDOMEXPRESSIONHola|Primer mensaje|Un saludo|¡Buenos días!|Aquí estamosEND');
    })

    it('issueISSUENUMBER second li is correct', () => {
        cy.get('ul[data-cy=RANDOMEXPRESSIONchatroom|chat_roomEND_RANDOMEXPRESSIONmsg|message|messagesEND_list]')
        .find('li')
        .eq(1)
        .should('have.text', 'RANDOMEXPRESSIONVamos a ser los mejores|Vamos a triunfar|Seremos la bombaEND');
    })
})
