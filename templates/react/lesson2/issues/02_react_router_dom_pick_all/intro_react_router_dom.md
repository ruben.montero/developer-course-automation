¡Varias páginas! (react-router-dom)

## :books: Resumen

* Instalaremos la librería `react-router-dom`
* Crearemos un `Router` y mostraremos la página principal en localhost:3000/ (como hasta ahora)

## Descripción

[React](https://es.react.dev/):

> te permite construir interfaces de usuario a partir de piezas individuales llamadas componentes

Eso está muy bien, pero siempre mostramos una página en http://localhost:3000

¿Se pueden servir varias páginas, por ejemplo, desde http://localhost:3000 y también desde http://localhost:3000/perfil?

¡_Sí_ se puede!

Hay que usar la librería...

### [React Router](https://reactrouter.com/en/main)

Específicamente, esta librería provee distintos _routers_, que son las piezas de código encargadas de que distintos _puntos de entrada_ (para una web, distintas URLs[^1]) sean gestionadas por componentes distintos.

¡Adelante! Desde un terminal (cmd.exe) posiciónate con `cd` en la carpeta `react-states/` de tu repositorio. Allí, **ejecuta**:

```
npm install react-router-dom
```

Esto _incluirá_ la dependencia a `react-router-dom` en el fichero `package.json` de tu proyecto, y además, la instalará en `node_modules/`.

La carpeta `node_modules/` no es _importante_, pues tú no eres el autor. Basta con hacer `npm install` para _re-generarla_ a partir de `package.json`. ¡`package.json` es importante!

Si haces:

```
git status
```

...verás los ficheros con _cambios en sucio_ en tu repositorio. Seguramente, algo como:

```
C:\Users\Developer\Desktop\react-states>git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   package-lock.json
        modified:   package.json

no changes added to commit (use "git add" and/or "git commit -a")
```

Prueba ahora a lanzar:

```
git diff package.json
```

Ello nos mostrará, específicamente, _qué cambios en sucio_ hay en el fichero `package.json`. 

¿La salida se parece a lo siguiente?

```
C:\Users\Developer\Desktop\react-states>git diff package.json
diff --git a/package.json b/package.json
index 2e33086..8587837 100644
--- a/package.json
+++ b/package.json
@@ -7,6 +7,7 @@
     "@testing-library/user-event": "^13.5.0",
     "react": "^18.1.0",
     "react-dom": "^18.1.0",
+    "react-router-dom": "^6.22.3",
     "react-scripts": "5.0.1",
     "web-vitals": "^2.1.4"
   },
```

Fíjate que se ha añadido la línea `"react-router-dom": "^6.22.3",`. Esta línea es _importante_, pues hemos declarado una nueva _dependencia_. Con esa línea, cualquier persona que clone nuestro repositorio y haga `npm install` obtendrá las librerías necesarias. Sin ella... ¡no! (Y el código que vamos a hacer a continuación no funcionará)

## :package: La tarea

Después de **instalar** `react-router-dom` como se indica arriba, verifica que en `App.js` tienes un código como este:

```jsx
import MainPage from "./pages/MainPage";

function App() {
  return (
    <MainPage />
  );
}
```

**Reemplázalo** por lo siguiente:

```jsx
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./pages/MainPage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<MainPage />}></Route>
      </Routes>
    </BrowserRouter>
  );
}
```

**Verifica** ahora cómo se ve http://localhost:3000. Todo está igual, ¿verdad?

Pero, _por debajo_, has cambiado tu código. Ahora, gracias a la librería `react-router-dom` has indicado _explícitamente_ que la URL con ruta `/` (o sea, http://localhost:3000/) debe pintarse con el componente `<MainPage />`.

En la siguiente tarea crearemos otra _ruta_ y entenderás en profundidad el significado de este cambio.

### :trophy: Por último

Aunque no has modificado nada _funcional_ en tu página, ¡has llevado a cabo un _refactor_ importante!

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: ¿Has escuchado hablar de React Native? Lo que aprendes sobre React también se usa para desarrollar aplicaciones móviles con React Native. Los _routers_, en React Native, tienen un significado ligeramente distinto, pues no gobiernan páginas diferentes de una web, sino _pantallas_ de una _app_.
