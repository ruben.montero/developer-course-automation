/// <reference types="cypress" />

describe('not_found', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/ejemploNoExiste')
    })
    
    it('not found contains title', () => {
        cy.get('hRANDOMNUMBER1-4END[data-cy=not_found_header]')
        .should('have.text', 'RANDOMEXPRESSIONAquí no está|No encontrado|Not found|Not here|Sorry!|¡Lo siento!END');
    })
    
    it('not found contains body', () => {
        cy.get('p[data-cy=not_found_body]')
        .should('have.text', 'RANDOMEXPRESSION¡Aquí no está!|¡Eso que buscas no está!|Estos no son los androides que buscas|Esa página no existe|Página que no existe|¡Vaya! Parece que te has perdido|Uh-oh! Te has perdidaEND');
    })
})
