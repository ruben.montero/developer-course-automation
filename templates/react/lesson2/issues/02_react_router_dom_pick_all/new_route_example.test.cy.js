/// <reference types="cypress" />

describe('new_route_example', () => {
  
    it('change from initial  issue is still there', () => {
        cy.visit('http://localhost:3000/')
        cy.get('h1[data-cy=issue1Title]')
        .should('have.text', 'RANDOMEXPRESSIONHola, developer|¡Hola, developer!|Hola :D|Arrancando motores|Arrancando motores, developer...|Encendiendo, developer|Calentando motores, developer...END');
    })
    
    it('new route is defined', () => {
        cy.visit('http://localhost:3000/RANDOMEXPRESSIONejemplo|unEjemplo|paginaEjemplo|paginaDeEjemplo|ejemplilloEND')
    })
    
    it('new route contains example text', () => {
        cy.visit('http://localhost:3000/RANDOMEXPRESSIONejemplo|unEjemplo|paginaEjemplo|paginaDeEjemplo|ejemplilloEND')
        cy.get('h2[data-cy=example_page_header]')
        .should('have.text', '¡Esta es RANDOMEXPRESSIONuna página de ejemplo|una página con un ejemplo|una página ejemplar|una página digna de ser ejemplo|una página digna de servir de ejemplo|una página que puede servir de ejemploEND!');
    })
})
