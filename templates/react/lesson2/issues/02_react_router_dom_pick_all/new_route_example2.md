Pues añadamos.. ¡otra página más!

## :books: Resumen

* Crearemos `PaginaDeEjemplo2.js` y la mostraremos desde http://localhost:3000/RANDOMEXPRESSIONotroEjemplo|nuevoEjemplo|otraPaginaEjemplo|otraPaginaDeEjemplo|otroEjemplilloEND

## Descripción

¡A practicar con una nueva `<Route>`!

## :package: La tarea

**Crea** un nuevo fichero en `src/pages/` que sea `PaginaDeEjemplo2.js`. **Debe** tener un elemento raíz `<div>` y dentro, mostrar:

* Un `<hRANDOMNUMBER1-4END>` con `data-cy='example2header'`. Contendrá el texto: `RANDOMEXPRESSIONEjemplo 2|Ejemplo dos|Segundo ejemploEND`
* Un `<p>` con `data-cy='example2body'`. Contendrá el texto: `Este es un ejemplo de una página en un proyecto React que contiene varias rutas. RANDOMEXPRESSIONSi tienes curiosidad|Si quieres indagar más|Si te pica la curiosidad|Si quieres leer sobre ello|Si te genera curiosidadEND, pincha:`
* Un `<a>` con `href='https://www.freecodecamp.org/espanol/news/aprende-sobre-react-router-en-5-minutos-tutorial-para-principiantes/'`. Tendrá también `data-cy='example2link'`. Contendrá el texto: `RANDOMEXPRESSIONAquí|AQUÍ|¡Aquí!|¡AQUÍ!END`

Luego, **modifica** `App.js` para que, desde http://localhost:3000/RANDOMEXPRESSIONotroEjemplo|nuevoEjemplo|otraPaginaEjemplo|otraPaginaDeEjemplo|otroEjemplilloEND se muestre el componente `PaginaDeEjemplo2.js`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
