Pues añadamos.. ¡otra página!

## :books: Resumen

* Crearemos un nuevo sencillo componente: `PaginaDeEjemplo.js`
* Aprovechando que hemos añadido un `Router`, crearemos la ruta a http://localhost:3000/RANDOMEXPRESSIONejemplo|unEjemplo|paginaEjemplo|paginaDeEjemplo|ejemplilloEND
* En esa nueva _página_, mostraremos dicho componente

## Descripción

Manos en la masa y, directamente, a trabajar con el cemento que hemos dejado preparado la tarea anterior.

## :package: La tarea

**Crea** un nuevo fichero en `src/pages/` que sea `PaginaDeEjemplo.js`. Hazlo **así**:

```jsx
const PaginaDeEjemplo = (props) => {
    return <div>
        <h2 data-cy="example_page_header">¡Esta es RANDOMEXPRESSIONuna página de ejemplo|una página con un ejemplo|una página ejemplar|una página digna de ser ejemplo|una página digna de servir de ejemplo|una página que puede servir de ejemploEND!</h2>
    </div>
}

export default PaginaDeEjemplo;
```

Y, ahora, **muestra** dicho componente bajo la ruta `/RANDOMEXPRESSIONejemplo|unEjemplo|paginaEjemplo|paginaDeEjemplo|ejemplilloEND` de tu web.

Para ello, **añade** la siguiente `<Route>` a tu `App.js`:

```diff
+import PaginaDeEjemplo from "./pages/PaginaDeEjemplo";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<MainPage />}></Route>
+        <Route path="/RANDOMEXPRESSIONejemplo|unEjemplo|paginaEjemplo|paginaDeEjemplo|ejemplilloEND" element={<PaginaDeEjemplo />}></Route>
      </Routes>
    </BrowserRouter>
  );
}
```

¿Ves el resultado correctamente desde http://localhost:3000/RANDOMEXPRESSIONejemplo|unEjemplo|paginaEjemplo|paginaDeEjemplo|ejemplilloEND?

¡Enhorabuena! Has empezado a crear páginas con _varias páginas_.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
