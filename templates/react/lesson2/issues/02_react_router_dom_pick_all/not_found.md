No encontrado :(

## :books: Resumen

* Crearemos `NoEncontrado.js`
* Se mostrará en nuestra página siempre que el usuario vaya a una URL desconocida

## Descripción

¿Has probado a visitar una URL no contemplada en tu `<Routes>`? Si lo has hecho, enhorabuena, tienes sangre de _QA tester_.

Si no lo has hecho, navega a http://localhost:3000/estoNoExiste. ¿Qué ves? ¿Está en blanco?

¡Claro! ¡Ninguna `<Route>` contempla esa página!

## :package: La tarea

**Crea** un nuevo fichero en `src/pages/` que sea `NoEncontrado.js`. **Debe** tener un elemento raíz `<div>` y dentro, mostrar:

* Un `<hRANDOMNUMBER1-4END>` con `data-cy='not_found_header'`. Contendrá el texto: `RANDOMEXPRESSIONAquí no está|No encontrado|Not found|Not here|Sorry!|¡Lo siento!END`
* Un `<p>` con `data-cy='not_found_body'`. Contendrá el texto: `RANDOMEXPRESSION¡Aquí no está!|¡Eso que buscas no está!|Estos no son los androides que buscas|Esa página no existe|Página que no existe|¡Vaya! Parece que te has perdido|Uh-oh! Te has perdidaEND`

A continuación, **añade** la siguiente `<Route>` a tu `App.js`:

```jsx
  <Route path="*" element={<NoEncontrado />}></Route>
```

_(No olvides el `import` necesario)_

¿Qué ves ahora desde http://localhost:3000/estoNoExiste?

¿Comprendes que el _wildcard_ (`*`) sirve para _matchear_ cualquier ruta?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
