/// <reference types="cypress" />

describe('new_route_example2', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/RANDOMEXPRESSIONotroEjemplo|nuevoEjemplo|otraPaginaEjemplo|otraPaginaDeEjemplo|otroEjemplilloEND')
    })
    
    it('new route is defined', () => {
        
    })
    
    it('new route contains title', () => {
        cy.get('hRANDOMNUMBER1-4END[data-cy=example2header]')
        .should('have.text', 'RANDOMEXPRESSIONEjemplo 2|Ejemplo dos|Segundo ejemploEND');
    })
    
    it('new route contains body', () => {
        cy.get('p[data-cy=example2body]')
        .should('have.text', 'Este es un ejemplo de una página en un proyecto React que contiene varias rutas. RANDOMEXPRESSIONSi tienes curiosidad|Si quieres indagar más|Si te pica la curiosidad|Si quieres leer sobre ello|Si te genera curiosidadEND, pincha:');
    })
    
    it('new route contains link', () => {
        cy.get('a[data-cy=example2link]')
        .should('have.text', 'RANDOMEXPRESSIONAquí|AQUÍ|¡Aquí!|¡AQUÍ!END');
    })
})
