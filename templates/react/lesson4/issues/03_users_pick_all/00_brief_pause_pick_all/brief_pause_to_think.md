Meditemos...

## :books: Resumen

* Meditaremos junto a la hoguera :fire: durante un rato para buscar respuestas a la pregunta: ¿Cómo añadimos usuarios y autenticación?

## Descripción

¿Cómo permitir a los usuarios subir datos a nuestro `react-stackoverrun`?

En el proyecto anterior, `react-dashboards`, permitíamos la publicación _anónima_ de preguntas y respuestas. ¿Cómo pasamos del _anonimato_ a _usuarios registrados_? ¿Cómo se _loguea_ un usuario?

¿Los usuarios deben almacenarse en la base de datos?

Desde el punto de vista del servidor de base de datos (el lado del API REST), ¿cómo te fiarías de que una _petición_ es auténtica? ¿Cómo permitirías a los usuarios _autenticarse_ y realizar _acciones_?

¿Te suena el concepto de _sesión_?

¿_Cómo_ podríamos implementarla?

**Meditemos** sobre esto un rato. A veces es bueno, simplemente, pararse a reflexionar.

<img src="react-stackoverrun/react-frontend/docs/firelink.png" width=550 />

### :trophy: Por último

No hagas nada más.
