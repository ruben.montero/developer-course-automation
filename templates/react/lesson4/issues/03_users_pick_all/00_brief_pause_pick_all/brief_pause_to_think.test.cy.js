/// <reference types="cypress" />

describe('a_brief_pause_to_think', () => {

  it('issueISSUENUMBER - user has thoughfully considered the issue description', () => {
    // We can't know for sure if the developer has been thinking
    // about user authentication. But we'll have some TRUST! :-)
    const developerIsAGoodPerson = true;
    expect(developerIsAGoodPerson).to.be.true;
  })
})
