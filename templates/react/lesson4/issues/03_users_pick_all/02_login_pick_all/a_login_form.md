Un formulario de login bonito

## :books: Resumen

* Crearemos `UserLogin.js`
* Añadiremos un formulario de login con 2 campos: _Nombre de usuario_ y _Contraseña_, enlazados a sus respectivos estados
* Completaremos el formulario con algo de CSS. Todavía _no_ será funcional

## Descripción

Esta tarea es muy similar a la creación de la página de registro.

Ahora, implementaremos una pantalla de _login_ que _todavía no será funcional_.

## :package: La tarea

**Crea** una nueva pantalla `src/screens/user/login/UserLogin.js`.

En `UserLogin.js`, **añade** los siguientes estados:

```jsx
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
```

En la parte visual del componente (`return`), **añade** un `<div>` principal, y dentro:


* Una etiqueta `<h1 data-cy=RANDOMEXPRESSIONpageHeader|headerEND>` que muestre `Inicia sesión`
* Un `<form data-cy=RANDOMEXPRESSIONloginForm|userLoginForm|newSessionFormEND> que contenga:
  * Un primer `<input>` con:
    * `data-cy=inputUsername`
    * `type=text`
    * `onChange={onChangeUsername}` _(La escribiremos a continuación)_
    * `placeholder='RANDOMEXPRESSIONmarco.polo|the.john.doe|the.jane.doe|new.user|my.nameEND'`
    * `value={username}`
  * Después, una etiqueta `<br />`
  * Un segundo `<input>`:
    * `data-cy=inputPassword`
    * `type=password`
    * `onChange={onChangePassword}` _(La escribiremos a continuación)_
    * `placeholder='Contraseña'`
    * `value={password}`
  * Después, una etiqueta `<br />`
  * Un último `<input>`:
    * `data-cy=inputSubmit`
    * `type=submit`
    * `value='RANDOMEXPRESSIONLogin|Iniciar sesión|AutenticarseEND'`

A continuación, **añade** las funciones necesarias:

```jsx
  const onChangeUsername = (e) => {
    setUsername(e.target.value);
  }
  
  const onChangePassword = (e) => {
    setPassword(e.target.value);
  }
```

### ¡Veamos nuestro formulario!

**Añade** en `App.js`:

```jsx
      <Route path='/login' element={<UserLogin/>}/>
```

Y ahora ya estará visible la página http://localhost:3000/login

¿Se ve bien?

### Un poco de CSS

**Crea** un nuevo fichero `src/screens/user/login/UserLogin.css` con el siguiente CSS:

```css
.loginHeader, .loginForm {
  padding-top: RANDOMNUMBER30-50ENDpx;
  text-align: center;
}

.loginFormInput {
  height: RANDOMNUMBER45-50ENDpx;
  background-color: rgba(57, 57, 57, 0.0RANDOMNUMBER5-9END);
  border: none;
  border-radius: RANDOMNUMBER10-15ENDpx;
  margin: 5px;
  padding: 0px RANDOMNUMBER20-25ENDpx;
}

.loginFormInput:hover {
  background-color: rgba(57, 57, 57, 0.20);
}
```

Para terminar, en `UserLogin.js` **añade** el `import`:

```jsx
import ./UserLogin.css
```

Y **añade** también los `className` necesarios:

* `<h1>` :arrow\_right:  `className="loginHeader"`
* `<form>` :arrow\_right:  `className="loginForm"`
* `<input>` _(los 4)_ :arrow\_right:  `className="loginFormInput`

¿Qué tal se ve http://localhost:3000/login ahora?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_. 

