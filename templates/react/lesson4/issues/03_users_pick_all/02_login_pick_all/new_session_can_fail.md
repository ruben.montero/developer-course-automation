Loguearse puede ir mal

## :books: Resumen

* Añadiremos dos `<p>` a `UserLogin.js` indicando que el usuario no existe o la contraseña es incorrecta
* Los mostraremos en función de si falla la petición de _login_

## Descripción

Ya que hemos mencionado que se deben controlar los errores en nuestras peticiones HTTP... ¡Hagamos eco de estas buenas prácticas!

## :package: La tarea

**Añade** esta clase a `UserLogin.css`:

```css
.loginUserFeedback {
  color: rgb(RANDOMNUMBER150-170END, 60, 60);
  text-align: center;
  font-weight: RANDOMNUMBER700-800END;
  font-size: RANDOMNUMBER15-20ENDpx;
}
```

Luego, en `UserLogin.js`, **añade** estos dos estados:

```jsx
const [errorWrongPasswordHidden, setErrorWrongPasswordHidden] = useState(true);
const [errorUserNotFoundHidden, setErrorUserNotFoundHidden] = useState(true);
```

y **añade** estos dos `<p>` ubicados _encima_ del `<form>`

* `<p data-cy='errorWrongPassword' className='loginUserFeedback' hidden={errorWrongPasswordHidden}>`. Mostrará `RANDOMEXPRESSIONLa contraseña es incorrecta|Contraseña incorrecta|401 - Contraseña incorrecta|Parece que la contraseña introducida no es válida|Contraseña introducida inválidaEND`
* `<p data-cy='errorUserNotFound' className='loginUserFeedback' hidden={errorUserNotFoundHidden}>`. Mostrará `RANDOMEXPRESSIONEse usuario no existe|No existe un usuario con ese nombre|No se ha encontrado una cuenta de usuario para ese nombreEND`

Y luego, **añade** `.catch()` a la petición _login_ de `axios` para que:

* Si se recibe un código de error HTTP 401, se muestre el texto de _RANDOMEXPRESSIONLa contraseña es incorrecta|Contraseña incorrecta|401 - Contraseña incorrecta|Parece que la contraseña introducida no es válida|Contraseña introducida inválidaEND_ (`setErrorWrongPasswordHidden(false);`)
* Si se recibe un código de error HTTP 404, se muestre el texto de _RANDOMEXPRESSIONEse usuario no existe|No existe un usuario con ese nombre|No se ha encontrado una cuenta de usuario para ese nombreEND_ (`setErrorUserNotFoundHidden(false);`)

**Verifica** estos escenarios en http://localhost:3000/login

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_. 
