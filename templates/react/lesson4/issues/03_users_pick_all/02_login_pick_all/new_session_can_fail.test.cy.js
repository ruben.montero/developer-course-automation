describe('new_session_can_fail', () => {
  const randomOkName = "okName" + parseInt(Math.random()*1000);
  const randomKoName = "koName" + parseInt(Math.random()*1000);
  const randomPass = 'a' + parseInt(Math.random()*1000);
  const randomIncorrectPass = 'b' + parseInt(Math.random()*1000);
  const randomToken = 'c' + parseInt(Math.random()*1000);
  const randomSessId = 'd' + parseInt(Math.random()*1000);

  beforeEach(() => {
	cy.intercept('GET', '**/api/v2/dashboards', (req) => {
      req.reply({statusCode: 200, fixture: 'dashboardsList_v2.json'})
    })
    cy.intercept('POST', '**/api/v2/sessions', (req) => {
      const { body } = req
      if (
        body.hasOwnProperty("username") &&
        body.hasOwnProperty("password")
      ) {
        if (
          (body.username == randomOkName) &&
          (body.password == randomPass)
         ) {
            req.reply(200, {created: true, session_id: randomSessId, session_token: randomToken}, [])
          } else if (body.username == randomKoName) {
            req.reply({statusCode: 404, fixture: 'error.json'})
          } else if (body.password == randomIncorrectPass) {
            req.reply({statusCode: 401, fixture: 'error.json'})
          } else {
            // incorrect request according to typed characters
            throw 'Incorrect request'
          }
      } else {
        // incorrect request due to missing parameters
            throw 'Incorrect request'
      }
    }).as("onePost")
    cy.visit('http://localhost:3000/login')
  })

  it('issueISSUENUMBER - login response feedback paragraphs exist with correct text, CSS, and initially hidden', () => {
    cy.get('p[data-cy=errorWrongPassword]')
    .should('have.text', 'RANDOMEXPRESSIONLa contraseña es incorrecta|Contraseña incorrecta|401 - Contraseña incorrecta|Parece que la contraseña introducida no es válida|Contraseña introducida inválidaEND')
    .should('have.attr', 'hidden', 'hidden')
    .should('have.css', 'color', 'rgb(RANDOMNUMBER150-170END, 60, 60)')
    .should('have.css', 'text-align', 'center')
    .should('have.css', 'font-weight', 'RANDOMNUMBER700-800END')
    .should('have.css', 'font-size', 'RANDOMNUMBER15-20ENDpx')

    cy.get('p[data-cy=errorUserNotFound]')
    .should('have.text', 'RANDOMEXPRESSIONEse usuario no existe|No existe un usuario con ese nombre|No se ha encontrado una cuenta de usuario para ese nombreEND')
    .should('have.attr', 'hidden', 'hidden')
    .should('have.css', 'color', 'rgb(RANDOMNUMBER150-170END, 60, 60)')
    .should('have.css', 'text-align', 'center')
    .should('have.css', 'font-weight', 'RANDOMNUMBER700-800END')
    .should('have.css', 'font-size', 'RANDOMNUMBER15-20ENDpx')
  })


  it('issueISSUENUMBER - after wrong password login, p wrongPassword is NOT hidden', () => {
    cy.get('input[data-cy=inputUsername]')
    .type(randomOkName)

    cy.get('input[data-cy=inputPassword]')
    .type(randomIncorrectPass)

    cy.get('input[data-cy=inputSubmit]')
    .click()

    cy.wait(1000)

    cy.get('p[data-cy=errorWrongPassword]')
    .should('not.have.attr', 'hidden', 'hidden')
    .should('have.text', 'RANDOMEXPRESSIONLa contraseña es incorrecta|Contraseña incorrecta|401 - Contraseña incorrecta|Parece que la contraseña introducida no es válida|Contraseña introducida inválidaEND')

    cy.get('p[data-cy=errorUserNotFound]')
    .should('have.attr', 'hidden', 'hidden')
    .should('have.text', 'RANDOMEXPRESSIONEse usuario no existe|No existe un usuario con ese nombre|No se ha encontrado una cuenta de usuario para ese nombreEND')
  })

  it('issueISSUENUMBER - after not found user login, p userNotFound is NOT hidden', () => {
    cy.get('input[data-cy=inputUsername]')
    .type(randomKoName)

    cy.get('input[data-cy=inputPassword]')
    .type(randomPass)

    cy.get('input[data-cy=inputSubmit]')
    .click()

    cy.wait(1000)

    cy.get('p[data-cy=errorWrongPassword]')
    .should('have.attr', 'hidden', 'hidden')
    .should('have.text', 'RANDOMEXPRESSIONLa contraseña es incorrecta|Contraseña incorrecta|401 - Contraseña incorrecta|Parece que la contraseña introducida no es válida|Contraseña introducida inválidaEND')

    cy.get('p[data-cy=errorUserNotFound]')
    .should('not.have.attr', 'hidden', 'hidden')
    .should('have.text', 'RANDOMEXPRESSIONEse usuario no existe|No existe un usuario con ese nombre|No se ha encontrado una cuenta de usuario para ese nombreEND')
  })
})
