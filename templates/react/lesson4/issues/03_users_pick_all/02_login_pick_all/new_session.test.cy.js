describe('new_session', () => {
  const randomOkName = "okName" + parseInt(Math.random()*1000);
  const randomKoName = "koName" + parseInt(Math.random()*1000);
  const randomPass = 'a' + parseInt(Math.random()*1000);
  const randomIncorrectPass = 'b' + parseInt(Math.random()*1000);
  const randomToken = 'c' + parseInt(Math.random()*1000);
  const randomSessId = 'd' + parseInt(Math.random()*1000);

  beforeEach(() => {
	cy.intercept('GET', '**/api/v2/dashboards', (req) => {
      req.reply({statusCode: 200, fixture: 'dashboardsList_v2.json'})
    })
    cy.intercept('POST', '**/api/v2/sessions', (req) => {
      const { body } = req
      if (
        body.hasOwnProperty("username") &&
        body.hasOwnProperty("password")
      ) {
        if (
          (body.username == randomOkName) &&
          (body.password == randomPass)
         ) {
            req.reply(200, {created: true, session_id: randomSessId, session_token: randomToken}, [])
          } else if (body.username == randomKoName) {
            req.reply({statusCode: 404, fixture: 'error.json'})
          } else if (body.password == randomIncorrectPass) {
            req.reply({statusCode: 401, fixture: 'error.json'})
          } else {
            // incorrect request according to typed characters
            throw 'Incorrect request'
          }
      } else {
        // incorrect request due to missing parameters
            throw 'Incorrect request'
      }
    }).as("onePost")
    cy.visit('http://localhost:3000/login')
  })

  it('issueISSUENUMBER - login form sends correct random POST', () => {
    cy.get('input[data-cy=inputUsername]')
    .type(randomOkName)

    cy.get('input[data-cy=inputPassword]')
    .type(randomPass)

    cy.get('input[data-cy=inputSubmit]')
    .click()

    cy.wait('@onePost')
    .its('request.body')
    .then(($reqBody) => {
      expect($reqBody.username).to.eq(randomOkName)
      expect($reqBody.password).to.eq(randomPass)
    })
  })

  it('issueISSUENUMBER - login form correctly navigates', () => {
    cy.get('input[data-cy=inputUsername]')
    .type(randomOkName)

    cy.get('input[data-cy=inputPassword]')
    .type(randomPass)

    cy.get('input[data-cy=inputSubmit]')
    .click()

    cy.wait('@onePost')
    .its('request.body')
    .then(($reqBody) => {
      expect($reqBody.username).to.eq(randomOkName)
      expect($reqBody.password).to.eq(randomPass)
    })
    
    cy.url().should('be.equal', 'http://localhost:3000/')
  })

  it('issueISSUENUMBER - login form correctly persists session token', () => {
    cy.get('input[data-cy=inputUsername]')
    .type(randomOkName)

    cy.get('input[data-cy=inputPassword]')
    .type(randomPass)

    cy.get('input[data-cy=inputSubmit]')
    .click()

    cy.wait('@onePost')
    .its('request.body')
    .then(($reqBody) => {
      expect($reqBody.username).to.eq(randomOkName)
      expect($reqBody.password).to.eq(randomPass)
    })
    cy.wait(500)

    let sessionToken;
    cy.getAllSessionStorage().then(result => {
      cy.log("retrieving session storage")
      for (const [k, v] of Object.entries(result)) {
        // v is the key-value dict holding sessionStorage
        // for a certain origin
        if (v['SESSION_TOKEN'] != undefined) {
          sessionToken = v['SESSION_TOKEN']
        }
      } 
    }).then(result => {
      expect(sessionToken).equal(randomToken)
    })
  })

  it('issueISSUENUMBER - login form correctly persists session id', () => {
    cy.get('input[data-cy=inputUsername]')
    .type(randomOkName)

    cy.get('input[data-cy=inputPassword]')
    .type(randomPass)

    cy.get('input[data-cy=inputSubmit]')
    .click()

    cy.wait('@onePost')
    .its('request.body')
    .then(($reqBody) => {
      expect($reqBody.username).to.eq(randomOkName)
      expect($reqBody.password).to.eq(randomPass)
    })
    cy.wait(500)

    let sessionId;
    cy.getAllSessionStorage().then(result => {
      cy.log("retrieving session storage")
      for (const [k, v] of Object.entries(result)) {
        // v is the key-value dict holding sessionStorage
        // for a certain origin
        if (v['SESSION_ID'] != undefined) {
          sessionId = v['SESSION_ID']
        }
      } 
    }).then(result => {
      expect(sessionId).equal(randomSessId)
    })
  })
})
