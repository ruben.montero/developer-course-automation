Un formulario de login bonito... y funcional

## :books: Resumen

* Hablaremos de un flujo de autenticación a través de un _token_ de sesión
* Enviaremos la petición POST necesaria cuando se clica en _Login_
* Si el servidor responde con éxito, guardaremos el _token_  con `sessionStorage` y redirigiremos al usuario a la página principal

## Descripción

Las [APIs REST (REpresentational State Transfer)](https://desarrolloweb.com/articulos/que-es-rest-caracteristicas-sistemas.html) no almacenan estado del cliente. Toda la información relevante viaja en la petición. Es decir, no podemos esperar que el servidor REST [_nos recuerde_](https://youtu.be/Ua3w9CXfwcQ)

<img src="react-stackoverrun/django-backend/doc/diagrams/stateless_login_problems.png" width=700 />

Para abordar el concepto de _sesión_, en el ecosistema HTTP, se suele emplear la siguiente aproximación:

<img src="react-stackoverrun/django-backend/doc/diagrams/stateless_login_explained.png" width=700 />

Aunque este flujo puede suceder de manera más _automática_ (entre el navegador y el servidor) si se configura [autenticación mediante _cookies_](https://swagger.io/docs/specification/authentication/cookie-authentication/), nosotros vamos a implementarlo más _manualmente_ y a gestionar, desde nuestro código React:

1. La recepción del _token_ (tras un _login_)
2. Guardarlo
3. Enviarlo en peticiones que requieren autenticación

En esta tarea, nos centraremos en (1) y (2).

## :package: La tarea

**Añade** `onSubmit={onSubmit}` a la etiqueta `<form>` en `UserLogin.js`.

Después, **añade** dicho método como se indica a continuación:

```jsx
  const onSubmit = (e) => {
    e.preventDefault()
    if ((username.length === 0) || (password.length === 0)) {
      return;
    }
    axios.post('http://raspi:8081/api/v2/sessions', { username: username, password: password }).then(response => {
      console.log(response.data.session_token);
      console.log(response.data.session_id);
    });
```

Como ves, recibimos el _token_ de sesión (y también un _id_; que será relevante para desloguearnos).

Ahora, hay que almacenarlos en el navegador.

### [Web Storage API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API)

Los navegadores tienen una funcionalidad [Web Storage](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API). Nos otorga dos objetos:

* `localStorage`: Almacenamos datos en la caché del navegador. Estos datos se mantienen ahí hasta que se borra la caché.
* `sessionStorage`: Almacenamos datos en la _sesión_ de la pestaña del navegador. Dichos datos no son compartidos con otras pestañas del navegador. Cuando cierras la pestaña actual, se borran automáticamente.

Para nuestro caso de uso, `sessionStorage` es más adecuado[^1].

**Añade** estas líneas al `onSubmit`:

```diff
    axios.post('http://raspi:8081/api/v2/sessions', { username: username, password: password }).then(response => {
      console.log(response.data.session_token);
      console.log(response.data.session_id);
+     sessionStorage.setItem('SESSION_TOKEN', response.data.session_token);
+     sessionStorage.setItem('SESSION_ID', response.data.session_id);
    });
```

Y, tras **importar** [`useNavigate`](https://reactrouterdotcom.fly.dev/docs/en/v6/hooks/use-navigate) (`const navigate = useNavigate();`) como ya has hecho anteriormente, **redirige** al usuario a la página principal:

```diff
    axios.post('http://raspi:8081/api/v2/sessions', { username: username, password: password }).then(response => {
      console.log(response.data.session_token);
      console.log(response.data.session_id);
      sessionStorage.setItem('SESSION_TOKEN', response.data.session_token);
      sessionStorage.setItem('SESSION_ID', response.data.session_id);
+     navigate('/');
    });
```

¡Enhorabuena! Has implementado una página de _login_

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_. 
  
------------------------------------------------------------------------

[^1]: `sessionStorage` no es óptimo para guardar datos de sesión, pues [es vulnerable a ataques XSS](https://stackoverflow.com/questions/54258233/do-i-have-to-store-tokens-in-cookies-or-localstorage-or-session). Lo ideal es emplear [_Cookies_ de sesión](https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies), donde, mediante cabeceras HTTP, la sesión es gestionada por el navegador automáticamente. Sin embargo, es más didáctico trabajar con los datos de la sesión _a mano_. 

