Publicar respuesta, ¡funcional y pulido!

## :books: Resumen

* Crearemos un formulario para subir una nueva pregunta en la pantalla `NewAnswer.js`
* Cuando el usuario lo rellente y haga `submit`, enviaremos la petición POST necesaria para publicar una respuesta nueva
* Tras un POST exitoso, navegaremos la página anterior. Tras un POST de error, mostraremos una alerta
* Si el usuario visita `NewAnswer.js` sin estar _logueado_, mostraremos un error y no le permitiremos crear respuestas

## Descripción

Vamos a completar `NewAnswer.js` de forma análoga a como hemos hecho con `NewQuestion.js` en las tareas anteriores.

Esta vez, la petición POST que se espera que mandemos es algo así:

```
POST /dashboards/<dashboardId>/questions/<questionId>/answers
Session-Token: abc

{
  "description": "Descripción ejemplo abc"
}
```

## :package: La tarea

**Crea** un nuevo fichero `src/screens/new_answer/NewAnswer.css` con el siguiente CSS:

```css
.newAnswerForm {
  padding-top: RANDOMNUMBER30-50ENDpx;
  text-align: center;
}

.newAnswerFormInput {
  height: RANDOMNUMBER45-50ENDpx;
  background-color: rgba(57, 57, 57, 0.0RANDOMNUMBER5-9END);
  border: none;
  border-radius: RANDOMNUMBER10-15ENDpx;
  margin: 5px;
  padding: 0px RANDOMNUMBER20-25ENDpx;
}

.newAnswerFormInput:hover {
  background-color: rgba(57, 57, 57, 0.20);
}

.newAnswerLoginWarning {
  text-align: center;
  color: red;
}
```

**Impórtalo** (`import "./NewAnswer.css"`) desde `NewAnswer.js`.

Luego, en `NewAnswer.js` crea este estado y funciones:

```jsx
  const [formDescription, setFormDescription] = useState("");

  const onChangeDescription = (e) => {
    setFormDescription(e.target.value);
  }
  
  const onSubmitNewAnswer = (e) => {
    e.preventDefault();
    if (formDescription.length === 0) {
      return;
    }
    // To-Do: Enviar POST para subir nueva respuesta
    // (Lo completaremos luego)
  }
```

Y **añade** estos elementos (`<form>` y `<p>`) dentro del `<div>` principal; por _encima_ del `<footer>`:

```jsx
    <form className="newAnswerForm" onSubmit={onSubmitNewAnswer}>
        <textarea data-cy='inputAnswerDescription' onChange={onChangeDescription} value={formDescription} className="newAnswerFormInput" cols="100" placeholder='Escribe aquí tu respuesta'/>
        <br/>
        <input type='submit' data-cy='RANDOMEXPRESSIONsubmitButton|inputSubmit|submit|cySubmit|inputCySubmitEND' className="newAnswerFormInput" value='Publicar respuesta' disabled={ sessionStorage.getItem("SESSION_TOKEN") === null }></input>
    </form>
    <p data-cy='loginWarning' className="newAnswerLoginWarning" hidden={ sessionStorage.getItem("SESSION_TOKEN") !== null }>RANDOMEXPRESSIONDebes loguearte|Haz login|¿Por qué no haces login?|Inicia sesiónEND</p>
```

### ¡Mandar el POST!

**Añade** `const navigate = useNavigate();` a `NewAnswer.js`.

Luego, **completa** la función `onSubmitNewAnswer` **así**:

```diff
  const onSubmitNewAnswer = (e) => {
    e.preventDefault();
    if (formDescription.length === 0) {
      return;
    }
-   // To-Do: Enviar POST para subir nueva respuesta
-   // (Lo completaremos luego)
+   const sessionToken = sessionStorage.getItem("SESSION_TOKEN");
+   const requestBody = { description: formDescription }
+   axios.post("http://raspi:8081/dashboards/" + params.dashboardId + "/questions/" + params.questionId + "/answers", requestBody, { headers: { "Session-Token": sessionToken } }).then(response => {
+     // Respuesta de éxito
+     console.log(response);
+     navigate("/dashboards/" + params.dashboardId + "/questions/" + params.questionId);
+   }).catch(error => {
+     alert("RANDOMEXPRESSIONSe produjo un error|Ha habido un error|No se pudo publicar la respuestaEND");
+   })
  }
```

_(No olvides los `import` necesarios)_

### ...y terminando

Para finalizar, en `QuestionDetail.js`, **añade** el siguiente enlace:

```jsx
<Link data-cy='RANDOMEXPRESSIONnewAnswerLink|createAnswerLink|publishAnswerLinkEND' to='newAnswer'>Publicar respuesta</Link>
```

**Verifica** que publicar _respuesta_ funciona igual de bien que publicar _pregunta_.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
