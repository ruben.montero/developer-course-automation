Puliendo el not so happy path

## :books: Resumen

* Si el usuario visita `NewQuestion.js` sin estar _logueado_, mostraremos un error y no le permitiremos crear preguntas
* También, tras publicar una pregunta navegaremos a la página anterior
* Si la petición falla (`.catch`) mostraremos una alerta

## Descripción

El [_happy path_](https://www.uifrommars.com/que-es-happy-path/) en programación son aquellas líneas que funcionan cuando todo va bien.

¡Hay que estar preparados para lo peor!

¿Qué pasa si abres una pestaña nueva (sin estar _logueado_) y vas a http://localhost:3000/dashboards/1/newQuestion? ¿Funcionará el formulario?

## :package: La tarea

**Añade** este CSS a `NewQuestion.css`:

```css
.newQuestionLoginWarning {
  text-align: center;
  color: red;
}
```

Y **añade** este `<p>` a `NewQuestion.js`, _debajo_ del `<form>`:

```jsx
<p data-cy='loginWarning' className="newQuestionLoginWarning" hidden={ sessionStorage.getItem("SESSION_TOKEN") !== null }>RANDOMEXPRESSIONDebes iniciar sesión|Debes loguearte|Haz login|¿Por qué no haces login?|Inicia sesiónEND</p>
```

**Modifica** también el atributo `disabled` del botón `submit` del formulario, **así**:

```diff
-    <input type='submit' data-cy='RANDOMEXPRESSIONsubmitButton|inputSubmit|submit|cySubmit|inputCySubmitEND className="newQuestionInputField" value='Publicar pregunta'></input>
+    <input type='submit' data-cy='RANDOMEXPRESSIONsubmitButton|inputSubmit|submit|cySubmit|inputCySubmitEND className="newQuestionInputField" value='Publicar pregunta' disabled={ sessionStorage.getItem("SESSION_TOKEN") === null }></input>
```

¡Ya estamos mejor preparados!

### ...y reaccionando tras la petición

**Añade** `useNavigate` a `NewQuestion.js`:

```diff
+import { useNavigate } from "react-router-dom";

const NewQuestion = () => {
+  const navigate = useNavigate();
```

Y **úsalo** dentro de `onSubmitNewQuestion`, en `.then`:

```diff
   axios.post("http://raspi:8081/dashboards/" + params.dashboardId + "/questions", requestBody, { headers: { "Session-Token": sessionToken } }).then(response => {
     // Respuesta de éxito
     console.log(response);
+    navigate("/dashboards/" + params.dashboardId);
   })
```

Para terminar, en caso de que haya un error en el POST, **muestra** una alerta **así**:

```diff
   axios.post("http://raspi:8081/dashboards/" + params.dashboardId + "/questions", requestBody, { headers: { "Session-Token": sessionToken } }).then(response => {
     // Respuesta de éxito
     console.log(response);
     navigate("/dashboards/" + params.dashboardId);
-  })
+  }).catch(error => {
+    alert("RANDOMEXPRESSIONSe ha producido un error|Se produjo un error|Ha habido un error|No se pudo publicar la preguntaEND");
+  })
```

¡Excelente trabajo!

Verifica estas implementaciones. Tu página ya está algo más pulida, ¿verdad?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
