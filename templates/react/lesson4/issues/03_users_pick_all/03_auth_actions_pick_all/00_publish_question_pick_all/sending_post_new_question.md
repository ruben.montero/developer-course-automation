¡...publicando preguntas!

## :books: Resumen

* Añadiremos estados enlazados a los campos del formulario de `NewQuestion.js`
* Cuando el usuario escriba una pregunta y haga `submit`, enviaremos la petición POST necesaria para publicar una pregunta nueva

## Descripción

¿Recuerdas el flujo de autenticación de sesiones?

<img src="react-stackoverrun/django-backend/doc/diagrams/stateless_login_explained.png" width=700 />

En el tercer paso donde el cliente, para confirmar su identidad, manda el _token_ al servidor (arriba `Session-Token: 38afes7a8`), dicho valor _podría_ viajar en cualquier parte de la petición HTTP:

1. Línea de petición
2. Cabeceras
3. Cuerpo

...pero lo habitual es que _viaje en las cabeceras HTTP_.

### ¿Cómo mandar datos en las cabeceras HTTP con axios?

[Enviando (pasando) un argumento](https://blog.logrocket.com/using-axios-set-request-headers/)[^1] cuando invocamos `axios.get`, `axios.post`,... así:

```js
{
  headers: {
    Cabecera1: "Valor1",
    Cabecera2: "Valor2"
  }
}
```

### Diferencia entre `axios.get` y `axios.post`

* `axios.get`[^2], admite _2_ parámetros: (1) La `"url"` y (2) el _objeto_ `{ headers: { Cabecera1: "Valor1" } }`:

```js
axios.get("url", { headers: { Cabecera1: "Valor1" } })

// Si sólo pasas 1 parámetro, se entiende que es la url
axios.get("url")
```

* `axios.post`[^3], admite _3_ parámetros: (1) La `"url"`, (2) el cuerpo de la petición (`{ }`) y (3) el _objeto_ `{ headers: { Cabecera1: "Valor1" } }`:


```js
axios.post("url", requestBody, { headers: { Cabecera1: "Valor1" } })

// Si sólo pasas 2 parámetros, se entiende que son la url y el cuerpo de la petición
axios.post("url", requestBody)

// Si sólo pasas 1 parámetro, se entiende que es la url
axios.post("url")

// Para los dos primeros casos, recuerda que el requestBody
// suele ser también un objeto JSON, e.g.:
axios.post("url", { nuevoComentario: "¡Genial!" })
```

En sí, esto se debe a que [ciertos métodos HTTP (`GET`, `DELETE`) _no_ soportan enviar un cuerpo de la petición](https://www.baeldung.com/cs/http-get-with-body).

## :package: La tarea

**Crea** estos estados en `NewQuestion.js`:

```jsx
const [formTitle, setFormTitle] = useState("");
const [formDescription, setFormDescription] = useState("");
```

**Añade** estas funciones:

```jsx
  const onChangeTitle = (e) => {
    setFormTitle(e.target.value);
  }
  
  const onChangeDescription = (e) => {
    setFormDescription(e.target.value);
  }
  
  const onSubmitNewQuestion = (e) => {
    e.preventDefault();
    if ((formTitle.length === 0) || (formDescription.length === 0)) {
      return;
    }
    // To-Do: Enviar POST para subir nueva pregunta
    // (Lo completaremos luego)
  }
```

...y **modifica** el `<form>` para _enlazar_ los estados a los `<input>`, así como el `onSubmit`:


```diff
-   <form className="newQuestionForm">
+   <form className="newQuestionForm" onSubmit={onSubmitNewQuestion}>
-       <input type='text' data-cy='inputQuestionTitle' className="newQuestionFormInput" placeholder='Título de la pregunta'/>
+       <input type='text' data-cy='inputQuestionTitle' onChange={onChangeTitle} value={formTitle} className="newQuestionFormInput" placeholder='Título de la pregunta'/>
        <br/>
-       <textarea data-cy='inputQuestionDescription' className="newQuestionFormInput" cols="100" placeholder='Detalle de la pregunta'/>
+       <textarea data-cy='inputQuestionDescription' onChange={onChangeDescription} value={formDescription} className="newQuestionFormInput" cols="100" placeholder='Detalle de la pregunta'/>
        <br/>
        <input type='submit' data-cy='RANDOMEXPRESSIONsubmitButton|inputSubmit|submit|cySubmit|inputCySubmitEND' className="newQuestionFormInput" value='Publicar pregunta'></input>
    </form>
```

### ¡Mandar el POST!

De acuerdo al fichero de especificación `react-stackoverrun/django-backend/doc/swagger/swagger.yaml` de tu repositorio, el POST debe ser así:

```
POST /dashboards/<id>/questions
Session-Token: abc

{
  "title": "Título ejemplo abc",
  "description": "Descripción ejemplo abc"
}
```

**Completa** la función `onSubmitNewQuestion` **así**:

```diff
  const onSubmitNewQuestion = (e) => {
    e.preventDefault();
    if ((formTitle.length === 0) || (formDescription.length === 0)) {
      return;
    }
-   // To-Do: Enviar POST para subir nueva pregunta
-   // (Lo completaremos luego)
+   const sessionToken = sessionStorage.getItem("SESSION_TOKEN");
+   const requestBody = { title: formTitle, description: formDescription }
+   axios.post("http://raspi:8081/dashboards/" + params.dashboardId + "/questions", requestBody, { headers: { "Session-Token": sessionToken } }).then(response => {
+     // Respuesta de éxito
+     console.log(response);
+   })
  }
```

¡Listo! Verifica en http://localhost:3000/dashboards/1/newQuestion que, en la consola del navegador, aparece algo demostrando que mandas con éxito un POST para publicar una nueva pregunta.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Ese argumento es la _configuración_ (`config`), y soporta, además de `headers`, [otros valores](https://axios-http.com/docs/req_config)

[^2]: U otros como `axios.delete`

[^3]: U otros como `axios.put`
