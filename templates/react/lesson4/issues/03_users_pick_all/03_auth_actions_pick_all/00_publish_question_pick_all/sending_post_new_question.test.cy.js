describe('sending_post_new_question', () => {
  const randomDashboardId = parseInt(Math.random()*1000)
  const randomQuestion = "this is my question - " + parseInt(Math.random()*1000);
  const randomQuestionTitle = "title" + parseInt(Math.random()*1000);
  const randomToken = "token" + parseInt(Math.random()*1000);

  beforeEach(() => {
	cy.intercept('GET', '**/api/v2/dashboards/**', (req) => {
      req.reply({statusCode: 200, fixture: 'dashboard1_v2.json'})
    })
    cy.intercept('POST', '**/api/v2/dashboards/'+randomDashboardId+'/questions', (req) => {
      const { body } = req
      if (
        body.hasOwnProperty("description") &&
        body.hasOwnProperty("title")
      ) {
        if ((body.description === randomQuestion) && (body.title === randomQuestionTitle)) {
          if (req.headers['session-token'] === randomToken) {
            req.reply({statusCode: 201, fixture: 'created.json'})
          } else {
            req.reply({statusCode: 401, fixture: 'error.json'})
          }
      } else {
        // incorrect request due to description not matching what was typed
        throw "Request not correct"
      }
    } else {
      // incorrect request due to missing description
      throw "Request not correct"
    }
    }).as("onePost")
  })


  it('issueISSUENUMBER - question form has correct submit button that sends a properly formed POST with Header when user is logged in (attempt 1)', () => {
    cy.window().then(function(win){
      // win is the remote window
      // of the page at: http://localhost:8080/app
      win.sessionStorage.setItem('SESSION_TOKEN', randomToken);

      const url = 'http://localhost:3000/dashboards/'+randomDashboardId+'/newQuestion'
      cy.visit(url)
  
      cy.get('[data-cy=inputQuestionDescription]')
      .type(randomQuestion)
  
      cy.get('[data-cy=inputQuestionTitle]')
      .type(randomQuestionTitle)
  
      cy.get('[data-cy=RANDOMEXPRESSIONsubmitButton|inputSubmit|submit|cySubmit|inputCySubmitEND]')
      .click()

      cy.wait('@onePost')
      .its('request')
      .then(($req) => {
        const reqBody = $req.body;
        const headers = $req.headers;
        expect(reqBody.description).to.eq(randomQuestion);
        expect(reqBody.title).to.eq(randomQuestionTitle)
        expect(headers['session-token']).to.eq(randomToken);
      });
    })
  })
  
  it('issueISSUENUMBER - question form has correct submit button that sends a properly formed POST with Header when user is logged in (attempt 2)', () => {
    cy.window().then(function(win){
      // win is the remote window
      // of the page at: http://localhost:8080/app
      win.sessionStorage.setItem('SESSION_TOKEN', randomToken);

      const url = 'http://localhost:3000/dashboards/'+randomDashboardId+'/newQuestion'
      cy.visit(url)
  
      cy.get('[data-cy=inputQuestionDescription]')
      .type(randomQuestion)
  
      cy.get('[data-cy=inputQuestionTitle]')
      .type(randomQuestionTitle)
  
      cy.get('[data-cy=RANDOMEXPRESSIONsubmitButton|inputSubmit|submit|cySubmit|inputCySubmitEND]')
      .click()

      cy.wait('@onePost')
      .its('request')
      .then(($req) => {
        const reqBody = $req.body;
        const headers = $req.headers;
        expect(reqBody.description).to.eq(randomQuestion);
        expect(reqBody.title).to.eq(randomQuestionTitle)
        expect(headers['session-token']).to.eq(randomToken);
      });
    })
  })
})
