Formulario para publicar preguntas

## :books: Resumen

* Crearemos un formulario para subir una nueva pregunta en la pantalla `NewQuestion.js`
* Todavía no será funcional

## Descripción

Vamos a implementar la pantalla que contiene el formulario para subir una nueva pregunta a la página. Todavía no será funcional.

## :package: La tarea

**Crea** un nuevo fichero `src/screens/new_question/NewQuestion.css` con el siguiente CSS:

```css
.newQuestionForm {
  padding-top: RANDOMNUMBER30-50ENDpx;
  text-align: center;
}

.newQuestionFormInput {
  height: RANDOMNUMBER45-50ENDpx;
  background-color: rgba(57, 57, 57, 0.0RANDOMNUMBER5-9END);
  border: none;
  border-radius: RANDOMNUMBER10-15ENDpx;
  margin: 5px;
  padding: 0px RANDOMNUMBER20-25ENDpx;
}

.newQuestionFormInput:hover {
  background-color: rgba(57, 57, 57, 0.20);
}
```

**Impórtalo** (`import ./NewQuestion.css`) desde `NewQuestion.js`.

También, en `NewQuestion.js`, **añade** el siguiente `<form>` dentro del `<div>` principal; por _encima_ del `<footer>`:

```jsx
    <form className="newQuestionForm">
        <input type='text' data-cy='inputQuestionTitle' className="newQuestionFormInput" placeholder='Título de la pregunta'/>
        <br/>
        <textarea data-cy='inputQuestionDescription' className="newQuestionFormInput" cols="100" placeholder='Detalle de la pregunta'/>
        <br/>
        <input type='submit' data-cy='RANDOMEXPRESSIONsubmitButton|inputSubmit|submit|cySubmit|inputCySubmitEND' className="newQuestionFormInput" value='Publicar pregunta'></input>
    </form>
```

¿Ha quedado un bonito formulario en http://localhost:3000/dashboards/1/newQuestion?

Para terminar, en `DashboardDetail.js`, **añade** el siguiente enlace (en la ubicación de la página que tú elijas):

```jsx
<Link data-cy='RANDOMEXPRESSIONnewQuestionLink|createQuestionLink|publishQuestionLinkEND' to='newQuestion'>Publicar pregunta</Link>
```

Verifica desde la página de _detalle de dashboard_ (http://localhost:3000/dashboards/1) que puedes navegar a la pantalla para publicar _pregunta_.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
