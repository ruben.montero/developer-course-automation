describe('question_form', () => {
  const randomDashboardId = parseInt(Math.random()*1000)
  const randomQuestion = "this is my question - " + parseInt(Math.random()*1000);
  const randomQuestionTitle = "title" + parseInt(Math.random()*1000);

  it('issueISSUENUMBER - Link exists in DashboardDetail and redirects to correct URL', () => {
    const url = 'http://localhost:3000/dashboards/'+randomDashboardId;
    cy.visit(url)

    cy.get('a[data-cy=RANDOMEXPRESSIONnewQuestionLink|createQuestionLink|publishQuestionLinkEND]')
    .click()

    cy.url()
    .should('be.equal', url + '/newQuestion')
  })

  it('issueISSUENUMBER - question form has correct submit button, and inputs work', () => {
    const url = 'http://localhost:3000/dashboards/'+randomDashboardId+'/newQuestion'
    cy.visit(url)

    cy.get('[data-cy=inputQuestionDescription]')
    .type(randomQuestion)

    cy.get('[data-cy=inputQuestionTitle]')
    .type(randomQuestionTitle)
    
    
    cy.get('[data-cy=RANDOMEXPRESSIONsubmitButton|inputSubmit|submit|cySubmit|inputCySubmitEND]')
    .should('be.visible')

  })
})
