describe('unhappy_path_new_question', () => {
  const randomDashboardId = parseInt(Math.random()*1000)
  const randomQuestion = "this is my question - " + parseInt(Math.random()*1000);
  const randomQuestionTitle = "title" + parseInt(Math.random()*1000);
  const randomToken = "token" + parseInt(Math.random()*1000);

  beforeEach(() => {
	cy.intercept('GET', '**/api/v2/dashboards/**', (req) => {
      req.reply({statusCode: 200, fixture: 'dashboard1_v2.json'})
    })
    cy.intercept('POST', '**/api/v2/dashboards/'+randomDashboardId+'/questions', (req) => {
      const { body } = req
      if (
        body.hasOwnProperty("description") &&
        body.hasOwnProperty("title")
      ) {
        if ((body.description === randomQuestion) && (body.title === randomQuestionTitle)) {
          if (req.headers['session-token'] === randomToken) {
            req.reply({statusCode: 201, fixture: 'created.json'})
          } else {
            req.reply({statusCode: 401, fixture: 'error.json'})
          }
      } else {
        // incorrect request due to description not matching what was typed
        throw "Request not correct"
      }
    } else {
      // incorrect request due to missing description
      throw "Request not correct"
    }
    }).as("onePost")
  })

  it('issueISSUENUMBER - question form has correct submit button, disabled initially if user is logged out. loginWarning displays correct message too', () => {
    const url = 'http://localhost:3000/dashboards/'+randomDashboardId+'/newQuestion'
    cy.visit(url)

    cy.get('[data-cy=inputQuestionDescription]')
    .type(randomQuestion)

    cy.get('[data-cy=inputQuestionTitle]')
    .type(randomQuestionTitle)

    cy.get('[data-cy=RANDOMEXPRESSIONsubmitButton|inputSubmit|submit|cySubmit|inputCySubmitEND]')
    .should('have.attr', 'disabled')

    cy.get('[data-cy=loginWarning]')
    .should('not.have.attr', 'hidden')

    cy.get('[data-cy=loginWarning]')
    .should('have.text', 'RANDOMEXPRESSIONDebes iniciar sesión|Debes loguearte|Haz login|¿Por qué no haces login?|Inicia sesiónEND');
  })

  it('issueISSUENUMBER - submit button is enabled and warning paragraph hidden when user is logged in', () => {
    cy.window().then(function(win){
      // win is the remote window
      // of the page at: http://localhost:8080/app
      win.sessionStorage.setItem('SESSION_TOKEN', randomToken);

      const url = 'http://localhost:3000/dashboards/'+randomDashboardId+'/newQuestion'
      cy.visit(url)
  
      cy.get('[data-cy=inputQuestionDescription]')
      .type(randomQuestion)
  
      cy.get('[data-cy=inputQuestionTitle]')
      .type(randomQuestionTitle)
  
      cy.get('[data-cy=RANDOMEXPRESSIONsubmitButton|inputSubmit|submit|cySubmit|inputCySubmitEND]')
      .should('not.have.attr', 'disabled')
  
      cy.get('[data-cy=loginWarning]')
      .should('have.attr', 'hidden')
  
      cy.get('[data-cy=loginWarning]')
      .should('have.text', 'RANDOMEXPRESSIONDebes iniciar sesión|Debes loguearte|Haz login|¿Por qué no haces login?|Inicia sesiónEND');
      })
  })

  it('issueISSUENUMBER - question form has correct submit button that sends a properly formed POST with Header when user is logged in', () => {
    cy.window().then(function(win){
      // win is the remote window
      // of the page at: http://localhost:8080/app
      win.sessionStorage.setItem('SESSION_TOKEN', randomToken);

      const url = 'http://localhost:3000/dashboards/'+randomDashboardId+'/newQuestion'
      cy.visit(url)
  
      cy.get('[data-cy=inputQuestionDescription]')
      .type(randomQuestion)
  
      cy.get('[data-cy=inputQuestionTitle]')
      .type(randomQuestionTitle)
  
      cy.get('[data-cy=RANDOMEXPRESSIONsubmitButton|inputSubmit|submit|cySubmit|inputCySubmitEND]')
      .click()

      cy.wait('@onePost')
      .its('request')
      .then(($req) => {
        const reqBody = $req.body;
        const headers = $req.headers;
        expect(reqBody.description).to.eq(randomQuestion);
        expect(reqBody.title).to.eq(randomQuestionTitle)
        expect(headers['session-token']).to.eq(randomToken);
      });
    })
  })

  it('issueISSUENUMBER - new question screen correctly displays an alert when response is KO', () => {
    const badToken = "badToken"+randomToken;
    const stub = cy.stub();
    cy.on('window:alert', stub);
    cy.window().then(function(win){
      // win is the remote window
      // of the page at: http://localhost:8080/app
      win.sessionStorage.setItem('SESSION_TOKEN', badToken);

      const url = 'http://localhost:3000/dashboards/'+randomDashboardId+'/newQuestion'
      cy.visit(url)
  
      cy.get('[data-cy=inputQuestionDescription]')
      .type(randomQuestion)
  
      cy.get('[data-cy=inputQuestionTitle]')
      .type(randomQuestionTitle)
  
      cy.get('[data-cy=RANDOMEXPRESSIONsubmitButton|inputSubmit|submit|cySubmit|inputCySubmitEND]')
      .click()

      cy.wait('@onePost')
      .its('request')
      .then(($req) => {
        const reqBody = $req.body;
        const headers = $req.headers;
        expect(reqBody.description).to.eq(randomQuestion);
        expect(reqBody.title).to.eq(randomQuestionTitle)
        expect(headers['session-token']).to.eq(badToken);
      });

      cy.wait(1000)
      .then(() => {
        expect(stub.getCall(0)).to.be.calledWith('RANDOMEXPRESSIONSe ha producido un error|Se produjo un error|Ha habido un error|No se pudo publicar la preguntaEND')
      })
    })
  })

  it('issueISSUENUMBER - new question screen correctly redirects when answer is OK', () => {
    const stub = cy.stub();
    cy.on('window:alert', stub);
    const url = 'http://localhost:3000/dashboards/'+randomDashboardId
    cy.window().then(function(win){
      // win is the remote window
      // of the page at: http://localhost:8080/app
      win.sessionStorage.setItem('SESSION_TOKEN', randomToken);

      cy.visit(url+'/newQuestion')
  
      cy.get('[data-cy=inputQuestionDescription]')
      .type(randomQuestion)
  
      cy.get('[data-cy=inputQuestionTitle]')
      .type(randomQuestionTitle)
  
      cy.get('[data-cy=RANDOMEXPRESSIONsubmitButton|inputSubmit|submit|cySubmit|inputCySubmitEND]')
      .click()

      cy.wait('@onePost')
      .its('request')
      .then(($req) => {
        const reqBody = $req.body;
        const headers = $req.headers;
        expect(reqBody.description).to.eq(randomQuestion);
        expect(reqBody.title).to.eq(randomQuestionTitle)
        expect(headers['session-token']).to.eq(randomToken);
      });

      cy.wait(1000)

      cy.url()
      .should('be.equal', url)
    })
  })
})
