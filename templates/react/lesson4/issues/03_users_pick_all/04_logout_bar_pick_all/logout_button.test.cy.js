describe('logout_button', () => {
  const randomOkName = "okName" + parseInt(Math.random()*1000);
  const randomKoName = "koName" + parseInt(Math.random()*1000);
  const randomPass = 'a' + parseInt(Math.random()*1000);
  const randomIncorrectPass = 'b' + parseInt(Math.random()*1000);
  const randomToken = 'c' + parseInt(Math.random()*1000);
  const randomSessId = parseInt(Math.random()*1000);

  beforeEach(() => {
    cy.intercept('GET', '**/api/v2/dashboards', (req) => {
      req.reply({statusCode: 200, fixture: 'dashboardsList_v2.json'})
    })

    cy.intercept('DELETE', '**/api/v2/sessions/'+randomSessId, (req) => {
      if (req.headers['session-token'] === randomToken) {
        req.reply({statusCode: 200, fixture: 'created.json'})
      } else {
        throw 'Wrong request, it looks'
      }
    }).as("oneDelete")
  })


  it('issueISSUENUMBER - TopBar has correct main message and logged out message after logout', () => {
    cy.window().then(function(win){
      // win is the remote window
      // of the page at: http://localhost:8080/app
      win.localStorage.setItem('SESSION_TOKEN', randomToken);
      win.localStorage.setItem('SESSION_ID', randomSessId);
      
      cy.visit('http://localhost:3000/')
      cy.wait(500)
      cy.get('[data-cy=topBar]')
      .find('a[href*="#"]')
      .click()

      cy.wait(500)
      
      cy.get('[data-cy=topBar]')
      .find('p[data-cy=loginInfo]')
      .should('contain.text', 'Parece que no estás logueado | Login | Registro')
      .should('not.have.attr', 'hidden')

      cy.get('[data-cy=topBar]')
      .find('p[data-cy=logoutInfo]')
      .should('contain.text', 'RANDOMEXPRESSIONEstás logueado|Has iniciado sesión|Tu sesión está iniciadaEND. ¿Quieres desloguearte?')
      .should('have.attr', 'hidden')
    })
  })

  it('issueISSUENUMBER - TopBar correctly sends DELETE on logout', () => {
    cy.window().then(function(win){
      // win is the remote window
      // of the page at: http://localhost:8080/app
      win.localStorage.setItem('SESSION_TOKEN', randomToken);
      win.localStorage.setItem('SESSION_ID', randomSessId);
      
      cy.visit('http://localhost:3000/')
      cy.wait(500)
      cy.get('[data-cy=topBar]')
      .find('a[href*="#"]')
      .click()

      cy.wait('@oneDelete')
      .its('request')
      .then(($req) => {
        const headers = $req.headers;
        expect(headers['session-token']).to.eq(randomToken);
      });
    })
  })
})
