describe('improving_top_bar', () => {
  const randomDashboardId = parseInt(Math.random()*1000)
  const randomOkName = "okName" + parseInt(Math.random()*1000);
  const randomKoName = "koName" + parseInt(Math.random()*1000);
  const randomPass = 'a' + parseInt(Math.random()*1000);
  const randomIncorrectPass = 'b' + parseInt(Math.random()*1000);
  const randomToken = 'c' + parseInt(Math.random()*1000);
  const randomSessId = parseInt(Math.random()*1000);

  beforeEach(() => {
	cy.intercept('GET', '**/api/v2/dashboards', (req) => {
      req.reply({statusCode: 200, fixture: 'dashboardsList_v2.json'})
    })
	cy.intercept('GET', '**/api/v2/dashboards/'+randomDashboardId, (req) => {
      req.reply({statusCode: 200, fixture: 'dashboard1_v2.json'})
    })
    cy.intercept('POST', '**/api/v2/sessions', (req) => {
      const { body } = req
      if (
        body.hasOwnProperty("username") &&
        body.hasOwnProperty("password")
      ) {
        if (
          (body.username == randomOkName) &&
          (body.password == randomPass)
         ) {
            req.reply(200, {created: true, session_id: randomSessId, session_token: randomToken}, [])
          } else if (body.username == randomKoName) {
            req.reply({statusCode: 404, fixture: 'error.json'})
          } else if (body.password == randomIncorrectPass) {
            req.reply({statusCode: 401, fixture: 'error.json'})
          } else {
            // incorrect request according to typed characters
            throw 'Incorrect request'
          }
      } else {
        // incorrect request due to missing parameters
            throw 'Incorrect request'
      }
    }).as("onePost")
  })


  it('issueISSUENUMBER - Bar has correct CSS in index', () => {
    cy.visit('http://localhost:3000/')
    cy.get('[data-cy=topBar]')
    .should('have.css','padding-top', '10px')
    .should('have.css', 'height', '70px')
    cy.get('[data-cy=topBar]')
    .first()
    .then(($topBar) => {
      const cssBackground = $topBar.css('background');
      expect(cssBackground).to.contain('linear-gradient(90deg, rgb(189, RANDOMNUMBER204-235END, 252) 0%, rgb(142, 142, 230) 35%, rgb(96, 172, 187) 100%)')
    })
  })
  
  it('issueISSUENUMBER - Bar has correct CSS in login', () => {
    cy.visit('http://localhost:3000/login')
    cy.get('[data-cy=topBar]')
    .should('have.css','padding-top', '10px')
    .should('have.css', 'height', '70px')
    cy.get('[data-cy=topBar]')
    .first()
    .then(($topBar) => {
      const cssBackground = $topBar.css('background');
      expect(cssBackground).to.contain('linear-gradient(90deg, rgb(189, RANDOMNUMBER204-235END, 252) 0%, rgb(142, 142, 230) 35%, rgb(96, 172, 187) 100%)')
    })
  })
  
  it('issueISSUENUMBER - Bar has correct CSS in dashboard detail', () => {
    cy.visit('http://localhost:3000/dashboards/'+randomDashboardId)
    cy.get('[data-cy=topBar]')
    .should('have.css','padding-top', '10px')
    .should('have.css', 'height', '70px')
    cy.get('[data-cy=topBar]')
    .first()
    .then(($topBar) => {
      const cssBackground = $topBar.css('background');
      expect(cssBackground).to.contain('linear-gradient(90deg, rgb(189, RANDOMNUMBER204-235END, 252) 0%, rgb(142, 142, 230) 35%, rgb(96, 172, 187) 100%)')
    })
  })
  
  it('issueISSUENUMBER - TopBar has correct main message and logged out message initially', () => {
    cy.visit('http://localhost:3000/')
    cy.get('[data-cy=topBar]')
    .should('contain.text','¡Hola! Quizá quieras ir a la página principal')

    cy.get('[data-cy=topBar]')
    .find('p[data-cy=loginInfo]')
    .should('contain.text', 'Parece que no estás logueado | Login | Registro')
    .should('not.have.attr', 'hidden')

    cy.get('[data-cy=topBar]')
    .find('p[data-cy=logoutInfo]')
    .should('contain.text', 'RANDOMEXPRESSIONEstás logueado|Has iniciado sesión|Tu sesión está iniciadaEND. ¿Quieres desloguearte?')
    .should('have.attr', 'hidden')
  })
  
  it('issueISSUENUMBER - TopBar has correct main message and logged in message when tokenSession is persisted initially', () => {
    cy.window().then(function(win){
      // win is the remote window
      // of the page at: http://localhost:8080/app
      win.localStorage.setItem('SESSION_TOKEN', randomToken);
      win.localStorage.setItem('SESSION_ID', randomSessId);
      
      cy.visit('http://localhost:3000/')
      cy.wait(500)
      cy.get('[data-cy=topBars]')
      .should('contain.text','¡Hola! Quizá quieras ir a la página principal')

      cy.get('[data-cy=topBar]')
      .find('p[data-cy=loginInfo]')
      .should('contain.text', 'Parece que no estás logueado | Login | Registro')
      .should('have.attr', 'hidden')

      cy.get('[data-cy=topBar]')
      .find('p[data-cy=logoutInfo]')
      .should('contain.text', 'RANDOMEXPRESSIONEstás logueado|Has iniciado sesión|Tu sesión está iniciadaEND. ¿Quieres desloguearte?')
      .should('not.have.attr', 'hidden')
    })
  })
  
  it('issueISSUENUMBER - TopBar has correct main message and logged in message after login', () => {
    cy.visit('http://localhost:3000/login')

    cy.get('input[data-cy=inputUsername]')
    .type(randomOkName)

    cy.get('input[data-cy=inputPassword]')
    .type(randomPass)

    cy.get('input[data-cy=inputSubmit]')
    .click()

    cy.wait('@onePost')
    .its('request.body')
    .then(($reqBody) => {
      expect($reqBody.username).to.eq(randomOkName)
      expect($reqBody.password).to.eq(randomPass)
    })

    cy.wait(1000)

    cy.get('[data-cy=topBar]')
    .should('contain.text','¡Hola! Quizá quieras ir a la página principal')

    cy.get('[data-cy=topBar]')
    .find('p[data-cy=loginInfo]')
    .should('contain.text', 'Parece que no estás logueado | Login | Registro')
    .should('have.attr', 'hidden')

    cy.get('[data-cy=topBar]')
    .find('p[data-cy=logoutInfo]')
    .should('contain.text', 'RANDOMEXPRESSIONEstás logueado|Has iniciado sesión|Tu sesión está iniciadaEND. ¿Quieres desloguearte?')
    .should('not.have.attr', 'hidden')
  })
})
