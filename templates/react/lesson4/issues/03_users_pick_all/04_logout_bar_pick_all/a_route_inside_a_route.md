Rutas anidadas (usando Outlet)

## :books: Resumen

* Crearemos un nuevo componente `Container.js` que mostrará `¡Hola!`
* `Container.js` será el contenedor de todas las páginas, poniéndolo como `<Route />` padre
* Además, en `Container.js` incluiremos `<Outlet />`, que actuará como _lienzo_ donde cada ruta hija podrá pintarse

## Descripción

Hasta ahora, hemos escrito todas las `<Route>` al mismo _nivel_, como hermanas. Como en el _siguiente ejemplo_:

```jsx
    <Routes>
      <Route path='equipos' element={<Equipos/>} />
      <Route path='equipos/jugadores' element={<Jugadores/>} />
    </Routes>
```

¡Pero se pueden _anidar_! Observa:

```jsx
    <Routes>
      <Route path='equipos' element={<Equipos/>} >
        <Route path='jugadores' element={<Jugadores/>} />
      </Route>
    </Routes>
```

Esto será muy útil para implementar un panel lateral o un encabezado común que esté presente en todas las páginas.

Vamos a hacerlo, aprendiendo, en el camino, cómo funcionan las [_rutas anidadas_](https://reactrouterdotcom.fly.dev/docs/en/v6/getting-started/overview#nested-routes) y `<Outlet />` en `react-router-dom`.

## :package: La tarea

**Crea** `src/screens/container/Container.js`:

```jsx
const Container = () => {
  return <div>
      <div data-cy="topBar">¡Hola!</div>
    </div>
}

export default Container;
```

Y luego **modifica** `App.js` para que las rutas sean así:

```jsx
    <Routes>
      <Route path='/' element={<Container/>}>
        <Route path='register' element={<UserRegister/>}></Route>
        <Route path='login' element={<UserLogin/>}></Route>
        <Route path='dashboards/:dashboardId' element={<DashboardDetail/>}></Route>
        <Route path='dashboards/:dashboardId/newQuestion' element={<NewQuestion/>}></Route>
        <Route path='dashboards/:dashboardId/questions/:questionId' element={<QuestionDetail/>}></Route>
        <Route path='dashboards/:dashboardId/questions/:questionId/newAnswer' element={<NewAnswer/>}></Route>
      </Route>
      <Route path='*' element={<NotFound/>}/>
    </Routes>
```

* A cada `path` _(excepto wildcard `*`)_ le hemos eliminado el prefijo `/`, que ahora pertenece a la ruta _padre_ (`<Container />`).

Ahora verifica, por ejemplo, http://localhost:3000/register

¡Vaya...! ¡Sólo se ve `¡Hola!`! ¡Hemos roto nuestra web!

### [<Outlet>](https://reactrouterdotcom.fly.dev/docs/en/v6/components/outlet)

De acuerdo a la documentación:

> An Outlet should be used in parent route elements to render their child route elements

**Añade** lo siguiente a `Container.js`:

```diff
+import { Outlet } from "react-router-dom"

 const Container = () => {
   return <div>
       <div data-cy="topBar">¡Hola!</div>
+      <Outlet/>
     </div>
 }
```

Observarás que casi todas las páginas funcionan ahora bien, y además muestran el `¡Hola!`.

### [`index`](https://reactrouter.com/en/main/route/route#index)

Sin embargo, la principal (http://localhost:3000/) aún _no_ se ve bien.

**Añade** lo siguiente en `App.js`:

```diff
    <Routes>
      <Route path='/' element={<Container/>}>
+       <Route index element={<Dashboards/>}></Route>
        <Route path='register' element={<UserRegister/>}></Route>
        ...
    </Routes>
```

De esta manera, indicas que `<Dashboards />` es la [principal (índice)](https://reactrouter.com/en/main/route/route#index), y debe _poblar_ el `<Outlet />` en caso de que se visite la ruta principal (y no una sub-ruta).

Verifica ahora http://localhost:3000/

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
