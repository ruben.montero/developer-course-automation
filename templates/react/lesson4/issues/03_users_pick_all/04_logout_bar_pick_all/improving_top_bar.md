Una barra superior más bonita

## :books: Resumen

* Añadiremos algo de CSS a la ruta padre con la barra superior
* Añadiremos unos enlaces que se mostrarán si estás _logueado_ o no

## Descripción

Retomemos lo que emprendimos la tarea anterior:

>>>
Vamos a trabajar para que:

```jsx
    <div data-cy="topBar">¡Hola!</div>
```

...de `Container.js` sea una _barra superior_ muy chula. Tendrá una apariencia u otra según si el usuario se ha _logueado_ o no.

>>>

## :package: La tarea

**Crea** `src/screens/container/Container.css`

```css
.topBar {
  padding-top: 10px;
  width: 100%;
  height: 70px;
  background: linear-gradient(90deg, rgb(189, RANDOMNUMBER204-235END, 252) 0%, rgb(142, 142, 230) 35%, rgb(96, 172, 187) 100%);
}
```

A continuación, **importa** dicho CSS en `Container.js`.

Luego, **añade** un par de enlaces y mejora el `¡Hola!`, **así**:

```diff
-    <div data-cy="topBar">¡Hola!</div>
+    <div data-cy="topBar" className="topBar">
+      <strong>¡Hola!</strong> Quizá quieras ir a la <Link to='/'>página principal</Link>
+      <p hidden={loggedIn} data-cy='loginInfo'>Parece que no estás logueado | <Link to='/login'>Login</Link> | <Link to='/register'>Registro</Link></p>
+      <p hidden={!loggedIn} data-cy='logoutInfo'>RANDOMEXPRESSIONEstás logueado|Has iniciado sesión|Tu sesión está iniciadaEND. ¿Quieres <a href="#" onClick={doLogout}>desloguearte</a>?</p>
+    </div>
```

Como ves, se especifica una función `doLogout` en el último enlace. **Añádela** (vacía de momento):

```jsx
const doLogout = () => {

}
```

¿Qué tal se ve http://localhost:3000?

¡En la próxima tarea permitiremos _desloguearnos_!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
