Estado loggedIn (useOutletContext)

## :books: Resumen

* Añadiremos un estado _booleano_ `[loggedIn, setLoggedIn]` a `Container.js`
* Lo pasaremos como `context` a las rutas hijas (`<Outlet />`)
* Desde una ruta hija, o sea, página de _login_, lo _usaremos_ con `useOutletContext`. Lo pondremos a `true` cuando el usuario se _loguea_

## Descripción

Vamos a trabajar para que:

```jsx
    <div data-cy="topBar">¡Hola!</div>
```

...de `Container.js` sea una _barra superior_ muy chula. Tendrá una apariencia u otra según si el usuario se ha _logueado_ o no.

Guardaremos esa posibilidad (`true` o `false`) en un nuevo estado.

Dicho estado será necesario en `Container.js` y en alguna ruta hija, así que lo almacenaremos en el elemento de mayor nivel (`Container.js`).

### [`useOutletContext`](https://reactrouter.com/docs/en/v6/hooks/use-outlet-context)

Para enviar un _estado_ de un padre a un hijo (`<Outlet />`), en vez de _props_ directamente, usamos [`useOutletContext`](https://reactrouter.com/docs/en/v6/hooks/use-outlet-context):

> useOutletContext

> Often parent routes manage state or other values you want shared with child routes. You can create your own context provider if you like, but this is such a common situation that it's built into Outlet

```mermaid
graph TD
    subgraph X[Container]
    A["[loggedIn, setLoggedIn] = useState(false)"] ---- B["return &ltOutlet context=[loggedIn, setLoggedIn]&gt"]
    end
    subgraph Y["Ruta hija (e.g.: Login)"]
    C["const [loggedIn, setLoggedIn] = useOutletContext()"]
    end
    X ----> Y
```

## :package: La tarea

**Crea** un nuevo estado `loggedIn` en `Container.js`:

```jsx
import { useState } from "react";
import { Outlet } from "react-router-dom"

const Container = () => {
  const [loggedIn, setLoggedIn] = useState(false);
    
  return <div>
      <div data-cy="topBar">¡Hola!</div>
      <Outlet/>
    </div>
}
```

...y **pásalo** al `<Outlet />`, **así**:

```diff
-    <Outlet />
+    <Outlet context={[loggedIn, setLoggedIn]}/>
```

Luego, en `src/screens/user/login/UserLogin.js`, **úsalo** (recupéralo del padre) mediante [`useOutletContext`](https://reactrouter.com/docs/en/v6/hooks/use-outlet-context):

```diff
+import { useOutletContext } from "react-router-dom";

const UserLogin = () => {
+  const [loggedIn, setLoggedIn] = useOutletContext();
```

Y, finalmente, termina de **modificar** en `UserLogin.js` el `.then()` que gestiona la _respuesta exitosa de login_:

```diff
    axios.post('http://raspi:8081/api/v2/sessions', { username: username, password: password }).then(response => {
      console.log(response.data.session_token);
      console.log(response.data.session_id);
      sessionStorage.setItem('SESSION_TOKEN', response.data.session_token);
      sessionStorage.setItem('SESSION_ID', response.data.session_id);
      navigate('/');
+     setLoggedIn(true);
    });
```

¡Listo! Ya tienes un estado _booleano_ para saber si el usuario está _logueado_ o no. Habita en la ruta padre (`Container.js`).

Lo usaremos en la siguiente tarea :wink:

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
