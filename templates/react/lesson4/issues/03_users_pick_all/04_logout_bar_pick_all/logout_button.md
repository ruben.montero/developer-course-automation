Desloguearse

## :books: Resumen

* Enviaremos un HTTP DELETE para _desloguearnos_ al clicar en _desloguearte_ en `Container.js`
* También borraremos los datos de `sessionStorage` y haremos `setLoggedIn(false)`

## Descripción

Para finalizar nuestro maravilloso proyecto, permitamos al usuario _desloguearse_.

## :package: La tarea

**Añade** un método `sendLogoutRequest` a `Container.js` que envíe la siguiente petición DELETE:

```jsx
  const sendLogoutRequest = () => {
    const id = sessionStorage.getItem('SESSION_ID');
    const token = sessionStorage.getItem('SESSION_TOKEN');
    if ((id !== null) && (token !== null)) {
      axios.delete('http://raspi:8081/api/v2/sessions/'+id, { headers: {'Session-Token': token }})
    }
  }
```

Luego, **invócala** desde `doLogout`. También, **completa** el proceso de _logout_ **así**:


```jsx
const doLogout = () => {
  sendLogoutRequest()
  // Logout lado cliente
  sessionStorage.removeItem('SESSION_ID');
  sessionStorage.removeItem('SESSION_TOKEN');
  setLoggedIn(false);
}
```

Como curiosidad, verás que hemos decidido no esperar a la respuesta del HTTP DELETE. ¿Para qué hacerlo? Si esperamos a la confirmación del servidor de que nos hemos _deslogueado_ para hacer `sessionStorage.removeItem` y `setLoggedIn(false)` _podríamos_ estar dejando al usuario bloqueado sin poder _desloguearse_ en caso de que la petición fallase incontroladamente.

Haciéndolo como lo hemos hecho (sin esperar al servidor) garantizamos que nos _deslogueamos_ desde el cliente. Como desventaja, si el HTTP DELETE falla, el servidor no sabrá que esa sesión ya se ha perdido.

¿Comprendes las posibilidades? ¿Cuál te parece mejor?

### Y... ¡proyecto terminado!

Has completado `react-stackoverrun`.

**¡Felicidades!**

Comprueba su funcionamiento en http://localhost:3000. ¿Qué te parece el resultado?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
