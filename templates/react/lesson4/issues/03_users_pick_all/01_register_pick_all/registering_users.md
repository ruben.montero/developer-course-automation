Un formulario de registro bonito... y funcional

## :books: Resumen

* Escribiremos en `UserRegister.js` una función `onSubmit` que mandará una petición de registro
* Añadiremos `onSubmit={onSubmit}` al `<form>`

## Descripción

Haremos nuestro formulario de registro funcional. Cuando el usuario clique en _RANDOMEXPRESSIONCrear cuenta|Registrarse|Registro|EmpezarEND_, enviará:

```
POST http://raspi:8081/api/v2/users

Request Body (example):
{
  "username": "pepe.depura",
  "password": "1234",
  "password_confirm": "1234"
}
```

## :package: La tarea

**Añade** un método `onSubmit` a `UserRegister.js`:

```jsx
  const onSubmit = (e) => {
    e.preventDefault();
    
    // ...
  }
```

**Complétalo** con este comportamiento:

* Validará que los estados `username`, `password` y `passwordConfirm` no sean vacíos. Si _alguno_ es vacío (longitud `0`), se hará `return` para evitar que continúe la ejecución
* Después, usará `axios` para enviar la petición de registro:

```jsx
axios.post("http://raspi:8081/api/v2/users", { username: username, password: password, password_confirm: passwordConfirm }).then(response => {
  console.log("Registro exitoso");
})
```

Para terminar, **añade** `onSubmit={onSubmit}` al `<form>` HTML.

**Verifica** que si haces Inspeccionar > Consola en http://localhost:3000/register y te registras, ¿ves el `console.log` de éxito?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_. 
