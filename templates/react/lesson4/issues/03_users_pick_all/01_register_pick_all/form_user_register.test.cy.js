describe('form_user_register', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/register')
  })

  it('issueISSUENUMBER - register form has correct hierarchy and header', () => {
    cy.get('div')
    .find('form[data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND]')
    .find('input[data-cy=inputUsername]')

    cy.get('div')
    .find('form[data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND]')
    .find('input[data-cy=inputPassword]')

    cy.get('div')
    .find('form[data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND]')
    .find('input[data-cy=inputPasswordConfirm]')

    cy.get('div')
    .find('form[data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND]')
    .find('input[data-cy=inputSubmit]')

    cy.get('div')
    .find('h1[data-cy=RANDOMEXPRESSIONpageHeader|headerEND]')
    .should('have.text', 'Crea tu cuenta')
  })

  it('issueISSUENUMBER - register form has correct text, types and placeholders', () => {
    cy.get('div')
    .find('form[data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND]')
    .find('input[data-cy=inputUsername]')
    .should('have.attr', 'type', 'text')
    .should('have.attr', 'placeholder', 'RANDOMEXPRESSIONmarco.polo|the.john.doe|the.jane.doe|new.user|my.nameEND')

    cy.get('div')
    .find('form[data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND]')
    .find('input[data-cy=inputPassword]')
    .should('have.attr', 'type', 'password')
    .should('have.attr', 'placeholder', 'Contraseña')

    cy.get('div')
    .find('form[data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND]')
    .find('input[data-cy=inputPasswordConfirm]')
    .should('have.attr', 'type', 'password')
    .should('have.attr', 'placeholder', 'Confirmar contraseña')

    cy.get('div')
    .find('form[data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND]')
    .find('input[data-cy=inputSubmit]')
    .should('have.attr', 'value', 'RANDOMEXPRESSIONCrear cuenta|Registrarse|Registro|EmpezarEND')
  })

  it('issue - register form page has correct CSS', () => {
    cy.get('div')
    .find('form[data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND]')
    .find('input[data-cy=inputUsername]')
    .should('have.css', 'height', 'RANDOMNUMBER45-50ENDpx')
    .should('have.css', 'background-color', 'rgba(57, 57, 57, 0.0RANDOMNUMBER5-9END)')
    .should('have.css', 'border-radius', 'RANDOMNUMBER10-15ENDpx')
    .should('have.css', 'margin', '5px')
    .should('have.css', 'padding', '0px RANDOMNUMBER20-25ENDpx')

    cy.get('div')
    .find('form[data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND]')
    .find('input[data-cy=inputPassword]')
    .should('have.css', 'height', 'RANDOMNUMBER45-50ENDpx')
    .should('have.css', 'background-color', 'rgba(57, 57, 57, 0.0RANDOMNUMBER5-9END)')
    .should('have.css', 'border-radius', 'RANDOMNUMBER10-15ENDpx')
    .should('have.css', 'margin', '5px')
    .should('have.css', 'padding', '0px RANDOMNUMBER20-25ENDpx')

    cy.get('div')
    .find('form[data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND]')
    .find('input[data-cy=inputPasswordConfirm]')
    .should('have.css', 'height', 'RANDOMNUMBER45-50ENDpx')
    .should('have.css', 'background-color', 'rgba(57, 57, 57, 0.0RANDOMNUMBER5-9END)')
    .should('have.css', 'border-radius', 'RANDOMNUMBER10-15ENDpx')
    .should('have.css', 'margin', '5px')
    .should('have.css', 'padding', '0px RANDOMNUMBER20-25ENDpx')

    cy.get('div')
    .find('form[data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND]')
    .find('input[data-cy=inputSubmit]')
    .should('have.css', 'height', 'RANDOMNUMBER45-50ENDpx')
    .should('have.css', 'background-color', 'rgba(57, 57, 57, 0.0RANDOMNUMBER5-9END)')
    .should('have.css', 'border-radius', 'RANDOMNUMBER10-15ENDpx')
    .should('have.css', 'margin', '5px')
    .should('have.css', 'padding', '0px RANDOMNUMBER20-25ENDpx')

    cy.get('div')
    .find('form[data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND]')
    .should('have.css', 'text-align', 'center')
  })
})
