¿Me he registrado?

## :books: Resumen

* Añadiremos tres `<p>` y las mostraremos u ocultaremos en función del éxito o error en el registro
* Controlaremos el éxito con `.then()`
* Controlaremos los errores `400` (formulario incorrecto) y `409` (usuario ya existe) con `.catch()`

## Descripción

Siempre se debe controlar el comportamiento de una aplicación si falla una petición de red. El usuario debe recibir _feedback_ de _qué_ ha pasado.

Hasta ahora, hemos manejado las respuestas de éxito que recibimos en `axios` con `.then()`. ¡También podemos [gestionar los errores con `.catch()`](https://axios-http.com/es/docs/handling_errors)!

## :package: La tarea

**Añade** 3 `<p>` a `UserRegister.js`, _encima_ del formulario (`<form>`):

* La primera tendrá `data-cy='successText'`, `className='responseUserFeedback'` y mostrará el mensaje `RANDOMEXPRESSION¡Te has registrado con éxito!|¡Registro exitoso!|Cuenta creadaEND`
* La segunda tendrá `data-cy='errorPasswords'`, `className='responseUserFeedback'` y mostrará el mensaje `RANDOMEXPRESSIONNo se ha registrado el usuario. ¿Las contraseñas coincidían?|No se ha registrado el usuario, quizá porque las contraseñas no coincidíanEND`
* La tercera tendrá `data-cy='errorAlreadyRegistered'`, `className='responseUserFeedback'` y mostrará el mensaje `RANDOMEXPRESSIONLa petición ha fallado porque ya existe un usuario con ese nombre|Ya existe un usuario con ese nombreEND`

**Añade** el siguiente CSS a `UserRegister.css`:

```css
.responseUserFeedback {
  color: rgb(RANDOMNUMBER150-170END, 50, 50);
  text-align: center;
  font-weight: 700;
  font-size: RANDOMNUMBER15-20ENDpx;
}
```

### Mostrar u ocultar los `<p>`

**Añade** estos tres estados a `UserRegister.js`:

```jsx
const [successHidden, setSuccessHidden] = useState(true);
const [errorMatchingPasswordsHidden, setErrorMatchingPasswordsHidden] = useState(true);
const [errorAlreadyRegisteredHidden, setErrorAlreadyRegisteredHidden] = useState(true);
```

...y **enlázalos** a las etiquetas `<p>` añadidas anteriormente, respectivamente, con `hidden={successHiden}`, `hidden={errorMatchingPasswordsHidden}` y `hidden={errorAlreadyRegisteredHidden}`.

Para finalizar, **añade** estas líneas a la petición `axios` del `onSubmit`:

```diff
  axios.post("http://raspi:8081/api/v2/users", { username: username, password: password, password_confirm: passwordConfirm }).then(response => {
    console.log("Registro exitoso");
+   setSuccessHidden(false);  
- })
+ }).catch(error => {
+   if (error.response) {
+     // El servidor respondió con un código de error
+     if (error.response.status === 400) {
+       setErrorMatchingPasswordsHidden(false);
+     } else if (error.response.status === 409) {
+       setErrorAlreadyRegisteredHidden(false);
+     } else {
+       console.log(error.response);
+     }
+   } else {
+     // No se recibió respuesta del servidor
+     console.log(error);
+   }
+ })
```

**Verifica** estos escenarios en http://localhost:3000/register

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
