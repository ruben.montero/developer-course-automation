Un formulario de registro bonito

## :books: Resumen

* Crearemos `UserRegister.js`
* Añadiremos un formulario de registro con 3 campos: _Nombre de usuario_, _Contraseña_ y _Confirmar contraseña_, enlazados a sus respectivos estados
* Completaremos el formulario con algo de CSS. Todavía _no_ será funcional

## Descripción

Vamos a trabajar el registro y autenticación de usuarios.

El _registro de un usuario_ consiste en añadir una nueva fila a la base de datos. Nuestro [API REST](react-stackoverrun/django-backend/doc/swagger/swagger.yaml) tiene un _endpoint_ dedicado a ello:

* POST http://raspi:8081/api/v2/users

Para ir poco a poco, en esta tarea crearemos un formulario de registro _todavía no funcional_.

## :package: La tarea

**Crea** una nueva pantalla `src/screens/user/register/UserRegister.js`.

En `UserRegister.js`, **añade** los siguientes estados:

```jsx
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');
```

En la parte visual del componente (`return`), **añade** un `<div>` principal, y dentro:

* Una etiqueta `<h1 data-cy=RANDOMEXPRESSIONpageHeader|headerEND>` que muestre `Crea tu cuenta`
* Un `<form data-cy=RANDOMEXPRESSIONregisterForm|userRegisterForm|newUserFormEND>` que contenga:
  * Un primer `<input>` con:
    * `data-cy=inputUsername`
    * `type=text`
    * `onChange={onChangeUsername}` _(La escribiremos a continuación)_
    * `placeholder='RANDOMEXPRESSIONmarco.polo|the.john.doe|the.jane.doe|new.user|my.nameEND'`
    * `value={username}`
  * Después, una etiqueta `<br />`
  * Un segundo `<input>`:
    * `data-cy=inputPassword`
    * `type=password`
    * `onChange={onChangePassword}` _(La escribiremos a continuación)_
    * `placeholder='Contraseña'`
    * `value={password}`
  * Después, una etiqueta `<br />`
  * Un tercer `<input>`:
    * `data-cy=inputPasswordConfirm`
    * `type=password`
    * `onChange={onChangePasswordConfirm}` _(La escribiremos a continuación)_
    * `placeholder='Confirmar contraseña'`
    * `value={passwordConfirm}`
  * Después, una etiqueta `<br />`
  * Un cuarto `<input>`:
    * `data-cy=inputSubmit`
    * `type=submit`
    * `value='RANDOMEXPRESSIONCrear cuenta|Registrarse|Registro|EmpezarEND'`

A continuación, **añade** las funciones necesarias:

```jsx
  const onChangeUsername = (e) => {
    setUsername(e.target.value);
  }
  
  const onChangePassword = (e) => {
    setPassword(e.target.value);
  }
  
  const onChangePasswordConfirm = (e) => {
    setPasswordConfirm(e.target.value);
  }
```

### ¡Veamos nuestro formulario!

**Añade** en `App.js`:

```jsx
      <Route path='/register' element={<UserRegister/>}/>
```

Y ahora ya estará visible la página http://localhost:3000/register

¿Se ve bien?

### Un poco de CSS

**Crea** un nuevo fichero `src/screens/user/register/UserRegister.css` con el siguiente CSS[^1]:

```css
.registerHeader, .registerForm {
  padding-top: RANDOMNUMBER30-50ENDpx;
  text-align: center;
}

.registerFormInput {
  height: RANDOMNUMBER45-50ENDpx;
  background-color: rgba(57, 57, 57, 0.0RANDOMNUMBER5-9END);
  border: none;
  border-radius: RANDOMNUMBER10-15ENDpx;
  margin: 5px;
  padding: 0px RANDOMNUMBER20-25ENDpx;
}

.registerFormInput:hover {
  background-color: rgba(57, 57, 57, 0.20);
}
```

Para terminar, en `UserRegister.js` **añade** el `import`:

```jsx
import ./UserRegister.css
```

Y **añade** también los `className` necesarios:

* `<h1>` :arrow\_right:  `className="registerHeader"`
* `<form>` :arrow\_right:  `className="registerForm"`
* `<input>` _(los 4)_ :arrow\_right:  `className="registerFormInput`

¿Qué tal se ve http://localhost:3000/register ahora?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_. 

--------------------------------------------

[^1]: Si te preguntas: _¿Como se puede hacer un buen diseño usando CSS?_ la respuesta es: Practicando. Los navegadores ofrecen la útil herramienta _Click derecho > Inspeccionar_ que te permite entender _qué_ CSS exactamente está afectando a un elemento. Busca elementos similares en págines de Internet que te resulten atractivos e inspecciónalos. Basta con copiar y usar su CSS en tu página, intentando mantenerlo sencillo (principio [KISS](https://es.wikipedia.org/wiki/Principio_KISS)).
