/// <reference types="cypress" />

describe('write_params_handling', () => {

  it('issueISSUENUMBER - dashboard detail page is served and has correct footerDebugInfo', () => {
    const randomNumber = Math.random().toString().slice(2, 8);
    cy.visit('http://localhost:3000/dashboards/' + randomNumber)
    
    cy.get('footer')
    .find('p[data-cy=RANDOMEXPRESSIONfooterDebugInfo|footerDebugText|footerInfoDebug|footerTextDebugEND]')
    .should('have.text', 'RANDOMEXPRESSIONEstás visitando el dashboard con ID:|Este es el dashboard con ID:|Aquí está el dashboard con ID:END ' + randomNumber)
  })

  it('issueISSUENUMBER - new question page is served and has correct footerDebugInfo', () => {
    const randomNumber = Math.random().toString().slice(2, 8);
    cy.visit('http://localhost:3000/dashboards/' + randomNumber + '/newQuestion')

    cy.get('footer')
    .find('p[data-cy=RANDOMEXPRESSIONfooterDebugInfo|footerDebugText|footerInfoDebug|footerTextDebugEND]')
    .should('have.text', 'Desde aquí puedes crear una nueva pregunta para el dashboard con ID: ' + randomNumber)
  })

  it('issueISSUENUMBER - question detail page is served and has correct footerDebugInfo', () => {
    const randomNumber = Math.random().toString().slice(2, 8);
    const randomNumber2 = Math.random().toString().slice(2, 8);
    cy.visit('http://localhost:3000/dashboards/' + randomNumber + '/questions/' + randomNumber2)

    cy.get('footer')
    .find('p[data-cy=RANDOMEXPRESSIONfooterDebugInfo|footerDebugText|footerInfoDebug|footerTextDebugEND]')
    .should('have.text', 'RANDOMEXPRESSIONEstás visitando la pregunta con ID:|Esta es la pregunta con ID:|Aquí está la pregunta con ID:END ' + randomNumber2 + ', que pertenece al dashboard con ID: ' + randomNumber)
  })

  it('issueISSUENUMBER - new answer page is served and has correct footerDebugInfo', () => {
    const randomNumber = Math.random().toString().slice(2, 8);
    const randomNumber2 = Math.random().toString().slice(2, 8);
    cy.visit('http://localhost:3000/dashboards/' + randomNumber + '/questions/' + randomNumber2 + '/newAnswer')

    cy.get('footer')
    .find('p[data-cy=RANDOMEXPRESSIONfooterDebugInfo|footerDebugText|footerInfoDebug|footerTextDebugEND]')
    .should('have.text', 'Desde aquí puedes crear una nueva respuesta para la pregunta con ID: ' + randomNumber2 + ', que pertenece al dashboard con ID: ' + randomNumber)
  })
})


