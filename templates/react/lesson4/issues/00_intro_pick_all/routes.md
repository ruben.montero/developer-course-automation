Esqueleto de las páginas (I)

## :books: Resumen

* Añadiremos algunas `<Route/>` parecidas a las del proyecto anterior
* Añadiremos pantallas todavía no funcionales, que estarán disponibles en cada ruta

## Descripción

Vamos a revivir `react-dashboards`. Recuerda, esta vez, es `react-stackoverrun`.

## :package: La tarea

**Crea** los siguientes componentes (páginas) en tu proyecto, con las respectivas carpetas:

* `src/screens/dashboards/Dashboards.js`
* `src/screens/dashboard_detail/DashboardDetail.js`
* `src/screens/new_question/NewQuestion.js`
* `src/screens/question_detail/QuestionDetail.js`
* `src/screens/new_answer/NewAnswer.js`

En cada una de estas pantallas, **crea** un `<div>` principal, y _dentro_, **incluye** un `<footer>`.

Luego, dentro de cada `<footer>` **añade** un `<p data-cy='RANDOMEXPRESSIONfooterCopyright|copyrightFooter|copyright_footer|footer_copyrightEND'>` que contenga, como texto, el símbolo de Copyright (©) seguido de tu nombre y apellidos.

Para finalizar, **añade** las siguientes rutas (`<Route/>`) en `App.js`. 


Ruta                                                        | Componente
------------------------------------------------------------|-------------
`/`                                                         | `Dashboards.js`
`/dashboards/:dashboardId`                                  | `DashboardDetail.js`
`/dashboards/:dashboardId/newQuestion`                      | `NewQuestion.js`
`/dashboards/:dashboardId/questions/:questionId`            | `QuestionDetail.js`
`/dashboards/:dashboardId/questions/:questionId/newAnswer`  | `NewAnswer.js`

Visita cualquiera de ellas desde http://localhost:3000. ¿Qué tal?

### ¿Y el fondo?

Observarás que el fondo degradado que hemos añadido en la tarea anterior, se visualiza correctamente en el resto de páginas.

¡`NotFound.css` aplica _globalmente_!

Ten siempre esto en cuenta.

Ya que el selector CSS `body` afecta globalmente, es preferible tenerlo en una hoja de estilo general (y no _perdido_ en `NotFound.css`).

**Corta** el CSS asociado a `body` de `NotFound.css` y **pégalo** en el CSS general `App.css`.

Comprueba que el fondo sigue visible en todas las páginas.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
