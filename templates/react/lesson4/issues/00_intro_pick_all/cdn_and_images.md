Servir nuestras propias imágenes

## :books: Resumen

* Hablaremos sobre lo que es una [Content Delivery Network ó CDN](https://www.cloudflare.com/es-es/learning/cdn/what-is-a-cdn/)
* Serviremos una imagen desde la página _Not Found_, y añadiremos algo de CSS

## Descripción

Los ficheros CSS, imágenes, ficheros de tipografías, etc., son considerados ficheros _estáticos_ en una página web, porque no cambian con frecuencia.

Una estrategia para _reducir la carga del servidor web_ y que funcione mejor para más usuarios, es usar una [Content Delivery Network ó CDN](https://www.cloudflare.com/es-es/learning/cdn/what-is-a-cdn/).

### CDN

Se trata de un servicio que podemos contratar (o montar nosotros) en el que _muchos servidores_ entregan a los clientes los _ficheros estáticos_, y así el servidor principal se libera de dicha carga.

Imagina que las etiquetas `<img>` en vez de apuntar a `src='/images/mi-imagen.png'`, apuntan a _otro servidor_, como `src='https://un-servidor-cdn.com/imagenes/mi-imagen.png'`.

Entonces, los navegadores web de los clientes pedirán las imágenes a ese servidor. Si se trata de _nuestro_ CDN, albergará _nuestros_ estáticos en una red de servidores, y _el servidor más cercano atenderá la petición del cliente_.

Se trata de una técnica de optimización que no veremos en la práctica pero es interesante conocer.

### Un caso sencillo (sin CDN)

Hay distintas estrategias para [servir imágenes en un proyecto React](https://create-react-app.dev/docs/adding-images-fonts-and-files/).

A diferencia de [como hicimos en tareas anteriores](https://stackoverflow.com/a/45387838), ahora usaremos la carpeta `public/`.

En este proyecto existe una carpeta `images/` dentro de `public/`.

## :package: La tarea

**Añade** una etiqueta [`<img>`](https://developer.mozilla.org/es/docs/Web/HTML/Element/img) con el atributo `src='/images/RANDOMEXPRESSIONastronaut.png|monkey-face.png|monkey-ninja.pngEND'` a `NotFound.js`.

¿Ves la imagen correctamente en http://localhost:3000?

### Algo de CSS

**Añade** un nuevo archivo `NotFound.css` a la misma carpeta que `NotFound.js`. En ese archivo, **incluye**:

```css
body {
  width: 100%;
  height: 100%;
  background: linear-gradient(90deg, rgba(212,RANDOMNUMBER200-225END,212,1) 0%, rgba(194,194,247,1) 35%, rgba(195,245,255,1) 100%);
}
```

...para que la página tenga un bonito [degradado](https://cssgradient.io/) de fondo.

Luego, **completa** `NotFound.css` con el CSS que consideres oportuno para que los elementos HTML de la página tengan las siguientes características:

* `<h1>`:
  * `text-align: center;`
* `<p>`:
  * `text-align: center;`
* `<Link>`:
  * `display: block;`
  * `width: fit-content;`
  * `margin-left: auto;`
  * `margin-right: auto;`
* `<img>`:
  * `margin-top: RANDOMNUMBER70-130ENDpx;`
  * `display: block;`
  * `margin-left: auto;`
  * `margin-right: auto;`

...y, coherentemente, **añade** a dichos elementos los `className` que consideres oportunos en `NotFound.js`.

También tendrás que **importar** el CSS desde `NotFound.js`:

```jsx
import './NotFound.css'
```

### Animaciones

Para pulir todavía más el resultado, pongamos en funcionamiento una _animación_.

En primer lugar, **añade** al final del archivo `NotFound.css` la siguiente [definición](https://reactgo.com/rotate-image-css/):

```css
@keyframes rotation{
  from{
    transform:rotate(0deg);
  }

  to{
    transform:rotate(360deg);
  }
}
```

...y luego, **añade** la siguiente línea a la clase CSS correspondiente a la _imagen_ (`<img>`):

```css
   animation: rotation infinite RANDOMNUMBER15-30ENDs linear;
```

¿Te gusta el resultado en http://localhost:3000?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
