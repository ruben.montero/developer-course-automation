Esqueleto de las páginas (II)

## :books: Resumen

* Añadiremos un párrafo adicional al `<footer>` en 4 de las páginas creadas en la tarea anterior
* Dicho párrafo mostrará información contenida en la URL, también llamada _path param_

## Descripción

Como se puede ver, en 4 de las rutas añadidas anteriormente hay _parámetros de ruta_, que son especificados mediante dos puntos (`:`):

* `/dashboards/:dashboardId`
* `/dashboards/:dashboardId/newQuestion`
* `/dashboards/:dashboardId/questions/:questionId`
* `/dashboards/:dashboardId/questions/:questionId/newAnswer`

Esto son _variables_ que gestionaremos en nuestra web, ya que queremos que se comporte de manera diferente si el cliente visita `/dashboards/12` que `/dashboards/13`, por ejemplo.

Recordemos que se necesita usar el _hook_ [`useParams`](https://reactrouterdotcom.fly.dev/docs/en/v6/hooks/use-params) de `react-router-dom`. _Algo así_:

```jsx
const DashboardDetail = () => {

  const params = useParams();
  
  return <div>Aqui muestro el parámetro: {params.dashboardId}</div>
}
```
 
## :package: La tarea

**Añade** un nuevo `<p data-cy='RANDOMEXPRESSIONfooterDebugInfo|footerDebugText|footerInfoDebug|footerTextDebugEND'>` dentro del `<footer>` de las páginas indicadas a continuación, siguiendo las especificaciones para cada una:

### `/dashboards/:dashboardId`

Dentro del `<p data-cy='RANDOMEXPRESSIONfooterDebugInfo|footerDebugText|footerInfoDebug|footerTextDebugEND'>`, **muestra** el texto:

```
RANDOMEXPRESSIONEstás visitando el dashboard con ID:|Este es el dashboard con ID:|Aquí está el dashboard con ID:END {params.dashboardId}
```

### `/dashboards/:dashboardId/newQuestion`

Dentro del `<p data-cy='RANDOMEXPRESSIONfooterDebugInfo|footerDebugText|footerInfoDebug|footerTextDebugEND'>`, **muestra** el texto:

```
Desde aquí puedes crear una nueva pregunta para el dashboard con ID: {params.dashboardId}
```

### `/dashboards/:dashboardId/questions/:questionId`

Dentro del `<p data-cy='RANDOMEXPRESSIONfooterDebugInfo|footerDebugText|footerInfoDebug|footerTextDebugEND'>`, **muestra** el texto:

```
RANDOMEXPRESSIONEstás visitando la pregunta con ID:|Esta es la pregunta con ID:|Aquí está la pregunta con ID:END {params.questionId}, que pertenece al dashboard con ID: {params.dashboardId}
```

### `/dashboards/:dashboardId/questions/:questionId/newAnswer`

Dentro del `<p data-cy='RANDOMEXPRESSIONfooterDebugInfo|footerDebugText|footerInfoDebug|footerTextDebugEND'>`, **muestra** el texto:

```
Desde aquí puedes crear una nueva respuesta para la pregunta con ID: {params.questionId}, que pertenece al dashboard con ID: {params.dashboardId}
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

