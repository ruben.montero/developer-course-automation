/// <reference types="cypress" />

describe('routes', () => {

  it('issueISSUENUMBER - main page is served and has footer', () => {
    cy.visit('http://localhost:3000/')
    
    cy.get('footer')
    .find('p[data-cy=RANDOMEXPRESSIONfooterCopyright|copyrightFooter|copyright_footer|footer_copyrightEND]')
    .contains('©')
  })

  it('issueISSUENUMBER - dashboard detail page is served and has footer', () => {
    cy.visit('http://localhost:3000/dashboards/' + Math.random().toString().slice(2, 8))
    
    cy.get('footer')
    .find('p[data-cy=RANDOMEXPRESSIONfooterCopyright|copyrightFooter|copyright_footer|footer_copyrightEND]')
    .contains('©')
  })

  it('issueISSUENUMBER - new question page is served and has footer', () => {
    cy.visit('http://localhost:3000/dashboards/' + Math.random().toString().slice(2, 8) + '/newQuestion')
    
    cy.get('footer')
    .find('p[data-cy=RANDOMEXPRESSIONfooterCopyright|copyrightFooter|copyright_footer|footer_copyrightEND]')
    .contains('©')
  })

  it('issueISSUENUMBER - question detail page is served and has footer', () => {
    cy.visit('http://localhost:3000/dashboards/' + Math.random().toString().slice(2, 8) + '/questions/' + Math.random().toString().slice(2, 8))
    
    cy.get('footer')
    .find('p[data-cy=RANDOMEXPRESSIONfooterCopyright|copyrightFooter|copyright_footer|footer_copyrightEND]')
    .contains('©')
  })

  it('issueISSUENUMBER - question detail page is served and has footer', () => {
    cy.visit('http://localhost:3000/dashboards/' + Math.random().toString().slice(2, 8) + '/questions/' + Math.random().toString().slice(2, 8) + '/newAnswer')
    
    cy.get('footer')
    .find('p[data-cy=RANDOMEXPRESSIONfooterCopyright|copyrightFooter|copyright_footer|footer_copyrightEND]')
    .contains('©')
  })
})

