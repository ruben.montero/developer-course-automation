/// <reference types="cypress" />

describe('a_simple_not_found_start', () => {
  
  it('issueISSUENUMBER - not found page is served in a random location and has correct h1', () => {
      cy.visit("http://localhost:3000/" + Math.random().toString().slice(2, 8))
      
      cy.get('div[data-cy=pageBody]')
      .find('h1[data-cy=pageHeader]')
      .should('have.text', 'RANDOMEXPRESSIONUh!|Oh!|Ouch!|Uh-oh!END');
  })

  it('issueISSUENUMBER - not found page is served in a random location and has correct p', () => {
    cy.visit("http://localhost:3000/" + Math.random().toString().slice(2, 8))
    
    cy.get('div[data-cy=pageBody]')
    .find('p[data-cy=simpleMessage]')
    .should('have.text', 'RANDOMEXPRESSIONParece que estás perdido. La página que buscas no existe|Parece que estás perdido|La página que buscas no existe|Aquí no está lo que buscas|Has llegado a un lugar desconocido... ¡Demasiado desconocido!|Has llegado lejos. Tan lejos que... ¿Dónde estás?END');
  })

it('issueISSUENUMBER - not found page is served in a random location and has correct Link', () => {
  cy.visit("http://localhost:3000/" + Math.random().toString().slice(2, 8))
  
  cy.get('div[data-cy=pageBody]')
  .find('a')
  .should('have.attr', 'href', '/')
  .should('have.text', 'RANDOMEXPRESSIONVolver a casa|Volver|Regresar|Página principalEND')
  .click();

  cy.url()
  .should('eq', 'http://localhost:3000/')
  })
})
