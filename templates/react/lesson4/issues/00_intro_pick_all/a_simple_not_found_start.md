Not Found

## :books: Resumen

* Actualizaremos nuestro repositorio con `git pull` y comenzaremos a trabajar en la nueva carpeta `react-stackoverrun/`
* Instalaremos [`react-router-dom`](https://reactrouterdotcom.fly.dev/)
* Implementaremos un `<Router/>` con la ruta _wildcard_ (*) y una página de _Not Found_

## Descripción

¡Vamos a comenzar a trabajar en un nuevo proyecto!

El objetivo de este _sprint_ será crear una versión mejorada de `react-dashboards`. Crearemos `react-stackoverrun`, que será similar, pero permitirá a los usuarios registrarse y loguearse.

Quién sabe, quizá le hagamos competencia a [stack overflow](https://stackoverflow.com/) :smiley:

## :package: La tarea

En primer lugar, **efectúa** `git pull` de tu repositorio para asegurarte que estás al día.

Después, desde un terminal (cmd.exe), **posiciónate** (`cd`) en la carpeta `react-stackoverrun/react-frontend/` y **ejecuta**:

```
npm install
```

Cuando termine, para instalar la librería `react-router-dom`, **ejecuta**:

```
npm install react-router-dom
```

### Página _Not Found_

**Crea** el fichero (y las carpetas intermedias) `src/screens/not_found/NotFound.js`:

```jsx
const NotFound = () => {

  return <div data-cy='pageBody'></div>
}

export default NotFound;
```

Y, dentro del `<div data-cy='pageBody'>` principal, **añade**:

* Un `<h1 data-cy='pageHeader'>` que contenga el texto `RANDOMEXPRESSIONUh!|Oh!|Ouch!|Uh-oh!END`
* Un `<p data-cy='simpleMessage'>` que contenga el texto `RANDOMEXPRESSIONParece que estás perdido. La página que buscas no existe|Parece que estás perdido|La página que buscas no existe|Aquí no está lo que buscas|Has llegado a un lugar desconocido... ¡Demasiado desconocido!|Has llegado lejos. Tan lejos que... ¿Dónde estás?END`
* Un `<Link data-cy='homeLink'>` que redirija a `'/'` y tenga el texto `RANDOMEXPRESSIONVolver a casa|Volver|Regresar|Página principalEND` _(De momento el `Link` no tendrá efecto, aparentemente. En el futuro añadiremos la pantalla `/`)_ 

### Ruta _Not Found_

En `App.js`, **elimina** el contenido por defecto:

```diff
function App() {
  return (
-    <div className="App">
-      <header className="App-header">
-        <img src={logo} className="App-logo" alt="logo" />
-        <p>
-          Edit <code>src/App.js</code> and save to reload.
-        </p>
-        <a
-          className="App-link"
-          href="https://reactjs.org"
-          target="_blank"
-          rel="noopener noreferrer"
-        >
-          Learn React
-        </a>
-      </header>
-    </div>
  );
}

export default App;
```

...y en su lugar **añade** el siguiente esquema de rutas:

```jsx
import { BrowserRouter, Routes, Route } from "react-router-dom";
import NotFound from './screens/not_found/NotFound';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="*" element={<NotFound/>}></Route>
      </Routes>
    </BrowserRouter>
  );
}
```

Prueba a lanzar el servidor con `npm run start`

¿Ves correctamente tu _Not Found_, sea cual sea la página a la que navegues en http://localhost:3000?

¡Enhorabuena! Has arrancado el proyecto con éxito.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

No está de más verificar que tu _commit_ se ha subido correctamente desde GitLab (https://raspi).
