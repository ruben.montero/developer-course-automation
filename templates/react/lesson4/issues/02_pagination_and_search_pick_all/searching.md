Búsquedas en APIs REST

## :books: Resumen

* Añadiremos un estado `searchText` a `QuestionDetail.js`
* Añadiremos un formulario para permitir introducir un texto de búsqueda
* Cuando se haga `submit` del formulario, mandaremos una petición al API REST para refrescar la lista de respuestas que encajen con el resultado

## Descripción

Nuestro API REST soporta _buscar_ mediante el _query param_ `search` en los mismos _endpoints_ que soportan paginación:

* `/api/v2/dashboards/<dashboardId>`
* `/api/v2/dashboards/<dashboardId>/questions/<questionId>`

Vamos, ahora, a usar el segundo _endpoint_ para implementar _buscar respuestas_.

Por ejemplo, ¿qué datos devuelve esta petición?

* GET http://raspi:8081/api/v2/dashboards/1/questions/1

En contraposición, ¿qué datos devuelve esta otra? (al mismo _endpoint_)

* GET http://raspi:8081/api/v2/dashboards/1/questions/1?search=ayudarte

### Nuestro objetivo...

...es permitir a los usuarios de nuestra web aprovechar esta funcionalidad, y que puedan filtrar las respuestas de `QuestionDetail.js` en base a un texto de búsqueda.

## :package: La tarea

**Añade** en `QuestionDetail.js` un _estado_ que represente el texto _actual_ en el campo de búsqueda:

```jsx
  const [searchText, setSearchText] = useState('');
```

Luego, **añade** un `<form>` con las siguientes características:

* Estará ubicado después del `<h3>`
* La etiqueta `<form>` tendrá `data-cy='RANDOMEXPRESSIONsearchForm|formEND'` y `onSubmit={onSubmit}` _(Escribiremos la función `onSubmit` a continuación)_
* Contendrá una primera etiqueta `<input>` con:
  * `data-cy='RANDOMEXPRESSIONsearchInput|inputSearchEND'`
  * `type='text'`
  * `placeholder='RANDOMEXPRESSIONsolucionado|problema resueltoEND'`
  * `value={searchText}`
  * `onChange={onChange}` _(Escribiremos la función `onChange` a continuación)_
* Contendrá una segunda etiqueta `<input>` con:
  * `data-cy='RANDOMEXPRESSIONsubmitInput|inputSubmitEND'`
  * `type='submit'`
  * `value='RANDOMEXPRESSIONBuscar|Realizar búsqueda|BúsquedaEND'`
  
Luego, **añade** la función `onChange`, que se encarga de reflejar en el estado `searchText` lo que el usuario escribe:

```jsx
  const onChange = (e) => {
    setSearchText(e.target.value);
  }
```

Para terminar, **añade** la función `onSubmit`[^1]:

```jsx
  const onSubmit = (e) => {
    e.preventDefault();
    
    // Aquí lanzamos la búsqueda
    axios.get('http://raspi:8081/api/v2/dashboards/' + params.dashboardId + '/questions/' + params.questionId + '?search=' + searchText).then(response => {
      setAnswers(response.data.answers)
    })
  }
```

Como puedes observar, construye la URL apropiada para la petición HTTP (incluyendo el parámetro `?search=` y lanza la petición para que el API REST devuelva las _respuestas_ que coinciden con la búsqueda[^2].

¡**Verifica** en http://localhost:3000/dashboards/1/questions/1 que funciona correctamente!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: En contraposición al ejemplo de paginación, ahora _no_ reutilizamos el `useEffect`. Así, la petición sólo se envía cuando hacemos `onSubmit` (y no con cada carácter que teclea el usuario). ¿Es mejor? ¿Es peor? ¿Qué opinas?

[^2]: Esto es una búsqueda _lado servidor_. Si el conjunto de datos no fuera excesivo, podríamos plantearnos una búsqueda (filtrado) _lado cliente_. Para ello, descargaríamos todos los datos y los filtraríamos en local usando [`.filter`](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)

