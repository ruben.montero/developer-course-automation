Paginación... ¿y si se acaban las páginas?

## :books: Resumen

* Ocultaremos el botón _RANDOMEXPRESSIONCargar más|Cargar más elementos|Más elementosEND_ en `DashboardDetail.js` si, y sólo si, después de clicarlo se ha obtenido una página vacía (sin elementos)

## Descripción

Ahora mismo el botón para cargar más elementos se muestra siempre. Si llegas a la última página y no hay más elementos... ¡Sigue ahí!

## :package: La tarea

En `DashboardDetail.js`, **añade** la siguiente función:

```jsx
  const shouldHideLoadMore = () => {
    if (questions.length > 0) {
        const lastQuestion = questions[questions.length - 1]
        return lastQuestion.created_at === newPageOlderThan;
    }
    return true; // Si hay preguntas (questions vacío), ocultar botón
  }
```

Como ves, tiene la responsabilidad de devolver un valor _booleano_ `true` si el botón _RANDOMEXPRESSIONCargar más|Cargar más elementos|Más elementosEND_ se debe ocultar. Devolverá `false` en caso contrario.

En concreto, devuelve `true` si la fecha de la última pregunta es _igual_ a `newPageOlderThan`[^1]:

Ahora, **modifica** el `<button>` **así**:

```diff
-<button data-cy='RANDOMEXPRESSIONloadMoreButton|moreItemsButton|loadButtonEND' onClick={onClickLoadMore}>RANDOMEXPRESSIONCargar más|Cargar más elementos|Más elementosEND</button>
+<button data-cy='RANDOMEXPRESSIONloadMoreButton|moreItemsButton|loadButtonEND' hidden={ shouldHideLoadMore() } onClick={onClickLoadMore}>RANDOMEXPRESSIONCargar más|Cargar más elementos|Más elementosEND</button>
```

**Verifica** su funcionamiento. ¿El botón ahora se oculta en http://localhost:3000/dashboards/2 cuando no hay más _preguntas_ que cargar?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: ¿Comprendes _por qué_ esto es suficiente para decidir si el botón debe estar oculto?
