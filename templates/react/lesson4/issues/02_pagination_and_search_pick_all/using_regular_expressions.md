Validación mediante expresiones regulares

## :books: Resumen

* Introduciremos brevemente el concepto de _expresión regular_
* Añadiremos un `if` al método `onSubmit` de `QuestionDetail.js` para validar la entrada con una expresión regular. Si no se cumple, mostraremos una alerta al usuario

## Descripción

Las [expresiones regulares](https://platzi.com/blog/que-expresion-regular/) son patrones que sirven para encontrar ciertas combinacións de caracteres en un texto. Permiten efectuar filtrados, sustituciones...

No son algo exclusivo de JavaScript, sino que su uso está extendido en muchos lenguajes de programación y tipos de sistemas.

¿En qué [consisten](https://es.wikipedia.org/wiki/Expresi%C3%B3n_regular)?

Sencillamente, en unas reglas mediante las cuales produciremos una expresión. Algunos [ejemplos](https://support.google.com/a/answer/1371417?hl=es) son:

* Coincidencia con cualquier dirección IP que se incluya en el intervalo de 192.168.1.0 a 192.168.1.255.

```
192\.168\.1\.\d{1,3}
```

* Coincidencia con cualquier dirección de correo electrónico de los dominios yahoo.com, hotmail.com y gmail.com:

```
(\W|^)[\w.\-]{0,25}@(yahoo|hotmail|gmail)\.com(\W|$)
```

### Lo que nosotros necesitamos

Para nuestro sencillo caso de uso, sólo necesitamos saber que:

* En JavaScript, las expresiones regulares van englobadas en caracteres _slash_ (`/`)
* Se puede comprobar si una cadena de texto _matchea_ una expresión regular usando el método [match](https://www.freecodecamp.org/news/javascript-regex-match-example-how-to-use-the-js-replace-method-on-a-string/):

```javascript
if (miString.match(/EXPRESION REGULAR/)) {
  // ...
}
```

* La siguiente expresión regular _matchea_ cualquier _string_ con, exactamente, 2 letras del abecedario:

```
/.*[a-zA-Z]{2}/
```

* La siguiente expresión regular _matchea_ cualquier _string_ con, al menos, 2 letras del abecedario:

```
/.*[a-zA-Z]{2,}/
```

* La siguiente expresión regular _matchea_ cualquier _string_ con, al menos, 3 letras del abecedario:

```
/.*[a-zA-Z]{3,}/
```

## :package: La tarea

**Añade** un `if` al método `onSubmit` en `QuestionDetail.js`:

* Irá justo debajo de `e.preventDefault()`
* Validará que el texto introducido contiene, al menos, RANDOMNUMBER2-6END caracteres que sean letras del abecedario
* Si **no** (`!`) _matchea_ la expresión regular, mostrará una alerta al usuario y hará `return`
* Para mostrar la alerta, se usará la línea:

```javascript
alert('Introduce al menos RANDOMNUMBER2-6END letras')
```

**Verifica** este comportamiento en http://localhost:3000/dashboards/1/questions/1

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_. 
