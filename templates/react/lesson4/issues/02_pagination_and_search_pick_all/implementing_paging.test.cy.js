describe('implementing_paging', () => {
  beforeEach(() => {
    cy.intercept('GET', '**/api/v2/dashboards/2?**', (req) => {
      if ((req.query['page_size'] == 5) && (req.query['older_than'] == 1234)) {
        req.reply({statusCode: 200, fixture: 'dashboard2-size5-page2_v2.json' })
      } else if ((req.query['page_size'] == 5) && (req.query['older_than'] == 5555)) {
        req.alias = "correctSecondPageRequest"
        req.reply({statusCode: 200, fixture: 'dashboard2-empty_v2.json' })
      } else if (req.query['page_size'] == 5) {
        req.reply({statusCode: 200, fixture: 'dashboard2-size5_v2.json' })
      } else {
        req.reply({statusCode: 400 }) // Developer has sent a wrong request!
      }
    })
  })

  it('issueISSUENUMBER - dashboard 2 page has correct load button', () => {
    cy.visit('http://localhost:3000/dashboards/2')
    cy.wait(1000)

    cy.get('button[data-cy=RANDOMEXPRESSIONloadMoreButton|moreItemsButton|loadButtonEND]')
    .should('have.text', 'RANDOMEXPRESSIONCargar más|Cargar más elementos|Más elementosEND')
  })

  it('issueISSUENUMBER - dashboard 2 page has correct amount of questions after click', () => {
    cy.visit('http://localhost:3000/dashboards/2')
    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONquestionsList|listOfQuestions|questions_list|list_of_questionsEND]')
    .find('div')
    .should('have.length', 5)

    cy.get('button[data-cy=RANDOMEXPRESSIONloadMoreButton|moreItemsButton|loadButtonEND]')
    .click()

    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONquestionsList|listOfQuestions|questions_list|list_of_questionsEND]')
    .find('div')
    .should('have.length', 7)
  })

  it('issueISSUENUMBER - dashboard 2 page has correct question 1 content after click', () => {
    cy.visit('http://localhost:3000/dashboards/2')
    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONquestionsList|listOfQuestions|questions_list|list_of_questionsEND]')
    .find('div')
    .should('have.length', 5)

    cy.get('button[data-cy=RANDOMEXPRESSIONloadMoreButton|moreItemsButton|loadButtonEND]')
    .click()

    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONquestionsList|listOfQuestions|questions_list|list_of_questionsEND]')
    .find('div')
    .eq(5)
    .find('h3')
    .should('have.text', 'React Router Link')

    cy.get('div[data-cy=RANDOMEXPRESSIONquestionsList|listOfQuestions|questions_list|list_of_questionsEND]')
    .find('div')
    .eq(5)
    .find('p')
    .should('have.text', '¿Qué es un Link?')
  })

  it('issueISSUENUMBER - dashboard 2 page has correct question 2 content after click', () => {
    cy.visit('http://localhost:3000/dashboards/2')
    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONquestionsList|listOfQuestions|questions_list|list_of_questionsEND]')
    .find('div')
    .should('have.length', 5)

    cy.get('button[data-cy=RANDOMEXPRESSIONloadMoreButton|moreItemsButton|loadButtonEND]')
    .click()

    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONquestionsList|listOfQuestions|questions_list|list_of_questionsEND]')
    .find('div')
    .eq(6)
    .find('h3')
    .should('have.text', 'ReactNative')

    cy.get('div[data-cy=RANDOMEXPRESSIONquestionsList|listOfQuestions|questions_list|list_of_questionsEND]')
    .find('div')
    .eq(6)
    .find('p')
    .should('have.text', 'He escuchado hablar de React Native... ¿Qué es?')
  })


  it('issueISSUENUMBER - dashboard 2 page correctly sends load more request in 2nd page', () => {
    cy.visit('http://localhost:3000/dashboards/2')
    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONquestionsList|listOfQuestions|questions_list|list_of_questionsEND]')
    .find('div')
    .should('have.length', 5)

    cy.get('button[data-cy=RANDOMEXPRESSIONloadMoreButton|moreItemsButton|loadButtonEND]')
    .click()

    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONquestionsList|listOfQuestions|questions_list|list_of_questionsEND]')
    .find('div')
    .should('have.length', 7)

    cy.get('button[data-cy=RANDOMEXPRESSIONloadMoreButton|moreItemsButton|loadButtonEND]')
    .click()

    cy.wait('@correctSecondPageRequest')
  })
})
