describe('beginning_to_understand_pagination', () => {
  beforeEach(() => {
    cy.intercept('GET', '**/api/v2/dashboards/2?**', (req) => {
      if (req.query['page_size'] == 5) {
        req.reply({statusCode: 200, fixture: 'dashboard2-size5_v2.json' })
      } else {
        req.reply({statusCode: 400 }) // Developer has sent a wrong request!
      }
    })
  })


  it('issueISSUENUMBER - dashboard 2 page has correct amount of questions', () => {
    cy.visit('http://localhost:3000/dashboards/2')
    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONquestionsList|listOfQuestions|questions_list|list_of_questionsEND]')
    .find('div')
    .should('have.length', 5)
  })
})

