Paginación en APIs REST

## :books: Resumen

* Comprenderemos la necesidad de paginación cuando se consumen datos en una aplicación
* Hablaremos de un tipo sencillo de paginación con `offset` y `size`
* Añadiremos `page_size` a la petición de _detalle de dashboard_ (lista de preguntas) para empezar a trabajar

## Descripción

Hasta ahora, cada vez que atacamos un _endpoint_ y consumimos datos mediante un HTTP GET, se devuelven unos pocos.

Por ejemplo, el _endpoint_ de _dashboards_ http://raspi:8081/api/v2/dashboards devuelve `5` datos. ¡Qué pocos!

¿Y si hubiera más? ¿Y si hubiera 1.000.000.000?

Cuando se diseña e implementa un _endpoint_ REST que previsiblemente accede a _muchos_ datos, se suele implementar paginación.

### Paginación

Imagina que un _endpoint_ ficticio:

* GET http://servidorDeEjemplo:8081/mensajes

...devuelve los siguientes datos:

|Posición|Contenido|Fecha|
|-|-|-|
|1| ¡Empezamos la semana!                   | Lunes (9:00) |
|2| Ni te cases ni te embarques             | Martes (9:00) |
|3| Media semana, ¡qué bien!                | Miércoles (9:00) |
|4| El día de Júpiter                       | Jueves (9:00) |
|5| Hoy empieza el fin de semana :innocent: | Viernes (9:00) |
|6| ¡Un sábado de deporte!                  | Sábado (9:00) |
|7| ¡Bonito domingo de callos!              | Domingo (9:00) |

Si el API REST lo soporta podemos implementar una [versión sencilla de paginación](https://www.moesif.com/blog/technical/api-design/REST-API-Design-Filtering-Sorting-and-Pagination/#offset-pagination) empleando dos [_parámetros de query_ ó _query params_](https://rapidapi.com/blog/api-glossary/parameters/query/)[^1]:

* `offset`: Posicion desde donde extraer
* `size`:  Número de datos máximo a devolver

Así, por ejemplo, podríamos consumir los datos en páginas de 3 en 3:

* GET http://servidorDeEjemplo:8081/mensajes?offset=0&size=3 _(página 1)_

|Posición|Contenido|Fecha|
|-|-|-|
|1| ¡Empezamos la semana!                   | Lunes (9:00) |
|2| Ni te cases ni te embarques             | Martes (9:00) |
|3| Media semana, ¡qué bien!                | Miércoles (9:00) |

* GET http://servidorDeEjemplo:8081/mensajes?offset=3&size=3 _(página 2)_

|Posición|Contenido|Fecha|
|-|-|-|
|4| El día de Júpiter                       | Jueves (9:00) |
|5| Hoy empieza el fin de semana :innocent: | Viernes (9:00) |
|6| ¡Un sábado de deporte!                  | Sábado (9:00) |

* GET http://servidorDeEjemplo:8081/mensajes?offset=6&size=3 _(página 3)_

|Posición|Contenido|Fecha|
|-|-|-|
|7| ¡Bonito domingo de callos!              | Domingo (9:00) |

## :package: La tarea

Las siguientes peticiones en nuestro API REST soportan paginación:

* `/api/v2/dashboards/<dashboardId>`
* `/api/v2/dashboards/<dashboardId>/questions/<questionId>`

**Visita** http://localhost:3000/dashboards/2. Se ven 7 elementos, ¿verdad?

Ahora, **modifica** `DashboardDetail.js` para que en la petición al API REST se envíe el parámetro `?page_size=5`.

```diff
  useEffect(() => {
-   axios.get('http://raspi:8081/api/v2/dashboards/' + params.dashboardId).then(response => {
+   axios.get('http://raspi:8081/api/v2/dashboards/' + params.dashboardId + '?page_size=5').then(response => {
      setDashboardTitle(response.data.title);
      setDashboardDescription(response.data.description);
      setQuestions(response.data.questions);
    })
  }, [])
```

**Visita** de nuevo http://localhost:3000/dashboards/2. Ahora sólo se ven 5 elementos, ¿verdad?

Continuaremos implementando paginación en la siguiente tarea.

¡Spoiler! No será mediante un parámetro `offset`, pues presenta sus problemillas :wink: 

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Los _query params_ son parte del [estándar HTTP](https://www.w3.org/Protocols/). Viajan en la URL tras un signo de interrogación (`?key=value`). Si hay más de uno, se anexan con un _ampersand_  (`?key1=value1&key2=value2&key3=value3`)
