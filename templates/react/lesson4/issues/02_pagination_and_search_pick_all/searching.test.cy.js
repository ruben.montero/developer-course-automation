describe('searching', () => {
  beforeEach(() => {
    cy.intercept('GET', '**/api/v2/dashboards/1/questions/1**', (req) => {
      if (req.query['search'] == 'documentación') {
        req.reply({statusCode: 200, fixture: 'question1-d1-searchR2_v2.json' })
      } else if (req.query['search'] == 'Asteroide') {
        req.reply({statusCode: 200, fixture: 'question1-d1-searchR0_v2.json' })
      } else {
        req.reply({statusCode: 200, fixture: 'question1-d1_v2.json' })
      }
    })
  })

  it('issueISSUENUMBER - dashboards/1/questions/1 form has correct placeholder and submit text', () => {
    cy.visit('http://localhost:3000/dashboards/1/questions/1')

    cy.get('form[data-cy=RANDOMEXPRESSIONsearchForm|formEND]')
    .find('input[data-cy=RANDOMEXPRESSIONsearchInput|inputSearchEND]')
    .should('have.attr', 'placeholder', 'RANDOMEXPRESSIONsolucionado|problema resueltoEND')

    cy.get('form[data-cy=RANDOMEXPRESSIONsearchForm|formEND]')
    .find('input[data-cy=RANDOMEXPRESSIONsubmitInput|inputSubmitEND]')
    .should('have.attr', 'value', 'RANDOMEXPRESSIONBuscar|Realizar búsqueda|BúsquedaEND')
  })

  it('issueISSUENUMBER - dashboards/1/questions/1 has correct amount of results when searching documentación', () => {
    cy.visit('http://localhost:3000/dashboards/1/questions/1')
    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONanswersList|listOfAnswers|answers_list|list_of_answersEND]')
    .find('div')
    .should('have.length', 6);

    cy.get('form[data-cy=RANDOMEXPRESSIONsearchForm|formEND]')
    .find('input[data-cy=RANDOMEXPRESSIONsearchInput|inputSearchEND]')
    .type('documentación')

    cy.get('form[data-cy=RANDOMEXPRESSIONsearchForm|formEND]')
    .find('input[data-cy=RANDOMEXPRESSIONsubmitInput|inputSubmitEND]')
    .click()

    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONanswersList|listOfAnswers|answers_list|list_of_answersEND]')
    .find('div')
    .should('have.length', 2);
  })

  it('issueISSUENUMBER - dashboards/1/questions/1 has correct results content when searching documentación', () => {
    cy.visit('http://localhost:3000/dashboards/1/questions/1')
    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONanswersList|listOfAnswers|answers_list|list_of_answersEND]')
    .find('div')
    .should('have.length', 6);

    cy.get('form[data-cy=RANDOMEXPRESSIONsearchForm|formEND]')
    .find('input[data-cy=RANDOMEXPRESSIONsearchInput|inputSearchEND]')
    .type('documentación')

    cy.get('form[data-cy=RANDOMEXPRESSIONsearchForm|formEND]')
    .find('input[data-cy=RANDOMEXPRESSIONsubmitInput|inputSubmitEND]')
    .click()

    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONanswersList|listOfAnswers|answers_list|list_of_answersEND]')
    .find('div')
    .should('have.length', 2);

    cy.get('div[data-cy=RANDOMEXPRESSIONanswersList|listOfAnswers|answers_list|list_of_answersEND]')
    .find('div')
    .first()
    .find('p')
    .should('have.text', 'Aquí hay algo más de documentación https://www.udacity.com/blog/2021/01/javascript-for-loop.html')

    cy.get('div[data-cy=RANDOMEXPRESSIONanswersList|listOfAnswers|answers_list|list_of_answersEND]')
    .find('div')
    .eq(1)
    .find('p')
    .should('have.text', 'Aquí hay un enlace de documentación que puede ayudarte https://www.w3schools.com/js/js_loop_for.asp')
  })

  it('issueISSUENUMBER - dashboards/1/questions/1 has correct amount of results when searching Asteroide', () => {
    cy.visit('http://localhost:3000/dashboards/1/questions/1')
    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONanswersList|listOfAnswers|answers_list|list_of_answersEND]')
    .find('div')
    .should('have.length', 6);

    cy.get('form[data-cy=RANDOMEXPRESSIONsearchForm|formEND]')
    .find('input[data-cy=RANDOMEXPRESSIONsearchInput|inputSearchEND]')
    .type('Asteroide')

    cy.get('form[data-cy=RANDOMEXPRESSIONsearchForm|formEND]')
    .find('input[data-cy=RANDOMEXPRESSIONsubmitInput|inputSubmitEND]')
    .click()

    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONanswersList|listOfAnswers|answers_list|list_of_answersEND]')
    .find('div')
    .should('have.length', 0);
  })

  it('issueISSUENUMBER - dashboards/1/questions/1 has correct amount of results after searching twice', () => {
    cy.visit('http://localhost:3000/dashboards/1/questions/1')
    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONanswersList|listOfAnswers|answers_list|list_of_answersEND]')
    .find('div')
    .should('have.length', 6);

    cy.get('form[data-cy=RANDOMEXPRESSIONsearchForm|formEND]')
    .find('input[data-cy=RANDOMEXPRESSIONsearchInput|inputSearchEND]')
    .type('Asteroide')

    cy.get('form[data-cy=RANDOMEXPRESSIONsearchForm|formEND]')
    .find('input[data-cy=RANDOMEXPRESSIONsubmitInput|inputSubmitEND]')
    .click()

    cy.wait(1000)

    cy.get('div[data-cy=RANDOMEXPRESSIONanswersList|listOfAnswers|answers_list|list_of_answersEND]')
    .find('div')
    .should('have.length', 0);

    cy.get('form[data-cy=RANDOMEXPRESSIONsearchForm|formEND]')
    .find('input[data-cy=RANDOMEXPRESSIONsearchInput|inputSearchEND]')
    .clear()
    .type('documentación')

    cy.get('form[data-cy=RANDOMEXPRESSIONsearchForm|formEND]')
    .find('input[data-cy=RANDOMEXPRESSIONsubmitInput|inputSubmitEND]')
    .click()

    cy.get('div[data-cy=RANDOMEXPRESSIONanswersList|listOfAnswers|answers_list|list_of_answersEND]')
    .find('div')
    .should('have.length', 2);
  })
})

