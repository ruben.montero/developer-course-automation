¡Trabajo terminado!

## :trophy: ¡Felicidades!

Has realizado las tareas del _sprint_ y has validado tu trabajo con los _tests_. ¡Enhorabuena!

<img src="react-stackoverrun/react-frontend/docs/sunset.jpg" width=500 />

Has...

* Repasado el use de `<Route />` con `path="*"`para crear una página de _not found_
* Aprendido sobre qué es un CDN
* Reimplementado pantallas que mandan peticiones HTTP GET para obtener datos y pintarlos como listas
* Usado _query params_ (que van en la URL tras el `?`)
* Leído sobre un escenario de paginación sencilla con `offset` y `size`
* Implementado paginación con `filter` y `size` 
* Implementado una búsqueda lado servidor con `?search=`, que es más eficiente para volúmenes grandes de datos. En contraposición, para volúmenes pequeños, podríamos hacer un `array.filter()` en local
* Usado expresiones regulares sencillas
* Implementado un formulario de registro que manda un POST para guardar un nuevo usuario
* Implementado un formulario de _login_ que manda un POST y recibe un _token_ de sesión
* Empleado `sessionStorage` (más didáctico que profesional) para almacenar un _token_ de sesión
* Aprendido a enviar cabeceras HTTP con `axios`
* Enviado la cabecera `Session-Token` en múltiples peticiones para acreditar la identidad del cliente
* Usado `.catch` para controlar los escenarios de error en las peticiones HTTP
* Usado rutas anidadas y `<Outlet />` para crear un contenedor que tiene una barra superior y contiene a cada otra página
* Usado `useOutletContext` para pasar un estado de un contenedor a una ruta hija
* Implementado una petición HTTP DELETE para desloguearse