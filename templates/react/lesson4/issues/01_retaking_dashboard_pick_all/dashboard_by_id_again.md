Implementar detalle de un dashboard (otra vez)

## :books: Resumen

* Implementaremos la página `DashboardDetail.js`

## Descripción

Cada _dashboard_ tiene una la lista de preguntas asociadas:

```mermaid
graph TD
    D[Dashboard ] --> B[Question 1]
    D[Dashboard ] --> A[Question 2]
    D[Dashboard ] --> C[Question N]
```

Vamos a trabajar en la página que muestra el detalle de un _dashboard_.

El _endpoint_ REST que consumiremos (según qué _dashboard_ queramos mostrar, e.g.: 1, 2,...) será:

* http://raspi:8081/api/v2/dashboards/1
* http://raspi:8081/api/v2/dashboards/2
* ...

¡Al lío!

## :package: La tarea

### Una celda

**Crea** un fichero `src/components/question_list_item/QuestionListItem.css`:
  
```css
.question-li {
  margin: RANDOMNUMBER25-40ENDpx;
  padding: 5px;
  background-color: rgba(RANDOMNUMBER220-240END, 230, 243, 0.5);
  text-align: center;
  border-radius: 15px;
  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px 0px;
}

.question-li:hover {
  cursor: pointer;
  background-color: rgb(220, 230, 230);
}
```

Y, luego, **crea** `QuestionListItem.js` en la misma carpeta:

```jsx
import "./QuestionListItem.css"
import { useNavigate } from 'react-router-dom';

const QuestionListItem = (props) => {
  const navigate = useNavigate();
  
  const onClick = () => {
    navigate('questions/'+props.question.question_id)
  }

  return <div onClick={onClick} className='question-li' key={props.question.question_id}>
    <h3>{props.question.title}</h3>
    <p>{props.question.description}</p>
  </div>
}

export default QuestionListItem;
```

### Pedir los datos y mostrarlos

Primero, en `DashboardDetail.js` **añade** las dos líneas necesarias para poder usar los _parámetros_ de la URL:

```diff
+import { useParams } from "react-router-dom";

const DashboardDetail = (props) => {
+  const params = useParams();
```

Y luego **añade** los siguientes estados:

```jsx
  const [dashboardTitle, setDashboardTitle] = useState('');
  const [dashboardDescription, setDashboardDescription] = useState('');
  const [questions, setQuestions] = useState([]);
```

Ahora, lo más importante.

**Añade** este `useEffect` para pedir al API REST los datos detallados del _dashboard_:

```jsx
  useEffect(() => {
    axios.get('http://raspi:8081/api/v2/dashboards/' + params.dashboardId).then(response => {
      setDashboardTitle(response.data.title);
      setDashboardDescription(response.data.description);
      setQuestions(response.data.questions);
    })
  }, [])
```

Y, para terminar, **modifica** el `return` en `DashboardDetail.js` para añadir lo siguiente dentro del `<div>` principal (encima del `<footer>`):

```jsx
    <h1 data-cy="RANDOMEXPRESSIONpageHeader|header|screenHeaderEND">{dashboardTitle}</h1>
    <h3 data-cy="RANDOMEXPRESSIONpageDescription|description|screenDescriptionEND">{dashboardDescription}</h3>
    <div data-cy="RANDOMEXPRESSIONquestionsList|listOfQuestions|questions_list|list_of_questionsEND">
      { questions.map( q => { return <QuestionListItem question={q} /> }) }
    </div>
```

Ya puedes verificar que si clicas en cada uno de los _dashboards_ de http://localhost:3000 te lleva a un detalle donde ves correctamente los datos del _dashboard_ y sus preguntas.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
