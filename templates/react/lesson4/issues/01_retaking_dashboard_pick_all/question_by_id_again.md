Implementar detalle de una pregunta (otra vez)

## :books: Resumen

* Implementaremos la página `QuestionDetail.js`

## Descripción

Abordemos la página de _pregunta_ (donde se muestra una lista de _respuestas_).

## :package: La tarea

### Una celda

**Crea** un fichero `src/components/answer_list_item/AnswerListItem.css`:
  
```css
.answer-li {
  margin: RANDOMNUMBER25-40ENDpx;
  padding: 5px;
  background-color: rgba(RANDOMNUMBER220-240END, 230, 243, 0.5);
  text-align: center;
  border-radius: 15px;
  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px 0px;
}
```

Y, luego, **crea** `AnswerListItem.js` en la misma carpeta:

```jsx
import "./AnswerListItem.css"

const AnswerListItem = (props) => {

  return <div className='answer-li' key={props.answer.answer_id}>
    <p>{props.answer.description}</p>
  </div>
}

export default AnswerListItem;
```

### Pedir los datos y mostrarlos

Primero, en `QuestionDetail.js` **añade** las dos líneas necesarias para poder usar los _parámetros_ de la URL:

```diff
+import { useParams } from "react-router-dom";

const QuestionDetail = (props) => {
+  const params = useParams();
```

Y luego **añade** los siguientes estados:

```jsx
  const [questionTitle, setQuestionTitle] = useState('');
  const [question, setQuestion] = useState('');
  const [answers, setAnswers] = useState([]);
```

Ahora, lo más importante.

**Añade** este `useEffect` para pedir al API REST los datos detallados de una _pregunta_:

```jsx
  useEffect(() => {
    axios.get('http://raspi:8081/api/v2/dashboards/' + params.dashboardId + '/questions/' + params.questionId).then(response => {
      setQuestionTitle(response.data.question_title);
      setQuestionDescription(response.data.question);
      setAnswers(response.data.answers);
    })
  }, [])
```

Y, para terminar, **modifica** el `return` en `QuestionDetail.js` para añadir lo siguiente dentro del `<div>` principal (encima del `<footer>`):

```jsx
    <h1 data-cy="RANDOMEXPRESSIONpageHeader|header|screenHeaderEND">{questionTitle}</h1>
    <h3 data-cy="RANDOMEXPRESSIONpageDescription|description|screenDescriptionEND">{question}</h3>
    <div data-cy="RANDOMEXPRESSIONanswersList|listOfAnswers|answers_list|list_of_answersEND">
      { answers.map( a => { return <AnswerListItem answer={a} /> }) }
    </div>
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
