Implementar lista de dashboards (otra vez)

## :books: Resumen

* Implementaremos la página `Dashboards.js`

## Descripción

¿Recuerdas la metáfora del _museo de juguetes_?

<img src="react-stackoverrun/react-frontend/docs/consuming-rest-api-metaphor-explained.png" width=650 />

¡Volvamos a implementar una página que muestra _dashboards_!

En esta ocasión, nuestro [API REST](https://www.redhat.com/es/topics/api/what-is-a-rest-api) tiene el prefijo `/api/v2`. Puedes explorar su especificación en el fichero `react-stackoverrun/django-backend/doc/swagger/swagger.yaml` de tu repositorio.

## :package: La tarea

**Instala** `axios` (`npm install axios`) en `react-stackoverrun/react-frontend/`.

### Una celda

**Crea** un fichero `src/components/dashboard_list_item/DashboardListItem.css`:
  
```css
.dashboard-li {
  margin: RANDOMNUMBER15-25ENDpx;
  padding: 5px;
  background-color: rgba(RANDOMNUMBER230-240END, 240, 240, 0.5);
  text-align: center;
  border-radius: 10px;
  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px 0px;
}

.dashboard-li:hover {
  cursor: pointer;
  background-color: rgb(220, 230, 230);
}
```

Y, luego, **crea** `DashboardListItem.js` en la misma carpeta:

```jsx
import "./DashboardListItem.css"
import { useNavigate } from 'react-router-dom';

const DashboardListItem = (props) => {
  const navigate = useNavigate();
  
  const onClick = () => {
    navigate('/dashboards/'+props.dashboard.id)
  }

  return <div onClick={onClick} className='dashboard-li' key={props.dashboard.id}>
    <h3>{props.dashboard.title}</h3>
    <p>{props.dashboard.description}</p>
  </div>
}

export default DashboardListItem;
```

### Pedir los datos y mostrarlos en celdas

**Añade** a `Dashboards.js` un estado `dashboards`:

```jsx
  const [dashboards, setDashboards] = useState([])
```

Más abajo, en `Dashboards.js`, **añade** un `useEffect` que se encarga de pedir los datos (GET) a http://raspi:8081/api/v2/dashboards y usa `.then` para gestionar la respuesta:

```jsx
  useEffect(() => {
    axios.get('http://raspi:8082/api/v1/dashboards').then(response => {
      setDashboards(response.data);
    })
  }, [])
```

Y, finalmente, **incluye** en el `return` (dentro del `<div>` principal, por _encima_ del `<footer>`) el siguiente `<div>`:

```jsx
    <div data-cy='RANDOMEXPRESSIONdashboardsList|dashboards_list|listOfDashboards|list_of_dashboardsEND'>
      { dashboards.map((d) => { return <DashboardListItem dashboard={d}/> }) }
    </div>
```

¡Ya puedes verificar que http://localhost:3000 muestra la página inicial con una lista de _dashboards_!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

