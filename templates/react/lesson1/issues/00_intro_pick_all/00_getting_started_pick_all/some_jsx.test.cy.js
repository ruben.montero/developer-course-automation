/// <reference types="cypress" />

describe('some_jsx', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })

    it('issueISSUENUMBER p contains an appropriate message', () => {
        cy.get('p[data-cy=issueISSUENUMBER]')
        .should('have.text', 'RANDOMEXPRESSIONProbando|Probando...|1,2,3,Probando...|1,2,3,ProbandoEND');
    })
})
