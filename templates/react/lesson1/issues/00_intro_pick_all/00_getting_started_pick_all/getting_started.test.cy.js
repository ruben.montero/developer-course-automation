/// <reference types="cypress" />

describe('getting_started', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains an appropriate div with starting message', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('div[data-cy=issueISSUENUMBERdiv]')
        .should('have.text', 'RANDOMEXPRESSIONHola mundo|Hello there!|Buenos días|Aquí estamos|Mis primeros pasos en React|Un pequeño paso para mí, un gran salto para el mundo|¡Hola!|Baby steps...|On my way to becoming a React masterEND');
    })
})
