Probemos algo fácil

## :books: Resumen

* Añadiremos un pequeño cambio a `App.js`

## Descripción

¡Hagamos otra pequeña prueba!

## :package: La tarea

En `react-introduction/src/`, **añade** al archivo `App.js` un `<p>` como se indica a continuación:

```diff
function App() {
  return (
    <div className="App">
       <div data-cy='issue1'>
         <h1>Ejercicio 1</h1>
         <div data-cy='issue1div'>
           RANDOMEXPRESSIONHola mundo|Hello there!|Buenos días|Aquí estamos|Mis primeros pasos en React|Un pequeño paso para mí, un gran salto para el mundo|¡Hola!|Baby steps...|On my way to becoming a React masterEND
         </div>
       </div>
+      <p data-cy='issueISSUENUMBER'>RANDOMEXPRESSIONProbando|Probando...|1,2,3,Probando...|1,2,3,ProbandoEND</p>
    </div>
  );
}
```

Cuando guardes el fichero, la página http://localhost:3000 se refrescará automáticamente y mostrará los cambios.

¡Comprueba que ves correctamente el nuevo texto!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

No está de más verificar que tu _commit_ se ha subido correctamente desde GitLab (https://raspi).
