Comprendiendo el espacio de trabajo

## :books: Resumen

* Actualizaremos nuestro repositorio con `git pull` y comenzaremos a trabajar en la nueva carpeta `react-introduction`
* Modificaremos la página principal y serviremos un sencillo mensaje inicial
* Haremos `git add`, `git commit` y `git push` para subir los cambios al repositorio

## Descripción

El proyecto de [React](https://es.reactjs.org/) sobre el que trabajaremos este Sprint está alojado en la carpeta `react-introduction` de tu repositorio y ha sido creado mediante el comando `npx create-react-app react-introduction`, que es la forma más sencilla y [oficialmente recomendada](https://reactjs.org/docs/create-a-new-react-app.html#create-react-app) para poner a andar un proyecto React.

## :package: La tarea

Desde un terminal (cmd.exe), **ejecuta** `git pull` en tu repositorio. 

Habrá aparecido una nueva carpeta `react-introduction/`. **Cambia** (`cd`) a dicha carpeta, y dentro, **ejecuta** estos dos comandos:

```
npm install
npm run start
```

Es posible que `npm install` tarde un rato. Sólo será necesario invocarlo una vez, para instalar las librerías que se alojarán en `node_modules/`.

Debería abrirse un navegador que visita la página http://localhost:3000. ¡Enhorabuena! Has lanzado tu servidor React y visitado una página web servida desde tu máquina local.

### Un cambio

Ahora, **abre** la carpeta `react-introduction/` desde tu [IDE](https://www.redhat.com/es/topics/middleware/what-is-ide) favorito (VSCode, IntelliJ IDEA,...).

En la carpeta `src/`, **modifica** el archivo `App.js`.

* **Borra** el `<header>` de principio a fin y **sustitúyelo** por el `<div>` que se indica a continuación resaltado en verde:

```diff
function App() {
  return (
    <div className="App">
-      <header className="App-header">
-        <img src={logo} className="App-logo" alt="logo" />
-        <p>
-          Edit <code>src/App.js</code> and save to reload.
-        </p>
-        <a
-          className="App-link"
-          href="https://reactjs.org"
-          target="_blank"
-          rel="noopener noreferrer"
-        >
-          Learn React
-        </a>
-      </header>
+      <div data-cy='issueISSUENUMBER'>
+        <h1>Ejercicio ISSUENUMBER</h1>
+        <div data-cy='issueISSUENUMBERdiv'>
+          RANDOMEXPRESSIONHola mundo|Hello there!|Buenos días|Aquí estamos|Mis primeros pasos en React|Un pequeño paso para mí, un gran salto para el mundo|¡Hola!|Baby steps...|On my way to becoming a React masterEND
+        </div>
+      </div>
    </div>
  );
}
```

Cuando guardes el fichero, la página http://localhost:3000 se refrescará automáticamente y mostrará los cambios.

**¡Enhorabuena!** Has modificado el archivo inicial de tu proyecto React.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

No está de más verificar que tu _commit_ se ha subido correctamente desde GitLab (https://raspi).
