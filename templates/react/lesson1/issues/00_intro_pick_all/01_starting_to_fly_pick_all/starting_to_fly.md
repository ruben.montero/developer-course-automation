Quitando los ruedines

## :books: Resumen

* Añadiremos HTML simple a la página principal

## Descripción

En el ejercicio anterior hemos modificado el HTML de la página principal `App.js` en nuestro proyecto.

## :package: La tarea

En `App.js` **añade** un `<div data-cy='issueISSUENUMBER'>` debajo _(a continuación)_ del `<p>` correspondiente al ejercicio anterior. Este nuevo `<div>`, **debe** contener:

* Un `<h1>` indicando `Ejercicio ISSUENUMBER`
* Un `<p>` que indique `3 interesantes presidentes de EE.UU.`
* Un `<RANDOMEXPRESSIONul|olEND>`
* ...tendrá el atributo `data-cy='issueISSUENUMBERlist'`.
* ...contendrá los siguientes elementos (`li`):
  * RANDOMEXPRESSIONGeorge Washington|John Adams|Thomas Jefferson|James Madison|James MonroeEND
  * RANDOMEXPRESSIONAndrew Jackson|Martin Van Buren|John Tyler|Millard Fillmore|Franklin PierceEND
  * RANDOMEXPRESSIONAbraham Lincoln|Grover Cleveland|Theodore Roosevelt|Woodrow Wilson|Herbert HooverEND

### ¿Por qué se pide añadir el atributo `data-cy` en algunos elementos?

Es una buena práctica para identificar los elementos de las páginas mediante _tests_ automáticos programados con Cypress.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
