/// <reference types="cypress" />

describe('starting_to_fly', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains an appropriate p with title', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', '3 interesantes presidentes de EE.UU.');
    })

    it('issueISSUENUMBER div list has appropriate length', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('RANDOMEXPRESSIONul|olEND')
        .find('li')
        .should('have.length', 3);
    })

    it('issueISSUENUMBER div list contains an appropriate first element', () => {
        cy.get('[data-cy=issueISSUENUMBERlist]')
        .find('li')
        .first()
        .should('have.text', 'RANDOMEXPRESSIONGeorge Washington|John Adams|Thomas Jefferson|James Madison|James MonroeEND');
    })

    it('issueISSUENUMBER div list contains an appropriate second element', () => {
        cy.get('[data-cy=issueISSUENUMBERlist]')
        .find('li')
        .eq(1)
        .should('have.text', 'RANDOMEXPRESSIONAndrew Jackson|Martin Van Buren|John Tyler|Millard Fillmore|Franklin PierceEND');
    })

    it('issueISSUENUMBER div list contains an appropriate third element', () => {
        cy.get('[data-cy=issueISSUENUMBERlist]')
        .find('li')
        .last()
        .should('have.text', 'RANDOMEXPRESSIONAbraham Lincoln|Grover Cleveland|Theodore Roosevelt|Woodrow Wilson|Herbert HooverEND');
    })
})
  
