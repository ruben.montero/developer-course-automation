/// <reference types="cypress" />

describe('a_simple_hello', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate tag', () => {
        cy.get('RANDOMEXPRESSIONp|h1|h3END[data-cy=issueISSUENUMBER]')
        .should('have.text', 'RANDOMEXPRESSIONHola|¡Hola!|Hola!|Hello!|Hi!|Well, Hello!|Hey apple!|Hi there!|Hi there|HelloEND');
    })
})
  
