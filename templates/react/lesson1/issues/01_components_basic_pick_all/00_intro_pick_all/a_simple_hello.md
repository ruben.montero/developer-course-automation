Un componente... ¡Hola!

## :books: Resumen

* Crearemos una carpeta donde alojaremos el código de componentes
* Añadiremos un componente sencillo que sirva para _renderizar_ un `<RANDOMEXPRESSIONp|h1|h3END>RANDOMEXPRESSIONHola|¡Hola!|Hola!|Hello!|Hi!|Well, Hello!|Hey apple!|Hi there!|Hi there|HelloEND</RANDOMEXPRESSIONp|h1|h3END>`
* Modificaremos `App.js` para que incluya ese componente

## Descripción

En los primeros ejercicios hemos incrustado en `App.js` nuevo código HTML (en realidad, [JSX](https://es.reactjs.org/docs/introducing-jsx.html)) que queríamos que se viera en nuestra página.

La filosofía de React consiste en crear interfaces de usuario mediante [componentes](https://es.reactjs.org/docs/components-and-props.html). ¡Divide y vencerás!

## :package: La tarea

**Crea** una carpeta `components/` dentro de `src/`.

Dentro de `components/`, **crea** otra nueva carpeta llamada `simple_hello/`, y ahí, **crea** el fichero `Hello.js`.

En `Hello.js`, **escribe** una _función_[^1] llamada `Hello`. Esta _función_ será nuestro componente:

```jsx
function Hello(props) {

}

export default Hello;
```

Si lo prefieres, puedes escribirlo usando una sintaxis equivalente: _arrow function_:

```jsx
const Hello = (props) => {

}

export default Hello;
```

### Lo que se muestra

Ahora, **añade** un `return` a tu componente especificando lo que va a mostrar. **Así**:

```jsx
function Hello(props) {

  return <RANDOMEXPRESSIONp|h1|h3END data-cy='issueISSUENUMBER'>RANDOMEXPRESSIONHola|¡Hola!|Hola!|Hello!|Hi!|Well, Hello!|Hey apple!|Hi there!|Hi there|HelloEND</RANDOMEXPRESSIONp|h1|h3END>
}

export default Hello;
```

### Incluyéndolo en la página

En `App.js`, **añade** el componente `<Hello />` debajo del `<div>` de la tarea anterior:

```diff
...
        </RANDOMEXPRESSIONul|olEND>
      </div>
+     <Hello />  
    </div>
  );
}
```

Si lo prefieres, puedes usar la sintaxis con etiqueta de apertura y de cierre: `<Hello></Hello>`.

Tu IDE debe _autocompletar_ el `import` necesario. Si no lo hace, ten en cuenta que debes incluirlo arriba y será así:

```jsx
import Hello from './components/simple_hello/Hello'
```

**¡Ya está!** En http://localhost:3000 debes ver _RANDOMEXPRESSIONHola|¡Hola!|Hola!|Hello!|Hi!|Well, Hello!|Hey apple!|Hi there!|Hi there|HelloEND_. Como puedes observar, un componente _encapsula_ un pedacito de interfaz, y lo podemos mostrar desde otro sitio.

### :trophy: Por último

Tras comprobar que todo va bien, sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Como ves, la función (componente) `Hello` declara un argumento `props`. ¿Qué es? Lo veremos próximamente.
