Tabla de salarios

## :books: Resumen

* Añadiremos un componente que sirva para _renderizar_ una tabla con información de salarios
* Modificaremos `App.js` para que incluya ese componente

## Descripción

¿Te ha gustado crear una `<table>` y encapsularla en un componente?

¡Hagamos otra!

## :package: La tarea

Dentro de `src/components/`, **crea** una nueva carpeta llamada `salaries_table/`.

A continuación, **crea** el fichero `SalariesTable.js` dentro de dicha carpeta.

En ese fichero, **crea** el componente `SalariesTable` y **haz** que muestre (`return`) una `<table>` con las siguientes características:

* En la cabecera (`<thead>`), tendrá **1** fila (`<tr>`) con **2** columnas (`th`) que indicarán **RANDOMEXPRESSIONOferta de empleo|EmpleoEND** y **Salario anual**, en ese orden:

* En el cuerpo (`<tbody>`), tendrá **3 filas** (`<tr>`):
  * Los dos elementos (`<td>`) de la **primera fila** indicarán: **Desarrollador React**, **RANDOMNUMBER50000-80000END**, respectivamente. 
  * Los dos elementos (`<td>`) de la **segunda fila** indicarán: **Desarrollador iOS**, **RANDOMNUMBER40000-50000END**, respectivamente.
  * Los dos elementos (`<td>`) de la **tercera fila** indicarán: **Desarrollador Android**, **RANDOMNUMBER20000-40000END**, respectivamente.

### Usando el componente desde `App.js`

En `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Tendrá el atributo `data-cy='issueISSUENUMBER'` y un contenido como se muestra a continuación:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <SalariesTable />
</div>
```

_(No olvides la sentencia `import` necesaria)._

Verifica http://localhost:3000. ¿Qué tal se ve?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
