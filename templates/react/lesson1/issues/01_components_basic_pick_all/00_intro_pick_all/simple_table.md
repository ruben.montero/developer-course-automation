Una tabla sencilla

## :books: Resumen

* Añadiremos un componente sencillo que sirva para _renderizar_ una tabla con sonidos de animales
* Modificaremos `App.js` para que incluya ese componente

## Descripción

¡Vamos a seguir creando componentes!

Esta vez, hagamos uno más complejo que muestre una _tabla_.

Existe variada documentación en la web sobre las tablas en HTML. Si necesitas repasarlo, puede venirte bien [este enlace a la documentación de Mozilla](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table#try_it).

## :package: La tarea

**Crea** una carpeta `simple_table/` dentro de `components/`.

En `simple_table/`, **crea** un fichero `SimpleTable.js` que contenga lo siguiente:

```jsx
const SimpleTable = (props) => {

  return <table>
           <thead>
             <tr>
               <th>RANDOMEXPRESSIONAnimal|CriaturaEND</th><th>RANDOMEXPRESSIONRuido|SonidoEND</th>
             </tr>
           </thead>
           <tbody>
             <tr>
               <td>RANDOMEXPRESSIONPerro|PerritoEND</td><td>RANDOMEXPRESSIONGuau|Guau,guau|Guau!!END</td>
             </tr>
             <tr>
               <td>RANDOMEXPRESSIONGato|GatitoEND</td><td>RANDOMEXPRESSIONMiau|Meow!|Miaaaau|Miau,miauEND</td>
             </tr>
           </tbody>
         </table>
}

export default SimpleTable;
```

Ahora, en `App.js`, **añade** un `div` debajo _(a continuación)_ del `<Hello />` del ejercicio anterior. Este nuevo `<div>` debe tener un contenido y atributos **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <SimpleTable />
</div>
```

_(No olvides la sentencia `import` necesaria)._

¡Verifica si tu tabla se ve correctamente!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
