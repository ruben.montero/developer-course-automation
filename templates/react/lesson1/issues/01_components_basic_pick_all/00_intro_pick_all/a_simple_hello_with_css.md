Algo de color

## :books: Resumen

* Crearemos un archivo `Hello.css` con una clase CSS
* Usaremos dicha clase desde el componente `Hello.js`

## Descripción

¿No has escuchado varias veces eso de que la vida está llena de color? ¡Vamos a ponerlo en práctica con un poco de CSS!

Además, en el camino, veremos alguna característica de [JSX](https://es.reactjs.org/docs/introducing-jsx.html) (en contraposición a HTML) como que debemos usar el atributo `className` en vez de `class`.

## :package: La tarea

Dentro de `src/components/simple_hello/` **crea** un nuevo fichero `Hello.css`.

En ese fichero, añade el siguiente código[^1] CSS:

```css
.simple-hello-red {
  background-color: rgb(RANDOMNUMBER210-240END, RANDOMNUMBER10-13END, RANDOMNUMBER11-14END)
}
```

A continuación, en el fichero `Hello.js`, al principio de todo, **importa** este CSS, y **aplica** el atributo `className` con el nombre de la clase CSS recién creada:

```jsx
import "./Hello.css"

const Hello = (props) => {

  return <RANDOMEXPRESSIONp|h1|h3END className="simple-hello-red" data-cy='issue4'>RANDOMEXPRESSIONHola|¡Hola!|Hola!|Hello!|Hi!|Well, Hello!|Hey apple!|Hi there!|Hi there|HelloEND</RANDOMEXPRESSIONp|h1|h3END>
}

export default Hello;
```

**¡Listo!**

Acabas de usar CSS por primera vez en React. Como ves, puedes dar estilo a tus etiquetas. La única peculiaridad es que hay que _importar_ el CSS y debemos usar `className` (en vez de `class`).

Verifica en http://localhost:3000 que ahora el texto se ve rojo.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Cuando usamos CSS en un componente, dichos estilos acaban siendo aplicados a _toda_ la página. Por eso es importante usar nombres de clases muy específicos, como `.simple-hello-red`.
