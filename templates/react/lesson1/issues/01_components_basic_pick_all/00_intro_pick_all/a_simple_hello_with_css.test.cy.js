/// <reference types="cypress" />

describe('a_simple_hello_with_css', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate tag', () => {
        cy.get('RANDOMEXPRESSIONp|h1|h3END[data-cy=issue4]')
        .should('have.css', 'background-color', 'rgb(RANDOMNUMBER210-240END, RANDOMNUMBER10-13END, RANDOMNUMBER11-14END)');
    })
})
  
