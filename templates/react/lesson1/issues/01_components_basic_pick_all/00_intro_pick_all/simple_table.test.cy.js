/// <reference types="cypress" />

describe('simple_table', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains a table with correct first header', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('thead')
        .find('tr')
        .first()
        .find('th')
        .first()
        .should('have.text', 'RANDOMEXPRESSIONAnimal|CriaturaEND');
    })

    it('issueISSUENUMBER div contains a table with correct second header', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('thead')
        .find('tr')
        .first()
        .find('th')
        .last()
        .should('have.text', 'RANDOMEXPRESSIONRuido|SonidoEND');
    })

    it('issueISSUENUMBER div contains correct first row animal name', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .first()
        .find('td')
        .first()
        .should('have.text', 'RANDOMEXPRESSIONPerro|PerritoEND');
    })

    it('issueISSUENUMBER div contains correct first row animal sound', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .first()
        .find('td')
        .last()
        .should('have.text', 'RANDOMEXPRESSIONGuau|Guau,guau|Guau!!END');
    })
    
    it('issueISSUENUMBER div contains correct second row animal name', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(1)
        .find('td')
        .first()
        .should('have.text', 'RANDOMEXPRESSIONGato|GatitoEND');
    })

    it('issueISSUENUMBER div contains correct second row animal sound', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(1)
        .find('td')
        .last()
        .should('have.text', 'RANDOMEXPRESSIONMiau|Meow!|Miaaaau|Miau,miauEND');
    })
})
  
