/// <reference types="cypress" />

describe('a_simple_table_¨with_salaries', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains a table with correct first header', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('thead')
        .find('tr')
        .first()
        .find('th')
        .first()
        .should('have.text', 'RANDOMEXPRESSIONOferta de empleo|EmpleoEND');
    })

    it('issueISSUENUMBER div contains a table with correct second header', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('thead')
        .find('tr')
        .first()
        .find('th')
        .last()
        .should('have.text', 'Salario anual');
    })

    it('issueISSUENUMBER div contains correct first row job position name', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .first()
        .find('td')
        .first()
        .should('have.text', 'Desarrollador React');
    })

    it('issueISSUENUMBER div contains correct first row salary', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .first()
        .find('td')
        .last()
        .should('have.text', 'RANDOMNUMBER50000-80000END');
    })
    
    it('issueISSUENUMBER div contains correct second job position name', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(1)
        .find('td')
        .first()
        .should('have.text', 'Desarrollador iOS');
    })

    it('issueISSUENUMBER div contains correct second row salary', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(1)
        .find('td')
        .last()
        .should('have.text', 'RANDOMNUMBER40000-50000END');
    })
    
    it('issueISSUENUMBER div contains correct third row job position name', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .last()
        .find('td')
        .first()
        .should('have.text', 'Desarrollador Android');
    })

    it('issueISSUENUMBER div contains correct third row salary', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .last()
        .find('td')
        .last()
        .should('have.text', 'RANDOMNUMBER20000-40000END');
    })
})
  
