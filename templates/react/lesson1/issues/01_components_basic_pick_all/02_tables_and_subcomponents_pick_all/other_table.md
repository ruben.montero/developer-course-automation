Tabla de cocina

## :books: Resumen

* Crearemos un componente con otra tabla y modificaremos `App.js` para que incluya ese componente

## Descripción

¡Hagamos otra `<table>` en un componente!

## :package: La tarea

Dentro de `src/components/`, **crea** una nueva carpeta llamada `kitchen_table/`.

A continuación, **crea** el fichero `KitchenTable.js` dentro de dicha carpeta.

En ese fichero, **crea** el componente `KitchenTable` y **haz** que muestre (`return`) una `<table>` con las siguientes características:

* En la cabecera (`<thead>`), tendrá **1** fila (`<tr>`) con **3** columnas (`th`) que indicarán **RANDOMEXPRESSIONObjeto|CosaEND**, **RANDOMEXPRESSIONPrecio|ValorEND** y **Vida útil**, en ese orden:

* En el cuerpo (`<tbody>`), tendrá **2 filas** (`<tr>`):
  * Los tres elementos (`<td>`) de la **primera fila** indicarán: **RANDOMEXPRESSIONEstropajo|Estropajo verde|NanasEND**, **RANDOMNUMBER3-6END** y **25 días** respectivamente. 
  * Los tres elementos (`<td>`) de la **segunda fila** indicarán: **RANDOMEXPRESSIONLavavajillas|Horno|Horno pirolíticoEND**, **RANDOMNUMBER250-600END** y **9 años** respectivamente. 

Luego, en `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Tendrá el atributo `data-cy='issueISSUENUMBER'` y un contenido como se muestra a continuación:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <KitchenTable />
</div>
```

_(No olvides la sentencia `import` necesaria)._

Verifica http://localhost:3000. ¿Aparece todo correctamente?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
