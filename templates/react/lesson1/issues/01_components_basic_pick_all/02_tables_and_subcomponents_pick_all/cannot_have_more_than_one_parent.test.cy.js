/// <reference types="cypress" />

describe('cannot_have_more_than_one_parent', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains an h3 with correct text', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h3')
        .should('have.text', 'Este es un ejemplo de un componente que muestra dos RANDOMEXPRESSIONhijos|etiquetas hijas|nodos hijosEND');
    })

    it('issueISSUENUMBER div contains an h4 with correct text', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h4')
        .should('have.text', 'RANDOMEXPRESSIONagrupados bajo una raíz|agrupados bajo una raíz común|agrupados bajo una única raíz|que se han agrupado bajo una raíz común|que se han agrupado bajo una única raízEND');
    })
})
  
