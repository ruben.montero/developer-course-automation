Todos tenemos una raíz... ¡Una!

## :books: Resumen

* Veremos que en JSX las expresiones sólo pueden tener una raíz
* Veremos que la etiqueta vacía (`<>`) es tan válida como un `<div>`
* Modificaremos `App.js` para que incluya ese componente

## Descripción

JSX no es HTML.

La principal diferencia es que JSX es una sintaxis especial que, más tarde, se traduce a JavaScript (Por lo tanto, son lenguajes diferentes). Así, donde escribes `<MiComponente />`, en realidad, esto se _transformará_ en una llamada a [`createElement`](https://es.react.dev/reference/react/createElement):

```
React.createElement("MiComponente", {}, []);
```

También existen otras peculiaridades, como que debemos usar `className` en vez de `class`, o... ¡Que cada expresión debe tener un único elemento raíz!

Por ejemplo, si pruebas a crear un componente cualquiera y hacer que devuelva dos elementos:

```jsx
const ComponenteCualquiera = (props) => {

  // ¡JA! ¡NO FUNCIONA! :(
  
  return <h1>Un texto grande</h1>
         <h2>Texto no tan grande</h2>;
}
```

...¡está mal!

Siempre hay que _agrupar_ las expresiones JSX bajo un único elemento raíz, por ejemplo, un `<div>`:

```jsx
const ComponenteCualquiera = (props) => {

  // Esto va bien :)
  
  return <div>
      <h1>Un texto grande</h1>
      <h2>Texto no tan grande</h2>
    </div>;
}
```

¿Sabías que también se puede usar una etiqueta vacía (`<>`) que puede cumplir la misma función?

```jsx
const ComponenteCualquiera = (props) => {

  // Esto también es válido :D
  
  return <>
      <h1>Un texto grande</h1>
      <h2>Texto no tan grande</h2>
    </>;
}
```

## :package: La tarea

Dentro de `src/components/`, **crea** una nueva carpeta llamada `one_root_example/`.

A continuación, **crea** el fichero `OneRootExample.js` dentro de dicha carpeta.

En ese fichero, **crea** el componente `OneRootExample` **así**:

```jsx
const OneRootExample = (props) => {

  return <>
      <h3>Este es un ejemplo de un componente que muestra dos RANDOMEXPRESSIONhijos|etiquetas hijas|nodos hijosEND</h3>
      <h4>RANDOMEXPRESSIONagrupados bajo una raíz|agrupados bajo una raíz común|agrupados bajo una única raíz|que se han agrupado bajo una raíz común|que se han agrupado bajo una única raízEND</h4>
    </>;
}
```

Luego, en `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Tendrá el atributo `data-cy='issueISSUENUMBER'` y un contenido como se muestra a continuación:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <OneRootExample />
</div>
```

_(No olvides la sentencia `import` necesaria)._

Verifica http://localhost:3000. ¿Qué tal se ve?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
