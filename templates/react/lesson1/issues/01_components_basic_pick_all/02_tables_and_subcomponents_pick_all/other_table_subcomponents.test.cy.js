/// <reference types="cypress" />

describe('kitchen_table', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains a table with correct first header', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('thead')
        .find('tr')
        .first()
        .find('th')
        .first()
        .should('have.text', 'Actividad');
    })

    it('issueISSUENUMBER div contains a table with correct second header', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('thead')
        .find('tr')
        .first()
        .find('th')
        .eq(1)
        .should('have.text', 'RANDOMEXPRESSIONDiversión|Cantidad de diversiónEND');
    })

    it('issueISSUENUMBER div contains correct first row 1st value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .first()
        .find('td')
        .first()
        .should('have.text', 'RANDOMEXPRESSIONPlaya|Surf|TerraceoEND');
    })

    it('issueISSUENUMBER div contains correct first row 2nd value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .first()
        .find('td')
        .eq(1)
        .should('have.text', 'RANDOMEXPRESSIONMáxima|Altísima|Muy altaEND');
    })
    
})
  
