Lista ordenada

## :books: Resumen

* Crearemos una lista ordenada. Cada elemento, será un componente
* Mostraremos dicha lista en `App.js`

## Descripción

Para mostrar listas ordenadas (_ordered list_) se emplea la etiqueta `<ol>` en HTML. Cada elemento de dicha lista se especifica con `<li>`...

```html
<ol>
  <li>Primer elemento</li>
  <li>Segundo elemento</li>
  <li><h3>Este es el tercer <b>elemento</b></h3></li>
</ol>
```

...y se mostrará con un número a su izquierda. (Por eso es una lista _ordenada_... ¡tiene un orden!)

¡Vamos a implementar una lista ordenada empleando React!

## :package: La tarea

Dentro de `src/components/`, **crea** una nueva carpeta llamada `gems_list/`.

A continuación, **crea** el fichero `RANDOMEXPRESSIONDiamond|Ruby|SapphireENDListItem.js` dentro de dicha carpeta. En ese fichero, **crea** el componente `RANDOMEXPRESSIONDiamond|Ruby|SapphireENDListItem` **así**:

```jsx
const RANDOMEXPRESSIONDiamond|Ruby|SapphireENDListItem = (props) => {

  return <li>RANDOMEXPRESSIONDiamond|Ruby|SapphireEND</li>;
}

export default RANDOMEXPRESSIONDiamond|Ruby|SapphireENDListItem;
```

En la misma carpeta, **crea** el fichero `RANDOMEXPRESSIONOpal|Amethyst|PeridotENDListItem.js` dentro de dicha carpeta. En ese fichero, **crea** el componente `RANDOMEXPRESSIONOpal|Amethyst|PeridotENDListItem` **así**:

```jsx
const RANDOMEXPRESSIONOpal|Amethyst|PeridotENDListItem = (props) => {

  return <li>RANDOMEXPRESSIONOpal|Amethyst|PeridotEND</li>;
}

export default RANDOMEXPRESSIONOpal|Amethyst|PeridotENDListItem;
```

Por tercera vez, **crea** otro fichero `RANDOMEXPRESSIONQuartz|JasperENDListItem.js` dentro de dicha carpeta. En ese fichero, **crea** el componente `RANDOMEXPRESSIONQuartz|JasperENDListItem` **así**:

```jsx
const RANDOMEXPRESSIONQuartz|JasperENDListItem = (props) => {

  return <li>RANDOMEXPRESSIONQuartz|JasperEND</li>;
}

export default RANDOMEXPRESSIONQuartz|JasperENDListItem;
```

Ahora **crea** un nuevo fichero `GemsOrderedList.js` en la carpeta `gems_list/`.

Ese componente será **así**:

```jsx
import RANDOMEXPRESSIONDiamond|Ruby|SapphireENDListItem from './RANDOMEXPRESSIONDiamond|Ruby|SapphireENDListItem.js';
import RANDOMEXPRESSIONOpal|Amethyst|PeridotENDListItem from './RANDOMEXPRESSIONOpal|Amethyst|PeridotENDListItem.js';
import RANDOMEXPRESSIONQuartz|JasperENDListItem from './RANDOMEXPRESSIONQuartz|JasperENDListItem.js';

const GemsOrderedList = (props) => {

  return <ol>
      <RANDOMEXPRESSIONDiamond|Ruby|SapphireENDListItem />
      <RANDOMEXPRESSIONOpal|Amethyst|PeridotENDListItem />
      <RANDOMEXPRESSIONQuartz|JasperENDListItem />
    </ol>
}

export default GemsOrderedList;
```

¿Qué te parece? ¿Crees que se verá una lista ordenada de piedras preciosas?

### Usándolo en `App.js`

En `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Tendrá `data-cy='issueISSUENUMBER'` y será **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <GemsOrderedList />
</div>
```

_(No olvides la sentencia `import` necesaria)._

¿Sabes que las piedras preciosas que ahora se muestran en http://localhost:3000 están ordenadas por valor?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
