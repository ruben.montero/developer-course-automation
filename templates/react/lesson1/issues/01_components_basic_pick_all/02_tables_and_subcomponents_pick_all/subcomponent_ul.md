Lista desordenada

## :books: Resumen

* Crearemos una lista desordenada reutilizando, para cada elemento, los componentes de la tarea anterior
* Mostraremos dicha lista en `App.js`

## Descripción

Vamos a _reutilizar_ los componentes de piedras preciosas de la tarea anterior. ¡Es perfectamente válido!

Esta vez implementemos una lista _desordenada_.

## :package: La tarea

Dentro de `gems_list/` **crea** un nuevo fichero `GemsUnorderedList.js`.

En su interior **crea** un componente `GemsUnorderedList` que:

* Tenga, como elemento raíz, un `<ul>`
* Dentro de dicho `<ul>`, tendrá los elementos `<RANDOMEXPRESSIONDiamond|Ruby|SapphireENDListItem />`, `<RANDOMEXPRESSIONOpal|Amethyst|PeridotENDListItem />` y `<RANDOMEXPRESSIONQuartz|JasperENDListItem />` (en ese orden)
* ¡No olvides los _import_ ni _export_ necesarios!

### Usándolo en `App.js`

En `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Tendrá `data-cy='issueISSUENUMBER'` y será **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <GemsUnorderedList />
</div>
```

¿Qué tal se ve en http://localhost:3000?

¿Ves la diferencia entre usar `<ol>` (_ordered list_) y `<ul>` (_unordered list_)?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
