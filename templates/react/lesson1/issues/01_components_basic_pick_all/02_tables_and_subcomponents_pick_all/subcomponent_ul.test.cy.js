/// <reference types="cypress" />

describe('subcomponent_ul', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains an unordered list with correct first element', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('ul')
        .find('li')
        .first()
        .should('have.text', 'RANDOMEXPRESSIONDiamond|Ruby|SapphireEND');
    })

    it('issueISSUENUMBER div contains an unordered list with correct second element', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('ul')
        .find('li')
        .eq(1)
        .should('have.text', 'RANDOMEXPRESSIONOpal|Amethyst|PeridotEND');
    })
    
    it('issueISSUENUMBER div contains an unordered list with correct third element', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('ul')
        .find('li')
        .last()
        .should('have.text', 'RANDOMEXPRESSIONQuartz|JasperEND');
    })
})
  
