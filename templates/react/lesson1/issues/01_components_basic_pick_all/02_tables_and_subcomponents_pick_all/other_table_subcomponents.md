Subcomponentes

## :books: Resumen

* Veremos que un componente puede incluir otro componente
* Crearemos una tabla con varios componentes y la mostraremos en `App.js`

## Descripción

Hemos creado ya algunas cuantas `<table>`... ¿Sabes que podemos _partirla_ y _encapsularla_ en distintos componentes?

## :package: La tarea

Dentro de `src/components/`, **crea** una nueva carpeta llamada `summer_table/`.

A continuación, **crea** el fichero `SummerTableRow.js` dentro de dicha carpeta.

En ese fichero, **crea** el componente `SummerTableRow` **así**:

```jsx
const SummerTableRow = (props) => {

  return <tr><td>RANDOMEXPRESSIONPlaya|Surf|TerraceoEND</td><td>RANDOMEXPRESSIONMáxima|Altísima|Muy altaEND</td></tr>;
}

export default SummerTableRow;
```

Ahora, también dentro de la carpeta `summer_table/`, **crea** el fichero `SummerTable.js`.

En ese nuevo fichero, **añade** lo siguiente:

```jsx
import SummerTableRow from './SummerTableRow.js';

const SummerTable = (props) => {

  return <table>
      <thead>
        <tr><th>Actividad</th><th>RANDOMEXPRESSIONDiversión|Cantidad de diversiónEND</th></tr>
      </thead>
      <tbody>
        <SummerTableRow />
      </tbody>
    </table>;
}

export default SummerTable;
```

¿Qué te parece? Hemos usado `<SummerTableRow />` _(una fila de la tabla)_ dentro de `<SummerTable />`.

Ahora, en `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Tendrá `data-cy='issueISSUENUMBER'` y será **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <SummerTable />
</div>
```

_(No olvides la sentencia `import` necesaria)._

Verifica http://localhost:3000. ¿Se ve la tabla como cabe esperar? ¿Qué opinas de que unos componentes se puedan incluir dentro de otros? ¿No desbloquea eso un enorme potencial para modularizar tus interfaces web?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
