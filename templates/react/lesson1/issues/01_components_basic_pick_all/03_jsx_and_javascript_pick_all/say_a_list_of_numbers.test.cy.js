/// <reference types="cypress" />

describe('say_a_list_of_numbers', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains ul', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('ul');
    })
    
    it('issueISSUENUMBER div contains correct first li', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('ul')
        .find('li')
        .first()
        .should('have.text', 1);
    })

    it('issueISSUENUMBER div contains correct last li', () => {
        const NUMERO = RANDOMNUMBER7-15END
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('ul')
        .find('li')
        .eq(NUMERO - 1)
        .should('have.text', NUMERO);
    })
})
  
