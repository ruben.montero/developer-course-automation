Número aleatorio

## :books: Resumen

* Haremos un componente que muestra un número aleatorio

## Descripción

¡Vamos a seguir probando a embeber JavaScript dentro de nuestro JSX!

## :package: La tarea

Dentro de `src/components/simple_examples/` **crea** un nuevo fichero `RandomNumber.js`. Dentro, **crea** un componente con ese mismo nombre que:

* Defina (`const`) una variable `NUMERO_ALEATORIO`. Será un número aleatorio[^1] **entero** _entre_ `RANDOMNUMBER10-100END` y `RANDOMNUMBER101-400END` (ambos inclusive).
* _Renderice_ (`return`) un contenido como el siguiente:

```jsx
<div>
  <p>RANDOMEXPRESSIONEl número aleatorio elegido es|El número aleatorio elegido es:|El número aleatorio es|El número aleatorio es:END</p>
  <h4 data-cy="issueISSUENUMBERnumber">{NUMERO_ALEATORIO}</h4>
</div>
```

Para terminar, en `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Será **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <RandomNumber />
</div>
```

¿Qué tal se ve en http://localhost:3000? ¿Aparece un aleatorio nuevo cada vez que refrescas la página?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: ¿Sabes cómo calcular un número aleatorio en un rango dado usando JavaScript? ¿Por qué no visitas la sección _A proper random function_ de [este enlace a W3C](https://www.w3schools.com/JS/js_random.asp)?
