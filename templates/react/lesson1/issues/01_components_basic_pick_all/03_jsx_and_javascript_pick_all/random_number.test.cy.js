/// <reference types="cypress" />

describe('embedding_javascript', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains correct text', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('div')
        .find('p')
        .should('have.text', 'RANDOMEXPRESSIONEl número aleatorio elegido es|El número aleatorio elegido es:|El número aleatorio es|El número aleatorio es:END');
    })

    it('issueISSUENUMBER div contains correct number', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('div')
        .find('h4[data-cy=issueISSUENUMBERnumber]')
        .invoke('text').then(parseFloat).should('be.gte', RANDOMNUMBER10-100END)
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('div')
        .find('h4[data-cy=issueISSUENUMBERnumber]')
        .invoke('text').then(parseFloat).should('be.lte', RANDOMNUMBER101-400END)
    })
})
  
