Multiplica las posibilidades

## :books: Resumen

* Haremos un componente que imprime la tabla de multiplicar del `RANDOMNUMBER1-9END`

## Descripción

Vamos a seguir practicando la mecánica de añadir elementos JSX a un _array_ usando un `for`, para más tarde mostrar dicho _array_ en un componente.

## :package: La tarea

**Crea** una carpeta `multiplications/` dentro de `src/components/`. Ahí, **crea** un fichero `MultiplicationTable.js` y dentro, un componente homónimo **así**:

```jsx
const MultiplicationTable = (props) => {

  const NUMERO = RANDOMNUMBER1-9END;
  
  const listaDeElementos = []
  
  for (let i = 1; i <= 10; i++) {
    const elemento = <li>{NUMERO} RANDOMEXPRESSIONpor|multiplicado porEND {i} RANDOMEXPRESSIONes|daEND {NUMERO * i}</li>;
    listaDeElementos.push(elemento);
  }
  
  return <RANDOMEXPRESSIONul|olEND>{listaDeElementos}</RANDOMEXPRESSIONul|olEND>
}
```

Para finalizar, en `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Será **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <MultiplicationTable />
</div>
```

¿Qué tal se ve en http://localhost:3000?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
