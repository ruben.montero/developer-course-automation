Embebiendo JavaScript

## :books: Resumen

* Veremos que con _llaves_ (`{ }`) se puede _embeber_ JavaScript dentro del JSX
* Lo probaremos en un nuevo componente `JavaScriptExample`

## Descripción

Nuestros componentes son puro JSX hasta el momento. La verdad es que esto es útil, pero, ¿dónde está la conexión con lo que hemos aprendido sobre JavaScript anteriormente? ¿Cómo se pueden emplear _variables_, _sentencias de control_, etc.?

¡Tenemos que usar _llaves_ (`{ }`)!

```jsx
const EjemploComponente = (props) => {

  const NUMERO_PI = 3.1416;
  
  return <p>El número Pi vale {NUMERO_PI}</p>
}
```

De esta manera, lo que aparece entre las _llaves_ es _evaluado_ como código JavaScript... Y esto nos ofrece mayor flexibilidad:

```jsx
const EjemploComponente = (props) => {

  const NUMERO_PI = 3.1416;
  
  return <>
      <p>El número Pi vale {NUMERO_PI}</p>
      <p>El número Pi dos veces es {NUMERO_PI * 2}</p>
    </>
}
```

## :package: La tarea

**Crea** una carpeta `simple_examples/` dentro de `src/components/`. Ahí, **crea** un fichero `JavaScriptExample.js` y dentro, un componente homónimo **así**:

```jsx
const JavaScriptExample = (props) => {

  const VELOCIDAD_LUZ = 300000; // km / s
  const SEGUNDOS = RANDOMNUMBER3-25END
  
  return <p>En {SEGUNDOS} segundos la luz recorre {VELOCIDAD_LUZ * SEGUNDOS}</p>
}
```

Para terminar, en `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Será **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <JavaScriptExample />
</div>
```

¿Qué tal se ve en http://localhost:3000?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
