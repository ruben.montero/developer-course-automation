Hay cosas que no se pueden...

## :books: Resumen

* Comprenderemos algunas limitaciones del JavaScript que podemos _embeber_ (`{ }`) en JSX, como que no se pueden poner bucles
* Haremos un componente que muestra los primeros `RANDOMNUMBER7-15END` números en un `<ul>`

## Descripción

Es muy útil usar _llaves_ (`{ }`) para meter JavaScript dentro del JSX.

¡Pero hay limitaciones!

Por ejemplo, NO podemos escribir un bucle `for`:

```jsx
const EjemploComponente = (props) => {
  
  // ¡ESTO NO SE PUEDE HACER!
  return <p>Un bucle de tres números: { for (let i = 0; i<3; i++) { console.log(i) } }</p>
}
```

_(Esto se debe a que una expresión JSX es una declaración no procedimental)_

### ¿Y cómo hago cosas parecidas a un bucle?

En el futuro veremos la función [`map`](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Array/map).

Pero, por el momento... ¡busquemos la manera de usar un `for`!

### El JSX se puede tratar como una variable

Atrás vimos que JSX no es HTML. De hecho, cuando escribimos `<MiComponente />`, en realidad, esto será _transformado_ por el [compilador Babel](https://babeljs.io/docs/en) en una llamada a [`createElement`](https://es.react.dev/reference/react/createElement):

```
React.createElement("MiComponente", {}, []);
```

Por lo tanto, las _expresiones JSX_ pueden... ¡asignarse a variables!

```jsx
ENTERAMENTE VÁLIDO
const miVariable = <MiComponente />
```

## :package: La tarea

**Crea** una carpeta `numbers/` dentro de `src/components/`. Ahí, **crea** un fichero `NumbersList.js` y dentro, un componente homónimo **así**:

```jsx
const NumbersList = (props) => {

  const NUMERO = RANDOMNUMBER7-15END;
  
  const listaDeElementos = []
  
  for (let i = 1; i <= NUMERO; i++) {
    // En una variable... ¡metemos algo visual (JSX)!
    const elementoJsx = <li>{i}</li>
    
    // Y con .push() lo añadimos a la lista. Es como hacer .add() en Java
    listaDeElementos.push(elementoJsx)
  }
  
  return <ul>{listaDeElementos}</ul>
}
```

¿Ves cómo funciona?

Estamos usando un bucle `for` _fuera_ del `return`... y metemos en un _array_ varios elementos `<li>` que luego, simplemente, ¡se muestran!

### Usándolo en `App.js`

Para terminar, en `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Será **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <NumbersList />
</div>
```

¿Qué tal se ve en http://localhost:3000? Si le das a _Click derecho_ > _Inspeccionar_... ¿ves los `<ul>` y `<li>` como cabría esperar?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
