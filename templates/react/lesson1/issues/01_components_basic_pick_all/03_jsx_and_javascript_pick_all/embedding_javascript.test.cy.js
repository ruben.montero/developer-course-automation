/// <reference types="cypress" />

describe('embedding_javascript', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains correct text', () => {
        const VELOCIDAD_LUZ = 300000; // km / s
        const SEGUNDOS = RANDOMNUMBER3-25END
        const texto = "En " + SEGUNDOS + " segundos la luz recorre " + VELOCIDAD_LUZ * SEGUNDOS;

        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', texto);
    })
})
  
