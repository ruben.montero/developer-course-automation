/// <reference types="cypress" />

describe('a_simple_figure', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains a figure with correct image content', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('figure')
        .find('img')
        .should('have.attr', 'src', 'https://interactive-examples.mdn.mozilla.net/media/cc0-images/elephant-660-480.jpg');
    })

    it('issueISSUENUMBER div contains a figure with correct figcaption', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('figure')
        .find('img')
        .siblings()
        .first()
        .should('have.text', 'RANDOMEXPRESSIONUn bonito elefante|Elefante|Elefante y crepúsculo|El elefante del destino|Un elefante en una cacharrería|¡Elefantes y pájaros!END');
    })
})
  
