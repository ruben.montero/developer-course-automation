Un menú sencillo

## :books: Resumen

* Añadiremos un componente sencillo que sirva para _renderizar_ un menú
* Modificaremos la página principal para que se use ese componente y se muestre en nuestra web

## Descripción

¡Continuemos aprendiendo cómo _crear y usar componentes React basados en clases_!

## :package: La tarea

Dentro de la carpeta `src/components/`, **añade** un nueva nueva carpeta `simple_menu/`, y dentro, **crea** el archivo:

* `SimpleMenu.js`

En su interior, **define** un nuevo [componente React](https://es.reactjs.org/docs/components-and-props.html) como ya hemos hecho anteriormente, creando la función `SimpleMenu`. Este componente _renderizará_ (`return`):

* Un elemento `<menu>`
  * Dentro de dicho elemento, habrá **dos** `<li>`
    * Dentro cada `<li>`, habrá un `<a href=...>(...)</a>`

### Contenido de los enlaces del menú

* El **primer enlace** redirigirá a `RANDOMEXPRESSIONhttps://www.google.es|https://www.duckduckgo.es|https://www.wikipedia.orgEND` y mostrará el texto 'Buscador principal'
* El **segundo enlace** redirigirá a `RANDOMEXPRESSIONhttps://www.bing.com|https://www.baidu.com|https://www.search.yahoo.com|https://www.yandex.comEND` y mostrará el texto 'Buscador alternativo'

Si necesitas repasar cómo funciona el elemento `menu` de HTML, puedes visitar la [documentación de Mozilla](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/menu). También hay mucha información en la web sobre [`a href`](https://www.freecodecamp.org/espanol/news/codigo-de-enlace-html-como-insertar-un-enlace-a-un-sitio-web-con-href/).

### Usando el componente desde `App.js`

Una vez tengas tu componente `SimpleMenu`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Tendrá `data-cy='issueISSUENUMBER'` y será **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <SimpleMenu />
</div>
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
