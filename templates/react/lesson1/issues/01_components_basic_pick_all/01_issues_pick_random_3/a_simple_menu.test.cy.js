/// <reference types="cypress" />

describe('a_simple_menu', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div first menu item has correct name', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('menu')
        .find('li')
        .first()
        .find('a')
        .should('have.text', 'Buscador principal');
    })

    it('issueISSUENUMBER div second menu item has correct name', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('menu')
        .find('li')
        .eq(1)
        .find('a')
        .should('have.text', 'Buscador alternativo');
    })

    it('issueISSUENUMBER div first menu item has correct name', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('menu')
        .find('li')
        .first()
        .find('a')
        .should('have.attr', 'href', 'RANDOMEXPRESSIONhttps://www.google.es|https://www.duckduckgo.es|https://www.wikipedia.orgEND');
    })

    it('issueISSUENUMBER div second menu item has correct name', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('menu')
        .find('li')
        .eq(1)
        .find('a')
        .should('have.attr', 'href', 'RANDOMEXPRESSIONhttps://www.bing.com|https://www.baidu.com|https://www.search.yahoo.com|https://www.yandex.comEND');
    })
})
  
