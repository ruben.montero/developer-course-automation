/// <reference types="cypress" />

describe('a_simple_image', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains a figure with correct image content', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('img')
        .should('have.attr', 'src', 'RANDOMEXPRESSIONhttps://raw.githubusercontent.com/rubenmv0/fp/main/t-rex.jpg|https://raw.githubusercontent.com/rubenmv0/fp/main/spin.jpg|https://raw.githubusercontent.com/rubenmv0/fp/main/tric.jpgEND');
    })
})
  
