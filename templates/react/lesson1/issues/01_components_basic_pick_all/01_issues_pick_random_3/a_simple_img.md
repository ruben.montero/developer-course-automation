Una imagen sencilla

## :books: Resumen

* Añadiremos un componente sencillo que sirva para _renderizar_ una imagen
* Modificaremos la página principal para que se use ese componente y se muestre en nuestra web

## Descripción

¡Continuemos aprendiendo cómo _crear y usar componentes React basados en funciones_!

## :package: La tarea

Dentro de la carpeta `src/components/`, **añade** un nueva nueva carpeta `simple_image/`, y dentro, **crea** el archivo:

* `SimpleImage.js`

En su interior, **define** un nuevo [componente React](https://es.reactjs.org/docs/components-and-props.html) como ya hemos hecho anteriormente, creando la función `SimpleImage`. Este componente _renderizará_ (`return`):

* Un elemento `<img>` que mostrará, mediante el atributo `src`, la imagen que se alberga en: `RANDOMEXPRESSIONhttps://raw.githubusercontent.com/rubenmv0/fp/main/t-rex.jpg|https://raw.githubusercontent.com/rubenmv0/fp/main/spin.jpg|https://raw.githubusercontent.com/rubenmv0/fp/main/tric.jpgEND`

Si quieres, puedes [leer más sobre `<img>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img).

### Usando el componente desde `App.js`

Una vez tengas tu componente `SimpleImage`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Tendrá `data-cy='issueISSUENUMBER'` y será **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <SimpleImage />
</div>
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
