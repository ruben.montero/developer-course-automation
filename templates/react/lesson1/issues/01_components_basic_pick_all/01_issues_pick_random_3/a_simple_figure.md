Una figura sencilla

## :books: Resumen

* Añadiremos un componente sencillo que _renderice_ una figura consistente en una imagen y un pie de foto
* Modificaremos la página principal para que se use ese componente y se muestre en nuestra web

## Descripción

¡Continuemos aprendiendo cómo _crear y usar componentes React basados en funciones_!

## :package: La tarea

Dentro de la carpeta `src/components/` **añade** un nueva nueva carpeta `simple_figure/`, y dentro, **crea** el archivo:

* `SimpleFigure.js`

En su interior, **define** un nuevo [componente React](https://es.reactjs.org/docs/components-and-props.html) como ya hemos hecho anteriormente, creando la función `SimpleFigure`. Este componente _renderizará_ (`return`):

* Un elemento `<figure>`.
  * Dentro, un elemento `<img>` que mostrará, mediante el atributo `src`, la imagen que se alberga en: `https://interactive-examples.mdn.mozilla.net/media/cc0-images/elephant-660-480.jpg`
  * También, dentro, un elemento `<figcaption>` que contendrá el texto **RANDOMEXPRESSIONUn bonito elefante|Elefante|Elefante y crepúsculo|El elefante del destino|Un elefante en una cacharrería|¡Elefantes y pájaros!END**.

Si quieres, puedes [leer más sobre `<figure>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/figcaption).

### Usando el componente desde `App.js`

Una vez tengas tu componente `SimpleFigure`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Tendrá `data-cy='issueISSUENUMBER'` y será **así**:

```jsx
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <SimpleFigure />
</div>
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
