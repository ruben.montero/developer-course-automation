Tablas de multiplicar... ¡nuevas!

## :books: Resumen

* Crearemos un componente llamado `NewMultiplicationTableRow` que especifica una fila de una tabla con 3 elementos: El primer y segundo son operandos de una multiplicación; y tercero, el resultado
* Crearemos un componente llamado `NewMultiplicationTable` que pinta la tabla de multiplicar de un número dado usando la `NewMultiplicationTableRow`
* Mostraremos la `NewMultiplicationTable`, para un número, desde la página principal

## Descripción

Las tablas de multiplicar son una parte fundamental del aprendizaje durante la infancia, pues otorgan agilidad en los cálculos matemáticos durante toda la vida. Normalmente, en una temprana etapa de la educación, se estudian las tablas de multiplicar del 1 al 10.

La tabla del 7, por ejemplo, es:

PRIMER OPERANDO|SEGUNDO OPERANDO|RESULTADO
----|----|----|
7|1|7
7|2|14
7|3|21
7|4|28
7|5|35
7|6|42
7|7|49
7|8|54
7|9|63
7|10|70

## :package: La tarea

**Crea** una nueva carpeta `src/components/new_multiplication_table` que contenga **dos** componentes React.

El **primero** será `NewMultiplicationTableRow`. Este componente:

* Interpretará las _props_ `firstOperand` y `secondOperand`
* Devolverá (`return`) un `<tr>` que contenga **3** columnas (`<td>`): La primera será el valor de `firstOperand`. La segunda, `secondOperand`. Y la tercera, el valor del resultado de la multiplicación.


----------------------------------------------


El **segundo** será `NewMultiplicationTable`. Este componente:

* Interpretará la _prop_ `number`, que se esperará que sea un número del 1 al 10 (la tabla a pintar).
* Tendrá un bucle de `10` iteraciones. En cada una de ellas, generará una `NewMultiplicationTableRow`. Para ello, usará como _firstOperand_ el valor de `number`, y como _secondOperand_ los valores del índice del bucle: `1, 2, 3,... 10` sucesivamente. **OJO: EMPEZANDO POR 1; NO 0**.
* Devolverá (`return`) una `<table>`, que contendrá un `<thead>` y un `<tbody>`
  * En el encabezado, una fila (`<tr>`) con **3** columnas (`<th>`): 'PRIMER OPERANDO', 'SEGUNDO OPERANDO', 'RESULTADO'.
  * En el cuerpo, los `10` componentes `NewMultiplicationTableRow`, de tal manera que se visualice una tabla _similar_ a la del ejemplo.

Por **último**, desde `App.js` **crea** otro `div` a continuación del correspondiente al ejercicio anterior, con el atributo `data-cy='issueISSUENUMBER'`. En su contenido, **incluye**:

* Una etiqueta `<h1>` que indique el número de ejercicio como 'Ejercicio ISSUENUMBER'.
* El componente `<NewMultiplicationTable />`, y pasaremos la _prop_ `number=RANDOMNUMBER1-9END`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
