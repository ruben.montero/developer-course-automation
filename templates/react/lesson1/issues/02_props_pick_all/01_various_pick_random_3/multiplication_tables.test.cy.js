/// <reference types="cypress" />

describe('multiplication_tables', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains 1 table with 1 thead', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('thead')
        .should('have.length', 1);
    })

    it('issueISSUENUMBER div contains 1 table with 1 tbody', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .should('have.length', 1);
    })

    it('issueISSUENUMBER thead contains 1 row', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('thead')
        .find('tr')
        .should('have.length', 1);
    })

    it('issueISSUENUMBER div thead contains appropriate FIRST header', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('thead')
        .find('tr')
        .find('th')
        .eq(0)
        .should('have.text', 'PRIMER OPERANDO');
    })

    it('issueISSUENUMBER div thead contains appropriate SECOND header', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('thead')
        .find('tr')
        .find('th')
        .eq(1)
        .should('have.text', 'SEGUNDO OPERANDO');
    })

    it('issueISSUENUMBER div thead contains appropriate THIRD header', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('thead')
        .find('tr')
        .find('th')
        .last()
        .should('have.text', 'RESULTADO');
    })

    it('issueISSUENUMBER div tbody contains 10 rows', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .should('have.length', 10);
    })

    it('issueISSUENUMBER div tbody FIRST ROW has valid first value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .first()
        .find('td')
        .first()
        .should('have.text', RANDOMNUMBER1-9END)
    })

    it('issueISSUENUMBER div tbody FIRST ROW has valid second value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .first()
        .find('td')
        .eq(1)
        .should('have.text', 1)
    })

    it('issueISSUENUMBER div tbody FIRST ROW has correct calculated value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .first()
        .find('td')
        .last()
        .should('have.text', RANDOMNUMBER1-9END)
    })

    it('issueISSUENUMBER div tbody THIRD ROW has valid first value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(2)
        .find('td')
        .first()
        .should('have.text', RANDOMNUMBER1-9END)
    })

    it('issueISSUENUMBER div tbody THIRD ROW has valid second value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(2)
        .find('td')
        .eq(1)
        .should('have.text', 3)
    })

    it('issueISSUENUMBER div tbody THIRD ROW has correct calculated value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(2)
        .find('td')
        .last()
        .should('have.text', RANDOMNUMBER1-9END*3)
    })

    it('issueISSUENUMBER div tbody SIXTH ROW has valid first value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(5)
        .find('td')
        .first()
        .should('have.text', RANDOMNUMBER1-9END)
    })

    it('issueISSUENUMBER div tbody SIXTH ROW has valid second value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(5)
        .find('td')
        .eq(1)
        .should('have.text', 6)
    })

    it('issueISSUENUMBER div tbody SIXTH ROW has correct calculated value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(5)
        .find('td')
        .last()
        .should('have.text', RANDOMNUMBER1-9END*6)
    })

    it('issueISSUENUMBER div tbody NINTH ROW has valid first value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(8)
        .find('td')
        .first()
        .should('have.text', RANDOMNUMBER1-9END)
    })

    it('issueISSUENUMBER div tbody NINTH ROW has valid second value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(8)
        .find('td')
        .eq(1)
        .should('have.text', 9)
    })

    it('issueISSUENUMBER div tbody NINTH ROW has correct calculated value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(8)
        .find('td')
        .last()
        .should('have.text', RANDOMNUMBER1-9END*9)
    })

    it('issueISSUENUMBER div tbody TENTH ROW has valid first value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(9)
        .find('td')
        .first()
        .should('have.text', RANDOMNUMBER1-9END)
    })

    it('issueISSUENUMBER div tbody TENTH ROW has valid second value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(9)
        .find('td')
        .eq(1)
        .should('have.text', 10)
    })

    it('issueISSUENUMBER div tbody TENTH ROW has correct calculated value', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('table')
        .find('tbody')
        .find('tr')
        .eq(9)
        .find('td')
        .last()
        .should('have.text', RANDOMNUMBER1-9END*10)
    })
});
