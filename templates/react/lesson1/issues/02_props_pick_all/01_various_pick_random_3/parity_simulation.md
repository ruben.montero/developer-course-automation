Redundancia en discos duros

## :books: Resumen

* Crearemos un componente llamado `ParitySimulationRow` que especifica una fila de una tabla con 3 elementos de valor 0 ó 1
* Crearemos un componente llamado `ParitySimulationTable` que genera 0s ó 1s aleatoriamente y los pinta en una tabla usando la `ParitySimulationRow`
* Mostraremos la `ParitySimulationTable` desde la página principal

## Descripción

Todo número natural es par o impar. 10 es par. 11 es impar. 33 es impar. 42 es par...

Para calcular si un número es par o impar, la forma más sencilla es dividirlo entre 2. Si el resto de la divisón es `0`, entonces es _par_. Si el resto de la división es `1`, entonces es _impar_. Calcular el resto de una división entera es lo que se conoce en programación como la _operación módulo_, y suele escribirse usando el símbolo porcentaje `%`.

En pseudocódigo:

```
if (numero % 2) == 0 then

  // el número es PAR

else if (numero % 2) == 1 then

  // el número es IMPAR

end if
```

Veamos una aplicación práctica del cálculo de la paridad de un número.

El término [RAID](https://www.partitionwizard.com/resizepartition/raid-types.html), en informática, se refiere al uso combinado de varios discos duros para obtener beneficios de velocidad de acceso y/o redundancia de datos.

Por ejemplo, en un disco duro la información se almacena como una serie de 0s y 1s. En el siguiente esquema se representan como una columna muy larga.

Pues bien, si montamos un RAID 4, estamos dedicando un disco duro a almacenar la paridad (0 ó 1) correspondiente a la suma de los _bits_ de los otros discos duros, de 'datos'. Por ejemplo, así:

DISCO DE DATOS 1|DISCO DE DATOS 2|DISCO DE PARIDAD
----|----|----|
0|0|0 _(0 + 0 = 0; paridad 0 (par))_|
1|0|1 _(1 + 0 = 1; paridad 1 (impar))_|
0|1|1|
0|0|0|
1|1|0 _(1 + 1 = 2; paridad 0 (par))_|
0|0|0|
1|0|1|
1|0|1|
1|1|0|
0|1|1|

### ¿Y esto para qué?

Si un disco de datos falla _(sí, los discos duros fallan. ¡Haz backups!)_, entonces los datos pueden perderse. En caso de tener un disco duro con la paridad, podemos recuperar la información.

### ¿Cómo se recuperan los datos?

Imagínate que el DISCO DE DATOS 2 falla. Sus 0s y 1s se pierden. Entonces, disponiendo del _bit_ de paridad, podemos 'reconstruirlo'. ¿Cómo? El DISCO DE DATOS 1 tiene un `0` en su primera posición. La paridad también es `0`. _Por lo tanto_, el DISCO DE DATOS 2 debe tener un `0` (no puede ser `1`; o la paridad sería `1` también). Y así sucesivamente.

## :package: La tarea

**Crea** una nueva carpeta `src/components/parity_simulation` que contenga **dos** componentes React.

El **primero** será `ParitySimulationRow`. Este componente:

* Interpretará las _props_ `firstBit` y `secondBit`
* Devolverá (`return`) un `<tr>` que contenga **3** columnas (`<td>`): La primera será el valor de `firstBit`. La segunda, `secondBit`. Y la tercera, el valor de la paridad.


----------------------------------------------


El **segundo** será `ParitySimulationTable`. Este componente:

* Calculará en un bucle de `10` iteraciones valores **aleatorios** para _firstBit_ y _secondBit_ (0 ó 1).
* Devolverá (`return`) una `<table>`, que contendrá un `<thead>` y un `<tbody>`
  * En el encabezado, tendrá una fila (`<tr>`) con **3** columnas (`<th>`): 'DISCO DE DATOS 1', 'DISCO DE DATOS 2', 'DISCO DE PARIDAD'.
  * En el cuerpo contendrá `10` componentes `ParitySimulationRow`, de tal manera que se visualice una tabla _similar_ a la del ejemplo.

Por **último**, desde `App.js` **crea** otro `<div>` a continuación del correspondiente al ejercicio anterior, con el atributo `data-cy='issueISSUENUMBER'`. En su contenido, **incluye**:

* Una etiqueta `<h1>` que indique el número de ejercicio como 'Ejercicio ISSUENUMBER'.
* El componente `<ParitySimulationTable />`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
