Control de acceso

## :books: Resumen

* Crearemos un componente llamado `AttendanceControlRow` que especifica una fila de una tabla con 3 elementos: El primer y segundo son horas (0-24h), y el tercero, la diferencia entre ambas
* Crearemos un componente llamado `AttendanceControlTable` que pinta 5 filas con datos aleatorios para las primeras dos columnas
* Mostraremos la `AttendanceControlTable`, desde la página principal

## Descripción

El control del tiempo que alguien invierte en una ubicación o actividad es un problema recurrente en la informática: Control de asistencia a una reunión, control de jornada laboral, tiempo invertido en un curso online,...

Nosotros vamos a programar en React una tabla que simule datos de hora de entrada y hora de salida, y muestre la diferencia (tiempo total) entre ambos. Algo así:

HORA DE ENTRADA|HORA DE SALIDA|MINUTOS
----|----|----|
12:10|15:12|182
7:30|7:35|5
9:0|23:30|750
7:59|6:28|ERROR
0:14|1:14|60

## :package: La tarea

**Crea** una nueva carpeta `src/components/attendance_control` que contenga **dos** componentes React.

El **primero** será `AttendanceControlRow`. Este componente:

* Interpretará las _props_ `entranceHour`, `entranceMinute`, `exitHour` y `exitMinute`, que se esperará que sean valores numéricos
* Devolverá (`return`) una etiqueta `<tr>`, que contenga **3** columnas (`<td>`):
  * La primera `<td>` será el valor de `entranceHour` concatenado al carácter _dos puntos_ (`:`), y a `entranceMinute`. **No** generes dobles ceros (`00`)
  * La segunda `<td>` será el valor de `exitHour` concatenado al carácter _dos puntos_ (`:`), y a `exitMinute`. **No** generes dobles ceros (`00`)
  * La tercera `<td>` será el valor de los minutos transcurridos entre ambas horas. En caso de que la _hora de salida_ sea anterior a la _hora de entrada_, se mostrará el texto `RANDOMEXPRESSIONERROR|MAL|INCORRECTO|FALLOEND`


----------------------------------------------


El **segundo** será `AttendanceControlTable`. Este componente:

* Tendrá un bucle de `5` iteraciones. En cada una de ellas, generará una `AttendanceControlRow`. Para ello, generará un número aleatorio entre 0 y 23 para la hora de entrada; un número aleatorio entre 0 y 59 para el minuto de entrada; un número aleatorio entre 0 y 23 para la hora de salida; y un número aleatorio entre 0 y 59 para el minuto de salida.
* Devolverá (`return`) una `<table>` que contendrá un `<thead>` y un `<tbody>`:
  * En el encabezado habrá una fila (`<tr>`) con **3** columnas (`<th>`): 'HORA DE ENTRADA', 'HORA DE SALIDA', 'MINUTOS'.
  * En el cuerpo estarán los `5` componentes `AttendanceControlRow`, de tal manera que se visualice una tabla _similar_ a la del ejemplo. Usarán los datos generados en el bucle anterior.

Por **último**, desde `App.js` **crea** otro `<div>` a continuación del correspondiente al ejercicio anterior, con el atributo `data-cy='issueISSUENUMBER'`. En su contenido, **incluye**:

* Una etiqueta `<h1>` que indique el número de ejercicio como 'Ejercicio ISSUENUMBER'.
* El componente `<AttendanceControlTable />`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.


