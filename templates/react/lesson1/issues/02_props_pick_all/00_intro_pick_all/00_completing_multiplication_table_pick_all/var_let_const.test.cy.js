/// <reference types="cypress" />

describe('var_let_const', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains correct p', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'He entendido las diferencias entre RANDOMEXPRESSIONconst, let y var|const, var y let|var, let y const|var, const y let|let, var y const|let, const y varEND');
    })
})
  
