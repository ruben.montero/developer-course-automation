/// <reference types="cypress" />

describe('wow_a_coalesce_operator', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains correct p', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'He entendido el RANDOMEXPRESSIONcoalesce operator|operador coalesce|operador de coalesceEND');
    })
})
  
