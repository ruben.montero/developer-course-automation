Multiplica... un valor por defecto

## :books: Resumen

* Modificaremos `MultiplicationTable` para que use reciba una _prop_ especificando _qué_ tabla de multiplicar debe imprimir (1, 2, 3,...)
* Si el componente no recibe dicha _prop_, empleará `RANDOMNUMBER1-9END` por defecto. Así, preservaremos la funcionalidad de ejercicios anteriores

## Descripción

Si en un componente _usamos_ una _prop_ de nombre `apellido`:

```jsx
const EjemploComponente = (props) => {

  return <p>El apellido es: {props.apellido}</p>;
}
```

...entonces, asumimos que _desde otro lugar del código_ estamos _creando_ (invocando) dicho componente, y _pasando_ esa _prop_.

Por ejemplo:

```jsx
const MiComponenteRaiz = (props) => {

  return <div>
      <h1>Un título cualquiera</h1>
      <EjemploComponente apellido={"Etxevarría"} />
    </div>
}
```

¿Pero qué pasa si _no_ enviamos dicha _prop_?

¿Funciona igual?

```jsx
const MiComponenteRaiz = (props) => {

  return <div>
      <h1>Un título cualquiera</h1>
      <EjemploComponente />
    </div>
}
```

### ¡Sí!

No hay problema. La verdad es que, dentro de `EjemploComponente` el valor de `prop.apellido` será... ¡`undefined`!

## :package: La tarea

En `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Será **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <MultiplicationTable table={RANDOMNUMBER2-7END} />
</div>
```

Ahora, de forma coherente, **modifica** `MultiplicationTable.js` para _utilizar_ la _prop_ `table`:

```diff
const MultiplicationTable = (props) => {

-  const NUMERO = RANDOMNUMBER1-9END;
+  const NUMERO = props.table;
  
  const listaDeElementos = []
  
  for (let i = 1; i <= 10; i++) {
    const elemento = <MultiplicationTableItem tablaDeNumero={NUMERO} indice={i} />
    listaDeElementos.push(elemento);
  }
  
  return <RANDOMEXPRESSIONul|olEND>{listaDeElementos}</RANDOMEXPRESSIONul|olEND>
}
```

**Visita** http://localhost:3000. ¿Qué tal lo ves?

Parece el `<h1>Ejercicio ISSUENUMBER</h1>` se ve bien, pero... ¿qué hay de la `<MultiplicationTable />` usada anteriormente? ¿Aquella en la que _no_ pasábamos una _prop_ `table`?

**Modifica** `MultiplicationTable.js` para que use el valor `RANDOMNUMBER1-9END` por defecto. Así, preservaremos la funcionalidad en ejercicios anteriores:

```diff
const MultiplicationTable = (props) => {

-  const NUMERO = props.table;
+  let NUMERO;
+  if (props.table === undefined) {
+    NUMERO = RANDOMNUMBER1-9END;
+  } else {
+    NUMERO = props.table;
+  }
  
  const listaDeElementos = []
  
  for (let i = 1; i <= 10; i++) {
    const elemento = <MultiplicationTableItem tablaDeNumero={NUMERO} indice={i} />
    listaDeElementos.push(elemento);
  }
  
  return <RANDOMEXPRESSIONul|olEND>{listaDeElementos}</RANDOMEXPRESSIONul|olEND>
}
```

¿Qué tal ahora http://localhost:3000? ¿Se vuelve a ver correctamente el primer ejercicio de la tabla de multiplicar?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
