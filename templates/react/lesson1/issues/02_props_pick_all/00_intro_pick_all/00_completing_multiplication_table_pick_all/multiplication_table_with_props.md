Multiplica las posibilidades... ¡con props!

## :books: Resumen

* Modificaremos el componente `MultiplicationTable` creado anteriormente
* Usaremos un subcomponente que recibe _parámetros_ del componente principal... ¡llamadas _props_!

## Descripción

En una tarea anterior hemos escrito un componente así:

```jsx
const MultiplicationTable = (props) => {

  const NUMERO = RANDOMNUMBER1-9END;
  
  const listaDeElementos = []
  
  for (let i = 1; i <= 10; i++) {
    const elemento = <li>{NUMERO} RANDOMEXPRESSIONpor|multiplicado porEND {i} RANDOMEXPRESSIONes|daEND {NUMERO * i}</li>;
    listaDeElementos.push(elemento);
  }
  
  return <RANDOMEXPRESSIONul|olEND>{listaDeElementos}</RANDOMEXPRESSIONul|olEND>
}
```

¿Te has fijado que en el bucle creamos una variable `const elemento` de tipo `<li>`?

```jsx
    const elemento = <li>{NUMERO} RANDOMEXPRESSIONpor|multiplicado porEND {i} RANDOMEXPRESSIONes|daEND {NUMERO * i}</li>;
```

¿Y si en vez de un `<li>` quisiéramos usar un _componente_ nuestro? Digamos, por ejemplo, `<MultiplicationTableItem />`

¿Podríamos hacerlo?

¿Cómo hacemos que cada `<MultiplicationTableItem />` muestre el contenido correcto?

## :package: La tarea

**Crea** un componente `MultiplicationTableItem.js` dentro de la carpeta `src/components/multiplications/`:

```jsx
const MultiplicationTableItem = (props) => {

  const numero1 = props.tablaDeNumero;
  const numero2 = props.indice;

  return <li>{numero1} RANDOMEXPRESSIONpor|multiplicado porEND {numero2} RANDOMEXPRESSIONes|daEND {numero1 * numero2}</li>;
}
```

Por primera vez... ¡estás usando el parámetro `props`!

Estas `props` (_properties_) son una variable especial que contiene las _properties_ que hemos pasado al componente.

Son como _datos de entrada_.

### Modifica `MultiplicationTable.js`

Ahora, para entenderlo mejor, **modifica** `MultiplicationTable.js` así:

```diff
const MultiplicationTable = (props) => {

  const NUMERO = RANDOMNUMBER1-9END;
  
  const listaDeElementos = []
  
  for (let i = 1; i <= 10; i++) {
-   const elemento = <li>{NUMERO} RANDOMEXPRESSIONpor|multiplicado porEND {i} RANDOMEXPRESSIONes|daEND {NUMERO * i}</li>;
+   const elemento = <MultiplicationTableItem tablaDeNumero={NUMERO} indice={i} />
    listaDeElementos.push(elemento);
  }
  
  return <RANDOMEXPRESSIONul|olEND>{listaDeElementos}</RANDOMEXPRESSIONul|olEND>
}
```

_(Recuerda añadir la sentencia `import` necesaria)_

Para terminar, en `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Será **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <p>He entendido que he modificado el componente MultiplicationTable creado anteriormente. Ahora uso un componente MultiplicationTableItem para encapsular lo que muestra cada li. Aunque se ve exactamente igual, ha cambiado su código y ha mejorado... Lo cual se conoce como refactorizar.</p>
</div>
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
