var, let, const

## :books: Resumen

* Hay tres palabras reservadas para declarar variables en JavaScript: `var`, `let` y `const`
* Comprenderemos sus diferencias

## Descripción

En la tarea anterior efectuamos esta modificación:

```diff
-  const NUMERO = props.table;
+  let NUMERO;
+  if (props.table === undefined) {
+    NUMERO = RANDOMNUMBER1-9END;
+  } else {
+    NUMERO = props.table;
+  }
```

¿Te preguntas por qué hemos reemplazado `const` por `let`?

Pues, en realidad, hay tres posibilidades.

* `var`
* `let`
* `const`

### `var`

Era la única palabra reservada para declarar variables antes de 2015, en versiones de ECMAScript[^1] anteriores a la `6`.

Es la más _flexible_ y la _menos deseable_, pues su excesiva flexibilidad puede dar lugar a errores difíciles de _debugear_.

* Una variable declarada con `var` puede cambiar de valor e incluso redeclararse:

```javascript
var nombre = "Alberto";
nombre = "Pepe"; // OK
var nombre = "Pepe Depura" // OK
```

* Su alcance puede alterarse.

### ¿Qué es el alcance de una variable?

El _alcance_ de una variable es el fragmento del programa al que está limitado su uso.

Una variable tiene un alcance global si se declara al principio, o un alcance limitado si se declara en función o bloque de código (`while`, `if`, etc.).

Por ejemplo, en Java:

```java
System.out.println("Aquí empieza el programa");
int i = 0;
while (i < 2) {
  String saludo = "¡Hola!";
  System.out.println(saludo);
  i++;
}
System.out.println(saludo); // Error de compilación
                            // La variable 'saludo' tiene alcance
                            // limitado a dentro del while
```

En JavaScript, las variables definidas con `var` pueden cambiar su alcance en tiempo de ejecución. Eso no es deseable, pues da lugar fácilmente a errores inesperados cuando reutilizamos nombres de variables fuera de funciones.

### `let`

Existe desde ECMAScript `6` y se prefiere como alternativa a `var`:

* Una variable declarada con `let` tiene alcance _limitado_ al bloque de código donde se declaró.

```javascript
console.log("Aquí empieza el código del programa");
let deboSaludar = true;
if (deboSaludar) {
  let saludo = "¡Hola!";
  console.log(saludo);
}
console.log("El valor booleano de deboSaludar es: " + deboSaludar); // OK
console.log(saludo) // Error: El alcance de 'saludo' está limitado a
                    //        dentro del if
```

* Las variables `let` pueden cambiar de valor, pero no redeclararse.

```javascript
let nombre = "Alberto";
nombre = "Pepe"; // OK
let nombre = "Pepe Depura" // Error
```

¡Usa **siempre** `let`, y **nunca** `var`!

### `const`

Una versión más restrictiva que `let`:

* Una variable declarada con `const` tiene alcance _limitado_ al bloque de código donde se declaró (igual que `let`).
* Una variable declarada con `const` es _constante_: Su valor no puede variar.

```javascript
const pi = 3.1416;
pi = 3; // Error
const otraConstante; // Error: Las constantes deben tener
                     //        siempre un valor inicial
```

Debes usar `const` **siempre** que sea posible. Cuando te haga falta una variable en vez de una constante, usa `let`.

## :package: La tarea

En `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Será **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <p>He entendido las diferencias entre RANDOMEXPRESSIONconst, let y var|const, var y let|var, let y const|var, const y let|let, var y const|let, const y varEND</p>
</div>
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: ECMAScript es un _estándar_ que indica las _reglas_ de _cómo_ debe ser un lenguaje de programación (pero no es en sí _un lenguaje_). Este esfuerzo de _estandarización_ es necesario para homogeneizar el ecosistema de la _web_. Muchas veces unos navegadores _web_ soportan cosas distintas. Por ejemplo, en https://canisue.com puedes ver [qué navegadores soportan `BigInt` de JavaScript](https://caniuse.com/?search=bigint). Existen varias versiones de ECMAScript que van modernizándose. Puedes [consultar aquí](https://www.ecma-international.org/publications-and-standards/standards/ecma-262/) cuál es la versión más reciente.
