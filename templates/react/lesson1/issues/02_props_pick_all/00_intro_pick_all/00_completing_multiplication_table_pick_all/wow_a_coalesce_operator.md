var, let, const

## :books: Resumen
 
* Refactorizaremos `MultiplicationTable.js` para usar el operador de _coalesce_ (`??`)

## Descripción

El operador de _coalesce_ está presente en varios lenguajes de programación.

Se usa para dar un valor a una variable, y, en caso de que dicho valor sea _nulo_ (en caso de JavaScript, `undefined`), la variable tomará _otro valor_.

Partiendo de:

```js
// Hasta aquí todo bien
const miVariable = 3;
```

...pero usando otra variable en vez de un `3` _hardcodeado_:

```js
const miNumero = 3;

const miVariable = miNumero;
```

¡Usamos el operador _coalesce_ (`??`) para indicar un valor _por defecto_!

```js
const miNumero = 3;

// Ahora miVariable vale 3, porque miNumero es 3 (y no es undefined)
const miVariable = miNumero ?? 10;
```

Así:

```js
const miNumero = undefined;

// Ahora miVariable vale 10
const miVariable = miNumero ?? 10;
```

## :package: La tarea

**Refactoriza** `MultiplicationTable.js` para que use el _coalesce_ operator, **así**:

```diff
const MultiplicationTable = (props) => {

- let NUMERO;
- if (props.table === undefined) {
-   NUMERO = RANDOMNUMBER1-9END;
- } else {
-   NUMERO = props.table;
- }
+ const NUMERO = props.table ?? RANDOMNUMBER1-9END; 
 
  const listaDeElementos = []
  
  for (let i = 1; i <= 10; i++) {
    const elemento = <MultiplicationTableItem tablaDeNumero={NUMERO} indice={i} />
    listaDeElementos.push(elemento);
  }
  
  return <RANDOMEXPRESSIONul|olEND>{listaDeElementos}</RANDOMEXPRESSIONul|olEND>
}
```

Luego, en `App.js`, **añade** otro `<div>` debajo _(a continuación)_ del correspondiente al ejercicio anterior. Será **así**:

```
<div data-cy='issueISSUENUMBER'>
  <h1>Ejercicio ISSUENUMBER</h1>
  <p>He entendido el RANDOMEXPRESSIONcoalesce operator|operador coalesce|operador de coalesceEND</p>
</div>
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: ECMAScript es un _estándar_ que indica las _reglas_ de _cómo_ debe ser un lenguaje de programación (pero no es en sí _un lenguaje_). Este esfuerzo de _estandarización_ es necesario para homogeneizar el ecosistema de la _web_. Muchas veces unos navegadores _web_ soportan cosas distintas. Por ejemplo, en https://canisue.com puedes ver [qué navegadores soportan `BigInt` de JavaScript](https://caniuse.com/?search=bigint). Existen varias versiones de ECMAScript que van modernizándose. Puedes [consultar aquí](https://www.ecma-international.org/publications-and-standards/standards/ecma-262/) cuál es la versión más reciente.
