/// <reference types="cypress" />

describe('multiplication_table_with_props_and_default_value', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains correct multiplications 1 and 2', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('RANDOMEXPRESSIONul|olEND')
        .find('li')
        .eq(0)
        .should('have.text', 'RANDOMNUMBER2-7END RANDOMEXPRESSIONpor|multiplicado porEND 1 RANDOMEXPRESSIONes|daEND ' + RANDOMNUMBER2-7END*1);
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('RANDOMEXPRESSIONul|olEND')
        .find('li')
        .eq(1)
        .should('have.text', 'RANDOMNUMBER2-7END RANDOMEXPRESSIONpor|multiplicado porEND 2 RANDOMEXPRESSIONes|daEND ' + RANDOMNUMBER2-7END*2);
    })


    it('issueISSUENUMBER div contains correct multiplications 3 and 4', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('RANDOMEXPRESSIONul|olEND')
        .find('li')
        .eq(2)
        .should('have.text', 'RANDOMNUMBER2-7END RANDOMEXPRESSIONpor|multiplicado porEND 3 RANDOMEXPRESSIONes|daEND ' + RANDOMNUMBER2-7END*3);
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('RANDOMEXPRESSIONul|olEND')
        .find('li')
        .eq(3)
        .should('have.text', 'RANDOMNUMBER2-7END RANDOMEXPRESSIONpor|multiplicado porEND 4 RANDOMEXPRESSIONes|daEND ' + RANDOMNUMBER2-7END*4);
    })
    
    it('issueISSUENUMBER div contains correct multiplications 5 and 6', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('RANDOMEXPRESSIONul|olEND')
        .find('li')
        .eq(4)
        .should('have.text', 'RANDOMNUMBER2-7END RANDOMEXPRESSIONpor|multiplicado porEND 5 RANDOMEXPRESSIONes|daEND ' + RANDOMNUMBER2-7END*5);
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('RANDOMEXPRESSIONul|olEND')
        .find('li')
        .eq(5)
        .should('have.text', 'RANDOMNUMBER2-7END RANDOMEXPRESSIONpor|multiplicado porEND 6 RANDOMEXPRESSIONes|daEND ' + RANDOMNUMBER2-7END*6);
    })
    
    it('issueISSUENUMBER div contains correct multiplications 7 and 8', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('RANDOMEXPRESSIONul|olEND')
        .find('li')
        .eq(6)
        .should('have.text', 'RANDOMNUMBER2-7END RANDOMEXPRESSIONpor|multiplicado porEND 7 RANDOMEXPRESSIONes|daEND ' + RANDOMNUMBER2-7END*7);
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('RANDOMEXPRESSIONul|olEND')
        .find('li')
        .eq(7)
        .should('have.text', 'RANDOMNUMBER2-7END RANDOMEXPRESSIONpor|multiplicado porEND 8 RANDOMEXPRESSIONes|daEND ' + RANDOMNUMBER2-7END*8);
    })
    
    it('issueISSUENUMBER div contains correct multiplications 9 and 10', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('RANDOMEXPRESSIONul|olEND')
        .find('li')
        .eq(8)
        .should('have.text', 'RANDOMNUMBER2-7END RANDOMEXPRESSIONpor|multiplicado porEND 9 RANDOMEXPRESSIONes|daEND ' + RANDOMNUMBER2-7END*9);
        
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('RANDOMEXPRESSIONul|olEND')
        .find('li')
        .last()
        .should('have.text', 'RANDOMNUMBER2-7END RANDOMEXPRESSIONpor|multiplicado porEND 10 RANDOMEXPRESSIONes|daEND ' + RANDOMNUMBER2-7END*10);
    })
})
  
