/// <reference types="cypress" />

describe('multiplication_table_with_props', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains correct p', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('p')
        .should('have.text', 'He entendido que he modificado el componente MultiplicationTable creado anteriormente. Ahora uso un componente MultiplicationTableItem para encapsular lo que muestra cada li. Aunque se ve exactamente igual, ha cambiado su código y ha mejorado... Lo cual se conoce como refactorizar.');
    })
})
  
