props: Algo esencial

## :books: Resumen

* Comprenderemos que las _props_ son argumentos de entrada a un componente y cómo se usan
* Crearemos un componente que acepta una _prop_ y lo usaremos

## Descripción

Ya sabes que nuestros componentes React pueden usar atributos llamados _props_, con una sintaxis similar a los atributos en HTML:

```jsx
<Greeting name='Pepe' />
```

Desde dentro del código del componente, los recibimos (como parámetros de una función) y los usamos accediendo a `props`[^1]. Por ejemplo, así:

```jsx
function Greeting(props) {

  return <h1>Hello, {props.name}</h1>;
}
```

¿Observas la flexibilidad que esto le dará a nuestros componentes?

### Importante: Las _props_, dentro del componente, son sólo de lectura

[Las _props_ son _read-only_](https://reactjs.org/docs/components-and-props.html#props-are-read-only). Eso significa que _no_ podemos _escribir_ un valor en ellas desde el interior del componente:

```jsx
function Greeting(props) {

   props.name = "Un nombre"; // Esto es incorrecto
   
   return <h1>Hello, {props.name}</h1>;
}
```

El flujo de los datos, en lo relativo a las `props`, debe ir en un sentido. ¡De arriba a abajo!

```mermaid
graph TD
    A[props] --> B[Component]
    B --> C[Visual output]
```

## :package: La tarea

**Añade** el componente `Welcome.js` a `src/components/welcome/`.

* **Debe** _renderizar_ (`return`): `<RANDOMEXPRESSIONh2|h3|h4|pEND>Hello, {props.name}</RANDOMEXPRESSIONh2|h3|h4|pEND>`
* Dicha etiqueta `<RANDOMEXPRESSIONh2|h3|h4|pEND>` **debe** tener `data-cy='welcomeElement'`

A continuación, desde `App.js`, **crea** un `<div>` a continuación del correspondiente al ejercicio anterior, con el atributo `data-cy='issueISSUENUMBER'`. En su contenido, **incluye**:

* Una etiqueta `<h1>` que indique el número de ejercicio como 'Ejercicio ISSUENUMBER'.
* El componente `<Welcome name='RANDOMEXPRESSIONAlice|Bob|CharlieEND' />`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Realmente, llamarlos `props` dentro de un componente es una convención. Como la función (componente) la escribimos nosotros, podríamos decidir denominar al parámetro como nos apeteciera (por ejemplo, `p` en vez de `props`)

