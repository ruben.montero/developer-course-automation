/// <reference types="cypress" />

describe('simple_welcome', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER element contains an appropriate data-cy', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('RANDOMEXPRESSIONh2|h3|h4|pEND')
        .should('have.attr', 'data-cy', 'welcomeElement');
    })

    it('issueISSUENUMBER element contains correct message', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('RANDOMEXPRESSIONh2|h3|h4|pEND[data-cy=welcomeElement]')
        .should('have.text', 'Hello, RANDOMEXPRESSIONAlice|Bob|CharlieEND');
    })
})
