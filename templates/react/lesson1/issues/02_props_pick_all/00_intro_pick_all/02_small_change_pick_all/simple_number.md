Un número sencillo

## :books: Resumen

* Añadiremos un componente `SimpleNumber.js`
* Añadiremos otro `<div>` a la pantalla principal donde generaremos varios `<SimpleNumber />`... ¡con un `for`!

## Descripción

Como ya sabemos, en React existen cosas que podemos hacer y cosas que no.

¡La cuestión es irnos acostumbrando a ellas!

Como ya sabemos, contrario a nuestra intuición, _no_ podemos meter un `for` _dentro_ de JSX:

```jsx
    return <div className="ejemplo">
       { 
         // ¡ESTO NO SE PUEDE HACER!
         for(let i=0; i<10; i++) {
           { <div> Ejemplo de lo que no se hace </div> }
         }
       }
    <div>
```

Repasemos... ¿cómo podríamos conseguir algo así?

Como cada elemento JSX es, al final, una instancia de una clase JavaScript. Por ello, las podemos añadir a una lista y luego renderizar dicha lista. Por ejemplo, así:

```jsx
    const listaEjemploConDiezDivs = [];
    
    // ESTO SÍ PUEDE SER VÁLIDO
    for(let i=0; i<10; i++) {
      listaEjemploConDiezDivs.push(<div> Ejemplo de lo que sí se hace </div>);
    }
  
    return <div className="ejemplo">
       { listaEjemploConDiezDivs }
    <div>
```

## :package: La tarea

**Crea** una nueva carpeta `src/components/simple_number/`. En dicha carpeta, **crea** el archivo de código `SimpleNumber.js`.

En dicho fichero, **escribe** el componente `SimpleNumber`. **Será** un componente sencillo que sólo pinta un sencillo `<div>` donde muestra el valor de la _prop_ `theNumber`:

```jsx
    return <div>{props.theNumber}</div>;
```

**A continuación**, desde `App.js` **añade** el siguiente código al principio del componente (antes del `return`):

```diff
function App() {
+  const listaConDivs = [];

+  for(let i=0; i<RANDOMNUMBER5-11END; i++) {
+    listaConDivs.push(<SimpleNumber theNumber={i}/>);
+  }
  return (
    <div className="App">
```

_(No te olvides del `import` de `SimpleNumber)`_

Y, para terminar, _en el `return`_ de `App.js` y, **añade** un nuevo `<div>` a continuación del correspondiente al ejercicio anterior, con el atributo `data-cy='issueISSUENUMBER'`. En su contenido, **incluye**:

* Una etiqueta `<h1>` que indique el número de ejercicio como 'Ejercicio ISSUENUMBER'.
* La lista que acabas de crear, así: `{ listaConDivs }`

¿Cómo se ve el resultado?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
