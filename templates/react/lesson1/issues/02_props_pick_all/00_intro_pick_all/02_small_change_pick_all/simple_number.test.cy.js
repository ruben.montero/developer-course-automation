/// <reference types="cypress" />

describe('simple_number', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains correct divs', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('div')
        .should('have.length', RANDOMNUMBER5-11END);
    })
    
    
    
    it('issueISSUENUMBER div last div - 1 has correct content', () => {
		const divIndex = RANDOMNUMBER5-11END - 1
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('div')
        .eq(divIndex)
        .should('have.text', divIndex);
    })
    
    it('issueISSUENUMBER div last div - 2 has correct content', () => {
		const divIndex = RANDOMNUMBER5-11END - 2
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('div')
        .eq(divIndex)
        .should('have.text', divIndex);
    })


})
  
