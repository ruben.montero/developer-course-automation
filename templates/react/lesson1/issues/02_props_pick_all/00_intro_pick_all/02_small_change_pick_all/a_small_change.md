Un pequeño cambio en Welcome

## :books: Resumen

* Efectuaremos una modificación sobre `Welcome.js`

## Descripción

En el ejercicio anterior creamos un componente `<Welcome />`, que da un mensaje de bienvenida. Vamos a modificarlo para controlar su comportamiento en caso de que la _prop_ `name` no sea enviada.

## :package: La tarea

**Modifica** el fichero `Welcome.js` del ejercicio anterior para que, en caso de que el valor de la _prop_ `name` sea `undefined`, se _renderice_:

```jsx
<p data-cy='welcomeStranger'>Hello, RANDOMEXPRESSIONstranger|unknown person|thereEND!</p>
```

**A continuación**, en `App.js` **crea** otro `<div>` a continuación del correspondiente al ejercicio anterior, con el atributo `data-cy='issueISSUENUMBER'`. En su contenido, **incluye**:

* Una etiqueta `<h1>` que indique el número de ejercicio como 'Ejercicio ISSUENUMBER'.
* El componente `<Welcome name='RANDOMEXPRESSIONHomer|Bart|Maggie|Marge|LisaEND' />`.
* El componente `<Welcome />` (así, sin pasar la _prop_ `name`).

¿Cómo se ve el resultado en http://localhost:3000?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
