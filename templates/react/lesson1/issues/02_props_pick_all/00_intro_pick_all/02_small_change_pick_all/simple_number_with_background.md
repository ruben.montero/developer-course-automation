Un número sencillo... ¡con estilo!

## :books: Resumen

* Modificaremos el componente `SimpleNumber.js` para que muestre un fondo condicionalmente

## Descripción

Vamos a implementar un pequeño cambio sobre el `<SimpleNumber />` de la tarea anterior y probarlo.

## :package: La tarea

**Crea** un nuevo fichero llamado `simple_number.css` en la carpeta `src/components/simple_number/`. Dentro, **escribe**:

```css
.red-background {
  background-color: red
}
```

A continuación, edita el fichero `SimpleNumber.js` y **añade** el siguiente _import_, justo al principio:

```
import "./simple_number.css"
```

¡Así es como accedemos a hojas de estilo desde componentes React! 

Luego, **modifica** el componente `SimpleNumber.js` para que devuelva, condicionalmente, un `<div>` con o sin clase en función de la nueva _prop_ `paintRed`, que asumiremos será _booleana_:

```jsx
  if (props.paintRed) {
    return <div className='red-background'>{props.theNumber}</div>
  } else {
    return <div>{props.theNumber}</div>
  }
```

**A continuación**, en `App.js` **añade** código _al principio_ que:

* Declare una nueva variable tipo lista (`[ ]`), llamada `listaConDivs2`
* De forma análoga al ejercicio anterior, la _populará_ con RANDOMNUMBER5-11END `<SimpleNumber />` con `theNumber={i}`
* En este ejercicio, adicionalmente, pasará la _prop_ `paintRed={true}` **sólo** cuando el índice `i` sea **par**

Y, para terminar, _en el `return`_ de `App.js` **añade** un nuevo `<div>` a continuación del correspondiente al ejercicio anterior, con el atributo `data-cy='issueISSUENUMBER'`. En su contenido, **incluye**:

* Una etiqueta `<h1>` que indique el número de ejercicio como 'Ejercicio ISSUENUMBER'.
* La lista que acabas de crear, así: `{ listaConDivs2 }`

¿Se ve como cabría esperar?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
