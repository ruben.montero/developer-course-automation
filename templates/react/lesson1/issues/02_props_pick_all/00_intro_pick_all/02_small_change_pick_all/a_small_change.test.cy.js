/// <reference types="cypress" />

describe('a_small_change', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/')
    })
  
    it('issueISSUENUMBER div contains an appropriate h1', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('h1')
        .should('have.text', 'Ejercicio ISSUENUMBER');
    })

    it('issueISSUENUMBER div contains 1 welcome element', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('[data-cy=welcomeElement]')
        .should('have.length', 1);
    })

    it('issueISSUENUMBER div contains 1 welcome stranger element', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('[data-cy=welcomeStranger]')
        .should('have.length', 1);
    })

    it('issueISSUENUMBER div contains an appropriate content', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('[data-cy=welcomeElement]')
        .should('have.text', 'Hello, RANDOMEXPRESSIONHomer|Bart|Maggie|Marge|LisaEND');
    })

    it('issueISSUENUMBER div contains an appropriate content', () => {
        cy.get('div[data-cy=issueISSUENUMBER]')
        .find('[data-cy=welcomeStranger]')
        .last()
        .should('have.text', 'Hello, RANDOMEXPRESSIONstranger|unknown person|thereEND!');
    })
})
  
