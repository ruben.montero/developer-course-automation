Calendario mensual

## :books: Resumen

* Crearemos un componente llamado `CalendarMonth` que _renderizará_ varios `SimpleNumber` para pintar un mes (enero, febrero) al estilo calendario
* Recibirá dos _props_: El número de días del mes, y el día de semana correspondiente al día 1 del mes. (Será en formato numérico: 0=lunes, 1=martes,... hasta 6)

## Descripción

El calendario que utilizamos en nuestra cultura consta de 12 meses, de entre 28 y 31 días cada uno, y 365 días cada año excepto en los años denominados 'bisiestos', donde hay 366 días. Este calendario se llama [gregoriano](https://es.wikipedia.org/wiki/Calendario_gregoriano) por su promotor el papa Gregorio XIII, que sustituyó gradualmente al anterior calendario juliano.

El mes de noviembre del año 2029 (año en el que Skynet envía a Terminator al pasado para exterminar a John Connor) se podría representar así:

```
Lu  Ma  Mi  Ju  Vi  Sa  Do
            1   2   3   4
5   6   7   8   9   10  11
12  13  14  15  16  17  18
19  20  21  22  23  24  25
26  27  28  29  30
```

### Algo interesante... ¡Flexbox!

[Flexbox](https://developer.mozilla.org/es/docs/Learn/CSS/CSS_layout/Flexbox) es una especificación para CSS que permite _renderizar_ en nuestra web elementos de una colección en una o varias filas, y ajustar su disposición controlando el ancho que ocupan, su justificación o su comportamiento.

Lo principal que debemos saber para esta tarea es que, para un contenedor, especificamos que es de tipo `flex` mediante CSS así:

```css
.box {
  display: flex;
}
```

Además, si queremos que los elementos 'salten' a la siguiente línea en caso de desbordarla, configuraremos `flex-wrap` como `wrap` así:

```css
.box {
  display: flex;
  flex-wrap: wrap;
}
```

Por último, si queremos que un elemento ocupe un porcentaje de la fila en la que está, usaremos CSS para ese elemento así[^1]:

```css
.child {
  flex: 0 1 14% // Este elemento ocupará un 14% del ancho. Van a caber 7 por fila
}
```

## :package: La tarea

**Añade** la clase CSS `.child` vista arriba al fichero `src/components/simple_number/simple_number.css`.

Luego, **modifica** `SimpleNumber.js` para que el componente _siempre_ tenga la clase `child`. **Así**:


```diff
  if (props.paintRed) {
-    return <div className='red-background'>{props.theNumber}</div>
+    return <div className='child red-background'>{props.theNumber}</div>
  } else {
-    return <div>{props.theNumber}</div>
+    return <div className='child'>{props.theNumber}</div>
  }
```

Después, **crea** una nueva carpeta `src/components/calendar/`. En dicha carpeta, **crea** un archivo de estilos `calendar.css` con el CSS de `.box` visto arriba.

### `CalendarMonth.js`

**Crea** `CalendarMonth.js` en `src/components/calendar/`. Devolverá un `<div>` de clase `'box'` que mostrará una lista de `SimpleNumber`:

```jsx
import "./calendar.css";

const CalendarMonth = (props) => {
  const days = [];

  // Aquí rellenamos tantos <SimpleNumber> como sea necesario

  return <div className='box'>{days}</div>;
}
```

**Se pide** que el contenido de `CalendarMonth` ofrezca visualmente los días ordenados en su columna correspondiente, como en el ejemplo al principio de la tarea.

### ¿Cómo conseguir esto?

`CalendarMonth` va a recibir dos _props_. (1) `startWeekDay` y (2) `numberOfDays`.

* `startWeekDay` indicará el día de semana en el que empieza el mes. 0=lunes, 1=martes, etc. En el ejemplo de arriba, esperaríamos que `startWeekDay` valiese 3, pues empieza en jueves.


**Rellena** el _array_ `days` con posiciones en blanco inicialmente. Tantas como `startWeekDay`:

```jsx
for (let i = 0; i<props.startWeekDay; i++) {
  days.push(<SimpleNumber />);
}
```

* `numberOfDays` será el número de días del mes. marzo=31, abril=31, etc.


**Rellena** el _array_ `days` con tantos `<SimpleNumber />` como `props.numberOfDays`. ¡Ojo! Asegúrate que mandas los `theNumber` y `paintRed` correctos. (Deben verse con fondo rojo los días domingo).

### Y para finalizar...

Desde `App.js` **crea** otro `<div>` a continuación del correspondiente al ejercicio anterior, con el atributo `data-cy='issueISSUENUMBER'`. En su contenido, **incluye**:

* Una etiqueta `<h1>` que indique el número de ejercicio como 'Ejercicio ISSUENUMBER'.
* Una etiqueta `<h2>` que indique **RANDOMEXPRESSIONENERO|FEBRERO|MARZO|ABRIL|MAYO|JUNIO|JULIO|AGOSTO|SEPTIEMBRE|OCTUBRE|NOVIEMBRE|DICIEMBREEND** (en mayúsculas)
* Una etiqueta `<h3>` que indique 2023.
* El componente `<CalendarMonth startWeekDay={ ? } numberOfDays={ ? } />`, tal que ambas _props_ están correctamente configuradas para mostrar ese mes del calendario.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

-------------------------------------------------------

[^1]: El `0` se corresponde a [`flex-grow`](https://developer.mozilla.org/es/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox#la_propiedad_flex-grow) e indica, proporcionalmente, cuánto puede crecer el elemento. En nuestro caso, no queremos que crezca para ocupar espacio disponible (piensa en la última fila del calendario). Además, el `1` corresponde a [`flex-shrink`](https://developer.mozilla.org/es/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox#la_propiedad_flex-shrink) e indica cuánto puede encogerse. El tercer elemento, [`flex-basis`](https://developer.mozilla.org/es/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox#la_propiedad_flex-basis), es el más relevante, pues indica la anchura base.
