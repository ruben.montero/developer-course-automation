¡Trabajo terminado!

## :trophy: ¡Felicidades!

Has realizado las tareas del _sprint_ y has validado tu trabajo con los _tests_. ¡Enhorabuena!

<img src="react-introduction/docs/sunset.jpg" width=500 />

Has...

* Empezado a familiarizarte con la filosofía de React
* Escrito JSX (que se parece a HTML... ¡pero no lo es!)
* Empleado la sintaxis _arrow function_ (`=>`) para escribir distintos componentes React, y los has usado desde `App.js`
* Creado e importado CSS, usándolo mediante `className` (en vez del `class` de HTML)
* Creado tablas, figuras, imágenes y menús
* Comprendido que en JSX una expresión debe estar agrupada bajo un único elemento raíz
* Embebido código JavaScript dentro de JSX usando llaves (`{ }`)
* Comprendido que no _todo_ se puede embeber; por ejemplo, no se puede insertar código procedimental (bucles, `if`s,...) en JSX
* Empleado un _array_ (`[ ]`) y metido variables JSX dentro (con `.push`) para mostrar una lista de elementos en JSX
* Empleado _props_ para pasar datos de un componente padre a un componente hijo
* Distinguido entre `var`, `let` y `const` en JavaScript
* Comprendido que, si no se envía una _prop_, en el componente hijo su valor será `undefined`
* Usado el operador _coalesce_ (`??`)