Una ruta para gobernar a todas

## :books: Resumen

* Añadiremos una nueva página `NotFound.js`
* La serviremos siempre que el usuario navegue a una URL de nuestra página que no controlamos, mediante una `<Route>` que use el asterisco o _wildcard_ (`*`)

## Descripción

No sólo podemos especificar la ruta fija por la cual serviremos un componente u otro al cliente. También es posible especificar caracteres especiales.

Uno de ellos es el asterisco o _wildcard_ (`*`), que representa _todas las rutas posibles_.

## :package: La tarea

**Crea** una nueva `screen/NotFound.js`. **Tendrá** un `<div data-cy='pageBody'>` y dentro:

* Un `<h2 data-cy='pageHeader'>`. Indicará _RANDOMEXPRESSIONAquí no está|Lo siento|Si está, no lo veo|No encontrado|¿Sabes por quién preguntas?END_
* Un `<p data-cy='pageText'>`. Indicará _RANDOMEXPRESSION¿Por quién preguntas? Quizá quieras probar otra vez|Creo que te has equivocado. Esa página no existe|Disculpa, pero la página que buscas no está o se la han llevado|Estos no son los androides que estás buscandoEND_
* Un `<Link>` que redirigirá a `'/'` y mostrará _RANDOMEXPRESSIONVolver al inicio|Volver|InicioEND_

A continuación, **añade** una nueva `<Route>` en tu `App.js`, al final del todo:

```jsx
  <Routes>
    ...
    <Route path="*" element={<NotFound/>}></Route>
  </Routes>
```

¿Ves tu nueva página siempre que navegas a una ruta no controlada, por ejemplo http://localhost:3000/pepedepura?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
