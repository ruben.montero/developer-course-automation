/// <reference types="cypress" />

describe('routes_wildcard', () => {
  
  it('issueISSUENUMBER - random page shows not found with correct header', () => {
      cy.visit("http://localhost:3000/asdf" + Math.random().toString().slice(2, 8))
      
      cy.get('div[data-cy=pageBody]')
      .find('h2[data-cy=pageHeader]')
      .should('have.text', 'RANDOMEXPRESSIONAquí no está|Lo siento|Si está, no lo veo|No encontrado|¿Sabes por quién preguntas?END');
  })
  
  it('issueISSUENUMBER - random page shows not found with correct text', () => {
    cy.visit("http://localhost:3000/asdf" + Math.random().toString().slice(2, 8))
    
    cy.get('div[data-cy=pageBody]')
    .find('p[data-cy=pageText]')
    .should('have.text', 'RANDOMEXPRESSION¿Por quién preguntas? Quizá quieras probar otra vez|Creo que te has equivocado. Esa página no existe|Disculpa, pero la página que buscas no está o se la han llevado|Estos no son los androides que estás buscandoEND');
  })

  
  it('issueISSUENUMBER - random page shows not found with correct link', () => {
    cy.visit("http://localhost:3000/asdf" + Math.random().toString().slice(2, 8))
    
    cy.get('div[data-cy=pageBody]')
    .find('a')
    .should('have.attr', 'href', '/')
    .should('have.text', 'RANDOMEXPRESSIONVolver al inicio|Volver|InicioEND');
  })
})
