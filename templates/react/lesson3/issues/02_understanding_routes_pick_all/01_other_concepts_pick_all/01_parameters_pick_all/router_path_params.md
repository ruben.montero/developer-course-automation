Parámetros URL

## :books: Resumen

* Añadiremos una nueva página `Invoice.js` (Factura)
* La serviremos siempre que el usuario navegue a `/invoices/:invoiceId`
* Entenderemos que `:invoiceId` es un parámetro de URL ó _path_

## Descripción

Sabemos especificar rutas _fijas_, y, también, usar un _wildcard_ para servir una página genérica. ¿Existen más posibilidades? ¿Podemos tener más _poder_ con respecto a las rutas de las páginas?

¡Sí! Seguramente has visto muchas veces que _parte de la URL de una web estaba asociada a su contenido_. Por ejemplo:

* https://es.wikipedia.org/wiki/Newton
* https://es.wikipedia.org/wiki/Isaac_Asimov

¿Te das cuenta de que la parte final de la URL especifica el artículo que visitas? Pues bien, esa _variable_ se conoce como _parámetro de ruta_.

## :package: La tarea

**Añade** una nueva `screen/invoices/Invoice.js`:

```jsx
import { useParams } from "react-router-dom";

const Invoice = (props) => {
  const params = useParams(); // Usaremos esto enseguida
  return (
    <div data-cy='issueISSUENUMBERbody'>
    
    </div>
  )
}

export default Invoice;
```

A continuación, en `App.js` **añade** (considera los _imports_ necesarios) la siguiente ruta:

```jsx
  <Route path="/invoices/:invoiceId" element={<Invoice/>}></Route>
```

Añádela antes de `NotFound`.

### ¿Qué es `:invoiceId`?

Esa parte de la ruta representa un [_parámetro de la ruta_ ó _path param_](https://reactrouterdotcom.fly.dev/docs/en/v6/getting-started/tutorial#reading-url-params). Significa que es _variable_. Es decir, tanto si el usuario visita `/invoices/1234` como `/invoices/asdfasdf`, nuestro `<Routes/>` invocará al componente `Invoices.js`.

En tiempo de ejecución, desde `Invoice.js`, puedes acceder a su valor (`1234` o al `asdfasdf`).

### ¿Cómo?

En tu `Invoice.js`, **completa** de la siguiente la sentencia `return`:

```jsx
  return (
    <div data-cy='issueISSUENUMBERbody'>
      <RANDOMEXPRESSIONh2|h3|h4END data-cy='invoiceNumberHeader'>
        RANDOMEXPRESSIONFactura_|Factura Nº|Factura #END{params.invoiceId}
      </RANDOMEXPRESSIONh2|h3|h4END>
    </div>
  )
```

¿Qué ves si visitas http://localhost:3000/invoices/1?

¿Y si visitas http://localhost:3000/invoices/55? ¿http://localhost:3000/invoices/asdfasdf?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
