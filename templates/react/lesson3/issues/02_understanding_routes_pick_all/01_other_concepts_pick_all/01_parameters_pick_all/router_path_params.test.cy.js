/// <reference types="cypress" />

describe('router_path_params', () => {
  
  it('issueISSUENUMBER - invoices page works OK with random number (2,5)', () => {
    const randomNumber = Math.random().toString().slice(2, 5);
      cy.visit("http://localhost:3000/invoices/" + randomNumber)
      
      cy.get('div[data-cy=issueISSUENUMBERbody]')
      .find('RANDOMEXPRESSIONh2|h3|h4END[data-cy=invoiceNumberHeader]')
      .should('have.text', 'RANDOMEXPRESSIONFactura_|Factura Nº|Factura #END' + randomNumber);
  })
  
  it('issueISSUENUMBER - invoices page works OK with random number (2,6)', () => {
    const randomNumber = Math.random().toString().slice(2, 6);
      cy.visit("http://localhost:3000/invoices/" + randomNumber)
      
      cy.get('div[data-cy=issueISSUENUMBERbody]')
      .find('RANDOMEXPRESSIONh2|h3|h4END[data-cy=invoiceNumberHeader]')
      .should('have.text', 'RANDOMEXPRESSIONFactura_|Factura Nº|Factura #END' + randomNumber);
  })
})
