Comprendiendo el enrutado de lado cliente

## :books: Resumen

* Usaremos `<a href>` _directamente_ y `<Link>` para añadir enlaces de `Main.js` a las otras páginas
* Compararemos el comportamiento y extraeremos conclusiones sobre cómo funciona [`react-router-dom`](https://reactrouterdotcom.fly.dev/)
* Añadiremos `<Link>` a la página `About.js` y a `Examples.js`

## Descripción

Normalmente empleamos [`<a href>`](https://www.w3schools.com/tags/att_a_href.asp) para enlazar páginas web, ¿verdad?

Cuando un usuario pulsa en un `<a href>`, el navegador _pide_ (petición [HTTP](https://developer.mozilla.org/es/docs/Web/HTTP)) la nueva página web, la descarga, y la muestra.

Alternativamente, si usamos [`<Link>`](https://reactrouter.com/en/main/components/link) de [`react-router-dom`](https://reactrouterdotcom.fly.dev/)... ¡la navegación es una _ilusión_! No se envía ninguna petición HTTP. Todo está en el navegador (cliente) desde el principio.

¡Veámoslo!

## :package: La tarea

**Añade** `src/screens/main/main.css`:

```css
footer {
  padding: RANDOMNUMBER10-25ENDpx;
  background-color: RANDOMEXPRESSIONrgb(200, 200, 200)|rgb(205, 205, 205)|rgb(210, 210, 210)END;
  color: rgb(90, 90, RANDOMNUMBER220-240END);
}
```

Luego, en `Main.js`, **importa** dicho CSS y, a mayores, la clase `Link`:

```jsx
import { Link } from "react-router-dom";
import "./Main.css"
```

Y **añade** un `<footer>` dentro del `<div>` principal (después del `<h2>)`, **así**:

```jsx
<footer>
  <div className='footer-section'>
    <h3>Enlaces Link</h3>
    <ul>
      <li><Link to='/about'>RANDOMEXPRESSIONAcerca de|Página acerca deEND</Link></li>
      <li><Link to='/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND'>RANDOMEXPRESSIONEjemplos|Página ejemplosEND</Link></li>
    </ul>
  </div>
</footer>
```

¿Puedes navegar desde http://localhost:3000 a tus otras dos páginas, empleado esos enlaces `<Link>`?

¿No te parece una navegación sospechosamente... _rápida_?

### Comparando `<Link to>` con `<a href>`

**Añade** un nuevo `<div className='footer-section'>` _a continuación_ del anterior, dentro del `<footer>`. Esta vez el título será _Enlaces href_ en lugar de _Enlaces Link_:

```diff
<footer>
  <div className='footer-section'>
    <h3>Enlaces Link</h3>
    <ul>
      <li><Link to='/about'>Acerca de</Link></li>
      <li><Link to='/examples'>Ejemplos</Link></li>
    </ul>
  </div>
+  <div className='footer-section'>
+    <h3>Enlaces href</h3>
+    <ul>
+      <li><a href='/about'>Acerca de</a></li>
+      <li><a href='/examples'>Ejemplos</a></li>
+    </ul>
+  </div>
</footer>
```

También, para que las dos secciones del `<footer>` aparezcan una _al lado_ de la otra, en vez de _arriba_ y _abajo_, **añade** lo siguiente a `Main.css`:

```css
footer {
  
  /* otras propiedades */
  /* ... */
  
  overflow: auto; /* Esto hace que el contenedor footer no se colapse debido a que sus hijos tienen el atributo float */
}

.footer-section {
  padding-left: 50px;
  float: left;
}
```

**Compara** la navegación implementada en http://localhost:3000. ¿Percibes la diferencia?

Para terminar, **añade** en `About.js` **y** en `Examples.js` dos enlaces `Link` **debajo** del encabezado `<h2>`. Así:

```jsx
  <h4 data-cy='issueISSUENUMBERlink'>
    <Link to='/'>RANDOMEXPRESSIONVolver a principal|Regresar a principalEND</Link>
  </h4>
```

¡Enhorabuena! Comienzas a construir un conocimiento significativo sobre el enrutado en React y cómo funciona la programación web lado cliente.

### :trophy: Por último

Recuerda que, en tus webs programadas con React, debes usar `<Link to>`, y no `<a href>`.

Sube tus cambios al repositorio en un nuevo _commit_.
