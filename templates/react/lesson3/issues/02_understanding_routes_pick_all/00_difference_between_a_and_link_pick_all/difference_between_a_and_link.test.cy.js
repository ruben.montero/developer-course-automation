/// <reference types="cypress" />

describe('difference_between_a_and_link', () => {

  beforeEach(() => {
    // This is not specific to the test, but Main.js will be
    // requesting this after a certain task, so it might break
    // the test if we don't mock it
    cy.intercept('GET', '**/api/v1/dashboards', { fixture: 'dashboardsList.json' })
  })

  it('issueISSUENUMBER - main page content has footer with section 1 links and header', () => {
      cy.visit("http://localhost:3000")
      
      cy.get('footer')
      .find('div.footer-section')
      .first()
      .find('ul')
      .find('li')
      .should('have.length', 2);

      cy.get('footer')
      .find('div.footer-section')
      .first()
      .find('ul')
      .find('li')
      .first()
      .find('a')
      .should('have.text', 'Acerca de')
      .should('have.attr', 'href', '/about');

      cy.get('footer')
      .find('div.footer-section')
      .first()
      .find('ul')
      .find('li')
      .last()
      .find('a')
      .should('have.text', 'Ejemplos')
      .should('have.attr', 'href', '/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND');

      cy.get('footer')
      .find('div.footer-section')
      .first()
      .find('h3')
      .should('have.text', 'Enlaces Link');
  })

  it('issueISSUENUMBER - main page content has footer with section 1 links and header', () => {
    cy.visit("http://localhost:3000")
    
    cy.get('footer')
    .find('div.footer-section')
    .last()
    .find('ul')
    .find('li')
    .should('have.length', 2);

    cy.get('footer')
    .find('div.footer-section')
    .last()
    .find('ul')
    .find('li')
    .first()
    .find('a')
    .should('have.text', 'RANDOMEXPRESSIONAcerca de|Página acerca deEND')
    .should('have.attr', 'href', '/about');

    cy.get('footer')
    .find('div.footer-section')
    .last()
    .find('ul')
    .find('li')
    .last()
    .find('a')
    .should('have.text', 'RANDOMEXPRESSIONEjemplos|Página ejemplosEND')
    .should('have.attr', 'href', '/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND');

    cy.get('footer')
    .find('div.footer-section')
    .last()
    .find('h3')
    .should('have.text', 'Enlaces a href');
  })

  it('issueISSUENUMBER - main page footer has correct css', () => {
    cy.visit("http://localhost:3000")
  
    cy.get('footer')
    .should('have.css', 'padding', 'RANDOMNUMBER10-25ENDpx')
   .should('have.css', 'background-color', 'RANDOMEXPRESSIONrgb(200, 200, 200)|rgb(205, 205, 205)|rgb(210, 210, 210)END')
    .should('have.css', 'color', 'rgb(90, 90, RANDOMNUMBER220-240END)')
   .should('have.css', 'overflow', 'auto');
  })

  it('issueISSUENUMBER - main page footer sections have correct css', () => {
    cy.visit("http://localhost:3000")
  
    cy.get('footer')
    .find('div.footer-section')
    .should('have.css', 'padding-left', '50px')
    .should('have.css', 'float', 'left');
  })

  it('issueISSUENUMBER - about page has correct return link', () => {
    cy.visit("http://localhost:3000/about")
  
    cy.get('h4[data-cy=issueISSUENUMBERlink]')
    .find('a')
    .should('have.text', 'RANDOMEXPRESSIONVolver a principal|Regresar a principalEND')
    .should('have.attr', 'href', '/');
  })

  it('issueISSUENUMBER - examples page has correct return link', () => {
    cy.visit("http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND")
  
    cy.get('h4[data-cy=issue6link]')
    .find('a')
    .should('have.text', 'RANDOMEXPRESSIONVolver a principal|Regresar a principalEND')
    .should('have.attr', 'href', '/');
  })
})
