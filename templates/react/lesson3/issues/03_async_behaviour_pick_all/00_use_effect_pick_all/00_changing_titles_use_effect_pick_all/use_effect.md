useEffect

## :books: Resumen

* Introduciremos `useEffect`
* Lo usaremos para cambiar el título de la página principal

## Descripción

Vamos a suponer que queremos cambiar el _título de la pestaña_ del navegador.

<img src="react-dashboards/react-frontend/docs/webpage_title.png" width=500 />

Sabemos que se hace con:

```js
document.title = "Título de ejemplo";
```

Podríamos estar tentados de poner ese código en cualquier lugar del componente React:

```jsx
const PaginaDeEjemplo = () => {
  
  // ¡MAL!
  document.title = "Título de ejemplo";
  
  return <div>Lorem ipsum</div>
}
```

Esto estaría... ¡mal!

NO debemos poner código que produce _efectos colaterales_[^1] alegremente en cualquier sitio del componente. 

### ¿Por qué?

Porque un componente es una _función_ que React va a invocar indiscriminadamente _cada vez_ que necesite repintarlo.

¡No controlamos cuándo o cuántas veces se va a invocar!

### Entonces, ¿qué hago?

Para los _efectos colaterales_, existe en React [`useEffect`](https://reactjs.org/docs/hooks-effect.html).

Ahí, podemos poner código que se invoque _después_ de que el componente se _renderice_ (sea invocado):

```mermaid
graph LR
    D[Estado inicial] --> B
    A[Cambian estados o props] --reacciona--> B[Componente se renderiza]
    B --> C[Se invocan los efectos]
```

## :package: La tarea

**Añade** el siguiente `useEffect` al principio de `Main.js`:

```jsx
// Nuevo import
import { useEffect } from "react"

const Main = () => {
    useEffect(() => {
    
    });
    
  // ...
```

Como ves, a `useEffect` le pasamos una función. En este caso, especificada con la sintaxis _arrow_ (`() => { }`).

Ahora, en esa función, **añade** código que cambia el título. **Así**:

```jsx
const Main = () => {
    useEffect(() => {
      document.title = 'RANDOMEXPRESSIONPrincipal|Main|Página 1|Main pageEND'
    });
    
  // ...
```

¿Ves el nuevo título en la pestaña del navegador en http://localhost:3000/?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Cualquier código que afecte a la web más allá del _contexto_ del componente puede catalogarse de _efecto colateral_. Por ejemplo: Mandar una petición a otra página web, establecer una conexión (e.g.: chat, vídeo en streaming), invocar ciertas APIs del navegador...
