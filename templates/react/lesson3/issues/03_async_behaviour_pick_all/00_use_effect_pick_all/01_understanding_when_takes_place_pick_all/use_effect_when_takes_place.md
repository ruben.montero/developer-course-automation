¿Cuándo se ejecuta?

## :books: Resumen

* Entenderemos cuándo se ejecuta `useEffect`
* Comprenderemos que se puede pasar un segundo parámetro (un _array_ `[]`) a `useEffect` para controlar cuándo se ejecuta
* Añadiremos un `useEffect` a `SimpleCounter.js`

## Descripción

Vamos a ver que podemos poner barreras al _efecto_, y decirle que sólo debe ejecutarse _cuando cambien ciertas propiedades reactivas_ (estados o _props_).

Para ello, [`useEffect`](https://react.dev/reference/react/useEffect) admite un segundo parámetro de tipo _array_:

```jsx
useEffect(() => {

}, []) // <-- segundo parámetro []
```

Este _array_ contendrá aquellos estados o _props_ cuyos cambios producirán la ejecución del efecto.

Por el contrario, si un estado o _prop_ no está recogido en este _array_, entonces, aunque cambie, el efecto no se invocará.

## :package: La tarea

**Modifica** `SimpleCounter.js` para que contenga un efecto que cambie el título de la página:

```diff
-import { useState } from "react"
+import { useState, useEffect } from "react"

const SimpleCounter = () => {
  const [number, setNumber] = useState(0);

+ useEffect(()=>{
+     document.title = "RANDOMEXPRESSIONNúmero|Valor|EstadoEND: " + number; 
+ }, [])

  return <div>
      <p>RANDOMEXPRESSIONNúmero|Valor|EstadoEND: {number}</p>
      <button data-cy='issueISSUENUMBERbutton' onClick={ () => {setNumber(number+1) }}>Incrementar</button>
    </div>
}
```

Como ves, el título de la página ahora reflejará el valor de `number`.

¡Pero hemos añadido un _array_ vacío (`[ ]`) como segundo parámetro! Por lo tanto, el efecto sólo sucederá una vez, inicialmente.

Visita http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND.

Cuando clicas en el contador, no debe cambiar el título. ¿Es así?

En la siguiente tarea haremos una pequeña modificación :smiley:

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
