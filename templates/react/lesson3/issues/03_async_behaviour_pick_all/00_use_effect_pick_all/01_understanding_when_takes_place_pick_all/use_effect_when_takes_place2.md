¿Cuándo se ejecuta? (parte II)

## :books: Resumen

* Modificaremos el `useEffect` de `SimpleCounter.js`

## Descripción

Comprendamos lo que dijimos en la tarea anterior sobre el segundo parámetro (_array_) de [`useEffect`](https://react.dev/reference/react/useEffect):

> Este _array_ contendrá aquellos estados o _props_ cuyos cambios producirán la ejecución del efecto.

## :package: La tarea

**Modifica** `SimpleCounter.js` para que, ahora, el _efecto_ se ejecute ante los cambios producidos en `number`:

```diff
import { useState, useEffect } from "react"

const SimpleCounter = () => {
  const [number, setNumber] = useState(0);

  useEffect(()=>{
      document.title = "RANDOMEXPRESSIONNúmero|Valor|EstadoEND: " + number; 
- }, [])
+ }, [number])

  return <div>
      <p>RANDOMEXPRESSIONNúmero|Valor|EstadoEND: {number}</p>
      <button data-cy='issueISSUENUMBERbutton' onClick={ () => {setNumber(number+1) }}>Incrementar</button>
    </div>
}
```

Visita http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND.

¿Cambia ahora el título cada vez que haces _click_ en el contador y cambias `number`?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
