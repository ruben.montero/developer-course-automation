Peticiones asíncronas

## :books: Resumen

* Crearemos un componente `TimestampChecker.js`
* Usaremos la librería `axios` para lanzar una petición a http://time.akamai.com

## Descripción

Visita la siguiente página:

* http://time.akamai.com

Sólo sale un número, ¿verdad?

Es un _timestamp_. Concretamente, el número de segundos que han pasado desde el 1 de enero de 1970[^1].

Vamos a crear un componente que lanza una _petición asíncrona_ a http://time.akamai.com, obtiene la hora y la pinta.

## :package: La tarea

**Abre** un terminal (cmd.exe) y **posiciónate** en la carpeta `react-dashboards/react-frontend/`. Desde ahí, **ejecuta**:

```
npm install axios
```

¡Enhorabuena! Has instalado la librería [`axios`]((https://axios-http.com/docs/example)), que usaremos para que nuestros componentes manden peticiones HTTP _asíncronas_.

Ahora, **añade** un nuevo componente `src/components/timestamp_checker/TimestampChecker.js`:

```jsx
import { useState, useEffect } from "react"

const TimestampChecker = (props) => {
    const [time, setTime] = useState(undefined);
    
    useEffect(() => {

    }, [])
    
    return <div data-cy='timestampChecker'>
        <p data-cy='title'>RANDOMEXPRESSIONA continuación se muestra el timestamp según Akamai:|Este es el timestamp según Akamai:|El timestamp según Akamai:|El timestamp de acuerdo al servidor de Akamai:END</p>
        <p data-cy='timestamp'>{time}</p>
      </div>
}

export default TimestampChecker;
```

Ahora, ¡vamos a mandar la petición HTTP!

Primero, **importa** `axios`:

```js
import axios from "axios";
```

...y luego, en el `useEffect`, **envía** una petición a http://time.akamai.com **así**:

```jsx
    useEffect(() => {
        // Esto se ejecuta cuando sucede el efecto. ¡Enviamos la petición!
        axios.get("http://time.akamai.com")
    }, [])
```

Y después de enviar la petición... Nos _llegará_ una respuesta. Para _hacer cosas_ una vez llegue la respuesta, empleamos `.then`[^2].

**Completa** el código en `TimestampChecker.js` para que, tras obtener la respuesta de http://time.akamai.com, _guarde_ esa respuesta en el estado `time`. **Así**:

```jsx
    useEffect(() => {
        // Esto se ejecuta cuando sucede el efecto. ¡Enviamos la petición!
        axios.get("http://time.akamai.com").then(response => {
            // Esto se ejecuta cuando la respuesta HTTP (será response.data) nos llega
            setTime(response.data);
        })
    }, [])
```

Para finalizar, en `Examples.js`, **añade** un nuevo `<div>` _a continuación_ (debajo) del correspondiente al ejercicio más reciente. En él, **incluye** el componente que hemos creado y además un encabezado, **así**:

```jsx
  <div data-cy='issueISSUENUMBERdiv'>
    <h1>Ejercicio ISSUENUMBER</h1>
    <TimestampChecker />
  </div>
```

¿Ves el _timestamp_ desde http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: También denominado [_timestamp_ Unix](https://es.wikipedia.org/wiki/Tiempo_Unix). Es una convención para representar una fecha y hora en el tiempo independientemente del tipo de calendario que se use.
[^2]: La sintaxis [`async-await`](https://www.freecodecamp.org/espanol/news/como-usar-async-await-para-escribir-un-codigo-mejor-en-javascript/) es más habitual que `.then`
