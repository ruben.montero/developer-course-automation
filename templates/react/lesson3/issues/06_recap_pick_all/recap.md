¡Trabajo terminado!

## :trophy: ¡Felicidades!

Has realizado las tareas del _sprint_ y has validado tu trabajo con los _tests_. ¡Enhorabuena!

<img src="react-dashboards/react-frontend/docs/sunset.jpg" width=500 />

Has...

* Repasado cómo crear distintas páginas con distintas `<Route>`
* Modificado CSS, colores y estilos mediante estados
* Comprendido la diferencia entre `<a>` y `<Link>`
* Usado la ruta _wildcard_ (`*`) para una página de Not Found
* Usado parámetros (`:`) en tus `<Route>`
* Usado `useEffect` para producir efectos tras el renderizado, como cambiar el título
* Aprendido sobre el segundo parámetro de `useEffect`, que permite controlar _qué_ lo afecta
* Instalado y usado la librería `axios` para mandar peticiones HTTP
* Formateado una fecha usando la clase `Date`
* Aprendido sobre APIs REST, y visualizado un fichero de especificación Swagger
* Implementado distintas peticiones GET con `axios` y `useEffect`, para inicializar los datos de páginas de _dashboards_, _preguntas_ y _respuestas_
* Implementado formularios que mandaban peticiones POST con `axios` para subir datos
* Usado un estado para re-lanzar _efectos_ que cargan datos inicialmente, y así re-cargarlos tras publicar una _pregunta_ o una _respuesta_