/// <reference types="cypress" />

describe('introducing_hooks', () => {

  beforeEach(() => {
    // This is not specific to the test, but Main.js will be
    // requesting this after a certain task, so it might break
    // the test if we don't mock it
    cy.intercept('GET', '**/api/v1/dashboards', { fixture: 'dashboardsList.json' })
  })

  it('issueISSUENUMBER - main page content is now a div', () => {
      cy.visit("http://localhost:3000")
      
      cy.get('div[data-cy=issueISSUENUMBERbody]');
  })

  it('issueISSUENUMBER - main page content is now a div, and h2 is still there', () => {
    cy.visit("http://localhost:3000")
    
    cy.get('div[data-cy=issueISSUENUMBERbody]')
    .find('h2[data-cy=pageHeader]');
  })

  it('issueISSUENUMBER - about page content is now a div', () => {
    cy.visit("http://localhost:3000/about")
  
    cy.get('div[data-cy=issueISSUENUMBERbody]');
  })

  it('issueISSUENUMBER - about page content is now a div, and h2 is still there', () => {
    cy.visit("http://localhost:3000/about")

    cy.get('div[data-cy=issueISSUENUMBERbody]')
    .find('h2[data-cy=pageHeader]');
  })

  it('issueISSUENUMBER - examples page contains a new div with correct header', () => {
    cy.visit("http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND")

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('h1')
    .should('have.text', 'Ejercicio ISSUENUMBER');
  })

  it('issueISSUENUMBER - examples page div has correct text label', () => {
    cy.visit("http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND")

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p')
    .should('have.text', 'RANDOMEXPRESSIONNúmero|Valor|EstadoEND: 0');
  })

  it('issueISSUENUMBER - examples page div button increments amount by 4 correctly', () => {
    cy.visit("http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND")

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=issueISSUENUMBERbutton]')
    .click().click().click().click();

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p')
    .should('have.text', 'RANDOMEXPRESSIONNúmero|Valor|EstadoEND: 4');
  })
})
