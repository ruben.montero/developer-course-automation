Otra vez... ¡un contador!

## Resumen

* Añadiremos un `<div data-cy='issueISSUENUMBERbody'>` en `Main.js` y `About.js`
* Añadiremos un componente sencillo que consista en un contador
* Lo usaremos en la página `/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND`

## Descripción

Hasta ahora hemos empleado `useState` de React para almacenar un estado en un componente.

`useState` es un [_hook_](https://reactjs.org/docs/hooks-intro.html). Existe en React desde la versión 16.8 de [febrero de 2019](https://reactjs.org/blog/2019/02/06/react-v16.8.0.html). Antes de ese día... ¡los componentes se escribían como _clases_!

Hay dos _hooks_ principales: `useState` y `useEffect`. Entenderemos y usaremos `useEffect` en futuras tareas.

## :package: La tarea

**Añade** un `<div data-cy='issueISSUENUMBERbody'>` en `Main.js` y `About.js`, de tal manera que **englobe** el `<h2>` ya existente.

Luego, **crea** una carpeta `src/components/`, y, dentro de una subcarpeta, el **crea** siguiente componente:

* `src/components/simple_counter/SimpleCounter.js`

```jsx
import { useState } from "react"

const SimpleCounter = () => {
  const [number, setNumber] = useState(0);

  return <div>
      <p>RANDOMEXPRESSIONNúmero|Valor|EstadoEND: {number}</p>
      <button data-cy='issueISSUENUMBERbutton' onClick={ () => {setNumber(number+1) }}>Incrementar</button>
    </div>
}
```

Para finalizar, en `Examples.js`, **añade** un `<div  data-cy='issueISSUENUMBERdiv'>` debajo _(a continuación)_ del `<h2>` del ejercicio anterior. Dentro de ese `<div>`, **incluye** un `<h1>` y `<SimpleCounter />` **así**:

```jsx
    <div data-cy='issueISSUENUMBERdiv'>
      <h1>Ejercicio ISSUENUMBER</h1>
      <SimpleCounter />
    </div>
```

¿Funciona correctamente http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
