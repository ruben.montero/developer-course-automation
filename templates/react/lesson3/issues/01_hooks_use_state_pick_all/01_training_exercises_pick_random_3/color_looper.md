Bucle de colores

## :books: Resumen

* Añadiremos `SimpleColorLooper.js`, que permitirá cambiar el color de un texto, de entre una lista de 4 colores que se irán repitiendo
* Lo usaremos en la página `/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND`

## Descripción

¡Vamos a practicar!

## :package: La tarea

**Añade** un nuevo componente en `components/simple_color_looper/SimpleColorLooper.js`:

```jsx
import { useState } from "react"

const SimpleColorLooper = () => {
  const [currentCss, setCurrentCss] = useState("simpleColorLooperBackgroundRed");

  return <div data-cy='issueISSUENUMBERdiv'>

    </div>
}
```

Luego, **completa** el `return` y dentro del `<div data-cy='issueISSUENUMBERdiv'>`, **añade**:

* Un `<p data-cy='issueISSUENUMBERtext'>`:
  * Mostrará el texto `RANDOMEXPRESSIONOrdenación por burbuja|Ordenación por inserción|Ordenación por selección|Ordenación por fusión|Ordenación quicksortEND`[^1]
  * Mediante `className` su CSS será manipulado, como se explica más adelante


------------------------------------------------------------


* Un `<button data-cy='issueISSUENUMBERbutton'>`:
  * Mostrará el texto `RANDOMEXPRESSIONCambiar color|Cambio de color|Otro color|Siguiente colorEND`
  * Cuando se pulse, hará que el color _del texto_ (css `color`) cambie, y sea el siguiente color de los 4 de la siguiente lista:
    * (1) `rgb(RANDOMNUMBER128-255END, 0, 0)`
    * (2) `rgb(0, RANDOMNUMBER129-255END, 0)`
    * (3) `rgb(0, 0, RANDOMNUMBER130-255END)`
    * (4) `rgb(255, 255, RANDOMNUMBER131-255END)`
    
¡Tendrás que crear e importar un nuevo fichero `.css`, donde definas las distintas clases (e.g.: `.simpleColorLooperBackgroundRed`)!
    
### Funcionamiento

Se espera que el color del texto _inicialmente_ sea el rojo (1). Después de pulsar el botón _una vez_, el color del texto será (2). Si se pulsa _otra vez_, el color será (3). Si se pulsa _otra vez_, el color será (4). Si se pulsa _otra vez más_, el color del texto volverá a ser (1). Y así en bucle sucesivamente.

### Un último `<div>`

Para ver el componente, **añade** un nuevo `<div>` a la página `RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND` (http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND) y usa el componente recién creado. **Así**:

```jsx
    <div data-cy='issueISSUENUMBERdiv'>
      <h1>Ejercicio ISSUENUMBER</h1>
      <SimpleColorLooper />
    </div>
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

-------------------------------------------------

[^1]: _(¿Cuál es el algoritmo de ordenación más eficiente conocido a día de hoy?)_
