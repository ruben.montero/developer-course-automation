/// <reference types="cypress" />

describe('pick_a_color', () => {

  beforeEach(() => {
    // This is not specific to the test, but Main.js will be
    // requesting this after a certain task, so it might break
    // the test if we don't mock it
    cy.intercept('GET', '**/api/v1/dashboards', { fixture: 'dashboardsList.json' })
    
    cy.visit('http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND')
  })

  it('issueISSUENUMBER div exists and h1 is appropriate', () => {
      cy.get('div[data-cy=issueISSUENUMBERdiv]')
      .find('h1')
      .should('have.text', 'Ejercicio ISSUENUMBER');
  })
  
  it('issueISSUENUMBER div contains a div with valid width', () => {
      cy.get('div[data-cy=issueISSUENUMBERdiv]')
      .find('div[data-cy=issueISSUENUMBERcolor]')
      .should('have.css', 'width', 'RANDOMNUMBER100-200ENDpx');
  })

  it('issueISSUENUMBER div contains a div with valid height', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('div[data-cy=issueISSUENUMBERcolor]')
    .should('have.css', 'height', 'RANDOMNUMBER100-199ENDpx');
  })

  it('issueISSUENUMBER div contains valid first button', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=firstButton]')
    .should('have.text', 'Primer color');
  })

  it('issueISSUENUMBER div firstButton changes div color appropriately', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=firstButton]')
    .click();

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('div[data-cy=issueISSUENUMBERcolor]')
    .should('have.css', 'background-color', 'rgb(RANDOMNUMBER128-255END, 0, 0)');
  })

  it('issueISSUENUMBER div contains valid second button', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=secondButton]')
    .should('have.text', 'Segundo color');
  })

  it('issueISSUENUMBER div secondButton changes div color appropriately', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=secondButton]')
    .click();

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('div[data-cy=issueISSUENUMBERcolor]')
    .should('have.css', 'background-color', 'rgb(0, RANDOMNUMBER129-255END, 0)');
  })

  it('issueISSUENUMBER div contains valid third button', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=thirdButton]')
    .should('have.text', 'Tercer color');
  })

  it('issueISSUENUMBER div thirdButton changes div color appropriately', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=thirdButton]')
    .click();

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('div[data-cy=issueISSUENUMBERcolor]')
    .should('have.css', 'background-color', 'rgb(0, 0, RANDOMNUMBER130-255END)');
  })
})
