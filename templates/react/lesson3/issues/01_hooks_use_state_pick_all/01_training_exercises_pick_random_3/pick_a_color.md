Elegir un color

## :books: Resumen

* Añadiremos `SimpleColorPicker.js` que permitirá cambiar el color de un cuadrado alterando el CSS
* Lo usaremos en la página `/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND`

## Descripción

¡Vamos a practicar!

## :package: La tarea

**Añade** un nuevo componente en `components/simple_color_picker/SimpleColorPicker.js`:

```jsx
import { useState } from "react"

const SimpleColorPicker = () => {
  const [backgroundColorCss, setBackgroundColorCss] = useState("");

  return <div data-cy='issueISSUENUMBERdiv'>

    </div>
}
```

Luego, **completa** el `return` y dentro del `<div data-cy='issueISSUENUMBERdiv'>`, **añade**:

* Un `<div data-cy='issueISSUENUMBERcolor'>`. Tendrá:
  * Ancho (`width`) fijo de RANDOMNUMBER100-200END píxeles, configurado mediante CSS.
  * Alto (`height`) fijo de RANDOMNUMBER100-199END píxeles, configurado mediante CSS.


------------------------------------------------------------


* Tres `<button>`:
  * El primero tendrá `data-cy='firstButton'` e indicará _Primer color_. Cuando se pulse, el **color de fondo** del `<div data-cy='issueISSUENUMBERcolor'>` cambiará a `rgb(RANDOMNUMBER128-255END, 0, 0)`.
  * El segundo tendrá `data-cy='secondButton'` e indicará _Segundo color_. Cuando se pulse, el **color de fondo** del `<div data-cy='issueISSUENUMBERcolor'>` cambiará a `rgb(0, RANDOMNUMBER129-255END, 0)`.
  * El tercero tendrá `data-cy='thirdButton'` e indicará _Tercer color_. Cuando se pulse, el **color de fondo** del `<div data-cy='issueISSUENUMBERcolor'>` cambiará a `rgb(0, 0, RANDOMNUMBER130-255END)`.


¡Tendrás que crear e importar un nuevo fichero `.css`, donde definas las distintas clases que necesites!


### Un último `<div>`

Para ver el componente, **añade** un nuevo `<div>` a la página `/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND` (http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND) y usa el componente recién creado. **Así**:

```jsx
    <div data-cy='issueISSUENUMBERdiv'>
      <h1>Ejercicio ISSUENUMBER</h1>
      <SimpleColorPicker />
    </div>
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
