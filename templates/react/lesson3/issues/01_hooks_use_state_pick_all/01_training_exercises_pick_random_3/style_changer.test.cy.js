/// <reference types="cypress" />

describe('style_changer', () => {

  beforeEach(() => {
    // This is not specific to the test, but Main.js will be
    // requesting this after a certain task, so it might break
    // the test if we don't mock it
    cy.intercept('GET', '**/api/v1/dashboards', { fixture: 'dashboardsList.json' })
    
    cy.visit('http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND')
  })

  it('issueISSUENUMBER div exists and h1 is appropriate', () => {
      cy.get('div[data-cy=issueISSUENUMBERdiv]')
      .find('h1')
      .should('have.text', 'Ejercicio ISSUENUMBER');
  })
  
  it('issueISSUENUMBER div contains a p with valid text', () => {
      cy.get('div[data-cy=issueISSUENUMBERdiv]')
      .find('p[data-cy=issueISSUENUMBERtext]')
      .should('have.text', 'Los RANDOMEXPRESSIONplatelmintos|moluscos|anélidos|equinodermos|insectos|crustáceos|arácnidos|peces|anfibios|reptiles|mamíferosEND son parte del reino animal');
  })

  it('issueISSUENUMBER div contains a button with valid text', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=issueISSUENUMBERbutton]')
    .should('have.text', 'RANDOMEXPRESSIONCambiar estilo|Cambio de estilo|Otro estilo|Siguiente estiloEND');
  })

  it('issueISSUENUMBER div button changes font weight appropriately 1 time', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p[data-cy=issueISSUENUMBERtext]')
    .first()
    .then(($p) => {
      const oldFontWeight = $p.css('font-weight');

      cy.get('div[data-cy=issueISSUENUMBERdiv]')
      .find('button[data-cy=issueISSUENUMBERbutton]')
      .click();
      
      cy.get('p[data-cy=issueISSUENUMBERtext]').first().should('have.not.css', 'font-weight', oldFontWeight);
    })
  })

  
  it('issueISSUENUMBER div button changes font weight appropriately 2 times', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p[data-cy=issueISSUENUMBERtext]')
    .first()
    .then(($p) => {
      const oldFontWeight = $p.css('font-weight');

      cy.get('div[data-cy=issueISSUENUMBERdiv]')
      .find('button[data-cy=issueISSUENUMBERbutton]')
      .click();
      
      cy.get('p[data-cy=issueISSUENUMBERtext]').first().should('have.not.css', 'font-weight', oldFontWeight);
    })

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p[data-cy=issueISSUENUMBERtext]')
    .first()
    .then(($p) => {
      const oldFontWeight = $p.css('font-weight');

      cy.get('div[data-cy=issueISSUENUMBERdiv]')
      .find('button[data-cy=issueISSUENUMBERbutton]')
      .click();
      
      cy.get('p[data-cy=issueISSUENUMBERtext]').first().should('have.not.css', 'font-weight', oldFontWeight);
    })
  })

  
  it('issueISSUENUMBER div button changes font weight appropriately 3 times', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p[data-cy=issueISSUENUMBERtext]')
    .first()
    .then(($p) => {
      const oldFontWeight = $p.css('font-weight');

      cy.get('div[data-cy=issueISSUENUMBERdiv]')
      .find('button[data-cy=issueISSUENUMBERbutton]')
      .click();
      
      cy.get('p[data-cy=issueISSUENUMBERtext]').first().should('have.not.css', 'font-weight', oldFontWeight);
    })

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p[data-cy=issueISSUENUMBERtext]')
    .first()
    .then(($p) => {
      const oldFontWeight = $p.css('font-weight');

      cy.get('div[data-cy=issueISSUENUMBERdiv]')
      .find('button[data-cy=issueISSUENUMBERbutton]')
      .click();
      
      cy.get('p[data-cy=issueISSUENUMBERtext]').first().should('have.not.css', 'font-weight', oldFontWeight);
    })

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p[data-cy=issueISSUENUMBERtext]')
    .first()
    .then(($p) => {
      const oldFontWeight = $p.css('font-weight');

      cy.get('div[data-cy=issueISSUENUMBERdiv]')
      .find('button[data-cy=issueISSUENUMBERbutton]')
      .click();
      
      cy.get('p[data-cy=issueISSUENUMBERtext]').first().should('have.not.css', 'font-weight', oldFontWeight);
    })
  })
})


