/// <reference types="cypress" />

describe('color_looper', () => {

  beforeEach(() => {
    // This is not specific to the test, but Main.js will be
    // requesting this after a certain task, so it might break
    // the test if we don't mock it
    cy.intercept('GET', '**/api/v1/dashboards', { fixture: 'dashboardsList.json' })
    
    cy.visit('http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND')
  })

  it('issueISSUENUMBER div exists and h1 is appropriate', () => {
      cy.get('div[data-cy=issueISSUENUMBERdiv]')
      .find('h1')
      .should('have.text', 'Ejercicio ISSUENUMBER');
  })
  
  it('issueISSUENUMBER div contains a p with valid text', () => {
      cy.get('div[data-cy=issueISSUENUMBERdiv]')
      .find('p[data-cy=issueISSUENUMBERtext]')
      .should('have.text', 'RANDOMEXPRESSIONOrdenación por burbuja|Ordenación por inserción|Ordenación por selección|Ordenación por fusión|Ordenación quicksortEND');
  })

  it('issueISSUENUMBER div contains a button with valid text', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=issueISSUENUMBERbutton]')
    .should('have.text', 'RANDOMEXPRESSIONCambiar color|Cambio de color|Otro color|Siguiente colorEND');
  })

  it('issueISSUENUMBER div button changes p color appropriately 1 time', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=issueISSUENUMBERbutton]')
    .click();

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p[data-cy=issueISSUENUMBERtext]')
    .should('have.css', 'color', 'rgb(0, RANDOMNUMBER129-255END, 0)');
  })

  it('issueISSUENUMBER div button changes p color appropriately 2 times', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=issueISSUENUMBERbutton]')
    .click().click();

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p[data-cy=issueISSUENUMBERtext]')
    .should('have.css', 'color', 'rgb(0, 0, RANDOMNUMBER130-255END)');
  })

  it('issueISSUENUMBER div button changes p color appropriately 3 times', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=issueISSUENUMBERbutton]')
    .click().click().click();

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p[data-cy=issueISSUENUMBERtext]')
    .should('have.css', 'color', 'rgb(255, 255, RANDOMNUMBER131-255END)');
  })

  it('issueISSUENUMBER div button changes p color appropriately 4 times', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=issueISSUENUMBERbutton]')
    .click().click().click().click();

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p[data-cy=issueISSUENUMBERtext]')
    .should('have.css', 'color', 'rgb(RANDOMNUMBER128-255END, 0, 0)');
  })

  it('issueISSUENUMBER div button changes p color appropriately 5 times', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=issueISSUENUMBERbutton]')
    .click().click().click().click().click();

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p[data-cy=issueISSUENUMBERtext]')
    .should('have.css', 'color', 'rgb(0, RANDOMNUMBER129-255END, 0)');
  })

  it('issueISSUENUMBER div button changes p color appropriately 10 times', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('button[data-cy=issueISSUENUMBERbutton]')
    .click().click().click().click().click().click().click().click().click().click();

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p[data-cy=issueISSUENUMBERtext]')
    .should('have.css', 'color', 'rgb(0, 0, RANDOMNUMBER130-255END)');
  })
})

