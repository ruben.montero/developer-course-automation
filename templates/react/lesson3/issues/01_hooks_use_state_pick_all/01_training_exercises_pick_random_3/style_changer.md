Cambiar estilo

## :books: Resumen

* Añadiremos `SimpleStyleChanger.js` que permitirá alterar el estilo de un texto (`normal`, `bold`, `lighter`) alterando el CSS
* Lo usaremos en la página `/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND`

## Descripción

Vamos a crear y probar un nuevo componente React, para aprender más sobre componentes basados en funciones.

## :package: La tarea

**Añade** un nuevo componente en `components/simple_style_changer/SimpleStyleChanger.js`:

```jsx
import { useState } from "react"

const SimpleStyleChanger = () => {
  const [currentTextStyleCss, currentTextStyleCss] = useState("simpleStyleChangerStyleNormal");

  return <div data-cy='issueISSUENUMBERdiv'>

    </div>
}
```

Luego, **completa** el `return` y dentro del `<div data-cy='issueISSUENUMBERdiv'>`, **añade**:

* Un `<p data-cy='issueISSUENUMBERtext'>`:
  * Mostrará el texto `Los RANDOMEXPRESSIONplatelmintos|moluscos|anélidos|equinodermos|insectos|crustáceos|arácnidos|peces|anfibios|reptiles|mamíferosEND son parte del reino animal`[^1]
  * Mediante `className` su CSS será manipulado, como se explica más adelante


------------------------------------------------------------


* Un `<button data-cy='issueISSUENUMBERbutton'>` que:
  * Mostrará el texto `RANDOMEXPRESSIONCambiar estilo|Cambio de estilo|Otro estilo|Siguiente estiloEND`
  * Cuando se pulse, hará que el estilo _del texto_ (css `font-weight`) cambie, y sea uno de los siguientes: `normal`, `bold`, `lighter`, con las siguientes condiciones:
    * El estilo _al que se cambiará_ será elegido **aleatoriamente**
    * **No** se repetirá el mismo estilo anterior. Es decir, siempre _se alterará_ el estilo actual

¡Tendrás que crear e importar un nuevo fichero `.css`, donde definas las distintas clases que necesites (e.g.: `.simpleStyleChangerStyleNormal`)!

### Un último `<div>`

Para ver el componente, **añade** un nuevo `<div>` a la página `/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND` (http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND) y usa el componente recién creado. **Así**:

```jsx
    <div data-cy='issueISSUENUMBERdiv'>
      <h1>Ejercicio ISSUENUMBER</h1>
      <SimpleStyleChanger />
    </div>
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

-------------------------------------------------

[^1]: ¿Recuerdas cómo funciona la clasificación taxonómica de los seres vivos?
