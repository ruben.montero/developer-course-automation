describe('dashboards_length_in_examples', () => {

  beforeEach(() => {
    cy.intercept('GET', '**/api/v1/dashboards', { fixture: 'dashboardsList.json' })
    cy.visit('http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND')
  })

  it('issueISSUENUMBER - examples page div has correct header', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('h1')
    .should('have.text', 'Ejercicio ISSUENUMBER')
  })

  it('issueISSUENUMBER - examples page has correct initial message', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p')
    .should('have.text', 'RANDOMEXPRESSIONHay un total de|Existen|El API REST devuelve|Se han obtenido|Se han recuperadoEND 0 dashboards')
    
  })

  it('issueISSUENUMBER - examples page has correct message after load', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p')
    .should('have.text', 'RANDOMEXPRESSIONHay un total de|Existen|El API REST devuelve|Se han obtenido|Se han recuperadoEND 5 dashboards');
  })
})
