Nuestro API REST dashboards (sobre el papel)

## :books: Resumen

* Veremos nociones básicas de un API REST: Verbos HTTP, peticiones y respuestas
* Introduciremos el API REST _dashboards_ que usaremos en el proyecto
* Señalaremos el fichero OpenAPI (YAML) usado para describir dicho API REST

## Descripción

Cuando se programa una página web lado servidor ([PHP](https://www.php.net/), [Django](https://www.djangoproject.com/),...) es el servidor web quien se encarga establecer una conexión con la base de datos, usando la tecnología que sea, y, luego, responder al cliente con un documento HTML bien formado y completo:

<img src="react-dashboards/django-backend/doc/diagrams/server-side-web-architecture.png" width="500">

Esta es una arquitectura muy _cómoda_ y _segura_ para el cliente, pero _todo el peso_ se lo lleva el servidor. La arquitectura lado servidor, generalmente, produce páginas web más lentas y menos escalables.

¿No funcionaría mejor si le entregamos al cliente una página web con mucho JavaScript que se encargue de pedir los datos ella misma?

<img src="react-dashboards/django-backend/doc/diagrams/client-side-web-architecture.png" width="500">

Una arquitectura cliente _distribuye_ de forma más eficiente la computación necesaria para _renderizar_ las páginas web.

### APIs REST

Una API REST se trata de una _fachada [HTTP](https://es.wikipedia.org/wiki/Protocolo_de_transferencia_de_hipertexto)_ empleada para exponer los datos de una base de datos.

Será útil para que nuestras páginas web consuman o publiquen datos ejecutando código JavaScript. ¡Ojo! También será útil para otras plataformas que necesitan de APIs REST para consumir datos (e.g.: _apps_ móviles).

<img src="react-dashboards/django-backend/doc/diagrams/distributed-app-architecture.png" width="500">

### Peticiones HTTP

Las peticiones HTTP tienen 3 partes:

* **Línea de petición**. En la primera línea se especifica el [verbo](https://www.ramoncarrasco.es/es/content/es/kb/157/resumen-de-verbos-http) (ó método) HTTP y el _recurso_ solicitado. Algo así:

```
GET /publicaciones
```

Los verbos más usados son GET, POST, PUT, DELETE

* **Cabeceras**. Son pares clave-valor que especifican información adicional. Hay unas cuantas [cabeceras predefinidas](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers), pero también podemos incluir nuestras cabeceras _no estándar_.

```
User-agent: Mozilla Firefox
Accept: application/json
```

* **Cuerpo**. Viene después de una línea en blanco e indica información adicional que se manda al servidor. Por ejemplo, si estamos usando una petición POST para crear una nueva _publicación_, seguramente viaja en el cuerpo. El formato del cuerpo es libre, aunque trabajaremos con JSON, como se muestra a continuación:

```
{
  'texto_publicación': '¡Estoy trasteando con APIs REST!'
}
```

### Respuestas HTTP

Las respuestas HTTP también constan de 3 partes:

* **Línea de respuesta**: Contiene un [código de respuesta](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status). ¿Te suena el típico _404 Not Found_? Es un código de respuesta HTTP. Se dividen en 5 categorías: 1xx Información, 2xx Éxito, 3xx Redirección, 4xx Error cliente, 5xx Error servidor.

* **Cabeceras**: Pares clave-valor enviados en la respuesta. Ls famosas _cookies_ viajan en las cabeceras de petición/respuesta.

* **Cuerpo**: Donde el API REST responde información relevante. Suele venir en formato JSON. En caso de una petición web, normalmente obtenemos HTML del servidor.

### Nuestro caso: El API REST _Dashboards_

Nosotros vamos a crear una página de _dashboards_ que son _categorías_ donde los usuarios hacen preguntas y respuestas anónimamente.

Ya está disponible el API REST para el proyecto. Puedes abrir un navegador y consultar cualquiera de los 3 _endpoints_ GET disponibles:

* http://raspi:8081/api/v1/dashboards
* http://raspi:8081/api/v1/dashboards/3
* http://raspi:8081/api/v1/dashboards/3/questions/1

La definición formal de este API REST está disponible en el fichero `react-dashboards/django-backend/doc/swagger/swagger.yaml` de tu repositorio. Está en formato [OpenAPI](https://swagger.io/specification/).

## :package: La tarea

**Abre** Visual Studio Code, y en la pestaña `Complementos`, **instala** `Swagger Viewer`:

<img src="react-dashboards/django-backend/doc/diagrams/swagger_viewer_vscode.png" width="250">

Luego, también desde Visual Studio Code, **abre** el fichero `react-dashboards/django-backend/doc/swagger/swagger.yaml`.

**Pulsa** F1 y ejecuta _Preview Swagger_ (esta opción sólo está disponible si has instalado el complemento `Swagger Viewer`):

<img src="react-dashboards/django-backend/doc/diagrams/preview_swagger.png" width="400">

Deberías ver una _previsualización_ del API REST _dashboards_:

<img src="react-dashboards/django-backend/doc/diagrams/dashboards_api_swagger.png" width="500">

Ahí está la información de _cómo es_ el API REST que atacaremos en las siguientes tareas. Curiosea un poco.

### :trophy: Por último

No hagas nada más. ¡Esta tarea no es de código! No hace falta subir ningún _commit_.
