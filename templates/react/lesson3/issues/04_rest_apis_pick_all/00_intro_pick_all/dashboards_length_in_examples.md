Nuestro API REST dashboards (¡al ataque!)

## :books: Resumen

* Implementaremos una petición sencilla en `Examples.js`, usando `axios`, para pedir una lista de _dashboards_ y mostrar su longitud

## Descripción

Uno de los _endpoints_ de nuestro API REST es el siguiente:

* http://raspi:8081/api/v1/dashboards

Está destinado a devolver una lista de _dashboards_ (la idea es: tablones donde los usuarios preguntan y responden).

¡Pongamos a prueba lo aprendido!

## :package: La tarea

**Añade** un nuevo estado a `Examples.js`:

```jsx
  const [dashboards, setDashboards] = useState([]);
```

Luego, **implementa** un nuevo `useEffect` en `Examples.js`, y con `axios`, **pide** los datos de la lista de _dashboards_ al _endpoint_ REST mencionado anteriormente:

```jsx
  useEffect(() => {
    axios.get('http://raspi:8081/api/v1/dashboards').then(response => {

      // Cuando recibimos la respuesta del API REST,
      // la almacenamos en nuestro estado
      setDashboards(response.data);
    })
  }, [])
```

Finalmente, **añade** un `<div>` a continuación del último ejercicio, que muestre la siguiente información:

```jsx
  <div data-cy='issueISSUENUMBERdiv'>
    <h1>Ejercicio ISSUENUMBER</h1>
    <p>RANDOMEXPRESSIONHay un total de|Existen|El API REST devuelve|Se han obtenido|Se han recuperadoEND {dashboards.length} dashboards</p>
  </div>
```

¡Enhorabuena! En http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND has consumido desde React tus primeros datos de un API REST, y estás mostrando la _longitud_ (`length`) de la respuesta.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
