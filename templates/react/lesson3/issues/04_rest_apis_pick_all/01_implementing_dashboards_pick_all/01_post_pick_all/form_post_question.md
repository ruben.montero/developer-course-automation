Subir (POST) una pregunta (I)

## :books: Resumen

* Crearemos un `<form>` en `DashboardDetailPage.js`
* Todavía no será funcional

## Descripción

En las siguientes tareas vamos a permitir a usuarios anónimos subir preguntas.

Nuestro API REST soporta dicha funcionalidad mediante una petición HTTP POST. Si no lo crees, puedes revisar el fichero `react-dashboards/django-backend/doc/swagger/swagger.yaml` en tu repositorio.

## :package: La tarea

**Crea** `src/screens/dashboard/DashboardDetailPage.css`:

```css
.formContainer {
  margin: 25px auto; /* Con auto, el div se centrará */
  padding: RANDOMNUMBER10-20ENDpx;
  width: 50%;
  background-color: rgb(185, 200, 200);
  text-align: center;
  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px 0px;
}
```

Y, luego, **importa** ese fichero desde `DashboardDetailPage.js`:

```jsx
import "./DashboardDetailPage.css"
```

A continuación, también en `DashboardDetailPage.js` **añade** un `<div>` que contenga el `<form>` donde el usuario podrá escribir el título y la descripción de su pregunta. **Así**:

```diff
  return <div data-cy='issueISSUENUMBERbody'>
      <h1 data-cy='issueISSUENUMBERtitle'>{title}</h1>
      <p data-cy='issueISSUENUMBERdescription'>{description}</p>
      <div data-cy='questionsList'>
      { questions.map((q) => { return <QuestionListItem question={q} dashboardId={params.idDashboard}/> }) }
      </div>
+     <div data-cy='formContainer' className='formContainer'>
+       <h4>RANDOMEXPRESSION¿No encuentras lo que buscas? Haz una pregunta a la comunidad|¿Quieres añadir tu propia pregunta?|Haz tu pregunta a la comunidad|Pregunta lo que necesites saber|¡Adelante! Publica tu preguntaEND</h4>
+       <form data-cy='newQuestionForm'>
+         <input data-cy='newQuestionTitle' placeholder='Título de la pregunta'></input><br/>
+         <input data-cy='newQuestionDescription' placeholder='Texto de la pregunta'></input><br/>
+         <button data-cy='postDataButton'>RANDOMEXPRESSIONPreguntar|Hacer pregunta|Publicar preguntaEND</button>
+       </form>
+     </div>
    </div>
```

¡De momento no es funcional!

En las siguientes tareas iremos implementándolo. De momento, puedes **verificar** su apariencia visual.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
