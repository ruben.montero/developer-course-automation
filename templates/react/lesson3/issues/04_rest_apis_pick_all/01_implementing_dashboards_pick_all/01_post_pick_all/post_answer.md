Subir (POST) una respuesta

## :books: Resumen

* Crearemos un `<form>` en `QuestionDetailPage.js` que permitirá subir una _respuesta_
* Lo enlazaremos a un nuevo estado `formDescription`
* Cuando se haga _submit_ del formulario, mandaremos un HTTP POST
* Añadiremos un nuevo estado para que tras subir una _respuesta_, aparezca automáticamente

## Descripción

Vamos a implementar, de forma análoga a las tareas anteriores, la funcionalidad para _subir una respuesta_.

Recuerda que la especificación para el _endpoint_ POST que usaremos está en el fichero `react-dashboards/django-backend/doc/swagger/swagger.yaml` en tu repositorio.

## :package: La tarea

Como primer paso, **añade** un nuevo estado a `QuestionDetailPage.js`:

```jsx
  const [formDescription, setFormDescription] = useState('');
```

...y dos funciones, **así**:

```jsx
  const handleChangeDescription = (event) => {
    setFormDescription(event.target.value);
  }
  
  const handleSubmit = (e) => {
    e.preventDefault();
    if (formDescription.length == 0) {
      return;
    }
    
    // Vaciar el formulario
    setFormDescription(''); 
  }
```

### `<form>`

**Crea** `src/screens/question/QuestionDetailPage.css`:

```css
.formContainer {
  margin: 25px auto; /* Con auto, el div se centrará */
  padding: RANDOMNUMBER10-20ENDpx;
  width: 50%;
  background-color: rgb(185, 200, 200);
  text-align: center;
  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px 0px;
}
```

Y, luego, **importa** ese fichero desde `QuestionDetailPage.js`:

```jsx
import "./QuestionDetailPage.css"
```

A continuación, **añade** un `<div>` con un `<form>` donde el usuario podrá escribir la respuesta. Será similar al de la tarea anterior. **Añádelo** _debajo_ del `<p data-cy='noAnswers'>`:

```jsx
     <div data-cy='formContainer' className='formContainer'>
       <h4>RANDOMEXPRESSION¡Publica tu respuesta!|Adelante, responde a la pregunta|¿Conoces la respuesta?|¿Puedes dar respuesta a la pregunta?END</h4>
       <form data-cy='newAnswerForm' onSubmit={handleSubmit}>
         <input data-cy='newAnswerQuestionDescription' placeholder='Texto de la respuesta' value={formDescription} onChange={onChangeDescription}></input><br/>
         <button data-cy='postDataButton'>RANDOMEXPRESSIONResponder|Escribir respuesta|Publicar respuestaEND</button>
       </form>
     </div>
``` 

### Petición POST

**Añade** las siguientes líneas a `handleSubmit` para que envíe la petición HTTP POST responsable de _publicar_ la respuesta:

```diff
  const handleSubmit = (e) => {
    e.preventDefault();
    if (formDescription.length == 0) {
      return;
    }
+
+   const httpRequestBody = { description: formDescription }
+   axios.post('http://raspi:8081/api/v1/dashboards/' + params.idDashboard + '/questions/' + params.idQuestion + '/answers', httpRequestBody)

    // Vaciar el formulario
    setFormDescription(''); 
  }
```

### Refrescar automáticamente

Para terminar, igual que en la tarea anterior:

* **Añade** un nuevo _estado_ (por ejemplo, `answersPublishedRightNow`) con valor por defecto `0`
* **Asegúrate** de que el `useEffect` en `QuestionDetailPage.js` reacciona ante ese estado
* **Modifica** ese estado (incrementándolo en `1`) si la petición POST para publicar una _respuesta_ tiene éxito (`.then`)

¡Enhorabuena!

Ya funciona la publicación de _respuestas_.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
