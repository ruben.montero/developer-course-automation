Mostrando una lista de preguntas (I)

## :books: Resumen

* Crearemos un componente `QuestionListItem.js` que será usado para pintar una celda que muestra una _pregunta_
* De momento, sólo mostraremos uno de prueba en `Examples.js`

## Descripción

Vamos a crear un componente que representará una _celda_ que representará una _pregunta_.

## :package: La tarea

**Crea** `src/components/question_list_item/QuestionListItem.css` con el siguiente CSS:

```css
.question-li {
  margin: RANDOMNUMBER10-20ENDpx;
  padding: 5px;
  background-color: rgb(RANDOMNUMBER240-250END, 240, 240);
  text-align: center;
  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px 0px;
}

.question-li:hover {
  cursor: pointer;
  background-color: rgb(220, 230, 230);
}
```

A continuación, en la misma carpeta, **crea** `src/components/question_list_item/QuestionListItem.js`.

```jsx
import { useNavigate } from 'react-router-dom';
import "./QuestionListItem.css"

const QuestionListItem = (props) => {
  const navigate = useNavigate();
  
  const onClick = () => {
    // Esto navega a una URL que todavía no hemos implementado...
    // TO-DO: Implementar detalle de pregunta (lista de respuestas)
    // Lo haremos en un par de tareas
    navigate('questions/'+props.question.question_id);
  }

  return <div onClick={onClick} className='question-li' key={props.question.question_id}>
    <h3>{props.question.title}</h3>
    <p>{props.question.description}</p>
  </div>
}

export default QuestionListItem;
```

¡Componente visual listo!

Probémoslo de la siguiente manera:

En `Examples.js`, **añade** un `<div>` a continuación del último ejercicio, **así**:

```jsx
  <div data-cy='issueISSUENUMBERdiv'>
    <h1>Ejercicio ISSUENUMBER</h1>
    <p>RANDOMEXPRESSIONAhora|Aquí|A continuación|DebajoEND se muestra un QuestionListItem de ejemplo</p>
    <QuestionListItem question={{question_id: 0, title: "RANDOMEXPRESSIONTest|Título|TitleEND", description: "RANDOMEXPRESSIONContenido de prueba|Contenido test|Contenido mock|Contenido de mentirijillaEND"}} />
  </div>
```

¿Ves tu nuevo componente en http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND?

En la siguiente tarea le daremos un uso más _real_.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
