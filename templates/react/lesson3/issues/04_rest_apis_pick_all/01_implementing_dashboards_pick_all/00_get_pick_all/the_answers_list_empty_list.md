Una pequeña mejora... ¡sin respuesta!

## :books: Resumen

* Añadiremos un `<p>` a `QuestionDetailPage.js` que mostrará un texto en caso de que no haya _respuestas_ a una _pregunta_

## Descripción

### El _operador ternario_ (`? :`)

¡Es una especie de `if` compactado!

```
expresion booleana ? valor si se cumple : valor si no se cumple
```

Por ejemplo:

```js
const edad > 18 ? "Puedes pasar" : "No puedes pasar"

console.log(edad); 
```

Vamos a introducir una pequeña mejora en tu web `react-dashboards`...

* http://localhost:3000

...usando el _operador ternario_ (`? :`).

## :package: La tarea

**Añade** en `src/screens/question/QuestionDetailPage.js` el siguiente `<p>`:

```diff
  return <div data-cy='issueISSUENUMBERbody'>
      <h1 data-cy='issueISSUENUMBERtitle'>{questionTitle}</h1>
      <p data-cy='issueISSUENUMBERdescription'>{questionDescription}</p>
      <div data-cy='answersList'>
      { answers.map((a) => { return <AnswerListItem answer={a}/> }) }
      </div>
+     <p data-cy='noAnswers'>{answers.length === 0 ? "RANDOMEXPRESSION¡Vaya! No hay respuestas|Parece que no hay respuestas|Nadie ha contestado nada todavía|Aquí no hay respuestas|Parece que nadie sabe la respuestaEND" : ""}</p>
    </div>
```

Ahora encuentra alguna _pregunta_ para la que no haya _respuestas_. Deberías ver el texto `RANDOMEXPRESSION¡Vaya! No hay respuestas|Parece que no hay respuestas|Nadie ha contestado nada todavía|Aquí no hay respuestas|Parece que nadie sabe la respuestaEND` en tu página.

¡Enhorabuena!

Has empleado el operador ternario, que permite escribir `if`s de forma compacta y se puede embeber en JSX.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
