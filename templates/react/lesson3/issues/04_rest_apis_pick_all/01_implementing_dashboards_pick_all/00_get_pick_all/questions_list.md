Mostrando una lista de preguntas (II)

## :books: Resumen

* Crearemos `DashboardDetailPage.js` y lo enlazaremos con una `<Route>` a http://localhost:3000/dashboards/:idDashboard
* En `DashboardDetailPage.js` pediremos el _detalle de un dashboard_ (lista de preguntas) y las almacenaremos en un estado `questions`, además de otros datos
* Mediante `questions.map` transformaremos cada elemento en un `<QuestionListItem>`

## Descripción

Entendamos que, nuestro API REST tiene un _modelo_ donde _dashboards_ contienen _preguntas_, y _preguntas_ contienen _respuestas_:

```mermaid
graph TD
    D[Dashboard  1] --> B[Question 1]
    D[Dashboard  1] --> A[Question 2]
    D[Dashboard  1] --> C[Question N]
    B --> E[Answer 1]
    B --> F[Answer 2]
    B --> G[Answer N]
    H[Dashboard N]
```

Nosotros, ahora, vamos a crear un componente para ver el _detalle_ de un _dashboard_, y mostraremos la lista de preguntas asociadas:

```mermaid
graph TD
    D[Dashboard ] --> B[Question 1]
    D[Dashboard ] --> A[Question 2]
    D[Dashboard ] --> C[Question N]
```

El _endpoint_ REST que consumiremos (según qué _dashboard_ queramos mostrar, e.g.: 1, 2,...) será:

* http://raspi:8081/api/v1/dashboards/1
* http://raspi:8081/api/v1/dashboards/2
* ...

¡A trabajar!

## :package: La tarea

**Crea** `src/screens/dashboard/DashboardDetailPage.js`:

```jsx
import { useParams } from "react-router-dom";

const DashboardDetailPage = (props) => {
  const params = useParams();

  return <div></div>
}

export default DashboardDetailPage;
```

...y en `App.js` **incluye** la siguiente ruta:

```jsx
      <Route path="/dashboards/:idDashboard" element={<DashboardDetailPage/>}></Route>
```

Ya puedes verificar lo que vayas haciendo desde:

* http://localhost:3000/dashboards/1

A continuación, en `DashboardDetailPage.js` **añade** los siguientes estados:

```jsx
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [questions, setQuestions] = useState([]);
```

Y **añade** este `useEffect` para pedir al API REST los datos detallados del _dashboard_:

```jsx
  useEffect(() => {
    axios.get('http://raspi:8081/api/v1/dashboards/' + params.idDashboard).then(response => {
      setTitle(response.data.title);
      setDescription(response.data.description);
      setQuestions(response.data.questions);
    })
  }, [])
```

Y, para terminar, **modifica** el `return` en `DashboardDetailPage.js` para que sea el siguiente:

```jsx
  return <div data-cy='issueISSUENUMBERbody'>
      <h1 data-cy='issueISSUENUMBERtitle'>{title}</h1>
      <p data-cy='issueISSUENUMBERdescription'>{description}</p>
      <div data-cy='questionsList'>
      { questions.map((q) => { return <QuestionListItem question={q} /> }) }
      </div>
    </div>
```

**Verifica** que puedes ver el detalle de cada _dashboard_ correctamente.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
