Mostrando una lista de respuestas (I y II)

## :books: Resumen

* Crearemos un componente `AnswerListItem.js` que será usado para pintar una celda que muestra una _respuesta_
* Crearemos `QuestionDetailPage.js` y lo enlazaremos con una `<Route>` a http://localhost:3000/dashboards/:idDashboard/questions/:idQuestion
* En `QuestionDetailPage.js` pediremos el _detalle de una pregunta_ (lista de respuestas) y las almacenaremos en un estado `answers`, además de otros datos
* Mediante `answers.map` transformaremos cada elemento en un `<AnswerListItem>`

## Descripción

Recordemos que en nuestro proyecto hay tres niveles de jerarquía lógicos:

```mermaid
graph TD
    D[Dashboard  1] --> B[Question 1]
    D[Dashboard  1] --> A[Question 2]
    D[Dashboard  1] --> C[Question N]
    B --> E[Answer 1]
    B --> F[Answer 2]
    B --> G[Answer N]
    H[Dashboard N]
```

Ahora es momento de trabajar en el último eslabón:

```mermaid
graph TD
    D[Question] --> B[Answer 1]
    D --> A[Answer 2]
    D --> C[Answer N]
```

Así que, si lo deseas, puedes eliminar el comentario que dejamos hace un par de tareas en `QuestionListItem.js`:

```diff
  const onClick = () => {
-   // Esto navega a una URL que todavía no hemos implementado...
-   // TO-DO: Implementar detalle de pregunta (lista de respuestas)
-   // Lo haremos en un par de tareas
    navigate('questions/'+props.question.question_id);
  }
```

## :package: La tarea

**Crea** `src/components/answer_list_item/AnswerListItem.css` con el siguiente CSS:

```css
.answer-li {
  margin: RANDOMNUMBER25-30ENDpx;
  padding: RANDOMNUMBER10-15ENDpx;
  background-color: rgb(RANDOMNUMBER235-245END, 200, 200);
  text-align: center;
  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px 0px;
}
```

Luego, **crea** un nuevo componente: `src/components/answer_list_item/AnswerListItem.js` con **estas** características:

* **Tendrá** un `<div className='answer-li' key={props.answer.answer_id}>` raíz
* Dicho `<div>` **contendrá** un `<p>{props.answer.description}</p>`

A continuación, **crea** `src/screens/question/QuestionDetailPage.js`:

```jsx
import { useParams } from "react-router-dom";

const QuestionDetailPage = (props) => {
  const params = useParams();

  return <div></div>
}

export default QuestionDetailPage;
```

...y en `App.js` **incluye** la siguiente ruta:

```jsx
      <Route path="/dashboards/:idDashboard/questions/:idQuestion" element={<QuestionDetailPage/>}></Route>
```

Ya puedes verificar lo que vayas haciendo desde:

* http://localhost:3000/dashboards/1/questions/1

A continuación, en `QuestionDetailPage.js` **añade** los siguientes estados:

```jsx
  const [questionTitle, setQuestionTitle] = useState('');
  const [questionDescription, setQuestionDescription] = useState('');
  const [answers, setAnswers] = useState([]);
```

Y **añade** este `useEffect` para pedir al API REST los datos detallados de la _pregunta_:

```jsx
  useEffect(() => {
    axios.get('http://raspi:8081/api/v1/dashboards/' + params.idDashboard + '/questions/' + params.idQuestion).then(response => {
      setQuestionTitle(response.data.question_title);
      setQuestionDescription(response.data.question);
      setAnswers(response.data.answers);
    })
  }, [])
```

Y, para terminar, **modifica** el `return` en `QuestionDetailPage.js` para que sea el siguiente:

```jsx
  return <div data-cy='issueISSUENUMBERbody'>
      <h1 data-cy='issueISSUENUMBERtitle'>{questionTitle}</h1>
      <p data-cy='issueISSUENUMBERdescription'>{questionDescription}</p>
      <div data-cy='answersList'>
      { answers.map((a) => { return <AnswerListItem answer={a}/> }) }
      </div>
    </div>
```

**Verifica** tu resultado.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
