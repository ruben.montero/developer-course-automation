describe('the_answers_list_empty_list', () => {
  beforeEach(() => {
    cy.intercept('GET', '**/api/v1/dashboards/3/questions/1', { fixture: 'question1.json' })
    cy.intercept('GET', '**/api/v1/dashboards/3/questions/2', { fixture: 'question2.json' })
  })

  it('issueISSUENUMBER - question 1 page has correct EMPTY p NO ANSWERS text', () => {
    cy.visit('http://localhost:3000/dashboards/3/questions/1')
    cy.wait(1000)

    cy.get('p[data-cy=noAnswers]')
    .should('have.text', '');
  })

  it('issueISSUENUMBER - question 2 page has correct p NO ANSWERS text', () => {
    cy.visit('http://localhost:3000/dashboards/3/questions/2')
    cy.wait(1000)

    cy.get('p[data-cy=noAnswers]')
    .should('have.text', 'RANDOMEXPRESSION¡Vaya! No hay respuestas|Parece que no hay respuestas|Nadie ha contestado nada todavía|Aquí no hay respuestas|Parece que nadie sabe la respuestaEND');
  })
})

