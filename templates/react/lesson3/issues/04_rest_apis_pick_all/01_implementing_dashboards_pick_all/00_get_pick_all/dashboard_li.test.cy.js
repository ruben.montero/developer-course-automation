describe('dashboards_li', () => {

  beforeEach(() => {
    cy.intercept('GET', '**/api/v1/dashboards', { fixture: 'dashboardsList.json' })
    cy.visit('http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND')
  })

  it('issueISSUENUMBER - examples page div has correct header', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('h1')
    .should('have.text', 'Ejercicio ISSUENUMBER')
  })

  it('issueISSUENUMBER - examples page has correct initial message', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('p')
    .first()
    .should('have.text', 'RANDOMEXPRESSIONAhora|Aquí|A continuación|DebajoEND se muestra un DashboardListItem de ejemplo')
  })

  it('issueISSUENUMBER - examples page dashboardlistitem has correct title, description and css', () => {
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('div')
    .first()
    .find('h3')
    .should('have.text', 'RANDOMEXPRESSIONTest|Título|TitleEND')
    
    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('div')
    .first()
    .find('p')
    .should('have.text', 'RANDOMEXPRESSIONContenido de prueba|Contenido test|Contenido mock|Contenido de mentirijillaEND');

    cy.get('div[data-cy=issueISSUENUMBERdiv]')
    .find('div')
    .first()
    .should('have.css', 'margin', 'RANDOMNUMBER15-25ENDpx')
    .should('have.css', 'padding', '5px')
    .should('have.css', 'background-color', 'rgb(RANDOMNUMBER230-240END, 240, 240)')
    .should('have.css', 'text-align', 'center')
    .should('have.css', 'box-shadow', 'rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px 0px')
  })
})
