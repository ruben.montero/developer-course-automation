Mostrando la lista de dashboards (II)

## :books: Resumen

* En `Main.js` pediremos la lista de _dashboards_ y los almacenaremos en un nuevo estado `dashboards`
* Mediante `dashboards.map` transformaremos cada elemento en un `<DashboardListItem>`

## Descripción

Para ayudarnos a entender lo que haremos en esta tarea, nos acompañará una metáfora: Queremos montar un museo de juguetes.

Para ello, pediremos los juguetes desde el museo. Cuando los recibamos, guardaremos las cajas. Dentro del museo, los sacaremos de las cajas y los prepararemos para la exposición.

<img src="react-dashboards/react-frontend/docs/consuming-rest-api-metaphor.png" width=650 />

¡Vamos a ello!

## :package: La tarea

Lo primero será _pedir los juguetes al proveedor_, y _recibirlos en sus cajas_:

**Añade** a `Main.js` un estado `dashboards`:

```jsx
  const [dashboards, setDashboards] = useState([])
```
Más abajo, en `Main.js`, **añade** un `useEffect` que se encarga de pedir los datos (GET) a http://raspi:8081/api/v1/dashboards y usa `.then` para gestionar la respuesta:

```jsx
  useEffect(() => {
    axios.get('http://raspi:8081/api/v1/dashboards').then(response => {
      setDashboards(response.data);
    })
  }, [])
```

Ahora toca _sacar los juguetes de las cajas y mostrarlos_.

En `Main.js`, **añade** un `<div data-cy='dashboardsList'>` debajo del `<h2>` (antes del `<footer>`):

```jsx
    <div data-cy='dashboardsList'>

    </div>
```

Y, dentro de él, **invoca** `.map` sobre el estado `dashboards`, del forma que cada _dashboard_ sea transformado en un `<DashboardListItem />`.

Será fácil, pues en la tarea anterior ya preparamos `DashboardListItem.js` para que recibiera una _prop_ `dashboard` (recuerda: Es un objeto con `id`, `title` y `description`):

```jsx
    <div data-cy='dashboardsList'>
      { dashboards.map((d) => { return <DashboardListItem dashboard={d}/> }) }
    </div>
```

_(Acuérdate de importar `DashboardListItem`)_

¡Los juguetes ya están montados y listos para exponer!

<img src="react-dashboards/react-frontend/docs/consuming-rest-api-metaphor-explained.png" width=650 />

**Verifica** que puedes ver la lista de _dashboards_ en http://localhost:3000

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
