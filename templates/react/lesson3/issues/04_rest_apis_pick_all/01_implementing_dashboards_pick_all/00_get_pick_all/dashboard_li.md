Mostrando la lista de dashboards (I)

## :books: Resumen

* Crearemos un componente `DashboardListItem.js` que será usado para pintar una celda que muestra un _dashboard_
* De momento, sólo mostraremos uno de prueba en `Examples.js`

## Descripción

Vamos a crear un componente que representará una _celda_.

En la siguiente tarea, una lista de estas _celdas_ servirán para dar apariencia visual a la respuesta de API REST:

* http://raspi:8081/api/v1/dashboards

De momento... ¡sólo crearemos la _celda_!

## :package: La tarea

**Crea** `src/components/dashboard_list_item/DashboardListItem.css` con el siguiente CSS[^1]:

```css
.dashboard-li {
  margin: RANDOMNUMBER15-25ENDpx;
  padding: 5px;
  background-color: rgb(RANDOMNUMBER230-240END, 240, 240);
  text-align: center;
  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px 0px;
}

.dashboard-li:hover {
  cursor: pointer;
  background-color: rgb(220, 230, 230);
}
```

A continuación, en la misma carpeta, **crea** `src/components/dashboard_list_item/DashboardListItem.js`.

```jsx
import "./DashboardListItem.css"

const DashboardListItem = (props) => {
  const onClick = () => {
    console.log('clicked');
  }

  return <div onClick={onClick} className='dashboard-li' key={props.dashboard.id}>
    <h3>{props.dashboard.title}</h3>
    <p>{props.dashboard.description}</p>
  </div>
}

export default DashboardListItem;
```

¡Componente visual listo!

Como puedes ver, asumimos que mediante una _prop_ `dashboard` recibimos cierta información (`id`, `title` y `description`).

Probémoslo de la siguiente manera:

En `Examples.js`, **añade** un `<div>` a continuación del último ejercicio, **así**:

```jsx
  <div data-cy='issueISSUENUMBERdiv'>
    <h1>Ejercicio ISSUENUMBER</h1>
    <p>RANDOMEXPRESSIONAhora|Aquí|A continuación|DebajoEND se muestra un DashboardListItem de ejemplo</p>
    <DashboardListItem dashboard={{id: 0, title: "RANDOMEXPRESSIONTest|Título|TitleEND", description: "RANDOMEXPRESSIONContenido de prueba|Contenido test|Contenido mock|Contenido de mentirijillaEND"}} />
  </div>
```

¿Ves tu nuevo componente en http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND?

En la siguiente tarea le daremos un uso más _real_.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Hay una variedad de [sombras](https://getcssscan.com/css-box-shadow-examples) que pueden hacer una web mucho más estética con un poco de CSS
