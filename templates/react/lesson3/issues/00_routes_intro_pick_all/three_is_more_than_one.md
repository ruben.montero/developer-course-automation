Tres en uno

## :books: Resumen

* Añadiremos otra página (ruta) a nuestro proyecto

## Descripción

En nuestro proyecto `react-dashboards/react-frontend` ya se sirven dos páginas distintas:

* http://localhost:3000 (componente `<Main/>`)
* http://localhost:3000/about (componente `<About/>`)

¡Vamos a añadir una tercera!

## :package: La tarea

**Añade** una tercera ruta a tu proyecto, con estas características:

* Se mostrará en `/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND` (http://localhost:3000/RANDOMEXPRESSIONexamples|simple_examples|simpleExamplesEND)
* Enlazará a `<Examples/>`
  * **Crea** dicho componente en `src/screens/examples/Examples.js`
  * En `Examples.js`, devuelve (`return`) un `<div>`. Dentro de dicho `<div>`, **incluye** un `<h2 data-cy='pageHeader'>` cuyo contenido **será** `RANDOMEXPRESSIONExamples|Ejemplos|Ejemplillos|Otros ejemplos|Otros|Algunos ejemplosEND`
  
### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
