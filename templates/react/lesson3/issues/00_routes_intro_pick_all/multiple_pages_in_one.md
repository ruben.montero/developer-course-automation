Varias páginas con React

## :books: Resumen

* Actualizaremos nuestro repositorio con `git pull` y comenzaremos a trabajar en la nueva carpeta `react-dashboards/`
* Instalaremos [`react-router-dom`](https://reactrouterdotcom.fly.dev/)
* Implementaremos una versión sencilla de aplicación multipágina, con dos páginas web

## Descripción

Recordemos que [React](https://reactjs.org/) es un _framework_ para la creación de interfaces web basadas en componentes. La filosofía de React se limita a:

1. Servir al cliente un montón de JavaScript y un HTML `<div></div>` vacío
2. El JavaScript se ejecuta después de ser obtenido por el navegador, o en respuesta a interacciones del usuario.
3. Cuando esto sucede, modifica el [DOM](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction#what_is_the_dom) para que se visualice algo _distinto_ a un sencillo `<div></div>`
4. El usuario disfrutará del nuevo contenido y de su rápida velocidad de respuesta

Estamos hablando de una [SPA (single-page application)](https://codigoencasa.com/que-es-un-spa-single-page-aplication/). Una sola URL. Una sóla página.

### _Client-side routing_ con [`react-router-dom`](https://reactrouterdotcom.fly.dev/)

Esta librería nos confiere los componentes necesarios para manejar navegación dentro la página web. Por debajo, usa [la librería history](https://github.com/ReactTraining/history) y [React context](https://ui.dev/react-context) para implementar _navegación_ sin renunciar a los beneficios de las SPAs.

Ya lo hemos puesto en práctica. ¡Pongámoslo de nuevo!

## :package: La tarea

En primer lugar, **efectúa** `git pull` de tu repositorio para asegurarte que estás al día.

Después, desde un terminal (cmd.exe), **posiciónate** (`cd`) en la carpeta `react-dashboards/react-frontend/`[^1] y **ejecuta**:

```
npm install
```

Cuando termine, para instalar la librería `react-router-dom`, **ejecuta**:

```
npm install react-router-dom
```

A continuación, **crea** una carpeta `screens/` dentro de `src/` donde albergaremos el código de cada página. Dentro, **crea** dos carpetas separadas (`main/` y `about/`).

**Crea** dos nuevos ficheros en cada carpeta, **así**:

* `src/screens/main/Main.js`

```jsx
const Main = () => {
    return <h2 data-cy='pageHeader'>
      RANDOMEXPRESSIONMain|Principal|Página principalEND
    </h2>
}

export default Main;
```

* `src/screens/about/About.js`

```jsx
const About = () => {
    return <h2 data-cy='pageHeader'>
      RANDOMEXPRESSIONAbout|Acerca de|Otra informaciónEND
    </h2>
}

export default About;
```

Ahora, **elimina** el contenido por defecto en `App.js`:

```diff
function App() {
  return (
-    <div className="App">
-      <header className="App-header">
-        <img src={logo} className="App-logo" alt="logo" />
-        <p>
-          Edit <code>src/App.js</code> and save to reload.
-        </p>
-        <a
-          className="App-link"
-          href="https://reactjs.org"
-          target="_blank"
-          rel="noopener noreferrer"
-        >
-          Learn React
-        </a>
-      </header>
-    </div>
  );
}
```

...y, en su lugar, **añade** un `<BrowserRouter>` y un `<Routes>`:

```jsx
import { BrowserRouter, Routes } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Routes>
      
      </Routes>
    </BrowserRouter>
  );
}
```

### La parte fundamental: Definir las rutas

Para terminar, dentro del `<Routes> </Routes>`, **añade** estas dos rutas:

```jsx
      <Route path="/" element={<Main/>}></Route>
      <Route path="/about" element={<About/>}></Route>
```

(Y, recuerda, los `imports` necesarios):

```jsx
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Main from './screens/main/Main';
import About from './screens/about/About';
```

Prueba a lanzar el servidor con `npm run start`

¿Ves correctamente las siguientes páginas?

* http://localhost:3000/
* http://localhost:3000/about

¡Enhorabuena! Has arrancado el proyecto con éxito.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_ con un buen mensaje descriptivo.

No está de más verificar que tu _commit_ se ha subido correctamente desde GitLab (https://raspi).

------------------------------------------------

[^1]: ¡Ojo! En este _sprint_ tendremos dos carpetas principales: `react-dashboards/react-frontend/` contiene el proyecto React y trabajaremos sobre ella. `react-dashboards/django-backend/` contiene código que es parte del proyecto, pero del _backend_. No trabajaremos aquí, pero puedes echar un vistazo si te da curiosidad
