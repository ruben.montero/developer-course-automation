Petición POST de inicio de sesión

## :books: Resumen

* Cuando el usuario pulse _RANDOMEXPRESSIONIniciar sesión|Inicio de sesión|Login|LoguearseEND_ enviaremos una petición HTTP al API REST para que genere una nueva sesión
* La petición contendrá un cuerpo JSON
* Si la respuesta es exitosa, mostraremos un `Toast` con un dato de la respuesta llamado _Token de sesión_

## Descripción

Anteriormente hemos creado una petición `POST` para _registrar un nuevo usuario_:

```mermaid
graph LR
    A[App Android] -->|"Petición POST /users<br><br>{ 'username': 'pepe.depura',<br>'password':'Pass1234' }"| B[Servidor HTTP]
    B -->|"201 Created"| A
```

Ahora, implementaremos una petición `POST` que, empleando las credenciales de la misma forma, _genera_ una nueva sesión en el servidor y tras ello _recibe_ un **_Token de sesión_**:

```mermaid
graph LR
    A[App Android] -->|"Petición POST /sessions<br><br>{ 'username': 'pepe.depura',<br>'password':'Pass1234' }"| B[Servidor HTTP]
    B -->|"201 Created<br>{ 'sessionToken': 'a365b2cd5f' }"| A
```

Profundizaremos más adelante en _cómo se usa_ dicho _Token_. De momento, únicamente lo mostraremos en un `Toast`.

## :package: La tarea

En `LoginActivity.java`, **añade** un atributo tipo `RequestQueue`. **Inicialízalo** en `onCreate`:

```java
public class LoginActivity extends AppCompatActivity {
    /* ... */
    private RequestQueue RANDOMEXPRESSIONrequestQueue|queue|queueForRequestsEND;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /* ... */

        RANDOMEXPRESSIONrequestQueue|queue|queueForRequestsEND = Volley.newRequestQueue(this);
    }
}
```

Luego, **crea** un nuevo método privado `RANDOMEXPRESSIONloginUser|startUserSession|sendLoginRequest|sendPostLogin|sendPostRequestEND`. De momento estará vacío, pero servirá para _encapsular_ la lógica relacionada a enviar el `POST` de _login_:

```java
public class LoginActivity extends AppCompatActivity {
    /* ... */

    private void RANDOMEXPRESSIONloginUser|startUserSession|sendLoginRequest|sendPostLogin|sendPostRequestEND() {
    
    }
}
```

A continuación, **añade** un atributo tipo `Button` e **inicialízalo** conectándolo al botón de `"RANDOMEXPRESSIONIniciar sesión|Inicio de sesión|Login|LoguearseEND"`. Algo parecido a lo que hiciste en la tarea anterior con el otro botón.

Después, para este botón, **establece** un `OnClickListener`. Dentro de `onClick`, **invoca** el método `RANDOMEXPRESSIONloginUser|startUserSession|sendLoginRequest|sendPostLogin|sendPostRequestEND` recién creado.

Así, cuando el usuario pulse `"RANDOMEXPRESSIONIniciar sesión|Inicio de sesión|Login|LoguearseEND"`, ¡se ejecutará el método `RANDOMEXPRESSIONloginUser|startUserSession|sendLoginRequest|sendPostLogin|sendPostRequestEND`!

Trabajemos ahora en `RANDOMEXPRESSIONloginUser|startUserSession|sendLoginRequest|sendPostLogin|sendPostRequestEND`.

**Instancia** un objeto `JSONObject` y **almacena** el _nombre de usuario_ y _contraseña_ que el usuario ha introducido en los campos de texto. Lo haremos bajo las claves `"username"` y `"password"`, **igual** que en la pantalla de registro:

```java
    private void RANDOMEXPRESSIONloginUser|startUserSession|sendLoginRequest|sendPostLogin|sendPostRequestEND() {
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("username", /* .getText().toString() sobre tu instancia de editText para el nombre de usuario */ );
            requestBody.put("password", /* .getText().toString() sobre tu instancia de editText para la contraseña*/ );
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        
    }
```

### ¡Enviemos la petición HTTP!

A continuación del código anterior, **instancia** un `JsonObjectRequest`. Los parámetros del constructor serán:

* (1) `Request.Method.POST`.
* (2) `Server.name + "/sessions"`.
* (3) `requestBody`, creado en las líneas anteriores.
* (4) Un `Response.Listener` _anónimo_.
* (5) Un `Response.ErrorListener` _anónimo_.

Quedará **así**:

```java
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                Server.name + "/sessions",
                requestBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        
                    }
                }
        );
```

¡Ya tenemos prácticamente diseñada nuestra petición HTTP! Ahora, para completar la tarea:

1. **Añade** la petición a `this.RANDOMEXPRESSIONrequestQueue|queue|queueForRequestsEND` para que la petición se envíe
2. En `onResponse`, **muestra** un `Toast` que indique `"RANDOMEXPRESSIONToken:|Token de sesión:|Token|Token de sesiónEND XXX"` donde `XXX` es el _Token de sesión_ recibido en la respuesta. **Así**:

```java
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String receivedToken;
                        try {
                            receivedToken = response.getString("sessionToken");
                        } catch (JSONException e) {
                            // Si el JSON de la respuesta NO contiene "sessionToken", vamos a lanzar
                            // una RuntimeException para que la aplicación rompa.
                            // En preferible que sea NOTORIO el problema del servidor, pues desde
                            // la aplicación no podemos hacer nada. Estamos 'vendidos'.
                            throw new RuntimeException(e);
                        }
                        // Si la respuesta está OK, mostramos un Toast
                        // Esta línea asume que private Context context = this; está definido
                        Toast.makeText(context, "RANDOMEXPRESSIONToken:|Token de sesión:|Token|Token de sesiónEND " + receivedToken, Toast.LENGTH_SHORT).show();
                    }
                },
```

Cuando termines estos pasos, puedes lanzar la _app_ y verificar que el `Toast` aparece tras hacer _Login_. 

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
