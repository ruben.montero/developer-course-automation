Pantalla de inicio de sesión

## :books: Resumen

* Crearemos una nueva actividad `LoginActivity.java`
* La escogeremos como `LAUNCHER` y `MAIN` en el manifiesto, para que sea la actividad inicial
* Añadiremos _dos_ `<EditText>` y _dos_ `<Button>`
* Conectaremos `LoginActivity` con `RegisterActivity`

## Descripción

Ya tenemos una pantalla de registro. pero, ¿acaso es lo que ves en las _apps_ que usas habitualmente?

Normalmente la pantalla inicial te permite hacer _Login_, ¿verdad? Tiene lógica. Si un usuario se descarga tu _app_ es posible que ya tenga una cuenta creada en tu plataforma. Por otro lado, el proceso de registro sólo se lleva a cabo una vez.

¡Vamos a empezar a trabajar en una pantalla de _Login_! De momento, sólo en la parte visual.

## :package: La tarea

**Añade** un nuevo archivo de _layout_ `activity_login.xml`. Tendrá un `<ConstraintLayout>` como elemento raíz, y dentro:

* **Añade** un `<EditText>` (será para la _contraseña_). Debe tener:
  * Ancho proporcional al 60% del contenedor.
  * Alto ajustado al contenido (`wrap_content`).
  * Posición centrada, vertical y horizontalmente.
  * `android:inputType="textPassword"`.
  * `android:hint="Contraseña"`.
  * Un `android:id` escogido por ti.
  
* **Añade** otro `<EditText>` (será para el _nombre de usuario_). Debe tener:
  * Ancho proporcional al 60% del contenedor.
  * Alto ajustado al contenido (`wrap_content`).
  * Posicionado _sobre_ (encima) el `<EditText>` anterior.
  * `android:hint="RANDOMEXPRESSIONbob.doe|alice.doe|john.doe|jane.doe|william.doe|elliot.doe|eric.doe|marylin.doeEND"`.
  * Un `id` escogido por ti.

* **Añade** un `<Button>`, que servirá para _iniciar sesión_. Debe tener:
  * _Constraints_ que lo peguen abajo, a la izquierda y a la derecha del contenedor.
  * Ancho ajustado a las _constraints_ (`0dp`).
  * Alto ajustado al contenido (`wrap_content`).
  * Indicará el texto `"RANDOMEXPRESSIONIniciar sesión|Inicio de sesión|Login|LoguearseEND"`.
  * Un `id` escogido por ti.
  
* **Añade** un `<Button>` que servirá para _ir a la pantalla de registro_. Debe tener:
  * _Constraints_ que lo peguen, a la izquierda y a la derecha del contenedor. También, estará posicionado _encima_ del `<Button>` anterior.
  * Ancho ajustado a las _constraints_ (`0dp`).
  * Alto ajustado al contenido (`wrap_content`).
  * Indicará el texto `"RANDOMEXPRESSION¿Aún no tienes cuenta?|¿No tienes cuenta?|¿Quieres una cuenta?END"`,

Con esto, habrás terminado el trabajo en el archivo de interfaz `activity_login.xml`.

Luego, **crea** una nueva clase Java `LoginActivity.java`. **Escribe** la cláusula para que _herede_ de `AppCompatActivity`, e **implementa** el método `onCreate` así:

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }
```

Después, **edita** el archivo `AndroidManifest.xml`. **Añade** una nueva declaración para la actividad recién creada, indicando los `<intent-filter>` `LAUNCHER` y `MAIN`. **Así**:

```xml
        <activity
            android:name=".LoginActivity"
            android:exported="true"> <!-- exported debe indicarse como "true" para actividades LAUNCHER desde Android 12 -->
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />
                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
```

* `"android.intent.action.MAIN"` significa que esta actividad es válida como _top-level_ para acceder al resto de actividades y no recibe ningún 'Extra'
* `"android.intent.category.LAUNCHER"` significa que es la actividad usada como punto de entrada cuando el usuario abre la aplicación

También, **elimina** dichas declaraciones de `RegisterActivity`:

```diff
        <activity
            android:name=".RegisterActivity"
            android:exported="true">
-            <intent-filter>
-                <action android:name="android.intent.action.MAIN" />
-
-                <category android:name="android.intent.category.LAUNCHER" />
-            </intent-filter>
        </activity>
```

### Conectando la pantalla de _Login_ y la de _Registro_

Ahora que ya está creada la actividad de _Login_ puedes lanzar la _app_ y probarla.

Como últimos pasos de esta tarea, vamos a conectar mejor las pantallas que ya tenemos.

Primero, en `LoginActivity.java`:

* **Añade** un atributo privado de tipo `Button` e **inicialízalo** en `onCreate`, conectado al botón de `"RANDOMEXPRESSION¿Aún no tienes cuenta?|¿No tienes cuenta?|¿Quieres una cuenta?END"` (debes recordar _qué_ `id` le diste).
* Después, en `onCreate`, **establece** un `OnClickListener` para dicho botón.
* Dentro del `onClick`, **lanza** la **`RegisterActivity`** usando un `Intent` y `startActivity`, como ya hiciste anteriormente.

¡Si pruebas la _app_ deberás poder ir a la pantalla de _Registro_ pulsando el botón!

Por último, **abre** `RegisterActivity.java`. Ahí, **invoca** [`finish();`](https://developer.android.com/reference/android/app/Activity#finish()) **cuando la petición de _registro_ se completa satisfactoriamente**. No elimines el `Toast` que implementamos anteriormente. Invoca `finish()` en la línea _posterior_ al `Toast`.

La idea es que, tras registrarse con éxito, el usuario regrese _automáticamente_ a la pantalla de _Login_ para iniciar sesión.

Puedes lanzar la _app_ y registrarte para verificarlo. ¿Regresas automáticamente a la pantalla de _Login_?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
