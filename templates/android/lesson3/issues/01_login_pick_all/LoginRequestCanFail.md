¿Y si el inicio de sesión falla?

## :books: Resumen

* Mostraremos un `Toast` si la petición de inicio de sesión falla

## Descripción

¿Qué pasa si introducimos mal nuestra contraseña? El servidor devolverá un `401 Unauthorized`.

¿Y si introducimos mal el nombre de usuario? El servidor devolverá un `404 Not Found`.

Como ves, hay escenarios habituales donde el servidor puede dar un código de error.

## :package: La tarea

En `LoginActivity.java`, dentro de `Response.ErrorListener`, **muestra** un `Toast` al con el mensaje `"RANDOMEXPRESSIONEstado de respuesta:|Estado de respuesta|El servidor respondió con|El servidor respondió con:|Código de respuesta|Código de respuesta:END XXX"`, siendo `XXX` el código de error HTTP respondido por el servidor. **Especifica** `Toast.LENGTH_LONG`.

También sería aconsejable contemplar el escenario donde se produce un error de conexión, ¿verdad?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
