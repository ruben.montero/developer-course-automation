Petición GET y SharedPreferences

## :books: Resumen

* Lanzaremos una petición GET en `StatusActivity.java` para obtener el 'Estado' del usuario
* Usaremos las `SharedPreferences` para _persistir_ el nombre del usuario que inició sesión y usarlo en la petición
* Añadiremos una `LauncherActivity` para que se inicie `LoginActivity` ó `StatusActivity`, dependiendo de si el usuario ya se ha _logueado_ con anterioridad
* No nos desanimaremos a pesar de que, al final de la tarea, todavía no funcionará nuestra petición de 'Estado'

## Descripción

Hemos mencionado que el propósito de esta aplicación es que el usuario _almacene_ un 'Estado'.

* Sólo podrá tener _un_ 'Estado'.
* Podrá grabar _un nuevo_ 'Estado', pero al hacerlo, reeemplazará el anterior.

Para ello, se lanzan peticiones HTTP a estos dos _endpoints_:

* `GET http://raspi:8000/users/<nombreUsuario>/status` (recuperar 'Estado' actual)
* `PUT http://raspi:8000/users/<nombreUsuario>/status` (registrar nuevo 'Estado')

Por ejemplo, si queremos _recuperar_ el estado de `john.doe`, mandaremos un `GET` a `/users/john.doe/status`

En esta tarea trabajaremos en ello, y de paso nos familizarizaremos con un nuevo recurso del API de Android: Las [`SharedPreferences`](https://developer.android.com/reference/android/content/SharedPreferences).

### [API de Android](https://developer.android.com/reference/packages) y [`SharedPreferences`](https://developer.android.com/reference/android/content/SharedPreferences)

¡El API de Android lo hemos estado usando todo el rato! Se trata del conjunto de clases y métodos que están a nuestra disposición y sirven de intermediarios entre nuestra _app_ y los recursos del teléfono, como _cámara_, _micrófono_, _Wi-Fi_,...

<img src="android-sessions/android-frontend-sessions/docs/android_api_explained.png" width=450 />

Con [`SharedPreferences`](https://developer.android.com/reference/android/content/SharedPreferences) puedes acceder al _almacenamiento secundario_. Es decir, la tarjeta SD ó memoria interna del teléfono donde los datos _no_ se eliminan de manera _volátil_.

¡Vamos a trabajar en ello!

## :package: La tarea

En `StatusActivity.java`, **crea** un atributo `RequestQueue` e **inicialízalo**. También, **añade** un método `RANDOMEXPRESSIONgetStatus|retrieveStatus|obtainStatus|getUserStatus|retrieveUserStatus|obtainUserStatusEND` que será invocado al final de `onCreate`. **Así**:

```java
public class StatusActivity extends AppCompatActivity {
    private Context context = this;
    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        // Inicializamos la cola de peticiones y preparamos la petición inicial
        queue = Volley.newRequestQueue(this);
        RANDOMEXPRESSIONgetStatus|retrieveStatus|obtainStatus|getUserStatus|retrieveUserStatus|obtainUserStatusEND();
    }
    
    private void RANDOMEXPRESSIONgetStatus|retrieveStatus|obtainStatus|getUserStatus|retrieveUserStatus|obtainUserStatusEND() {
        // Mandaremos la petición GET
        
    }
}
```

Ahora, _podríamos_ ponernos a trabajar en `RANDOMEXPRESSIONgetStatus|retrieveStatus|obtainStatus|getUserStatus|retrieveUserStatus|obtainUserStatusEND`, donde enviamos la petición `GET` mencionada arriba:

```java
    private void RANDOMEXPRESSIONgetStatus|retrieveStatus|obtainStatus|getUserStatus|retrieveUserStatus|obtainUserStatusEND() {
        // Mandaremos la petición GET
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                Server.name + "/users/" + "???" + "/status" // ?????? qué pongo aquí (?)
                
        )
```

¡Ups! Parece que hay un problema.

### La URL debe contener el _username_

Sí, así es.

### Hay que sacarlo de algún sitio

Exacto.

### ¡El usuario introdujo su _username_ en `LoginActivity.java`!

¡Correcto!

### Guardando el _username_ tras hacer _Login_ en `LoginActivity.java`

Vamos a usar las [`SharedPreferences`](https://developer.android.com/reference/android/content/SharedPreferences) para persistir el _username_.

Primero, en `LoginActivity.java`, después de un _Login_ exitoso (`onResponse`), **almacena** el nombre de usuario que se _logueó_. **Empieza** _instanciando_ unas `SharedPreferences` **así**:

```java
   new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            String receivedToken;
            // Aquí tendremos el código asociado a:
            // (1) Mostrar un Toast con el Token de sesión
            // (2) Iniciar la StatusActivity
            /* ... */
            
            // Instanciamos un objeto de tipo SharedPreferences
            // En el constructor pasamos un String. SIEMPRE será el mismo para nuestra aplicación.
            SharedPreferences preferences = context.getSharedPreferences("SESSIONS_APP_PREFS", MODE_PRIVATE);
```

El objeto `SharedPreferences` es suficiente para _recuperar_ valores, pero para _almacenarlos_ necesitamos usar una clase distinta. Lleva el sufijo `.Editor`.

**Añade** la siguiente línea después de la indicada arriba:

```java
            SharedPreferences.Editor editor = preferences.edit();
```

Después de esto, basta con invocar `.putXXX` para guardar un dato concreto.

**Invoca** `putString` y **usa** la clave `"VALID_USERNAME"`. Como valor, **guarda** el contenido del `EditText` donde el usuario tecleó su _username_:

```java
            editor.putString("VALID_USERNAME", /* .getText().toString() sobre tu instancia de editText para el nombre de usuario */ );
```

Y finalmente, **invoca** `.commit()` (e **invoca** `finish()`, como se indica):

```java
            // .apply() es preferible y cuando se usa, el sistema almacena
            // los datos en segundo plano de forma más lenta
            
            // .commit() es bloqueante y los almacena inmediatamente. Debe
            // usarse sólo si es crítico que los datos se guarden de forma
            // instantánea. ¡En nuestro caso es así!
            editor.commit();
            
            // Un detalle extra:
            // Añadamos 'finish()' para que, tras un Login exitoso, la
            // LoginActivity se termine. Así evitamos que al pulsar Atrás
            // el usuario regrese a dicha LoginActivity.
            // ¡Sería confuso!
            finish();
```

### Volviendo a `StatusActivity.java`

¡Ahora estamos listos para mandar la petición a la URL correcta!

En `StatusActivity.java`, **instancia** un `JsonObjectRequest` con los parámetros correctos en `RANDOMEXPRESSIONgetStatus|retrieveStatus|obtainStatus|getUserStatus|retrieveUserStatus|obtainUserStatusEND` y **añádelo** a la cola de peticiones. **Muestra** un `Toast` de éxito y uno de error como se expone a continuación:

```java
   private void RANDOMEXPRESSIONgetStatus|retrieveStatus|obtainStatus|getUserStatus|retrieveUserStatus|obtainUserStatusEND() {
        // Recuperamos el nombre de usuario de las preferencias
        SharedPreferences preferences = getSharedPreferences("SESSIONS_APP_PREFS", MODE_PRIVATE);
        String username = preferences.getString("VALID_USERNAME", null); // null será el valor por defecto
        // Mandaremos la petición GET
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                Server.name + "/users/" + username + "/status",
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(context, "RANDOMEXPRESSIONEstado obtenido|Estado recibido|Estado OKEND", Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "RANDOMEXPRESSIONProblema con la petición de estado|Problema recibiendo el estado|Estado KOEND", Toast.LENGTH_LONG).show();
                    }
                }
        );
        queue.add(request);
    }
```

Antes de verificar que funciona, dejémonos llevar por la emoción y trabajemos en una `LauncherActivity`.

### `LauncherActivity`

**Declara** en `AndroidManifest.xml` una `LauncherActivity`, e **indica** que es la nueva `LAUNCHER` y `MAIN`:

```diff
+       <activity android:name=".LauncherActivity"
+           android:exported="true"
+           android:noHistory="true"> <!-- con noHistory evitamos que, al pulsar Atrás, el usuario regrese a esta actividad -->
+           <intent-filter>
+               <action android:name="android.intent.action.MAIN" />
+
+               <category android:name="android.intent.category.LAUNCHER" />
+           </intent-filter>
+       </activity>
        <activity
            android:name=".LoginActivity"
            android:exported="true"> <!-- exported debe indicarse como "true" para actividades LAUNCHER desde Android 12 -->
-           <intent-filter>
-               <action android:name="android.intent.action.MAIN" />
-
-               <category android:name="android.intent.category.LAUNCHER" />
-           </intent-filter>
        </activity>
```

Luego, **crea** la clase `LauncherActivity.java` y **provéela** del siguiente código:

```java
public class LauncherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Recuperamos el nombre de usuario de las preferencias
        SharedPreferences preferences = getSharedPreferences("SESSIONS_APP_PREFS", MODE_PRIVATE);
        String username = preferences.getString("VALID_USERNAME", null); // null será el valor por defecto
        
        // Si el usuario NO se ha logueado, el valor es 'null' por defecto
        // ¡Vamos a iniciar la pantalla de Login!
        if (username == null) {
            Intent loginActivity = new Intent(this, LoginActivity.class);
            startActivity(loginActivity);
        
        // Si el usuario SÍ se ha logueado, ya disponemos de su nombre de usuario
        // ¡Vamos a iniciar la pantalla principal!
        } else {
            Intent statusActivity = new Intent(this, StatusActivity.class);
            startActivity(statusActivity);
        }
    }
}
```

Presta atención a este código y compréndelo. ¡Sirve para que la _app_ vaya a la pantalla correcta tras iniciarse!

Si lanzas tu _app_, verás que aparece la `StatusActivity` en caso de que ya te hayas _logueado_.

Puedes _borrar_ las `SharedPreferences` yendo, en el dispositivo, a _Settings_ > _Apps & Notifications_ > _Sessions_ > _Storage_ > **_Clear Storage_**. ¡Es como cerrar la sesión!

Tras esto, puedes _abrir_ de nuevo la aplicación y verás la `LoginActivity`.

### ¡Probémoslo!

Prueba a lanzar la _app_ y hacer _Login_. Verás que aparece el Toast `"RANDOMEXPRESSIONProblema con la petición de estado|Problema recibiendo el estado|Estado KOEND"`

Parece que la petición `GET` para obtener el 'Estado', ¡falla!

Lo arreglaremos en la siguiente tarea.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
