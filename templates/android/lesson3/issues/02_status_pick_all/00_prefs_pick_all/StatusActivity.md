Actividad principal

## :books: Resumen

* Crearemos una `StatusActivity`
* La iniciaremos después de un _Login_ exitoso

## Descripción

Nuestra aplicación va a ser muy sencilla. Una vez se _loguea_ el usuario, verá su 'Estado'. Algo así como el 'Estado' en WhatsApp. Se trata ni más ni menos que de un sencillo _String_ almacenado en el servidor. Ni siquiera se podrá ver el estado de otros usuarios.

## :package: La tarea

De momento, añadiremos una nueva actividad destinada a mostrar al usuario su 'Estado' personal.

1. **Haz** _click_ derecho en _java_ > _com.afundacion.fp.sessions_ y **selecciona** _New_ > _Activity_ > **_Empty Views Activity_**. (Ojo: No elijas _Empty View_ basada en Kotlin)
2. En el asistente, **escribe** `StatusActivity` como nombre de la actividad. Verás que el nombre del XML se actualiza coherentemente.
3. **Selecciona** `Finish`.

¡Actividad creada! Es mucho más rápido que hacerlo manualmente :smile:

### Añadiendo un `<TextView>`

Dentro del `<ConstraintLayout>` que se ha generado en `activity_status.xml`, **añade** un `<TextView>`:

* Con texto `"RANDOMEXPRESSIONCargando...|CargandoEND"`.
* Con tamaño de texto `RANDOMNUMBER24-32ENDsp`.
* Centrado _horizontalmente_ en el contenedor.
* Posicionado _verticalmente_ a la altura que quieras, libremente.

### ¿Y cuándo aparece esta actividad?

Como último paso, en `LoginActivity.java`, **lanza** la `StatusActivity` _después_ de un _Login_ exitoso.

**Hazlo** en la línea _posterior_ al `Toast` de la tarea anterior.

¡Debes ser capaz de lanzar la _app_ y llegar a tu actividad con el texto `"RANDOMEXPRESSIONCargando...|CargandoEND"` después de hacer _login_!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
