Autenticación

## :books: Resumen

* Persistiremos en las `SharedPreferences` el _Token de sesión_ que recibimos al hacer un _Login_ exitoso
* Lo mandaremos como _cabecera_ HTTP en la petición `GET /users/<username>/status`

## Descripción

En la tarea anterior hemos implementado una petición `GET` a:

* `http://raspi:8000/users/<username>/status`

¡Pero la petición falla! Vemos `"RANDOMEXPRESSIONProblema con la petición de estado|Problema recibiendo el estado|Estado KOEND"` cuando iniciamos la pantalla principal. La verdad, es que el servidor REST nos está enviando una respuesta que indica esto:

```json
{
  "error": "Missing token header"
}
```

Las [APIs REST (REpresentational State Transfer)](https://desarrolloweb.com/articulos/que-es-rest-caracteristicas-sistemas.html) no almacenan estado del cliente. _Toda_ la información relevante viaja en la petición. Es decir, _no_ podemos esperar que el servidor REST [_nos recuerde_](https://youtu.be/Ua3w9CXfwcQ)

![](android-sessions/android-frontend-sessions/docs/stateless_login_problems.png)

### Concepto de sesión: Autenticarse con un token

Cuando nos _logueamos_ obtenemos un _Token de sesión_. Nuestra _app_ debe _recordarlo_ y _usarlo_ en futuras peticiones.

![](android-sessions/android-frontend-sessions/docs/stateless_login_explained.png)

### En nuestro caso

¿Recuerdas _dónde_ recibimos el _Token de sesión_ del servidor?

¡Es en `LoginActivity.java`, dentro del `onResponse` que gestiona la respuesta a la petición `POST` de _Login_!

```java
        @Override
        public void onResponse(JSONObject response) {
            String receivedToken;
            try {
                receivedToken = response.getString("sessionToken");
            } catch (JSONException e) {
                // Si el JSON de la respuesta NO contiene "sessionToken", vamos a lanzar
                // una RuntimeException para que la aplicación rompa.
                // En preferible que sea NOTORIO el problema del servidor, pues desde
                // la aplicación no podemos hacer nada. Estamos 'vendidos'.
                throw new RuntimeException(e);
            }
            // Si la respuesta está OK, mostramos un Toast
            // Esta línea asume que private Context context = this; está definido
            Toast.makeText(context, "RANDOMEXPRESSIONToken:|Token de sesión:|Token|Token de sesiónEND " + receivedToken, Toast.LENGTH_SHORT).show();
            
            /* ... */
```

En ese código, más abajo, _ya_ tienes código que persiste el _nombre de usuario_ (¡pero no el _Token_!)

```java
            SharedPreferences preferences = context.getSharedPreferences("SESSIONS_APP_PREFS", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("VALID_USERNAME", username.getText().toString());
            editor.commit();
```

## :package: La tarea

Para empezar, dentro del `onResponse` de _Login_ en `LoginActivity.java`, **persiste** el _Token de sesión_ (además del _nombre de usuario_). **Así**:

```diff
            SharedPreferences preferences = context.getSharedPreferences("SESSIONS_APP_PREFS", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("VALID_USERNAME", username.getText().toString());
+           editor.putString("VALID_TOKEN", receivedToken);
            editor.commit();
```

Ahora, vamos a conseguir que ese _dato_ viaje en la petición `GET /users/<username>/status` que tiene lugar en `StatusActivity.java`, mediante una cabecera HTTP.

### Enviando una cabecera HTTP con Volley

Es un poco más rebuscado de lo que debería. No hay soporte _out-of-the-box_ para hacerlo, así que debemos _crear una subclase_ de `JsonObjectRequest` (ó `JsonArrayRequest`):

Primero, **añade** una nueva clase Java. **Dale** el nombre `RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND.java`.

Luego, **declara** que tu clase hereda (`extends`) de `JsonObjectRequest`. Al hacerlo, saldrá un error porque no hay constructor _por defecto_. Este es un problema similar al que afrontábamos con nuestros `ViewHolder`. Para solventarlo, **haz** _click_ en la bombilla roja de Android Studio, **selecciona** _Create constructor matching 'super'_, y **elige** la última opción (que corresponde al constructor más completo):

```java
public class RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND extends JsonObjectRequest {
    public RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND(int method, String url, @Nullable JSONObject jsonRequest, Response.Listener<JSONObject> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }
}
```

A continuación, **añade** un atributo privado tipo `Context` a la clase. También, **decláralo** en la firma del constructor que se acaba de autogenerar. Luego, **almacena** su valor en el atributo, como es habitual en un método constructor. **Así**:

```diff
public class RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND extends JsonObjectRequest {
+   private Context context;

    public RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND(int method,
                                               String url,
                                               @Nullable JSONObject jsonRequest,
                                               Response.Listener<JSONObject> listener,
                                               @Nullable Response.ErrorListener errorListener,
+                                              Context context) {
        super(method, url, jsonRequest, listener, errorListener);
+       this.context = context;
    }
}
```

_(Esto lo hacemos para recuperar el Token de sesión de las preferencias desde dentro de `RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND`, en un método que implementaremos a continuación)._

### Sobreescribiendo `getHeaders`

**Comienza** escribiendo un nuevo método en la clase `RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND`, tecleando `getHe`. **Dale** a ENTER para aplicar el autocompletado de Android Studio y tener:

```java
public class RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND extends JsonObjectRequest {
    private Context context;

    /* ... */

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return super.getHeaders();
    }
```

Este método que acabamos de sobreescribir (`@Override`), tiene esta documentación:

> Returns a list of extra HTTP headers to go along with this request. (...)

_(O sea que, ¡podemos usarlo para devolver (`return`) cabeceras HTTP!)_

Ahora, **reemplaza** el `return super.getHeaders();` por las líneas necesarias para recuperar el _Token de sesión_ de la persistencia. **Así**[^1]:

```java
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        SharedPreferences preferences = context.getSharedPreferences("SESSIONS_APP_PREFS", Context.MODE_PRIVATE);
        String sessionToken = preferences.getString("VALID_TOKEN", null);
        
    }
```

A continuación, **lanza** `AuthFailureError` si el _Token de sesión_ no está persistido (es `null`). Si revisas la documentación completa de `getHeaders`, ¡veras que es lo adecuado!

```java
        if (sessionToken == null) {
            throw new AuthFailureError();
        }
```

Y para terminar, **añade** estas líneas que hacen el `return` deseado:

```java
        HashMap<String, String> myHeaders = new HashMap<>();
        myHeaders.put("Session-Token", sessionToken);
        return myHeaders;
```

_(Recordemos que, según [el estándar](https://datatracker.ietf.org/doc/html/rfc7230#section-3.2), las cabeceras HTTP viajan en formato clave-valor)._

```
GET www.google.es
Accept-Language: es-ES
User-Agent: Mozilla/5.0 
```

_(Por ello, en `getHeaders` se devuelve un `Map<String, String>`, que es la versión Java de un diccionario clave-valor, ambos `String`. ¡Así conseguimos que nuestra cabecera se envíe!)_

```
Session-Token: 234a5e6cb9
```

_(`Session-Token` no es una cabecera estándar. La usamos así por diseño del API REST)._

Con esto, método **completado**.

### Lanzando la petición `GET status`, ahora con cabecera HTTP de sesión

En `StatusActivity.java`, **sustituye** la `JsonObjectRequest` por `RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND`. Como es una subclase, puedes hacerlo. Adicionalmente, **envía** un `context` al constructor:

```diff
   private void RANDOMEXPRESSIONgetStatus|retrieveStatus|obtainStatus|getUserStatus|retrieveUserStatus|obtainUserStatusEND() {
        // Recuperamos el nombre de usuario de las preferencias
        SharedPreferences preferences = getSharedPreferences("SESSIONS_APP_PREFS", MODE_PRIVATE);
        String username = preferences.getString("VALID_USERNAME", null); // null será el valor por defecto
        // Mandaremos la petición GET
-       JsonObjectRequest request = new JsonObjectRequest(
+       RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND request = new RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND(
                Request.Method.GET,
                Server.name + "/users/" + username + "/status",
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(context, "RANDOMEXPRESSIONEstado obtenido|Estado recibido|Estado OKEND", Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "RANDOMEXPRESSIONProblema con la petición de estado|Problema recibiendo el estado|Estado KOEND", Toast.LENGTH_LONG).show();
                    }
-               }
+               },
+               context
        );
        queue.add(request);
    }
```

Si verificas la _app_, deberías ver el `Toast` que dice `"RANDOMEXPRESSIONEstado obtenido|Estado recibido|Estado OKEND"`.

Para completar la tarea, **añade** un atributo `TextView` a `StatusActivity.java`:

```java
    private TextView RANDOMEXPRESSIONtextViewStatus|textViewUserStatusEND;
```

**Inicialízalo** con `findViewById` en `onCreate`. Debes referirte al `<TextView>` de `activity_status.xml` que pusimos con el texto `"RANDOMEXPRESSIONCargando...|CargandoEND"`. Si no tiene ID en el XML... ¡debes añadirlo!

Por último, en `onResponse` de `StatusActivity.java`, después del `Toast`, **muestra** en dicho `TextView` el mensaje en el JSON del servidor bajo la clave `"status"`:

```java
RANDOMEXPRESSIONtextViewStatus|textViewUserStatusEND.setText(response.getString("status"));
```

¡No olvides el necesario `try-catch`!

Cuando completes la tarea, **¡enhorabuena!** Has conseguido lanzar una petición con una cabecera HTTP _custom_ en Volley para obtener un JSON así del servidor:

```json
{
  "status": "Hey there! I'm using Sessions"
}
```

...y mostrarlo en la actividad principal de la _app_.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

-------------------------------------------------------

[^1]: Sería lo suyo almacenar estos _Strings_ como constantes `final` en alguna clase, en forma de variables estáticas.
