Actualizar el estado

## :books: Resumen

* Al _clicar_ el botón del diálogo, mandaremos la petición `PUT` que actualiza el estado en el servidor
* Hablaremos sobre cómo refrescar datos en nuestro cliente cuando enviamos nueva información al servidor
* Optaremos por lo más sencillo: Repetir el `GET`

## Descripción

Ya tenemos la UI (Interfaz de usuario) preparada para lanzar la petición `PUT` que habíamos mencionado anteriormente, como la de este ejemplo:

```
PUT /users/pepe.depura/status
Session-Token: a26cbe5ead

{
  "status": "Estado actualizado! Hurra!"
}
```

¡Permitamos al usuario actualizar su estado contra el API REST!

## :package: La tarea

Primero, en `StatusActivity.java`, **crea** un método privado `RANDOMEXPRESSIONsendUpdateStatusRequest|putNewStatus|updateStatusEND` e **invócalo** desde el `onClick` del botón del _diálogo_ (`"RANDOMEXPRESSIONCambiar estado|Actualizar estado|Nuevo estado|Cambiar|Actualizar|Modificar|Modificar estadoEND"`):

```diff
                myBuilder.setPositiveButton("RANDOMEXPRESSIONCambiar estado|Actualizar estado|Nuevo estado|Cambiar|Actualizar|Modificar|Modificar estadoEND", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(context, "RANDOMEXPRESSIONCambiar a:|Modificar a:|Actualizar a:END " + RANDOMEXPRESSIONeditTextNewStatus|editTextUpdateStatus|editTextChangeStatus|editTextModifyStatus|editTextPutStatusEND.getText().toString(), Toast.LENGTH_LONG).show();
+                       RANDOMEXPRESSIONsendUpdateStatusRequest|putNewStatus|updateStatusEND();
                    }
                }); // Esto añade un botón al diálogo
                AlertDialog myDialog = myBuilder.create(); // Esta línea es como 'new AlertDialog'
                myDialog.show();
            }
        });
    }
    
+   private void RANDOMEXPRESSIONsendUpdateStatusRequest|putNewStatus|updateStatusEND() {
+   
+   }
```

_(En este método encapsularemos la lógica necesaria para enviar la petición `PUT`)._

Como el `<username>` está en la URL del `PUT`... ¡Debemos recuperarlo de las _preferencias_! Igual que hacíamos en `RANDOMEXPRESSIONgetStatus|retrieveStatus|obtainStatus|getUserStatus|retrieveUserStatus|obtainUserStatusEND()`[^1]. Por ello, **añade** estas líneas a tu método `RANDOMEXPRESSIONsendUpdateStatusRequest|putNewStatus|updateStatusEND`:

```java
    private void RANDOMEXPRESSIONsendUpdateStatusRequest|putNewStatus|updateStatusEND() {
        // Recuperamos el nombre de usuario de las preferencias
        SharedPreferences preferences = getSharedPreferences("SESSIONS_APP_PREFS", MODE_PRIVATE);
        String username = preferences.getString("VALID_USERNAME", null);
    
    }
```

**Añade** este código para crear un `JSONObject` donde _lo que el usuario introduzca en el diálogo_ viajará bajo la clave `"status"`. Dicho `JSONObject` será el cuerpo de la petición, como en el _Login_ o en el _Registro_:

```java
    private void RANDOMEXPRESSIONsendUpdateStatusRequest|putNewStatus|updateStatusEND() {
        // Recuperamos el nombre de usuario de las preferencias
        SharedPreferences preferences = getSharedPreferences("SESSIONS_APP_PREFS", MODE_PRIVATE);
        String username = preferences.getString("VALID_USERNAME", null); // null será el valor por defecto
        // Creamos el cuerpo de la petición
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("status", RANDOMEXPRESSIONeditTextNewStatus|editTextUpdateStatus|editTextChangeStatus|editTextModifyStatus|editTextPutStatusEND.getText().toString());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        
        // Aquí va la petición
        // ...
    }
```

### Lanzando la petición

A continuación de las líneas anteriores, **añade** la instanciación de esta ``RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND`` y su adición a la cola de peticiones, como se indica:

```java
        // Aquí va la petición
        RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND request = new RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND(
                Request.Method.PUT,
                Server.name + "/users/" + username + "/status",
                requestBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                },
                context
        );
        queue.add(request);
```

¡Casi hemos terminado!

Si pruebas la _app_, podrás _actualizar_ el estado. ¡Pero el nuevo estado _no_ se ve! Este es un problema común y hay muchas formas de afrontarlo.

* Desde el cliente tenemos el _texto_ que fue introducido. _Podríamos_ directamente hacer un `.setText()` y cambiarlo.

Sin embargo, ante datos más complejos no es la opción preferible. Por ejemplo; si estamos subiendo un _Comentario_ a una lista de _Comentarios_... ¿Quién nos garantiza que nadie insertó un _Comentario_ entre el nuestro y el último?

Por ello:

* ¡Es buena idea refrescar los datos _a partir del servidor_!

En nuestro caso, **vuelve** a lanzar el `GET`. Para ello, **invoca** `RANDOMEXPRESSIONgetStatus|retrieveStatus|obtainStatus|getUserStatus|retrieveUserStatus|obtainUserStatusEND()` en el `onResponse` anterior:

```java
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        RANDOMEXPRESSIONgetStatus|retrieveStatus|obtainStatus|getUserStatus|retrieveUserStatus|obtainUserStatusEND();
                    }
                },
```

Además, **invoca** `.setText()` para cambiar el texto a _"RANDOMEXPRESSIONCargando...|CargandoEND"_ mientras la petición no se completa. Así el usuario sabrá que hay trabajo en proceso:

```java
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        RANDOMEXPRESSIONtextViewStatus|textViewUserStatusEND.setText("RANDOMEXPRESSIONCargando...|CargandoEND");
                        RANDOMEXPRESSIONgetStatus|retrieveStatus|obtainStatus|getUserStatus|retrieveUserStatus|obtainUserStatusEND();
                    }
                },
```

¡Listo!

Decide libremente si quieres llevar a cabo alguna acción en caso de que la respuesta del `PUT` sea de error.

### ¡Terminado!

**¡Enhorabuena!** Has creado tu primera aplicación que permite a usuarios _loguearse_ y llevar a cabo _acciones que requieren autenticación_.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Sería bueno encapsular (unificar) esas líneas en un método distinto. Copiar y pegar código suele indicar un antipatrón.

