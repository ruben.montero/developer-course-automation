Un diálogo con cuerpo

## :books: Resumen

* Crearemos un archivo de interfaz `RANDOMEXPRESSIONchange_status_dialog|new_status_dialog|update_status_dialog|modify_status_dialogEND.xml`, que contendrá únicamente un `<EditText>`
* Lo _inflaremos_ mediante un nuevo método privado en `StatusActivity`. De paso, guardaremos una referencia al `EditText`
* Lo mostraremos en el diálogo
* Añadiremos un `OnClickListener` al botón del diálogo

## Descripción

Existen diferentes métodos de utilidad en `AlertDialog.Builder` para mostrar _contenido_ en un `AlertDialog`:

* `setMessage`: Hace que muestre un _texto_.
* `setMultiChoiceItems`: Permite mostrar opciones de selección _múltiple_.
* `setSingleChoiceItems`: Permite mostrar opciones de selección _sencilla_ (_exclusiva_).
* `setTitle`: Permite cambiar el título.
* `setIcon`: Permite cambiar el icono.

Desgraciadamente, ningún método nos ayudará inmediatamente a mostrar un _Campo de texto_ (`EditText`).

Vamos a hacerlo manualmente con:

* `setView`: Permite mostrar un contenido totalmente personalizado.

## :package: La tarea

**Crea** en `res/layout/` un nuevo fichero de interfaz `RANDOMEXPRESSIONchange_status_dialog|new_status_dialog|update_status_dialog|modify_status_dialogEND.xml`:

Dentro, **añade** un `<EditText>` con las siguientes características:

```xml
    <EditText
        android:id="@+id/edit_text_change_status"
        android:hint="RANDOMEXPRESSIONHey! I've updated my status|Hey! I have updated my status|Hey! I've changed my status|Hey! I have changed my status|Hey! I've modified my status|Hey! I have modified my statusEND"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_margin="40dp"/>
```

Ahora, en `StatusActivity.java`, **crea** un nuevo método privado:

```java
    private View RANDOMEXPRESSIONinflateDialogView|inflateDialog|inflateDialogBodyEND() {
    }
```

Como ves, devuelve un tipo `View`.

Este `View` será nuestro archivo de interfaz XML _inflado_ manualmente. **Implementa** el método **así**:

```java
    private View RANDOMEXPRESSIONinflateDialogView|inflateDialog|inflateDialogBodyEND() {
        LayoutInflater inflater = getLayoutInflater();
        View inflatedView = inflater.inflate(R.layout.RANDOMEXPRESSIONchange_status_dialog|new_status_dialog|update_status_dialog|modify_status_dialogEND, null);
        return inflatedView;
    }
```

A continuación, vamos a guardar una referencia al `EditText` de la vista para poder acceder al texto tecleado por el usuario más tarde.

**Añade** un atributo tipo `EditText` a la clase e **inicialízalo** desde el método `RANDOMEXPRESSIONinflateDialogView|inflateDialog|inflateDialogBodyEND` como se muestra a continuación:

```diff
public class StatusActivity extends AppCompatActivity {
    /* ... */
+   private EditText RANDOMEXPRESSIONeditTextNewStatus|editTextUpdateStatus|editTextChangeStatus|editTextModifyStatus|editTextPutStatusEND;

    /* ... */
    
    private View RANDOMEXPRESSIONinflateDialogView|inflateDialog|inflateDialogBodyEND() {
        LayoutInflater inflater = getLayoutInflater();
        View inflatedView = inflater.inflate(R.layout.RANDOMEXPRESSIONchange_status_dialog|new_status_dialog|update_status_dialog|modify_status_dialogEND, null);
+       RANDOMEXPRESSIONeditTextNewStatus|editTextUpdateStatus|editTextChangeStatus|editTextModifyStatus|editTextPutStatusEND = inflatedView.findViewById(R.id.edit_text_change_status);
        return inflatedView;
    }
```

Con `RANDOMEXPRESSIONinflateDialogView|inflateDialog|inflateDialogBodyEND` completado, vuelve a `onClick` e **invoca** `.setView()` para que el _diálogo_ muestre el XML creado al principio de la tarea, **así**:

```diff
            @Override
            public void onClick(View view) {
                AlertDialog.Builder myBuilder = new AlertDialog.Builder(context);
+               myBuilder.setView(RANDOMEXPRESSIONinflateDialogView|inflateDialog|inflateDialogBodyEND());
                myBuilder.setPositiveButton("RANDOMEXPRESSIONCambiar estado|Actualizar estado|Nuevo estado|Cambiar|Actualizar|Modificar|Modificar estadoEND", null); // Esto añade un botón al diálogo
                AlertDialog myDialog = myBuilder.create(); // Esta línea es como 'new AlertDialog'
                myDialog.show();
            }
```

Para termimnar, _aún_ no vamos a mandar la petición `PUT`. Pero, cuando el usuario pulse el botón, **muestra** un `Toast` que nos verifique el texto tecleado, **así**:

```java
                myBuilder.setPositiveButton("RANDOMEXPRESSIONCambiar estado|Actualizar estado|Nuevo estado|Cambiar|Actualizar|Modificar|Modificar estadoEND", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(context, "RANDOMEXPRESSIONCambiar a:|Modificar a:|Actualizar a:END " + RANDOMEXPRESSIONeditTextNewStatus|editTextUpdateStatus|editTextChangeStatus|editTextModifyStatus|editTextPutStatusEND.getText().toString(), Toast.LENGTH_LONG).show();
                    }
                }); // Esto añade un botón al diálogo
```

_(Como ves, para ello necesitamos reemplazar el segundo parámetro `null` de `setPositiveButton` por un `DialogInterface.OnClickListener`)._

¡Prueba la _app_! ¿Qué tal funciona?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
