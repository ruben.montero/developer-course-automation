¿Es un pájaro? ¿Es un avión? ¡Es un botón!

## :books: Resumen

* Crearemos un `<FloatingActionButton>` en nuestra `activity_status.xml`
* Desde el código Java, haremos que muestre un diálogo incompleto al pulsarse

## Descripción

Vamos a añadir la funcionalidad de _Cambiar el estado_. Así, los usuarios podrán almacenar un estado distinto al `"Hey there! I'm using Sessions"` por defecto.

El objetivo final es que los usuarios puedan mandar una petición HTTP `PUT` como la del siguiente ejemplo:

```
PUT /users/pepe.depura/status
Session-Token: a26cbe5ead

{
  "status": "Estado actualizado! Hurra!"
}
```

En esta tarea, de momento, no se mandará dicha petición. Sólo comenzaremos a trabajar en la parte visual.

Como novedad, aprenderemos a mostrar un _diálogo_ al usuario. Existen [varias formas de mostrar _diálogos_](https://developer.android.com/guide/topics/ui/dialogs) al usuario. Nosotros vamos a optar por la clase [`AlertDialog`](https://developer.android.com/reference/android/app/AlertDialog), ya que con poco código la tendremos funcionando.

## :package: La tarea

Primero, **añade** un `<FloatingActionButton>` en `activity_status.xml`:

```xml
    <com.google.android.material.floatingactionbutton.FloatingActionButton
        android:id="@+id/button_open_dialog"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        android:src="@android:drawable/ic_input_add"
        android:layout_margin="15dp"
        />
```

### `StatusActivity.java`

Luego, en `StatusActivity.java`, **añade** un atributo tipo `FloatingActionButton` e **inicialízalo** debidamente en `onCreate`. También, **asígnale** un `OnClickListener`:

```java
public class StatusActivity extends AppCompatActivity {
    /* ... */
    private FloatingActionButton RANDOMEXPRESSIONbuttonChangeStatus|buttonUpdateStatus|buttonNewStatus|buttonPutStatusEND;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /* ... */
        RANDOMEXPRESSIONbuttonChangeStatus|buttonUpdateStatus|buttonNewStatus|buttonPutStatusEND = findViewById(R.id.button_open_dialog);
        RANDOMEXPRESSIONbuttonChangeStatus|buttonUpdateStatus|buttonNewStatus|buttonPutStatusEND.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            
            }
        });
    }
```

Queremos que se muestre un diálogo y permita al usuario introducir un texto. El diálogo se mostrará cuando el usuario haga _click_ en el botón flotante.

Lamentablemente, _no_ podemos hacer `new` directamente:

```java
            @Override
            public void onClick(View view) {
                // Esto NO funciona
                AlertDialog myDialog = new AlertDialog();
            }
```

Así que... **usa** la clase `AlertDialog.Builder` así:

```java
            @Override
            public void onClick(View view) {
                AlertDialog.Builder myBuilder = new AlertDialog.Builder(context);
            }
```

**Completa** el código dentro de `onClick` de esta manera, para definir algunas propiedades del _diálogo_:

```java
            @Override
            public void onClick(View view) {
                AlertDialog.Builder myBuilder = new AlertDialog.Builder(context);
                myBuilder.setPositiveButton("RANDOMEXPRESSIONCambiar estado|Actualizar estado|Nuevo estado|Cambiar|Actualizar|Modificar|Modificar estadoEND", null); // Esto añade un botón al diálogo
                AlertDialog myDialog = myBuilder.create(); // Esta línea es como 'new AlertDialog'
                myDialog.show();
            }
```

_(Como ves, hay que invocar `.show();` sobre la instancia de `AlertDialog` para mostrarlo)._

¡Puedes verificar que aparece un diálogo al pulsar en el botón flotante!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
