¡Trabajo terminado!

## :trophy: ¡Felicidades!

Has realizado las tareas del _sprint_ y has validado tu trabajo con los _tests_. ¡Enhorabuena!

<img src="android-sessions/android-frontend-sessions/docs/sunset.jpg" width=500 />

Has...

* Usado `EditText`
* Aprendido sobre `layout_constraintWidth_percent`
* Empleado `"android.permission.INTERNET"` y `android:usesCleartextTraffic="true"`
* Creado una pantalla de _Login_
* Usado `JSONObject` como objeto para almacenar lo que se envía en el cuerpo JSON de una petición de _Login_ (usuario y contraseña)
* Almacenado en `SharedPreferences` un _token_ de sesión, distinguiendo entre `.commit()` y `.apply()`
* Creado una `LauncherActivity` para iniciar la _app_ en la pantalla de _Login_ o en la pantalla principal, en función de si hay un usuario logueado
* Creado una subclase `RANDOMEXPRESSIONJsonObjectRequestWithAuthentication|JsonObjectRequestAuthenticated|JsonObjectRequestWithAuthHeader|JsonObjectRequestWithCustomAuthEND` para enviar cabeceras HTTP personalizadas (también se puede sobreescribir `getHeaders` empleando `{ }` al final de la instanciación de una `JsonObjectRequest` normal; ¡pruébalo!)
* Usado `FloatingActionButton`
* Creado y mostrado un `AlertDialog`, empleando `AlertDialog.Builder`


 