Comenzando con... ¡Inserción de datos!

## :books: Resumen

* Añadiremos un componente `<EditText>` a la aplicación
* Haremos `git add`, `git commit` y `git push` para subir los cambios al repositorio

## Descripción

Desbloquear la posibilidad de consumir datos de un [API REST](https://rockcontent.com/es/blog/api-rest/) y presentarlos al usuario mediante un `RecyclerView` es un logro notable. Pero no es el pastel completo. Abre Twitter, Facebook, Reddit, WhatsApp, Telegram, Instagram, Pinterest, TikTok,... Además de _consumir_ datos, permiten _subir_ datos.

Nosotros vamos a comenzar implementando una pantalla de _registro_. Así, los usuarios podrán _registrarse_ en nuestra aplicación. Para ello, enviaremos una petición `POST` a un _endpoint_ del API REST. Los datos _enviados por el cliente_ en la petición (nuevo nombre de usuario, nueva contraseña) se guardarán en una base de datos en el _backend_, para su futuro uso.

En esta primera tarea, tan sólo aprenderemos a usar un [`EditText`](https://developer.android.com/reference/android/widget/EditText). Es el componente visual que la plataforma Android nos ofrece para permitir al usuario _introducir_ texto.

## :package: La tarea

Como primer paso, **abre** un terminal (cmd.exe) en tu ordenador y cambia el directorio activo hasta la ubicación de tu repositorio usando `cd`. Luego, **haz**:

```
git pull
```

...para traer los cambios de remoto a local, que contienen el _esqueleto_ del proyecto de este _sprint_.

**Abre** el proyecto `android-sessions/android-frontend-sessions` desde Android Studio _(no abras `android-sessions` del nivel superior)_.

Después, **abre** el fichero de interfaz `activity_register.xml`. Dentro, **elimina** el `"Hello world!"` por defecto:

```diff
 <?xml version="1.0" encoding="utf-8"?>
 <androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".RegisterActivity">
-
-   <TextView
-       android:layout_width="wrap_content"
-       android:layout_height="wrap_content"
-       android:text="Hello World!"
-       app:layout_constraintBottom_toBottomOf="parent"
-       app:layout_constraintEnd_toEndOf="parent"
-       app:layout_constraintStart_toStartOf="parent"
-       app:layout_constraintTop_toTopOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```

Luego, **añade** un `<EditText />` _dentro_ del `<ConstraintLayout>` en `activity_register.xml`. Estará _centrado_ vertical y horizontalmente. **Así**:

```xml
    <EditText
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"/>
```

**Lanza** la aplicación en el emulador. ¿Qué tal se ve?

Seguramente observes que el `<EditText>` aparece _comprimido_. Esto se debe a que `layout_width="wrap_content"` hace que el _ancho_ se ajuste a... ¡Nada! No hay texto. Por lo tanto, se ve prácticamente _colapsado_.

_Podríamos_ usar una medida en `dp` estática, como `50dp`, `80dp`, `120dp`... Pero vamos a aprender [cómo dimensionar un elemento proporcionalmente con respecto a su contenedor](https://stackoverflow.com/questions/49639231/constraintlayout-proportional-width-height-to-self).

Por ello, **modifica** `layout_width` para tener un tamaño fijo (`0dp`) y **añade** el atributo `app:layout_constraintWidth_percent` como se indica:

```diff
    <EditText
        android:layout_width="0dp"
-       android:layout_width="wrap_content"
+       android:layout_width="0dp"
+       app:layout_constraintWidth_percent="0.6"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"/>
```

_(Así conseguimos que `<EditText>` tenga un ancho equivalente al 60% del padre)._

### `hint`

¿Como sabe el usuario _qué_ introducir en el `<EditText>`?

Una poderosa herramienta es el atributo `android:hint`. Esto mostrará un texto que aparecerá dentro del `<EditText>`, en gris, y se _esfumará_ en cuanto el usuario empiece a teclear.

**Añade** el siguiente `hint` a tu código `<EditText>` y verifica cómo se ve.


```xml
        android:hint="RANDOMEXPRESSIONalice.doe|bob.doe|charlie.doe|carole.doe|john.doe|jane.doeEND"
```

### :trophy: Por último

Desde el terminal de Windows (cmd.exe), nos posicionamos en `android-sessions/` y hacemos `git add *`, `git commit` y `git push`.

No está de más visitar la página de GitLab (https://raspi) y verificar que el _commit_ se ha subido.
