¿Qué escribió el usuario?

## :books: Resumen

* Añadiremos un botón para _Registrarse_
* De momento, no registrará al usuario. Sólo mostrará un `Toast` enseñando el texto introducido

## Descripción

Nuestros usuarios pueden teclear un _Nombre de usuario_ y _Contraseña_ en dos campos de texto. Vamos a hacer algo con esos datos. Es tan fácil como invocar `.getText().toString()`.

## :package: La tarea

**Abre** `RegisterActivity.java`. Dentro, **añade** dos `EditText` e **inicialízalos** en `onCreate` como se muestra a continuación:

```java
public class RegisterActivity extends AppCompatActivity {
    // Creamos dos atributos nuevos
    private EditText RANDOMEXPRESSIONeditTextUsername|editTextName|editTextUserEND;
    private EditText editTextPassword;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        
        // Los inicializamos a partir de su contraparte en el XML
        RANDOMEXPRESSIONeditTextUsername|editTextName|editTextUserEND = findViewById( /* Aquí, el ID que elegiste en activity_register.xml */ );
        editTextPassword = findViewById( /* Aquí, el otro ID que elegiste en activity_register.xml */ );
    }
}
```

Como siguiente paso, **vuelve** al archivo de interfaz `activity_register.xml` y **añade** un `<Button>` con las siguientes características:

* Indicará `"RANDOMEXPRESSIONRegistrarse|Registro|Crear cuenta|Crear usuarioEND"`.
* Estará posicionado pegado a abajo, izquierda y derecha del contenedor.
* Tendrá un ID escogido por ti.

Y ahora, ¡usemos ese botón!

**Abre** de nuevo el código Java en `RegisterActivity.java` y **añade** un nuevo atributo privado tipo `Button` a la clase. **Llámalo** `RANDOMEXPRESSIONregisterButton|buttonRegisterEND`.

En `onCreate`, **inicializa** dicho atributo. A continuación, **añade** un `OnClickListener` al botón.

Dentro del `onClick`, **muestra** un `Toast` con el siguiente texto:

```
RANDOMEXPRESSIONUsuario:|Nombre:|Usuario|NombreEND XXX
```

...donde `XXX` es el texto introducido por el usuario en el campo de _Nombre de usuario_.

Ahora, como hemos dicho, basta con invocar:

* `RANDOMEXPRESSIONeditTextUsername|editTextName|editTextUserEND.getText().toString();`

...para obtener en formato `String` el texto introducido por el usuario en _Nombre de usuario_. ¡**Concaténalo** adecuadamente para mostrar en un `Toast` el texto correcto indicado arriba!

¿Si pruebas la _app_ y clicas el botón, ves el texto introducido aparecer en un `Toast`?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
