¿Y si el registro falla?

## :books: Resumen

* Mostraremos un `Toast` si la petición de registro falla

## Descripción

Hay muchos escenarios donde la petición HTTP puede fallar.

Por ejemplo, ¿y si un usuario con ese _nombre_ ya está registrado? En ese caso, el servidor devolverá un código de error `409 Conflict`.

## :package: La tarea

En `RegisterActivity.java`, dentro de `Response.ErrorListener`, **añade** el código necesario para que:

* Si hay un problema estableciendo la conexión, se muestre un `Toast` con el mensaje `"RANDOMEXPRESSIONNo se pudo establecer la conexión|Petición fallida|Sin conexión|Imposible conectar al servidor|La conexión no se ha establecido|No se pudo alcanzar al servidorEND"`. Especifica `Toast.LENGTH_LONG`.
* Si hay un código de error de servidor `XXX`, se muestre en un `Toast` con el mensaje `"RANDOMEXPRESSIONEstado de respuesta:|Estado de respuesta|El servidor respondió con|El servidor respondió con:|Código de respuesta|Código de respuesta:END XXX"`. Especifica `Toast.LENGTH_LONG`.

Recuerda que para diferenciar estos dos escenarios...

```java
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error.networkResponse == null) {
                // No se ha establecido la conexión
            } else {
                // El servidor ha dado una respuesta de error
                
                // La siguiente variable contendrá el código HTTP,
                // por ejemplo RANDOMEXPRESSION404|401|403|405|409END, 500,...
                int serverCode = error.networkResponse.statusCode;
            }
        }
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

