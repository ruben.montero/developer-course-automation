Petición POST de registro

## :books: Resumen

* Cuando el usuario pulse _Registrarse_ enviaremos una petición HTTP al API REST para que grabe los datos del nuevo usuario
* La petición contendrá un cuerpo JSON
* Si la respuesta es exitosa, mostraremos un `Toast`

## Descripción

Hasta ahora nuestras peticiones eran `GET` que recuperaban datos del servidor. Usábamos el _cuerpo_ de la _respuesta_.

```mermaid
graph LR
    A[App Android] -->|"Petición GET /health"| B[Servidor HTTP]
    B -->|"Respuesta {'status': 'Server is healthy!'}"| A
```

Ahora, mandaremos una petición `POST` que _sube_ datos al servidor:

```mermaid
graph LR
    A[App Android] -->|"Petición POST /users<br><br>{ 'username': 'pepe.depura',<br>'password':'Pass1234' }"| B[Servidor HTTP]
    B -->|"Respuesta 201 Created"| A
```

Esto, en la práctica, lo conseguiremos _creando_ un `JSONObject` y pasándolo a una instancia de `JsonObjectRequest` como tercer parámetro, ¡en lugar de el `null` que siempre hemos pasado hasta ahora!

## :package: La tarea

**Sigue** los pasos indicados en `Server.java` para configurar la IP del servidor y que nuestro emulador tenga conectividad.

A continuación, en `RegisterActivity.java` **añade** un atributo tipo `RequestQueue` e **inicialízalo** en `onCreate` como se indica a continuación:

```java
public class RegisterActivity extends AppCompatActivity {
    /* ... */
    private RequestQueue RANDOMEXPRESSIONqueue|requestQueueEND;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /* ... */

        RANDOMEXPRESSIONqueue|requestQueueEND = Volley.newRequestQueue(this);
    }
}
```

Ahora encapsularemos la _lógica_ relacionada a _enviar_ la petición `POST` en un nuevo método privado `RANDOMEXPRESSIONregisterUser|registerNewUser|sendRegisterRequest|sendPostRegister|sendPostRequestEND`. Dicho método se _invocará_ cuando el usuario haga _click_ en el botón. Para ello, **añade** el código como se expone a continuación:

```java
public class RegisterActivity extends AppCompatActivity {
    /* ... */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /* ... */
        
        RANDOMEXPRESSIONregisterButton|buttonRegisterEND.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Aquí estará tu Toast de la tarea anterior
                // No lo elimines
                
                RANDOMEXPRESSIONregisterUser|registerNewUser|sendRegisterRequest|sendPostRegister|sendPostRequestEND();
            }
        });
    }
    
    private void RANDOMEXPRESSIONregisterUser|registerNewUser|sendRegisterRequest|sendPostRegister|sendPostRequestEND() {
    
    }
}
```

Luego, en `RANDOMEXPRESSIONregisterUser|registerNewUser|sendRegisterRequest|sendPostRegister|sendPostRequestEND` **instancia** un objeto `JSONObject` e **invoca** `.put` como se indica a continuación:

```java
    private void RANDOMEXPRESSIONregisterUser|registerNewUser|sendRegisterRequest|sendPostRegister|sendPostRequestEND() {
        JSONObject requestBody = new JSONObject();
        requestBody.put("username", RANDOMEXPRESSIONeditTextUsername|editTextName|editTextUserEND.getText().toString());
        requestBody.put("password", editTextPassword.getText().toString());

    }
```

_(Así, hemos construido un objeto JSON que, predeciblemente, representa un JSON como el siguiente, donde `"pepe.depura"` y `"Pass1234"` serán los valores reales introducidos por el usuario)_

```json
{
  "username": "pepe.depura",
  "password": "Pass1234"
}
```

También, **envuelve** dicho código en un `try-catch`:

```java
    private void RANDOMEXPRESSIONregisterUser|registerNewUser|sendRegisterRequest|sendPostRegister|sendPostRequestEND() {
        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("username", RANDOMEXPRESSIONeditTextUsername|editTextName|editTextUserEND.getText().toString());
            requestBody.put("password", editTextPassword.getText().toString());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        
    }
```

Ahora... ¡vamos a enviar una petición HTTP para _registrar_ al usuario en el servidor!

Como primer paso, **instancia** un objeto `JsonObjectRequest` y como _cinco_ parámetros del constructor, **especifica**:

* (1) `Request.Method.POST`.
* (2) `Server.name + "/users"`.
* (3) `requestBody`. Solíamos usar `null` en otras peticiones... ¡Aquí no!
* (4) `Response.Listener` _anónimo_.
* (5) `Response.ErrorListener` _anónimo_.

Quedará **así**:

```java
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                Server.name + "/users",
                requestBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        
                    }
                }
        );
```

A continuación, en `onResponse` (respuesta de _éxito_), **muestra** un `Toast` con el texto: `"RANDOMEXPRESSIONUsuario registrado|Cuenta registrada|Cuenta creada|Usuario creado|Registro correcto|Cuenta creada con éxito|Usuario creado con éxitoEND"`. **Indica** `Toast.LENGTH_LONG`.

Luego, al final del método `RANDOMEXPRESSIONregisterUser|registerNewUser|sendRegisterRequest|sendPostRegister|sendPostRequestEND()`, **añade** la petición a `this.RANDOMEXPRESSIONqueue|requestQueueEND` para que se envíe.

### Configuración general en una _app_ para usar Internet

Cuando termines los pasos anteriores, puedes efectuar dos cambios necesarios para que funcione la petición.

Primero, en `AndroidManifest.xml` **declara** el permiso de `INTERNET`. **Así**:

```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    package="com.afundacion.fp.sessions">
    
    <!-- Aquí, teclea <uses-permi y verás cómo Android Studio autocompleta -->
    <!-- Queremos añadir "android.permission.INTERNET" -->
    <application
        android:allowBackup="true"
```

_(Debemos declarar `INTERNET` siempre que nuestra aplicación lance peticiones de red. De lo contrario, serán denegadas por el propio dispositivo.)_

Después, también en `AndroidManifest.xml`, **añade** `android:usesCleartextTraffic="true"`:

```diff
    <application
+       android:usesCleartextTraffic="true"
        android:allowBackup="true"
```

_(Nuestro API REST no es seguro. Emplea HTTP en lugar de conexiones cifradas HTTPS. Por ello, debemos añadir este atributo. ¡Ojo! Esto n* debe ser así para apps que estén publicadas.)_

Después de esto, ya puedes probar la _app_ y registrarte introduciendo datos y pulsando el botón.

**¡Enhorabuena!** Has completado con éxito tu primer `POST` a un API REST para registrar usuarios.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

