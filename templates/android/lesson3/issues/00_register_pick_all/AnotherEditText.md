Contraseñas

## :books: Resumen

* Añadiremos otro `<EditText>` a la pantalla de registro, que ocultará la _entrada_ de usuario

## Descripción

¡Veamos cómo se configura un `<EditText>` que sirve para introducir una contraseña!

## :package: La tarea

**Añade** un nuevo `<EditText>` a `activity_register.xml`, debajo del anterior, con las siguientes características:

* Tendrá `layout_width="0dp"`, `layout_height="wrap_content"` y `layout_constraintWidth_percent="0.6"` _(como el anterior)_.
* También estará centrado _horizontalmente_.
* Con respecto a su posición _vertical_:
  * Su parte superior (`Top`) estará ubicada justo bajo la parte inferior (`Bottom`) del `<EditText>` anterior. Es decir, aparecerá justo _debajo_.
  * Para ello, deberás añadir un `android:id` al `<EditText>` anterior. Añade, de paso, otro `android:id` al nuevo `<EditText>`. Escógelos libremente.
  * No lo pegues al inferior del contenedor (`Bottom`).
* Tendrá `android:inputType="textPassword"`. ¿Cómo se ve el texto que tecleas tras añadir este atributo?
* Tendrá `android:hint="Contraseña"`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
