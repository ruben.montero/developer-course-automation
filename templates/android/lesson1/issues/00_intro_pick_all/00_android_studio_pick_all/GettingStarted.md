Primeros pasos programando para móviles

## :books: Resumen

* Veremos consideraciones especiales de desarrollar código para dispositivos móviles
* Instalaremos y configuraremos Android Studio. Probaremos la aplicación
* Eliminaremos el texto _Hello World!_ y comprobaremos de nuevo la aplicación
* Haremos `git add`, `git commit` y `git push` para subir los cambios al repositorio

## Descripción

Desarrollar aplicaciones para uso móvil _no_ es lo mismo que crear aplicaciones para escritorio (Windows, Linux,...). Es extremadamente importante que:

* Los _recursos_ (RAM, ancho de red,...) usados por nuestra aplicación sean los _mínimos_.
* Controlemos el _ciclo de vida_ de la aplicación. ¿Qué pasa si llaman por teléfono al usuario mientras nuestra _app_ está descargando un vídeo?
* Las aplicaciones sean [**_responsive_**](https://mockitt.wondershare.com/app-design/responsive-web-app.html). Deben adaptarse perfectamente a diferentes tamaños de pantalla.
* El _feedback_ visual sea inmediato. ¡Nada de tocar la pantalla y que dé la impresión de que la _app_ se ha quedado "pillada"!

### Principales plataformas

[Android](https://developer.android.com/guide?hl=es) e [iOS](https://developer.apple.com) conforman la pareja vencedora en lo que respecta a sistemas operativos móviles [hoy en día](https://es.statista.com/grafico/18920/cuota-de-mercado-mundial-de-smartphones-por-sistema-operativo).

<img src="android-introduction/docs/stores_logos.png" width=250/>

Usaremos Android como vehículo de aprendizaje pues la cuota de mercado es mayor (86% vs 14%), el precio para la publicación de _apps_ es menor (pago único de 25$ frente a 99$ anuales) y no dependemos de un _hardware_ específico (Xcode sólo funciona en MacOS).

Para comenzar a trabajar en Android, tenemos que haber _bajado_ nuestro repositorio. Si ya lo hiciste, puedes saltarte el siguiente apartado.

### Configuración de Git e inicialización del repositorio

**Instala** [Git](https://git-scm.com/download/win).

Después, **genera** una clave SSH[^1] en tu máquina local. Para ello, abre un terminal (tecla de Windows > cmd.exe) y escribe:

```
ssh-keygen -t rsa
```

**Pulsa** ENTER varias veces para completar el proceso. Luego, **abre** la carpeta `.ssh` que se habrá creado en tu directorio de usuario[^2]. Allí verás dos archivos `id_rsa` e `id_rsa.pub`. **Abre** (con el Bloc de Notas) el archivo `id_rsa.pub` y **copia** (CTRL+C) su contenido.

Después, **navega** a https://raspi y haz _login_ con tu cuenta de usuario. Arriba a la derecha, abre el _popup_ de ajustes y **navega** a `Settings`.

A la izquierda verás un panel lateral. Busca `SSH and GPG keys` y **haz click** ahí.

En esa pantalla, verás un recuadro de texto grande. **Pega** (CTRL+V) tu clave pública y **pulsa** en el botón verde `Add key` más abajo.

¡Clave configurada!

Ahora, desde tu terminal (cmd.exe) **clona** (descarga) el repositorio. Para ello, **escribe**:

```
git clone <url_de_tu_repositorio>
```

Donde `<url_de_tu_repositorio>` será la URL que puedes encontrar en https://raspi, yendo a tu proyecto y desde el botón azul de _Clone_:

<img src="android-introduction/docs/ssh_url_gitlab.png" width=400 />

¡Acabas de obtener la carpeta `pmdm/` (tu repositorio)!

Para terminar la configuración, desde el terminal, **ejecuta**:

```
git config --global user.name "Nombre Apellido"
git config --global user.email "nombre.apellido@fpcoruna.afundacion.org"
```

¡**Sustituye** correctamente `"Nombre Apellido"` y `"nombre.apellido@fpcoruna.afundacion.org"` por tu nombre real y tu correo institucional! (Debes mantener las comillas `" "`).

## :package: La tarea

Desde la página web de [Android Studio](https://developer.android.com/studio/), **descarga** el instalador `.exe` haciendo _click_ en `Download Android Studio` y **acepta** los términos de licencia[^3].

Una vez descargado, **ejecútalo** y procede con las opciones por defecto, hasta que llegues a la ventana donde permite abrir un proyecto existente o crear un proyecto nuevo.

Desde Android Studio, **selecciona `Open project`** y abre la carpeta `android-introduction/` que se encuentra el repositorio local de nuestro ordenador. Indica **Trust Project** y **Trust Server Certificate**. Si no has _clonado_ tu repositorio todavía, debes seguir los pasos de más arriba. 

### Paciencia :smile:

_Arrancar_ un proyecto es lento.

Antes de hacer nada, hay que cerciorarse de que el _banner_ amarillo `Gradle Project Sync in Process...` ha desaparecido y abajo no hay barras de progreso indicando tareas en segundo plano.

Aunque tarda hasta 5 ó 10 minutos, terminará estabilizándose y veremos a la izquierda nuestra estructura de proyecto:

```
- app/
   |
   |-- manifests/
   |
   |-- java/
   |    |
   |    |-- com.afundacion.fp.library/
   |    |    |
   |    |    |-- MainActivity.java
   |    |
   |    |-- com.afundacion.fp.library/ (androidTest)
   |    |-- com.afundacion.fp.library/ (test)
   |
   |-- res/

- Gradle Scripts/
```

### Lanzar la aplicación

**Haz** _click_ en _Devices Manager_, a la derecha. Pulsa el icono (`+`) para _Create Device_.

A continuación, crearemos un emulador sencillo. **Elige**:

* **Nexus S** (4.0") de 480x800 hdpi.

En la siguiente ventana del asistente, **selecciona** (**Download**) la versión:

* **Pie** (API Level 28) para x86

...del sistema operativo que usará la máquina simulada. Acepta el acuerdo de licencia y espera a que se descargue.

------------------------------------------------------------------------

**Importante**: **No** descargues una versión de API `> 28` o [tendrás problemas en algunos _tests_](https://github.com/android/android-test/issues/803).

------------------------------------------------------------------------

En la última página de _Verify configuration_ **cambia** _Emulated Performance_ > _Graphics_ y **elige** `Hardware - GLES 2.0`. Si está disponible, esto debería aprovechar la acelaración _hardware_ para que el emulador funcione más deprisa.

Una vez le damos a **_Finish_**, ¡emulador creado!

**Lanza** la aplicación con el **botón verde 'Play'** en la barra superior. Aparecerá a la derecha de _Nexus S API 28_ donde antes aparecía _No Devices_.

¡Y ten paciencia :smile:!

Si todo funciona como es debido, aparecerá el emulador a la derecha dentro de Android Studio:

<img src="android-introduction/docs/emulator_fresh_start.png" width=271/>

### Posibles problemas

* Si la aplicación no llega a lanzarse, es posible que el dispositivo esté _congelado_. Puedes verificarlo si Android Studio responde a tus clicks (e.g.: Abrir menú _File_) pero el emulador **no** hace **ni caso**. En ese caso:
  1. Cerramos el emulador (icono **X** en la pestaña al lado del nombre)
  2. Vamos a **Dispositivos** _(menú desplegable en la barra de arriba)_ > _Device Manager_ > _Actions_ > Flecha ⬇ (último icono) > _Wipe Data_.
  3. **Re-lanzamos** la aplicación con paciencia

Más adelante, veremos alternativas al _emulador de Android Studio_.

### Continuemos...

Trabajemos ahora en la aplicación.

Las _interfaces visuales_ de las pantallas se definen en archivos XML.

**Abre** `res > layout > activity_main.xml`. Al hacer doble _click_ en ella, verás:

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Hello World!"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```

**Elimina** las líneas 9-16 asociadas al `TextView`.

Si vuelves a lanzar la aplicación verás que el pequeño texto `Hello World!` ha desaparecido.

**¡Enhorabuena!** Primera tarea completada.

### :trophy: Por último

Desde el terminal de Windows (cmd.exe), nos posicionamos en `android-introduction/` y escribimos:

```
git add *
```

...para marcar los cambios del un nuevo _commit_. Después:

```
git commit -m "Tarea #1: Eliminado TextView hello world en actividad principal"
```

...y por último subimos los cambios al repositorio remoto:

```
git push
```

No está de más visitar la página de GitLab (https://raspi) y verificar que el _commit_ se ha subido.

---------------------------------------------------

[^1]: En realidad son un par de claves: La pública y la privada.

[^2]: Tu directorio de usuario está dentro de la ruta `C:\Users`. Por ejemplo, si tu usuario se llama `PepeDepura`, será `C:\Users\PepeDepura`.

[^3]: Estos términos de licencia los impone Google para que usemos su [SDK](https://www.xatakandroid.com/tutoriales/como-instalar-el-android-sdk-y-para-que-nos-sirve). Básicamente, nos está dando las herramientas necesarias para que nuestras aplicaciones funcionen en dispositivos reales y quiere que, en la medida de lo posible, le exoneremos de responsabilidad ante los daños que nuestras aplicaciones puedan causar
