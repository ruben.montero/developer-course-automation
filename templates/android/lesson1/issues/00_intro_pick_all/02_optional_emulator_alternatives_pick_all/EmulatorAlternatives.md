Opcional: Alternativas al emulador de Android Studio

## :books: Resumen

* Veremos algunas alternativas **opcionales** al emulador de Android Studio

## Descripción

Si estás conforme con Android Studio y su emulador, puedes **saltarte** esta tarea. No es evaluable.

### Si el emulador de Android Studio te parece lento...

No te preocupes. Estos problemas son **habituales**. A pesar de que la rapidez del emulador _ha mejorado notablemente_, sigue siendo poco práctico según qué ordenador estemos usando.

Puedes probar alguna de estas alternativas. La idea es que **consigas** desarrollar código de forma **agradable**. Si desarrollar código Android te resulta pesado, hay que buscar soluciones.

### Solución 1

#### [Usar un dispositivo real](https://developer.android.com/studio/run/device)

Existe diferente [documentación](https://www.desktopclass.com/skills/how-to-connect-an-android-device-in-android-studio.html) y tutoriales que nos pueden ayudar.

Lo que está claro es que necesitamos **un dispositivo Android físico**. Si contamos con él, debemos _activar las opciones para desarrolladores_:

* Iremos en el dipositivo físico a _Ajustes_ > _Sistema_ > _Acerca de_. Según qué Android tengas instalado, este menú puede ser distinto. Pero queremos llegar a **_Número de compilación_**. Haz `tap` con tu dedo _sobre_ **_Número de compilación_** repetidas veces, 6 ó 7. Verás que empiezan a salir _Toast_: `"¡Estás a X pasos de convertirte en desarrollador!"`
* Cuando pulses suficientes veces para indicar `"Te has convertido en desarrollador"`, habrás _habilitado un menú oculto_. Puedes buscarlo en los _Ajustes_. Se llama **_Opciones de desarrollador_**.
* Iremos a **_Opciones de desarrollador_** y nos aseguraremos que el `Check` (botón) principal está **habilitado**

Una vez hecho esto, basta con **conectar por USB** el móvil al ordenador.

**Debería** aparecer como dispositivo elegido en Android Studio. A partir de ahí, ya podremos **lanzar** nuestra aplicación en él.

#### ¿Y qué soluciona esto?

Como nuestro ordenador ya no tendrá que lanzar un emulador... ¡Nos ahorraremos mucha RAM! Indudablemente se traducirá en una mejora de la eficiencia.

#### ¿Y si me encuentro problemas?

El menú _Dispositivos_ > _Troubleshoot Device Connections_ de Android Studio puede ser de ayuda.

### Solución 2

#### Licencia personal de GenyMotion

[GenyMotion](https://www.genymotion.com/) es una alternativa al emulador de Android Studio. **No** nos ahorrará demasiada RAM en nuestro ordenador, pero al lanzar el emulador con un programa distinto, es posible que se nos haga más fácil el desarrollo.

Lo descargaremos de su [página de descarga](https://www.genymotion.com/download/). Si no tienes VirtualBox en la máquina, elegiremos **with VirtualBox**.

El proceso es sencillo. Bajamos e instalamos el programa con las opciones que deseemos. Tendremos que elegir una _licencia personal_, que está restringida a:

* 1 persona
* 1 puesto de trabajo
* Usos no comerciales

Una vez tengas GenyMotion **instalado** y tras **lanzarlo**, debería aparecer en Android Studio automáticamente para que puedas ejecutar tu _app_ en él.
