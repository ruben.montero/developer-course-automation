Un botón que ocupe todo el ancho

## :books: Resumen

* Añadiremos un botón alineado con la parte inferior
* Distinguiremos entre `wrap_content` y `0dp`
* Haremos que el botón ocupe todo el ancho

## Descripción

Tal y como hemos visto, hay cuatro posibles valores con los que podemos indicar que nuestras _constraints_ ajusten los componentes a:

* Arriba (`Top`)
* Abajo (`Bottom`)
* Derecha (`End`) _(ó `Right`, que es derecha estrictamente, incluso en países Left-to-Right)_
* Izquierda (`Start`) _(ó `Left`, que es izquierda estrictamente, incluso en países Left-to-Right)_

Y, también, los componentes tienen:

* `layout_width` (ancho)
* `layout_height` (alto)

En la tarea anterior hemos aplicado `layout_width="wrap_content"` a un botón. Esto hace que el _ancho_ del botón envuelva a su contenido.

Vamos a añadir un botón y usar `layout_width="0dp"`. `0dp` es [sinónimo de MATCH_CONSTRAINT](https://developer.android.com/reference/androidx/constraintlayout/widget/ConstraintLayout#widgets-dimension-constraints), o sea que la _anchura_ del botón ya no se ajusta a su contenido, sino que se ajusta a las _constraints_. ¡A continuación lo pondremos en práctica!

### Inciso: Sobre `dp`

Cuando indicamos medidas en Android, podemos usar [`dp`, `sp` ó `px`](https://openwebinars.net/blog/diferencias-entre-px-dp-y-sp-en-android/).

`px` es la medida en _píxeles_. Como distintas pantallas tienen distintas resoluciones (e.g.: 480x800 vs. 1080x1920), usar `px` es una _mala práctica_. `30px` ocupa _más lienzo_ en una pantalla pequeña que en una pantalla grande.

`dp` es la medida en _píxeles de densidad independiente_, es decir, [relativos al tamaño de la pantalla](https://openwebinars.net/blog/diferencias-entre-px-dp-y-sp-en-android/).

`sp` es similar a `dp`, pero adecuado para tamaños de textos.

## :package: La tarea

En `activity_main.xml`, **añade** una nueva etiqueta `<Button>`. Estará dentro del `<ConstraintLayout>` y será **así**:

```xml
    <Button
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="RANDOMEXPRESSIONIniciar otra actividad|Arrancar otra actividad|Lanzar otra actividad|Iniciar nueva actividad|Arrancar nueva actividad|Lanzar nueva actividadEND"/>
```

Como ves, la _constraint_ `Bottom_toBottomOf="parent"` indica que debe estar pegado abajo.

¡**Lanza** la aplicación y pruébalo! ¿Cómo se ve?

A continuación, **modifica** el valor `"wrap_content"` por `"0dp"` en `layout_width`:

_(Las líneas en rojo indican código que hay que borrar, y en verde es código para añadir)_

```diff
    <Button
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
-       android:layout_width="wrap_content"
+       android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:text="RANDOMEXPRESSIONIniciar otra actividad|Arrancar otra actividad|Lanzar otra actividad|Iniciar nueva actividad|Arrancar nueva actividad|Lanzar nueva actividadEND"/>
```

**Lanza** de nuevo la aplicación. ¿Cómo ves ahora el botón?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
