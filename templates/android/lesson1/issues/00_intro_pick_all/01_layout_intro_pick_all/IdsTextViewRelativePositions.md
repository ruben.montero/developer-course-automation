TextView, IDs, posición relativa

## :books: Resumen

* Añadiremos un `TextView`
* Usaremos el atributo `id` en nuestro primer botón
* Posicionaremos el `TextView` _debajo_ del primer botón

## Descripción

Hasta ahora sólo hemos usado [`Button`](https://developer.android.com/reference/android/widget/Button). 

Otro componente esencial en Android es [`TextView`](https://developer.android.com/reference/android/widget/TextView). Sirve para mostrar un texto sencillo.

## :package: La tarea

**Añade** un elemento `<TextView>` dentro del `<ConstraintLayout>` de nuestro `activity_main.xml`. Si escribes `<TextV`, Android Studio autocompletará fácilmente:

```xml
    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
```

**Añade** a dicho `<TextView>` el atributo `android:text=` (igual que hemos hecho con los botones) para que indique el texto: _RANDOMEXPRESSIONLa mantequilla no está incluida|Mantequilla no incluida|La mermelada no está incluida|Mermelada no incluida|No se incluye la mantequilla|No se incluye la mermeladaEND_.

También, **añade** las _constraints_ `Start_toStart="parent"` y `End_toEnd="parent"` para **centrar** el texto. En total, te quedará algo así:

```xml
    <TextView
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="RANDOMEXPRESSIONLa mantequilla no está incluida|Mantequilla no incluida|La mermelada no está incluida|Mermelada no incluida|No se incluye la mantequilla|No se incluye la mermeladaEND"/>
```

¡Ya tenemos el `<TextView>`! Ahora vamos a posicionarlo debajo del primer botón.

### Posicionando el texto _debajo_ del botón

En primer lugar, **añade** el atributo:

```xml
    android:id="@+id/toastButton"
```

...**al `<Button>`** de la primera tarea que indicaba `"RANDOMEXPRESSIONLanzar tostada|Tostada|Enseñar tostada|Ver tostada|Tostar panEND"`.

Este atributo sirve para dar un identificador único a un componente. Es útil para posicionar elementos de forma relativa en un `ConstraintLayout`. También lo usaremos más adelante, escribiendo código Java.

Ahora, **añade** esta _constraint_ a nuestro `<TextView>`:

```xml
    app:layout_constraintTop_toBottomOf="@id/toastButton"
```

Como puedes ver, se indica que la parte superior del componente (el `Top` del `<TextView>`) debe estar pegada a (`to`) la parte inferior (`Bottom`) del `"@id/toastButton"`.

**¡Listo!** Si lanzas la _app_, verás el texto _RANDOMEXPRESSIONLa mantequilla no está incluida|Mantequilla no incluida|La mermelada no está incluida|Mermelada no incluida|No se incluye la mantequilla|No se incluye la mermeladaEND_ debajo del primer botón.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
