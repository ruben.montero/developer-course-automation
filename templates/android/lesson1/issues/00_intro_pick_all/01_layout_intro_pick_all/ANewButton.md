Un botón

## :books: Resumen

* Añadiremos un botón al archivo de interfaz que editamos en la tarea anterior
* Verificaremos que nuestro botón aparece al lanzar la aplicación
* _(Y como siempre, subiremos nuestros cambios al repositorio)_

## Descripción

Los ficheros que describen las _interfaces_ (apariencia visual) en Android se escriben en lenguaje [XML _(eXtensible Markup Language)_](https://developer.mozilla.org/es/docs/Web/XML/XML_introduction). 

Como sabes, en [XML](https://developer.mozilla.org/es/docs/Web/XML/XML_introduction) la información se estructura mediante _etiquetas_, que son textos delimitados por _menor que_ y _mayor que_ (`<`, `>`).

Además, la información tiene estructura _jerárquica_, de tal forma que unas _etiquetas_ pueden contener otras _etiquetas_. Para ello:

* Se escribe una etiqueta de apertura (e.g.: `<MiEtiqueta>`)
* Se escriben etiquetas _hijas_
* Se escribe la etiqueta de cierre, que es igual pero con un _slash_ (`/`) (e.g.: `</MiEtiqueta>`)

En el fichero `activity_main.xml` que editamos en la tarea anterior hay _atributos_ y _espacios de nombres_ que dificultan la legibilidad, pero puedes apreciar que existe una única etiqueta de tipo:

```
androidx.constraintlayout.widget.ConstraintLayout
```

Un [ConstraintLayout](https://developer.android.com/reference/androidx/constraintlayout/widget/ConstraintLayout) es un tipo de _contenedor_ Android. En esta tarea, vamos a _añadir_ un botón ([`<Button>`](https://developer.android.com/reference/android/widget/Button)) dentro del _contenedor_.

## :package: La tarea

Edita el archivo `activity_main.xml` que quedó así tras la tarea anterior:

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    
</androidx.constraintlayout.widget.ConstraintLayout>
```

**Escribe** en el espacio en blanco del medio, la _etiqueta de apertura_ de `<Button>`:

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <Button
    
</androidx.constraintlayout.widget.ConstraintLayout>
```

Verás que Android intenta autocompletar lo que estás escribiendo. Pulsa **ENTER** para permitirlo:

```
<Button
    android:layout_width=""
    android:layout_height=""
```

Los atributos `android:layout_width` y `android:layout_height` sirven para indicar el ancho y alto del componente.

**Escribe** [`wrap_content`](https://developer.android.com/reference/android/view/ViewGroup.LayoutParams#WRAP_CONTENT):

```
<Button
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
```

...de esta manera, el ancho y alto del botón se ajustan a su contenido.

### ¿Qué contenido?

El componente `Button` puede mostrar un texto. **Añádelo** con el atributo `android:text=`, **así**:

```
<Button
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:text="RANDOMEXPRESSIONLanzar tostada|Tostada|Enseñar tostada|Ver tostada|Tostar panEND"
```

Para terminar, **cierra** la etiqueta como hemos visto antes:

```xml
<Button
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:text="RANDOMEXPRESSIONLanzar tostada|Tostada|Enseñar tostada|Ver tostada|Tostar panEND">
</Button>
```

Si queremos abreviarlo, podemos escribirlo así:

```xml
<Button
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:text="RANDOMEXPRESSIONLanzar tostada|Tostada|Enseñar tostada|Ver tostada|Tostar panEND"/> <!-- Nótese el slash (/) -->
```

**¡Enhorabuena!** Has añadido un botón a tu interfaz principal. Ahora puedes lanzar la aplicación con el botón verde de _Play_ y, tras esperar a que cargue, verás tu aplicación de nuevo. ¡Ahí está nuestro botón!
 
Puedes ignorar las advertencias que aparecen en el XML. Nos iremos encargando de ellas en las próximas tareas.

### :trophy: Por último

Haz `git add *` y `git commit -m "Tarea #2, botón añadido"` para crear un _commit_. Sube tus cambios al repositorio con `git push`.

