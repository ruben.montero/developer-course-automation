ConstraintLayout

## :books: Resumen

* Introduciremos el funcionamiento de `ConstraintLayout` y el uso de _constraints_
* Añadiremos 3 _constraints_ al botón

## Descripción

El elemento raíz de las interfaces Android con las que trabajaremos, [ConstraintLayout](https://developer.android.com/reference/androidx/constraintlayout/widget/ConstraintLayout), sirve para albergar vistas y posicionarlas de forma sencilla y escalable.

### **`Constraint`**

Son _atributos_ de los elementos que especifican su posición _dentro_ del `ConstraintLayout`, y tienen una forma como:

```
app:layout_constraintBottom_toBottomOf="parent"
```

_(Nótese que el prefijo es `app:` en vez de `android:`)_

### La alerta

En nuestro `<Button>` no hemos especificado ninguna _constraint_ y por eso aparece esta advertencia:

>>>
This view is not constrained. It only has designtime position, so it will jump to (0,0) at runtime unless you add the constraints
>>>

Es decir, que la razón por la que el botón aparece _arriba a la izquierda_ (el origen de coordenadas (0,0)) no es arbitraria, sino un efecto directo de que faltan _constraints_. Podemos imaginar que el `<Button>` está _flotando libremente_ porque ninguna _constraint_ lo restringe.

En [la documentación](https://developer.android.com/reference/androidx/constraintlayout/widget/ConstraintLayout#RelativePositioning) hay muchos casos de uso bien explicados. Comencemos viendo unas _constraint_ básicas:

![](android-introduction/docs/constraints_top_start.gif)

Como puedes observar, una [_constraint_](https://developer.android.com/reference/androidx/work/Constraints) indica:

* **_Qué_ (1)** parte del componente se unirá a...
* **..._qué_ (2)** parte de otro componente **(3)**.

En el ejemplo, se une el `Top` **(1)** del botón con el `Top` **(2)** del `"parent"`, es decir, el contenedor **(3)**. Si hubiera varios niveles de jerarquía, _"parent"_ haría referencia al inmediatamente superior (no necesariamente al `ConstraintLayout` raíz).

```
                     (1)   (2)    (3)
app:layout_constraintTop_toTopOf="parent"
```

La siguiente _constraint_ une el `Start` **(1)** del botón con el `Start` **(2)** del contenedor **(3)**:

```
                     (1)     (2)      (3)
app:layout_constraintStart_toStartOf="parent"
```

`Start` es _izquierda_, con la ventaja de que si nuestra aplicación se ejecuta en un dispositivo con idioma local _Right-To-Left_ (como árabe o hebreo), `Start` se posicionará en la _derecha_ automáticamente, para ofrecer a esos usuarios su sentido de lectura natural.

Las cuatro dimensiones que podemos usar para _arriba_, _abajo_, _izquierda_ y _derecha_ respectivamente son:

* `Top`
* `Bottom`
* `Start`
* `End`

## :package: La tarea

En `activity_main.xml`, **añade** las _constraints_ expuestas anteriormente a tu `<Button>`:

```xml

<Button
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:text="RANDOMEXPRESSIONLanzar tostada|Tostada|Enseñar tostada|Ver tostada|Tostar panEND"
    app:layout_constraintTop_toTopOf="parent"
    app:layout_constraintStart_toStartOf="parent" >
</Button
```

Si ejecutas la aplicación, verás que el botón aparece _arriba a la izquierda_. Igual que antes, pero ahora no hay ninguna advertencia en el XML indicando que la posición del botón es _ambigua_.

### Centrar el botón

Es posible especificar simultáneamente una _constraint_ `Start_toStart` y `End_toEnd`.

Dado que el _ancho_ está especificado como `wrap_content`, cada _constraint_ "tirará" del componente hacia su lado y se centrará[^1].

![](android-introduction/docs/constraints_top_start_end.png)

**Añade** una _constraint_ `End_toEnd` para _centrar_ tu botón en la parte superior.

Puedes lanzar la aplicación y verificar que el botón aparece centrado arriba.

### :trophy: Por último

Haz `git add *` y `git commit -m "Tarea 3, botón centrado"` para crear un _commit_. Sube tus cambios al repositorio con `git push`.

-----------------------

[^1]: Podrías esperar que el `<Button>` se deformase y se _pegase_ a ambos lados, ocupando todo el ancho. Eso puede suceder si el **ancho** es [algo _distinto_](https://stackoverflow.com/questions/6975607/what-does-layout-height-0dp-mean) a `wrap_content`. Lo veremos más adelante.
