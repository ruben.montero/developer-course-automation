Un tercer fragmento

## :books: Resumen

* Crearemos un tercer _Fragment_
* Se verá en cuando el usuario pulse el tercer _item_ de la barra inferior

## Descripción

Vamos a añadir un tercer _Fragment_.

## :package: La tarea

* **Añade** un XML `fragment_3.xml`. Contendrá un `ConstraintLayout`, y dentro un `ImageView`:
  * `contentDescription` será `"RANDOMEXPRESSIONUn último Digimon poderoso|Un último Digimon muy fuerte|Un último Digimon fuerte|Un Digimon excelente|Un Digimon asombrosoEND"`.
  * `src` usará la imagen `birdramon`.
  * Rellenará por completo el `ConstraintLayout` (usa `match_parent`).
  * Tendrá el atributo `android:scaleType="fitXY"`. ¿Cómo se verá?
* **Añade** un `Fragment3.java`, análogo al de la tarea anterior.
* En `MonstersActivity.java`, cuando el usuario haga _click_ en el _tercer item_ del menú, **efectúa** una transacción que reemplace el contenedor por una _instancia_ de `Fragment3`.

¡Verifica que puedes clicar todos los _items_ de la barra inferior y funciona como es esperable!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
