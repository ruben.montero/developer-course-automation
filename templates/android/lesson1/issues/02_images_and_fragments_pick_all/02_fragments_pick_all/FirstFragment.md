Un primer fragmento

## :books: Resumen

* Hablaremos de _Fragments_ en Android
* Añadiremos un `FrameLayout` a `activity_monsters.xml` que servirá de contenedor para distintos _fragments_
* Crearemos un primer _Fragment_
* Se verá en el contenedor cuando el usuario pulse el primer _item_ de la barra inferior

## Descripción

Los [_Fragments_](https://developer.android.com/guide/fragments) son algo así como _sub-actividades_ en Android. Representan una parte reutilizable de la interfaz de usuario y definen y administran su propio diseño. También tienen su propio [ciclo de vida](https://developer.android.com/guide/fragments/lifecycle).

¡Trabajemos con _Fragments_!

## :package: La tarea

Como primer paso, **elimina** el `ImageView` de `activity_monsters.xml`:

```diff
-    <ImageView
-        android:layout_width="0dp"
-        android:layout_height="0dp"
-        android:src="@drawable/RANDOMEXPRESSIONtogemon|kabuterimonEND"
-        android:contentDescription="RANDOMEXPRESSIONUn Digimon muy poderoso|Un Digimon poderoso|Un Digimon muy fuerteEND"
-        app:layout_constraintTop_toBottomOf="@id/monstersTitle"
-        app:layout_constraintStart_toStartOf="parent"
-        app:layout_constraintEnd_toEndOf="parent"
-        app:layout_constraintBottom_toTopOf="@id/bottomNavigation"/>
```

En su lugar, **añade** un [`<FrameLayout>`](https://developer.android.com/reference/android/widget/FrameLayout)[^1]. **Así**:

```xml
    <FrameLayout
        android:id="@+id/fragmentContainer"
        android:layout_width="0dp"
        android:layout_height="0dp"
        app:layout_constraintTop_toBottomOf="@id/monstersTitle"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintBottom_toTopOf="@id/bottomNavigation"
        />
```

Ahora que hemos sustituido nuestro `<ImageView>` por un `<FrameLayout>`, lo usaremos como _contenedor_ de los _Fragments_.

### _Fragments_

Tienen dos elementos: Interfaz XML y clase Java.

### Interfaz XML

**Haz** _click_ derecho en _res_ > _layout_ y **selecciona** _New_ > _Layout Resource File_.

El **Root Element** será `androidx.constraintlayout.widget.ConstraintLayout`, como viene por defecto.

**Dale** el nombre `fragment_1.xml`:

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

</androidx.constraintlayout.widget.ConstraintLayout>
```

Ahora, dentro de _este_ `ConstraintLayout` **añade** la imagen que borramos anteriormente. **Así**:

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <ImageView
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:src="@drawable/RANDOMEXPRESSIONtogemon|kabuterimonEND"
        android:contentDescription="RANDOMEXPRESSIONUn Digimon muy poderoso|Un Digimon poderoso|Un Digimon muy fuerteEND" />
        
</androidx.constraintlayout.widget.ConstraintLayout>
```

_(Como ves, se usa `match_parent` para `layout_width` y `layout_height`. Esto hace que la imagen encaje (rellene) el padre)._

### Clase Java

Nuestro _Fragment_ debe tener una clase asociada.

**Haz** _click_ derecho en _src_ > _com.afundacion.fp.library_ y **selecciona** _New_ > _Java Class_.

**Dale** el nombre `Fragment1`:

```java
public class Fragment1 {

}
```

A continuación, **declara** que la clase _herede_ de la superclase `Fragment`. **Así**:

```java
import androidx.fragment.app.Fragment;

public class Fragment1 extends Fragment {

}
```

Además, estamos _obligados_ a proveer un _constructor vacío_. Esta es una cuestión propia de cómo Android gestiona los _Fragments_. **Añádelo** así:

```java
import androidx.fragment.app.Fragment;

public class Fragment1 extends Fragment {

    public Fragment1() {
    }
}
```

¡Ya casi hemos terminado con nuestro _Fragment_!

Sólo falta implementar el método [`onCreateView`](https://developer.android.com/guide/fragments/lifecycle#fragment_created_and_view_initialized). Este método es análogo al `onCreate` de las actividades.

**Teclea** `onCreateView` debajo del constructor y verás que Android Studio nos permite autocompletar:

```java
public class Fragment1 extends Fragment {

    public Fragment1() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
```

Para terminar este método, **reemplaza** la línea `return super.onCreateView` por el siguiente contenido:

```java
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // 'Inflamos' el XML fragment_1 y lo ponemos en el 'container'
        return inflater.inflate(R.layout.fragment_1, container, false);
    }
```

_(Usamos el método `.inflate` del objeto provisto `inflater`. Esto sirve para leer el archivo `fragment_1.xml` y convertirlo un objeto tipo `View` que Android podrá usar)._

### Uniendo barra inferior y _Fragments_

Ahora, en un par de líneas de código, vamos a [unir los dos conceptos](https://www.youtube.com/watch?v=NfuiB52K7X8) con los que hemos trabajado recientemente.

Para empezar, en `MonstersActivity.java`, **instancia** un nuevo `Fragment1` dentro del `if` del ejercicio anterior, correspondiente al código que se ejecuta cuando el usuario hace _click_ en el primer elemento de la barra inferior:

```java
import androidx.fragment.app.Fragment;

public class MonstersActivity extends AppCompatActivity {
    // ...

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // ...
        bar.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.item1) {
                    // Aquí va el Toast de la tarea anterior. No lo borres
                    // ...
                    
                    // Instaciamos un Fragment1
                    Fragment myFragment = new Fragment1();
                }
                // Aquí van los if de item2 e item3
                // ... 
                
            }
        });
    }
}
```

A continuación, con _una línea_ **indica** que quieres _reemplazar_ (visualmente) el _contenedor_ con el _Fragment_.

**Empieza** así:

```java
            Fragment myFragment = new Fragment1();
            getSupportFragmentManager()
```

...para obtener una _instancia_ de un objeto administrador de _Fragments_, sobre el que indicarás que quieres empezar una nueva _transacción_ **así**:

```java
            Fragment myFragment = new Fragment1();
            getSupportFragmentManager().beginTransaction()
```

...y ahora sólo falta indicar que queremos reemplazar una vista con un `android:id` específico por el _Fragment_ de la línea anterior. **Termina** así:

```java
            Fragment myFragment = new Fragment1();
            // Recuerda que R.id.fragmentContainer es el id de nuestro FrameLayout añadido al principio
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, myFragment)
```

¡Ah! Un último paso. **Haz** efectivo el cambio con `.commit();`:

```java
            Fragment myFragment = new Fragment1();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, myFragment).commit();
```

¡Ya has conseguido que se vea un _Fragment_ cuando el usuario hace _click_ en **Digimon 1** de la barra inferior!

### Pero inicialmente `MonstersActivity` está vacía...

Cierto. Hemos eliminado el `<ImageView>` al principio así que era algo que cabía esperar.

**Añade** la siguiente línea al principio de `onCreate` en `MonstersActivity.java` para que, inicialmente, el _Fragment_ sea visible:

```diff
public class MonstersActivity extends AppCompatActivity {
    // ...

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monsters);
        
+       // Aquí, inicialmente también añadimos un Fragment1 a la interfaz
+       getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new Fragment1()).commit();
        
        // Otro código, como .setOnItemSelectedListener
        // ...
    }
}
```

**¡Listo!** Puedes verificar cómo funciona.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Es un tipo de _layout_ especial que se usa normalmente para albergar _un único hijo_.
