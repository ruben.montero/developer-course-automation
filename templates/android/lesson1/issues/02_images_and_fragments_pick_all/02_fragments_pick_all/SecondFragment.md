Un segundo fragmento

## :books: Resumen

* Crearemos un segundo _Fragment_
* Se verá en cuando el usuario pulse el segundo _item_ de la barra inferior

## Descripción

Vamos a añadir un segundo _Fragment_, siguiendo los mismos pasos de la tarea anterior.

## :package: La tarea

* **Añade** un XML `fragment_2.xml`. Contendrá un `ConstraintLayout`, y dentro un `ImageView`:
  * `contentDescription` será `"RANDOMEXPRESSIONOtro Digimon poderoso|Otro Digimon muy fuerte|Otro Digimon muy poderosoEND"`.
  * `src` usará la imagen `RANDOMEXPRESSIONgarurumon|greymonEND`.
  * Rellenará por completo el `ConstraintLayout` (usa `match_parent`).
  * Tendrá el atributo `android:scaleType="centerCrop"`. Así aprenderemos sobre este atributo.
* **Añade** un `Fragment2.java`, análogo al de la tarea anterior.
* En `MonstersActivity.java`, cuando el usuario haga _click_ en el _segundo item_ del menú, **efectúa** una transacción que reemplace el contenedor por una _instancia_ de `Fragment2` (análogo a la tarea anterior).

¡Verifica que puedes clicar en los dos primeros _items_ de la barra inferior y funciona como es esperable!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

