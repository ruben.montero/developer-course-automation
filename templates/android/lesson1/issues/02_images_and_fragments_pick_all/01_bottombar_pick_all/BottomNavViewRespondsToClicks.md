Respondiendo a clicks en la barra de navegación

## :books: Resumen

* Añadiremos código a `MonstersActivity.java` para lanzar un `Toast` cada vez que se haga _click_ en un elemento de la barra inferior

## Descripción

Nuestra `BottomNavigationView` es bonita, pero no hace nada.

Vamos a aprender _cómo_ responder a los _clicks_ del usuario.

## :package: La tarea

Para empezar, en `MonstersActivity.java`, **instancia** una variable de tipo `BottomNavigationView` en el método `onCreate`, asociándola al elemento del XML, **así**:

```java
public class MonstersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monsters);

        BottomNavigationView bar = findViewById(R.id.bottomNavigation);
        // ...
    }
}
```

Luego, sobre `bar`, **invoca** `setOnItemSelectedListener` (es similar a `setOnClickListener`):

```java
        bar.setOnItemSelectedListener();
```

...y dentro de los paréntesis, **instancia** un _listener_ de tipo [`NavigationBarView.OnItemSelectedListener`](https://developer.android.com/reference/com/google/android/material/navigation/NavigationBarView.OnItemSelectedListener) _anónimo_.

Será anónimo. Cuando escribamos `new` _(y espacio en blanco)_, Android Studio nos permitirá autocompletar:

```java
        bar.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return false;
            }
        });
```

Como ves, sólo hay un método `onNavigationItemSelected`. En él, _recibimos_ un parámetro tipo `MenuItem` que representa el _item_ clicado por el usuario. Dentro de este `MenuItem` podemos usar `.getItemId()` para distinguirlo.

**Escribe** el siguiente código:

```java
        bar.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.item1) {
                    // Hacer algo si el usuario pulsa Digimon 1
                }
                if (item.getItemId() == R.id.item2) {
                    // Hacer algo si el usuario pulsa Digimon 2
                }
                if (item.getItemId() == R.id.item3) {
                    // Hacer algo si el usuario pulsa Digimon 3 
                }
                return false;
            }
        });
```

También debemos modificar `return false`. Con ese _booleano_ devolvemos a Android si _hemos sido capaces de gestionar el click_. En la práctica, siempre vamos a poder, así que **sustitúyelo** por `true`:

```diff
-                return false;
+                return true;
```

Para terminar, **implementa** el siguiente comportamiento:

* Si el usuario pulsa en **Digimon 1**, se lanzará un _Toast_ con el texto: `"RANDOMEXPRESSIONHas clicado en Digimon 1|Clicaste Digimon 1|Este es el primero|Primer Digimon|Este es el primer DigimonEND"`
* Si el usuario pulsa en **Digimon 2**, se lanzará un _Toast_ con el texto: `"RANDOMEXPRESSIONHas clicado en Digimon 2|Clicaste Digimon 2|Este es el segundo|Segundo Digimon|Este es el segundo DigimonEND"`
* Si el usuario pulsa en **Digimon 3**, se lanzará un _Toast_ con el texto: `"RANDOMEXPRESSIONHas clicado en Digimon 3|Clicaste Digimon 3|Este es el tercero|Tercer Digimon|Este es el tercer DigimonEND"`

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
