Barra de navegación inferior

## :books: Resumen

* Añadiremos un archivo XML que representa un _menú_
* Añadiremos una barra de navegación inferior a nuestra `MonstersActivity`
* Vincularemos la barra de navegación a dicho _menú_ XML

## Descripción

La navegación en Android se basa en actividades. Cada vez que iniciamos una actividad, se añade a una _pila de navegación_. El usuario puede navegar hacia atrás pulsando el botón de _Atrás_.

Pues bien, es bueno no obligar al usuario a _navegar_ por demasiadas pantallas. ¡Esto no es la web! En una _app_ móvil, preferimos tener todo a golpe de _click_. Cuantas menos interacciones, mejor.

### [Menús](https://developer.android.com/guide/topics/ui/menus)

Los menús son elementos recurrentes en Android. Hay muchas formas de mostrarlos.

Nosotros vamos a añadir un _menú_ y permitir al usuario seleccionar sus elementos mediante una _barra inferior_. Al clicar en cada elemento del menú, se mostrará un [_Digimon_](https://es.wikipedia.org/wiki/Digimon) distinto.

## :package: La tarea

En primer lugar, desde la vista de carpetas a la izquierda **expande** _res > menu_. Haz _click_ derecho en él y **selecciona** _New > Menu Resource File_.

**Dale** el nombre `bottom_menu.xml`. Verás que se autogenera un archivo así:

```xml
<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android">

</menu>
```

Dentro de este menú XML, **añade** un primer elemento `<item />`. Luego, **especifica** _tres_ atributos: `android:id`, `android:title` y `android:icon`. **Así**:

```xml
<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item
        android:id="@+id/item1"
        android:title="Digimon 1"
        android:icon="@drawable/ic_monster" />
        
</menu>
```

¡Ese es el primer elemento del menú! Ahora, como queremos mostrar _tres_ [_Digimon_](https://es.wikipedia.org/wiki/Digimon) distintos, **añade** estos otros dos elementos:

```xml
<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item
        android:id="@+id/item1"
        android:title="Digimon 1"
        android:icon="@drawable/ic_monster" />
    <item
        android:id="@+id/item2"
        android:title="Digimon 2"
        android:icon="@drawable/ic_monster"/>
    <item
        android:id="@+id/item3"
        android:title="Digimon 3"
        android:icon="@drawable/ic_monster"/>
</menu>
```

### Barra de navegación inferior

En `MonstersActivity` vamos a [usar una `BottomNavigationView`](https://www.geeksforgeeks.org/bottomnavigationview-inandroid/), que nos permitirá ofrecer varias opciones al usuario desde la parte inferior de la pantalla.

¡Esto es muy útil! Cuando coges el teléfono con una mano, la parte inferior queda más cerca del pulgar que la parte superior. Es más _usable_.

Para empezar, **añade** un `<com.google.android.material.bottomnavigation.BottomNavigationView>` a `activity_monsters.xml` dentro del `<ConstraintLayout>`, al final, como se indica a continuación:


```xml
    <!--
    Hace falta disponer de la librería de Google Material para usar BottomNavigationView.
    Para ello, basta con que en el archivo build.gradle se indique en
    'dependencies' la línea:
        implementation 'com.google.android.material:material:x.y.z'
    Con la versión x.y.z de la librería deseada.
    
    Esta línea YA está añadida en nuestro proyecto
    -->
    <com.google.android.material.bottomnavigation.BottomNavigationView
        android:id="@+id/bottomNavigation"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintBottom_toBottomOf="parent"/>
```

_(Como ves, las `constraints` están pensadas para que la barra se quede pegada abajo, ocupando todo el ancho)_

Para completar lo necesario en `<com.google.android.material.bottomnavigation.BottomNavigationView>`, **añádele** el siguiente atributo `app:menu`, **así**:

```xml
        app:menu="@menu/bottom_menu"
```

De esta manera, nuestro `<BottomNavigationView>` ya está _ligado_ al menú XML.

### Un pequeño detalle...

También en `activity_monsters.xml`, para que imagen no se _superponga_ a nuestra barra inferior, **posiciónala** adecuadamente:

```diff
    <ImageView
        android:layout_width="0dp"
        android:layout_height="0dp"
        android:src="@drawable/RANDOMEXPRESSIONtogemon|kabuterimonEND"
        android:contentDescription="RANDOMEXPRESSIONUn Digimon muy poderoso|Un Digimon poderoso|Un Digimon muy fuerteEND"
        app:layout_constraintTop_toBottomOf="@id/monstersTitle"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
-       app:layout_constraintBottom_toBottomOf="parent"
+       app:layout_constraintBottom_toTopOf="@id/bottomNavigation" />
```

¡Listo! Prueba a ver tu barra inferior en la `MonstersActivity`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
