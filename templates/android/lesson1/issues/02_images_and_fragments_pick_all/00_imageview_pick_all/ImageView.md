Imágenes

## :books: Resumen

* Aprenderemos sobre el componente `ImageView`
* Mostraremos una imagen en `MonstersActivity`

## Descripción

Hasta ahora hemos usado botones y textos.

Son dos componentes fundamentales en desarrollo móvil, pero a los usuarios también le gusta observar recursos gráficos en las aplicaciones.

[`ImageView`](https://developer.android.com/reference/android/widget/ImageView) es un componente que sirve para mostrar una imagen. Nosotros vamos a usar una imagen que _ya viene_ con la aplicación.

### ¿_Ya viene_ con la aplicación?

Sí. En la carpeta `res/` podemos añadir distintos tipos de _resources_ (recursos). Algunos, como ya sabes, son archivos XML que describen las interfaces.

Pues bien, dentro de `res/drawables/` es común añadir _imágenes_ (`.png`, `.jpg`) que mostraremos dentro de nuestra aplicación.

¡Ojo! No queremos que estas imágenes ocupen demasiado, pues _aumentarán el tamaño de la aplicación_. ¿Alguna vez has ido a bajar una aplicación de la PlayStore y después de ver que pesaba 200Mb has cancelado la descarga?

## :package: La tarea

En primer lugar, en `activity_monsters.xml`, **añade** un `android:id` al `TextView` que ya habíamos creado en una tarea anterior:

```xml
    <TextView
        android:id="@+id/monstersTitle"
```

Luego, **añade** un `ImageView`:

```xml
    <ImageView
        android:layout_width=""
        android:layout_height=""
```

Es _raro_ que se use `wrap_content` con un `ImageView`. Pantallas de diferentes tamaños mostrarán la misma imagen más grande o más pequeña, pues los píxeles de la imagen son los mismos.

En su lugar, suele concebirse el `ImageView` como un _lienzo_. Ajustamos dicho _lienzo_ empleando _constraints_. Para ello, **usa** `0dp` como _ancho_ y _alto_:

```xml
    <ImageView
        android:layout_width="0dp"
        android:layout_height="0dp"
```

A continuación, **añade** _constraints_ para posicionarlo _debajo_ del título, y _pegado_ al borde de la pantalla por izquierda, derecha y abajo. **Así**:

```xml
    <ImageView
        android:layout_width="0dp"
        android:layout_height="0dp"
        app:layout_constraintTop_toBottomOf="@id/monstersTitle"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintBottom_toBottomOf="parent"/>
```

### `android:src`

Lo único que nos falta es indicar _qué_ imagen debe mostrar. Nosotros vamos a mostrar la imagen de _RANDOMEXPRESSIONtogemon|kabuterimonEND_.

**Añade** el siguiente atributo a tu `ImageView`:

```xml
        android:src="@drawable/RANDOMEXPRESSIONtogemon|kabuterimonEND"
```

### Y... una buena práctica

También es _recomendable_ añadir el atributo `contentDescription` a las imágenes.

Este atributo sirve para _describir_ una imagen por cuestiones de [accesibilidad](https://es.wikipedia.org/wiki/Accesibilidad). Es decir, una persona ciega o con visión reducida usará el _lector de pantalla_ y agradecerá disponer de una ayuda para comprender el contenido visual de la imagen.

Para terminar, **añade** este otro atributo a tu `ImageView`:

```xml
        android:contentDescription="RANDOMEXPRESSIONUn Digimon muy poderoso|Un Digimon poderoso|Un Digimon muy fuerteEND"
```

**¡Listo!** Ya hemos mostrado nuestra primera imagen en una aplicación. ¿Qué tal se ve?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
