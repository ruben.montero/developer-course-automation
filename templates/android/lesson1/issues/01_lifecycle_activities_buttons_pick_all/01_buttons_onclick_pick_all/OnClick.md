Clicar un botón

## :books: Resumen

* Veremos cómo responder a un _click_ de un usuario en un botón
* Hablaremos brevemente de interfaces Java
* Mostraremos un _Toast_ si el usuario clica en el botón superior

## Descripción

Responder al _click_ de un usuario en un componente es muy fácil.

Basta con usar un objeto `OnClickListener`:

### [`OnClickListener`](https://developer.android.com/reference/android/view/View.OnClickListener)

Es una interfaz Java que declara un único método:

```java
public void onClick(View v)
```

Recuerda que las interfaces, en Java, son _firmas de métodos_. Si una clase _tiene_ esos métodos, se dice que _conforma_ ó _implementa_ esa interfaz Java.

Por ejemplo, una interfaz Java _válida_ sería:

```java
public interface InterfazEjemploNumero {

  public int devolverUnNumero(boolean positivo);
}
```

Como ves, el método acaba en punto y coma (`;`) en vez tener un cuerpo entre llaves (`{ }`). Eso es porque en la interfaz sólo está presente la _firma_ del método. Son las _clases_ quienes implementan las _interfaces_ especificando `implements`, por ejemplo:

```java
public class ClaseImplementadora implements InterfazEjemploNumero {

  @Override
  public int devolverUnNumero(boolean positivo) {
    if (positivo) {
      return 10;
    } else {
      return -10;
    }
  }
}
```

La anotación `@Override` sirve para indicar sobreescritura de un método de _superclase_ **o** de una _interfaz_. Este doble uso puede despistar a veces, pero no le demos mucha importancia.

## :package: La tarea

**Añade** una nueva clase. Para ello, **haz** _click_ derecho en `com.afundacion.fp.library` > _New_ > _Java Class_. **Dale** el nombre `RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND`.

**Añade** el siguiente contenido a la clase (te resultará fácil de entender si ya eres familiar con los [POJOs](https://www.javatpoint.com/pojo-in-java)):

```java
package com.afundacion.fp.library;

import android.content.Context;

public class RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND {
    private Context context;
    
    public RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND(Context context) {
        this.context = context;
    }
}
```

Ahora, en `MainActivity.java`, **crea** una nueva instancia de `RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND` después del `Toast` de la tarea anterior. **Así**:

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(this, "RANDOMEXPRESSIONLa primera tostada del día|¡La primera tostada del día!|Primera tostada del día|¡Primera tostada del día!|Tostada del desayuno|La tostada del desayuno|¡Tostada del desayuno!|¡La tostada del desayuno!END", Toast.LENGTH_SHORT).show();
        RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND myHandler = new RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND(this); // Instanciamos un nuevo RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND
    }
```

_(Una `Activity` **es** un `Context`, por eso, podemos pasar `this` en el constructor)_

Ahora vamos a identificar y asociar nuestro botón al `RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND`.

En `activity_main.xml`, el `<Button>` es una etiqueta.

En Java, será una _variable_ de tipo `Button` que no _inicializamos_ usando `new`, sino usando [`findViewById`](https://metapx.org/android-findviewbyid/).

Adelante, **añade** la siguiente línea:

```java
        Button myButton = findViewById(R.id.toastButton);
```

_(Como puedes ver, `R.id.toastButton` es el mismo `android:id` que habíamos indicado en el XML)_

### El penúltimo paso

Vamos a unir nuestro `myHandler` al botón. Eso se hace _invocando_ `setOnClickListener` sobre el botón.

**Hazlo** así:

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(this, "RANDOMEXPRESSIONLa primera tostada del día|¡La primera tostada del día!|Primera tostada del día|¡Primera tostada del día!|Tostada del desayuno|La tostada del desayuno|¡Tostada del desayuno!|¡La tostada del desayuno!END", Toast.LENGTH_SHORT).show();
        RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND myHandler = new RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND(this);
        Button myButton = findViewById(R.id.toastButton);
        myButton.setOnClickListener(myHandler);
    }
```

### Hemos terminado con `MainActivity.java`

No te preocupes por la alerta que aparece en la última línea:

>>>
RequiredType: OnClickListener

ProvidedType: RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND
>>>

Java está indicando: _"Eh, quería que pasases un `OnClickListener` a este método y me has pasado un `RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND` en su lugar"_ ¡Lleva razón!

Vamos a...

### ...hacer que `RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND` sea también `OnClickListener`

En `RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND.java`, **añade** `implements View.OnClickListener`:

```java
import android.content.Context;
import android.view.View;

public class RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND implements View.OnClickListener{
    private Context context;

    public RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND(Context context) {
        this.context = context;
    }
}
```

Verás que la primera línea aparece subrayada en rojo. 

>>>
Class 'RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND' must (...) implement abstract method 'onClick(View)'
>>>

¡Claro! Tras declarar que se _implementa_ una interfaz... ¡Debemos implementarla!

**Añade** el método `onClick`:

```java
public class RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND implements View.OnClickListener{
    private Context context;

    public RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND(Context context) {
        this.context = context;
    }

    @Override
    public void onClick(View view) {

    }
}
```

Y, para terminar, en el método `onClick`, **haz** que se lance un `Toast`. **Así**:

```java
    @Override
    public void onClick(View view) {
        Toast.makeText(this.context, "RANDOMEXPRESSION¡Tostada al aire!|¡Ahí va la tostada!|¡Rápido, coge un plato!|¡Cuidado, que quema!|¡No te quemes!|¡Ha salido un poco churruscada!END", Toast.LENGTH_LONG).show();
    }
```

**¡Enhorabuena!** Has conectado tu primer botón en Android.

Ya puedes lanzar `app` y hacer _click_ en "RANDOMEXPRESSIONLanzar tostada|Tostada|Enseñar tostada|Ver tostada|Tostar panEND", ¿qué ves?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
