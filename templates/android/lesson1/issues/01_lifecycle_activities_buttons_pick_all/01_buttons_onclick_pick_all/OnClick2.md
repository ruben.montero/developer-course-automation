Clicar un botón sin escribir una clase nueva

## :books: Resumen

* Veremos cómo instanciar una interfaz con una clase _anónima_
* Mostraremos un _Toast_ si el usuario clica en el botón inferior

## Descripción

Comenzamos la tarea anterior diciendo:

> Responder al _click_ de un usuario en un componente es muy fácil.

Sin embargo, tuvimos que escribir una nueva clase, implementar una interfaz... ¡Qué complicado!

Vamos a aprender cómo podemos conseguir lo mismo sin añadir una nueva clase.

## :package: La tarea

Como primer paso, en `activity_main.xml`, **otórgale** un `id` al botón inferior:

```
    android:text="RANDOMEXPRESSIONIniciar otra actividad|Arrancar otra actividad|Lanzar otra actividad|Iniciar nueva actividad|Arrancar nueva actividad|Lanzar nueva actividadEND"
    android:id="@+id/bottomButton"/>
```

Ahora, en `MainActivity.java`, **instancia** el `Button` al final del método `onCreate`:

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // ...
        Button otherButton = findViewById(R.id.bottomButton);
    }
```

### Dejemos que Android Studio autocomplete

Tras la línea anterior, **escribe** `otherButton.setOnClickListener()` y **dentro** de los paréntesis, **escribe** `new `. En cuanto insertemos un espacio en blanco, Android Studio nos sugerirá una lista de opciones de autocompletado.

La primera seguramente sea `RANDOMEXPRESSIONClickHandler|TapHandler|OnClickHandler|ActionHandlerEND`. En ese caso nos interesa la segunda, `View.OnClickListener{...}`. **Dale** a ENTER:

```java
        otherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
            }
        });
```

Ya tenemos nuestro deseado `onClick`.

Esto es lo que se conoce como crear una clase _anónima_. Estamos _instanciando_ (con `new`) una clase que estamos creando al momento, y por eso no tiene nombre.

Para terminar, **muestra** un `Toast` con el mensaje `"RANDOMEXPRESSIONIniciando otra actividad...|Arrancando otra actividad...|Pasando a otra actividad...|Mostrando otra actividad...|Empezando otra actividad...END"` en dicho `onClick`:

```java
        otherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(this, "RANDOMEXPRESSIONIniciando otra actividad...|Arrancando otra actividad...|Pasando a otra actividad...|Mostrando otra actividad...|Empezando otra actividad...END", Toast.LENGTH_LONG).show();
            }
        });
```

### ¡Un momento! Android Studio indica un error

Verás un error:

> _Cannot resolve method..._

Se debe a que estamos invocando `makeText` con parámetro incorrectos.

_En esa parte del código_ (dentro de `onClick`), `this` no se refiere a la `Activity`, sino a la clase _anónima_ recién creada. Por lo tanto, no es un `Context`. Por lo tanto, no vale.

### ¿Cómo lo solucionamos?

**Añade** un atributo de tipo `Context` a `MainActivity` que sea `this`:

```java
import android.content.Context;

public class MainActivity extends AppCompatActivity {
    private Context context = this;

    // ...
}
```

Y luego, en la línea problemática, **usa** `context` en lugar de `this`:

```java
        otherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "RANDOMEXPRESSIONIniciando otra actividad...|Arrancando otra actividad...|Pasando a otra actividad...|Mostrando otra actividad...|Empezando otra actividad...END", Toast.LENGTH_LONG).show();
            }
        });
```

¡Listo! Puedes lanzar tu `app` y verificar que ambos botones muestran distintos `Toast`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
