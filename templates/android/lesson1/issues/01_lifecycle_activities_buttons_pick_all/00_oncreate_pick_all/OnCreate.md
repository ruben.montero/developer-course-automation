Ciclo de vida: onCreate

## :books: Resumen

* Introduciremos el concepto de actividad y distinguiremos XML y Java
* Hablaremos brevemente del método `onCreate`
* Mostraremos un _Toast_ cuando se crea nuestra actividad

## Descripción

La experiencia de usuario con aplicaciones móviles es distinta a las aplicaciones de escritorio. En una _app_ móvil no se puede esperar que haya un único punto de entrada. Por ejemplo, si abres la aplicación de Gmail verás tu bandeja de entrada. Pero si seleccionas un PDF desde el explorador de archivos y usas _Compartir con > Gmail_, aparecerá directamente la ventana para redactar un nuevo correo con el archivo adjunto.
 
Las [actividades](https://developer.android.com/guide/components/activities/intro-activities) son el elemento central de la programación Android y facilitan este paradigma.

Debemos entender una _actividad_ como una _pantalla_, digamos, un _lienzo_ donde nuestra aplicación puede pintar la interfaz de usuario. La mayoría de aplicaciones tienen varias pantallas, por lo que tienen varias actividades.

Hay _tres_ piezas que construyen una actividad:

### 1) Archivo de interfaz XML

Como `activity_main.xml` con el que hemos estado trabajando. Suelen comenzar con `activity_` y luego llevar en `snake_case` el nombre de la actividad.

### 2) Declaración en el manifiesto XML

El [archivo de manifiesto](https://developer.android.com/guide/topics/manifest/manifest-intro) Android es un único archivo XML que tiene cada aplicación. En él se declaran cosas como el _nombre_ de la aplicación, el _icono_, o las _actividades_. 

Para declarar una actividad, basta con que se incluya en el sitio adecuado (dentro de `<application>`) una etiqueta como:

```xml
<activity android:name=".ExampleActivity" />
```

Si echas un vistazo al archivo _app > manifests > AndroidManifest.xml_ de nuestra aplicación, verás [cómo está declarada la (única) actividad de la aplicación](https://developer.android.com/guide/components/activities/intro-activities).

Observa que tiene un par de cosas adicionales. Entre ellas, `android.intent.category.LAUNCHER` sirve para indicar que es la actividad _inicial_.

### 3) Clase Java

Asumiendo cierto grado de conocimiento de [programación orientada a objetos (POO)](https://ifgeekthen.nttdata.com/es/herencia-en-programacion-orientada-objetos) diremos que:

* Cada actividad tiene una clase asociada. Esta clase debe _heredar_ (ser una subclase) de `Activity` (o `AppCompatActivity`).
* Por ello, el nombre de la clase suele llevar el sufijo `Activity`, e.g.: `MainActivity`.
* Nosotros **no** _instanciamos_ nuestra clase. El sistema Android se encarga de ello.
* Nuestra responsabilidad es _sobreescribir_ (`@Override`) los **métodos** que responden a eventos.

### ¿Métodos que responden a eventos?

Sí.

El método principal es `onCreate`. Se trata de un método que **se va a ejecutar una (1) sóla vez** cuando nuestra actividad sea creada.

## :package: La tarea

**Abre** _app > java > com.afundacion.fp.library > `MainActivity.java`_. Puedes ver la declaración de una clase, como habíamos prometido, y, además, hay un método llamado `onCreate`:

```java
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
```

Dentro de este método hay un par de líneas típicas que sirven para inicializar la actividad.

A continuación de esas dos líneas, **añade** una línea que sirva para lanzar un `Toast`[^1]:

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Vamos a lanzar un Toast
        Toast.makeText() // ??
    }
```

El método estático `makeText` recibe tres parámetros:

* Un `Context`. Esta clase es esencial en muchos métodos Android. Basta saber que una `Activity` _también_ es un `Context`, así que podemos enviar `this`.
* Un texto en `String`. Nosotros usaremos `"RANDOMEXPRESSIONLa primera tostada del día|¡La primera tostada del día!|Primera tostada del día|¡Primera tostada del día!|Tostada del desayuno|La tostada del desayuno|¡Tostada del desayuno!|¡La tostada del desayuno!END"`.
* Una constante que indique si se mostrará un tiempo corto o largo. Sólo vale `Toast.LENGTH_SHORT` y `Toast.LENGTH_LONG`.

Esto devuelve un objeto tipo `Toast` sobre el que podemos invocar `.show()` para que se muestre:

```java
Toast oneToast = Toast.makeText(this, "RANDOMEXPRESSIONLa primera tostada del día|¡La primera tostada del día!|Primera tostada del día|¡Primera tostada del día!|Tostada del desayuno|La tostada del desayuno|¡Tostada del desayuno!|¡La tostada del desayuno!END", Toast.LENGTH_SHORT);
oneToast.show();
```

Por brevedad suele escribirse todo en una línea:

```java
Toast.makeText(this, "RANDOMEXPRESSIONLa primera tostada del día|¡La primera tostada del día!|Primera tostada del día|¡Primera tostada del día!|Tostada del desayuno|La tostada del desayuno|¡Tostada del desayuno!|¡La tostada del desayuno!END", Toast.LENGTH_SHORT).show();
```

¡Adelante! **Añade** esta tostada en ~~nuestro desayuno~~ nuestra aplicación.

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Vamos a lanzar un Toast
        Toast.makeText(this, "RANDOMEXPRESSIONLa primera tostada del día|¡La primera tostada del día!|Primera tostada del día|¡Primera tostada del día!|Tostada del desayuno|La tostada del desayuno|¡Tostada del desayuno!|¡La tostada del desayuno!END", Toast.LENGTH_SHORT).show();
    }
```

**¡Enhorabuena!** Has programado tu primera _tostada_.

Prueba a ejecutar tu _app_ en el emulador. ¿Qué ves nada más arranca?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Un `Toast` es un pequeño elemento visual típico de Android que salta momentáneamente en la pantalla, como una tostada. Es útil para verificar que el código se está ejecutando.
