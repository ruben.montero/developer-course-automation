Ciclo de vida: onStop

## :books: Resumen

* Veremos las posibilidades en el ciclo de vida de una actividad Android
* Implementaremos `onStop` en nuestro `MainActivity`

## Descripción

El [_ciclo de vida_](https://developer.android.com/guide/components/activities/activity-lifecycle) de una actividad Android consiste en los posibles estados que puede atravesar mientras el usuario disfruta de ella.

Inicialmente se creará y entonces `onCreate` será invocado.

¿Qué más puede pasar?

### Ciclo de vida

![](android-introduction/docs/activity_lifecycle.png)

Nosotros vamos a utilizar [`onStop`](https://developer.android.com/guide/components/activities/activity-lifecycle#onstop). Sirve para controlar que la _app_ ha dejado de estar en primer plano, igual que `onDestroy`. La diferencia es que si hay escasez de memoria RAM, el sistema Android puede matar nuestra aplicacion _sin_ invocar `onDestroy.`

## :package: La tarea

En `MainActivity.java`, añade un nuevo método _dentro_ de la clase. **Teclea**:

```
onSto
```

Verás cómo Android Studio autocompleta por nosotros. **Dale** a ENTER para que eso pase:

```java
public class MainActivity extends AppCompatActivity {
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) { /* ... */ }

    @Override
    protected void onStop() {
        super.onStop();
        // ...
    }
}
```

A continuación, _dentro_ de `onStop` (después de `super.onStop()`), **lanza** un `Toast` con el texto `"RANDOMEXPRESSIONMainActivity en segundo plano|MainActivity fue a segundo plano|MainActivity se fue|Adiós, MainActivity|Hasta luego, MainActivity|MainActivity se ha ido|Se ha ido MainActivityEND"`.

Una vez lo hayas escrito, **prueba** tu `app` en el emulador.

¿Qué ves si haces click en el botón inferior para empezar la otra actividad?

¿Qué ves si pulsas el botón de _Atrás_ un par de veces para salir de la aplicación

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
