Nueva actividad

## :books: Resumen

* Crearemos una nueva actividad
* La iniciaremos si el usuario _clica_ en el botón inferior

## Descripción

Nuestra aplicación tiene sólo una actividad, `MainActivity`. Vamos a crear otra actividad llamada `MonstersActivity`. Recordemos que una actividad tiene _tres_ componentes fundamentales:

* Clase Java
* Interfaz XML
* Declaración en `AndroidManifest.xml`

Vayamos añadiéndolos uno a uno.

## :package: La tarea

### Clase Java

**Añade** una nueva clase Java. **Dale** el nombre `MonstersActivity`.

En su contenido, **añade** el siguiente código:

```java
package com.afundacion.fp.library;

import androidx.appcompat.app.AppCompatActivity;

public class MonstersActivity extends AppCompatActivity {
}
```

_(Como ves, `MonstersActivity` hereda de `AppCompatActivity`. La herencia se declara en Java con la palabra reservada `extends`)_

### Interfaz XML

En las carpetas de la izquierda, **expande** _res_. **Haz** _click_ derecho en _res > layout_. Ahí, **selecciona** _New > Layout Resource File_.

Seguramente ya nos aparezca como **Root element** `androidx.constraintlayout.widget.ConstraintLayout`. Eso está bien.

Ahora, **escribe** `activity_monsters.xml` como **File Name** y dale a OK. Se generará un XML como este:

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

</androidx.constraintlayout.widget.ConstraintLayout>
```

### Declaración en `AndroidManifest.xml`

Ya hemos visto que en _manifest > `AndroidManifest.xml`_ se añaden etiquetas `activity` para declarar nuevas actividades. ¡Hagámoslo!

**Añade** `<activity android:name=".MonstersActivity"/>` a `AndroidManifest.xml`:

```xml
    <application
        ...>
        <activity
            android:name=".MainActivity"
            ... >
        </activity>
        
        <!-- Añadimos una nueva activity debajo de la otra -->
        <activity android:name=".MonstersActivity"/>
        
    </application>
```

¡Actividad creada y declarada!

### [Iniciando la actividad](https://developer.android.com/training/basics/firstapp/starting-activity) con `startActivity`

Vamos a hacer que la _app_ navegue a esta nueva actividad cuando el usuario clique el botón "RANDOMEXPRESSIONIniciar otra actividad|Arrancar otra actividad|Lanzar otra actividad|Iniciar nueva actividad|Arrancar nueva actividad|Lanzar nueva actividadEND."

¿Recuerdas que en `MainActivity.java` ya tenías el siguiente _on click_?

```java
        otherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(this, "RANDOMEXPRESSIONIniciando otra actividad...|Arrancando otra actividad...|Pasando a otra actividad...|Mostrando otra actividad...|Empezando otra actividad...END", Toast.LENGTH_LONG).show();
            }
        });
```

¡Bien! **Complétalo** añadiendo estas dos líneas para _arrancar_ otra actividad.

En primer lugar, **crea** un `Intent` en `MainActivity.java`, como se indica a continuación:

```java
        // ...
        // Este código pertenece a onCreate en MainActivity.java
        otherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "RANDOMEXPRESSIONIniciando otra actividad...|Arrancando otra actividad...|Pasando a otra actividad...|Mostrando otra actividad...|Empezando otra actividad...END", Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(context, MonstersActivity.class);
                context.startActivity(myIntent);
            }
        });
```

_(`Intent` representa algo así como una intención. La intención de iniciar la `MonstersActivity`.)_

**¡Enhorabuena!** Has creado y arrancado tu primera actividad desde cero. 

Prueba el comportamiento en el emulador. ¿Al hacer _click_ ves una nueva pantalla vacía?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
