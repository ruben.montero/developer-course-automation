Un título sencillo

## :books: Resumen

* En nuestra nueva `MonstersActivity` añadiremos un `TextView`
* Aprenderemos sobre los atributos `margin`, `textSize` y `textColor`

## Descripción

Vamos a añadir un texto de encabezado a la nueva actividad.

Para ello, editaremos el archivo de interfaz XML.

### `activity_monsters.xml`

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

</androidx.constraintlayout.widget.ConstraintLayout>
```

## :package: La tarea

**Añade** a `activity_monsters.xml` un `TextView` con el texto **"Biblioteca de monstruos"**:

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">
    
    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Biblioteca de monstruos"/>

</androidx.constraintlayout.widget.ConstraintLayout>
```

Con _constraints_, **posiciónalo** centrado horizontalmente y ubicado en la parte superior. **Así**:

```xml
    <TextView
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Biblioteca de monstruos"/>
```

_(Android Studio resaltará `app` en rojo. La ayuda nos permitirá autocompletar `xmlns:app="http://schemas.android.com/apk/res-auto"`, que es la definición de espacio de nombres necesaria para que funcione)_

Podemos indicar `android:layout_margin{Top|Bottom|Start|End}` para que el componente esté espaciado una cantidad de `dp` en dicha dimensión.

**Añade** el siguiente `layout_marginTop` para alejarlo un poco del margen superior:

```xml
        android:layout_marginTop="10dp"
```

Y, a continuación, **añade** los atributos `textSize` y `textColor`:

```xml
        android:textSize="RANDOMNUMBER22-30ENDsp"
        android:textColor="#0RANDOMNUMBER1-9END0RANDOMNUMBER2-9END01"
```

Recuerda que para el tamaño de texto debemos usar las unidades `sp`, y para el color indicaremos un [código de color HTML](https://html-color.codes/).

### `onCreate`

Nuestras modificaciones en `activity_monsters.xml` no servirán para nada si no tenemos el código de inicialización apropiado en `MonstersActivity.java`.

**Abre** `MonstersActivity.java`, y, dentro de la clase, **escribe** `onCrea`... ¡Verás que Android Studio autocompleta!

```java
public class MonstersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
```

Luego, **añade** `setContentView` para _enlazar_ nuestro XML a la actividad. **Así**:

```java
public class MonstersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monsters);
    }
}
```

Prueba ahora el resultado. ¿Qué tal se ve?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

