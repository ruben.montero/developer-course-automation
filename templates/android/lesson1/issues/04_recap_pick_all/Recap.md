¡Trabajo terminado!

## :trophy: ¡Felicidades!

Has realizado las tareas del _sprint_ y has validado tu trabajo con los _tests_. ¡Enhorabuena!

<img src="android-introduction/docs/sunset.jpg" width=500/>

Has...

* Usado `Button`, `TextView` e `ImageView`
* Posicionado elementos con _constraints_ de forma relativa a `parent` o a otros elementos
* Distinguido entre `wrap_content`, `match_parent` y `0dp`
* Comprendido que las actividades en Android se basan en una clase Java, un archivo de interfaz XML, y están declaradas en `AndroidManifest.xml`
* Jugado con algún método `onClick` y usado `Toast`
* Comprendido el ciclo de vida de las actividades y empleado algunos métodos (`onCreate`, `onStop`,...)
* Iniciado otra actividad con `startActivity`
* Usado una `BottomNavigationView`
* Respondido _manualmente_ (`setOnItemSelectedListener`) a los _clicks_ en la `BottomNavigationView`
* Creado `Fragment`s
* Reemplazado dichos `Fragment`s en un contenedor
* Distinguido posibles valores de `scaleType` en un `ImageView`
