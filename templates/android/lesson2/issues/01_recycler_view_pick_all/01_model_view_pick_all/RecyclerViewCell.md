Celda para nuestro RecyclerView (XML)

## :books: Resumen

* Añadiremos un nuevo archivo `recycler_view_cell.xml`

## Descripción

Aunque sabemos que hay _cinco_ clips que vienen como respuesta a la petición http://raspi:8000/clips, la idea es que el servidor pueda responder _lo que le dé la gana_ (una lista de 10, 20, 100 clips...) y nuestra _app_ lo pinte correctamente. Es decir, nuestro `<RecyclerView>` muestra un número _indefinido_ (en tiempo de compilación) de elementos.

Por ello, debemos añadir un único archivo de _layout_ (XML) que representa una _celda_. Más adelante, será trabajo del _adaptador_ encargarse de decidir _cuántas_ celdas se pintan y con _qué_ contenido.

En esta tarea, sólo definiremos la _maqueta_ (_layout_ XML) de una celda:

## :package: La tarea

En la jerarquía de archivos, desde _res_ > _layout_ **haz** _click_ derecho > _New_ > _Layout Resource File_. **Dale** el nombre `recycler_view_cell` y **haz** _click_ en OK, manteniendo el `ConstraintLayout` como elemento raíz:

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

</androidx.constraintlayout.widget.ConstraintLayout>
```

**Modifica** `layout_height` en `recycler_view_cell.xml` para que valga `wrap_content`:

```diff
 <?xml version="1.0" encoding="utf-8"?>
 <androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
-   android:layout_height="match_parent">
+   android:layout_height="wrap_content">
+
</androidx.constraintlayout.widget.ConstraintLayout>
```

_(Múltiples de estos `recycler_view_cell.xml` van a estar dentro de nuestro `<RecyclerView>`, que, recordemos, ocupa toda la pantalla. Por lo tanto, ¡no podemos dejar `match_parent` como altura! Si lo hacemos, cada celda llenará todo el alto de la pantalla)._

Ahora, dentro de la celda, vamos a mostrar únicamente un _texto_. Será el _título_ de un _clip_ de vídeo devuelto por el API REST.

### Añadiendo el contenido

Dentro del `<ConstraintLayout>`, **añade** un `<TextView>`. **Dale** un `android:id` libremente (el que quieras).

**Posiciónalo** como quieras dentro del `<ConstraintLayout>`, con los márgenes que consideres oportunos.

Para terminar, en dicho `<TextView>`, **indica** un tamaño de texto: `RANDOMNUMBER20-30ENDsp`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
