Modelo

## :books: Resumen

* Crearemos una clase `Clip` que representa un objeto de la lista JSON enviada por el servidor
* Crearemos una clase `ClipsList`, que contiene varios `Clip` y representa la lista entera
* Declararemos un nuevo atributo de tipo `ClipsList` en nuestra `MainActivity`
* Lo inicializaremos con la respuesta del servidor

## Descripción

Recordemos que los datos que vienen de:

* http://raspi:8000/clips

...son:

```json
[
  {
    "id": 1,
    "title": "Automatic Dialer (Part II)",
    "videoUrl": "https://raw.githubusercontent.com/rubenmv0/fp/main/simpsons/automatic_dialer_part_ii.mp4"
  },
  {
    "id": 2,
    "title": "Desparasitándome",
    "videoUrl": "https://raw.githubusercontent.com/rubenmv0/fp/main/simpsons/desparasitandome.mp4"
  },
  {
    "id": 3,
    "title": "Glasses",
    "videoUrl": "https://raw.githubusercontent.com/rubenmv0/fp/main/simpsons/glasses.mp4"
  },
  {
    "id": 4,
    "title": "Marcador Automático",
    "videoUrl": "https://raw.githubusercontent.com/rubenmv0/fp/main/simpsons/marcador_automatico.mp4"
  },
  {
    "id": 5,
    "title": "Tarta",
    "videoUrl": "https://raw.githubusercontent.com/rubenmv0/fp/main/simpsons/tarta.mp4"
  }
]
```

Consisten en una lista de _clips_ que queremos mostrar al usuario.

```mermaid
graph LR
    A[App Android] -->|"Petición GET /clips"| B[Servidor HTTP]
    B -->|"Respuesta JSON"| A
    C[App Android] -->|"Convierte JSON a"|D["ClipsList"]
    D -->|"Se presenta al usuario visualmente con"|E[RecyclerView]
```

Vamos a crear clases Java que representan los datos del servidor. Serán nuestras clases _modelo_. En concreto, trabajaremos con:

```mermaid
graph LR
    C[App Android] -->|"Convierte JSON a"|D["ClipsList"]
```

## :package: La tarea

**Crea** una nueva clase Java. **Dale** el nombre `Clip`.

Dentro de `Clip.java`, **añade** el siguiente código:

```java
public class Clip {
    private int id;
    private String RANDOMEXPRESSIONtitle|videoTitleEND;
    private String RANDOMEXPRESSIONvideoUrl|urlVideoEND;
    
    public Clip(JSONObject json) throws JSONException {
        this.id = json.getInt("id");
        this.RANDOMEXPRESSIONtitle|videoTitleEND = json.getString("title");
        this.RANDOMEXPRESSIONvideoUrl|urlVideoEND = json.getString("videoUrl");
    }
}
```

_(Como puedes ver, hay tres atributos que son los datos de un clip, y además, hemos añadido un método constructor especial que sirve para construir una instancia de `Clip` a partir de un `JSONObject`)._

Para terminar con esta clase, **añade** los métodos _**getters**_ de los tres atributos, con sus nombres estándar. Puedes autogenerarlos desde la barra superior de Android Studio con _Code_ > _Generate_ > _Getters and Setters_.

Luego, **crea** otra nueva clase Java. **Dale** el nombre `ClipsList`.

Dentro de `ClipsList.java`, **añade** el siguiente código:

```java
public class ClipsList {
    private List<Clip> clips;
    
    public ClipsList(JSONArray array) {
        clips = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject jsonElement = array.getJSONObject(i);
                Clip aClip = new Clip(jsonElement);
                clips.add(aClip);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
```

_(Como puedes observar, tiene un atributo `List<Clip>` que se inicializa a partir del contenido de un `JSONArray`, con el código del método constructor)._

Para terminar con esta clase, también **añade** un método _**getter**_ del único atributo que existe, con un nombre estándar (`getClips()`).

### Usando `ClipsList`

Estas clases que hemos creado (`ClipsList.java` y `Clip.java`) son _modelos_. Representan datos que vienen del servidor, y gracias a ellas, podemos desentendernos de _parsear_ el JSON del servidor y centrarnos en manejar los datos.

En `MainActivity.java`, **declara** un nuevo atributo de tipo `ClipsList`, y un _getter_ y un _setter_ como se indica a continuación:

```java
public class MainActivity extends AppCompatActivity {
    // ...
    private ClipsList clips;
    
    // ...
    
    public void setClips(ClipsList clips) {
        this.clips = clips;
    }

    public ClipsList getClipsForTest() {
        return clips;
    }
```

_(Como puedes ver, `getClipsForTest` no es un nombre estándar para un método getter. Este método sólo sirve para que funcione un test automático que verificarás al final del sprint)._

Para terminar la tarea, en el `onResponse` de la petición a http://raspi:8000/clips, **añade** la siguiente línea `setClips(new ClipsList(response));`:

```java
        // ...
        new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                progressBar.setVisibility(View.INVISIBLE);
                Snackbar.make(mainLayout, "RANDOMEXPRESSIONClips received|Clips obtained|List received|List obtainedEND", Snackbar.LENGTH_SHORT).show();
                
                // Parseamos la respuesta y la asignamos a nuestro atributo
                setClips(new ClipsList(response));
```

¡Listo! Aunque no hay cambios visuales en la aplicación, en esta tarea hemos conseguido almacenar como un atributo `ClipsList` los datos que vienen en la `response` del servidor.

### ¿Ha valido la pena?

Indudablemente. El [patrón de arquitectura](https://es.wikipedia.org/wiki/Patrones_de_arquitectura) [Modelo-Vista-Controlador](https://es.wikipedia.org/wiki/Modelo%E2%80%93vista%E2%80%93controlador) es de los más populares y nosotros estamos poniéndolo en práctica.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
