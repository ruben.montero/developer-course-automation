RecyclerView

## :books: Resumen

* Comprenderemos la idea esencial del componente visual `RecyclerView`
* Añadiremos un `<RecyclerView>` a `activity_main.xml`. De momento, no mostrará nada

## Descripción

Mostrar una _lista_ es relativamente común en la mayoría de aplicaciones: La lista de correos de la bandeja de entrada, lista de _posts_ de Facebook, lista de noticias...

Un componente esencial en Android es [RecyclerView](https://developer.android.com/guide/topics/ui/layout/recyclerview?hl=es-419). Análogo a [UITableView](https://developer.apple.com/documentation/uikit/uitableview) de iOS, se dedica a _reciclar_ las vistas asociadas a cada celda de la _lista_.

### [`RecyclerView`](https://developer.android.com/guide/topics/ui/layout/recyclerview?hl=es-419), ¿por qué?

Hoy en día existen 890 pokemon.

<img src="android-clips/android-frontend-clips/docs/bulbasaur.png" width=80/>

Si quisiéramos desarrollar una actividad Android que los mostrase a todos, podríamos emplear una implementación [demasiado sencilla](https://www.vogella.com/tutorials/AndroidListView/article.html#listview_listviewexample) del primitivo componente [`ListView`](https://developer.android.com/reference/android/widget/ListView) en cuyo caso almacenaríamos en memoria RAM todos los datos.

Suponiendo que mostramos un PNG de 1024x1024 (~500Kb) por cada uno, necesitaremos:

<div align="center">500 Kb * 890 = 445 000 Kb = 434,57 Mb</div>

Esto es...

...más del doble de memoria RAM que tenía [el primer dispositivo Android](https://wwwhatsnew.com/2018/11/28/cual-fue-el-primer-smartphone-android-y-que-tenia-para-ofrecer/). ¡Demasiado!

`RecyclerView` reduce el uso de memoria RAM _reutilizando_ (reciclando) las _celdas_ visibles en pantalla.

Por ejemplo, si sólo caben 4 celdas en pantalla:

<img src="android-clips/android-frontend-clips/docs/recycler_view.png" width=200/>

...la memoria RAM necesaria para tener las imágenes será como mucho de:

<div align="center">500 Kb * 4 = 2 000 Kb = 1,95 Mb</div>

### ¡Vendido! Quiero un `RecyclerView`

Implementarlo llevará ciertos pasos:

1. Añadir un `<RecyclerView>` al XML de la actividad.
2. Añadir un nuevo fichero XML que represente una _celda_.
3. Añadir una clase Java vinculada al XML de la celda. Esta clase Java representa una _celda_, pero en código Java. Hereda de `ViewHolder`.
4. Añadir una clase _adaptador_. Esta clase heredará de `RecyclerView.Adapter`.

Para esta tarea, sólo añadiremos `<RecyclerView>` al XML.

## :package: La tarea

En `activity_main.xml`, **escribe** una nueva etiqueta `<RecyclerV`, y **dale** a ENTER para que Android Studio autocomplete:

```xml
    <androidx.recyclerview.widget.RecyclerView
        android:layout_width=""
        android:layout_height=""
```

**Añade** al `<androidx.recyclerview.widget.RecyclerView>` los atributos necesarios para que esté ajustado al contenedor, de tal forma que ocupe toda la pantalla. Puedes usar:

* `match_parent` en `layout_width` y `layout_height`
* `0dp` en `layout_width` y `layout_height`, y _constraints_ `Start_toStart`, `End_toEnd`, `Top_toTop` y `Bottom_toBottom`

...lo que prefieras.

Para terminar, **añade** un `android:id` al `<RecyclerView>`. El que quieras. Lo usaremos más adelante.

_(No te preocupes porque teóricamente el `<RecyclerView>` y el `<ProgressBar>` se solapen. Ambas vistas no estarán visibles simultáneamente)._

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
