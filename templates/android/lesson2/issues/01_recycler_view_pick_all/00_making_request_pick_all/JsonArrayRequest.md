Una petición de una lista

## :books: Resumen

* Lanzaremos una petición que solicita un _array_ JSON
* Mostraremos un _SnackBar_ si es exitosa

## Descripción

Nuestra aplicación lanza una petición de red que obtiene una respuesta sencilla.

```json
{
  "status": "Server is healthy!"
}
```

Ahora vamos a lanzar una petición que obtendrá datos más elaborados. Un _array_:

```json
[
  {
    "id": 1,
    "title": "Automatic Dialer (Part II)",
    "videoUrl": "https://raw.githubusercontent.com/rubenmv0/fp/main/simpsons/automatic_dialer_part_ii.mp4"
  },
  {
    "id": 2,
    "title": "Desparasitándome",
    "videoUrl": "https://raw.githubusercontent.com/rubenmv0/fp/main/simpsons/desparasitandome.mp4"
  },
  {
    "id": 3,
    "title": "Glasses",
    "videoUrl": "https://raw.githubusercontent.com/rubenmv0/fp/main/simpsons/glasses.mp4"
  },
  {
    "id": 4,
    "title": "Marcador Automático",
    "videoUrl": "https://raw.githubusercontent.com/rubenmv0/fp/main/simpsons/marcador_automatico.mp4"
  },
  {
    "id": 5,
    "title": "Tarta",
    "videoUrl": "https://raw.githubusercontent.com/rubenmv0/fp/main/simpsons/tarta.mp4"
  }
]
```

El objetivo final es _presentar_ estos datos al usuario.

```mermaid
graph LR
    A[App Android] -->|"Petición GET /clips"| B[Servidor HTTP]
    B -->|"Respuesta JSON"| A
    C[App Android] -->|"Convierte JSON a"|D["ClipsList"]
    D -->|"Se presenta al usuario visualmente con"|E[RecyclerView]
```

En esta tarea, sólo vamos a preocuparnos de mandar la petición HTTP.

## :package: La tarea

Para empezar, en `MainActivity.java`, **crea** un método privado (dentro de la clase):

```java
    private void RANDOMEXPRESSIONrequestClips|getClips|requestClipList|getClipListEND() {
    
    }
```

...y luego, **invócalo** desde `onCreate`, como se indica a continuación:

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.queue = Volley.newRequestQueue(context);

        // Solicitamos una lista de clips
        RANDOMEXPRESSIONrequestClips|getClips|requestClipList|getClipListEND();
        
        // Aquí lanzaremos una petición HTTP
        // ...
```

_(De esta manera, `RANDOMEXPRESSIONrequestClips|getClips|requestClipList|getClipListEND` encapsulará la lógica para enviar la nueva petición HTTP y no haremos crecer excesivamente nuestro `onCreate`)._

A continuación, dentro de `RANDOMEXPRESSIONrequestClips|getClips|requestClipList|getClipListEND`, **instancia** un nuevo objeto `JsonArrayRequest`. Los parámetros enviados al método constructor serán:

* `Request.Method.GET`
* `Server.name + "/clips"`
* `null`
* `new Response.Listener{...}`
* `new Response.ErrorListener{...}`

Luego, **añade** dicha petición a la cola de red con `this.queue.add( ... );`

### [SnackBar](https://developer.android.com/reference/com/google/android/material/snackbar/Snackbar)

Para terminar la tarea, vamos a mostrar un `SnackBar` si la petición a `/clips` se ha completado correctamente. Es un componente _similar_ al `Toast`, con estas diferencias:

* Puede permitir al usuario _clickar_ para llevar a cabo alguna acción.
* Cuando lo construimos, debemos especificar una `View`. Se pintará _debajo_ de dicha `View`.

Para mostrar un `SnackBar`, como primer paso, **añade** `id` al `ConstraintLayout` en `activity_main.xml`:

```diff
 <?xml version="1.0" encoding="utf-8"?>
 <androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
+   android:id="@+id/main_layout"
    tools:context=".MainActivity">
```

...y luego, desde `MainActivity.java`, **añade** un atributo privado (`ConstraintLayout`) e **inicialízalo** desde `onCreate` empleando `findViewById`. Quedará **así**:

```java
public class MainActivity extends AppCompatActivity {
    private RequestQueue queue;
    private Context context = this;
    private ConstraintLayout mainLayout; // Añadimos un nuevo atributo

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.queue = Volley.newRequestQueue(context);
        this.mainLayout = findViewById(R.id.main_layout); // Lo asignamos con findViewById
        
        // ...
```

_(En general es buena práctica almacenar los componentes como atributos privados e inicializarlos sólo una vez en `onCreate`. Esto se debe a que `findViewById` se considera una operación costosa)._

Finalmente, otra vez en `MainActivity.java`, **añade** esta línea a tu nuevo `onResponse` (dentro de `RANDOMEXPRESSIONrequestClips|getClips|requestClipList|getClipListEND`):

```java
    Snackbar.make(mainLayout, "RANDOMEXPRESSIONClips received|Clips obtained|List received|List obtainedEND", Snackbar.LENGTH_LONG).show();
```

¡Listo! Si pruebas la aplicación, deberías ver un `SnackBar` con el mensaje `"RANDOMEXPRESSIONClips received|Clips obtained|List received|List obtainedEND"` que demuestra que la petición a http://raspi:8000/clips se ha completado con éxito.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
