Una petición de una lista puede fallar

## :books: Resumen

* Mostraremos un _SnackBar_ si la petición de `/clips` falla

## Descripción

Ya sabes que nuestra aplicación manda una petición HTTP a:

* http://raspi:8000/clips

...y recibe los datos. ¡Pero las cosas pueden ir mal!

¿Y si recibimos una respuesta inesperada, como un código `RANDOMEXPRESSION404|401|403|405|500|502END`?

Debemos controlar, de forma similar a como hemos hecho en tareas anteriores.

## :package: La tarea

* Si hay un problema estableciendo la conexión al solicitar `/clips`, **muestra** un `SnackBar` con el mensaje `"RANDOMEXPRESSIONCould not establish connection|Could not reach server|Unable to establish connection|Unable to reach server|Connection could not be established|Server could not be reached|Connection could not be established!|Server could not be reached!|There are problems with the connection|Problems with the connection!END"`.
* Si hay un código de error de servidor `XXX`, **muestra** en un `SnackBar` el mensaje: `"RANDOMEXPRESSIONClips answer:|Server status:|Server status is|Server replied with|Server responded with|Server response|Server KO:END XXX"`, donde `XXX` es el código de error.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
