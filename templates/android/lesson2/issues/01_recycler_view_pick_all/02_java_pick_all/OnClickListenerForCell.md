Clicks en las celdas

## :books: Resumen

* Añadiremos un `OnClickListener` _anónimo_ a cada celda `ClipViewHolder`
* Añadiremos también un atributo privado de tipo `Clip`
* Cuando se _clique_ en una celda, se mostrará un _Toast_ con información del _Clip_ clicado

## Descripción

Nuestro `RecyclerView` sirve para mostrar información de una lista de _clips_ de vídeo, obtenida de un API REST.

¡Vamos a permitir al usuario interaccionar con la lista!

Más concretamente, cuando el usuario _clique_ una celda, se mostrará un `Toast` con el `id` del `Clip` correspondiente a esa celda.

Ese `id`, en `ClipViewHolder.java`, sólo está disponible mediante el `Clip clip` del método:

```java
    public void RANDOMEXPRESSIONshowData|showClip|bindData|bindClipEND(Clip clip) {
```

...pero, como verás a continuación, ¡necesitaremos almacenar el `Clip` y usarlo desde el método constructor!

### Gráficamente

```mermaid
sequenceDiagram

participant Created as Constructor
participant Shown as bindData
participant Attribute as private Clip clip;
participant Clicked as onClick


Created->>Clicked: Establece OnClickListener
Shown->>Attribute: Almacena
Clicked->>Attribute: Accede a clip.getId()
Attribute-->>Clicked: "Valor del ID"
Clicked->>Clicked: Muestra un Toast con el "ID" adecuado
```

## :package: La tarea

Dentro de la clase `ClipViewHolder.java` tenemos un constructor que admite un parámetro tipo `View`. Es la _vista_ de la celda, así que podemos usarla a nuestro antojo. **Establece** en dicho `itemView` un `OnClickListener` _anónimo_ **así**: 

```java
    // Método constructor
    public ClipViewHolder(@NonNull View itemView) {
        // ...
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
```

Luego, **añade** a `ClipViewHolder.java` un atributo privado tipo `Clip`:

```java
public class ClipViewHolder extends RecyclerView.ViewHolder {
    // Nuevo atributo
    private Clip clip;
    // ...
```

...e **inicialízalo** añadiendo esta línea al método `RANDOMEXPRESSIONshowData|showClip|bindData|bindClipEND`:

```java
    public void RANDOMEXPRESSIONshowData|showClip|bindData|bindClipEND(Clip clip) {
        // ...
        this.clip = clip;
```

Regresando al `OnClickListener` _anónimo_, **añade** las siguientes líneas para obtener el `id` del _clip_ y conseguir un `context` que servirá para mostrar un `Toast`:

```diff
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
+               int clipId = clip.getId();
+               Context context = view.getContext();
+
+               // Mostramos un Toast para completar la tarea
+               // ...

            }
        });
```

Finalmente, para completar el método `onClick` y la tarea **muestra** un `Toast` con el mensaje `"RANDOMEXPRESSIONClicked on cell with clipId:|Touched cell with clipId:|Tap on cell with clipId:|Clicked on ViewHolder with clipId:|Touched ViewHolder with clipId:|Tap on ViewHolder with clipId:END " + clipId"`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
