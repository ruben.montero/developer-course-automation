Celda para nuestro RecyclerView (Java)

## :books: Resumen

* Añadiremos un nuevo archivo `ClipViewHolder.java`

## Descripción

Estamos acostumbrados a escribir una clase Java cada vez que añadimos un fichero de interfaz XML.

* Cuando creamos una actividad, el XML lleva asociado una `Activity` en Java
* Cuando creamos un _fragment_, el XML lleva asociado un `Fragment` en Java

En esta ocasión, crearemos un `ViewHolder`. O sea, la clase Java para cada _celda_ de nuestro `RecyclerView`.

## :package: La tarea

En la jerarquía de archivos, desde _java_ > _com.afundacion.fp.clips_ **haz** _click_ derecho > _New_ > _Java Class_. **Dale** el nombre `ClipViewHolder`:

```java
package com.afundacion.fp.clips;

public class ClipViewHolder {
}
```

Como primer paso **indica** que `ClipViewHolder` _hereda_ de `ViewHolder`. Concretamente, de `RecyclerView.ViewHolder`. **Así**:

```java
package com.afundacion.fp.clips;

import androidx.recyclerview.widget.RecyclerView;

public class ClipViewHolder extends RecyclerView.ViewHolder {
}
```

Verás que aparece el error:

> There is no default constructor available in 'androidx.recyclerview.widget.RecyclerView.ViewHolder'

Esto se debe a que no está permitido _instanciar_ una subclase de `ViewHolder` _directamente_. Es decir, por ser una subclase de `ViewHolder`, no podríamos hacer `v = new ClipViewHolder();`. ¡El constructor _vacío_ ya no existe!

Estamos, por ello, obligados a crear un _constructor_ que recibe una _vista_ (`View`).

**Usa** la ayuda de Android Studio (bombillita roja) y **selecciona** _Create constructor matching super_ para llegar a:

```java
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ClipViewHolder extends RecyclerView.ViewHolder {
    public ClipViewHolder(@NonNull View itemView) {
        super(itemView);
    }
}
```

Luego, **añade** a `ClipViewHolder.java` un atributo privado de tipo `TextView`, llamado `RANDOMEXPRESSIONclipTitle|cellTextView|cellTitle|clipTextView|textViewEND`. **Inicialízalo** desde el método constructor mediante `findViewById`, como se indica a continuación:

```java
public class ClipViewHolder extends RecyclerView.ViewHolder {
    private TextView RANDOMEXPRESSIONclipTitle|cellTextView|cellTitle|clipTextView|textViewEND;
    
    public ClipViewHolder(@NonNull View itemView) {
        super(itemView);
        RANDOMEXPRESSIONclipTitle|cellTextView|cellTitle|clipTextView|textViewEND = itemView.findViewById( /* El ID que le diste a tu TextView en recycler_view_cell.xml */);
    }
}
```

_(Como puedes ver, estamos invocando `findViewById` sobre `itemView`. Este objeto será creado dentro del adaptador en la siguiente tarea, donde uniremos todas las piezas)._

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
