¿Quién aparece en la pantalla?

## :books: Resumen

* Añadiremos `Character.java` y `CharactersList.java`, _modelos_ para una nueva petición
* Añadiremos `CharacterViewHolder.java` y `character_recycler_cell.xml`, para un nuevo tipo de celda
* Añadiremos un `CharactersAdapter.java`, nuevo _adaptador_ para un nuevo _RecyclerView_
* Aparecerá sobre el vídeo cuando el usuario haga _click_, para mostrar los personajes que hay en la pantalla en ese instante

## Descripción

En [Amazon Prime Video](https://www.primevideo.com/?_encoding=UTF8&language=es_ES) y otras plataformas de _streaming_, puedes obtener información de qué personajes aparecen en la pantalla para un momento dado de un vídeo.

Nosotros no vamos a ser menos.

Por suerte, nuestro API REST cuenta con una petición HTTP exclusivamente dedicada a eso. Sólo hay que enviar el `id` del _clip_ de vídeo, y el _tiempo de reproducción_ (en milisegundos).

Por ejemplo:

* http://raspi:8000/clips/5/appearances?milliseconds=16000

_(En el clip `5`, a los `16` segundos, aparecen los siguientes personajes):_

```json
[
  {
    "name": "Lisa",
    "surname": "Simpson",
    "description": "Benevolente y educada hija de la familia Simpson",
    "imageUrl": "https://raw.githubusercontent.com/rubenmv0/fp/main/simpsons/lisa.png"
  },
  {
    "name": "Bart",
    "surname": "Simpson",
    "description": "Primogénito de la familia Simpson, siempre buscando problemas",
    "imageUrl": "https://raw.githubusercontent.com/rubenmv0/fp/main/simpsons/bart.png"
  }
]
```

## :package: La tarea

### Modelos: `Character.java` y `CharactersList.java`

**Añade** una nueva clase Java, `Character.java`. **Escribe** estos atributos:

```java
    private String name;
    private String RANDOMEXPRESSIONsurname|lastNameEND;
    private String RANDOMEXPRESSIONdescription|characterDescriptionEND;
    private String RANDOMEXPRESSIONimageUrl|urlImageEND;
```

También, **implementa** los _getters_ con su nomenclatura por defecto. Luego, **implementa** un método constructor que reciba un `JSONObject` por parámetro e inicialice los atributos de acuerdo a la respuesta de expuesta arriba (claves `"name"`, `"surname"`, `"description"` y `"imageUrl"`), de forma parecida a como has hecho en `Clip.java`.


**Añade** otra nueva clase Java, `CharactersList.java`. **Escribe** este atributo:

```java
    private List<Character> characters;
```

También, **implementa** el _getter_ con su nomenclatura por defecto. Luego, **implementa** un método constructor que reciba un `JSONArray` por parámetro e inicialice el atributo de acuerdo a la respuesta de expuesta arriba (esperando una lista de `Character`), de forma parecida a como has hecho en `ClipsList.java`.

### Celda: XML y Java

**Añade** un nuevo archivo de interfaz `character_recycler_cell.xml`. **Haz** que el elemento raíz sea un `<ConstraintLayout>`, con `layout_width="match_parent"` y `layout_height="wrap_content"`. _Dentro_ del `<ConstraintLayout>`, **añade** los dos elementos que se indican a continuación:

* Un `<ImageView>` con _id_, _anchura_, _altura_ y _descripción de contenido_ como se indica a continuación:

```xml
    <ImageView
        android:id="@+id/image_view_character"
        android:layout_width="RANDOMNUMBER50-60ENDdp"
        android:layout_height="RANDOMNUMBER50-60ENDdp"
        android:contentDescription="RANDOMEXPRESSIONPersonaje en pantalla|Imagen de personaje|Personaje en clipEND"
        />
```

...y _además_, tendrá `scaleType="centerInside"` y _constraints_ para pegarlo a _izquierda_, _derecha_ y _arriba_ de su contenedor (¡pero no abajo!)

* Un `<TextView>` con _id_, _anchura_, _altura_, _color de texto_ y _tamaño de texto_ como se indica a continuación:

```xml
    <TextView
        android:id="@+id/text_view_character"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:textColor="#fff"
        android:textSize="RANDOMNUMBER10-18ENDsp"
        />
```

...y _además_, tendrá _constraints_ para pegarlo a _izquierda_, _derecha_ y _abajo_ de su contenedor. Por _arriba_ estará pegado a la parte _inferior_ de la imagen[^1].

Después, **añade** una nueva clase Java `CharacterViewHolder.java`. En su contenido, **incluye** el siguiente código:

```java
public class CharacterViewHolder extends RecyclerView.ViewHolder {
    private TextView RANDOMEXPRESSIONtextView|textName|nameTextViewEND;
    private ImageView RANDOMEXPRESSIONimageView|imageCharacter|characterImageViewEND;


    public CharacterViewHolder(@NonNull View itemView) {
        super(itemView);
        RANDOMEXPRESSIONtextView|textName|nameTextViewEND = itemView.findViewById(R.id.text_view_character);
        RANDOMEXPRESSIONimageView|imageCharacter|characterImageViewEND = itemView.findViewById(R.id.image_view_character);
    }

    public void showData(Character character) {
        this.RANDOMEXPRESSIONtextView|textName|nameTextViewEND.setText(character.getName());
        Util.downloadBitmapToImageView(character.getImageUrl(), this.RANDOMEXPRESSIONimageView|imageCharacter|characterImageViewEND);
    }
}
```

...y **añade** los `import` necesarios.

_(Como puedes observar, esta clase Java es análoga a ClipViewHolder.java)._

### Adaptador

**Añade** una nueva clase Java `CharactersAdapter.java` que comience de esta manera:

```java
public class CharactersAdapter extends RecyclerView.Adapter<CharacterViewHolder> {
    private CharactersList RANDOMEXPRESSIONcharactersList|characters|charactersToShow|charactersToDisplayEND;

    public CharactersAdapter(CharactersList charactersList) {
        this.RANDOMEXPRESSIONcharactersList|characters|charactersToShow|charactersToDisplayEND = charactersList;
    }
    
    
    // Completa aquí los métodos de RecyclerView.Adapter
    /* ... */
```

...y **completa** su implementación. Será _análoga_ a `ClipsAdapter.java` y contendrá `onCreateViewHolder`, `onBindViewHolder` y `getItemCount`.

### Completar la _activity_

En `activity_video.xml` **añade** una etiqueta `<RecyclerView>` a continuación del `<VideoView>` que ya existe. **Así**:

```xml
    <VideoView
        android:id="@+id/video_view"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>

    <!-- Nuevo RecyclerView -->
    <!-- Solapa (por encima) al VideoView parcialmente. Es lo que queremos -->
    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recycler_view_characters"
        android:layout_width="RANDOMNUMBER70-81ENDdp"
        android:layout_height="0dp"
        app:layout_constraintRANDOMEXPRESSIONStart|EndEND_toRANDOMEXPRESSIONStart|EndENDOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintBottom_toBottomOf="parent"/>
```

Y para dar uso a lo que hemos creado hasta ahora, **abre** `VideoActivity.java`.

En `VideoActivity.java` **añade** los nuevos atributos `recyclerView`, `queue` y `charactersOnScreen` que se muestran a continuación:

```java
public class VideoActivity extends AppCompatActivity {
    /* ... */
    private RecyclerView recyclerView;
    private RequestQueue queue;
    private CharactersList charactersOnScreen;
```

En el método `onCreate`, **inicializa** `recyclerView` y `queue` como cabe esperar:

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        
        queue = Volley.newRequestQueue(this); // Nueva cola. Idealmente, en una app, se comparte una sola RequestQueue
        recyclerView = findViewById(R.id.recycler_view_characters); // Buscamos por ID el recyclerView recién añadido
        
        // ...
```

Ahora, vamos a hacer que cuando el usuario pulse en el elemento principal de la pantall (`videoView`), la _app_ mande una petición al API REST para consultar los personajes en pantalla.

**Completa** el código de `onCreate` como se muestra a continuación:

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        
        // ...
        
        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // clipId fue inicializado más arriba, a traves los 'Extras'
                // .getCurrentPosition() devuelve el milisegundo actual del vídeo
                sendAppearancesRequest(clipId, videoView.getCurrentPosition());
            }
        });
    }
```

...y **añade** el método privado `sendAppearancesRequest`:

```java
    private void sendAppearancesRequest(int clipId, int milliseconds) {
    
        /* ... */

    }
```

_Dentro_ de este método:

* (1) **Crea** una nueva instancia de `JsonArrayRequest`
* (2) Para ello, **indica** el método HTTP `Request.Method.GET`
* (3) También, **indica** la URL apropiada que fue especificada al inicio de la tarea, en función de `clipId` y `milliseconds`. (_Recuerda_ usar `Server.name` como _host_).
* (4) Si la petición es exitosa, en `Response.Listener` **_parsea_** la respuesta del servidor y usa un método _setter_ para almacenarla, _análogamente_ a `MainActivity .java` (¡el método `setCharactersOnScreen` lo escribiremos a continuación!):

```java
        new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                CharactersList parsedServerResponse = new CharactersList(response);
                setCharactersOnScreen(parsedServerResponse);
            }
        },
```

* (5) Si la petición falla, en `Response.ErrorListener` **muestra** un `Toast` con el mensaje que tú quieras.
* (6) Al final, tras instanciar tu `JsonArrayRequest`, **añade** dicha petición a la cola de red con `queue.add( /*...*/ );`.

Para finalizar la tarea, **escribe** el método _setter_ `setCharactersOnScreen`, **así**:

```java
    private void setCharactersOnScreen(CharactersList charactersOnScreen) {
        this.charactersOnScreen = charactersOnScreen;
        CharactersAdapter myAdapter = new CharactersAdapter(this.charactersOnScreen);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
```

_(Funciona como el `setClips` de `MainActivity.java`. Esta vez puede ser `private`. No hace falta crear un getter)._

Si has seguido todos los pasos de esta importante tarea, **¡enhorabuena!** Has implementado una importante funcionalidad haciendo uso de todo lo visto hasta ahora.

Adelante, lanza la _app_ y después de esperar a que cargue algún vídeo, toca la pantalla. ¿Qué sucede?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: La _constraint_ que necesitarás será: `app:layout_constraintTop_toBottomOf="@id/image_view_character"`
