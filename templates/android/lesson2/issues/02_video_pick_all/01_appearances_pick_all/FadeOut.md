Fade Out

## :books: Resumen

* En `VideoActivity.java`, añadiremos un `ObjectAnimator` que hará que la lista de personajes desaparezca con una animación
* Añadiremos un atributo _booleano_ `isAnimating` a la actividad, para evitar efectos indeseados en la animación

## Descripción

Nuestra funcionalidad de _lista de personajes en pantalla_ es muy buena. ¡Vamos a mejorarla!

Ahora mismo, cuando pulsas en la pantalla, los personajes que aparecen permanecen ahí. Pero el vídeo avanza, así que deja de ser coherente...

Vamos a hacer que la lista sólo permanezca en la pantalla 3 segundos, y luego, desaparezca progresivamente durante 0,5 segundos.

Hay múltiples formas de llevar a cabo animaciones en Android. Nosotros vamos a usar el sencillo [`ObjectAnimator`](https://developer.android.com/reference/android/animation/ObjectAnimator). Sirve para añadir animaciones que modifican valores de las propiedades.

## :package: La tarea

En `VideoActivity.java`, **añade** este código a `setCharactersOnScreen`:

```java
    private void setCharactersOnScreen(CharactersList charactersOnScreen) {
        this.charactersOnScreen = charactersOnScreen;
        CharactersAdapter myAdapter = new CharactersAdapter(this.charactersOnScreen);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Nuevo código
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(recyclerView, "alpha", 1f, 0f);
        fadeOut.setDuration(500);
        fadeOut.setStartDelay(3000);
        fadeOut.start();
    }
```

¡Pruébalo! Verás que funciona bien... ¡sólo la primera vez!

Se debe a que el valor de transparencia (_alpha_) se transforma en `0`... ¡y ahí se queda! No vuelve a ser `1` hasta que el _delay_ inicial de 3 segundos transcurre de nuevo.

Para asegurarnos que durante dicho _delay_ el valor de _alpha_ sea `1` (y por lo tanto, se vean los personajes), **establece** su valor manualmente al inicio:

```diff
+       recyclerView.setAlpha(1);
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(recyclerView, "alpha", 1f, 0f);
        fadeOut.setDuration(500);
        fadeOut.setStartDelay(3000);
        fadeOut.start();
```

¡Funciona mejor! Aunque si pulsas repetidamente en la pantalla, el código se ejecuta una o varias veces _mientras_ la animación toma lugar... ¡Y puede haber efectos impredecibles!

Para enfrentarnos al problema, podemos _deshabilitar_ la animación si _ya está en curso_. De hecho, incluso nos ahorraremos la petición HTTP.

**Añade** un atributo _booleano_ a la clase:

```java
public class VideoActivity extends AppCompatActivity {
    /* ... */
    private boolean isAnimating = false;
    
    // ...
```

Luego, **establécelo** a `true` cuando la animación comienza, y a `false` cuando la animación termina. **Así**:

```diff
        recyclerView.setAlpha(1);
+       isAnimating = true; // Ponemos el booleano a true
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(recyclerView, "alpha", 1f, 0f);
        fadeOut.setDuration(500);
        fadeOut.setStartDelay(3000);
        fadeOut.start();
        // Debemos usar un listener para saber cuándo termina la animación
        fadeOut.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
+               // Y aquí, el booleano será false
+               isAnimating = false;
            }
        });
```

Con esto, tienes una variable `isAnimating` que vale `true` mientras el `RecyclerView` está en pantalla, y, cuando la animación termina (`alpha=0`), se vuelve `false`.

**Añade** la siguiente condición de guarda `if (isAnimating) { return; }` a `sendAppearancesRequest`:

```java
    private void sendAppearancesRequest(int clipId, int milliseconds) {
        if (isAnimating) { return; }

        JsonArrayRequest /* ... */
```

¡Mejora completada! Ahora _no_ se envía la petición HTTP si _ya se están mostrando_ los personajes en la pantalla.

Verifica cómo funciona todo. ¿Estás satisfecho con el resultado?

Has desarrollado `Clips`, una _app_ que pone en práctica los principios fundamentales de aplicaciones móviles multiplataforma. **¡Enhorabuena!**

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
