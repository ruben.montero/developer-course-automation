Actividad que muestra vídeos (pero todavía no)

## :books: Resumen

* Añadiremos una nueva actividad `VideoActivity`. De momento, sólo recibirá dos datos: `id` y `url `del vídeo
* Los mostrará en un `Toast` (aún no reproducirá el vídeo)

## Descripción

Ya que podemos hacer _click_ en cada celda y mostrar información asociada, pasaremos dicha información a una nueva actividad.

Hasta ahora no hemos _pasado datos de una actividad a otra_. ¡Pero se puede hacer!

Basta con usar [`putExtra`](https://developer.android.com/reference/android/content/Intent#putExtra(java.lang.String,%20android.os.Bundle)) cuando lanzamos la actividad y [`getXXXExtra`](https://developer.android.com/reference/android/content/Intent#getExtras()) en la nueva actividad.

### Un detalle: Tipo de dato recuperado en la actividad _destino_

Mientras `putExtra` es el _mismo_ método independientemente del tipo de dato (`int`, `String`, `boolean`,...) que queramos pasar, `getXXXExtra` tiene nombres distintos.

Debemos saber _qué_ tipo de dato queremos recuperar y emplear el método apropiado[^1]:

* `getIntExtra`
* `getLongExtra`
* `getFloatExtra`
* `getDoubleExtra`
* `getStringExtra`
* `getStringArrayExtra`
* `getStringArrayListExtra`
* ...

### Otro detalle: Valores primitivos por defecto

Cuando usamos algún `getXXXExtra`, por ejemplo, `getIntExtra`, ponemos un _segundo_ parámetro que representa el _valor por defecto_. En este ejemplo es `-1`:

```java
// Si no se ha hecho `putExtra` de "NUMERO" desde la
// actividad origen, entonces aquí, en la actividad
// destino, el valor por defecto es -1
int elNumeroQueHanPasado = getIntExtra("NUMERO", -1);
```

### Otro detalle más: Claves

Para identificar los valores que están siendo _transmitidos_ de una actividad a otra se usan _claves_ de tipo `String`, como `"NUMERO"` en el ejemplo anterior.

Podemos elegir cualquier `String` arbitrariamente. Siempre que _coincidan_ en `.putExtra` y `.getXXXExtra`.

Si no coinciden... ¡La liamos! Por ello, es una buena práctica definir estas claves en un sólo sitio. Por ejemplo, como variables estáticas en la actividad[^2][^3]. Más adelante lo pondremos en práctica.

## :package: La tarea

**Crea** una nueva actividad con estas características:

* El archivo de interfaz se llamará `activity_video.xml` y sólo contendrá un `<ConstraintLayout/>` por defecto
* La clase Java será `VideoActivity.java`. Recuerda que debe heredar de `AppCompatActivity`
* ¡No olvides declarar la actividad en el _manifiesto_!

Luego, al final del `onClick` de `ClipViewHolder.java` **añade** estas líneas para empezar la nueva actividad:

```diff
            @Override
            public void onClick(View view) {
                int clipId = clip.getId();
                Context context = view.getContext();

                // Mostramos un Toast para completar la tarea
                // ...
                
+               // Y ahora... ¡Iniciamos la VideoActivity!
+               Intent intent = new Intent(context, VideoActivity.class);
+               context.startActivity(intent);
            }
```

A continuación, **añade** estas dos líneas entre medias de las dos anteriores. Sirven para indicar que queremos _transmitir_ dos datos a la nueva actividad. **Sustituye** coherentemente el comentario de la URL del vídeo por un valor correcto:

```java
                // Y ahora... ¡Iniciamos la VideoActivity!
                Intent intent = new Intent(context, VideoActivity.class);
                intent.putExtra("CLIP_ID", clipId);
                intent.putExtra("CLIP_URL", /* Aquí, la URL del vídeo. ¿De qué atributo de la clase la podemos sacar? */);
                context.startActivity(intent);
```

Luego, ve a la `VideoActivity.java` que creaste en un principio.

**Añade** a `onCreate` las líneas que se muestran a continuación:

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        
        Intent intent = getIntent(); // Con esto accedemos al Intent que se usó para iniciar la actividad
        int clipId = intent.getIntExtra("CLIP_ID", -1);
        String clipUrl = intent.getStringExtra("CLIP_URL");
        Toast.makeText(this, "RANDOMEXPRESSIONclipId:|videoId:|clip Id:|video Id:|clipID:|videoID:END " + clipId, Toast.LENGTH_LONG).show();
    }
```

```mermaid
graph LR
    A[MainActivity<br>.putExtra 'CLIP_ID'<br>.putExtra 'CLIP_URL'] --> |"inicia y pasa datos a"| B[VideoActivity<br>.getIntExtra 'CLIP_ID'<br>.getStringExtra 'CLIP_URL' ]
```

_(Recuerda que, originalmente, hemos obtenido estos datos del API REST)._

¡Enhorabuena! Has conseguido pasar a la `VideoActivity` dos datos que provienen de la celda `ClipViewHolder`, y, además, puedes verificar que el `id` se muestra en un `Toast`.

Para terminar, vamos a refactorizar el código para aplicar una buena práctica aconsejada al principio.

**Añade** estos atributos estáticos a `VideoActivity.java`:

```java
public class VideoActivity extends AppCompatActivity {
    public static final String INTENT_CLIP_ID = "CLIP_ID";
    public static final String INTENT_CLIP_URL = "CLIP_URL";
    // ...
```

Luego, **sustitúyelos** en `onCreate` de `VideoActivity.java`:

```diff
-        int clipId = intent.getIntExtra("CLIP_ID", -1);
-        String clipUrl = intent.getStringExtra("CLIP_URL");
+        int clipId = intent.getIntExtra(VideoActivity.INTENT_CLIP_ID, -1);
+        String clipUrl = intent.getStringExtra(VideoActivity.INTENT_CLIP_URL);
```

...y **sustitúyelos** también en `ClipViewHolder.java`:

```diff
-                intent.putExtra("CLIP_ID", clipId);
-                intent.putExtra("CLIP_URL", /* ... */);
+                intent.putExtra(VideoActivity.INTENT_CLIP_ID, clipId);
+                intent.putExtra(VideoActivity.INTENT_CLIP_URL, /* ... */);
```

Así, las _claves_ de los valores transmitidos están declaradas en un sólo sitio, y es más difícil que haya _bugs_ por errores al teclear.

¡Tarea terminada!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

-------------------------------------------

[^1]: Si queremos _transmitir_ una instancia de una _clase_ nuestra, deberá implementar la interfaz [`Serializable`](https://developer.android.com/reference/android/content/Intent#getSerializableExtra(java.lang.String,%20java.lang.Class%3CT%3E)).

[^2]: El modificador `final` en Java establece que una variable no puede _reescribirse_. Es por tanto, constante

[^3]: Suele usarse notación distinta a _lowerCamlCase_ cuando se definen **constantes**
