¡Mostrando vídeos!

## :books: Resumen

* Añadiremos un `VideoView` a nuestra actividad `VideoActivity.java`
* Con la URL de vídeo que recibimos en la tarea anterior, reproduciremos un vídeo

## Descripción

[`VideoView`](https://developer.android.com/reference/android/widget/VideoView) es una clase extremadamente útil en Android que permite reproducir un vídeo obtenido de una URL o de un _recurso_ propio de la _app_.

## :package: La tarea

**Añade** un `<VideoView>` a `activity_video.xml`:

```xml
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <VideoView
        android:id="@+id/video_view"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>

</androidx.constraintlayout.widget.ConstraintLayout>
```

_(Como ves, se usa `match_parent`. Esto deformará el vídeo para ajustarlo al contenedor. Nos vale, ya que esperamos que el vídeo tenga una relación de aspecto parecida a la pantalla. También es útil jugar con `wrap_content` y constraints según qué escenario)._

Luego, en `VideoActivity.java`, **añade** un atributo tipo `VideoView` e **inicialízalo** con `findViewById` en `onCreate`, como ya estamos acostumbrados:

```java
public class VideoActivity extends AppCompatActivity {
    public static final String INTENT_CLIP_ID = "CLIP_ID";
    public static final String INTENT_CLIP_URL = "CLIP_URL";

    private VideoView videoView; // Nuevo atributo

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        Intent intent = getIntent();
        int clipId = intent.getIntExtra(VideoActivity.INTENT_CLIP_ID, -1);
        String clipUrl = intent.getStringExtra(VideoActivity.INTENT_CLIP_URL);
        Toast.makeText(this, "RANDOMEXPRESSIONclipId:|videoId:|clip Id:|video Id:|clipID:|videoID:END " + clipId, Toast.LENGTH_LONG).show();
        
        // VideoView
        videoView = findViewById(R.id.video_view);
```

A continuación, ¡basta con invocar [`setVideoUri`](https://developer.android.com/reference/android/widget/VideoView#setVideoURI(android.net.Uri)) para reproducir un vídeo de Internet asociado a una URL! **Hazlo** así:

```java
        // VideoView
        videoView = findViewById(R.id.video_view);
        videoView.setVideoURI(Uri.parse(clipUrl));
        videoView.start();
```

Puedes lanzar la _app_ y verificar que cualquier vídeo se ve correctamente. Quizás la pantalla del emulador se quede en negro bastante rato (incluso 4-5 minutos) si el ancho de banda de la red es muy reducido para descargar el vídeo. Sé paciente. ¡Seguramente verás tu vídeo!

### Si giras la pantalla...

Es posible que el vídeo se vea demasiado _deformado_ al girar la pantalla.

Vamos a optar por _deshabilitar_ el cambio de orientación. Forzaremos a que la pantalla siempre se muestre _apaisada_ (vista `landscape`).

En el _manifiesto_, **añade** `screenOrientation` a la etiqueta de nuestra _activity_. **Así**:

```xml
        <activity
            android:name=".VideoActivity"
            android:screenOrientation="landscape" />
```

Esto _obligará_ a los usuarios a girar la pantalla cuando se inicie la actividad. ¡Pruébalo!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
