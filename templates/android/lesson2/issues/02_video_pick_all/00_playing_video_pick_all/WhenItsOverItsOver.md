Cuando termina, termina

## :books: Resumen

* Terminaremos la `VideoActivity` cuando el vídeo termina

## Descripción

Ahora mismo, nuestro _vídeo_ llega al final y se queda parado. Ese es el comportamiento por defecto.

Vamos a hacer que la _actividad termine_ (y, por tanto, regrese a la lista) cuando el vídeo llega al final.

Sólo necesitamos dos piezas:

### [`finish()`](https://developer.android.com/reference/android/app/Activity#finish())

Es un método de la clase `Activity`. Podemos invocarlo en cualquier sitio y terminará la actividad sobre la que se invoca. _Como_ si el usuario pulsase _Atrás_.

### [`OnCompletionListener`](https://developer.android.com/reference/android/widget/VideoView#setOnCompletionListener(android.media.MediaPlayer.OnCompletionListener))

Un _listener_ especial que podemos añadir a los `VideoView`. Responde a _que el vídeo termine de reproducirse_.

## :package: La tarea

En primer lugar, **añade** un atributo `Activity` dentro de `VideoActivity.java`:

```java

public class VideoActivity extends AppCompatActivity {

    // ...
    private Activity activity = this;
```

Luego, en el método `onCreate`, **invoca** `setOnCompletionListener()` sobre `videoView`.

**Escribe** `new` dentro de los paréntesis (`( )`) para que Android Studio autocomplete con un _listener anónimo_:

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        
        // ...
        
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {

            }
        });
```

Dentro de `onCompletion`, **invoca** `activity.finish()` para que la actividad termine:

```java
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        
        // ...
        
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                activity.finish();
            }
        });
```

Prueba ahora a reproducir un vídeo.

¿Termina correctamente cuando llega al final?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
