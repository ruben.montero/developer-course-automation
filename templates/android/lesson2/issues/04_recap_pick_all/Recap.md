¡Trabajo terminado!

## :trophy: ¡Felicidades!

Has realizado las tareas del _sprint_ y has validado tu trabajo con los _tests_. ¡Enhorabuena!

<img src="android-clips/android-frontend-clips/docs/sunset.jpg" width=500/>

Has...

* Usado la librería Volley para lanzar una petición HTTP
* Gestionado las respuestas exitosas y erróneas con `Response.Listener` y `Response.ErrorListener`, respectivamente
* Distinguido entre `JsonObjectRequest` y `JsonArrayRequest` en función de si el JSON consumido tiene como elemento raíz un diccionario (`{ }`) o una lista (`[ ]`)
* Mostrado y ocultado una `ProgressBar` mientras hay una petición en curso
* Convertido la respuesta JSON a objetos Java creados por ti
* Usado `RecyclerView`
* Creado celdas (`ViewHolder`s) para el `RecyclerView`
* Implementado un `RecyclerView.Adapter` y comprendido su funcionamiento
* Iniciado otra actividad al _clicar_ en una celda, pasando datos mediante `Intent`
* Reproducido un vídeo desde una URL con `VideoView`
* Implementado `MediaPlayer.OnCompletionListener`
