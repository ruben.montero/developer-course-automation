Respuesta incorrecta del servidor, o no

## :books: Resumen

* Entenderemos cómo puede fallar una petición HTTP
* Mostraremos distintos _Toast_ en distintos escenarios problemáticos

## Descripción

En la tarea anterior hemos programado el _happy path_ para mostrar un _Toast_ con un mensaje del servidor. Pero es igual de importante programar los casos de error. Por ejemplo, ¿y si el usuario _no_ tiene Internet?

Con Volley, disponemos de `Response.ErrorListener`. Recuerda que lo habíamos añadido en la primera tarea:

```java
        }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            
        }
```
 
`onErrorResponse` es análogo a `onResponse`, pero para situaciones de error. Principalmente dos escenarios:

### 1) La conexión no se ha establecido

```mermaid
graph LR
    A[App Android] -->|"Petición GET /health"| B[No llega al servidor HTTP]
```

* El cliente no tiene conexión de red
* La red de destino no es accesible por cuestiones de _enrutamiento_
* El servidor HTTP recibe el mensaje, pero rechaza la conexión
* El servidor HTTP recibe el mensaje, pero lo ignora silenciosamente

### 2) La conexión funciona, pero el servidor responde un código de error

```mermaid
graph LR
    A[App Android] -->|"Petición GET /vitality"| B[Servidor HTTP]
    B -->|"Respuesta 404 Not Found"| A
```

* El servidor HTTP acepta la conexión y responde con un código de error `4xx` ó `5xx`

Los _códigos de respuesta_ HTTP se dividen en 5 familias:

1. `1xx`: Informacionales
2. `2xx`: De éxito
3. `3xx`: De redirección
4. `4xx`: De error de cliente
5. `5xx`: De error de servidor

## :package: La tarea

En `MainActivity.java`, **añade** el siguiente `if-else` dentro del `onErrorResponse`:

```java
        @Override
        public void onErrorResponse(VolleyError error) {
            if (error.networkResponse == null) {
                // Error: No se ha establecido la conexión
                
            } else {
                // Error: El servidor ha dado una respuesta de error
                
                // La siguiente variable contendrá el código HTTP,
                // por ejemplo RANDOMEXPRESSION404|401|403|405END, 500,...
                int serverCode = error.networkResponse.statusCode;
            }
        }
```

_(Como puedes observar, se estudia si `error.networkResponse` es nulo para distinguir entre los dos escenarios de error descritos anteriormente)._

A continuación, dentro del `if`, **muestra** un `Toast` con el mensaje `"RANDOMEXPRESSIONCould not establish connection|Could not reach server|Unable to establish connection|Unable to reach server|Connection could not be established|Server could not be reached|Connection could not be established!|Server could not be reached!|There are problems with the connection|Problems with the connection!END"`

Para terminar, dentro del `else`, **muestra** un `Toast` con el mensaje `"RANDOMEXPRESSIONServer status:|Server status is|Server replied with|Server responded with|Server response|Server KO:END " + serverCode"`. Como ves, se concatena el código de error del servidor al _String_, para que dicho código se muestre al usuario en el `Toast`.

¡Enhorabuena! Has controlado por primera vez los errores que pueden surgir de una petición HTTP. Lanza la _app_ y verifica tu código.

### ¿Cómo verificarlo?

1. Para verificar que _no_ hay conexión, desde el emulador o un dispositivo físico apaga la Wi-Fi en _Ajustes_ y apaga el Uso de Datos Móviles en _Ajustes_. ¡Ya no hay Internet!
2. Para verificar un _código de error_ puedes, _momentáneamente_, alterar el _endpoint_:

```diff
-    Server.name + "/health",
+    Server.name + "/health2", // El servidor responderá 404 porque esta URL no existe
```

_(Luego déjalo como estaba)_

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

