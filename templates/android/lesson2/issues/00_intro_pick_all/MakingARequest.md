Petición de red

## :books: Resumen

* Veremos cómo usar la librería Volley para lanzar una solicitud de red simple
* Si el servidor responde correctamente, mostraremos un _Toast_
* Haremos `git add`, `git commit` y `git push` para subir los cambios al repositorio

## Descripción

Una aplicación móvil no es de mucha utilidad si no se conecta a Internet. Como mucho podremos programar una calculadora o un pequeño juego _offline_, pero, siendo realistas... ¿Cuántas _apps_ instaladas en un dispositivo real acceden a Internet? ¡Todas!

### HTTP

[HiperText Transfer Protocol](https://es.wikipedia.org/wiki/Protocolo_de_transferencia_de_hipertexto) es el [protocolo de aplicación](https://comofriki.com/capa-de-aplicacion-del-modelo-de-osi-protocolos/) más empleando en la red de redes: _Internet_.

Tu navegador intercambia mensajes HTTP con algún servidor cada vez que visitas una página web.

Nosotros, desde nuestra aplicación, lanzaremos una solicitud HTTP al servidor http://raspi y esperaremos una respuesta.

Usaremos una librería llamada **Volley**. Hace falta tener la línea:

```
implementation 'com.android.volley:volley:1.2.1'
```

...en `build.gradle`. _(Ya está añadido en nuestro proyecto)_

## :package: La tarea

Como primer paso, **abre** un terminal (cmd.exe) en tu ordenador y cambia el directorio activo hasta la ubicación de tu repositorio usando `cd`. Luego, **haz**:

```
git pull
```

...para traer los cambios de remoto a local, que contienen el _esqueleto_ del proyecto de este _sprint_.

**Abre** el proyecto `android-clips/android-frontend-clips` desde Android Studio.

¡Ojo! En este _sprint_ tendremos dos carpetas principales:

* `android-frontend-clips/`: Es la principal. Contiene el proyecto Android y trabajaremos sobre ella.
* `django-backend/`: Contiene código que es parte del proyecto, pero del _backend_. No trabajaremos aquí, pero puedes echar un vistazo a medida que avancemos

Después de abrir el proyecto, desde Android Studio, **abre** _java_ > _com.afundacion.fp.clips_ > `MainActivity.java` que ya nos resulta familiar:

```java
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // Aquí lanzaremos una petición HTTP
    }
}
```

### Lanzar una petición HTTP... ¿a dónde?

Al _endpoint_ REST disponible en http://raspi:8000/health. Si abres dicha URL desde un navegador, verás una respuesta en formato JSON. La idea es _solicitar_ ese JSON _desde Android_.

Como primer paso, **crea** una _instancia_ de [`JsonObjectRequest`](https://github.com/google/volley/blob/master/core/src/main/java/com/android/volley/toolbox/JsonObjectRequest.java)[^1]:

```java
        // Aquí lanzaremos una petición HTTP
        JsonObjectRequest request = new JsonObjectRequest();
```

Ahora, entre los paréntesis de `JsonObjectRequest()`, debemos pasar _cinco_ parámetros que recibe el método constructor:

1. Una _constante_ indicando el [método HTTP](https://developer.mozilla.org/es/docs/Web/HTTP/Methods) de la petición. Nosotros siempre usaremos `Request.Method.GET`.
2. Un _String_ indicando la URL.
3. El cuerpo de la petición. Es opcional. Siempre usaremos `null`.
4. Un _listener_ en el que escribiremos el código responsable de manejar una _respuesta de éxito_.
5. Un _listener_ en el que escribiremos el código responsable de manejar una _respuesta de error_.

Para empezar, **escribe** los tres primeros parámetros:

```java
        // Aquí lanzaremos una petición HTTP
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                Server.name + "/health",
                null,
                // ...
        );
```

### _Listeners_

El siguiente _parámetro_ del _constructor_ es un `Response.Listener<JSONObject>`.

Si escribimos `new` y un _espacio en blanco_, Android Studio nos permitirá autocompletar y generar una _instancia anónima_. **Hazlo** así:

```java
        // Aquí lanzaremos una petición HTTP
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                Server.name + "/health",
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }
        );
```

Aparecerá todo subrayado en _rojo_, porque el _constructor_ recibe _cinco_ parámetros, pero de momento sólo hemos indicado _cuatro_.

Para terminar, **añade** una _coma_ (`,`) tras la última llave (`}`), y **escribe** `new`. Android Studio autocompletará generando un nuevo `Response.ErrorListener` _anónimo_:

```java
        // Aquí lanzaremos una petición HTTP
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                Server.name + "/health",
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                
            }
        }
        );
```

¡Perfecto! Hemos construido un `JsonObjectRequest`. Ahora, lo importante es _qué_ código ponemos en `onResponse` y `onErrorResponse`.

En `onResponse`, **muestra** un _Toast_ que indique `"RANDOMEXPRESSIONResponse OK|Reply OK|Answer OK|Hit OK|GET OKEND"`:

```java
                // ...
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Aquí mostramos un Toast
                        // que indique "RANDOMEXPRESSIONResponse OK|Reply OK|Answer OK|Hit OK|GET OKEND"
                    }
                }, new Response.ErrorListener() {
                // ...
```

### Cola de peticiones de red

**Añade** un atributo privado a `MainActivity`, de tipo `RequestQueue`:

```java
public class MainActivity extends AppCompatActivity {
    private RequestQueue queue;
    
    // ...
```

...y, en la tercera línea de `onCreate`, **inicialízalo** empleando. **Así**:

```diff
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
+       this.queue = Volley.newRequestQueue(context);
+        
        // Aquí lanzaremos una petición HTTP
        // ...
```

Este _objeto_ servirá para _lanzar_ la petición `JsonObjectRequest` que hemos creado anteriormente. _Representa_ una cola de peticiones de red. ¡Sólo hace falta invocar `queue.add`![^2]

**Hazlo** _al final_ de `onCreate`:

```java
        // ...
        this.queue.add(request);
    }
```

### Un último problemilla...

Si pruebas la aplicación, verás esta línea en los _logs_:

```
java.net.UnknownHostException: Unable to resolve host "raspi": No address associated with hostname
```

Nuestro emulador no es capaz de traducir `raspi` a una dirección IP válida de la misma manera que nuestro ordenador anfitrión _sí_ lo hace. La IP de `raspi` es asignada por el DHCP de la red, que el emulador no _alcanza_ a ver. Si `raspi` fuera un nombre de dominio públicamente accesible (como `www.google.es`), no habría esta complicación.

Ante esta limitación del emulator en lo relativo al DNS, debemos _sustituir_ `raspi` en `Server.java` por una IP válida `X.X.X.X`.

**Sigue** las instrucciones en `Server.java` para hacer un _ping_, y luego, **sustituye** `raspi` por la IP correcta.

Una vez hecho esto, prueba de nuevo.

### ¡Funciona!

**¡Enhorabuena!** Has lanzado tu primera petición de red.

### :trophy: Por último

Desde el terminal de Windows (cmd.exe), nos posicionamos en `android-clips/` y escribimos:

```
git add *
```

...para marcar los cambios del un nuevo _commit_. Después:

```
git commit -m "Tarea #20: Implementada petición simple a /health en android-clips"
```

...y por último subimos los cambios al repositorio remoto:

```
git push
```

No está de más visitar la página de GitLab (https://raspi) y verificar que el _commit_ se ha subido.

--------------------------------------------------

[^1]: `JsonObjectRequest` es una clase de la librería Volley que _representa_ una petición HTTP. Hay _tres_ posibilidades: `StringRequest` (Petición sencilla que espera un _texto plano_ como respuesta), `JsonObjectRequest` (Espera un objeto JSON (`{ ... }`) como respuesta) y `JsonArrayRequest` (Espera un _array_ JSON (`[ ... ]`) como respuesta).

[^2]: No, no falta sólo eso. Para que una aplicación pueda lanzar peticiones de red debe declara el permiso `INTERNET` en el archivo _manifest_. Además, si el tráfico va por `http` y no `https`, será necesario `usesCleartextTraffic="true"`. Puedes verificar que ambas cuestiones ya está en el `AndroidManifest.xml` del proyecto.
