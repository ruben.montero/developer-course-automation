Respuesta correcta del servidor

## :books: Resumen

* Veremos cómo trabajar con la respuesta del servidor a una petición
* Mostraremos en el _Toast_ algo respondido por el servidor

## Descripción

En la tarea anterior hemos aprendido a lanzar una petición HTTP.

Sabemos que esta petición es correspondida por nuestro servidor con una _respuesta_ (en formato JSON).

```mermaid
graph LR
    A[App Android] -->|"Petición GET /health"| B[Servidor HTTP]
    B -->|"Respuesta {'status': 'Server is healthy!'}"| A
```

Podemos verificarlo visitando desde el navegador el _endpoint_ http://raspi:8000/health.

### ¿Y desde Android puedo usar esa respuesta?

¡Sí!

### ¿Cómo?

A través del `JSONObject` que obtenemos como _parámetro_ en nuestro `onResponse`.

En `MainActivity.java`:

```java
        // Aquí lanzaremos una petición HTTP
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                Server.name + "/health",
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Aquí el parámetro 'response' se corresponde
                        // con la RESPUESTA del SERVIDOR
                        
                        // Lanzamos el Toast (tarea anterior)
                        // ...
```

Dado que la respuesta contiene una _clave-valor_ con _clave_ `status`, podríamos hacer:

```java
// Imprime 'Server is healthy!'
System.out.println(response.getString("status"));
```

## :package: La tarea

**Modifica** el `Toast` de la tarea anterior, que mostraba `"RANDOMEXPRESSIONResponse OK|Reply OK|Answer OK|Hit OK|GET OKEND"`, para que ahora **concatene** _dos puntos_ (`:`), un _espacio en blanco_ y el _mensaje del servidor_ (bajo la clase `"status"`):

```diff
- "RANDOMEXPRESSIONResponse OK|Reply OK|Answer OK|Hit OK|GET OKEND"
+ "RANDOMEXPRESSIONResponse OK|Reply OK|Answer OK|Hit OK|GET OKEND: " + response.getString("status")
```

* Si te encuentras con algún problema de una excepción _no controlada_ al acceder[^1] al JSON, basta con que la envuelvas en un `try-catch`.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Recuerda que esto sucede para obligarnos a considerar: _¿Qué pasa si la respuesta JSON no contiene la clave deseada? (En este caso, `status`)_
