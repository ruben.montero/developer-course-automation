Fade Bad Grades Out

## :books: Resumen

* Explicaremos el sistema de evaluación
* Solicitaremos al profesor que suba los _tests_, y los ejecutaremos
* Si hay errores en nuestro código de las tareas anteriores, lo corregiremos

## Descripción

La nota de cada _sprint_ será calculada individualmente, lanzando _tests_ automáticos sobre tu código.

* ¡Ojo! Cada tarea **no** vale lo mismo. Las tareas con más _tests_ pesan más.

Los _tests_ no están inicialmente en tu repositorio, y para lanzarlos **debes solicitar al profesor** que los **suba**. Debes hacerlo cuando hayas completado todas las tareas anteriores.

Luego, podrás ejecutar:

```
git pull
```

...desde una línea de comandos (cmd.exe), y verás que los _tests_ habrán aparecido en _com.afundacion.fp.clips (androidTest)_. Dentro del repositorio, la carpeta es `android-clips/android-frontend-clips/app/src/androidTest/java/com/afundacion/fp/clips`.

### Ya tengo _tests_ automáticos... ¿y ahora?

¡Lánzalos!

Desde Android Studio, **expande** la carpeta:

```
com.afundacion.fp.clips/ (androidTest)
```

...a la izquierda y **haz _click_ derecho** en el primer _test_. Selecciona **Run '...'** y espera a que se compile la aplicación y se lance en el emulador.

Si se despliega una ventana inferior y ves _ticks verdes_ abajo a la izquierda, entenderemos que nuestro código _pasa el test_ de la tarea adecuadamente.

Ahora la misión es **verificar que todos los _tests_ pasan** (es decir, _ticks verdes_).

**Si encuentras que algún _test_ no pasa (_tick rojo_), debes buscar el error y corregir tu código para que el _test_ pase**. Esa es nuestra misión. Podemos hacer los _commits_ adicionales que queramos para arreglar errores en el código. ¡Nuestra nota será directamente proporcional al número de _tests_ que pasen satisfactoriamente!

### :trophy: Por último

Una vez verifiques que **todos** los _tests_ pasan correctamente... ¡Trabajo del _sprint_ finalizado!
