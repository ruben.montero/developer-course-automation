Lista inicial

## :books: Resumen

* Crearemos un `<RANDOMEXPRESSIONol|ulEND>` en `nameGenerator.html`
* Añadiremos una función a `nameGenerator.js` que _añade_ 30 `<li>` con nombres aleatorios
* La invocaremos inicialmente

## Descripción

Vamos a invocar la función `generateRandomName` de la tarea anterior para que se generen 30 nombres distintos, y mostrarlos en una lista en el HTML.

## :package: La tarea

**Añade** un `<RANDOMEXPRESSIONol|ulEND id='RANDOMEXPRESSIONnamesList|randomNamesListEND' data-cy='RANDOMEXPRESSIONnamesList|randomNamesList|listOfNames|listOfRandomNamesEND'></RANDOMEXPRESSIONol|ulEND>` **después** del `h1` en `nameGenerator.html`.

Luego, en `nameGenerator.js` **crea** esta función:

```js
function appendNewNames() {
    // Esto recupera el <RANDOMEXPRESSIONol|ulEND> del HTML y lo vuelca en una variable
    const theHtmlList = document.getElementById("RANDOMEXPRESSIONnamesList|randomNamesListEND");
    
    // Bucle for con 30 iteraciones
    for (let i = 0; i < 30; i++) {
        // Se crea elemento <li>
        const newItemForList = document.createElement("li");
        
        // Se establece que el <li> muestre un texto dado usando el atributo innerHTML
        const textToShow = generateRandomName();
        newItemForList.innerHTML = textToShow;
        
        // Y se añade dicho elemento a la lista
        theHtmlList.append(newItemForList)
    }
}
```

Para terminar, al final de `nameGenerator.js` **invoca** esta función (no borres el `console.log` de la tarea anterior):

```js
console.log(generateRandomName());

appendNewNames();
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_. Acuérdate de hacer `git push`.
