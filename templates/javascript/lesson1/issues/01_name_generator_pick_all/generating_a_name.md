Nombre aleatorio

## :books: Resumen

* Añadiremos un fichero JavaScript dentro una nueva carpeta `js/`
* Lo enlazaremos como `<script>` dentro de `nameGenerator.html`
* Crearemos algunas funciones para generar un _nickname_ aleatorio
* Lo invocaremos inicialmente y lo mostraremos por consola

## Descripción

Nuestro primer proyecto generará una lista infinita de nombres aleatorios, que servirán como _sugerencias_ para un usuario que busca la respuesta a: _¿Qué nombre de usuario me puedo poner en una página web dada?_

La manera de generar un nombre aleatorio será _concatenando_ tres partes:

* Una primera cadena de caracteres elegida aleatoriamente del conjunto:

```
'Mega', 'Turbo', 'Hiper', 'Super', 'Great', 'Big', 'Small', 'Nitro', 'Shadow', 'Random'
```

* Una segunda cadena de caracteres elegida aleatoriamente del conjunto:

```
'Dog', 'Cat', 'Lizard', 'Croco', 'Coconut', 'Apple', 'Demon', 'Car', 'Tree', 'Light', 'JavaScript'
```

* Un número aleatorio en el rango _desde_ `1DEVELOPERNUMBERRANDOMNUMBER10-20END` y con un _tamaño_ de `RANDOMNUMBER60-70END`.

## :package: La tarea

**Crea** una **nueva carpeta** dentro de `projects/` llamada `js`.

Dentro de `www/projects/js/`, **crea** un **nuevo fichero** JavaScript: `nameGenerator.js`.

En `nameGenerator.js`, **escribe**  este sencillo `console.log`:

```js
console.log("RANDOMEXPRESSIONArchivo cargado|JavaScript cargado|Código cargado|Archivo inicializado|JavaScript inicializado|Código inicializadoEND");
```

A continuación, en `nameGenerator.html`, **añade** un `<script>` para _enlazarlo_:

```html
  <head>
    <meta charset="utf8"/>
+   <script defer src="/projects/js/nameGenerator.js"></script>
  </head>
```

### Generando un nombre aleatorio

Ahora, en `nameGenerator.js`, **escribe** una función `generateRandomName` que genera un nombre aleatorio de acuerdo a la descripción inicial de la tarea.

```js
console.log("RANDOMEXPRESSIONArchivo cargado|JavaScript cargado|Código cargado|Archivo inicializado|JavaScript inicializado|Código inicializadoEND");

function generateRandomName() {
    const randomPart1 = choose(['Mega', 'Turbo', 'Hiper', 'Super', 'Great', 'Big', 'Small', 'Nitro', 'Shadow', 'Random']);
    
    // ...
}
```

¡Sería genial si existiera una función `choose`! Pero tenemos que **implementarla**.

**Escribe**:

```js
// Asumimos que el parámetro 'choices' será un array
function choose(choices) {
}
```

Ahora, generaremos un _índice_ aleatorio para extraer un único elemento del parámetro `choices`. Lo haremos con [`Math.random()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random), que genera un número aleatorio _con decimales_ entre `0` _(incluido)_ y `1` _(no incluido)_.

**Escribe**:

```js
function choose(choices) {
    const index = Math.random() * choices.length;
}
```

¡Al multiplicarlo por la longitud del _array_, tenemos el número que necesitamos!

Por ejemplo, siendo `choices.length == 10`:

* `0      * 10 = 0;` 
* `0.123  * 10 = 1.23;`
* `0.42   * 10 = 4.2;`
* `0.9999 * 10 = 9.999;`

**Completa** la función `choose` _truncando_ los decimales del índice aleatorio y devolviendo el elemento elegido al azar:

```js
function choose(choices) {
  const index = Math.trunc(Math.random() * choices.length);
  return choices[index];
}
```

### Casi hemos terminado...

Como ves, usando `choose` es muy fácil la _aleatorización_ de la primera y segunda parte del nombre.

```js
function choose(choices) {
    const index = Math.floor(Math.random() * choices.length);
    return choices[index];
}
    
function generateRandomName() {
    const randomPart1 = choose(['Mega', 'Turbo', 'Hiper', 'Super', 'Great', 'Big', 'Small', 'Nitro', 'Shadow', 'Random']);
    const randomPart2 = choose(['Dog', 'Cat', 'Lizard', 'Croco', 'Coconut', 'Apple', 'Demon', 'Car', 'Tree', 'Light', 'JavaScript']);
    
    // ...
}
```

Tal y como se especificó al inicio de la tarea, la tercera parte del nombre será un número aleatorio en un rango de _tamaño_ `RANDOMNUMBER60-70END`:

```js
Math.random() * RANDOMNUMBER60-70END
```

...y el número inicial será `1DEVELOPERNUMBERRANDOMNUMBER10-20END`:

```js
1DEVELOPERNUMBERRANDOMNUMBER10-20END + (Math.random() * RANDOMNUMBER60-70END)
```

Dicho de otra forma, nuestro número aleatorio estará _entre_ `1DEVELOPERNUMBERRANDOMNUMBER10-20END` _y_ `1DEVELOPERNUMBERRANDOMNUMBER10-20END+RANDOMNUMBER60-70END`:

¡**Completa** el código de `nameGenerator.js`!

```js
function randomNumber(min, rangeLength) {
    return Math.trunc(min + (Math.random() * rangeLength));
}
    
function choose(choices) {
    const index = Math.floor(Math.random() * choices.length);
    return choices[index];
}
    
function generateRandomName() {
    const randomPart1 = choose(['Mega', 'Turbo', 'Hiper', 'Super', 'Great', 'Big', 'Small', 'Nitro', 'Shadow', 'Random']);
    const randomPart2 = choose(['Dog', 'Cat', 'Lizard', 'Croco', 'Coconut', 'Apple', 'Demon', 'Car', 'Tree', 'Light', 'JavaScript']);
    const randomPart3 = randomNumber(1DEVELOPERNUMBERRANDOMNUMBER10-20END, RANDOMNUMBER60-70END);
    
    return randomPart1 + randomPart2 + randomPart3;
}
```


Y, al final, **añade**  un `console.log` para probarlo:

```js
const aRandomName = generateRandomName();
console.log(aRandomName);
```

Visita http://localhost:8000/projects/nameGenerator.html e inspecciona la _Consola_ del navegador.

¿Ves un nombre aleatorio distinto cada vez que refrescas la página?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
