Mis proyectos

## :books: Resumen

* Modificaremos `index.html` para mostrar una lista y un enlace
* Cambiaremos el `<h1>` en `nameGenerator.html`

## Descripción

Vamos a trabajar en proyectos que estarán dentro de la carpeta `www/projects/`.

## :package: La tarea

**Abre** y **modifica** el `<h1>` al final de `index.html`:

```diff
- <h1>Ojalá un <i>developer</i> excelente escriba aquí un título apropiado</h1>
+ <h1 data-cy="projectsHeader">RANDOMEXPRESSIONMis proyectos|Proyectos|Proyectos del sprint|Mis proyectos de JavaScript|Proyectos de JavaScript|Proyectos JavaScriptEND</h1>
```
A continuación, **añade** un `<RANDOMEXPRESSIONul|olEND>`...

```html
  <h1 data-cy="projectsHeader">RANDOMEXPRESSIONMis proyectos|Proyectos|Proyectos del sprint|Mis proyectos de JavaScript|Proyectos de JavaScript|Proyectos JavaScriptEND</h1>
  <RANDOMEXPRESSIONul|olEND data-cy="RANDOMEXPRESSIONprojectsList|listOfProyects|myProjectsEND">
  </RANDOMEXPRESSIONul|olEND>
```

...que contendrá varios elementos, cada uno con un _enlace_ a la página de un proyecto distinto.

Para empezar, **añade** sólo el siguiente:

```html
  <h1 data-cy="projectsHeader">RANDOMEXPRESSIONMis proyectos|Proyectos|Proyectos del sprint|Mis proyectos de JavaScript|Proyectos de JavaScript|Proyectos JavaScriptEND</h1>
  <RANDOMEXPRESSIONul|olEND data-cy="RANDOMEXPRESSIONprojectsList|listOfProyects|myProjectsEND">
    <li><a href="/projects/nameGenerator.html">RANDOMEXPRESSIONGenerador de nombres|Generador de nombres aleatorios|Generador de nicknamesEND</a></li>
  </RANDOMEXPRESSIONul|olEND>
```

¿Ves en http://localhost:8000 tus nuevas modificaciones?

¿Funciona el enlace?

Si es así, para terminar, **dale** a `nameGenerator.html` un nuevo título:

```diff
- <h1>De momento, nada</h1>
+ <h1 data-cy="pageHeader">RANDOMEXPRESSIONAquí puedes ver sugerencias de|Aquí se generan|Aquí aparecen aleatoriamente|Aquí verás sugerencias de|Aquí hay sugerencias de|Aquí hay ideas paraEND <i>nicknames</i></h1>
```

¡Listo!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
