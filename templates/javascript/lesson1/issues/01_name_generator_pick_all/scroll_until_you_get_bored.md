Scroll infinito

## :books: Resumen

* Veremos cómo funciona el _Intersection Observer API_ y por qué nació
* Añadiremos un simple `<p>` a `nameGenerator.html` que indicará `RANDOMEXPRESSIONGenerando más...|Creando más...|Produciendo más...|Generando más nombres...|Creando más nombres...|Produciendo más nombres...END`, al final de la página
* Siempre que dicho `<p>` sea visible, concatenaremos 30 `<li>` _más_ a la lista

## Descripción

En la tarea anterior hemos visto algunas funcionalidades para manipular elementos del HTML a través del [HTML DOM API](https://developer.mozilla.org/en-US/docs/Web/API/HTML_DOM_API) (`createElement`, `getElementById`, `append`,...).

Vamos a _ampliar_ nuestro horizonte. Cada proyecto nos descubrirá algunas de las [numerosas APIs](https://developer.mozilla.org/en-US/docs/Web/API) que están a nuestro alcance.

### [_Intersection Observer API_](https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API)

Históricamente, detectar la visibilidad de un elemento en la pantalla era una tarea difícil. Se solían implementar soluciones poco fiables y que usaban bucles infinitos para comprobar el posicionamiento de los elementos. Con frecuencia no funcionaban bien y ralentizaban la página. ¡Pero hay muchos escenarios donde es necesario!

* Averiguar si un anuncio se está viendo para calcular el _revenue_ (ganancia monetaria)
* Implementar carga _lazy_ de imágenes u otro contenido a medida que el usuario hace _scroll_
* Implementar _scroll_ infinito
* ...

Por ello, [_Intersection Observer API_](https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API) surgió para que estos escenarios se pudieran implementar con sencillas llamadas a las librerías propias del navegador.

[Aquí puedes ver](https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API#result) un ejemplo de uso avanzado de esta API.

Nosotros implementaremos un sencillo de _scroll_ infinito.

### La idea

* Ubicaremos un párrafo `<p>` al final del todo, en el HTML.
* Cuando el `<p>` esté _visible_ en pantalla, invocaremos `appendNewNames()`. Así, 30 nombres _más_ se añadirán a la lista y el `<p>` pasará a estar más abajo, por lo tanto, habrá que hacer _scroll_ para verlo de nuevo.
* Luego, se repetirá el proceso una y otra vez a medida que hacemos _scroll_ hacia abajo. ¡_Scroll_ infinito!

### ¿Cómo?

Debemos [crear un `IntersectionObserver`](https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API#creating_an_intersection_observer), al que pasaremos _dos_ parámetros: Una función (`callback`) y unas _opciones_.

A dicho `IntersectionObserver` se le añade un elemento HTML `"X"` con `.observe()`.

Entonces, cada vez que el elemento `"X"` es visible de acuerdo a las _opciones_, se invocará automáticamente la función (`callback`).

## :package: La tarea

**Añade** el siguiente `<p>` en `nameGenerator.html`, **después** del `<RANDOMEXPRESSIONol|ulEND>` de la tarea anterior:

```html
  <p id="textLoadingMore" data-cy="RANDOMEXPRESSIONtextLoadingMore|textLoadMore|textMore|textIsLoadingMore|textMoreElements|textLoadingMoreNames|textloadMoreNamesEND">RANDOMEXPRESSIONGenerando más...|Creando más...|Produciendo más...|Generando más nombres...|Creando más nombres...|Produciendo más nombres...END</p>
```

Luego, en `nameGenerator.js`, **añade**:

```js
const paragraphLoading = document.getElementById("textLoadingMore");

const options = {
    root: null,
    rootMargin: '0px',
    threshold: 1.0
}

const observer = new IntersectionObserver(appendNewNames, options);
observer.observe(paragraphLoading);
```

Cada vez que el `<p>RANDOMEXPRESSIONGenerando más...|Creando más...|Produciendo más...|Generando más nombres...|Creando más nombres...|Produciendo más nombres...END</p>` aparezca en pantalla totalmente (100%), se _ejecutará_ `appendNewNames`.

### ¡Verifícalo!

Pruébalo en http://localhost:8000/projects/nameGenerator.html

¿Funciona?

**¡Enhorabuena!** Has implementado tu primer **_scroll_ infinito**.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
