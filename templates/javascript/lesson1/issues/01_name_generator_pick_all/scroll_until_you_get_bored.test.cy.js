/// <reference types="cypress" />

describe('infinite scroll', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8000/projects/nameGenerator.html');
      cy.reload(true);
    })
  
    it('issueISSUENUMBER paragraph exists and amount is initially correct', () => {
        cy.get('p[data-cy=RANDOMEXPRESSIONtextLoadingMore|textLoadMore|textMore|textIsLoadingMore|textMoreElements|textLoadingMoreNames|textloadMoreNamesEND]')
        .should('have.text', 'RANDOMEXPRESSIONGenerando más...|Creando más...|Produciendo más...|Generando más nombres...|Creando más nombres...|Produciendo más nombres...END');

        cy.get('RANDOMEXPRESSIONol|ulEND[data-cy=RANDOMEXPRESSIONnamesList|randomNamesList|listOfNames|listOfRandomNamesEND]')
        .find('li')
        .should('have.length.lte', 60);
    })

    it('issueISSUENUMBER amount of names gets correctly updated', () => {
        cy.get('RANDOMEXPRESSIONol|ulEND[data-cy=RANDOMEXPRESSIONnamesList|randomNamesList|listOfNames|listOfRandomNamesEND]')
        .find('li')
        .should('have.length.lte', 60)
        .then($p => {
            cy.scrollTo(0, 1000);

            cy.get('RANDOMEXPRESSIONol|ulEND[data-cy=RANDOMEXPRESSIONnamesList|randomNamesList|listOfNames|listOfRandomNamesEND]')
            .find('li')
            .should('have.length.gt', $p.length)
            .should('have.length.lte', $p.length + 120)

            // Check all names are valid
            for (let i = 0; i < 30; i++) {
                cy.get('RANDOMEXPRESSIONol|ulEND[data-cy=RANDOMEXPRESSIONnamesList|randomNamesList|listOfNames|listOfRandomNamesEND]')
                .find('li')
                .eq(i)
                .then($li => {
                    const name = $li.text();
                    assertStringIsValidRandomName(name);
                })
            }
        });
    })

    function assertStringIsValidRandomName(string) {
        let containsChoice1 = false;
        for (const c of ['Mega', 'Turbo', 'Hiper', 'Super', 'Great', 'Big', 'Small', 'Nitro', 'Shadow', 'Random']) {
            if (string.includes(c)) {
                containsChoice1 = true;
            }
        }
        expect(containsChoice1).to.be.eq(true);
        let containsChoice2 = false;
        for (const c of ['Dog', 'Cat', 'Lizard', 'Croco', 'Coconut', 'Apple', 'Demon', 'Car', 'Tree', 'Light', 'JavaScript']) {
            if (string.includes(c)) {
                containsChoice2 = true;
            }
        }
        expect(containsChoice2).to.be.eq(true);
        const number = parseInt(string.substring(string.length - 5));
        expect(number).to.be.gte(1DEVELOPERNUMBERRANDOMNUMBER10-20END);
        expect(number).to.be.lte(1DEVELOPERNUMBERRANDOMNUMBER10-20END + RANDOMNUMBER60-70END);
    }
})
