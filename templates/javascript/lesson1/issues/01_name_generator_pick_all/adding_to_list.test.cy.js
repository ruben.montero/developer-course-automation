/// <reference types="cypress" />

describe('adding_to_list', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8000/');
      cy.reload(true);
    })
  
    it('issueISSUENUMBER index.html is correctly modified', () => {
        cy.get('h1[data-cy=projectsHeader]')
        .should('have.text', 'RANDOMEXPRESSIONMis proyectos|Proyectos|Proyectos del sprint|Mis proyectos de JavaScript|Proyectos de JavaScript|Proyectos JavaScriptEND');

        cy.get('RANDOMEXPRESSIONul|olEND[data-cy=RANDOMEXPRESSIONprojectsList|listOfProyects|myProjectsEND]')
        .find('li')
        .first()
        .find('a')
        .should('have.text', 'RANDOMEXPRESSIONGenerador de nombres|Generador de nombres aleatorios|Generador de nicknamesEND');
    })

    it('issueISSUENUMBER nameGenerator.html is correctly modified', () => {
      cy.get('RANDOMEXPRESSIONul|olEND[data-cy=RANDOMEXPRESSIONprojectsList|listOfProyects|myProjectsEND]')
      .find('li')
      .first()
      .find('a')
      .click();

      cy.get('h1[data-cy=pageHeader]')
      .should('have.text', 'RANDOMEXPRESSIONAquí puedes ver sugerencias de|Aquí se generan|Aquí aparecen aleatoriamente|Aquí verás sugerencias de|Aquí hay sugerencias de|Aquí hay ideas paraEND nicknames');

      cy.get('h1[data-cy=pageHeader]')
      .find('i')
      .should('have.text', 'nicknames');
  })
})
