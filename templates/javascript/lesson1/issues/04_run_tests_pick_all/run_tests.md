Que las buenas notas no terminen

## :books: Resumen

* Solicitaremos al profesor que suba los _tests_, y los ejecutaremos
* Si hay errores en nuestro código de las tareas anteriores, lo corregiremos

## Sobre la evaluación...

La nota de cada _sprint_ será calculada individualmente, lanzando _tests_ automáticos sobre tu código. Estos _tests_ no están inicialmente en tu repositorio.

**Solicita** al profesor que **suba** tus _tests_. Debes hacerlo cuando hayas completado todas las tareas anteriores.

Luego, desde un terminal (cmd.exe), **ejecuta**:

```
git pull
```

...y verás que los _tests_ habrán aparecido en la carpeta `javascript/cypress/e2e`

### Ya tengo _tests_ automáticos... ¿y ahora?

¡Lánzalos!

Para ello, **instala** `Node.js`.

* https://nodejs.org/en

### Ejecutar los _tests_

Desde un nuevo terminal (cmd.exe), **cambia** (`cd`) a la carpeta `javascript` y ejecuta **dos** comandos:

```
npm install
```

...instalará la librería **Cypress** dentro de una nueva carpeta, `node_modules/`. Acepta todo lo necesario para terminar la instalación.

Luego:

```
npx cypress open
```

...para **lanzar Cypress**:

Durante la ejecución, **agregamos** Cypress como **excepción al _firewall_ de Windows**.

<img src="javascript/docs/cypress_firewall.png" width=250 />

### Cypress

Se abrirá una ventana. Elegimos _end-to-end testing_ (e2e):

<img src="javascript/docs/cypress_e2e.png" width=250 />

Luego, elegimos iniciar el _software_ de pruebas en nuestro navegador preferido.

Se abrirá el navegador y mostrará la lista de _tests_.

### ¡Ejecuta todos los _tests_!

Puedes probar todos los _tests_ del _sprint_.

* Después de lanzar uno, puedes volver al listado con el botón de la izquierda.
* Si quieres re-lanzar un conjunto de _tests_, puedes usar el botón de la derecha.

<img src="javascript/docs/cypress_alltests_reload.png" width=250 />

## :package: La tarea

Ahora la misión es **verificar que todos los _tests_ pasan** (es decir, _ticks verdes_).

**Si encuentras que algún _test_ no pasa (_tick rojo_), debes buscar el error y corregir tu código para que el _test_ pase**. 

**Haz** los _commits_ adicionales que creas necesarios, arreglando errores en el código. ¡Tu nota será directamente proporcional al número de _tests_ que pasen satisfactoriamente!

### :trophy: Por último

Una vez verifiques que **todos** los _tests_ pasan correctamente... ¡Trabajo del _sprint_ finalizado!
