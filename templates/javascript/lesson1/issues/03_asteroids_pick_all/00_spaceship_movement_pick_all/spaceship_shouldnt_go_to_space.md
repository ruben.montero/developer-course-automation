El espacio exterior es demasiado amplio...

## :books: Resumen

* Restringiremos el área hasta donde permitimos moverse a nuestra nave

## Descripción

En la tarea anterior hemos implementado el movimiento de la nave espacial:

Pero, `GAME_STATE.shipPosX` y `GAME_STATE.shipPosY`, ¡no tienen límites!

Nuestro _lienzo_ es de `640`x`480`. Sin embargo, la nave puede llegar has la posición `x=3568`, `y=6589` porque nada se lo impide.

## :package: La tarea

**Añade** el código que consideres necesario para **limitar** la localización de la nave al **interior** del lienzo.

Tendrás que sustituir cada una de las líneas que _actualizan_ la posición de la nave y evitar que salga:

Un par de herramientas con las que puedes resolver la tarea son:

* Sentencia [`if`](https://www.javascripttutorial.net/javascript-if/)
* [`Math.max`](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Math/max#pru%C3%A9balo) y [`Math.min`](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Math/min#ejemplos)

### Importante

<img src="javascript/docs/spaceship_out_of_canvas.png" width=450 />

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
