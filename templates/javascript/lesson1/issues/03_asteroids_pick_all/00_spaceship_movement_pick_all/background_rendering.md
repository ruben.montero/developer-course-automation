Color sólido de fondo

## :books: Resumen

* Veremos cómo se dibujan elementos 2D en un `Canvas`
* Pintaremos un fondo de color oscuro

## Descripción

Nuestro `<canvas>` en HTML es todo lo que necesitamos desde JavaScript para manipular imágenes en 2D.

### [`CanvasRenderingContext2D`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D)

Es un tipo de objeto que nos provee funciones útiles para _renderizar_ (dibujar) elementos en un _canvas_ (lienzo) de 2 dimensiones.

En esta tarea usaremos:

* [`fillStyle`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/fillStyle): Permite establecer el _color_ con el que se dibujarán líneas y figuras a continuación. Es algo así como _cambiar el color de la brocha_.
* [`fillRect`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/fillRect): Permite dibujar un rectángulo.

### Sistema de coordenadas

Tanto para `fillRect` como para otras _funciones_ disponibles, debemos entender el _sistema de coordenadas_ del `<canvas>`.

No (:warning:) es como las coordenadas `x` e `y` con las que estamos acostumbrados a trabajar en matemáticas:

* `x`: Eje horizontal. Izquierda (`-`) a derecha (`+`)
* `y`: Eje vertical. Abajo (`-`) a arriba (`+`)

...sino **así**:

* `x`: Eje horizontal. Izquierda (`-`) a derecha (`+`)
* `y`: Eje vertical. **Arriba** (`-`) a **abajo** (`+`)

<img src="javascript/docs/canvas_example.png" width=350 />

### [`fillRect`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/fillRect)

Se usa especificando cuatro parámetros:

* Origen `x` del rectángulo
* Origen `y` del rectángulo
* Anchura del rectángulo
* Altura del rectángulo

Por ejemplo, la siguiente _invocación_:

```
CANVAS_CTX.fillRect(6, 2, 5, 1);
```

...produciría un rectángulo así:

<img src="javascript/docs/fillrect_example.png" width=350 />

### Ejercicio mental

¿Eres capaz de dibujar en tu mente _cómo_ serían los rectángulos producidos por...

* `CANVAS_CTX.fillRect(1, 1, 2, 2);`
* `CANVAS_CTX.fillRect(12, 0, 1, 4);`
* `CANVAS_CTX.fillRect(0, 0, 13, 4);`

...?

## :package: La tarea

En la primera línea de `www/projects/js/asteroids.js`, **inicializa** una variable [^1] que representa el _canvas_ HTML, y, a continuación, con dicha variable (de tipo [`HTMLCanvasElement`](https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement)) **obtén** un [`CanvasRenderingContext2D`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D). **Así**:

```js
const CANVAS = document.getElementById("gameContainer");
const CANVAS_CTX = CANVAS.getContext("2d");
```

A continuación, **establece** el ancho[^2] y alto[^2] del _canvas_ en `640` por `480`:

```js
CANVAS.width = 640;
CANVAS.height = 480;
```

### Centrando el lienzo

Estableceremos dos propiedades CSS _a través_ de JavaScript. Sólo debemos _manipular_ el atributo `.style` del elemento en cuestión. Después del código que añadiste antes, **añade**:

```js
CANVAS.style.margin = "auto";
CANVAS.style.display = "block";
```

Luego, **añade** la siguiente función para rellenar el fondo del _canvas_ (lienzo):

```js
function renderBackground() {
    CANVAS_CTX.fillStyle = "#RANDOMNUMBER0-2ENDRANDOMNUMBER0-8ENDRANDOMNUMBER0-4ENDRANDOMNUMBER0-9ENDRANDOMEXPRESSION0|1|2|3|4|5|6|7|8|9|aENDRANDOMEXPRESSION0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|fEND";
    CANVAS_CTX.fillRect(0, 0, CANVAS.width, CANVAS.height);
}
```

Y para terminar, justo después... ¡**invoca** la función!

```js
renderBackground();
```

Visita http://localhost:8000/projects/asteroids.html. ¿Ves el color de fondo?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: ¿Te preguntas por qué `CANVAS` ha sido escrito en mayúsculas? Este fichero irá creciendo y no nos interesa mezclar variables locales con globales. Para mantener el orden, a las variables globales (constantes) les pondremos un nombre en mayúsculas.

[^2]: Nótese que `CANVAS.width` y `CANVAS.height` entregan el _ancho_ y _alto_ reales del lienzo. Pero, en `<canvas>`, puede ser distinto al _ancho_ y _alto_ mostrado en HTML _si fue manipulado mediante CSS_. En otras palabras, existen 4 variables: `CANVAS.width`, `CANVAS.style.width`, `CANVAS.height`, `CANVAS.style.height` y las discrepancias entre ellas, ¡pueden dar problemas de interpolado!
