/// <reference types="cypress" />

describe('game loop', () => {
    let parameter1;
    let callCount = 0;
    beforeEach(() => {
        cy.visit('http://localhost:8000/projects/asteroids.html');
        cy.reload(true); // Force-reload to prevent cache errors
        cy.visit('http://localhost:8000/projects/asteroids.html', {
            onBeforeLoad (win) {
                cy.stub(win.console, 'log', (x) => {
                  parameter1 = x
                  callCount++;
                }).as("consoleLog").log(false);
              },
        })
    })

    it('issueISSUENUMBER game loop log is generated', () => {
        cy.get('@consoleLog').should(() => {
            expect(parameter1).to.contain('Han RANDOMEXPRESSIONtranscurrido|pasadoEND');
            expect(parameter1).to.contain('RANDOMEXPRESSIONms desde la última actualización|milisegundos desde la última actualización|ms desde el último frame|milisegundos desde el último frameEND');
        });
    })
})
