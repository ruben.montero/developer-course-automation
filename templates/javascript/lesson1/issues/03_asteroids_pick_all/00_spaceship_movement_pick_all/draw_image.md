Dibujar nave espacial

## :books: Resumen

* Pintaremos la nave espacial con `drawImage`

## Descripción

Ya sabemos usar `fillRect`... ¡Adelante! Escribamos todo nuestro juego usando `fillRect` para _renderizar_ rectángulos.

### Ehh...

¿Sí?

### ¿No hay una forma mejor? Parece tedioso...

Vaya, pues sí.

En la práctica, `fillRect` tiene un uso bastante limitado.

Así que en esta tarea usaremos [`drawImage`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage) para dibujar una _imagen_.

Para _disponer_ de la imagen que vamos a pintar, emplearemos la clase [`HTMLImageElement`](https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement).

Y, desde JavaScript, generaremos algo análogo al HTML:

```html
<img src="/projects/images/spaceship.png" />
```

¡De hecho, si visitas http://localhost:8000/projects/images/spaceship.png verás la imagen que usaremos! Esa nave espacial lleva ahí desde el principio del _sprint_.

### :package: Antes de empezar: Un poco de organización

Como nuestro fichero va a crecer, empecemos organizándolo ahora. **Añade** comentarios[^1] que sirvan para dividir el fichero en secciones así:

```diff
+// Constantes y variables 
+// --------------------------------------------------------------------------------------------------

 const CANVAS = document.getElementById("gameContainer");
 const CANVAS_CTX = CANVAS.getContext("2d");
 CANVAS.width = 640;
 CANVAS.height = 480;
 CANVAS.style.margin = "auto";
 CANVAS.style.display = "block";

+
+// Bucle de renderizado
+// --------------------------------------------------------------------------------------------------

 function renderBackground() {
     CANVAS_CTX.fillStyle = "#RANDOMNUMBER0-2ENDRANDOMNUMBER0-8ENDRANDOMNUMBER0-4ENDRANDOMNUMBER0-9ENDRANDOMEXPRESSION0|1|2|3|4|5|6|7|8|9|aENDRANDOMEXPRESSION0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|fEND";
     CANVAS_CTX.fillRect(0, 0, CANVAS.width, CANVAS.height);
 }
+
+// Código principal
+// --------------------------------------------------------------------------------------------------

 renderBackground();
```

## :package: La tarea

Ahora, **crea** la imagen en la sección de código _"Contantes y variables"_. Con el atributo `src`, **establece** la URL que se indica a continuación:

```js
// Constantes y variables 
// --------------------------------------------------------------------------------------------------

const IMAGES_BASE_PATH = "/projects/images/"; // Esto nos ayuda a tener la ruta donde el servidor sirve las imágenes en un solo sitio, por si cambia
const imageSpaceship = new Image();
imageSpaceship.src = IMAGES_BASE_PATH + 'spaceship.png';

// ...
```

### `drawImage`

Ahora, en la sección _"Código principal"_ **invoca** `drawImage`:

```js
// Código principal
// --------------------------------------------------------------------------------------------------

renderBackground(); 
CANVAS_CTX.drawImage(imageSpaceship, RANDOMNUMBER220-380END, RANDOMNUMBER150-300END);
```

_(Nótese que los números `RANDOMNUMBER220-380END` y `RANDOMNUMBER150-300END` hacen referencia al origen (x, y) donde se dibujará la imagen)._

Si revisas ahora http://localhost:8000/projects/asteroids.html, _no_ verás la nave espacial :sad_face: La imagen todavía _no se ha descargado_ en el momento que invocamos `drawImage`.

1. Se descarga el HTML y el JavaScript.
2. El JS se ejecuta.
3. Intenta hacer `drawImage`, pero `imageSpaceship` todavía no ha descargado el `png`. Falla silenciosamente.

### Esperar a que se descargue la imagen

Usaremos `decode()`. Aunque no profundizaremos en esta sintaxis, a continuación puedes ver cómo usar `.then` junto a una _arrow function_ para escribir código que _espera a que se descargue la imagen_:

Adelante, **modifica** tu código para que sea así:

```js
imageSpaceship.decode().then(() => {
    CANVAS_CTX.drawImage(imageSpaceship, RANDOMNUMBER220-380END, RANDOMNUMBER150-300END);
})
```

Si visitas http://localhost:8000/projects/asteroids.html quizá te _asustes_ (:ghost:). No hay problema. Mira las posibilidades de _sobrecarga_ que tenemos para [invocar `drawImage()`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage#syntax). En vez de usar la primera (que preserva el tamaño original de la imagen), usaremos la segunda, donde se puede especificar _ancho_ y _alto_.

**Modifica** el código como se indica a continuación:

```diff
 // Constantes y variables 
 // --------------------------------------------------------------------------------------------------

 // ...
 
+const SHIP_SIZE = RANDOMNUMBER45-62END;
```

```diff
 // Código principal
 // --------------------------------------------------------------------------------------------------

 // ...
 
 imageSpaceship.decode().then(() => {
-    CANVAS_CTX.drawImage(imageSpaceship, RANDOMNUMBER220-380END, RANDOMNUMBER150-300END);
+    CANVAS_CTX.drawImage(imageSpaceship, RANDOMNUMBER220-380END, RANDOMNUMBER150-300END, SHIP_SIZE, SHIP_SIZE);
 })
```

¿Qué tal ahora?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Dividir un fichero de código en secciones puede ser útil, pero para código que crece mucho más allá, es necesario agruparlo en distintos ficheros. Un código no es demasiado más elegante por tener comentarios. Pero tener nombres de variables autodescriptivos, funciones y clases que no entremezclan lógica... ¡Eso sí es **elegante**!
