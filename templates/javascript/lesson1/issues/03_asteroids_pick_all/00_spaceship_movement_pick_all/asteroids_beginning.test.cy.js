/// <reference types="cypress" />

describe('asteroids_beginning', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8000/');
      cy.reload(true);
    })
  
    it('issueISSUENUMBER index.html is correctly modified and asteroids.html contains canvas', () => {
        cy.get('RANDOMEXPRESSIONul|olEND[data-cy=RANDOMEXPRESSIONprojectsList|listOfProyects|myProjectsEND]')
        .find('li')
        .eq(2)
        .find('a')
        .should('have.text', 'RANDOMEXPRESSIONAsteroides|Asteroids|Nave espacial|Spaceship|Juego|Juego de nave espacial|Juego de asteroides|Asteroids gameEND');

        cy.get('RANDOMEXPRESSIONul|olEND[data-cy=RANDOMEXPRESSIONprojectsList|listOfProyects|myProjectsEND]')
        .find('li')
        .eq(2)
        .find('a')
        .click();

        cy.get('body')
        .find('canvas#gameContainer');

        cy.request('http://localhost:8000/projects/js/asteroids.js');
    })
})
