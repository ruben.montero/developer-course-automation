Bucle del juego

## :books: Resumen

* Separaremos el _estado_ del juego a una variable aparte. ¡No queremos entremezclarlo!
* Hablaremos sobre bucles de _renderizado_ en videojuegos
* Entenderemos `requestAnimationFrame` de JavaScript
* Implementaremos un _game loop_ que de momento no actualiza el estado del juego

## Descripción

En la tarea anterior hemos añadido la línea:

```js
    CANVAS_CTX.drawImage(imageSpaceship, RANDOMNUMBER220-380END, RANDOMNUMBER150-300END, SHIP_SIZE, SHIP_SIZE);
```

...que es responsable de pintar la nave espacial en unas coordenadas determinadas.

Predeciblemente, esas coordenadas van a _variar_ mientras jugamos. ¡No pueden estar _hardcodeadas_!

Vamos a separar el _estado del juego_ (`GAME_STATE`).

### ¿Qué es el _estado del juego_?

Las variables que determinan _cómo_ es el juego para un _instante_ dado. Por ejemplo, si estás jugando a SuperMario y te encuentras en el mundo 5 :globe_with_meridians: con 2 vidas :heart: y 83 monedas :moneybag:, esas variables forman parte del _estado del juego_.

### ¿Por qué separar el estado del juego?

Porque así podemos _calcular_ los cambios que se producen en el _estado del juego_ (el universo del juego) y, en una fase separada, _pintar_ (_renderizar_) dichos cambios.

Esas dos fases tienen lugar en un _bucle del juego_ (_game loop_).

¡Así funcionan los juegos!

### Una primera mala aproximación

Si nadie nos explica _cómo_ escribir un _game loop_ en JavaScript, podemos pensar en una aproximación sencilla. Una función que se repita cíclicamente:

<img src="javascript/docs/bad_game_loop.png" width=450 />

En esa función, _asumimos_ que han transcurrido `200ms` y hacemos los cálculos necesarios para _actualizar_ el _estado del juego_ (`GAME_STATE`).

¡:warning: Es una mala idea! No podemos asumir que nuestro código se ejecutará con una periodicidad _exacta_.

Debemos usar `requestAnimationFrame`.

### ¿[`requestAnimationFrame`](https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame)?

Cuando invocamos `requestAnimationFrame` solicitamos al navegador que _invoque_ una función dada antes del siguiente _ciclo de renderizado_. Como el navegador tiene sus propios _ciclos de renderizado_, cuando usamos `requestAnimationFrame`, estamos _adaptándonos_, ¡así las cosas funcionan bien!

<img src="javascript/docs/game_loop_timestamp.png" width=450 />

El navegador _nos informará_ de cuánto tiempo ha transcurrido inyectando un parámetro a la función que le demos.

## :package: La tarea

**Abre** `asteroids.js`. En la sección _Constantes y variables_ **añade** `GAME_STATE` así:

```js
const GAME_STATE = {
    shipPosX: RANDOMNUMBER220-380END,
    shipPosY: RANDOMNUMBER150-300END,
}
```

Después, **añade** las funciones `draw` y `loop` a la sección _Bucle de renderizado_, como se indican a continuación:

```diff
 // Bucle de renderizado
 // --------------------------------------------------------------------------------------------------
 
 function renderBackground() {
     CANVAS_CTX.fillStyle = "#RANDOMNUMBER0-2ENDRANDOMNUMBER0-8ENDRANDOMNUMBER0-4ENDRANDOMNUMBER0-9ENDRANDOMEXPRESSION0|1|2|3|4|5|6|7|8|9|aENDRANDOMEXPRESSION0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|fEND";
     CANVAS_CTX.fillRect(0, 0, CANVAS.width, CANVAS.height);
 } 
+
+function draw() {
+    renderBackground();
+    CANVAS_CTX.drawImage(imageSpaceship, GAME_STATE.shipPosX, GAME_STATE.shipPosY, SHIP_SIZE, SHIP_SIZE);
+}
+
+function loop(timestamp) {
+
+    draw()
+
+    window.requestAnimationFrame(loop);
+}
```

Como ves, lo que hay en `draw` es lo mismo[^1] que tenemos en la sección _Código principal_, así que **elimina** ese código de ahí.

En su lugar, **sustitúyelo** por una llamada a `requestAnimationFrame` que _ponga en marcha_ el bucle:

```diff
 // Código principal
 // --------------------------------------------------------------------------------------------------

-
-renderBackground();
-imageSpaceship.decode().then(() => {
-    CANVAS_CTX.drawImage(imageSpaceship, RANDOMNUMBER220-380END, RANDOMNUMBER150-300END, SHIP_SIZE, SHIP_SIZE);
-})
+window.requestAnimationFrame(loop);
```

### ¿Qué es `timestamp`?

Ese parámetro que está declarado en `loop` nos lo va  entregar el navegador y es una _marca de tiempo_. Literalmente, un _`timestamp`_, como se explicó al inicio de la tarea.

Pero _no_ nos interesa el _`timestamp`_ absoluto, sino la diferencia de tiempo entre dos _frames_.

### `lastRender` para calcular diferencia de tiempo

**Añade** la variable `lastRender` a la sección _Constantes y variables_:

```diff
 // Constantes y variables 
 // --------------------------------------------------------------------------------------------------
+
+let lastRender = 0;
```

Y **añade** el siguiente código para obtener la _diferencia_ de tiempo entre un _frame_ y el anterior:

```diff
 function loop(timestamp) {
+    const progress = timestamp - lastRender;
+
     draw()
+
+    lastRender = timestamp;
     window.requestAnimationFrame(loop);
 }
```

<img src="javascript/docs/game_loop_progress.png" width=450 />

### Actualizar el _estado del juego_

Ahora, **añade** una nueva sección de código para _Actualizar estado del juego_.

**Escribe** una función `update` que de momento sólo hace un `console.log`, e **invoca** dicha función desde `loop`:

```diff
+// Actualizar estado del juego
+// --------------------------------------------------------------------------------------------------
+
+function update(progressInMilliseconds) {
+    console.log("Han RANDOMEXPRESSIONtranscurrido|pasadoEND " + progressInMilliseconds + " RANDOMEXPRESSIONms desde la última actualización|milisegundos desde la última actualización|ms desde el último frame|milisegundos desde el último frameEND");
+}

 // Bucle de renderizado
 // --------------------------------------------------------------------------------------------------

 // ...
 
 function loop(timestamp) {
     const progress = timestamp - lastRender;
+
+    update(progress);
     draw();
  
     lastRender = timestamp;
     window.requestAnimationFrame(loop);
}
```

¡Nuestro bucle del juego está listo!

Si visitas http://localhost:8000/projects/asteroids.html verás todo estático. Está bien.

En la siguiente tarea trabajaremos en `update` para implementar el motor del juego.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Como esto se va a ejecutar en bucle, nos olvidamos de `.decode().then`. Tarde o temprano el `.png` se descargará. No nos supone un grave problema si en los primeros _frames_ aún no está disponible.

