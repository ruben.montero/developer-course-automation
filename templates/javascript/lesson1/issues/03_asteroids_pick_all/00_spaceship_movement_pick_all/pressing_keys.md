¡Movimiento!

## :books: Resumen

* Añadiremos `pressedKeys` al estado del juego
* Usaremos el API de entrada de usuario para modificar `pressedKeys` en función de si se pulsa W, A, S, D
* Actualizaremos la posición de la nave espacial en función de las teclas pulsadas

## Descripción

Vamos a usar el [API de eventos UI](https://developer.mozilla.org/en-US/docs/Web/API/UI_Events) para _saber_ si el usuario ha pulsado teclas.

Simplemente, escucharemos por los eventos `"keydown"` y `"keyup"`, que tienen lugar cada vez que una tecla se _pulsa_ o se _suelta_:

## :package: La tarea

**Añade** el siguiente código para escuchar los eventos `keydown` y `keyup`:

```diff
 // Código principal
 // --------------------------------------------------------------------------------------------------

 // ...
+
+window.addEventListener("keydown", onKeyDown);
+window.addEventListener("keyup", onKeyUp);
```

...y **crea** las correspondientes funciones `onKeyDown` y `onKeyUp` en una **nueva** sección _Entrada del usuario_:

```js
// Entrada del usuario
// --------------------------------------------------------------------------------------------------

function onKeyDown(event) {
    console.log("Has pulsado la tecla con código: " + event.keyCode);
    
}

function onKeyUp(event) {
    console.log("Has soltado la tecla con código: " + event.keyCode);
    
}
```

Luego, **modifica** `GAME_STATE` para incluir `pressedKeys`:

```js
const GAME_STATE = {
    shipPosX: RANDOMNUMBER220-380END,
    shipPosY: RANDOMNUMBER150-300END,
    pressedKeys: {
        left: false,
        right: false,
        up: false,
        down: false
    },
}
```

...y **añade** una función que mantiene `pressedKeys` actualizado[^1]. **Estúdiala** un rato antes de añadirla:

```js
// Entrada del usuario
// --------------------------------------------------------------------------------------------------

function setPressedKey(keyCode, isPressed) {
    if (keyCode === 68) {
        GAME_STATE.pressedKeys['right'] = isPressed; 
    }
    if (keyCode === 65) {
        GAME_STATE.pressedKeys['left'] = isPressed; 
    }
    if (keyCode === 87) {
        GAME_STATE.pressedKeys['up'] = isPressed; 
    }
    if (keyCode === 83) {
        GAME_STATE.pressedKeys['down'] = isPressed; 
    }
}

function onKeyDown(event) {
    console.log("Has pulsado la tecla con código: " + event.keyCode);
    setPressedKey(event.keyCode, true);
}

function onKeyUp(event) {
    console.log("Has soltado la tecla con código: " + event.keyCode);
    setPressedKey(event.keyCode, false);
}
```

### Actualizar el _estado del juego_

¡Aquí viene la parte emocionante!

**Añade** la función `updateSpaceship` e invócala desde `update`, **así**:

```js
// Actualizar estado del juego
// --------------------------------------------------------------------------------------------------

// Nueva función
function updateSpaceship(timeDelta) {

}

function update(progressInMilliseconds) {
    console.log("Han RANDOMEXPRESSIONtranscurrido|pasadoEND " + progressInMilliseconds + " RANDOMEXPRESSIONms desde la última actualización|milisegundos desde la última actualización|ms desde el último frame|milisegundos desde el último frameEND");
    const progressInSeconds = progressInMilliseconds / 1000;
    updateSpaceship(progressInSeconds);
    
}
```

### Velocidad

Ahora, **añade** esta _constante_ en la parte superior:

```js
const VELOCITY = RANDOMNUMBER80-110END; // pxs / sec
```

Son _píxeles_ por _segundo_.

### V = s/t

```
Velocidad = espacio / tiempo
```

Despejamos la ecuación suponiendo que `espacio` es nuestra variable de interés:

```
Espacio = velocidad * tiempo
```

Es decir: Si vamos en coche a 120 km/h, entonces, en 2 horas recorremos 240 kilómetros. ¿No?

Nosotros usamos _píxeles_ y _segundos_, pero la fórmula es la misma.

**Añade** la siguiente línea a tu `updateSpaceship`:

```js
function updateSpaceship(timeDelta) {
    const distanceTraveled = VELOCITY * timeDelta;
    
}
```

¡Y ahora **actualiza** la posición de la nave en el `GAME_STATE` de manera acorde, si el usuario pulsó la tecla para ir a la derecha!

```js
function updateSpaceship(timeDelta) {
    const distanceTraveled = VELOCITY * timeDelta;
    
    if (GAME_STATE.pressedKeys.right) {
        GAME_STATE.shipPosX = GAME_STATE.shipPosX + distanceTraveled;
    }
    
}
```

**Comprueba** el movimiento de tu nave en http://localhost:8000/projects/asteroids.html. ¿Puedes moverte a la derecha con la tecla `D`?

### ¡Sí!

¡Genial!

Pues ahora **completa** `updateSpaceship` para poder desplazarte en cualquier dirección:

```js
function updateSpaceship(timeDelta) {
    const distanceTraveled = VELOCITY * timeDelta;

    if (GAME_STATE.pressedKeys.right) {
        GAME_STATE.shipPosX = GAME_STATE.shipPosX + distanceTraveled;
    }
    if (GAME_STATE.pressedKeys.left) {
        // ...
    }
    if (GAME_STATE.pressedKeys.up) {
        // ...
    }
    if (GAME_STATE.pressedKeys.down) {
        // ...
    }
}
```
 
### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: `68`, `65`, `87`, `83` son los códigos para `D`, `A`, `W` y `S`, respectivamente. Si quieres saber, por ejemplo, _cuál_ es el carácter correspondiente al código `RANDOMNUMBER60-100END`, o _cuáles_ son otros códigos, puedes curiosear [aquí](https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes)
