En el año 2651

## :books: Resumen

* Presentaremos _Asteroids_
* Crearemos la página inicial de nuestro nuevo proyecto, que usará `<canvas>`
* Añadiremos un enlace desde `index.html`

## Descripción

_En el año 2651 la esperanza ha desaparecido para la humanidad. Hace más de un siglo que la polución hizo imposible la vida basada en el carbono dentro del planeta Tierra. Hemos escapado de nuestro hogar en busca de un exoplaneta habitable a 614 años luz._

Esta no es la realidad, sino la historia que algún usuario podría imaginarse cuando juegue a nuestro nuevo proyecto _Asteroids_:

<img src="javascript/docs/asteroids_demo.gif" />

Usaremos el [API _Canvas_](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API), que provee lo necesario para dibujar gráficos (principalmente 2D) usando el elemento `<canvas>` de HTML. 

### Una nota importante

Los conceptos con los que trabajaremos sobre _coordenadas_, dibujar figuras, bucles de _renderizado_... ¡Son muy útiles! No se limitan a JavaScript. Este tipo de _trabajo 2D_ suele aparecer en prácticamente cualquier entorno de videojuegos.

## :package: La tarea

**Añade** un archivo `asteroids.html` dentro de `projects/`:

```html
<html>
  <head>
    <meta charset="utf8"/>
    <script defer src="/projects/js/asteroids.js"></script>
  </head>
  <body>
    <canvas id="gameContainer">
    </canvas>
  </body>
</html>
```

¡Enhorabuena! Has añadido _todo_ el HTML (y CSS) que contendrá nuestro proyecto. Todo lo que escribiremos después, será JavaScript.

**Añade** el archivo `asteroids.js` a `projects/js/`. Estará vacío de momento.

A continuación, **añade** un enlace `<a href>` en `index.html`. Igual que en los proyectos anteriores, **escríbelo** dentro de un `<li>` en la lista principal.

**Haz** que _dirija_ (href) a `"/projects/asteroids.html"` y muestre el texto `RANDOMEXPRESSIONAsteroides|Asteroids|Nave espacial|Spaceship|Juego|Juego de nave espacial|Juego de asteroides|Asteroids gameEND`. Visítalo en http://localhost:8000 y asegúrate que funciona. 

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
