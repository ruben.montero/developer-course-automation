Generando asteroides (I)

## :books: Resumen

* Añadiremos un _array_ al estado del juego, que contendrá la información de cada asteroide
* Generaremos un asteroide periódicamente. De momento, no se verá nada

## Descripción

Nuestro juego, _Asteroids_, sería un poco vergonzoso si no tuviera asteroides.

En esta tarea, vamos a generar asteroides periódicamente. Pero en JavaScript no existe una función `sleep` como en otros lenguajes de programación. Para programar una _demora_ en la aparición de cada asteroide, usaremos `setTimeout`.

### [`setTimeout`](https://developer.mozilla.org/en-US/docs/Web/API/setTimeout)

Cuando la usamos, especificamos una _demora_ (en milisegundos) y el nombre de una función.

La función se ejecutará después de los milisegundos especificados.

### Posición y velocidad

Los asteroides estarán siempre, inicialmente, en la posición `640`x`480`, correspondiente a la esquina inferior izquierda.

Como cada asteroide va a tener un vector de velocidad aleatorio, podrá salir por cualquiera de las esquinas.

<img src="javascript/docs/asteroid_generation.png" width=400 />

## :package: La tarea

**Añade** el _array_ `asteroids`, inicialmente vacío, al `GAME_STATE`:

```js
const GAME_STATE = {
    shipPosX: RANDOMNUMBER220-380END,
    shipPosY: RANDOMNUMBER150-300END,
    pressedKeys: {
        left: false,
        right: false,
        up: false,
        down: false
    },
    asteroids: [], // Nuevo array en el GAME_STATE
}
```

Luego, **añade** la nueva sección _Asteroides y colisiones_ a tu código, y en ella, **añade** la función `generateAsteroid`:

```js
// Asteroides y colisiones
// --------------------------------------------------------------------------------------------------

function generateAsteroid() {
    const randomVelocityX = Math.trunc(Math.random() * 100) - 50; // [-50, +50]
    const randomVelocityY = Math.trunc(Math.random() * 100) - 50; // [-50, +50]
    const newAsteroid = {
        "posX": CANVAS.width,
        "posY": CANVAS.height,
        "velX": randomVelocityX,
        "velY": randomVelocityY
    };
    GAME_STATE.asteroids.push(newAsteroid);
    console.log('RANDOMEXPRESSIONSe ha generado un nuevo asteroide|Se ha creado un nuevo asteroide|Se ha añadido un nuevo asteroide|Se ha generado un asteroide|Se ha creado un asteroide|Se ha añadido un asteroideEND');
    console.log('velX=' + randomVelocityX);
    console.log('velY=' + randomVelocityY);
}
```

* `randomVelocityX` y `randomVelocityY` representan la velocidad del asteroide. Cada asteroide viajará a una velocidad distinta, por ello, en la información de cada uno debemos almacenar (separadamente) dicha velocidad. Los asteroides tendrán [movimiento rectilíneo uniforme](https://www.fisicalab.com/apartado/mru)
* `newAsteroid = { ... }` crea un objeto (JSON) con la información del asteroide
* `"posX"` y `"posY"` son la posición (**x** e **y**) del asteroide. Inicialmente, siempre es la esquina inferior derecha, donde no es visible.

### ¡A generar asteroides!

Ahora, **añade** la siguiente línea a _Código principal_ para invocar `generateAsteroid()` inicialmente:

```js
// Código principal
// --------------------------------------------------------------------------------------------------

// ...

generateAsteroid();
``` 

¡Ya se genera el primer asteroide!

Para que se sigan generando un asteroide tras otro, **completa** la función `generateAsteroid` **añadiendo** esta línea _al final_:

```diff
 function generateAsteroid() {

     // ...
+
+    // Está permitido que una función se invoque a sí misma despues de X milisegundos, usando setTimeout
+   setTimeout(generateAsteroid, RANDOMEXPRESSION5000|6000|7000|8000|9000END);
 }
```

¡Listo!

Así, cada `RANDOMEXPRESSION5000|6000|7000|8000|9000END` milisegundos la función `generateAsteroid` es invocada, y cuando se ejecuta (añade un asteroide) vuelve a preparar que `generateAsteroid` se vuelva a invocar `RANDOMEXPRESSION5000|6000|7000|8000|9000END` milisegundos después. ¡Parece recursividad!

Aunque todavía no se _renderizan_ los asteroides, puedes comprobar en la _Consola_ del navegador que se están _generando_.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
