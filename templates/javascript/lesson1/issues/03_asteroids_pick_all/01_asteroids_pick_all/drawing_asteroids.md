Generando asteroides (II)

## :books: Resumen

* Añadiremos una función `updateAsteroid`. Actualizará la posición de cada asteroide en función de su velocidad
* Crearemos un nuevo `Image` para la _imagen_ del asteroide
* _Renderizaremos_ cada uno de los asteroides según su posición

## Descripción

Recordemos que la función `update` se encarga de calcular el avance del _universo_ del juego entre _frame_ y _frame_.

```js
function update(progressInMilliseconds) {
    console.log("Han RANDOMEXPRESSIONtranscurrido|pasadoEND " + progressInMilliseconds + " RANDOMEXPRESSIONms desde la última actualización|milisegundos desde la última actualización|ms desde el último frame|milisegundos desde el último frameEND");
    const progressInSeconds = progressInMilliseconds / 1000;
    updateSpaceship(progressInSeconds);
    
}
```

En esta tarea la actualizaremos para que calcule el avance de cada asteroide.

Y, en la función `draw`... ¡Nos encargaremos de _renderizarlos_!

## :package: La tarea

**Añade**:

```diff
+function updateAsteroid(asteroid, timeDelta) {
+    asteroid.posX += asteroid.velX * timeDelta;
+    asteroid.posY += asteroid.velY * timeDelta;
+}

 function update(progressInMilliseconds) {
     console.log("Han RANDOMEXPRESSIONtranscurrido|pasadoEND " + progressInMilliseconds + " RANDOMEXPRESSIONms desde la última actualización|milisegundos desde la última actualización|ms desde el último frame|milisegundos desde el último frameEND");
     const progressInSeconds = progressInMilliseconds / 1000;
     updateSpaceship(progressInSeconds);
+
+    for (const asteroid of GAME_STATE.asteroids) {
+        updateAsteroid(asteroid, progressInSeconds);
+    }
 }
```

Como ves, en `updateAsteroid` se usa `espacio = velocidad * tiempo` para calcular la nueva posición en un par de líneas[^1]

### Si sale por un lado... ¡que entre por el otro!

<img src="javascript/docs/asteroid_looping.png" width=450 />

**Añade** lo siguiente:

```diff
 // Constantes y variables 
 // --------------------------------------------------------------------------------------------------

 // ...
 
+const ASTEROID_SIZE = RANDOMNUMBER60-100END;
```

Y ahora, **completa** la función `updateAsteroid` para que tenga en cuenta los 4 posibles escenarios donde un asteroide sale por la pantalla:

```js
function updateAsteroid(asteroid, timeDelta) {
    asteroid.posX += asteroid.velX * timeDelta;
    asteroid.posY += asteroid.velY * timeDelta;

    if (asteroid.posX > CANVAS.width) {
        asteroid.posX -= (CANVAS.width + ASTEROID_SIZE)
    } else if (asteroid.posX < -ASTEROID_SIZE) {
        asteroid.posX += CANVAS.width + ASTEROID_SIZE
    } else if (asteroid.posY > CANVAS.height) {
        asteroid.posY -= (CANVAS.height + ASTEROID_SIZE)
    } else if (asteroid.posY < -ASTEROID_SIZE) {
        asteroid.posY += CANVAS.height + ASTEROID_SIZE
    }
}
```

### _Renderizando_ asteroides

**Añade** las siguientes dos líneas:

```diff
 // Constantes y variables 
 // --------------------------------------------------------------------------------------------------

 // ...
 
+const imageAsteroid = new Image();
+imageAsteroid.src = IMAGES_BASE_PATH + 'asteroid.png';
```

Y ahora, **añade** un bucle `for...of`[^2] que recorra el _array_ con la de asteroides y los dibuje apropiadamente.

```diff
 function draw() {
     renderBackground();
+    for (const asteroid of GAME_STATE.asteroids) {
+        CANVAS_CTX.drawImage(imageAsteroid, asteroid.posX, asteroid.posY, ASTEROID_SIZE, ASTEROID_SIZE);
+    }
     CANVAS_CTX.drawImage(imageSpaceship, GAME_STATE.shipPosX, GAME_STATE.shipPosY, SHIP_SIZE, SHIP_SIZE);
 }
```

**Verifica** el resultado en http://localhost:8000/projects/asteroids.html

Debes ver... ¡asteroides!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: El operador `+=` es una forma abreviada de escribir un _incremento_. En otras palabras, `a = a + b` es equivalente a `a += b`. Por lo tanto, `posX += velX * time` es lo mismo que `posX = posX + (velX * time)`

[^2]: El bucle `for...of` es como un bucle _for-each_ de Java.
