import { generateSolidBackgroundImageTo } from './utils'

describe('endgame_image', () => {
  
    it('issueISSUENUMBER image', () => {
        const endgameComparisonImage = 'cypress/images/emptyBackgroundEndgame.png'
        generateSolidBackgroundImageTo(endgameComparisonImage, 640, 480, '#00RANDOMNUMBER2-9ENDRANDOMNUMBER1-8ENDRANDOMNUMBER6-9ENDRANDOMNUMBER5-9END').then(() => {
            const filenames = {
                actual: 'www/projects/images/RANDOMEXPRESSIONgameover|endgame|gamefinished|game_endEND.png',
                expected: endgameComparisonImage
            }
            cy.task('compare', { filenames }).then($test => {
                console.log($test); // Example: Object { match: false, reason: "pixel-diff", diffCount: 55612, diffPercentage: 88.9792 }
                expect($test.diffPercentage).to.be.lessThan(80);
            })
        });
    })

})
