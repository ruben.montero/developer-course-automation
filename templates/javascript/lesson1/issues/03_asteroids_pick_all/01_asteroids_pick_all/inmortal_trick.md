Que el juego no termine

## :books: Resumen

* Añadiremos un pequeño truco para que el juego no termine nunca

## Descripción

Los _query params_ son parámetros que viajan en la URL de una petición HTTP.

Ahora mismo, puedes visitar tu juego desde:

* http://localhost:8000/projects/asteroids.html

Pues esa misma URL, con un _query param_ `inmortal` de valor `true` es:

* http://localhost:8000/projects/asteroids.html?inmortal=true

## :package: La tarea

**Incluye** este código en tu `asteroids.js`:

```js
function isInmortalTrickActive() {
    return window.location.search === '?inmortal=true';
} 
```

Y **modifica** esta línea en `update`:

```diff
-        if (isCrashing(asteroid)) {
+        if (isCrashing(asteroid) && !isInmortalTrickActive()) {
```

**Juega** ahora desde http://localhost:8000/projects/asteroids.html?inmortal=true.

¿Cuánto tiempo eres capaz de sobrevivir ahora? :smile:

* Nota: Este cambio favorecerá reducir _flakiness_ en algún _test_ anterior.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Por lo general, es buena práctica usar un prefijo `is` para nombres de variables y parámetros de tipo _booleano_. `gameOver` podría ser una variable tipo JSON con datos del final de la partida, mientras que `isGameOver` resulta menos confuso.
