¡Esquívalo!

## :books: Resumen

* Añadiremos un _flag_ al estado del juego, `isGameOver`
* Cada vez que actualicemos la posición de un asteroide, verificaremos si está _en colisión_ con la nave espacial
* En caso afirmativo, estableceremos `isGameOver=true`
* Cuando esto suceda, se detendrá el bucle del juego y mostraremos la imagen `RANDOMEXPRESSIONgameover|endgame|gamefinished|game_endEND.png`

## Descripción

Sólo nos falta que la partida termine cuando hay una colisión.

Eso de calcular las colisiones es un problema matemático recurrente en informática. En nuestro caso, vamos a implementar una sencilla función que compruebe si un _punto_ está dentro de un _rectángulo_:

<img src="javascript/docs/point_inside_rectangle.png" width=300 />

Y si el punto medio de la nave espacial _entra_ en un rectángulo de un asteroide... ¡Colisión!

<img src="javascript/docs/asteroid_collision_explanation.png" width=400 />

## :package_ La tarea

**Añade** un _booleano_ al `GAME_STATE`, llamado `isGameOver`[^1]:

```js
const GAME_STATE = {
    shipPosX: RANDOMNUMBER220-380END,
    shipPosY: RANDOMNUMBER150-300END,
    pressedKeys: {
        left: false,
        right: false,
        up: false,
        down: false
    },
    asteroids: [],
    isGameOver: false // Nuevo booleano
}
```

Ahora, en `update`, **añade** este código para verificar que _cada asteroide_ está (o no) colisionando con la nave espacial:

```diff
 function update(progressInMilliseconds) {
     console.log("Han RANDOMEXPRESSIONtranscurrido|pasadoEND " + progressInMilliseconds + " RANDOMEXPRESSIONms desde la última actualización|milisegundos desde la última actualización|ms desde el último frame|milisegundos desde el último frameEND");
     const progressInSeconds = progressInMilliseconds / 1000;
     updateSpaceship(progressInSeconds);

     for (const asteroid of GAME_STATE.asteroids) {
         updateAsteroid(asteroid, progressInSeconds);
+        if (isCrashing(asteroid)) {
+           console.log("RANDOMEXPRESSIONPartida terminada|Partida finalizada|Ha habido una colisión|Ha habido un choque|La nave espacial ha sufrido una colisión|La nave espacial ha explotado|Fin de la partida|Se ha producido un accidente, por favor, cubra los datos de su seguro espacialEND");
+           GAME_STATE.isGameOver = true;
+        }
     }
 }
```

A continuación **añade** la función `isCrashing`:

```js
// Asteroides y colisiones
// --------------------------------------------------------------------------------------------------

function isCrashing(asteroid) {

}
```

Esta función va a recibir por parámetro un JSON representando la información de un asteroide y devolverá un _booleano_ indicando si hay colisión o no.

Para ello, también **añade** una función que sirva para identificar si un _punto_ se halla dentro de un _rectángulo_:

```js
function isPointInsideRectangle(pointX, pointY, rectangleX, rectangleY, rectangleWidth, rectangleHeight) {
    const rectangleLeftSide = rectangleX;
    const rectangleRightSide = rectangleX + rectangleWidth;
    const rectangleTopSide = rectangleY;
    const rectangleBottomSide = rectangleY + rectangleHeight;
    return (pointX > rectangleLeftSide) && (pointX < rectangleRightSide) && (pointY > rectangleTopSide) && (pointY < rectangleBottomSide);
}
```

_(¿Entiendes cómo funciona?)_

Y ahora... **invoca** `isPointInsideRectangle` desde `isCrashing` pasando los valores adecuados:

```js
function isCrashing(asteroid) {
    const spaceshipMidX = GAME_STATE.shipPosX + (SHIP_SIZE / 2);
    const spaceshipMidY = GAME_STATE.shipPosY + (SHIP_SIZE / 2);
    return isPointInsideRectangle(spaceshipMidX, spaceshipMidY, asteroid.posX, asteroid.posY, ASTEROID_SIZE, ASTEROID_SIZE);
}
```

### Terminando la partida

Ahora nuestro _flag_ `isGameOver` se actualizará a `true` en cuanto un meteorito choque contra la nave espacial. Cuando eso suceda, detendremos el _game loop_.

Para ello, **añade** esta condición de _guarda_ a `loop`.

```js
function loop(timestamp) {
    if (GAME_STATE.isGameOver) { // Condición de guarda
        return;
    }
    
    // ...
}
```

Y **añade** una condición de _guarda_ similar a `generateAsteroid`:

```js
function generateAsteroid() {
    if (GAME_STATE.isGameOver) { // Condición de guarda
        return;
    }
    
    // ...
}
```

Y no nos olvidemos... ¡de pintar la pantalla de _Game Over_!

En primer lugar, **añade** la nueva `Image`:

```js
// Constantes y variables 
// --------------------------------------------------------------------------------------------------

// ...

const imageGameover = new Image();
imageGameover.src = IMAGES_BASE_PATH + 'RANDOMEXPRESSIONgameover|endgame|gamefinished|game_endEND.png';
```

...y **úsala** en `draw`:

```js
function draw() {
    if (GAME_STATE.isGameOver) {
        CANVAS_CTX.drawImage(imageGameover, 0, 0, CANVAS.width, CANVAS.height);
        return;
    }
    
    // ...
}
```

**¡Listo!**

Puedes probar tu juego: http://localhost:8000/projects/asteroids.html

Tu juego está casi terminado, ¡enhorabuena!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: Por lo general, es buena práctica usar un prefijo `is` para nombres de variables y parámetros de tipo _booleano_. `gameOver` podría ser una variable tipo JSON con datos del final de la partida, mientras que `isGameOver` resulta menos confuso.
