Fin de partida

## :books: Resumen

* Crearemos una imagen para cuando la partida termine
* La subiremos a `www/projects/images/`

## Descripción

Vamos a prepararnos para el final de la partida.

## :package: La tarea

**Crea** una imagen con tu herramienta favorita ([Microsoft Paint](https://support.microsoft.com/es-es/windows/ayuda-en-paint-d62e155a-1775-6da4-0862-62a3e9e5a511), [Gimp](https://www.gimp.org/),...) cumpliendo los siguientes requisitos:

* Debe tener dimensiones `640x480` (_ancho_ x _alto_)
* Debe tener un **fondo** (que ocupe al menos el 20% del total de píxeles) de color `#00RANDOMNUMBER2-9ENDRANDOMNUMBER1-8ENDRANDOMNUMBER6-9ENDRANDOMNUMBER5-9END`
* Debe indicar con un texto que la partida ha terminado. Por ejemplo: _Game Over!_ o _Fin de la partida :(_
* Debe indicar con un texto que se debe **refrescar** la página para volver a jugar. Por ejemplo: _Press F5 to play again_
* Debes tener formato **png** y llamarse `RANDOMEXPRESSIONgameover|endgame|gamefinished|game_endEND.png`

No se valorará la calidad gráfica, pero puedes trabajar en tu imagen todo lo que quieras.

Esta será la imagen que mostraremos en la tarea siguiente cuando la partida termine.

De momento, tan sólo **súbela** a tu repositorio, dentro de la carpeta `www/projects/images/`

### ¡Compruébala!

Debería estar accesible desde aquí: http://localhost:8000/projects/images/RANDOMEXPRESSIONgameover|endgame|gamefinished|game_endEND.png

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
