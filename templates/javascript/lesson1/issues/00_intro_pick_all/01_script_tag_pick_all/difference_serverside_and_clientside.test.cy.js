/// <reference types="cypress" />

describe('difference_serverside_and_clientside', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8000/');
      cy.reload(true); // Force-reload to prevent cache errors
    })
  
    it('issueISSUENUMBER p1 is changed', () => {
        cy.get('p[data-cy=paragraph1]')
        .should('have.text', 'RANDOMEXPRESSIONEste mensaje ha sido puesto aquí mediante JavaScript|Este texto ha sido puesto aquí mediante JavaScript|Este contenido ha sido puesto aquí mediante JavaScript|Este mensaje ha sido puesto aquí con JavaScript|Este texto ha sido puesto aquí con JavaScript|Este contenido ha sido puesto aquí con JavaScriptEND');
    })
})
