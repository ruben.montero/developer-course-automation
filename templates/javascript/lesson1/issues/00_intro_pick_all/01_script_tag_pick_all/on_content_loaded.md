DOMContentLoaded

## :books: Resumen

* Expondremos una limitación de `async` y `defer`
* Veremos una alternativa, _escuchar_ el evento `DOMContentLoaded`
* Hablaremos sobre _listeners_
* Añadiremos un `<script>` nuevo a `index.html`

## Descripción

`async` y `defer` son dos atributos de `<script>` que sirven para indicar al navegador _web_ que _no_ bloquee la carga de la página por culpa de los _scripts_.

Pero sólo funcionan para _scripts_ externos.

Es decir:

```html
<html>
  <head>
    <script defer>
      // El siguiente script no funcionará correctamente
      //
      // defer y async NO sirven para código JavaScript interno en la página
      //
      document.getElementById("p1").innerHTML = "Texto modificado";
    </script>
  </head>
  <body>
    <p id="p1">Texto original</p>
  </body>
</html>
```

### ¡Pues usemos **siempre** _scripts_ externos!

Esa es una solución.

Pero a veces, el código que tenemos es pequeño y queremos _ahorrarnos_ la descarga de un archivo `.js` externo.

Para ese _y otros_ escenarios, podemos emplear la siguiente herramienta:

```html
<html>
  <head>
    <script>
      // Esto SÍ funciona como se espera
      // En la página verás "Texto modificado"
      document.addEventListener("DOMContentLoaded", function() {
      
        document.getElementById("p1").innerHTML = "Texto modificado";
      });
    </script>
  </head>
  <body>
    <p id="p1">Texto original</p>
  </body>
</html>
```

Expliquémoslo poco a poco:

### _Listeners_

Al usar `document.addEventListener` estamos indicando:

1. El nombre de un evento.
2. Código (una `function`) que se ejecutará _cuando tenga lugar el evento_.

Por lo tanto el código no se ejecutará de forma _secuencial_, es decir, de _arriba_ a _abajo_ (como estamos acostumbrados).

¡Cada `function` será invocada por el sistema cuando su evento correspondiente ocurra!

### `DOMContentLoaded`

Este _evento_ significa _Cuando el DOM (página) se ha cargado_.[^1]

### Por lo tanto...

Usando:

```
document.addEventListener("DOMContentLoaded", function() {
  // Código JS
});
```

...garantizamos que nuestro `// Código JS` se ejecuta tras la carga del HTML.

## :package: La tarea

**Añade** otro `<script>` en `<head>` de `index.html` (¡no borres lo de las tareas anteriores!).

Tendrá una función que **se ejecutará en `DOMContentLoaded`**. El código interno de la `function` será:

```javascript
    document.getElementById("p3").innerHTML = "RANDOMEXPRESSIONEsto ha cambiado en DOMContentLoaded|Esto se ha modificado en DOMContentLoaded|Esto se ha alterado en DOMContentLoaded|Esto se ha manipulado en DOMContentLoadedEND";
```

¿Ves el tercer párrafo de la página con el texto actualizado?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

------------------------------------------------------------------------

[^1]: `DOM` significa [Document Object Model](https://developer.mozilla.org/es/docs/Glossary/DOM). Es una _representación_ del documento HTML.

