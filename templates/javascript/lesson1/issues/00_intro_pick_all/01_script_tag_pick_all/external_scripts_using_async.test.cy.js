/// <reference types="cypress" />

describe('external_scripts_using_async', () => {
    it('issueISSUENUMBER external js 1 has been created', () => {
        cy.request('http://localhost:8000/asyncRANDOMEXPRESSIONExample1|Tiny1|Small1END.js');
    })

    it('issueISSUENUMBER external js 2 has been created', () => {
        cy.request('http://localhost:8000/asyncRANDOMEXPRESSIONExample1|Tiny1|Small1END.js');
    })
    
    it('issueISSUENUMBER console logs are ok', () => {
        cy.visit('http://localhost:8000/index.html');
        cy.reload(true); // Force-reload to prevent cache errors
        cy.visit('http://localhost:8000/index.html', {
            onBeforeLoad (win) {
                cy.stub(win.console, 'log').as("consoleLog").log(false);
              },
        })
        
        cy.get('@consoleLog').should('be.calledWith', "Se está ejecutando asyncRANDOMEXPRESSIONExample1|Tiny1|Small1END");
        cy.get('@consoleLog').should('be.calledWith', "Se está ejecutando asyncRANDOMEXPRESSIONExample2|Tiny2|Small2END");
    })
  })
