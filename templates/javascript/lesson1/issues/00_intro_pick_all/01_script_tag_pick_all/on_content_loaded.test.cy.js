/// <reference types="cypress" />

describe('on_content_loaded', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8000/');
      cy.reload(true); // Force-reload to prevent cache errors
    })
  
    it('issueISSUENUMBER p3 is changed', () => {
        cy.get('p[data-cy=paragraph3]')
        .should('have.text', 'RANDOMEXPRESSIONEsto ha cambiado en DOMContentLoaded|Esto se ha modificado en DOMContentLoaded|Esto se ha alterado en DOMContentLoaded|Esto se ha manipulado en DOMContentLoadedEND');
    })
})
