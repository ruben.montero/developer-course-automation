/// <reference types="cypress" />

describe('external_scripts_defer', () => {
    it('issueISSUENUMBER external js has been created', () => {
        cy.request('http://localhost:8000/RANDOMEXPRESSIONexample|tiny|smallEND.js');
    })
  
    it('issueISSUENUMBER p2 is changed', () => {
        cy.visit('http://localhost:8000/index.html');
        cy.reload(true); // Force-reload to prevent cache errors

        cy.get('p[data-cy=paragraph2]')
        .should('have.text', 'RANDOMEXPRESSIONModificado con un script externo|Alterado con un script externo|Manipulado con un script externo|Cambiado con un script externo|Modificado mediante un script externo|Alterado mediante un script externo|Manipulado mediante un script externo|Cambiado mediante un script externoEND');
    })
})
