Servidor vs. cliente

## :books: Resumen

* Veremos la diferencia entre programar páginas web con código que se ejecuta en el servidor o código que se ejecuta en el cliente
* Introduciremos JavaScript
* Añadiremos un sencillo código JavaScript en `index.html`
* _(Y como siempre, subiremos nuestros cambios al repositorio)_

## Descripción

Hasta ahora disponemos de una carpeta `www/` donde residen nuestras páginas _web_ y son entregadas al cliente en el momento que las solicita.

Esta es la aproximación más tradicional del mundo _web_: Mi servidor _sirve_ ficheros.

### ¡Qué poco dinámico!

¡Es verdad! En esta aproximación, el HTML es estático.

Para mejorar un poco la situación, surgieron lenguajes de programación servidor, como [PHP](https://www.php.net/). El código escrito en estos lenguajes se ejecuta en el servido y genera un resultado en HTML, que luego se envía al cliente.

Por ejemplo, en [PHP](https://www.php.net/) puedes embeber código ejecutable entre las etiquetas `<?php` y `?>`. El servidor lo procesará y generará un HTML:

<img src="javascript/docs/php_example.png" width=550 />

Pero a pesar de que hay muchas páginas escritas en PHP (u otros lenguajes de lado servidor), tiene sus desventajas. ¡Toda la carga de trabajo se la lleva el servidor!

### ¿Por qué darle todo el trabajo duro al servidor?

Eso, ¿por qué?

### ¿No sería mejor repartir un poco?

¡Buena idea!

En la **programación _web_ lado cliente**, se entrega **código ejecutable al cliente (navegador)**.

El **navegador _web_** es el encargado de **ejecutar el código**. ¡Así, el procesamiento _se distribuye a los clientes_ y nuestros servidores no sufren tanta carga!

### ¿Cómo se programa **lado cliente**?

Deberemos entregar al cliente algo más que HTML...

## [JavaScript](https://www.javascript.com/)

Es un lenguaje que lleva con nosotros más de 25 años y todos los navegadores _web_ son capaces de entender y ejecutar. A pesar del nombre, no se parece a Java.

¡Adelante! **Prueba** el breve tutorial introductorio a JavaScript que no te llevará ni un minuto:

* [Try JavaScript!](https://www.javascript.com/try)

## :package: La tarea

**Modifica** tu archivo `index.html` y añade `<script> </script>`[^1] _justo antes_ de la etiqueta de cierre `</body>`:

```diff
     ...
+    <script> 
+    </script>
   </body>
 </html>
```

Luego, **añade** la siguiente línea de código JavaScript dentro del `<script></script>`:

```html
    <script>
      document.getElementById("p1").innerHTML = "RANDOMEXPRESSIONEste mensaje ha sido puesto aquí mediante JavaScript|Este texto ha sido puesto aquí mediante JavaScript|Este contenido ha sido puesto aquí mediante JavaScript|Este mensaje ha sido puesto aquí con JavaScript|Este texto ha sido puesto aquí con JavaScript|Este contenido ha sido puesto aquí con JavaScriptEND";
    </script>
```

¿Qué ves al visitar http://localhost:8000?

¿Cuál es el contenido del `<p>` con `id="p1"`?

¿Tiene sentido?

### :trophy: Por último

Haz `git add *` y `git commit -m "Tarea #3, cambiar p1 de index.html con JavaScript"` para crear un _commit_. Sube tus cambios al repositorio con `git push`.

-----------------------------------------------------------------------

[^1]: En la versión 4 de HTML era obligatorio especificar el atributo `type`, como `<script type="text/javascript>"`. Sin embargo, desde la versión definitiva de HTML 5 publicada en octubre de 2014, no es obligatorio.
