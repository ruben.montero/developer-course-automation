defer vs. async

## :books: Resumen

* Veremos la diferencia entre `async` y `defer`
* Añadiremos dos nuevos ficheros `asyncRANDOMEXPRESSIONExample1|Tiny1|Small1END.js` y `asyncRANDOMEXPRESSIONExample2|Tiny2|Small2END.js` y los enlazaremos a `index.html`, indicando `async`

## Descripción

`defer` es una palabra reservada muy útil para que los `<script>` se carguen de forma _paralela_ de cargar el HTML, pero no es la única.

¡También existe `async`!

### `defer` vs. `async`

* Si especificamos varios _scripts_ con `defer`, tenemos garantizado que se ejecutarán _por orden_ y _después de cargar el HTML_:

```html
<html>
  <head>
    <script defer src="script1.js"></script>
    <script defer src="script2.js"></script>
    <script defer src="script3.js"></script>
  </head>
  <body>
  ...
  </body>
</html>
```
* Si especificamos `async`, _no_ tenemos garantizado su orden de ejecución. Es decir, a medida que se descarguen, el navegador los ejecutará... ¡Puede que el HTML no haya terminado de _renderizarse_!

```html
<html>
  <head>
    <script async src="script1.js"></script>
    <script async src="script2.js"></script>
    <script async src="script3.js"></script>
    
    <!-- Con async no sabemos qué script se ejecutará primero y cuáles después -->
    <!-- Ni siquiera sabemos si el HTML se habrá cargado por completo -->
    
  </head>
  <body>
  ...
  </body>
</html>
```


## :package: La tarea

**Crea** dos **nuevos archivos** dentro de `www/`, cada uno con una línea de código como se indica a continuación:

* `asyncRANDOMEXPRESSIONExample1|Tiny1|Small1END.js`

```
console.log("Se está ejecutando asyncRANDOMEXPRESSIONExample1|Tiny1|Small1END")
```

* `asyncRANDOMEXPRESSIONExample2|Tiny2|Small2END.js`

```
console.log("Se está ejecutando asyncRANDOMEXPRESSIONExample2|Tiny2|Small2END")
```

Luego, **añade** estas dos _nuevas_ etiquetas `<script>` a `index.html` (¡no borres lo de la tarea anterior!):

```diff
<html>
  <head>
    <meta charset="utf8"/>
    <script defer src="RANDOMEXPRESSIONexample|tiny|smallEND.js"></script>
+   <script async src="asyncRANDOMEXPRESSIONExample1|Tiny1|Small1END.js"></script>
+   <script async src="asyncRANDOMEXPRESSIONExample2|Tiny2|Small2END.js"></script>
  </head>
  <body>
  ...
```

¡Prueba la página!

Si desde el navegador haces _click derecho > Inspeccionar_ y vas a _Consola_, verás una salida de texto con los mensajes de los `console.log` anteriores.

Si refrescas varias veces la página, es posible que te encuentres con que el orden de estos mensajes a veces es distinto. ¡`async` no garantiza el orden de ejecución!

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

