Scripts externos, defer

## :books: Resumen

* Veremos cómo añadir código JavaScript a una página web usando un archivo separado
* Añadiremos un nuevo fichero `RANDOMEXPRESSIONexample|tiny|smallEND.js` y lo cargaremos con un `<script>` de manera externa desde `index.html`
* Aprenderemos por qué suele ser buena idea indicar `defer`

## Descripción

JavaScript es una herramienta muy poderosa. Es habitual que existan centenares de líneas de JavaScript en una página _web_. 

¿Van todas _embebidas_ dentro de un `<script></script>` del HTML?

### ¡No!

En el mundo _web_ hay **3** formatos esenciales:

* `HTML`: El alma de la página. Contiene hipertexto.
* `CSS`: _Hojas de estilo_ que sirven para modificar la apariencia de elementos del HTML.
* `JavaScript`: Código ejecutable por el navegador _web_.

Cada uno de ellos _suele viajar_ en archivos separados, con formato `.html`, `.css` y `.js` respectivamente. Desde el HTML, se _enlaza_ al `CSS` y al `JavaScript`.

Para enlazar un fichero JavaScript externo a un HTML, se especifica el atributo `src` dentro de `<script>`.

Además, como veremos a continuación, los `<script>` suelen incluirse dentro de la etiqueta [`<head>`](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/The_head_metadata_in_HTML) de HTML. Contiene metadatos que no se muestran dentro de la página directamente, como el _título_, _codificación de caracteres_, _enlaces a hojas de estilo_... ¡Y _JavaScript_!

## :package: La tarea

**Crea** un **nuevo archivo** dentro de `www/`. Llámalo `RANDOMEXPRESSIONexample|tiny|smallEND.js`.

**Dentro** de dicho archivo, **escribe** esta línea:

```javascript
document.getElementById("p2").innerHTML = "RANDOMEXPRESSIONModificado con un script externo|Alterado con un script externo|Manipulado con un script externo|Cambiado con un script externo|Modificado mediante un script externo|Alterado mediante un script externo|Manipulado mediante un script externo|Cambiado mediante un script externoEND";
```

### `<head>`

**Añade** una etiqueta `<script>` a `index.html`, que _enlace_ al fichero recién creado:

```diff
<html>
  <head>
    <meta charset="UTF8"/>
+   <script src="RANDOMEXPRESSIONexample|tiny|smallEND.js"></script>
  </head>
  <body>
  ...
```

¡Prueba la página!

### Hmm... No ha cambiado el `p2`

¿No aparece el texto `RANDOMEXPRESSIONModificado con un script externo|Alterado con un script externo|Manipulado con un script externo|Cambiado con un script externo|Modificado mediante un script externo|Alterado mediante un script externo|Manipulado mediante un script externo|Cambiado mediante un script externoEND` en el segundo párrafo?

### No...

Debe ser porque los `<script>` se ejecutan a medida que el navegador _lee_ el HTML. Es decir, de forma _síncrona_.

1. El navegador _comienza a leer_ el HTML, para saber _qué_ debe mostrar.
2. Se encuentra con una etiqueta `<head>` que contiene un `<script>`. Descarga `RANDOMEXPRESSIONexample|tiny|smallEND.js` y lo ejecuta.
3. Como _no hay_ ningún elemento `p2`... ¡No tiene efecto!
4. Sigue _leyendo_ el HTML. Pinta todos los elementos (incluido el `p2`).

Puedes pensar que, si es así, deberíamos poner los `<script>` siempre al final, antes del cierre `</body>`. (¡Como en el ejercicio anterior, que funcionaba!) Pero esa es una solución _mala_. Para páginas muy grandes, primero se cargará todo el HTML y luego todo el JavaScript. ¿Por qué descargarlos _secuencialmente_ si se puede hacer de forma _paralela_?

### `defer`

**Especifica** `defer` en tu `<script>` de `index.html`.

```diff
<html>
  <head>
    <meta charset="utf8"/>
-   <script src="RANDOMEXPRESSIONexample|tiny|smallEND.js"></script>
+   <script defer src="RANDOMEXPRESSIONexample|tiny|smallEND.js"></script>
  </head>
  <body>
  ...
```

Así, tu navegador descargará `"RANDOMEXPRESSIONexample|tiny|smallEND.js"` de forma _paralela_, mientras el resto del HTML se _renderiza_. Y _después_ de pintar _todo_ el HTML, lo _ejecutará_.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

