/// <reference types="cypress" />

describe('getting_started', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8000/');
      cy.reload(true); // Force-reload to prevent cache errors
    })
  
    it('issueISSUENUMBER header is changed', () => {
        cy.get('h1[data-cy=pageHeader]')
        .should('have.text', 'RANDOMEXPRESSIONHola mundo|Hello there!|Buenos días|Aquí estamos|Mis primeros pasos en la web|Un pequeño paso para mí, un gran salto para el mundo|¡Hola!|Poco a poco...|On my way to becoming a Web masterEND');
    })
})
