Un primer servidor

## :books: Resumen

* Instalaremos un sencillo servidor web y serviremos la carpeta `www` de nuestro repositorio
* Realizaremos una modificación en `index.html` y comprobaremos el resultado
* Haremos `git add`, `git commit` y `git push` para subir los cambios al repositorio

## Descripción

[Internet](https://es.wikipedia.org/wiki/Internet) es un conjunto de redes de comunicaciones que usan la familia de protocolos [TCP/IP](https://es.wikipedia.org/wiki/Familia_de_protocolos_de_Internet), lo cual garantiza que las diferentes redes físicas que la componen constituyen una _gran red mundial_.

Internet es la _red_ de _redes_.

Hablaremos más durante el curso, pero, por el momento, ¡manos a la obra! ¡levantemos nuestro primer _servidor web_!

### Servidores web

Los servidores _web_ más populares son [Apache HTTP Server](https://httpd.apache.org/ABOUT_APACHE.html), que sirve el ~23,04% de las páginas de Internet, y [Nginx](https://www.nginx.com/), el ~22,01%.

Nosotros no vamos a centrarnos (en esta asignatura) en el proceso de despliegue de aplicaciones web.

Vamos a conformarnos con un _pequeño servidor_ de juguete: [Simple Web Server](https://simplewebserver.org).

<img src="javascript/docs/simple_web_server.png" width=400 />

## :package: La tarea

Desde [la página de descargas](https://simplewebserver.org/download.html), **descarga** el instalador de Windows (versión 64 bits) o la versión portable si la prefieres. Luego **ejecuta** el instalador `.exe`.

Si nos aparece un aviso de Microsoft, indicamos **Ejecutar de todas formas** y procedemos a aceptar el acuerdo de licencia para terminar la instalación.

Cuando termine la instalación, se ejecutará el programa servidor. Pero...

### ¿...qué voy a _servir_?

Vamos a servir una carpeta `www/` que se encuentra en tu repositorio de trabajo. Para ello, ¡debemos tener clonado el repositorio en nuestra máquina local!

Si ya hiciste esto, puedes saltarte el siguiente paso. En caso contrario, debes configurar Git e inicializar tu repositorio.

### Configuración de Git e inicialización del repositorio

**Instala** [Git](https://git-scm.com/download/win).

Después, **genera** una clave SSH[^1] en tu máquina local. Para ello, abre un terminal (tecla de Windows > cmd.exe) y escribe:

```
ssh-keygen -t rsa
```

**Pulsa** ENTER varias veces para completar el proceso. Luego, **abre** la carpeta `.ssh` que se habrá creado en tu directorio de usuario[^2]. Allí verás dos archivos `id_rsa` e `id_rsa.pub`. **Abre** (con el Bloc de Notas) el archivo `id_rsa.pub` y **copia** (CTRL+C) su contenido.

Después, **navega** a https://raspi y haz _login_ con tu cuenta de usuario. Arriba a la derecha, abre el _popup_ de ajustes y **navega** a `Settings`.

A la izquierda verás un panel lateral. Busca `SSH and GPG keys` y **haz click** ahí.

En esa pantalla, verás un recuadro de texto grande. **Pega** (CTRL+V) tu clave pública y **pulsa** en el botón verde `Add key` más abajo.

¡Clave configurada!

Ahora, desde tu terminal (cmd.exe) **clona** (descarga) el repositorio. Para ello, **escribe**:

```
git clone <url_de_tu_repositorio>
```

Donde `<url_de_tu_repositorio>` será la URL que puedes encontrar en https://raspi, yendo a tu proyecto y desde el botón azul de _Clone_:

<img src="javascript/docs/ssh_url_gitlab.png" width=400 />

¡Acabas de obtener la carpeta `dwec/` (tu repositorio)!

Para terminar la configuración, desde el terminal, **ejecuta**:

```
git config --global user.name "Nombre Apellido"
git config --global user.email "nombre.apellido@fpcoruna.afundacion.org"
```

¡**Sustituye** correctamente `"Nombre Apellido"` y `"nombre.apellido@fpcoruna.afundacion.org"` por tu nombre real y tu correo institucional! (Debes mantener las comillas `" "`).

### Lanzando el servidor

Ahora que tienes la carpeta del repositorio `dwec/` en tu ordenador, vamos a _servir_ `dwec/javascript/www/` a través de HTTP.

Con Simple Web Server abierto,  **haz** *click* en *Get Started*. Verás una lista de servidores:

<img src="javascript/docs/simple_web_server_list.png" width=400 />

**Selecciona** `New Server`, y, en la configuración:

* **Elige** como _**Folder path**_ la carpeta `www/` del repositorio que hemos clonado en el paso anterior (dentro de `javascript/`)
* **Cambia** _Port_ para que sea **8000** en lugar de **8080**

Una vez lo hayas hecho, puedes darle a **Create Server**.

Luego, **visita** tu página web accediendo a http://localhost:8000 desde tu navegador _web_. Nótese que `localhost` es un _alias_ para referirse a tu propio ordenador. ¿Ves la página! ¡Enhorabuena!

### Un pequeño cambio en la página

**Modifica** el archivo `www/index.html` y **sustituye** el contenido de la etiqueta `h1` por `RANDOMEXPRESSIONHola mundo|Hello there!|Buenos días|Aquí estamos|Mis primeros pasos en la web|Un pequeño paso para mí, un gran salto para el mundo|¡Hola!|Poco a poco...|On my way to becoming a Web masterEND`. Puedes hacerlo con tu editor favorito ([VSCode](https://code.visualstudio.com/), [Geany](https://www.geany.org/),...)

```diff
- <h1 data-cy="pageHeader">¡Enhorabuena!</h1>
+ <h1 data-cy="pageHeader">RANDOMEXPRESSIONHola mundo|Hello there!|Buenos días|Aquí estamos|Mis primeros pasos en la web|Un pequeño paso para mí, un gran salto para el mundo|¡Hola!|Poco a poco...|On my way to becoming a Web masterEND</h1>
```

Una vez lo hayas hecho, **verifica** desde el navegador _web_ que el cambio se refleja en http://localhost:8000/index.html

**¡Enhorabuena!** Primera tarea completada.

### :trophy: Por último

Desde el terminal de Windows (cmd.exe), posiciónate en `javascript` y escribe:

```
git add *
```

...para marcar los cambios del un nuevo _commit_. Después:

```
git commit -m "Tarea #1: Cambio de prueba en index.html"
```

...y por último subimos los cambios al repositorio remoto:

```
git push
```

No está de más visitar la página de GitLab (https://raspi) y verificar que el _commit_ se ha subido.

------------------------------------------------------------------------

[^1]: En realidad son un par de claves: La pública y la privada.

[^2]: Tu directorio de usuario está dentro de la ruta `C:\Users`. Por ejemplo, si tu usuario se llama `PepeDepura`, será `C:\Users\PepeDepura`.
