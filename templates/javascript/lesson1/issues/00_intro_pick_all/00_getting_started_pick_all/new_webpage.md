Otra página web

## :books: Resumen

* Añadiremos un nuevo archivo `issueISSUENUMBER.html` a la carpeta `www/`
* Lo comprobaremos desde nuestro navegador _web_
* Añadiremos un enlace `<a href>` en `index.html`
* _(Y como siempre, subiremos nuestros cambios al repositorio)_

## Descripción

Es habitual que un servidor web ofrezca varias páginas, e incluso existan _enlaces_ de unas a otras.

Uno de los elementos principales en HTML son los [hiperenlaces](https://es.wikipedia.org/wiki/Hiperenlace) (`<a>`).

* Entre la etiqueta de _apertura_ y la de _cierre_, se escribe el texto visible por el usuario: `<a>Enlace</a>`
* Con el _atributo_ `href`, se indica la dirección a la que lleva. 
  * Puede ser _absoluta_: `<a href="www.google.es">Enlace</a>`
  * O _relativa_, dentro del servidor: `<a href="/issueISSUENUMBER.html">Enlace</a>`[^2]

## :package: La tarea

**Crea** un **nuevo** archivo llamado `issueISSUENUMBER.html`[^1] dentro de la carpeta `www/`.

**Escribe** el siguiente contenido HTML:

```html
<html>
  <body>
    <h1 data-cy="issueISSUENUMBERheader">RANDOMEXPRESSIONAlfa Romeo|Aston Martin|Audi|Bentley|BMW|Dacia|Ferrari|Fiat|Ford|Hyundai|Jeep|Lexus|Maserati|Nissan|Peugueot|Renault|Seat|Subaru|VolkswagenEND</h1>
  </body>
</html>
```

**Verifica** cómo se ve, lanzando tu servidor HTTP y visitando http://localhost:8000/issueISSUENUMBER.html desde el navegador.

¡**No** (:warning:) hagas _doble click_ en el fichero `issueISSUENUMBER.html` para abrirlo directamente! Cada vez que alguien hace esto, Dios mata un gatito. La manera correcta es tener Simple Web Server abierto y navegar a http://localhost:8000/issueISSUENUMBER.html. Si no lo haces así, no estarás verificando la navegación real y en las tareas posteriores no funcionarán los enlaces (`<a>`) correctamente. 

### Añadiendo un _enlace_ en `index.html`

**Edita** el fichero `index.html` de la tarea anterior.

Ahí, **añade** un enlace a `issueISSUENUMBER.html`.

* El texto visible será `RANDOMEXPRESSIONMarca de coche|Marca de vehículo|Marca de coches|Marca de vehículos|FabricanteEND`
* Tendrá el atributo `data-cy="issueISSUENUMBERlink"`. _(¡Importante! Es usado por el test automático que lanzarás más adelante)_
* Debe ir dentro de `<body>`, donde tú quieras.

```
<a href="/issueISSUENUMBER.html" data-cy="issueISSUENUMBERlink">RANDOMEXPRESSIONMarca de coche|Marca de vehículo|Marca de coches|Marca de vehículos|FabricanteEND</a>
```

### :trophy: Una vez termines...

Haz `git add *` y `git commit -m "Tarea #2, enlace a issueISSUENUMBER.html"` para crear un _commit_. Sube tus cambios al repositorio con `git push`.

------------------------------------------------

[^1]: Es importante que la **extensión** (`.html`) del archivo sea la correcta. Si lo creas con el Bloc de Notas, puede que se mantenga la extensión `.txt` y esté oculta por la configuración de Windows. ¡Verifica que esto no pasa!

[^2]: En caso de ser una dirección relativa, si indicas la barra (`/`) inicial, estarás explicando que es una ruta desde la _raíz_ del servidor. Si no pones la barra (`/`) inicial, tu enlace se calculará de manera relativa con respecto a la ubicación del fichero actual.
