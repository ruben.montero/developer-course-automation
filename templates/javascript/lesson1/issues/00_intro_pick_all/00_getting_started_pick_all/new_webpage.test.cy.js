/// <reference types="cypress" />

describe('new_webpage', () => {
    it('issueISSUENUMBER HTML is served', () => {
        cy.visit('http://localhost:8000/issueISSUENUMBER.html');
        cy.reload(true); // Force-reload to prevent cache errors

        cy.get('h1[data-cy=issueISSUENUMBERheader]')
        .should('have.text', 'RANDOMEXPRESSIONAlfa Romeo|Aston Martin|Audi|Bentley|BMW|Dacia|Ferrari|Fiat|Ford|Hyundai|Jeep|Lexus|Maserati|Nissan|Peugueot|Renault|Seat|Subaru|VolkswagenEND');
    })
    
    it('issueISSUENUMBER link has been created in index.html', () => {
        cy.visit('http://localhost:8000/');
        cy.reload(true); // Force-reload to prevent cache errors

        cy.get('body')
        .find('a[data-cy=issueISSUENUMBERlink]')
        .should('have.text', 'RANDOMEXPRESSIONMarca de coche|Marca de vehículo|Marca de coches|Marca de vehículos|FabricanteEND')
        .should('have.attr', 'href', '/issueISSUENUMBER.html');
    })
})
