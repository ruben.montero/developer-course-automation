/// <reference types="cypress" />

describe('fullscreen', () => {
    // https://docs.cypress.io/faq/questions/using-cypress-faq#How-do-I-spy-on-console-log
    beforeEach(() => {
        cy.visit('http://localhost:8000/projects/kanban.html');
        cy.reload(true); // Force-reload to prevent cache errors
        cy.visit('http://localhost:8000/projects/kanban.html', {
            onBeforeLoad(win) {
            // Stub your functions here
            cy.stub(win.console, 'log').as('consoleLog')
          },
        })
    })

    it('issueISSUENUMBER button is there', () => {
        cy.get('div[data-cy=RANDOMEXPRESSIONtopBar|pageBar|topBarContainer|toolsBar|pageToolsBar|pageToolsBarContainer|toolsBarContainerEND]')
        .find('div[data-cy=RANDOMEXPRESSIONbuttonContainer|topRightButtonContainer|fullscreenButtonContainer|buttonDiv|topRightButtonDiv|fullscreenButtonDivEND]')
        .should('have.css', 'float', 'right');

        cy.get('div[data-cy=RANDOMEXPRESSIONtopBar|pageBar|topBarContainer|toolsBar|pageToolsBar|pageToolsBarContainer|toolsBarContainerEND]')
        .find('div[data-cy=RANDOMEXPRESSIONbuttonContainer|topRightButtonContainer|fullscreenButtonContainer|buttonDiv|topRightButtonDiv|fullscreenButtonDivEND]')
        .find('button[data-cy=RANDOMEXPRESSIONfullscreenButton|topRightButtonEND]')
        .should('have.text', 'Pantalla completa')
    })

    it('issueISSUENUMBER button triggers function', () => {
        cy.once('uncaught:exception', () => false); // Ignore fullscreen requested -> false, which is expected during automated test

        cy.get('div[data-cy=RANDOMEXPRESSIONtopBar|pageBar|topBarContainer|toolsBar|pageToolsBar|pageToolsBarContainer|toolsBarContainerEND]')
        .find('div[data-cy=RANDOMEXPRESSIONbuttonContainer|topRightButtonContainer|fullscreenButtonContainer|buttonDiv|topRightButtonDiv|fullscreenButtonDivEND]')
        .find('button[data-cy=RANDOMEXPRESSIONfullscreenButton|topRightButtonEND]')
        .click();

        cy.get('@consoleLog').should('be.calledWith', "RANDOMEXPRESSIONFullscreen has been requested|Fullscreen requested|Fullscreen was requested|User has requested fullscreen|User has requested fullscreen modeEND");
    })
})
