/// <reference types="cypress" />

describe('initial kanban board', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8000/');
      cy.reload(true);
    })
  
    it('issueISSUENUMBER index.html is correctly modified', () => {
        cy.get('RANDOMEXPRESSIONul|olEND[data-cy=RANDOMEXPRESSIONprojectsList|listOfProyects|myProjectsEND]')
        .find('li')
        .eq(1)
        .find('a')
        .should('have.text', 'RANDOMEXPRESSIONTablero kanban|Tablero|Tablero agile|Tablero de tareasEND');
    })

    it('issueISSUENUMBER kanban.html has initial 5 column titles correct', () => {
        cy.get('RANDOMEXPRESSIONul|olEND[data-cy=RANDOMEXPRESSIONprojectsList|listOfProyects|myProjectsEND]')
        .find('li')
        .eq(1)
        .find('a')
        .click();

        cy.get('div.kanbanColumn')
        .first()
        .should('have.class', 'column1');
        cy.get('div.kanbanColumn')
        .eq(1)
        .should('have.class', 'column2');
        cy.get('div.kanbanColumn')
        .eq(2)
        .should('have.class', 'column3');
        cy.get('div.kanbanColumn')
        .eq(3)
        .should('have.class', 'column4');
        cy.get('div.kanbanColumn')
        .eq(4)
        .should('have.class', 'column5');
        
        cy.get('div.kanbanColumn')
        .first()
        .find('RANDOMEXPRESSIONh3|h2END')
        .should('have.text', 'RANDOMEXPRESSIONSin empezar|Por hacer|Sin comenzar|PendienteEND');
        cy.get('div.kanbanColumn')
        .eq(1)
        .find('RANDOMEXPRESSIONh3|h2END')
        .should('have.text', 'Diseño');
        cy.get('div.kanbanColumn')
        .eq(2)
        .find('RANDOMEXPRESSIONh3|h2END')
        .should('have.text', 'Implementación');
        cy.get('div.kanbanColumn')
        .eq(3)
        .find('RANDOMEXPRESSIONh3|h2END')
        .should('have.text', 'RANDOMEXPRESSIONPruebas|TestsEND');
        cy.get('div.kanbanColumn')
        .eq(4)
        .find('RANDOMEXPRESSIONh3|h2END')
        .should('have.text', 'RANDOMEXPRESSIONTerminado|Finalizado|AcabadoEND');
  })

  it('issueISSUENUMBER kanban.html has initial 5 column styles correct', () => {
    cy.request('http://localhost:8000/projects/css/kanban.css');
    
    cy.get('RANDOMEXPRESSIONul|olEND[data-cy=RANDOMEXPRESSIONprojectsList|listOfProyects|myProjectsEND]')
    .find('li')
    .eq(1)
    .find('a')
    .click();

    cy.get('div.kanbanColumn')
    .first()
    .should('have.css', 'background-color', 'rgb(255, 185, 185)')
    .should('have.css', 'float', 'left');
    cy.get('div.kanbanColumn')
    .eq(1)
    .should('have.css', 'background-color', 'rgb(255, 185, RANDOMNUMBER240-248END)')
    .should('have.css', 'float', 'left');
    cy.get('div.kanbanColumn')
    .eq(2)
    .should('have.css', 'background-color', 'rgb(255, RANDOMNUMBER220-231END, 181)')
    .should('have.css', 'float', 'left');
    cy.get('div.kanbanColumn')
    .eq(3)
    .should('have.css', 'background-color', 'rgb(244, 255, 181)')
    .should('have.css', 'float', 'left');
    cy.get('div.kanbanColumn')
    .eq(4)
    .should('have.css', 'background-color', 'rgb(RANDOMNUMBER223-240END, 255, 181)')
    .should('have.css', 'float', 'left');
  })
})
