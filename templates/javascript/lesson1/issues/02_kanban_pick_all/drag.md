Drag

## :books: Resumen

* Hablaremos del API de _Drag and Drop_
* Crearemos un `<div>` que, formateado con CSS, tendrá apariencia de _post-it_. Representará una tarea
* Estableceremos en el HTML el atributo `draggable` para permitir al usuario arrastrarlo

## Descripción

El [API de _Drag and Drop_](https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API) es una poderosa herramienta que permite a los usuarios arrastrar (_drag_) elementos con el ratón, y soltarlos (_drop_) en otra ubicación de la página.

De momento trabajaremos con la propiedad `draggable`. Se establece en aquellos elementos HTML que el usuario podrá _arrastrar_.

De momento, trabajaremos con esta y otras propiedades en el HTML. Más adelante veremos que se pueden manipular desde JavaScript:

## :package: La tarea

**Añade** un `<div>` a `kanban.html` que representará una tarea inicial.

Estará en la primera columna y tendrá el texto de ejemplo `"RANDOMEXPRESSIONDetección de colisiones entre dos objetos 2D en JS|Detección de colisiones entre objetos 2D en JS|Detección de colisiones de dos objetos 2D en JS|Detección de colisiones de objetos 2D en JSEND"`:

```html
      <div class="kanbanColumn column1">
        <p><RANDOMEXPRESSIONh3|h2END>RANDOMEXPRESSIONSin empezar|Por hacer|Sin comenzar|PendienteEND</RANDOMEXPRESSIONh3|h2END></p>

        <!-- Una nota inicial -->
        <div id="task0"
          class="tinyPaper yellow">
          RANDOMEXPRESSIONDetección de colisiones entre dos objetos 2D en JS|Detección de colisiones entre objetos 2D en JS|Detección de colisiones de dos objetos 2D en JS|Detección de colisiones de objetos 2D en JSEND
        </div>

      </div>
```

### CSS

**Añade** las _clases_ `tinyPaper` y `yellow` a `kanban.css`:

```css
.tinyPaper {
    margin: 2%;
    padding-left: 3%;
    padding-right: 3%;
    padding-top: 5%;
    padding-bottom: 10%;
    border: 1.5px solid rgb(79, 77, 77);
}

.yellow {
    background-color: rgb(250, 250, RANDOMNUMBER1-12END);
}
```

¡Listo!

¿Ya ves tu nuevo `<div>` aparecer en la página al recargar? En caso contrario, es posible que los archivos JavaScript o CSS se guarden en la _caché_ del navegador por un tiempo. _Fuerza_ que se _refresque_ la página con SHIFT+F5 o borra los datos de navegación cuando creas que no está reflejando lo último que has escrito en tu código.

¡Asegúrate también de haber guardado los ficheros que estás editando (`.js`, `.css`)!

### `draggable=true`

**Añade** `draggable=true` al `<div>` añadido anteriormente:

```diff
        <!-- Un post-it inicial -->
        <div id="task0"
-         class="tinyPaper yellow">
+         class="tinyPaper yellow"
+         draggable="true">
          Detección de colisiones entre dos objetos 2D en JS
        </div>
```

¡Verifica en http://localhost:8000/projects/kanban.html que puedes _arrastrar_ el `<div>`!

De momento no parece que lo puedas _soltar_ en ningún sitio, ¿verdad?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
