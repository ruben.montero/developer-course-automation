/// <reference types="cypress" />

describe('new task form', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8000/projects/kanban.html');
      cy.reload(true);
    })
  
    it('issueISSUENUMBER new task form is created and contains correct amount of children', () => {
        cy.get('div[data-cy=RANDOMEXPRESSIONtopBar|pageBar|topBarContainer|toolsBar|pageToolsBar|pageToolsBarContainer|toolsBarContainerEND]')
        .find('div[data-cy=RANDOMEXPRESSIONbuttonContainer|topRightButtonContainer|fullscreenButtonContainer|buttonDiv|topRightButtonDiv|fullscreenButtonDivEND]')
        .next()
        .should('have.attr', 'data-cy', 'RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND')

        cy.get('div[data-cy=RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND]')
        .find('form[data-cy=RANDOMEXPRESSIONnewTaskForm|formNewTask|formAddTask|addTaskFormEND]')
        .find('label')
        .should('have.length', 1);

        cy.get('div[data-cy=RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND]')
        .find('form[data-cy=RANDOMEXPRESSIONnewTaskForm|formNewTask|formAddTask|addTaskFormEND]')
        .find('input')
        .should('have.length', 2);

        cy.get('div[data-cy=RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND]')
        .find('form[data-cy=RANDOMEXPRESSIONnewTaskForm|formNewTask|formAddTask|addTaskFormEND]')
        .find('select')
        .should('have.length', 1);
    })

    it('issueISSUENUMBER new task form label is correct', () => {
        cy.get('div[data-cy=RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND]')
        .find('form[data-cy=RANDOMEXPRESSIONnewTaskForm|formNewTask|formAddTask|addTaskFormEND]')
        .find('label')
        .find('b')
        .contains('RANDOMEXPRESSIONNueva tarea:|Crear tarea:|Añadir tarea:END');
    })

    it('issueISSUENUMBER new task form inputs are correct', () => {
        cy.get('div[data-cy=RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND]')
        .find('form[data-cy=RANDOMEXPRESSIONnewTaskForm|formNewTask|formAddTask|addTaskFormEND]')
        .find('input[data-cy=RANDOMEXPRESSIONinputTaskName|inputNameTask|inputAddTaskNameEND]')
        .should('have.attr', 'placeholder', 'RANDOMEXPRESSIONFuncionalidad de login|Funcionalidad de logout|Funcionalidad de registro|Funcionalidad de fullscreenEND')
        .should('have.attr', 'type', 'text')
        .should('have.attr', 'name', 'inputTaskName');

        cy.get('div[data-cy=RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND]')
        .find('form[data-cy=RANDOMEXPRESSIONnewTaskForm|formNewTask|formAddTask|addTaskFormEND]')
        .find('select[data-cy=RANDOMEXPRESSIONinputTaskType|inputTypeTask|inputAddTaskTypeEND]')
        .should('have.attr', 'name', 'inputTaskType');

        cy.get('div[data-cy=RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND]')
        .find('form[data-cy=RANDOMEXPRESSIONnewTaskForm|formNewTask|formAddTask|addTaskFormEND]')
        .find('select[data-cy=RANDOMEXPRESSIONinputTaskType|inputTypeTask|inputAddTaskTypeEND]')
        .find('option')
        .should('have.length', 4);

        cy.get('div[data-cy=RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND]')
        .find('form[data-cy=RANDOMEXPRESSIONnewTaskForm|formNewTask|formAddTask|addTaskFormEND]')
        .find('select[data-cy=RANDOMEXPRESSIONinputTaskType|inputTypeTask|inputAddTaskTypeEND]')
        .find('option')
        .eq(0)
        .should('have.attr', 'value', 'other')
        .contains('Otro');

        cy.get('div[data-cy=RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND]')
        .find('form[data-cy=RANDOMEXPRESSIONnewTaskForm|formNewTask|formAddTask|addTaskFormEND]')
        .find('select[data-cy=RANDOMEXPRESSIONinputTaskType|inputTypeTask|inputAddTaskTypeEND]')
        .find('option')
        .eq(1)
        .should('have.attr', 'value', 'backend')
        .contains('Backend');

        cy.get('div[data-cy=RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND]')
        .find('form[data-cy=RANDOMEXPRESSIONnewTaskForm|formNewTask|formAddTask|addTaskFormEND]')
        .find('select[data-cy=RANDOMEXPRESSIONinputTaskType|inputTypeTask|inputAddTaskTypeEND]')
        .find('option')
        .eq(2)
        .should('have.attr', 'value', 'frontend')
        .contains('Frontend');

        cy.get('div[data-cy=RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND]')
        .find('form[data-cy=RANDOMEXPRESSIONnewTaskForm|formNewTask|formAddTask|addTaskFormEND]')
        .find('select[data-cy=RANDOMEXPRESSIONinputTaskType|inputTypeTask|inputAddTaskTypeEND]')
        .find('option')
        .eq(3)
        .should('have.attr', 'value', 'urgent')
        .contains('RANDOMEXPRESSIONUrgente|ImportanteEND');
        
        cy.get('div[data-cy=RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND]')
        .find('form[data-cy=RANDOMEXPRESSIONnewTaskForm|formNewTask|formAddTask|addTaskFormEND]')
        .find('input[data-cy=RANDOMEXPRESSIONinputCreateTaskSubmit|inputAddTaskSubmit|inputNewTaskSubmit|inputSubmitEND]')
        .should('have.attr', 'type', 'submit')
        .contains('RANDOMEXPRESSIONAñadir tarea|Crear tareaEND');
    })

    it('issueISSUENUMBER top bar css was added', () => {
        cy.get('div[data-cy=RANDOMEXPRESSIONtopBar|pageBar|topBarContainer|toolsBar|pageToolsBar|pageToolsBarContainer|toolsBarContainerEND]')
        .should('have.css', 'background-color', 'rgb(RANDOMNUMBER222-231END, 255, RANDOMNUMBER241-246END)')
    })
})
