Añadiendo tareas

## :books: Resumen

* Implementaremos el código necesario para añadir un nuevo _post-it_ (`<div>`) con los datos introducidos en el formulario
* Añadiremos algo de CSS
* Nos daremos cuenta de... ¡un _bug_ en nuestro código!

## Descripción

Vamos a hacer _funcional_ el formulario HTML que hemos creado en la tarea anterior.

## :package: La tarea

**Añade** en `kanban.js` una nueva función que se ejecutará cuando el usuario introduzca datos en el formulario. Con `addEventListener`, **enlázala** al evento `"submit"`:

```js
function onSubmitNewTask(event) {
    // Con esta línea se previene el comportamiento por defecto del <form>
    // que consiste en mandar una petición HTTP (recarga la página)
    event.preventDefault();

}

const newTaskForm = document.getElementById("newTaskForm");
newTaskForm.addEventListener("submit", onSubmitNewTask);
```

### Validando la entrada

¡No queremos un puñado de _post-it_ vacíos!

**Obliga** al usuario a introducir al menos `RANDOMNUMBER5-9END` caracteres usando una _condición (`if`) de guarda_:

```js
function onSubmitNewTask(event) {
    event.preventDefault();
    const taskTitle = event.target.elements.inputTaskName.value;
    if (taskTitle.length < RANDOMNUMBER5-9END) {
        alert("El título de RANDOMEXPRESSIONla|tuEND tarea debe tener por lo menos RANDOMNUMBER5-9END caracteres");
        return;
    }
    
}
```

### Tipo de tarea

A continuación, vamos a elegir un _color de fondo_ para cada _tipo_ de tarea. Tal y como especificamos en `<select>` anteriormente, las _tareas (post-it)_ añadidas por el usuario pueden ser:

* Otro
* Frontend
* Backend
* RANDOMEXPRESSIONUrgente|ImportanteEND

**Añade** los siguientes estilos a `kanban.css`:

```css
.blue {
    background-color: rgb(RANDOMNUMBER58-65END, RANDOMNUMBER151-155END, 255);
}

.green {
    background-color: rgb(0, RANDOMNUMBER190-199END, 109);
}

.red {
    background-color: rgb(RANDOMNUMBER234-239END, 0, 0);
}
```

Usaremos la clase CSS `yellow` (que ya existe) para 'Otro', y, en función del tipo de tarea, **asígnalas** así:

```js
function onSubmitNewTask(event) {
    event.preventDefault();
    const taskTitle = event.target.elements.inputTaskName.value;
    if (taskTitle.length < RANDOMNUMBER5-9END) {
        alert("El título de RANDOMEXPRESSIONla|tuEND tarea debe tener por lo menos RANDOMNUMBER5-9END caracteres");
        return;
    }
    const taskType = event.target.elements.inputTaskType.value;
    let backgroundColorClassName;
    if (taskType === "backend") {
        backgroundColorClassName = "green";
    } else if (taskType === "frontend") {
        backgroundColorClassName = "blue";
    } else if (taskType === "urgent") {
        backgroundColorClassName = "red";
    } else {
        backgroundColorClassName = "yellow";
    }
    
}
```

Luego, **completa** `onSubmitNewTask`:

```js
function onSubmitNewTask(event) {

    // ...

    addNewPostIt("taskX", taskTitle, backgroundColorClassName);
    event.target.reset(); // Con esto, se borran los campos del formulario
}
```

### Crear el _post-it_ (`<div>`)

Ahora, **añade** la nueva función `addNewPostIt`:

```js
function addNewPostIt(id, title, className) {
    const aNewPostIt = document.createElement("div");
    aNewPostIt.innerHTML = title;
    aNewPostIt.setAttribute("id", id);
    aNewPostIt.setAttribute("class", "tinyPaper " + className); // Tiene 2 clases CSS; tinyPaper y la que sea que usemos de color de fondo
    aNewPostIt.setAttribute("draggable", "true");
    aNewPostIt.addEventListener("dragstart", drag);
}
```

Observa que lo mismo que hacíamos con `draggable="true"` y `ondragstart="drag(event)"` desde HTML para el _post-it_ inicial, ahora lo hemos indicado mediante JavaScript.

### ¡Y añadirlo a la _columna inicial_!

...la cual carece de `id` de momento. Así que, **dáselo** en `kanban.html`:

```diff
-   <div class="kanbanColumn column1" ondrop="drop(event)" ondragover="allowDrop(event)">
+   <div id="RANDOMEXPRESSIONtoDoColumn|pendingTasksColumn|unstartedColumn|notStartedColumn|toStartColumnEND" class="kanbanColumn column1" ondrop="drop(event)" ondragover="allowDrop(event)">
```

Y luego, **completa** la función `addNewPostIt`:

```diff
function addNewPostIt(id, title, className) {
    const aNewPostIt = document.createElement("div");
    aNewPostIt.innerHTML = title;
    aNewPostIt.setAttribute("id", id);
    aNewPostIt.setAttribute("class", "tinyPaper " + className); // Tiene 2 clases CSS; tinyPaper y la que sea que usemos de color de fondo
    aNewPostIt.setAttribute("draggable", "true");
    aNewPostIt.addEventListener("dragstart", drag);
+   const pendingTasksColumn = document.getElementById("RANDOMEXPRESSIONtoDoColumn|pendingTasksColumn|unstartedColumn|notStartedColumn|toStartColumnEND");
+   pendingTasksColumn.append(aNewPostIt);
}
```

### ¡Parece que funciona!

Efectivamente. Puedes rellenar datos en el formulario y las _notas_ se añaden a la columna inicial, ¿verdad?

Aunque... Nuestro código aún no es perfecto.

De hecho, tiene un _**bug**_ muy relevante.

¿Eres capaz de descubrir cuál es? (No se trata de que puedas anidar unos _post-it_ a otros; eso está bien).

### Investiga, crea tareas, muévelas por las columnas...

...a ver si das con el problema.

La siguiente tarea del _Sprint_ está enfocada a _solventar el problema_.

### :trophy: Por último

Si no encuentras el problema, no te preocupes. En la próxima tarea lo investigaremos.

Sube tus cambios al repositorio en un nuevo _commit_.
