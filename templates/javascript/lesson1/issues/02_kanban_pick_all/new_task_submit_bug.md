bug: Añadiendo tareas

## :books: Resumen

* Solventaremos el _bug_ que existe actualmente al añadir tareas

## Descripción

Se ha encontrado un _bug_ en la funcionalidad de añadir tareas al Kanban

### _Steps to reproduce_

1. Añade una tarea con el título "TAREA FÁCIL INICIAL"
2. Añade otra tarea con el título "TAREA COMPLICADA FINAL"
3. Arrastra la "TAREA COMPLICADA FINAL" de la primera a la segunda columna
4. ¡En realidad ha aparecido "TAREA FÁCIL INICIAL" en la segunda columna!

### ¿Qué puede estar pasando?

El _bug_ seguramente provenga de esta línea en `kanban.js`:

```js
function onSubmitNewTask(event) {
    event.preventDefault();
    // ...
    // AQUÍ
    // SIEMPRE se está usando "taskX" como ID del nuevo <div>
    addNewPostIt("taskX", taskTitle, backgroundColorClassName);
    event.target.reset(); // Con esto, se borran los campos del formulario
}
```

Como tenemos _múltiples post-it_ (`<div>`) todos con `id="taskX"`, `id="taskX"`,... Nuestros métodos de _Drag and Drop_ no parecen escoger el `<div>` que _realmente movemos_, sino _el primero que tiene ese `id`_.

Los `ids` en HTML deberían ser únicos.

### La solución

Debería ser que en:

```js
    addNewPostIt(/* Un ID distinto cada vez que se añade una nota */, taskTitle, backgroundColorClassName);
```

..._pasemos_ cada vez un ID distinto.

O sea, que en vez de un `"taskX"` _hardcodeado_, tengamos una variable que se vaya incrementando y sea `"task1"`, `"task2"`, `"task3"`... cada vez que el usuario añade un _post-it_ nuevo.

## :package: La tarea

**Solventa** el _bug_.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

