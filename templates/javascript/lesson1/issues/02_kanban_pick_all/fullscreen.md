Pantalla completa

## :books: Resumen

* Añadiremos una barra superior a la página usando HTML y CSS, que contendrá un botón
* Cuando se pulse el botón la página entrará en pantalla completa

## Descripción

Este tablero Kanban parece que será de mucha utilidad como ayuda visual. Sería excelente si los usuarios pudieran centrarse en él por completo, ignorando otras partes de la _interfaz_ del navegador Web.

Un momento, ¡es posible! Con el [API _Fullscreen_](https://developer.mozilla.org/en-US/docs/Web/API/Fullscreen_API).

## :package: La tarea

Primero, **añade** en `kanban.html` un `<div>` que servirá de barra superior, y dentro de él, otro `<div>` que servirá de contenedor interno y tendrá dentro un botón. Luego, un `<hr>` **así**:

```html
  <body>
    <div id="parent">
      <div class="topBar" data-cy="RANDOMEXPRESSIONtopBar|pageBar|topBarContainer|toolsBar|pageToolsBar|pageToolsBarContainer|toolsBarContainerEND">
        <div class="fullscreenButtonContainer" data-cy="RANDOMEXPRESSIONbuttonContainer|topRightButtonContainer|fullscreenButtonContainer|buttonDiv|topRightButtonDiv|fullscreenButtonDivEND">
          <button id="fullscreenButton" data-cy="RANDOMEXPRESSIONfullscreenButton|topRightButtonEND">Pantalla completa</button>
        </div>
      </div>
      
      <hr class="topBarSeparator">
      
      <!-- Resto del parent -->
```

En `kanban.css` **añade** estas clases para que el botón flote arriba a la derecha:

```css
.fullscreenButtonContainer {
    padding-top: 3px;
    padding-right: 5px;
    float: right;
}

.topBarSeparator {
    clear: both;
}
```

### [API _Fullscreen_](https://developer.mozilla.org/en-US/docs/Web/API/Fullscreen_API)

Basta con invocar `requestFullscreen()` sobre aquel elemento que queremos que se muestre en pantalla completa.

**Añade** la siguiente función a `kanban.js`:

```js
function onFullscreenRequested(event) {
    console.log("RANDOMEXPRESSIONFullscreen has been requested|Fullscreen requested|Fullscreen was requested|User has requested fullscreen|User has requested fullscreen modeEND");
    const parent = document.getElementById("parent");
    parent.requestFullscreen();
}
```

Y **completa** la funcionalidad conectando `onFullscreenRequested` al evento `"click"` del botón.

```js
document.getElementById("fullscreenButton").addEventListener("click", onFullscreenRequested);
```

¿Se ve en pantalla completa la página cuando lo pulsas?

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.

