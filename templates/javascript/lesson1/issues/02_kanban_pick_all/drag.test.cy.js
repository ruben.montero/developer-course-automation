/// <reference types="cypress" />

describe('draggable note', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8000/projects/kanban.html');
      cy.reload(true);
    })
  
    it('issueISSUENUMBER task0 was added and is correct', () => {
        cy.get('div.kanbanColumn')
        .first()
        .find('div[id=task0]')
        .should('have.attr', 'draggable', 'true');

        cy.get('div[id=task0]')
        .should('have.css', 'background-color', 'rgb(250, 250, RANDOMNUMBER1-12END)');

        cy.get('div[id=task0]')
        .contains('RANDOMEXPRESSIONDetección de colisiones entre dos objetos 2D en JS|Detección de colisiones entre objetos 2D en JS|Detección de colisiones de dos objetos 2D en JS|Detección de colisiones de objetos 2D en JSEND');
    })
})
