/// <reference types="cypress" />

describe('new task form', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8000/projects/kanban.html');
      cy.reload(true);
    })
    
    it('issueISSUENUMBER new task with red color is created', () => {
        assertAddedNoteTypeAndBackground('urgent', 'rgb(RANDOMNUMBER234-239END, 0, 0)');
    })

    it('issueISSUENUMBER new task with green color is created', () => {
        assertAddedNoteTypeAndBackground('backend', 'rgb(0, RANDOMNUMBER190-199END, 109)');
    })

    it('issueISSUENUMBER new task with blue color is created', () => {
        assertAddedNoteTypeAndBackground('frontend', 'rgb(RANDOMNUMBER58-65END, RANDOMNUMBER151-155END, 255)');
    })
    
    it('issueISSUENUMBER alert is shown when min characters are not met', () => {
        const stub = cy.stub()  
        cy.on ('window:alert', stub)

        let text = "";
        for (let i = 1; i < RANDOMNUMBER5-9END; i++) {
            text = text + "a";
        }
        cy.get('input[data-cy=RANDOMEXPRESSIONinputTaskName|inputNameTask|inputAddTaskNameEND]')
        .type(text);

        cy.get('input[data-cy=RANDOMEXPRESSIONinputCreateTaskSubmit|inputAddTaskSubmit|inputNewTaskSubmit|inputSubmitEND]')
        .click().then(() => {
            expect(stub.getCall(0)).to.be.calledWith('El título de RANDOMEXPRESSIONla|tuEND tarea debe tener por lo menos RANDOMNUMBER5-9END caracteres');
        });

        cy.get('input[data-cy=RANDOMEXPRESSIONinputTaskName|inputNameTask|inputAddTaskNameEND]')
        .type("a");

        cy.get('input[data-cy=RANDOMEXPRESSIONinputCreateTaskSubmit|inputAddTaskSubmit|inputNewTaskSubmit|inputSubmitEND]')
        .click().then(() => {
            expect(stub.getCall(1)).to.not.exist;
        });
    })
    
    function assertAddedNoteTypeAndBackground(type, backgroundColor) {
        const randName = "HolaCaracola" + Math.trunc(Math.random() * 10000);

        cy.get('input[data-cy=RANDOMEXPRESSIONinputTaskName|inputNameTask|inputAddTaskNameEND]')
        .type(randName);

        cy.get('select[data-cy=RANDOMEXPRESSIONinputTaskType|inputTypeTask|inputAddTaskTypeEND]')
        .select(type);

        cy.get('input[data-cy=RANDOMEXPRESSIONinputCreateTaskSubmit|inputAddTaskSubmit|inputNewTaskSubmit|inputSubmitEND]')
        .click();

        cy.get('div#RANDOMEXPRESSIONtoDoColumn|pendingTasksColumn|unstartedColumn|notStartedColumn|toStartColumnEND')
        .find('div')
        .contains(randName)
        .should('have.css', 'background-color', backgroundColor);

        // assert that only the initial note and the added one are there
        cy.get('div#RANDOMEXPRESSIONtoDoColumn|pendingTasksColumn|unstartedColumn|notStartedColumn|toStartColumnEND')
        .find('div')
        .should('have.length', 2);
    }
})
