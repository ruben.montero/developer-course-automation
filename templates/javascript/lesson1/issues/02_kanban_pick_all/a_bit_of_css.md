Una herramienta para los gestores de proyecto

## :books: Resumen

* Hablaremos sobre metodologías de desarrollo _software_
* Crearemos un archivo `kanban.html`. Mostrará cinco _columnas_ (`<div>`)
* Le daremos una apariencia correcta con un nuevo `kanban.css`

## Descripción

¿Qué es _desarrollar software_? ¿Escribir un programa?

Sí. Más o menos.

Cuando escribimos _código_, estamos _desarrollando_. Puede ser el JavaScript de una página web, el código Erlang de un _backend_ o el Java de una aplicación Android. 

Hasta ahora, el principal enemigo al que nos enfrentamos somos nosotros mismos. Nuestros errores de sintaxis. Nuestros errores de lógica.

* ¿Y si el proyecto es más grande? ¿Y si somos 100 desarrolladores?
* ¿Y si los requisitos del proyecto cambian a medida que lo estamos creando?
* ¿Y si no nos coordinaremos correctamente con nuestros compañeros y cómo hacen las cosas?

Existen _metodologías de desarrollo_ que pretenden abordar estas realidades, pero...

### ...no empecemos la casa por el tejado

Antes de pasar a ver algunas _metodologías de desarrollo_ debemos tener claro:

* El desarrollo _software_ está plagado de dificultades que nada tienen que ver con saber o no saber _cómo_ desarrollar un _código_.
* Existen muchas _metodologías_ que pretenden ser un modelo de _cómo_ afrontar este proceso, pero ninguna es perfecta. 
* Una _metodología_ es una serie de reglas abstractas. Un compañero es real. Escucha a tus compañeros antes que a nadie.

### Y desde luego...

...tengamos claro que hay ciertos valores igual o más importantes que nuestra capacidad para hacer código de calidad o seguir una metodología concreta:

* **Compañerismo**: Ayudar a quien está de nuestro lado como si fuéramos nosotros mismos.
* **Asertividad**: Criticar las ideas y aceptar las críticas sin que se convierta en algo personal.
* **Humildad**: No somos mejores que nadie.
* **Dignidad**: Nadie es mejor que nosotros. La única forma de demostrarlo es mediante nuestro trabajo y nuestros resultados.
* **Honestidad**: _"¡Sí! Yo introduje ese bug que rompió la web durante 15 horas"_, o _"Lo siento. Aún esforzándome al máximo, no puedo llegar al compromiso de pasado mañana"_ serán infinitamente más valiosos que _"No sé cómo pudo llegar ese error ahí"_ o _"Claro, claro, voy a tener todo hecho"_. Quien no comete errores es quien no trabaja. Pero nunca es el fin del mundo. Afrontar nuestros errores es tan necesario como creer en nosotros mismos.

### [Metodologías de desarrollo](https://www.becas-santander.com/es/blog/metodologias-desarrollo-software.html)

La más tradicional es [cascada ó _Waterfall_](https://www.workfront.com/project-management/methodologies/waterfall). Consiste en tener claros todos los requisitos del producto que estamos creando y atravesar etapas de _Diseño_, _Implementación_ y _Pruebas_ uno tras otro. Mientras no está todo diseñado, no pasamos a implementarlo. Y mientras no está todo implementado, no pasamos a probarlo. Esta metodología tiene sus riesgos: Un error en los _requisitos_ se traducirá a correcciones que serán más costosas cuanto más avance el proyecto. Además, no se pueden ver resultados hasta bien llegado el final.

### Metodologías de desarrollo _ágiles_

Son las preferidas hoy en día. Se centran en entregar _resultados_ lo antes posible.

Aquí tenemos un cuadro comparativo de dos metodologías: [SCRUM](https://www.atlassian.com/es/agile/scrum) y [Kanban](https://kanbanize.com/es/recursos-de-kanban/primeros-pasos/que-es-kanban):

 |- | **SCRUM** | **KANBAN**
 |--|-----------|------------
 |Cadencia|_Sprints_ regulares de duración limitada (2-4 semanas)|Flujo continuo
 |Entrega de valor|Al final de cada _Sprint_, con la aprobación del _Product Owner_|Entrega continua o a discreción del equipo
 |Roles|_Product Owner_, _Scrum Master_, _Dev. Team_|No hay roles. Puede incluirse un _Agile Coach_
 |Filosofía de cambio|No hay cambio durante el _Sprint_|El cambio puede ocurrir en cualquier momento

### Tablero Kanban

Es una herramienta útil para dar _visibilidad_ a lo que está llevando a cabo el equipo de desarrollo. 

Está basada en tener _post-it_ en una pared e ir moviéndolos de columna según su estado.

Esta herramienta, a pesar del nombre, no está restringida a la metodología Kanban. Podemos usarla también en Scrum u otras metodologías.

A continuación, se muestra lo que implementaremos al terminar este proyecto:

<img src="javascript/docs/kanban_demo.gif" />

## :package: La tarea

¡Comencemos a desarrollar nuestro tablero Kanban! En esta tarea, sólo abordaremos mostrar las cinco columnas, cada una con un color de fondo distinto.

**Añade** un nuevo enlace a la lista de enlaces en `index.html`:

```html
    <li><a href="/projects/kanban.html">RANDOMEXPRESSIONTablero kanban|Tablero|Tablero agile|Tablero de tareasEND</a></li>
```

Luego, **crea** el documento `kanban.html` dentro de `projects/`:

```html
<html>
  <head>
    <meta charset="utf8"/>
  </head>
  <body>
    <div id="parent">
    </div>
  </body>
</html>
```

¿Puedes navegar hasta la página vacía desde http://localhost:8000 si el servidor está corriendo?

### Cinco columnas en HTML

**Añade** cinco `<div>` **dentro** del `<div id="parent">`. Serán como este:

```html
      <div class="kanbanColumn column1">
        <p><RANDOMEXPRESSIONh3|h2END>RANDOMEXPRESSIONSin empezar|Por hacer|Sin comenzar|PendienteEND</RANDOMEXPRESSIONh3|h2END></p>
      </div>
```

Pon cada uno a continuación del anterior.

Mostrarán estos `RANDOMEXPRESSIONh3|h2END`:

* `RANDOMEXPRESSIONSin empezar|Por hacer|Sin comenzar|PendienteEND`
* `Diseño`
* `Implementación`
* `RANDOMEXPRESSIONPruebas|TestsEND`
* `RANDOMEXPRESSIONTerminado|Finalizado|AcabadoEND`

Todos tendrán como `class` `"kanbanColumn"`, y, adicionalmente, `"column1"`, `"column2"`, `"column3"`, `"column4"` y `"column5"` respectivamente.

### Hoja de estilos (CSS)

**Crea** una nueva carpeta `css/` dentro de `projects/`.

Luego, **crea** un nuevo archivo `kanban.css` dentro de `www/projects/css/`.

Esta _hoja de estilo_ sirve para dar una apariencia especial al HTML. Nosotros presentaremos _columnas_. Para ello, **añade** el siguiente CSS a `kanban.css`:

```css
.kanbanColumn {
    margin: 0.5%;
    padding: 0.5%;
    width: 18%;
    height: 100%;
    float: left;
}

.column1 {
    background-color: rgb(255, 185, 185);
}

.column2 {
    background-color: rgb(255, 185, RANDOMNUMBER240-248END);
}

.column3 {
    background-color: rgb(255, RANDOMNUMBER220-231END, 181);
}

.column4 {
    background-color: rgb(244, 255, 181);
}

.column5 {
    background-color: rgb(RANDOMNUMBER223-240END, 255, 181);
}
```

### ¡Hay que _referenciar_ la hoja de estilos!

Nuestro `kanban.css` todavía está _desconectado_ del HTML principal.

**Añade** la siguiente línea a `kanban.html`:

```diff
  <head>
    <meta charset="utf8"/>
+   <link rel="stylesheet" href="/projects/css/kanban.css" />
  </head>
```

¡Listo!

Visita ahora tu página en http://localhost:8000/projects/kanban.html. ¿Ves las columnas?

Cada una de ella representa una _fase de desarrollo_ en la que puede estar una tarea.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
