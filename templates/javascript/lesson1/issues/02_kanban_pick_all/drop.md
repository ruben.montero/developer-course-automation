Drop

## :books: Resumen

* Completaremos el código HTML y JavaScript para permitir que el _post-it_ pueda ser _soltado_ en cualquier columna

## Descripción

El [API de _Drag and Drop_](https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API) consiste en la propiedad `draggable` que establecimos en la tarea anterior, y una serie de _eventos_:

| Evento | Sucede cuando... |
|--------|------------------|
|[`dragend`](https://developer.mozilla.org/en-US/docs/Web/API/Document/dragend_event)  |...una operación de arrastre termina (e.g.: El usuario pulsa ESC) |
|[`dragenter`](https://developer.mozilla.org/en-US/docs/Web/API/Document/dragenter_event)  |...un elemento arrastrado entra en una zona válida para soltarse |
|[`dragleave`](https://developer.mozilla.org/en-US/docs/Web/API/Document/dragleave_event)  |...un elemento arrastrado sale de una zona válida para soltarse |
|[`dragover`](https://developer.mozilla.org/en-US/docs/Web/API/Document/dragover_event)  |...un elemento es arrastrado sobre una zona válida para soltarse. Este evento se repite en milisegundos a medida que el usuario arrastra |
|[`dragstart`](https://developer.mozilla.org/en-US/docs/Web/API/Document/dragstart_event)  |...un elemento comienza a ser _arrastrado_ |
|[`drop`](https://developer.mozilla.org/en-US/docs/Web/API/Document/drop_event)  |...un elemento es soltado en una zona válida |

### ¿Y cómo se usan estos _eventos_?

Se pueden establecer con `addEventListener` (que ya hemos usado) desde JavaScript, o bien desde HTML directamente.

Las funciones (_callbacks_) que responden a los eventos, por supuesto, deben estar escritas en JavaScript.

### Esquema de nuestra funcionalidad

```mermaid
 graph LR;
E[<b>dragover</b><br><i>e.preventDefault</i> para<br>permitir que se<br>pueda hacer drop]

A[<b>dragstart</b><br>Guardamos en dataTransfer<br>el <i>id</i> de la nota]-->|dataTransfer|B[<b>drop</b><br>Recuperamos el <i>id</i><br>para saber <i>qué</i> nota fue<br> arrastrada.<br>Cogemos ese elemento por<br>su <i>id</i> y lo desplazamos<br> con <i>append</i>]; 
```

## :package: La tarea

**Crea** un nuevo fichero `kanban.js` dentro de `www/projects/js/`.

**Enlázalo** desde `<head>` de nuestro `kanban.html`:

```html
  <head>
    <meta charset="utf8"/>
    <link rel="stylesheet" href="/projects/css/kanban.css" />
    <script defer src="/projects/js/kanban.js"></script>
  </head>
```

### Zona _válida_ para soltar

Todos los elementos HTML de la página, por defecto, no son válidos para _soltar_.

Sin embargo, basta con hacer `event.preventDefault()` durante [`dragover`](https://developer.mozilla.org/en-US/docs/Web/API/Document/dragover_event) para que un elemento sea considerado válido.

**Escribe** la siguiente función en `kanban.js`:

```js
function allowDrop(event) {
    event.preventDefault();
}
```

...y **establécela** como `ondragover` en `kanban.html` para el `<div>` de la segunda columna, **así**:

```diff
-      <div class="kanbanColumn column2">
+      <div class="kanbanColumn column2" ondragover="allowDrop(event)">
```

Si compruebas ahora http://localhost:8000/projects/kanban.html, verás que el _icono_ que aparece ya indica que se puede _soltar_ en la segunda columna.

### Comportamiento _Drag and Drop_

Ahora, nuestra misión es _pasar el **id**_ del `<div>` que está siendo _arrastrado_. Usaremos [`dragstart`](https://developer.mozilla.org/en-US/docs/Web/API/Document/dragstart_event) para empezar con ello.

**Escribe** la siguiente función en `kanban.js` (podría tener cualquier otro nombre que no fuera `drag`):

```js
function drag(event) {
    event.dataTransfer.setData("text", event.target.id);
}
```

...y **establécela** como `ondragstart` en `kanban.html` para el `<div>` de la nota inicial, **así**:

```diff
        <!-- Un post-it inicial -->
        <div id="task0"
          class="tinyPaper yellow"
-         draggable="true">
+         draggable="true"
+         ondragstart="drag(event)">
          Detección de colisiones entre dos objetos 2D en JS
        </div>
```

Podemos aclarar que:

* En la función `drag`, el valor de `event.target.id` es el `id` del elemento que está siendo arrastrado. Si arrastramos la nota inicial, será predeciblemente `"task0"`
* Dicho `id` se _almacena_ en `event.dataTransfer`. Este es un objeto especial del evento `"dragstart"` donde podemos _almacenar_ cosas

### ...y _D-d-d-drop the HTML_

**Modifica** el siguiente código en el `<div>` de la segunda columna, en `kanban.html`:

```diff
-      <div class="kanbanColumn column2" ondragover="allowDrop(event)">
+      <div class="kanbanColumn column2" ondragover="allowDrop(event)" ondrop="drop(event)">
```

Y ahora, **añade** la función `drop` (podría tener cualquier otro nombre) en `kanban.js`:

```js
function drop(event) {
    // Prevenimos la acción por defecto que será abrir el texto como un link
    event.preventDefault();
    
    // Recuperamos el id que nosotros mismos establecimos en drag(event)
    const draggedElementId = event.dataTransfer.getData("text");
    
    // Mediante el id, identificamos y asignamos a una variable el elemento (la nota)
    const draggedElement = document.getElementById(draggedElementId);
    
    // event.target es el elemento sobre el que se hace drop, por
    // ejemplo, la segunda columna
    // Al hacer .appendChild, el elemento se "va" de su padre original
    // y se añade a la nueva columna. ¡Es como arrastrarlo!
    event.target.appendChild(draggedElement);
}
```

¿Puedes arrastrar la nota inicial de la _primera_ columna _a la **segunda**_?

¡Enhorabuena!

Para terminar, **añade** `ondragover` y `ondrop` a **todas** las columnas (`<div>`) para permitir que las _tareas_ se puedan mover libremente de cualquier a cualquier columna.

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
