Formulario para añadir tarea

## :books: Resumen

* Añadiremos un `<form>` que servirá para añadir un nuevo _post-it_ al tablero Kanban
* ¡Todavía no funcionará!
* Añadiremos algo de CSS a la barra superior

## Descripción

Nuestro tablero Kanban ahora mismo sólo tiene un _post-it_ inicial, escrito en el HTML. ¡Qué triste :sad_face:!

¿No sería estupendo que el usuario pudiera crear sus propios _post-it_ y moverlos por el tablero?

Vamos a crear un `<form>` para ello, aunque todavía no vamos a escribir JavaScript.

## :package: La tarea

**Añade** un `<div>` que servirá de contenedor para el formulario, como se muestra a continuación:

En `kanban.html`:

```html
      <div class="topBar" data-cy="RANDOMEXPRESSIONtopBar|pageBar|topBarContainer|toolsBar|pageToolsBar|pageToolsBarContainer|toolsBarContainerEND">
        <div class="fullscreenButtonContainer" data-cy="RANDOMEXPRESSIONbuttonContainer|topRightButtonContainer|fullscreenButtonContainer|buttonDiv|topRightButtonDiv|fullscreenButtonDivEND">
          <button id="fullscreenButton" data-cy="RANDOMEXPRESSIONfullscreenButton|topRightButtonEND">Pantalla completa</button>
        </div>
                
        <!-- Nuevo contenedor -->
        <!-- Ponlo aquí, dentro de la barra superior -->
        <div class="newTaskContainer" data-cy="RANDOMEXPRESSIONnewTaskContainer|formContainer|newTaskFormContainer|newTaskDiv|formDivEND">
         
          <!-- Aquí va nuestro <form> -->
        
        </div> 
        
      </div>
      
      <hr class="topBarSeparator">
```

**Se pide** que, dentro de dicho contenedor, añadas un `<form id="newTaskForm" data-cy="RANDOMEXPRESSIONnewTaskForm|formNewTask|formAddTask|addTaskFormEND">` con:

* **(1)** Una etiqueta `<label>` que encierre el texto `RANDOMEXPRESSIONNueva tarea:|Crear tarea:|Añadir tarea:END`, en **negrita**.
* **(2)** Una etiqueta `<input>` con `name="inputTaskName"`, `type="text"`, `placeholder="RANDOMEXPRESSIONFuncionalidad de login|Funcionalidad de logout|Funcionalidad de registro|Funcionalidad de fullscreenEND"` y `data-cy="RANDOMEXPRESSIONinputTaskName|inputNameTask|inputAddTaskNameEND"`
* **(3)** Una etiqueta [`<select>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/select) con `name="inputTaskType"` y `data-cy="RANDOMEXPRESSIONinputTaskType|inputTypeTask|inputAddTaskTypeEND"`. Encerrará las siguientes _opciones_:

```html
    <option value="other">Otro</option>
    <option value="backend">Backend</option>
    <option value="frontend">Frontend</option>
    <option value="urgent">RANDOMEXPRESSIONUrgente|ImportanteEND</option>
```

* **(4)** Una etiqueta `<input>` con `type="submit"`, `value="RANDOMEXPRESSIONAñadir tarea|Crear tareaEND"` y `data-cy="RANDOMEXPRESSIONinputCreateTaskSubmit|inputAddTaskSubmit|inputNewTaskSubmit|inputSubmitEND"`

Además, **añade** el siguiente CSS a tu `kanban.css` para dar márgenes al formulario y color de fondo a la barra superior:

```css
.newTaskContainer {
    padding-top: 3px;
    padding-bottom: 1px;
    padding-left: 5px;
}

.topBar {
    background-color: rgb(RANDOMNUMBER222-231END, 255, RANDOMNUMBER241-246END);
}
```

### :trophy: Por último

Sube tus cambios al repositorio en un nuevo _commit_.
