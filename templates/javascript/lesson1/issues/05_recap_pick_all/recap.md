¡Trabajo terminado!

## :trophy: ¡Felicidades!

Has realizado las tareas del _sprint_ y has validado tu trabajo con los _tests_. ¡Enhorabuena!

<img src="javascript/docs/sunset.jpg" width=500 />

Has...

* Instalado _Simple Web Server_ para servir una carpeta `www`
* Creado páginas HTML, enlazadas mediante `<a href>`
* Usado `<script>` y distinguido entre que esté al principio o al final del documento
* Usado `<script>` apuntando a un documento JS externo, y distinguido entre `async` y `defer`
* Empleado `addEventListener` sobre `document`, escuchando al evento `"DOMContentLoaded"`
* Creado _nicknames_ aleatorios con JS
* Accedido a un elemento HTML mediante `getElementById`
* Creado elementos HTML con `document.createElement` y modificado su contenido mediante `innerHTML`
* Añadido elementos HTML recién creados al documento, empleando `append` sobre otro elemento
* Usado `IntersectionObserver`
* Usado el API de _Drag 'n' Drop_ y distinguido varios atributos y funciones del mismo
* Usado `event.dataTransfer` para pasar datos de un elemento _draggeado_ a otro donde se va a _dropear_ (en nuestro caso, pasábamos el ID del elemento para moverlo)
* Pasado a modo pantalla completa con `requestFullscreen()`
* Usado `<form>` y sobreescrito el comportamiento por defecto
* Empleado `<canvas>` para _renderizar_ gráficos 2D
* Pintado colores sólidos con `fillRect` e imágenes con `drawImage`
* Implementado el motor de un juego de asteroides
* Usado `addEventListener`, escuchando los eventos `"keydown"` y `"keyup"` para que el usuario pudiera mover la nave espacial
* Empleado `setTimeout` para generar un asteroide periódicamente
* Implementado detección de colisiones
