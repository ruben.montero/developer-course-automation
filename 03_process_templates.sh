#!/bin/bash

CSV_PATH=$1
TEMPLATES_FOLDER=$2
OUTPUT_PATH=$3

function print_help {
    echo Grabs template information from a given folder and generates
    echo GitLab issues as local files, randomized for several users.
    echo Also generates automatized tests for every issue.
    echo
    echo Usage: $0 PROJECTS_CSV_FILE TEMPLATES_FOLDER OUTPUT_FOLDER [--all]
    echo
    echo The PROJECTS_CSV_FILE must contain data formed like
    echo
    echo uid1,username1,Name1 LastName1,name1@email.com,project1_id,ssh_url
    echo uid2,username2,Name2 LastName2,name2@email.com,project2_id,ssh_url
    echo ...
    echo
    echo Typically, you want to use the output of 02_create_projects.sh
    echo as PROJECTS_CSV_FILE.
    echo
    echo The TEMPLATES FOLDER must contain the templates information as
    echo in the following example:
    echo
    echo "TEMPLATES_FOLDER/"
    echo " |"
    echo " |-- 01_ISSUES_FOR_GETTING_STARTED_pick_random_1/"
    echo " |    |"
    echo " |    |-- issue1.md"
    echo " |    |-- issue1.test.js"
    echo " |    |-- issue2.md"
    echo " |    |-- issue2.test.js"
    echo " |"
    echo " |-- 02_SECOND_BLOCK_pick_random_1/"
    echo " |    |"
    echo " |    |-- OPTION_A_pick_all/"
    echo " |    |    |"
    echo " |    |    |-- complex_issue.md"
    echo " |    |    |-- complex_issue.test.js"
    echo " |    |    |-- following_issue.md"
    echo " |    |    |-- following_issue.test.js"
    echo " |    |"
    echo " |    |-- OPTION_B_pick_all/"
    echo " |         |"
    echo " |         |-- other_complex_issue.md"
    echo " |         |-- other_complex_issue.test.js"
    echo " |         |-- other_following_issue.md"
    echo " |         |-- other_following_issue.test.js"
    echo " |"
    echo " |-- 03_FINAL_ISSUES_pick_all/"
    echo "      |"
    echo "      |-- issue_y.md"
    echo "      |-- issue_y.test.js"
    echo "      |-- issue_z.md"
    echo "      |-- issue_z.test.js"
    echo
    echo Note that:
    echo
    echo "* The root folder must contain only subfolders. They can have"
    echo "  any name with relevant prefixes if you want the script to"
    echo "  process them in a certain order, as long as the do not"
    echo "  contain whitespaces or weird characters and every folder"
    echo "  name MUST contain one of the following suffixes:"
    echo 
    echo "  - pick_all           (all sub-items will be processed)"
    echo "  - pick_random_{n}    (only {n} sub-items will be processed)"
    echo
    echo "* Folders can have subfolders, hierarchically, as many levels"
    echo "  as the operating system allows."
    echo
    echo "* If passing --all, the pick_random_{n} will be treated as"
    echo "  pick_all. This is useful for selecting all issue templates"
    echo "  when developing them"
    echo
    echo "* The issues filenames MUST not contain whitespaces or weird"
    echo "  characters and file format MUST be .md. They are expected to"
    echo "  contain valid GitLab markdown language. Inside markdown,"
    echo "  further randomization of the issue(s) body and, accordingly,"
    echo "  test file(s) throughout the whole bunch of issues for each"
    echo "  user, is allowed using these patterns:"
    echo
    echo "  - RANDOMNUMBERx-yEND  (will be replaced by a random integer"
    echo "                         between x and y)"
    echo "  - RANDOMEXPRESSIONa|b|cEND  (will be replaced by a, b or c,"
    echo "                               picked at random."
    echo "                               Any |-separated list of"
    echo "                               expressions is allowed)"
    echo -e "                              \e[1;31mKnown issue\e[0m: square brackets []"
    echo "                              within expressions list will"
    echo "                              cause trouble"
    echo "  - ISSUENUMBER (will be replaced by N, assuming the issue"
    echo "                 was picked as the Nth one in the total count"
    echo "                 for the current developer/project, regardless"
    echo "                 of which subfolder the issue belonged to)"
    echo "  - DEVELOPERNUMBER (will be replaced by the number of the"
    echo "                     developer, as represented by the ordered"
    echo "                     list from PROJECTS_CSV_FILE. 2-digit"
    echo "                     number from 01 up to 99 (+99 is untested)"
    echo "  - TESTFILENAME (will be replaced by the name of the"
    echo "                  generated test file without extension."
    echo "                  Useful for Java test classes)"
    echo
    echo "  * More than 1 pattern per line is allowed"
    echo "  * The value chosen for a given RANDOM pattern will be"
    echo "    preserved for all generated issues for a developer"
    echo
    echo "* Every issue should have an associated test file, which holds"
    echo "  the test for TDD-development. It MUST have the same name,"
    echo "  followed by .test and .{code_extension}. This scripts knows"
    echo "  nothing about code or how to run these scripts. It only"
    echo "  searches for the .test. part of the name and then picks it,"
    echo "  replacing any RANDOMNUMBERx-yEND, RANDOMEXPRESSIONa|b|cEND"
    echo "  or ISSUENUMBER according to the values chosen for the issue."
    echo "  Random values connected to other issues are allowed."
    echo
    echo For every line of the PROJECTS_CSV_FILE, this script generates
    echo one random output made up from the template information.
    echo In the example, for project_id1 of username1 an example output
    echo folder could go as follows:
    echo
    echo "username1/"
    echo " |"
    echo " |-- issues/"
    echo " |    |"
    echo " |    |-- 001_issue2.md (e.g. containing RANDOMNUMBER10-50END"
    echo " |    |                  replaced by 42)"
    echo " |    |-- 002_complex_issue.md"
    echo " |    |-- 003_following_issue.md"
    echo " |    |-- 004_issue_y.md    (e.g. ISSUENUMBER replaced by 4)"
    echo " |    |-- 005_issue_z.md"
    echo " |"
    echo " |-- tests/"
    echo "      |"
    echo "      |-- T001issue2.js (e.g. containing RANDOMNUMBER10-50END"
    echo "      |                  replaced by 42)"
    echo "      |-- T002complex_issue.js"
    echo "      |-- T003following_issue.js"
    echo "      |-- T004issue_y.js    (e.g. ISSUENUMBER replaced by 4)"
    echo "      |"
    echo "      |-- T005issue_z.js (e.g. RANDOMNUMBER10-50END"
    echo "                          replaced by 42 also)"
    echo
    echo The T prefix is used by default. If a .py extension is detected,
    echo the test_ prefix will be used instead.
    echo
    echo At the end of the execution you should expect one folder
    echo for each input username/project passed in PROJECTS_CSV_FILE,
    echo containing both issue bodies and automatized tests.
    echo
    echo -e "\e[1;33mNOTE THAT: \e[0m"
    echo This script does not connect to any GitLab API to publish
    echo issues nor commits the automatized tests to any repository.
    echo It just does the heavy lifting for randomizing issues for
    echo several users and outputs the chewed stuff to OUTPUT_FOLDER.
    echo
    echo Additionally, you can place a NOTICE file in the root of the
    echo TEMPLATES_FOLDER and its contents will be printed at the end
    echo of the execution. This can be useful as a reminder of extra
    echo tasks.
}



# =================================================
# Helper functions for processing template contents

function get_number_from_range {
	# Find (-) in the string, so that we can tell which numbers conform the range
	# Link [1] (see at bottom)
	local range="$1"; local substr="-"
	prefix=${range%%$substr*}
	index=${#prefix}
	if [[ index -eq ${#range} ]]; then
		echo ERR_NOT_VALID_RANGE; exit -1
	else
		number1=${range:0:$index}
		number2=${range:$index+1}
		# Verify they are numbers. Link [2] (see at bottom)
		re='^[0-9]+$'
		if ! [[ $number1 =~ $re || $number2 =~ $re ]]; then
			echo ERR_NOT_VALID_RANGE; exit -1
		fi
		range_length=$((number2-number1))
		echo $(( $RANDOM % range_length + number1 ))
	fi
}

function get_random_expression {
	local list="$1"; local i=0
	# We count the number of appearances of char | to know how many expressions are there
	number_of_expressions=$(($(echo "$list" | tr -cd '|' | wc -c)+1))
	# And then get a random one using cut
	random_index=$(( $RANDOM % $number_of_expressions ))
	expression=$(echo "$list" | cut -d '|' -f $((random_index+1)))
	echo $expression
}

function process_issue_and_test_templates {
	# Precondition: $1 and $2 must hold validated filenames 
	local issue_filename="$1"; local test_filename="$2"
	local test_fileextension=$(echo "$test_filename" | rev | cut -d '.' -f 1 | rev) 
	if [ $test_fileextension = "py" ]; then
		prefix="test_"
	else
		prefix="T"
	fi
	local output_issue_filename=$(printf "%0*d" 3 $current_dev_issue_count)_$(basename "$issue_filename")
	local output_test_filename=$prefix$(printf "%0*d" 3 $current_dev_issue_count)$(basename "$test_filename")
	output_test_filename=$(echo $output_test_filename | sed 's/\(.*\)\.test/\1/')
	local username=$3
	# Read the issue template and generate an output replacing placeholders
	# for appropriate values
	# NOTE: Hail to those capable of doing stuff like this with a couple of sed commands
	while IFS= read -r line; do # Don't ignore leading whitespaces [3]
		line_status="PENDING_PATTERNS"
		while [ $line_status == "PENDING_PATTERNS" ]; do
			case $line in
				*RANDOMNUMBER*END*)
					range=${line#*RANDOMNUMBER}; range=${range%%END*}
					simple_hash=$(echo $range | cksum | cut -f 1 -d ' ')
					# Chosen random values are stored in an associative array
					# using a hash function over the 'key', for later use when
					# replacing placeholders in .md file again, or in test file
					already_chosen_random_number=${patterns[simple_hash]}
					if [[ -z $already_chosen_random_number ]]; then
						# Not generated yet. Let's create a random number
						random_number=$(get_number_from_range $range)
						if [ $random_number == ERR_NOT_VALID_RANGE ]; then
							echo -e "\e[1;31mERROR: \e[0mFound invalid pattern when trying to process the following line:"
							echo "$line"
							echo "...inside file $issue_filename. We failed to parse a valid RANDOMNUMBERx-yEND pattern. Aborting now..."
							exit -1
						fi
						patterns[$simple_hash]=$random_number
						line=$(echo "$line" | sed -e "s/RANDOMNUMBER${range}END/$random_number/")
					else
						line=$(echo "$line" | sed -e "s/RANDOMNUMBER${range}END/$already_chosen_random_number/")
					fi;;
				*RANDOMEXPRESSION*END*)
					expressions_list=${line#*RANDOMEXPRESSION}; expressions_list=${expressions_list%%END*}
					simple_hash=$(echo "$expressions_list" | cksum | cut -f 1 -d ' ')
					already_chosen_random_expr=${patterns[simple_hash]}
					if [[ -z $already_chosen_random_expr ]]; then
						# Not generated yet. Let's pick a random expression from the list a|b|c
						random_expression=$(get_random_expression "$expressions_list")
						patterns[$simple_hash]=$random_expression
						new_line=$(echo "$line" | sed -e "s%RANDOMEXPRESSION${expressions_list}END%$random_expression%")
						if [[ -n $line && -z $new_line ]]; then
							echo -e "\e[1;31mERROR: \e[0m$line was non-empty, but it apparently got emptied after sed substitution. Maybe there was a problem with it. Try not to use error-prone characters within the RANDOMEXPRESSION placeholder. Aborting..."
							exit -1
						fi
						line=$(echo "$new_line")
					else
						line=$(echo "$line" | sed -e "s%RANDOMEXPRESSION${expressions_list}END%$already_chosen_random_expr%")
					fi;;
				*ISSUENUMBER*)
					line=$(echo "$line" | sed -e "s/ISSUENUMBER/$current_dev_issue_count/");;
				*DEVELOPERNUMBER*)
					dev_count_two_digit=$(printf "%0*d" 2 $dev_global_count)
					line=$(echo "$line" | sed -e "s/DEVELOPERNUMBER/$dev_count_two_digit/");;
				*TESTFILENAME*)
					output_test_filename_without_extension=$(echo "${output_test_filename%.*}")
					line=$(echo "$line" | sed -e "s/TESTFILENAME/$output_test_filename_without_extension/");;
				*)
					line_status="DONE"
					echo "$line" >> "$OUTPUT_PATH/$username/issues/$output_issue_filename";;
			esac
		done
	done < $issue_filename
	# Now read the test file and do the same, using same values for placeholders
	while IFS= read -r line; do # Don't ignore leading whitespaces [3]
		line_status="PENDING_PATTERNS"
		while [ $line_status == "PENDING_PATTERNS" ]; do
			case $line in
				*RANDOMNUMBER*END*)
					range=${line#*RANDOMNUMBER}; range=${range%%END*}
					simple_hash=$(echo $range | cksum | cut -f 1 -d ' ')
					already_chosen_random_number=${patterns[simple_hash]}
					if [[ -z $already_chosen_random_number ]]; then
						echo -e "\e[1;31mERROR: \e[0mFound invalid pattern when trying to process the following line:"
						echo $line
						echo "...inside file $test_filename. It appears to contain a RANDOMNUMBER pattern which wasn't used in any previous issue template. That is not allowed. Aborting now..."
						exit -1
					fi
					line=$(echo "$line" | sed -e "s/RANDOMNUMBER${range}END/$already_chosen_random_number/");;
				*RANDOMEXPRESSION*END*)
					expressions_list=${line#*RANDOMEXPRESSION}; expressions_list=${expressions_list%%END*}
					simple_hash=$(echo "$expressions_list" | cksum | cut -f 1 -d ' ')
					already_chosen_random_expr=${patterns[simple_hash]}
					if [[ -z $already_chosen_random_expr ]]; then
						echo -e "\e[1;31mERROR: \e[0mFound invalid pattern when trying to process the following line:"
						echo $line
						echo "...inside file $test_filename. It appears to contain a RANDOMEXPRESSION pattern which wasn't used in any previous issue template. That is not allowed. Aborting now..."
						exit -1
					fi
					line=$(echo "$line" | sed -e "s%RANDOMEXPRESSION${expressions_list}END%$already_chosen_random_expr%");;
				*ISSUENUMBER*)
					line=$(echo "$line" | sed -e "s/ISSUENUMBER/$current_dev_issue_count/");;
				*DEVELOPERNUMBER*)
					dev_count_two_digit=$(printf "%0*d" 2 $dev_global_count)
					line=$(echo "$line" | sed -e "s/DEVELOPERNUMBER/$dev_count_two_digit/");;
				*TESTFILENAME*)
					output_test_filename_without_extension=$(echo "${output_test_filename%.*}")
					line=$(echo "$line" | sed -e "s/TESTFILENAME/$output_test_filename_without_extension/");;
				*)
					line_status="DONE"
					echo "$line" >> "$OUTPUT_PATH/$username/tests/$output_test_filename";;
			esac
		done
	done < $test_filename
	echo -e "\e[32m[DONE]\e[0m Successfully generated $output_issue_filename and $output_test_filename"
}

function ensure_template_files_are_correct_and_go_on {
	local template_issue_name="$1"
	local user=$2
	local number_of_matching_test_files=0
	if [[ "$template_issue_name" != *.md ]]; then
		echo -e "\e[1;31mERROR: \e[0mI tried to process the template file $template_issue_name but it does not appear to have markdown file extension (.md). In case you did not read the beautifully explained help of this script, please, invoke me passing '-h' parameter. Aborting now..."
		exit -1
	fi
	for x in ${template_issue_name:0:${#template_issue_name}-3}.test.*; do
		template_test_name=$x
		number_of_matching_test_files=$((number_of_matching_test_files+1))
		if [ $number_of_matching_test_files -gt 1 ]; then
			echo -e "\e[1;31mERROR: \e[0mOnly ONE file like '${template_issue_name:0:${#template_issue_name}-3}.test.{code_extension}' is allowed. There are more than 1. Aborting now..."
			exit -1
		fi
	done
	if [[ "$template_test_name" == *"*"* ]]; then # contains '*'? Then wildcard failed to expand due to not matching any file
		echo -e "\e[1;31mERROR: \e[0mFailed to find one file like '${template_issue_name:0:${#template_issue_name}-3}.test.{code_extension}'. Aborting now..."
		exit -1
	fi
	mkdir -p "$OUTPUT_PATH/$user/issues"
	mkdir -p "$OUTPUT_PATH/$user/tests"
	current_dev_issue_count=$((current_dev_issue_count+1))
	process_issue_and_test_templates $template_issue_name $template_test_name $user
}



# ===============================================
# Helper functions for traversing through folders

function extract_pick_random_number_or_abort {
	# Finds 'pick_random' substring in the directory name, so that 
	# we can tell what's the number after it; e.g.: pick_random_{n}
	# Link [1] (see at bottom)
	local directory="$1"
	substr="pick_random"
	prefix=${directory%%$substr*}
	index=${#prefix}
	if [[ index -eq ${#directory} ]]; then
		echo ERR_PICK_RANDOM_NOT_FOUND; exit -1
	else
		number_in_str=${directory:$index+12} # 12 is 'pick_random_' length
		# Link [2] (see at bottom)
		re='^[0-9]+$'
		if ! [[ $number_in_str =~ $re ]]; then
			echo ERR_NOT_A_NUMBER; exit -1
		fi
		echo $number_in_str
	fi
}

function iterate_folder_or_file {
	local f="$1"
	local user=$2\
	local folder_subitems=()
	local i=0; local j=0
	if [ -f $f ]; then # f is a file
		# New in 1.9; print NOTICE at end of execution
		# So, when processing templates, we'll explicitly ignore such file
		if [[ ! "$f" = *"NOTICE" ]]; then
			echo "Processing template file $f..."
			ensure_template_files_are_correct_and_go_on $f $user
		else
			echo "Ignoring NOTICE file. It will be printed at the end of execution..."
		fi
	else               # f is a directory
		echo "Entering directory $f..."
		if [ -z "$(ls -A $f)" ]; then
			#FIX-ME: This doesn't take into account that a folder might
			#containt only .test. files; and that'd be troublesome
			echo -e "\e[1;31mERROR: \e[0mDirectory $f is empty. What to do? Aborting..."
			exit -1
		fi
		# Add to an array all to-be processed sub-items; those who are
		# either folders or .md issue description files. This is, just
		# skip .test. files 
		for subitem in $f/*; do
			if [[ "$subitem" != *.test.* ]]; then
				folder_subitems[$i]=$subitem
				i=$((i+1))
			fi
		done
		# Let's do the interesting part
		if [[ "$f" == "$TEMPLATES_FOLDER" || $(basename "$f") == *pick_all* ]]; then
			for subitem in ${folder_subitems[@]}; do
				iterate_folder_or_file $subitem $user
			done
		else
			number=$(extract_pick_random_number_or_abort $f)
			case $number in
			ERR_NOT_A_NUMBER)
				echo -e "\e[1;31mERROR: \e[0mNot a valid number. How many random sub-items should I process? I was expecting a directory ending with _pick_random_{n}, for instance, _pick_random_5. Aborting..."; exit -1;;
			ERR_PICK_RANDOM_NOT_FOUND)
				echo -e "\e[1;31mERROR: \e[0m$directory directory does not contain \'pick_all\' nor \'pick_random_{n}\'. One of those options is mandatory. Aborting..."; exit -1;;
			*)
				if [[ $DEBUG_GRAB_ALL_ISSUES == 'YES' ]]; then
					echo -e "\e[1;33mINFO: \e[0mForcefully accessing ALL elements inside a pick_random folder..."
					for subitem in ${folder_subitems[@]}; do
						iterate_folder_or_file $subitem $user
					done
				else
					# Shuffle the subitems array to randomize its content
					folder_subitems=( $(shuf -e "${folder_subitems[@]}") )
					# And process the first N elements - this is the simplest
					# way to randomly pick N elements among all
					while [ $j -lt $number ]; do
						iterate_folder_or_file ${folder_subitems[j]} $user
						j=$((j+1))
					done
				fi;;
			esac
		fi
	fi
}



# ===============================================
# Main

if [[ "$1" = "--help" || "$1" = "-h" ]]; then
	print_help
	echo
	echo -e "\e[33mVersion: \e[0m1.10.0 (12/11/2022)"
	exit 0
fi

if (( $# != 3 )); then
	if [[ $# == 4 && $4 == '--all' ]]; then
		# Force-ignore pick_random_{n} folders.
		# Useful when creating templates; as all will be picked
		DEBUG_GRAB_ALL_ISSUES=YES
	else
		echo -e "\e[1;31mERROR: \e[0mIllegal number of parameters"
		echo
		print_help
		exit -1
	fi
fi

if [ -d "$OUTPUT_PATH" ]; then
    echo -e "\e[1;31mERROR: \e[0mOutput folder '$OUTPUT_PATH' already exists. Aborting execution to prevent overwritting it..."
    exit -1
fi

dev_global_count=0
echo Creating output folder...
mkdir $OUTPUT_PATH
echo Reading CSV file with already existing user data...
while IFS=, read -r uid username name email projectid ssh_url
do
	echo ========================================
	# https://tecadmin.net/bash-remove-double-quote-string/
	current_username=`sed -e 's/^"//' -e 's/"$//' <<<"$username"`
	current_dev_issue_count=0
	dev_global_count=$((dev_global_count+1))
	patterns=()
	echo Iterating directories for user $current_username
	iterate_folder_or_file $TEMPLATES_FOLDER $current_username # Recursive main loop
done < $CSV_PATH

echo -e "\e[42m[OK]\e[0m All finished. Have a nice day!"

if [ -f "$TEMPLATES_FOLDER/NOTICE" ]; then
	echo
	echo ===================================================
	echo
	echo -e "\e[1;33mNOTICE\e[0m"
	echo
	while IFS= read -r line; do # Don't ignore leading whitespaces [3]
		echo -e "\e[1;33m$line\e[0m"
	done < "$TEMPLATES_FOLDER/NOTICE"
fi

# Links:
# [1] https://www.tutorialkart.com/bash-shell-scripting/bash-index-of-substring-in-string/
# [2] https://stackoverflow.com/questions/806906/how-do-i-test-if-a-variable-is-a-number-in-bash
# [3] http://mywiki.wooledge.org/BashFAQ/001#Trimming
