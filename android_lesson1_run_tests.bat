@ECHO off
set BROWSER=none

if [%1] == [] GOTO missing_csv

set INPUT_CSV=%1

:main
echo Before proceeding to run tests, ensure that the following are true:
echo - The repositories/ folder exists and contains every Git repository
echo   as created by 05_initialize_repositories.sh
echo - Git username/email are correctly configured here
echo - Android ecosystem is correct
echo ...
pause
echo Verifying emulator...
%USERPROFILE%\AppData\Local\Android\Sdk\emulator\emulator.exe -list-avds | find /i "Nexus_S_API_28"
if not errorlevel 1 (
  echo Nexus_S_API_28 is installed. Starting...
  start "EMULATORRUNNER" %USERPROFILE%\AppData\Local\Android\Sdk\emulator\emulator.exe -avd Nexus_S_API_28
  echo Done
  timeout 30
) else (
  echo ERROR: Nexus_S_API_28 not installed. Please install it via Android Studio.
)
SET ANDROID_HOME=%USERPROFILE%\AppData\Local\Android\Sdk
echo Changing to repositories folder...
cd repositories
(
FOR /F "tokens=2* delims=," %%A IN (..\%INPUT_CSV%) DO (
	echo %%A\programacion-multimedia-y-dispositivos-moviles
	cd %%A\programacion-multimedia-y-dispositivos-moviles
	git pull
	cd android-introduction
	run-tests.bat
	git add result.json
	git commit -m "Added test results for android-introduction sprint"
	git push -u origin master
	cd ..\..\..
 )
)

pause
cd ..
GOTO finish

:missing_csv
echo Missing 1st parameter. We are expecting the csv file path.
set /p USEDEFAULT="Do you want to use a default value (gitlab_projects.csv)? (y/n) "
if [%USEDEFAULT%] == [y] GOTO use_default_csv
echo OK. Aborting...
pause
exit

:use_default_csv
echo OK. Using default...
set INPUT_CSV=gitlab_projects.csv
GOTO main

:finish
echo All done. Finishing...
pause
