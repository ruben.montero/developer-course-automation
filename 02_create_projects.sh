#!/bin/bash

GITLAB_URL="$(cat credentials/gitlab_url)"
CERT_NAME="credentials/my.crt"
TOKEN="$(cat credentials/gitlab_admin_token)"
GITLAB_UID="$(cat credentials/gitlab_admin_uid)"
CSV_PATH=$1
OUTPUT_PATH="gitlab_projects.csv"
PROJECT_NAME=$2

function print_help {
    echo Authenticates against a GitLab API and creates
    echo a personal project with a given name for several users.
    echo
    echo Usage: $0 USERS_CSV_FILE PROJECT_NAME
    echo
    echo The USERS_CSV_FILE must contain data formed like
    echo
    echo uid1,username1,Name1 LastName1,name1@email.com
    echo uid2,username2,Name2 LastName2,name2@email.com
    echo ...
    echo
    echo Typically, you want to use the output of 01_create_users.sh
    echo as USERS_CSV_FILE.
    echo
    echo A file named \'gitlab_projects.csv\' will be generated with
    echo the very same content but the GitLab project id is added as the
    echo fifth column like this:
    echo
    echo uid1,username1,Name1 LastName1,name1@email.com,project_id1,ssh_url
    echo uid2,username2,Name2 LastName2,name2@email.com,project_id2,ssh_url
    echo ...
}

if [[ "$1" = "--help" || "$1" = "-h" ]]; then
	print_help
	echo
    echo -e "\e[33mVersion: \e[0m1.2.0 (30/05/2022)"
	exit 0
fi

if [[ $GITLAB_URL == 'https://my.gitlab.instance' ]]; then
    echo -e "\e[1;31mERROR: \e[0mIt appears that default (dummy) credentials are being used. You should appropriately configure the contents of credentials/ folder to allow access to your GitLab instance."
    exit -1
fi

if (( $# != 2 )); then
    echo -e "\e[1;31mERROR: \e[0mIllegal number of parameters."
    echo
    print_help
    exit -1
fi

if [ -z "$(command -v jq)" ]; then
	echo jq is not installed, you can install it using sudo apt-get install jq
	exit -1
fi

if [ -f "$OUTPUT_PATH" ]; then
    echo -e "\e[1;31mERROR: \e[0mOutput file '$OUTPUT_PATH' already exists. Aborting execution to prevent overwritting it..."
    exit -1
fi

echo Removing responses folder for any previous run...
rm -rf responses
echo Creating new folder...
mkdir responses

echo Sending out version check request...
curl --cacert $CERT_NAME --header "PRIVATE-TOKEN: $TOKEN" "$GITLAB_URL/api/v4/version" | jq . > responses/version.json
gversion=$(jq .version responses/version.json)
if [[ -z ${gversion} || "$gversion" == "null" ]]; then
	echo -e "\e[1;31mERROR: \e[0mWrong response. Halting execution. Maybe you want to verify the GITLAB_URL, GITLAB_UID, TOKEN or CERT_NAME inside the script"
	exit -1
else
	echo -e "\e[42m[OK]\e[0m Successfully received response from host."
	echo GitLab version is $gversion
fi
echo Reading CSV file with already existing user data...
while IFS=, read -r uid username name email
do
	# This will fire HTTP requests against the GitLab API in a for-loop
	# That is dangerous! Take into consideration the number of projects to
	# create and the rate limits configuration of your instance
	# https://docs.gitlab.com/ee/security/rate_limits.html
	echo ========================================
	username=`sed -e 's/^"//' -e 's/"$//' <<<"$username"` # https://tecadmin.net/bash-remove-double-quote-string/
	echo "Creating project named $PROJECT_NAME for $name ($email)"
	curl --cacert $CERT_NAME -X POST -F "name=$PROJECT_NAME" \
		--header "PRIVATE-TOKEN: $TOKEN" "$GITLAB_URL/api/v4/projects/user/$uid" | jq . > responses/create_project_$username.json
	response_id=$(jq .id responses/create_project_$username.json)
	ssh_url_to_repo=$(jq .ssh_url_to_repo responses/create_project_$username.json)
	if [[ -z ${response_id} || "$response_id" = "null" ]]; then
		echo -e "\e[1;31mERROR: \e[0mWrong response. Halting execution. Maybe you want to check the server reply under the responses/ folder..."
		echo -e "\e[1;33mWARNING: \e[0m$OUTPUT_PATH output file is incomplete and could be overwritten. You might want to backup it"
		exit -1
	fi
	echo -e "\e[42m[OK]\e[0m Successfully created project $response_id: $PROJECT_NAME for $name"
	echo Adding to output file...
	echo "$uid,$username,$name,$email,$response_id,$ssh_url_to_repo" >> $OUTPUT_PATH
done < $CSV_PATH
echo Removing responses folder after requests loop has exited successfully...
rm -rf responses
echo -e "\e[42m[OK]\e[0m All finished. Have a nice day!"
