#!/bin/bash

CSV_PATH=$1
REPO_RESULTS_PATH=$2
OUTPUT_PATH="lesson_results.csv"
REPOSITORIES_PATH="repositories/"

function print_help {
    echo Grabs information of test results from various existing
    echo repositories related to GitLab users and produces a
    echo -e "\e[1;33m$OUTPUT_PATH\e[0m file with all of them" 
    echo
    echo "Usage: $0 PROJECTS_CSV_FILE RESULTS_FILE_PATH_WITHIN_REPO"
    echo
    echo The PROJECTS_CSV_FILE should contain data like:
    echo
    echo uid1,username1,Name1 LastName1,name1@email.com,project_id1,ssh_url1
    echo uid2,username2,Name2 LastName2,name2@email.com,project_id2,ssh_url2
    echo
    echo Typically, you want to use the output of 02_create_projects.sh
    echo
    echo The RESULTS_FILE_PATH_WITHIN_REPO indicates where the test
    echo results file is located within each repository. It should
    echo include the name of the repository itself.
    echo
    echo For example, if you are concluding the lesson for developers
    echo with a web-development repository each one, then:
    echo 
    echo \"web-development/whatever/folder/inside/repo/results.json\"
    echo
    echo ...is the RESULTS_FILE_PATH_WITHIN_REPO you want. It is
    echo expected to point to a json file with the following key-value
    echo pairs:
    echo
    echo - tests_exitosos
    echo - tests_ejecutados
    echo - commit_info
}

if [[ "$1" = "--help" || "$1" = "-h" ]]; then
	print_help
	echo
    echo -e "\e[33mVersion: \e[0m1.0.0 (30/09/2022)"
	exit 0
fi

if (( $# != 2 )); then
    echo -e "\e[1;31mERROR: \e[0mIllegal number of parameters."
    echo
    print_help
    exit -1
fi

if [ -z "$(command -v jq)" ]; then
	echo jq is not installed, you can install it using sudo apt-get install jq
	exit -1
fi

if [ -f "$OUTPUT_PATH" ]; then
    echo -e "\e[1;31mERROR: \e[0mOutput file '$OUTPUT_PATH' already exists. Aborting execution to prevent overwritting it..."
    exit -1
fi

if [ ! -d "$REPOSITORIES_PATH" ]; then
    echo -e "\e[1;31mERROR: \e[0mDirectory '$REPOSITORIES_PATH' does not exist. We expect it to be there and contain repositories as output by 05_initialize_repositories.sh and with the test results JSON ready. Aborting execution..."
    exit -1
fi

echo Reading CSV file with user data...
while IFS=, read -r id username name
do
	echo ========================================
	echo "Reading results.json for user $name ($username)..."
	successes=$(jq .tests_exitosos $REPOSITORIES_PATH/$username/$REPO_RESULTS_PATH)
	total_run=$(jq .tests_ejecutados $REPOSITORIES_PATH/$username/$REPO_RESULTS_PATH)
	sha=$(jq .commit_info $REPOSITORIES_PATH/$username/$REPO_RESULTS_PATH)
	echo Adding to output file...
	echo "$username,$successes,$total_run,$sha" >> $OUTPUT_PATH
done < $CSV_PATH

echo -e "\e[42m[OK]\e[0m All finished. Have a nice day!"
